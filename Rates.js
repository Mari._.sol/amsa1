$(document).ready(function() {

    $('#tablaClients tfoot th').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        });
    
    
    var IdRate, opcion;
    opcion = 4;
    
        //VALIDAR QUE SE TENGAN LOS PERMISO EN CASO CONTRARIO NO SE PUEDE EDITAR LAS TARIFAS DE LOS RATES
    permiso= $("#editartarifas").val();
    if (permiso == 1){
        botones="<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button><button class='btn btn-danger btn-sm btneliminar'><i class='material-icons'>delete</i></button></div></div>";
    }
    else{
        botones="";
    }
    
    tablaRates= $('#tablaClients').DataTable({
        
        //resumen de total de lotes, total de pacas y suma de pesos 
   
        //para usar los botones   
            responsive: "true",
            dom: 'Brtilp',       
            buttons:[ 
                {
                    extend:    'excelHtml5',
                    text:      '<i class="fas fa-file-excel"></i> ',
                    titleAttr: 'Export to Excel',
                    className: 'btn btn-success',
                    exportOptions: {
                        columns: ":not(.no-exportar)"
                    }
                },
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fas fa-file-pdf"></i> ',
                    titleAttr: 'Export to PDF',
                    className: 'btn btn-danger',
                    exportOptions: {
                        columns: ":not(.no-exportar)"
                    }
                },
                /*{
                    extend:    'print',
                    text:      '<i class="fa fa-print"></i> ',
                    titleAttr: 'Imprimir',
                    className: 'btn btn-info'
                },*/
            ],
        
        initComplete: function () {
                // Apply the search
                this.api().columns().every( function () {
                    var that = this;
     
                    $( 'input', this.footer() ).on( 'keyup change clear', function () {
                        if ( that.search() !== this.value ) {
                            that
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );
        },
        "order": [ 0, 'dsc' ],
        "scrollX": true,
        "scrollY": "62vh",
        "autoWidth": true,
        "scrollCollapse": true,
        "lengthMenu": [100,200,300,500],
        //"aoColumnDefs": [{ "bVisible": false, "aTargets": [ 19 ]}],
        fixedColumns:   {
                left: 0,
                right: 1
            },
        "ajax":{            
            "url": "bd/crudRates.php", 
            "method": 'POST', //usamos el metodo POST
            "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
            "dataSrc":""
        },
        "columns":[
            {"data": "IDRate"},
            {"data": "Linea", width: 600},          
            {"data": "Zone", className: "text-center"},            
            {"data": "City",  width: 300, className: "text-center"},
            {"data": "AverageCost", className: "text-center"},
            {"data": "Total", className: "text-center", render: $.fn.dataTable.render.number( ',', '.', 2) },
            {"data": "StartDate", className: "text-center"},
            {"data": "EndDate", className: "text-center"},
            {"data": "Status", className: "text-center" },
           
            {"defaultContent": botones}
        ]

       
       

       
    });     

  

    
    $('.dataTables_length').addClass('bs-select');
    
    var fila; //captura la fila, para editar o eliminar
    //submit para el Alta y Actualización
    $('#formrates').submit(function(e){
    
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
        transport =  $("#transport").val();
        zone= $("#zone").val();
        city=$("#city").val();      
        promedio = $("#costprom").val();
        startdate=$("#startdate").val();
        fechafin= $("#enddate").val();

        const  [year, month, day] = startdate.split('-');
        startdate = `${day}/${month}/${year}`;

        const  [year1, month1, day1] = fechafin.split('-');
        fechafin = `${day1}/${month1}/${year1}`;
        //CONVERTIR A FECHA EL CAMPO DATE PARA HACER COMPARACIONES
        var fini = new Date(year, month, day); 
        var ffin = new Date(year1, month1, day1); 

        if(ffin < fini){
            document.getElementById('alerta').style.display = 'block'        
        }
        else{
           
                if (opcion == 5){
                
                $.ajax({
                    url: "bd/crudRates.php",
                    type: "POST",
                    datatype:"json",
                    data:  {opcion:opcion,zone:zone,transport:transport,promedio:promedio,startdate:startdate,fechafin:fechafin,city:city },    
                    success: function(data){
                        alert(data);
                        tablaRates.ajax.reload(null,false);
                        $('.inactivo').prop('checked', false);
                        $('.activo').prop('checked', false);
                        $('.todos').prop('checked', true);  
                        
                    
                    },
                    error: function(data) {
                        alert('error');
                    }
                });
            
                }

                else{
                    $.ajax({
                        url: "bd/crudRates.php",
                        type: "POST",
                        datatype:"json",
                        data:  {opcion:opcion,IdRate:IdRate,zone:zone,transport:transport,promedio:promedio,startdate:startdate,fechafin:fechafin,city:city },    
                        success: function(data){
                            alert(data);
                            tablaRates.ajax.reload(null,false);
                            $('.inactivo').prop('checked', false);
                            $('.activo').prop('checked', false);
                            $('.todos').prop('checked', true);  
                        
                        },
                        error: function(data) {
                            alert('error');
                        }
                    });
                }
                
            
                $('#modalCRUD').modal('hide');
                document.getElementById('alerta').style.display = 'none'  	
    }										     			
    });
            
     
    
    
    $("#btnNuevo").click(function(){
        //para limpiar los select antes de dar de Alta una nueva tarifa
        
        $("#state").val("")
        $("#transport").empty();
        $("#zone").empty();
        $("#city").empty();
        $("#zone").attr("readonly", false);  
        $("#costprom").val('');
        $("#startdate").val('');
        $("#enddate").val('');
        $("#total").val('');
        $("#zone").prop('disabled', false);
        $("#city").prop('disabled', false);
        $("#state").prop('disabled', false);
        document.getElementById('alerta').style.display = 'none'    

        $("#transport").html("<option value='' selected disabled>-Select-</option>");
        $("#zone").html("<option value='' selected disabled>-Select-</option>");
        $("#city").html("<option value='' selected disabled>-Select-</option>");
        

        opcion = 2;  //consultar las zonas disponibles en la BD
        $.ajax({
            url: "bd/crudRates.php",
            type: "POST",
            datatype:"json",
            data:  {opcion:opcion},    
            success: function(data){

                opts = JSON.parse(data);
                for (var i = 0; i< opts.length; i++){                    
                    $('#zone').append($('<option>').val(opts[i]).text(opts[i]));            }
     
            },
            error: function(data) {
                alert('error');
            }
        });

        //Mostrar las lineas de transporte dependiendo la zona seleccionada
  
       
        $("#zone").change(function () {
            $("#transport").empty();
            //$('#transport option').remove();     
     
          
            var zone = $(this).val();
          
            $.ajax({
                url: "bd/crudRates.php",
                cache: false,
                type: "POST",
                datatype:"json",
                data:  {opcion:1,zone:zone},    
                success: function(data){
                  
                    $("#transport").empty();
                    opts = JSON.parse(data);
                    for (var i = 0; i< opts.length; i++){    
                        if(opts[i].Linea!=null)
                        $('#transport').append($('<option>').val(opts[i].Linea).text(opts[i].Linea));         
                    
                    }
         
                },
                error: function(data) {
                   
                    alert('error');
                }
            });


            


        });

        //OBTENER LAS CIUDADES DEPENDIENDO EL ESTADO SELECCIONADO
        //opcion = 3;
        $("#state").change(function () {
            $("#city").empty();
            //$('#transport option').remove();     
     
            
            var estado = $(this).val();
          
            $.ajax({
                url: "bd/crudRates.php",
                cache: false,
                type: "POST",
                datatype:"json",
                data:  {opcion:3,estado:estado},    
                success: function(data){                  
                    $("#city").empty();
                    opts = JSON.parse(data);
                    for (var i = 0; i< opts.length; i++){    

                        $('#city').append($('<option>').val(opts[i].City).text(opts[i].City));         
                    
                    }
         
                },
                error: function(data) {
                   
                    alert('error');
                }
            });





        });

        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("New Rate");
        $('#modalCRUD').modal('show');	  

        opcion = 5;
  
    });
    
    //Editar        
    $(document).on("click", ".btnEditar", function(){
    
       
        $("#transport").empty();
        $("#zone").empty();
        $("#city").empty();
        document.getElementById('alerta').style.display = 'none'    
       
             

        fila = $(this).closest("tr");	  
        IdRate = fila.find('td:eq(0)').text(); //capturo el ID      
        transport = fila.find('td:eq(1)').text(); //capturo el ID
        zone = fila.find('td:eq(2)').text(); 
        city = fila.find('td:eq(3)').text(); 
        costo_promedio = fila.find('td:eq(4)').text(); 	
        startdate = fila.find('td:eq(6)').text(); 	   
        enddate = fila.find('td:eq(7)').text(); 	
        moment.locale('es');   

        var fhoy = new Date();  
        ffin = moment(enddate,'DD/MM/YYYY');
        //ffin = ffin.format('DD/MM/YYYY');            
       /* fhoy = moment(fhoy);
        fhoy = fhoy.format('DD/MM/YYYY');
      
        */
        ffin =  new Date(ffin)
        console.log(fhoy);
        console.log(ffin);
        console.log(ffin > fhoy)
       // console.log();
        if( ffin> fhoy){
            console.log("ES MAYOR");
          
            
        }

      
        const  [day, month, year] = startdate.split('/');
        startdate = `${year}-${month}-${day}`;


        //enddate = parseDate(enddate)
     
        const  [day1, month1, year1] = enddate.split('/');
        enddate = `${year1}-${month1}-${day1}`;
        opcion = 1;
        $.ajax({
            url: "bd/crudRates.php",
            type: "POST",
            datatype:"json",
            data:  {opcion:opcion,IdRate:IdRate,zone:zone,city:city},    
            success: function(data){

                opts = JSON.parse(data);
                est = opts[0].Estado;
                for (var i = 0; i< opts.length; i++){
                    
                    $('#transport').append($('<option>').val(opts[i].Linea).text(opts[i].Linea));
                    
                }
               // $('#state').append($('<option>').val(opts[0].Estado).text(opts[0].Estado));
               // $('#state').append('<option class="form-control selected">' + opts[0].Estado + '</option>');  

               $("#state").val(est);

                
               
            },
            error: function(data) {
                alert('error');
            }
        });  


        $("#costprom").val(costo_promedio);
        $("#startdate").val(startdate);
        $("#enddate").val(enddate);

      
        $('#zone').append('<option class="form-control selected">' + zone + '</option>');  
        $('#transport').append('<option class="form-control selected">' + transport + '</option>');  
        $('#city').append('<option class="form-control selected">' + city + '</option>');  
      
        $("#zone").prop('disabled', true);
        $("#city").prop('disabled', true);
        $("#state").prop('disabled', true);

        tot = parseFloat(costo_promedio * 120);
        tot=tot.toFixed(2)
        tot=separator(tot);
        $("#total").val(tot);



        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $(".modal-title").text("Edit Rates");		
        $('#modalCRUD').modal('show');
        
        opcion = 7

     

    });


    $('#activo').on('change',function(){
        if (this.checked) {
            $('.inactivo').prop('checked', false);
            $('.todos').prop('checked', false);    
            
            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex){
                   return (data[8] == "Active") ? true : false;
                }
             );
        }         
        tablaRates.draw();
        
        if(this.checked){
           $.fn.dataTable.ext.search.pop();    
        }
    });



    $('#inactivo').on('change',function(){
        if (this.checked) {
            $('.activo').prop('checked', false);
            $('.todos').prop('checked', false);

            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex){
                   return (data[8] == "Inactive") ? true : false;
                }
             );
        }
        
        //APLICAR FILTRO y MOSTRARLOS
        tablaRates.draw();
        
        if(this.checked){
           $.fn.dataTable.ext.search.pop();    
        }
    });


    $('#todos').on('change',function(){
        if (this.checked) {
            $('.inactivo').prop('checked', false);
            $('.activo').prop('checked', false);
        }  
        //volver a mostrar tabla
        tablaRates.draw();
    });  

  
});

$(document).on("click", ".btneliminar", function(){
        opcion = 8;
        fila = $(this).closest("tr");	  
        IdRate = fila.find('td:eq(0)').text(); //capturo el ID


        $.ajax({
            url: "bd/crudRates.php",
            type: "POST",
            datatype:"json",
            data:  {opcion:opcion,IdRate:IdRate},    
            success: function(data){   
                
                alert(data);
                tablaRates.ajax.reload(null,false);
                $('.inactivo').prop('checked', false);
                $('.activo').prop('checked', false);
                $('.todos').prop('checked', true);  
            },
            error: function(data) {
                alert('error');
            }
        });  
        
        




    });


function  costo(){
 
    promedio=document.getElementById("costprom").value;
    promedio = parseFloat(promedio);
    tot = parseFloat(promedio * 120);
    tot=tot.toFixed(2)
    tot=separator(tot);
    $("#total").val(tot);

}





function separator(numb) {
    var str = numb.toString().split(".");
    str[0] = str[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return str.join(".");
}
    


    