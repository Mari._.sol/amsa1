$(document).ready(function() {

    $('#tablaLiquidation tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    });

    usuario = $('#dropdownMenuLink2').text().trim();
    let botones = "";
    $.ajax({
        url: "bd/crudLiq.php",
        type: "POST",
        datatype: "json",  
        data: {opcion: 6, usuario: usuario}, 
        success: function(data) {
            var resp = JSON.parse(data);
            console.log(resp);
            
            if (resp[0].mailLiq == 1) {
                document.getElementById('btnComp').style.display = 'block';
                botones = "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar' title='Edit'><i class='material-icons'>edit</i></button><button class='btn btn-light btn-sm btnPreMail' title='Enviar Correo'><i class='bi bi-envelope fs-5'></i></button><button class='btn btn-success btn-sm viewExcel' title='Download Liquidation'><i class='bi bi-filetype-xlsx fs-5'></i></button></div></div>"; 
            } else if (resp[0].mailLiq == 2) {
                document.getElementById('btnCSF').style.display = 'block';
                botones = "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar' title='Edit'><i class='material-icons'>edit</i></button><button class='btn btn-success btn-sm viewExcel' title='Download Liquidation'><i class='bi bi-filetype-xlsx fs-5'></i></button><button class='btn btn-warning btn-sm btnMailProv' title='Enviar Correo a Prov'><i class='bi bi-envelope fs-5'></i></button></div></div>"; 
            } else {
                botones = "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar' title='Edit'><i class='material-icons'>edit</i></button><button class='btn btn-success btn-sm viewExcel' title='Download Liquidation'><i class='bi bi-filetype-xlsx fs-5'></i></button></div></div>";
            }
            inicializarDataTable();
        }
    });
    
    function inicializarDataTable() {
        var tablaLiq = $('#tablaLiquidation').DataTable({
            //para usar los botones   
            responsive: "true",
            dom: 'Brtilp',       
            buttons:[ 
                {
                    extend:    'excelHtml5',
                    text:      '<i class="fas fa-file-excel"></i> ',
                    titleAttr: 'Export to Excel',
                    className: 'btn btn-success',
                    exportOptions: {
                        columns: ":not(.no-exportar)"
                    }
                },
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fas fa-file-pdf"></i> ',
                    titleAttr: 'Export to PDF',
                    className: 'btn btn-danger',
                    exportOptions: {
                        columns: ":not(.no-exportar)"
                    }
                }
            ],
            
            initComplete: function () {
                // Apply the search
                this.api().columns().every(function() {
                    var that = this;
                    $('input', $(this.footer())).on('keyup change clear', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value, true, false).draw();
                        }
                    });
                });
            }, 
            "order": [0, 'desc'],
            "scrollX": true,
            "scrollY": "50vh",
            "scrollCollapse": true,
            "lengthMenu": [100,200,300,500], 
            fixedColumns: {
                left: 0,
                right: 1
            },
            "ajax": {            
                "url": "bd/crudLiq.php", 
                "method": 'POST',
                "data": {opcion: 1}, // Cambié aquí para usar la variable opcion global
                "dataSrc": ""
            },
            "columns": [
                {"data": "IdLiq"},
                {"data": "Crop"},
                {"data": "Region"},
                {"data": "Proveedor"},
                {"data": "Contrato"},
                {"data": "LiqID"},
                {"data": "Pacas"},
                {"data": "Precio",
                    "render": function (data) {
                        return '$' + parseFloat(data).toFixed(2); 
                    }
                },
                {"data": "PesoProm"},
                {"data": "TipoPago"},
                {"data": "TipoLiq"},
                {"data": "TipoDesc"},
                {"data": "No"},
                {"data": "SubNo"},
                {"data": "FechaEnv"},
                {"data": "SinDescuento",
                    "render": function (data) {
                        return '$' + parseFloat(data).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
                    }
                },
                {"data": "ConDescuento",
                    "render": function (data) {
                        return '$' + parseFloat(data).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
                    }
                },
                {"data": "Prom",
                    "render": function (data) {
                        return '$' + parseFloat(data).toFixed(2); 
                    }
                },
                {"data": "Pago",
                    "render": function (data) {
                        return '$' + parseFloat(data).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
                    }
                },
                {"data": "PagoPorc",
                    "render": function (data) {
                        return (parseFloat(data) * 100) + '%';
                    }
                },
                {"data": "Grado"},
                {"data": "Micro"},
                {"data": "Fibra"},
                {"data": "Resis"},
                {"data": "Otro"},
                {"data": "Total"},
                {"data": "SM"},
                {"data": "MP"},
                {"data": "M"},
                {"data": "SLMP"},
                {"data": "SLM"},
                {"data": "LMP"},
                {"data": "LM"},
                {"data": "SGOP"},
                {"data": "SGO"},
                {"data": "GO"},
                {"data": "O"},
                {"data": "mic1"},
                {"data": "mic2"},
                {"data": "mic3"},
                {"data": "mic4"},
                {"data": "mic5"},
                {"data": "mic6"},
                {"data": "fib1"},
                {"data": "fib2"},
                {"data": "fib3"},
                {"data": "fib4"},
                {"data": "fib5"},
                {"data": "res1"},
                {"data": "res2"},
                {"data": "res3"},
                {"data": "res4"},
                {"data": "res5"},
                {"data": "res6"},
                {"data": "Grd"},
                {"data": "Mic"},
                {"data": "Len"},
                {"data": "Str"},
                {"data": "Comentarios"},
                {"data": "filePago",
                    "render": function(data) { 
                        return data == '' || data == null ? 'NO' : 'YES';
                    }
                },
                {"data": "fileFactura",
                    "render": function(data) { 
                        return data == '' || data == null ? 'NO' : 'YES';
                    }
                },
                {"data": "datProv"},
                {"data": "datInv"},
                {"defaultContent": botones}  // Aquí valor global de `botones`
            ]
        });
        $('.dataTables_length').addClass('bs-select');


        $('#btnComp').on('click', function(e) { 
            $('#Aviso').html("");
            $('#avisoLiq').html("");  
            $("#formLiq").trigger("reset");
            $(".modal-header").css( "background-color", "#17562c");
            $(".modal-header").css( "color", "white" );
            $(".modal-title").text("Save Liquidations Data");
            $('#modalCRUD').modal('show');
        });

        
        $('#formLiq').submit(function(e) {
            e.preventDefault(); 
            var compCsv = $('#csvLiq').val().length;
            var comprobar = $('#fileExcel').val().length;
            var formulario = $("#formLiq");
            var archivosUB = new FormData();
            
            for (var i=0; i<(formulario.find('input[type=file]').length); i++){
                archivosUB.append((formulario.find('input[type="file"]:eq('+i+')').attr("name")),((formulario.find('input[type="file"]:eq('+i+')'))[0].files[0]));
            }

            if (compCsv > 0 && comprobar > 0){
                var archivos = new FormData();
                archivos.append('fileExcel', $('#fileExcel')[0].files[0]);
                archivos.append('fileExtension', $('#fileExtension').val());

                $.ajax({
                    url: 'bd/CreateLiq.php',
                    type: 'POST',
                    datatype: "json",
                    data: archivos,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        
                        var jsonResponse = JSON.parse(response);
                        if (jsonResponse.exists) {
                            if (confirm('La liquidación ' + jsonResponse.liqID + ' ya existe. ¿Deseas actualizarla?')) {
                                actualizarRegistro(jsonResponse.liqID, jsonResponse.TipoLiq);
                                $.ajax({
                                    url: "bd/LiqBales.php",
                                    type: "POST",
                                    contentType: false,
                                    data:  archivosUB,
                                    processData: false,
                                    success: function(dat) {
                                        datos = JSON.parse(dat);
                                        data = datos['Status'];
                                        cont = datos['cont'];
                                        upd = datos['upd'];
                                        lista = datos['pend'];
                                        crop = datos['crop'];
                                        gin = datos['gin'];
                        
                                        //exportar lista de bales que no encontro
                                        /* if(cont>0 && upd > 0){
                                            $("#exportlist").css("display", "block");
                                            $("#exportlist").click(function(e){
                                                e.preventDefault();
                                                window.open("./bd/balespendientes.php?lista="+lista+"&crop="+crop+"&gin="+gin);
                                            });                           
                                        }  */
                                        if (data == 'Columns'){
                                            $('#avisoLiq').html('<div id="alert1" class="alert alert-danger" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Check number of columns. </div>');
                                            setTimeout(function() {
                                                $('#alert1').fadeOut(1500);
                                            },1000);
                                            return false;
                                        }else{
                                        $('#avisoLiq').html('<div id="alert1" class="alert alert-success" style="width: 24rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i>Bales not found: '+cont+'; Updated records: '+upd+'</div>');
                                        }
                                        alert('Liquidacion actualizada correctamente');
                                    }
                                });
                                $('#avisoLiq').html('<div class="spinner-border" role="status"><span class="visually-hidden">Loading...</span></div>');
                            } else {
                                //crearNuevoRegistro(jsonResponse.liqID);
                                $('#modalCRUD').modal('hide');
                                tablaLiq.ajax.reload(null, false); 
                            }
                        }else{
                            crearNuevoRegistro(jsonResponse.liqID);
                            $.ajax({
                                url: "bd/LiqBales.php",
                                type: "POST",
                                contentType: false,
                                data:  archivosUB,
                                processData: false,
                                success: function(dat) {
                                    datos = JSON.parse(dat);
                                    data = datos['Status'];
                                    cont = datos['cont'];
                                    upd = datos['upd'];
                                    lista = datos['pend'];
                                    crop = datos['crop'];
                                    gin = datos['gin'];
                    
                                    //exportar lista de bales que no encontro
                                    /* if(cont>0 && upd > 0){
                                        $("#exportlist").css("display", "block");
                                        $("#exportlist").click(function(e){
                                            e.preventDefault();
                                            window.open("./bd/balespendientes.php?lista="+lista+"&crop="+crop+"&gin="+gin);
                                        });                           
                                    }  */
                            
                                    if (data == 'Columns'){
                                        $('#avisoLiq').html('<div id="alert1" class="alert alert-danger" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Check number of columns. </div>');
                                        setTimeout(function() {
                                            $('#alert1').fadeOut(1500);
                                        },1000);
                                        return false;
                                    }else{
                                    $('#avisoLiq').html('<div id="alert1" class="alert alert-success" style="width: 24rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i>Bales not found: '+cont+'; Updated records: '+upd+'</div>');
                                    }
                                    alert('Liquidacion creada correctamente');
                                }
                            });
                            $('#avisoLiq').html('<div class="spinner-border" role="status"><span class="visually-hidden">Loading...</span></div>');
                        }
                    },
                    error: function(error) {
                        console.error("Error:", error);
                    }
                });
                
                
            }else if (comprobar > 0) {
                var archivos = new FormData();
                archivos.append('fileExcel', $('#fileExcel')[0].files[0]);
                archivos.append('fileExtension', $('#fileExtension').val());

                $.ajax({
                    url: 'bd/CreateLiq.php',
                    type: 'POST',
                    datatype: "json",
                    data: archivos,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        

                        var jsonResponse = JSON.parse(response);
                        if (jsonResponse.exists) {
                            if (confirm('La liquidación ' + jsonResponse.liqID + ' ya existe. ¿Deseas actualizarla?')) {
                                actualizarRegistro(jsonResponse.liqID, jsonResponse.TipoLiq);
                                alert('Liquidacion actualizada correctamente');
                            } else {
                                //crearNuevoRegistro(jsonResponse.liqID);
                                $('#modalCRUD').modal('hide');
                                tablaLiq.ajax.reload(null, false); 
                            }
                        }else{
                            crearNuevoRegistro(jsonResponse.liqID);
                            alert('Liquidacion creada correctamente');
                        }
                    },
                    error: function(error) {
                        console.error("Error:", error);
                    }
                });
            } else {
                alert('Por favor selecciona un archivo');
            }
        });

        function actualizarRegistro(LiqID) {
            var archivos = new FormData();
            archivos.append('fileExcel', $('#fileExcel')[0].files[0]);
            archivos.append('fileExtension', $('#fileExtension').val());
            archivos.append('LiqID', LiqID);
        
            $.ajax({
                url: 'bd/UpdateLiq.php',
                type: 'POST',
                datatype: "json",
                data: archivos,
                contentType: false,
                processData: false,
                success: function(response) {
                    var jsonResponse = JSON.parse(response);
                    if (jsonResponse.liqID){
                        subirArchivos(jsonResponse.liqID, jsonResponse.TipoLiq); 
                        tablaLiq.ajax.reload(null, false); 
                    }else{
                        alert('Error');
                    }
                    
                },
                error: function(error) {
                    console.error("Error al actualizar:", error);
                }
            });
        }

        function crearNuevoRegistro(LiqID) {
            var archivos = new FormData();
            archivos.append('fileExcel', $('#fileExcel')[0].files[0]);
            archivos.append('fileExtension', $('#fileExtension').val());
            archivos.append('LiqID', LiqID);
        
            $.ajax({
                url: 'bd/CreateLiq.php',
                type: 'POST',
                datatype: "json",
                data: archivos,
                contentType: false,
                processData: false,
                success: function(response) {
                    var jsonResponse = JSON.parse(response);
                    if (jsonResponse.liqID){
                        subirArchivos(jsonResponse.liqID, jsonResponse.TipoLiq);
                        //alert('Liquidacion creada correctamente');
                        /* $('#Aviso').html('<div id="alert1" class="alert alert-primary" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Liq Actualizada </div>');
                        setTimeout(function() {
                            $('#alert1').fadeOut(3000);
                        },3000); */
                        //$('#modalCRUD').modal('hide');
                        tablaLiq.ajax.reload(null, false); 
                    }else{
                        alert('Error');
                    }
                },
                error: function(error) {
                    console.error("Error al crear nuevo registro:", error);
                }
            });
        }

        function subirArchivos(LiqID, TipoLiq) {
            var ajaxSuccessCount = 0;
            //subir el excel
            var file_data = $('#fileExcel').prop('files')[0];
            var form_data = new FormData();             
            form_data.append('file', file_data);
            form_data.append('LiqID', LiqID);
            form_data.append('TipoLiq', TipoLiq);

            $.ajax({
                url: 'LoadLiquidacion.php', 
                datatype:"json",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(data){              
                    opts = JSON.parse(data);
                    $('#Aviso').html("");
                    console.log('Liquidación subida:', opts);
                    ajaxSuccessCount++;
                    if (ajaxSuccessCount === 3) {
                        setTimeout(function() {
                            tablaLiq.ajax.reload(null, false); 
                        }, 500); 
                    }
                }
            });

            // cargar la imagen
            var img_data = $('#fileImg').prop('files')[0];
            var img_extension = $('#fileExtensionImg').val();
            var form_data2 = new FormData();
            form_data2.append('file2', img_data); 
            form_data2.append('fileExtensionImg', img_extension);
            form_data2.append('LiqID', LiqID);
            form_data2.append('TipoLiq', TipoLiq);
            $.ajax({
                url: 'LoadImg.php',
                datatype:"json",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data2,                         
                type: 'post',
                success: function(data){              
                    opts = JSON.parse(data);
                    $('#Aviso').html("");
                    console.log('Imagen subida:', opts);
                    ajaxSuccessCount++;
                    if (ajaxSuccessCount === 3) {
                        setTimeout(function() {
                            tablaLiq.ajax.reload(null, false); 
                        }, 500); 
                    }
                }
            });

            // cargar la imagen
            var img_dataP = $('#ImgPrice').prop('files')[0];
            var img_extP = $('#ExtensionImgP').val();
            var form_data3 = new FormData();
            form_data3.append('file3', img_dataP); 
            form_data3.append('ExtensionImgP', img_extP);
            form_data3.append('LiqID', LiqID);
            form_data3.append('TipoLiq', TipoLiq);
            $.ajax({
                url: 'LoadImgPrice.php',
                datatype:"json",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data3,                         
                type: 'post',
                success: function(data){              
                    opts = JSON.parse(data);
                    $('#Aviso').html("");
                    console.log('Img subida Price:', opts);
                    ajaxSuccessCount++;
                    if (ajaxSuccessCount === 3) {
                        setTimeout(function() {
                            //alert('¡Archivos subidos con éxito!');
                            tablaLiq.ajax.reload(null, false); 
                            //$('#modalCRUD').modal('hide');
                        }, 500); 
                    }
                }
            });
        }

        //Editar  
        $(document).on("click", ".btnEditar", function(){
            fila = $(this).closest("tr");
            liq = $(this).closest('tr').find('td:eq(5)').text();
            $("#formEdit").trigger("reset");
            $(".modal-header").css( "background-color", "#17562c");
            $(".modal-header").css( "color", "white" );
            $(".modal-title").text("Edit Liquidations " + liq );
            $('#modalEdit').modal('show');
            
            
            idLiq = parseInt(fila.find('td:eq(0)').text()); //capturo el Id
            liq = $(this).closest('tr').find('td:eq(5)').text();
            cDes = $(this).closest('tr').find('td:eq(16)').text();
            desc = parseInt(cDes.replace(/[$,]/g, ''));
            pago = $(this).closest('tr').find('td:eq(18)').text();
            pago = parseFloat(pago.replace(/[$,]/g, ''));
            porc = parseInt(fila.find('td:eq(19)').text());
            comt =  $(this).closest('tr').find('td:eq(58)').text();
            filePago =  $(this).closest('tr').find('td:eq(59)').text();
            fileInv =  $(this).closest('tr').find('td:eq(60)').text();
            datProv =  $(this).closest('tr').find('td:eq(61)').text();
            //datInv=  $(this).closest('tr').find('td:eq(62)').text();
            
            if (fileInv === "NO") {
                $('#upInv').show();
                $('#updateInv').hide();
                document.getElementById('documentoInv').style.display = 'none';
                document.getElementById('documentoXml').style.display = 'none'; 

            } else if (fileInv === "YES") {
                $('#upInv').hide();
                $('#updateInv').show();
                document.getElementById('documentoInv').style.display = 'block';
                document.getElementById('documentoXml').style.display = 'block'; 
                $.ajax({
                    url: "bd/crudLiq.php",
                    type: "POST",
                    datatype:"json",
                    data:  {opcion:4,idLiq:idLiq},    
                    success: function(data){
                        opts = JSON.parse(data);
                        if (opts[0].fileFactura === null || opts[0].fileXML === null) {
                            $("#load_Inv").text("Load Invoice");                  
                        }
                        else{
                            $("#actualfile_inv").val("Fac-"+ liq +"."+ opts[0].fileFactura)
                            $("#actualXml").val("Fac-"+ liq +"."+ opts[0].fileXML)
                            $("#load_Inv").text("Update Invoice");                
                        }
                    },
                    error: function(data) {
                        alert('error');
                    }
                });
            }

            if (filePago === "NO") {
                $('#comPago').show();
                $('#upComPago').hide();
                document.getElementById('documentoPago').style.display = 'none';
            } else if (filePago === "YES") {
                $('#comPago').hide();
                $('#upComPago').show();
                document.getElementById('documentoPago').style.display = 'block';
                $.ajax({
                    url: "bd/crudLiq.php",
                    type: "POST",
                    datatype:"json",
                    data:  {opcion:5,idLiq:idLiq},    
                    success: function(data){
                        opts = JSON.parse(data);
                        if (opts[0].filePago === null){
                            $("#loadVoucher").text("Load Voucher");                  
                        }
                        else{
                            $("#actualfile_pay").val("CompPago-"+ liq +"."+ opts[0].filePago)
                            $("#loadVoucher").text("Update Voucher");                
                        }
                    },
                    error: function(data) {
                        alert('error');
                    }
                });
            }
            
            $('#porc').val(porc);
            $('#pay').val(pago);
            $('#coment').val(comt);
            $('#porc').on('input', function() {
                let newPorc = parseInt($(this).val());  
                if (!isNaN(newPorc)) { 
                    let newTotal = ((desc * newPorc) / 100); 
                    $('#pay').val(newTotal); 
                }
            });
            $('#dateProv').val(datProv);
            //$('#dateInv').val(datInv);

            $(document).on("click",".save",function(){
                opcion = 3;
                porc = $('#porc').val();
                pay = $('#pay').val();
                coment = $('#coment').val();
                datProv = $('#dateProv').val();
                //datInv = $('#dateInv').val();
                $('#avisoEdit').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span></div>');  
                
                $.ajax({
                    url: "bd/crudLiq.php",
                    type: "POST",
                    datatype:"json",    
                    data:  {opcion:opcion,idLiq:idLiq,porc:porc,pay:pay,coment:coment,datProv:datProv}, //,datInv:datInv
                    success: function(data) {
                        console.log("correcto")
                        alert('Liquidacion actualizada con exito.');
                        $('#modalEdit').modal('hide');
                        $('#avisoEdit').html(''); 
                        tablaLiq.ajax.reload(null, false); 
                    }
                });
            });

            $('#loadInv').on('click', function(e) {
                e.preventDefault(); 
                $('#avisoInv').html("");
                
                var $el = $('#fileInvoice');
                $el.wrap('<form>').closest('form').get(0).reset(); //Limpiar el apartado de cargar archivo
                $el.unwrap();
                $(".modal-header").css("background-color", "#17562c");
                $(".modal-header").css("color", "white" );
                $('#modal_Invoice').modal('show'); 
            });
        
            $('#UpdInv').on('click', function(e) {
                e.preventDefault(); 
                $('#avisoInv').html("");
                
                var $el = $('#fileInvoice');
                $el.wrap('<form>').closest('form').get(0).reset(); //Limpiar el apartado de cargar archivo
                $el.unwrap();

                var $elXml = $('#fileXml');
                $elXml.wrap('<form>').closest('form').get(0).reset();
                $elXml.unwrap();
                $(".modal-header").css("background-color", "#17562c");
                $(".modal-header").css("color", "white" );
                $('#modal_Invoice').modal('show'); 
                $("#load_Inv").text("Update Invoice");
            });

            $('#loadPago').on('click', function(e) {
                e.preventDefault(); 
                $('#avisoP').html("");
                
                var $el = $('#fileVoucher');
                $el.wrap('<form>').closest('form').get(0).reset(); //Limpiar el apartado de cargar archivo
                $el.unwrap();
                $(".modal-header").css("background-color", "#17562c");
                $(".modal-header").css("color", "white" );
                $('#modal_VoucherPay').modal('show'); 
            });
        
            $('#UpdPago').on('click', function(e) {
                e.preventDefault(); 
                $('#avisoP').html("");
                
                var $el = $('#fileVoucher');
                $el.wrap('<form>').closest('form').get(0).reset(); //Limpiar el apartado de cargar archivo
                $el.unwrap();
                $(".modal-header").css("background-color", "#17562c");
                $(".modal-header").css("color", "white" );
                $('#modal_VoucherPay').modal('show'); 
                $("#loadVoucher").text("Update Voucher");
            });
        
            //SUBIR O ACTUALIZAR FACTURA
            $('#load_Inv').off('click').on('click', function(e) {
                e.preventDefault(); 
                $('#avisoInv').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');  

                var filePdf = $('#fileInvoice').prop('files')[0]; 
                var ExtPdf = $('#ExtPdf').val();
                var fileXml = $('#fileXml').prop('files')[0]; 
                var ExtXml = $('#ExtXml').val();
                var form_data = new FormData();
                
                form_data.append('filePdf', filePdf);
                form_data.append('ExtPdf', ExtPdf);
                form_data.append('fileXml', fileXml);
                form_data.append('ExtXml', ExtXml);
                form_data.append('LiqID', liq);
                
                $.ajax({
                    url: 'loadInvoice.php',
                    dataType: 'text',  
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,                         
                    type: 'post',
                    success: function(data){              
                        opts = JSON.parse(data);
                        console.log(opts)
                        if (opts.pdf && opts.xml) {
                            alert("Factura guardada con exito");
                            document.getElementById('documentoInv').style.display = 'block';
                            document.getElementById('documentoXml').style.display = 'block';                     
                            $("#actualfile_inv").val(opts.pdf);
                            $("#actualXml").val(opts.xml);
                            $('#avisoInv').html("");
                            $('#modal_Invoice').modal('hide');
                            $('#load_Inv').prop('disabled', true);
                            $("#load_Inv").text("Update Invoice");
                            $('#upInv').hide();
                            $('#updateInv').show();
                        }
                    }
                });
            });

            //SUBIR O ACTUALIZAR COMPROBANTE DE PAGO
            $('#loadVoucher').on('click', function(e) {
                e.preventDefault(); 
                $('#avisoP').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');  
                var file_data = $('#fileVoucher').prop('files')[0]; 
                var form_data = new FormData();             
                form_data.append('file', file_data);
                form_data.append('LiqID', liq);
                
                $.ajax({
                    url: 'loadComPago.php',
                    dataType: 'text',  
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,                         
                    type: 'post',
                    success: function(data){              
                        opts = JSON.parse(data);
                        $('#modal_VoucherPay').modal('hide');
                        $('#avisoP').html("");
                        alert("Comprobante guardado con exito");
                        console.log(opts)
                        document.getElementById('documentoPago').style.display = 'block';
                        $("#actualfile_pay").val(opts);
                        $("#loadVoucher").text("Update Voucher");
                        $('#loadVoucher').prop('disabled', true);
                        $('#comPago').hide();
                        $('#upComPago').show();
                    }
                });
            });
        }); 


        // Pre mail interno
        $(document).on("click", ".btnPreMail", function(){
            $('#subject').val('');
            $('#comentarios').val('');
            $('#exInv').val('');
            $('#fileZip').val('');
            
            $("#formLiq").trigger("reset");
            $(".modal-header").css( "background-color", "#17562c");
            $(".modal-header").css( "color", "white" );
            $(".modal-title").text("Send Liquidations Data");
            $('#modalMail').modal('show');

            fila = $(this).closest("tr");
            ID = parseInt(fila.find('td:eq(0)').text()); //capturo el Id
            reg = $(this).closest('tr').find('td:eq(2)').text().toUpperCase();
            prov =  $(this).closest('tr').find('td:eq(3)').text();
            liquidacion =  $(this).closest('tr').find('td:eq(5)').text();
            qty =  $(this).closest('tr').find('td:eq(6)').text();
            price =  $(this).closest('tr').find('td:eq(7)').text();
            tPago =  $(this).closest('tr').find('td:eq(9)').text();
            TipoLiq =  $(this).closest('tr').find('td:eq(10)').text();
            desc =  $(this).closest('tr').find('td:eq(11)').text();
            porc =  $(this).closest('tr').find('td:eq(19)').text();
            $('#LiqID').val(liquidacion);
            
            $.ajax({
                url: "bd/crudLiq.php",
                type: "POST",
                datatype:"json",  
                data:  {opcion:2,reg:reg}, 
                success: function(data) {
                    var org = data.slice(2, -2);
                    $("#mails").val(org)
                }
            });

            var txt ='';
            porcNum = parseFloat(porc.replace('%', ''));
            if ( porcNum != 100){
                txt = " al " +porc;
            }else{
                txt ="";
            }

            var tLiq = '';
            var libP='';
            var txtHvi = '';
            if (TipoLiq == 'Def'){
                tLiq = 'definitiva'
                txtLiq = 'Solicitud de liquidación definitiva'
                txtHvi='El análisis de HVI para estas pacas ya fue completado.'
            }else if (TipoLiq == 'Ant' && porcNum == 0){
                tLiq = 'anticipo'
                txtLiq = 'Solicitud de factura'
                libP = ' (sin liberación de pago).'
            }else if (TipoLiq == 'Ant' && porcNum > 0){
                tLiq = 'anticipo'
                txtLiq = 'Solicitud de anticipo'
            }
            else if (TipoLiq == 'Prov'){
                tLiq = 'provisional'
                txtLiq = 'Solicitud de liquidación provisional'
                txtHvi= 'Pendientes resultados de HVI de nuestro laboratorio.'
            } else {
                tLiq = ''
            }

            var tipPag = '';
            if (tPago == 'DQtl'){
                tipPag = 'Dólares el quital'
            }else if (tPago == 'CLbN'){
                tipPag = 'Centavos de dólar la libra neta'
            }

            var dto = '';
            if (desc == 'LAG24-1' || desc == 'LAG24-2') {
                dto = 'Laguna 23/24'
            }else if (desc == 'LAG25-1' || desc == 'LAG25-2') {
                dto = 'Laguna 24/25'
            }else if (desc == 'MAT24-1' || desc == 'MAT24-2') {
                dto = 'Matamoros 23/24'
            }else if (desc == 'MAT25-1' || desc == 'MAT25-2') {
                dto = 'Matamoros 24/25'
            }else if (desc == 'TAM24-1' || desc == 'TAM24-2') {
                dto = 'Tamaulipas 23/24'
            }else if (desc == 'TAM25-1' || desc == 'TAM25-2') {
                dto = 'Tamaulipas 24/25'
            }else if (desc == 'CHH23-1' || desc == 'CHH23-2') {
                dto = 'Chihuahua 22/23'
            }else if (desc == 'CHH24-1' || desc == 'CHH24-2') {
                dto = 'Chihuahua 23/24'
            }else if (desc == 'CHH25-1' || desc == 'CHH25-2') {
                dto = 'Chihuahua 24/25'
            }else if (desc == 'USA24-1' || desc == 'USA24-2') {
                dto = 'USA 23/24'
            }else {
                dto = desc
            }

            
            subject = txtLiq +  ' '+ prov + ': ' + liquidacion + ' - '+ qty + ' BC' + libP; 
            $('#subject').val(subject);

            var hora = new Date().getHours();
            var sald;
            if (hora < 12) {
                sald = 'Buenos días';
            } else if (hora < 18) {
                sald = 'Buenas tardes';
            } else {
                sald = 'Buenas noches';
            }

            var body='';
            document.getElementById('FileExtra').style.display = 'none';
            if (reg === 'USA'){
                document.getElementById('FileExtra').style.display = 'block';
                var body = sald + '.\n\nEstas pacas están listas para su liquidación ' + tLiq + 
                '. Se paga en ' + tipPag +'. Adjunto facturas y un archivo con el resumen de las mismas.';
            }
            else if (TipoLiq == 'Ant' && porcNum == 0){
                var body = sald + '.\n\nSe solicita facturación a cuenta de estas pacas, sin liberación de pago' + 
                '. Se calcula en ' + tipPag +', con descuentos actualizados a ' + dto + ' y precio provisional de ' 
                + price + ' (considerando base).';
            }
            else if (TipoLiq == 'Ant' && porcNum > 0){
                var body = sald + '.\n\nSe solicita pago de anticipo a cuenta de estas pacas' + 
                '. Se paga en ' + tipPag + txt +', con descuentos actualizados a ' + dto
            }
            else{
                var body = sald + '.\n\nEstas pacas están listas para su liquidación ' + tLiq + 
                '. Se paga en ' + tipPag + txt +', con descuentos actualizados a ' + dto + 
                '. \n\n' + txtHvi;
            }
            
            $('#datosLiq').val(body);
        });  

        // enviar correo interno
        $(document).on("click",".sendEmail",function(){
            LiqID = $('#LiqID').val();
            mails = $('#mails').val();
            subject = $('#subject').val();
            body = $('#datosLiq').val().trim();
            bodyHtml = body.replace(/\n/g, '<br>');
            comentarios = $('#comentarios').val();
            fileInv = $('#exInv')[0].files[0];
            fileZip = $('#fileZip')[0].files[0];

            let formData = new FormData();  
            formData.append("usuario", usuario);
            formData.append("LiqID", LiqID);
            formData.append("TipoLiq", TipoLiq);
            formData.append("mails", mails);
            formData.append("subject", subject);
            formData.append("body", bodyHtml);
            formData.append("comentarios", comentarios);
            
            if (fileInv) {
                formData.append("fileInv", fileInv);  
            }

            if (fileZip) {
                formData.append("fileZip", fileZip);  
            }

            $('#avisomail').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span></div>');  
            $.ajax({
                url: "bd/mailLiquidation.php",
                type: "POST",
                datatype:"json",  
                data: formData,
                contentType: false,
                processData: false,
                success: function(response){
                    try {
                        var jsonResponse = JSON.parse(response);
            
                        if (jsonResponse.exists) {
                            alert(jsonResponse.message || '¡Correo enviado correctamente!');
                        } else {
                            alert(jsonResponse.error || 'Error desconocido al procesar la solicitud.');
                        }

                        $('.btnMailProv').hide(); 
                        $('#modalMail').modal('hide');
                        $('#avisomail').html('');  
                        tablaLiq.ajax.reload(null, false);
                    } catch (e) {
                        console.error('Error al procesar la respuesta JSON:', e);
                        alert('Ocurrió un problema al procesar la respuesta del servidor.');
                        $('.btnMailProv').hide(); 
                        $('#modalMail').modal('hide');
                        $('#avisomail').html('');  
                    }
                },
                error: function (response){
                    alert('¡Error al enviar el correo!');
                    $('#modalMail').modal('hide');
                    $('#avisomail').html('');  
                }
            })
        });  

        // enviar correo a prov
        $(document).on("click", ".btnMailProv", function(){
            fila = $(this).closest("tr");
            ID = parseInt(fila.find('td:eq(0)').text()); //capturo el Id
            reg = $(this).closest('tr').find('td:eq(2)').text().toUpperCase();
            liquidacion =  $(this).closest('tr').find('td:eq(5)').text(); 
            bc = parseInt(fila.find('td:eq(6)').text());
            TipoLiqP =  $(this).closest('tr').find('td:eq(10)').text();
            dateProv =  $(this).closest('tr').find('td:eq(61)').text();
            
            /* if (dateProv != ''){
                alert("¡Esta Factura ya fue enviada!")
                console.log("ya se envio esta factura  :)")
            } */
            if (dateProv != '') {
                Swal.fire({
                    icon: 'warning',
                    text: '¡Esta Factura ya fue enviada! ¿Deseas continuar?',
                    showCancelButton: true, //botón de cancelar
                    confirmButtonText: 'Continuar',
                    cancelButtonText: 'Cerrar',
                }).then((result) => {
                    if (result.dismiss === Swal.DismissReason.cancel) {
                        $('#modalMailP').modal('hide'); 
                    }
                });
            }

            $.ajax({
                url: "bd/crudLiq.php",
                type: "POST",
                datatype: "json",
                data: { opcion: 7 },
                success: function(data) {
                    opts = JSON.parse(data);
                    dataDate = new Date(opts[0].DatCSF); 

                    hoy = new Date();
                    diffTime = hoy - dataDate;
                    diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24));

                    if (diffDays > 90) {
                        alert("¡Actualizar Constancia de Situacion Fiscal!");
                    }            
                }
            }); 
            
            $('#addCSF').prop('checked', false);
            $('#sendEmailP').prop('disabled', true);
            $("#formEmailP").trigger("reset");
            $(".modal-header").css( "background-color", "#17562c");
            $(".modal-header").css( "color", "white" );
            $(".modal-title").text("Send Mail Prov");
            $('#modalMailP').modal('show');

            $.ajax({
                url: "bd/crudLiq.php",
                type: "POST",
                datatype:"json",  
                data:  {opcion:8,liquidacion:liquidacion}, 
                success: function(data) {
                    console.log(data.length)
                    if (data.length <=7) {
                        $("#mailsP").val(""); 
                    } else {
                        var org = data.slice(2, -2); 
                        $("#mailsP").val(org);
                    }
                }
            });

            $('#LiqIDP').val(liquidacion);
            $('#TipoLiqP').val(TipoLiqP);

            subject = 'Solicitud factura liquidación ' + liquidacion + ' - ' +bc +' bc'; 
            $('#subjectP').val(subject);

            document.getElementById("payMetod").addEventListener("change", function() {
            
                if (this.value === "1") {
                    m = "PPD"; 
                    f = "99"
                }else if (this.value === "2") {
                    m = "PUE"
                    f = "TRANSFERENCIA"
                }else if (this.value === "3") {
                    m = "PUE"
                    f = "COMPENSACION"
                }

                var hora = new Date().getHours();
                var sald;
                if (hora < 12) {
                    sald = 'Buenos días';
                } else if (hora < 18) {
                    sald = 'Buenas tardes';
                } else {
                    sald = 'Buenas noches';
                }
                
                
                var body = sald + '.\n\nFavor enviar la factura solicitada con los siguientes datos:\n'+
                '\nUSO CFDI: G01'+
                '\nMÉTODO DE PAGO: '+ m +
                '\nFORMA DE PAGO: ' + f +
                '\nCLAVE SERVICIO: 11151507 FIBRAS DE ALGODÓN'+
                '\nMONEDA:  USD DÓLAR AMERICANO'+
                '\nTIPO DE CAMBIO:  CONSIDERAR EL DEL DIARIO OFICIAL DEL DIA DE LA FACTURACION.' +
                '\nOBJETO DE IMPUESTO: 02' +
                '\nTIPO FACTOR: TASA';
                $('#datosLiqP').val(body);
                $('#sendEmailP').prop('disabled', false);
            });
        });  

        $(document).on("click",".sendEmailP",function(){
            $('#avisomailP').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span></div>');  
            ConSF = $('#addCSF').is(':checked') ? '1' : '0';
            LiqIDP = $('#LiqIDP').val();
            TipoLiqP = $('#TipoLiqP').val();
            mailsP = $('#mailsP').val();
            subjectP = $('#subjectP').val();
            payMetod = $('#payMetod').val();
            bodyP = $('#datosLiqP').val();
            bodyHtmlP = bodyP.replace(/\n/g, '<br>');
            
            console.log(usuario)

            let formData = new FormData();  
            formData.append("usuarioP", usuario);
            formData.append("LiqIDP", LiqIDP);
            formData.append("TipoLiqP", TipoLiqP);
            formData.append("payMetod", payMetod);
            formData.append("mailsP", mailsP);
            formData.append("subjectP", subjectP);
            formData.append("bodyP", bodyHtmlP);
            formData.append("ConSF", ConSF);
            
            $.ajax({
                url: "bd/mailProv.php",
                type: "POST",
                datatype:"json",  
                data: formData,
                contentType: false,
                processData: false,
                success: function(response){
                    try {
                        var jsonResponse = JSON.parse(response);
            
                        if (jsonResponse.exists) {
                            alert(jsonResponse.message || '¡Correo enviado correctamente!');
                            $('#avisomailP').html(''); 
                        } else {
                            alert(jsonResponse.error || 'Error desconocido al procesar la solicitud.');
                            $('#avisomailP').html(''); 
                        }
            
                        $('#modalMailP').modal('hide');
                        $('#avisomail').html('');  
                        tablaLiq.ajax.reload(null, false);
                    } catch (e) {
                        console.error('Error al procesar la respuesta JSON:', e);
                        alert('Ocurrió un problema al procesar la respuesta del servidor.');
                        $('#modalMailP').modal('hide');
                        $('#avisomailP').html(''); 
                        $('#avisomail').html('');  
                    }
                },
                error: function (response){
                    alert('¡Error al enviar el correo!');
                    $('#modalMail').modal('hide');
                    $('#avisomailP').html(''); 
                    $('#avisomail').html('');  
                }
            })
        });

        $('#btnCSF').on('click', function(e) { 
            $('#AvisoR').html("");
            $("#formCSF").trigger("reset");
            $(".modal-header").css( "background-color", "#17562c");
            $(".modal-header").css( "color", "white" );
            $(".modal-title").text("Constacia de Situacion Fiscal");
            $('#modalCSF').modal('show');
        });       
        
        //SUBIR O ACTUALIZAR FACTURA
        $('#saveC').off('click').on('click', function(e) {
            e.preventDefault(); 
            $('#AvisoR').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');  

            var CSF = $('#fileCsf').prop('files')[0]; 
            var ExtCSF = $('#extC').val();

            var form_data = new FormData();
            form_data.append('CSF', CSF);
            form_data.append('ExtCSF', ExtCSF);
            
            $.ajax({
                url: 'LoadCompSF.php',
                dataType: 'text',  
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(data){              
                    opts = JSON.parse(data);
                    if (opts.conF) {
                        alert("Constancia guardada con exito");
                        $('#AvisoR').html("");
                        $('#saveC').prop('disabled', true);
                        $('#modalCSF').modal('hide');
                    }
                }
            });
        });

        // cerrar modal de cargar factura
        $('#close_Inv, #closeX').on('click', function(e) {
            e.preventDefault();
            var $el = $('#fileInvoice');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap(); 

            var $elXml = $('#fileXml');
            $elXml.wrap('<form>').closest('form').get(0).reset();
            $elXml.unwrap();

            $('#modal_Invoice').modal('hide');	
            $('#load_Inv').prop('disabled', true);
        });


        // cerrar modal de cargar comprobante de pago
        $('#closeVocher').on('click', function(e) {
            e.preventDefault();
            var $el = $('#fileVoucher');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap(); 
            $('#modal_VoucherPay').modal('hide');	
        });

        $('#closeXC').on('click', function(e) {
            e.preventDefault();
            var $el = $('#fileVoucher');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap(); 
            $('#modal_VoucherPay').modal('hide');
        });
    }
});



function extencion_file() {
    var filename = $("#fileExcel").val();
    var extension = filename.split('.').pop().toLowerCase(); 

    // Verifica si la extensión es válida
    if (filename === "") {
        alert('No ha seleccionado un archivo');  
    } else if (extension === 'xlsx' || extension === 'xls') {
        $("#fileExtension").val(extension); 
    } else {
        alert("Extensión no válida");
    }
}

function extencion_Img() {
    var filename2 = $("#fileImg").val();
    var extension2 = filename2.split('.').pop().toLowerCase();  

    // Verifica si la extensión es válida
    if (filename2 === "") {
        alert('No ha seleccionado un archivo');  
    } else if (extension2 == 'jpg' || extension2 == 'png') { 
        $("#fileExtensionImg").val(extension2); 
    } else {
        alert("Extensión no válida");
    }
}

function Img_Price() {
    var filename3 = $("#ImgPrice").val();
    var extension3 = filename3.split('.').pop().toLowerCase();  

    // Verifica si la extensión es válida
    if (filename3 === "") {
        alert('No ha seleccionado un archivo');  
    } else if (extension3 == 'jpg' || extension3 == 'png') { 
        console.log("Extensión del archivo: " + extension3);
        $("#ExtensionImgP").val(extension3); 
    } else {
        alert("Extensión no válida");
    }
}

function csvFile() {
    var filename3 = $("#csvLiq").val();
    var extension3 = filename3.split('.').pop().toLowerCase();  

    // Verifica si la extensión es válida
    if (filename3 === "") {
        alert('No ha seleccionado un archivo');  
    } else if (extension3 == 'CSV' || extension3 == 'csv') { 
        console.log("Extensión del archivo: " + extension3);
        $("#csvExt").val(extension3); 
    } else {
        alert("Extensión no válida");
    }
}

function LoadFileInv() {
    var filenamePdf = $("#fileInvoice").val();
    var extensionPdf = filenamePdf.split('.').pop().toLowerCase();  
    if (filenamePdf === "") {
        $('#load_Inv').prop('disabled', true);
    } else if (extensionPdf === 'pdf') { 
        $("#ExtPdf").val(extensionPdf);
    } else {
        alert("Extensión no válida para el archivo PDF");
        $('#load_Inv').prop('disabled', true);
    }

    var filenameXml = $("#fileXml").val();
    var extensionXml = filenameXml.split('.').pop().toLowerCase();
    if (filenameXml === "") {
        $('#load_Inv').prop('disabled', true);
    } else if (extensionXml === 'xml') {
        $("#ExtXml").val(extensionXml); 
    } else {
        alert("Extensión no válida para el archivo XML");
        $('#load_Inv').prop('disabled', true);
    }

    if (filenameXml === "" || filenamePdf === ""){
        $('#load_Inv').prop('disabled', true);
    }else{
        $('#load_Inv').prop('disabled', false);
    }
}

function LoadFileVoucher(){
    var filename = $("#fileVoucher").val();
    if(filename === null)
        alert('No ha seleccionado un archivo');  
    else{// si se eligio un archivo correcto obtiene la extension para validarla
        var extension = filename.replace(/^.*\./, '');  
        if (extension == filename)
            extension = '';
        else{
            extension = extension.toLowerCase();
            if(extension == 'pdf' ||extension == 'PDF' || extension == 'JPG' || extension == 'PNG'|| extension == 'jpg' || extension == 'png' ){
                $('#loadVoucher').prop('disabled', false);
            }
            else{
                alert("extencion no valida");
                $('#loadVoucher').prop('disabled', true);
            }
        }
    }
}

function ext_CSF() {
    var filename = $("#fileCsf").val();
    var extension = filename.split('.').pop().toLowerCase(); 

    // Verifica si la extensión es válida
    if (filename === "") {
        alert('No ha seleccionado un archivo');  
    } else if (extension === 'PDF' || extension === 'pdf') {
        $("#extC").val(extension); 
        $('#saveC').prop('disabled', false);
    } else {
        alert("Extensión no válida");
    }
}

$(document).on("click", ".viewExcel", function(e){
    e.preventDefault(); 
    fila = $(this).closest("tr");
    liqId = parseInt(fila.find('td:eq(0)').text()); //capturo el ID
    window.open("downloadLiq.php?liqId="+liqId);
});

$(document).on("click", ".viewfile_inv", function(e){
    e.preventDefault(); 
    liqId = parseInt(fila.find('td:eq(0)').text()); //capturo el ID
    window.open("downloadInv.php?liqId="+liqId);
});

$(document).on("click", ".viewfile_Xml", function(e){
    e.preventDefault(); 
    liqId = parseInt(fila.find('td:eq(0)').text()); //capturo el ID
    window.open("downloadXml.php?liqId="+liqId);
});

$(document).on("click", ".viewfile_pay", function(e){
    e.preventDefault(); 
    liqId = parseInt(fila.find('td:eq(0)').text()); //capturo el ID
    window.open("downloadPago.php?liqId="+liqId);
});
