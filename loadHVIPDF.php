
<?php

require __DIR__.'/vendor/autoload.php';

use Aws\S3\S3Client; 
use Aws\Exception\AwsException; 
include_once 'fpdf/fpdf.php';
include_once 'bd/conexion.php';
setlocale(LC_TIME, "spanish");
function lowercase($element)
{
    return "'".$element."'";
}




$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set("America/Mexico_City");


$cliid = (isset($_POST['planta'])) ? $_POST['planta'] : '';
$Lots = (isset($_POST['lotes'])) ? $_POST['lotes'] : '';
$fechaload= date('Y-m-d h:i:s a', time());  

$consulta = "SELECT UserClient FROM amsadb1.Users_Clients WHERE FIND_IN_SET('$cliid', CliID) > 0 Limit 1;";
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$usercliente=$resultado->fetch();
$cliente=$usercliente['UserClient'];

//$bucket = 'pruebasportal'; //bucket de pruebas

$bucket='files-hvi';
// Preparar la consulta preparada
$consulta_preparada = $conexion->prepare("INSERT INTO FilesHVI (FileName, TypeFile, DateLoadFile) VALUES (:nombrearchivo, :tipo, :fecha)");



//definir parametros del bucket 

$s3 = new S3Client([
    'version'     => 'latest',
    //'region'      => 'us-east-2', //bucket de pruebas    
    'region'      => 'us-east-1',
    'credentials' => [
        'key'    => 'AKIAT442VUCJ4A2UNRMK',
        'secret' => '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',
    ],
]);






$carpeta_destino = 'bd/testfiles/';
$array = explode(",",$Lots);

class PDF extends FPDF
    {
    //Cabecera de página
    function Header()
    {
        $this->SetFont('Arial','B',10);
        $this->Image('img/logo1.png', 11, 10, 10, 10, 'PNG');
        $this->Cell(0,6, utf8_decode('AGROINDUSTRIAS UNIDAS DE MEXICO S.A. DE C.V.'),0,0,'C');
        $this->Ln(4);
        $this->SetFont('Arial','B',9);
        $this->Cell(0,7,'COTTON DIVISION', 0, 0,'C');
        $this->Ln();
        $this->SetFont('Arial','B', 9);
        $this->Cell(26,5, utf8_decode('Bales'),'TB',0,'C');
        $this->Cell(15,5, utf8_decode('Grd'),'TB',0,'C');
        $this->Cell(15,5, utf8_decode('Trs'),'TB',0,'C');
        $this->Cell(19,5, utf8_decode('Mic'),'TB',0,'C');
        $this->Cell(15,5, utf8_decode('Len'),'TB',0,'C');
        $this->Cell(17,5, utf8_decode('Str'),'TB',0,'C');
        $this->Cell(19,5, utf8_decode('Unf'),'TB',0,'C');
        $this->Cell(13,5, utf8_decode('Rd'),'TB',0,'C');
        $this->Cell(13,5, utf8_decode('b'),'TB',0,'C');
        $this->Cell(15,5, utf8_decode('Col'),'TB',0,'C');
        $this->Cell(15,5, utf8_decode('Tar'),'TB',0,'C');
        $this->Cell(15,5, utf8_decode('Tcnt'),'TB',0,'C');
        $this->Cell(13,5, utf8_decode('Sfi'),'TB',0,'C');
        $this->Cell(19,5, utf8_decode('Elg'),'TB',0,'C');
        $this->Cell(17,5, utf8_decode('Mat'),'TB',0,'C');
        $this->Cell(15,5, utf8_decode('Mst'),'TB',0,'C');
        $this->Ln();
    }

    //Pie de página
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
    }

$contador=0;
foreach ($array as $dato){ //for ($i= 0; $i < array_count_values($array); $i++){ //

    $pdf = new PDF();

    $cols = ['Bal', 'Col1_Mod as Col1', 'Tgrd_Mod as Tgrd', 'Mic_Mod as Mic', 'Len_Mod as Len', 'Str_Mod as Str', 'Unf_Mod as Unf', 'Rd_Mod as Rd', 'b_Mod as b', 'Tar_Mod as Tar', 'Col2_Mod as Col2', 'Tcnt_Mod as Tcnt', 'Sfi_Mod as Sfi', 'Elg_Mod as Elg', 'Mat_Mod as Mat', 'Mst_Mod as Mst'];
    $placeholders = implode(",", array_fill(0, count($cols), "?"));
    $consulta = "SELECT " . implode(",", $cols) . " FROM Bales WHERE Lot = ? ORDER BY Bal ASC";

    $consultaMedia = "SELECT count(Bal) as SumBal, avg(Mic_Mod) as avgMic, avg(Tgrd_Mod) as avgTgrd, avg(Tar_Mod) as avgTar, avg(Tcnt_Mod) as avgTcnt, avg(Unf_Mod) as avgUnf, avg(Len_Mod) as avgLen, avg(Sfi_Mod) as avgSfi, avg(Str_Mod) as avgStr, avg(Elg_Mod) as avgElg, avg(Mat_Mod) as avgMat, avg(Rd_Mod) as avgRd, avg(b_Mod) as avgb FROM Bales WHERE Lot = ?";
    $stmt = $conexion->prepare($consulta);
    $stmtMedia = $conexion->prepare($consultaMedia);


        
        $pdf->AliasNbPages();
        $pdf->AddPage('Landscape','Letter');
        $pdf->SetTitle($cliente.'-'.$dato);
        
        $stmt->execute([$dato]);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
        $stmtMedia->execute([$dato]);
        $dataAvg = $stmtMedia->fetchAll(PDO::FETCH_ASSOC);
        $pdf->Cell(26,4, utf8_decode($dato),'TB',0,'C');
        $pdf->Cell(0,4, utf8_decode(''),'TB',0,'C');

        foreach ($data as $row) { //foreach($data as $row){
            $pdf->Ln();
            $pdf->SetFont('Arial','', 9);
            $pdf->Cell(26,3,($row['Bal']),0,0,'C');
            $pdf->Cell(15,3, ($row['Col1']),0,0,'C');
            $pdf->Cell(15,3, ($row['Tgrd']),0,0,'C');
            $pdf->Cell(19,3, (number_format(Round($row['Mic'],1),1)),0,0,'C');
            $pdf->Cell(15,3, (number_format(Round($row['Len'],2),2)),0,0,'C');
            $pdf->Cell(17,3, (number_format(Round($row['Str'],1),1)),0,0,'C');
            $pdf->Cell(19,3, (number_format(Round($row['Unf'],1),1)),0,0,'C');
            $pdf->Cell(13,3, (number_format(Round($row['Rd'],1),1)),0,0,'C');
            $pdf->Cell(13,3, (number_format(Round($row['b'],1),1)),0,0,'C');
            $pdf->Cell(15,3, ($row['Col1']."-".$row['Col2']),0,0,'C');
            $pdf->Cell(15,3, (number_format(Round($row['Tar'],2),2)),0,0,'C');
            $pdf->Cell(15,3, ($row['Tcnt']),0,0,'C');
            $pdf->Cell(13,3, (number_format(Round($row['Sfi'],1),1)),0,0,'C');
            $pdf->Cell(19,3, (number_format(Round($row['Elg'],1),1)),0,0,'C');
            $pdf->Cell(17,3, (number_format(Round($row['Mat'],2),2)),0,0,'C');
            $pdf->Cell(15,3, (number_format(Round($row['Mst'],1),1)),0,0,'C');
        }
        foreach ($dataAvg as $rowAvg) {
            $pdf->Ln();
            $pdf->SetFont('Arial','B', 9);
            $pdf->Cell(26,4, utf8_decode($rowAvg['SumBal']." Bales"),'T',0,'C');
            $pdf->Cell(15,4, utf8_decode(''),'T',0,'C');
            $pdf->Cell(15,4, utf8_decode(Round($rowAvg['avgTgrd'],0)),'T',0,'C');
            $pdf->Cell(19,4, utf8_decode(number_format(Round($rowAvg['avgMic'],2),2)),'T',0,'C');
            $pdf->Cell(15,4, utf8_decode(number_format(Round($rowAvg['avgLen'],2),2)),'T',0,'C');
            $pdf->Cell(17,4, utf8_decode(number_format(Round($rowAvg['avgStr'],2),2)),'T',0,'C');
            $pdf->Cell(19,4, utf8_decode(number_format(Round($rowAvg['avgUnf'],2),2)),'T',0,'C');
            $pdf->Cell(13,4, utf8_decode(number_format(Round($rowAvg['avgRd'],2),2)),'T',0,'C');
            $pdf->Cell(13,4, utf8_decode(number_format(Round($rowAvg['avgb'],2),2)),'T',0,'C');
            $pdf->Cell(15,4, utf8_decode(''),'T',0,'C');
            $pdf->Cell(15,4, utf8_decode(number_format(Round($rowAvg['avgTar'],2),2)),'T',0,'C');
            $pdf->Cell(15,4, utf8_decode(number_format(Round($rowAvg['avgTcnt'],2),2)),'T',0,'C');
            $pdf->Cell(13,4, utf8_decode(number_format(Round($rowAvg['avgSfi'],2),2)),'T',0,'C');
            $pdf->Cell(19,4, utf8_decode(number_format(Round($rowAvg['avgElg'],2),2)),'T',0,'C');
            $pdf->Cell(17,4, utf8_decode(number_format(Round($rowAvg['avgMat'],2),2)),'T',0,'C');
            $pdf->Cell(15,4, utf8_decode(''),'T',0,'C');
        }


$pdf_content = $pdf->Output('S');



// Especificar el nombre del archivo PDF
$nombre_archivo = $cliente.'-'.$dato.'.pdf';

// Vincular parámetros de consulta preparada

$consulta_preparada->bindValue(':nombrearchivo', $nombre_archivo);
$consulta_preparada->bindValue(':tipo', 'PDF');
$consulta_preparada->bindValue(':fecha', $fechaload);


// Ejecutar la consulta preparada
  $consulta_preparada->execute();
  

// Ruta completa del archivo PDF
$ruta_archivo = $carpeta_destino . $nombre_archivo;

file_put_contents($ruta_archivo, $pdf_content);

try {
    // Upload data.
    $result = $s3->putObject([
        'Bucket' => $bucket,
        'Key'    => $nombre_archivo,
        'SourceFile' => $ruta_archivo 

    ]);
} catch (S3Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}

unlink($ruta_archivo);


    

// Escribir el contenido del PDF en un archivo en la carpeta deseada

$contador ++;

}

print json_encode($contador, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX
$conexion=null;



