<?php
session_start();
header("Cache-Control: no-cache, must-revalidate"); //para borrar cache
if (!isset($_SESSION['username'])):
    include_once('index.php');
else:
$permiso = $_SESSION['Admin'];
$rates=$_SESSION['ViewRates'];
$ViewLiq=$_SESSION['ViewLiq'];
?>

<!doctype html>
<html lang="es en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="img/ecom.png" />
    <title>Liquidations</title>

    <!-- CSS bootstrap -->
    <link rel="stylesheet" href="../portalLogistica/assets/bootstrap/css/bootstrap.min.css">
    <!--datables CSS básico-->
    <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
    <!--datables estilo bootstrap 4 CSS-->
    <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

    <!-- CSS personalizado -->
    <link rel="stylesheet" href="../portalLogistica/main.css">
    <!--Google fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


    <!--font awesome con CDN  -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
</head>

<body>
    <!-- Scripts -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>

    <!-- SweetAlert2 CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <!-- jQuery, Popper.js, Bootstrap JS -->
    <script src="assets/jquery/jquery-3.3.1.min.js"></script>
    <script src="assets/popper/popper.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>

    <!-- librerias necesarias para finalizar sesion por inactividad -->
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <!-- Scrip para finalizar sesion por inactividad -->
    <script type="text/javascript" src="timer.js"></script>

    <!-- datatables JS -->
    <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>

    <!-- para usar botones en datatables JS -->
    <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
    <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
    <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
    <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="mainLiq.js"></script>

    <!-- Ficed columns -->
    <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>

    <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
    <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
        <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
            <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                    </svg>
                </a>

                <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                    <li><a class="dropdown-item" href="trucks.php">Trucks</a></li>
                    <li><a class="dropdown-item" href="lots.php">Lots</a></li>
                    <?php if ($_SESSION['username'] == 'ISELA LARA' || $_SESSION['username'] == 'DARIO ROMERO' || $_SESSION['username'] == 'CESAR ARMAS' || $_SESSION['username'] == 'LUIS ANGEL RENDON TARIN' || $_SESSION['username'] == 'JOSE RENDON'){?>
                    <li><a class="dropdown-item" href="bales.php">Bales</a></li>
                    <?php } ?>
                    <li><a class="dropdown-item" href="do.php">Delivery Order</a></li>
                    <li><a class="dropdown-item" href="adjustments.php">Adjustments</a></li>
                    <li><a class="dropdown-item" href="inv.php">Invoice</a></li>
                    <li><a class="dropdown-item" href="export.php">Containers</a></li>
                    <li><a class="dropdown-item" href="clients.php">Clients</a></li>
                    <li><a class="dropdown-item" href="routes.php">Routes</a></li>
                    <li><a class="dropdown-item" href="requisition.php">Requisition</a></li>
                    <li><a class="dropdown-item" href="CMS.php">CMS</a></li>
                    <?php if ($permiso == 1){?>
                        <li><a class="dropdown-item" href="adminusers.php">User Management</a></li>
                        <li><a class="dropdown-item" href="adminproveed.php">Proveed Management</a></li>
                    <?php } ?>
                    <?php if ($rates == 1){?>
                        <li><a class="dropdown-item" href="Rates.php">Rates</a></li>
                    <?php } ?>
                    <?php if ($ViewLiq == 1){?>
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="#">Liquidations</a></li>
                    <?php } ?>
                </ul>
            </div>
            <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-2" href="main.php">
                <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
            </a>
            <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                <p>/ Liquidations</p>
            </div>
            <div class="container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                <!-- Botones -->
                <button id="btnComp" type="button" class="btn btn-success" data-toggle="modal tooltip" data-placement="bottom" title="New Liquidations" style="display:none"><i class="bi bi-clipboard2-plus"></i></button>
                <button id="btnCSF" type="button" class="btn btn-primary" data-toggle="modal tooltip" data-placement="bottom" title="Comp Situacion Fiscal" style="display:none"><i class="bi bi-book-half"></i></button> 
            </div>
            <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"><?php echo $_SESSION['username']; ?></button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                    <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Inicio de la tabla -->
    <div class="card card-body" style="opacity:100%;">
        <div class="table-responsive" style="opacity:100%;">
            <table id="tablaLiquidation" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;">
                <thead style="background-color: #65ac7c;" style="opacity:100%;">
                    <tr>
                        <th colspan="20"></th>
                        <th colspan="2" style="border-left: 2px solid black;"></th>
                        <th colspan="4" style="border-right: 2px solid black;">DESCUENTO PROMEDIO</th>
                        <th colspan="5"></th>
                        <th colspan="6" style="border-right: 2px solid black;">GRADOS</th>
                        <th colspan="2"></th>
                        <th colspan="4"style="border-right: 2px solid black;">MICRONAIRE</th>
                        <th colspan="2"></th>
                        <th colspan="3" style="border-right: 2px solid black;">FIBRA</th>
                        <th colspan="2"></th>
                        <th colspan="4" style="border-right: 2px solid black;">RESISTENCIA</th>
                        <th colspan="9"></th>
                        <th class="no-exportar"></th>
                    </tr>
                    <tr>
                        <th class="th-sm">ID</th>
                        <th class="th-sm">Crop</th>
                        <th class="th-sm">Region</th>
                        <th class="th-sm">Proveedor</th>
                        <th class="th-sm" style="width: 90px;">Contrato</th>
                        <th class="th-sm">Liq Id</th>
                        <th class="th-sm">Pacas</th>
                        <th class="th-sm">Precio</th>
                        <th class="th-sm">Peso Promedio</th>
                        <th class="th-sm">Tipo de pago</th>
                        <th class="th-sm">Tipo de Liq</th>
                        <th class="th-sm">Tipo Descuento</th>
                        <th class="th-sm">Num</th>
                        <th class="th-sm">Sub Num</th>
                        <th class="th-sm" style="width: 90px;">Fecha envio</th>
                        <th class="th-sm">Sin Descuento</th>
                        <th class="th-sm">Con Descuento</th>
                        <th class="th-sm">Prom</th>
                        <th class="th-sm">Pago</th>
                        <th class="th-sm">Pago %</th>
                        <th class="th-sm">Grado</th>
                        <th class="th-sm">Micro</th>
                        <th class="th-sm">Fibra</th>
                        <th class="th-sm">Resistencia</th>
                        <th class="th-sm">Otro</th>
                        <th class="th-sm">Total</th>
                        <th class="th-sm">SM</th>
                        <th class="th-sm">MP</th>
                        <th class="th-sm">M</th>
                        <th class="th-sm">SLMP</th>
                        <th class="th-sm">SLM</th>
                        <th class="th-sm">LMP</th>
                        <th class="th-sm">LM</th>
                        <th class="th-sm">SGOP</th>
                        <th class="th-sm">SGO</th>
                        <th class="th-sm">GO</th>
                        <th class="th-sm">O</th>
                        <th class="th-sm">2.4-</th>
                        <th class="th-sm">2.5 a 2.9</th>
                        <th class="th-sm">3.0 a 3.4</th>
                        <th class="th-sm">3.5 a 4.9</th>
                        <th class="th-sm">5.0 a 5.4</th>
                        <th class="th-sm">5.5+</th>
                        <th class="th-sm">31-</th>
                        <th class="th-sm">32</th>
                        <th class="th-sm">33</th>
                        <th class="th-sm">34</th>
                        <th class="th-sm">35+</th>
                        <th class="th-sm">17-</th>
                        <th class="th-sm">18/19</th>
                        <th class="th-sm">20/21</th>
                        <th class="th-sm">22/23</th>
                        <th class="th-sm">24/25</th>
                        <th class="th-sm">26+</th>
                        <th class="th-sm">Grd</th>
                        <th class="th-sm">Mic</th>
                        <th class="th-sm">Len</th>
                        <th class="th-sm">Str</th>
                        <th class="th-sm">Comentarios</th>
                        <th class="th-sm">Comp de Pago</th>
                        <th class="th-sm">Comp de Factura</th>
                        <th class="th-sm">Envio a Prov</th>
                        <th class="th-sm">Fecha de factura</th>
                        <th class="no-exportar" ></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <th class="th-sm">ID</th>
                        <th class="th-sm">Crop</th>
                        <th class="th-sm">Region</th>
                        <th class="th-sm">Proveedor</th>
                        <th class="th-sm">Contrato</th>
                        <th class="th-sm">Liq Id</th>
                        <th class="th-sm">Pacas</th>
                        <th class="th-sm">Precio</th>
                        <th class="th-sm">Peso Promedio</th>
                        <th class="th-sm">Tipo de pago</th>
                        <th class="th-sm">Tipo de Liq</th>
                        <th class="th-sm">Tipo Descuento</th>
                        <th class="th-sm">Num</th>
                        <th class="th-sm">Sub Num</th>
                        <th class="th-sm">Fecha envio</th>
                        <th class="th-sm">Sin Descuento</th>
                        <th class="th-sm">Con Descuento</th>
                        <th class="th-sm">Prom</th>
                        <th class="th-sm">Pago</th>
                        <th class="th-sm">Pago %</th>
                        <th class="th-sm">Grado</th>
                        <th class="th-sm">Micro</th>
                        <th class="th-sm">Fibra</th>
                        <th class="th-sm">Resistencia</th>
                        <th class="th-sm">Otro</th>
                        <th class="th-sm">Total</th>
                        <th class="th-sm">SM</th>
                        <th class="th-sm">MP</th>
                        <th class="th-sm">M</th>
                        <th class="th-sm">SLMP</th>
                        <th class="th-sm">SLM</th>
                        <th class="th-sm">LMP</th>
                        <th class="th-sm">LM</th>
                        <th class="th-sm">SGOP</th>
                        <th class="th-sm">SGO</th>
                        <th class="th-sm">GO</th>
                        <th class="th-sm">O</th>
                        <th class="th-sm">2.4-</th>
                        <th class="th-sm">2.5 a 2.9</th>
                        <th class="th-sm">3.0 a 3.4</th>
                        <th class="th-sm">3.5 a 4.9</th>
                        <th class="th-sm">5.0 a 5.4</th>
                        <th class="th-sm">5.5+</th>
                        <th class="th-sm">31-</th>
                        <th class="th-sm">32</th>
                        <th class="th-sm">33</th>
                        <th class="th-sm">34</th>
                        <th class="th-sm">35+</th>
                        <th class="th-sm">17-</th>
                        <th class="th-sm">18/19</th>
                        <th class="th-sm">20/21</th>
                        <th class="th-sm">22/23</th>
                        <th class="th-sm">24/25</th>
                        <th class="th-sm">26+</th>
                        <th class="th-sm">Grd</th>
                        <th class="th-sm">Mic</th>
                        <th class="th-sm">Len</th>
                        <th class="th-sm">Str</th>
                        <th class="th-sm">Comentarios</th>
                        <th class="th-sm">Comp de Pago</th>
                        <th class="th-sm">Comp de Factura</th>
                        <th class="th-sm">Envio a Prov</th>
                        <th class="th-sm">Fecha de factura</th>
                        <th class="no-exportar"></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!--Modal para CRUD-->
    <div class="modal fade" id="modalCRUD" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="formLiq" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="fileExcel" class="col-form-label">Select Excel File:</label>
                                    <input type="file" class="form-control" id="fileExcel" name="fileExcel" accept=".xlsx, .xls" required onchange="extencion_file()">
                                    <input type="hidden" id="fileExtension" name="fileExtension"/>
                                    <br>
                                    <label for="fileImg" class="col-form-label">Select Image Liquidation:</label>
                                    <input type="file" class="form-control" id="fileImg" name="fileImg" accept=".PNG,.png" required onchange="extencion_Img()">
                                    <input type="hidden" id="fileExtensionImg" name="fileExtensionImg" />
                                    <br>
                                    <label for="ImgPrice" class="col-form-label">Select Image Price:</label>
                                    <input type="file" class="form-control" id="ImgPrice" name="ImgPrice" accept=".PNG,.png" required onchange="Img_Price()">
                                    <input type="hidden" id="ExtensionImgP" name="ExtensionImgP" />
                                    <br>
                                    <div id="upd" >
                                        <label for="csvLiq" class="col-form-label">Select Csv File:</label>
                                        <input type="file" class="form-control" id="csvLiq" name="csvLiq" accept=".CSV,.csv" onchange="csvFile()">
                                        <input type="hidden" id="csvExt" name="csvExt" />
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div style="height: 1em;" id="Aviso"></div>
                        <div style="height: 2em;" id="avisoLiq"></div>
                        <button id = "exportlist" type="button" class="btn btn-success" style="display:none;" data-bs-dismiss="modal">Export Excel</button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" id="btnGuardar" class="btn btn-dark">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--Modal para editar-->
    <div class="modal fade" id="modalEdit" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="formEdit" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="porc" class="col-form-label">Porcentaje:</label>
                                    <input type="text" class="form-control form-control-sm" id="porc" >
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="pay" class="col-form-label">Pago:</label>
                                    <input type="text" class="form-control form-control-sm" id="pay" >
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label for="coment" class="col-form-label">Comentario:</label>
                                    <input type="text" class="form-control form-control-sm" id="coment" >
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="dateProv" class="col-form-label">Envio correo a Proveedor:</label>
                                    <input type="date" class="form-control form-control-sm" id="dateProv" >
                                </div>
                            </div>
                            <!-- <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="dateInv" class="col-form-label">Carga de factura:</label>
                                    <input type="date" class="form-control form-control-sm" id="dateInv" >
                                    <input type="file" id="fileInput" style="display: none;" accept=".xlsx, .xls">
                                </div>
                            </div> -->
                        </div><br><br><br><br>
                        <div class="row">
                            <div class="col-lg-5" id="documentoInv" style="display: none">
                                <div class="input-group mb-3" id="">
                                    <input  class="form-control me-2" type="text" name="actualfile_inv" id="actualfile_inv" placeholder="" readonly style="" >           
                                    <div id="descarga_inv" style="">
                                        <button class="btn btn-success btn-sm viewfile_inv" title="Descargar factura"><i class="material-icons">file_download</i></button>      
                                    </div>
                                </div> 
                            </div> 
                            <div class="col-lg-5" id="documentoXml" style="display: none">
                                <div class="input-group mb-3">
                                    <input class="form-control me-2" type="text" name="actualXml" id="actualXml" placeholder="" readonly>
                                    <div id="descarga_Xml">
                                        <button class="btn btn-success btn-sm viewfile_Xml" title="Descargar Xml">
                                            <i class="material-icons">file_download</i>
                                        </button>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-lg-5" id="documentoPago" style="display: none">
                                <div class="input-group mb-3" id="">
                                    <input  class="form-control me-2" type="text" name="actualfile_pay" id="actualfile_pay" placeholder="" readonly style="" >           
                                    <div id="descarga_pay" style="">
                                        <button class="btn btn-success btn-sm viewfile_pay" title="Descargar comprobante de pago"><i class="material-icons">file_download</i></button>      
                                    </div>
                                </div> 
                            </div> 
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div id="upInv" >
                        <button class="btn btn-primary btn-sm d-flex align-items-center justify-content-center viewInvoice" title="Cargar Factura" id="loadInv">
                            Load Invoice <i class="material-icons">file_upload</i>
                        </button>
                    </div>
                    <div id="updateInv" >
                        <button class="btn btn-success btn-sm d-flex align-items-center justify-content-center UpdateInvoice" title="Actualizar factura" id="UpdInv">
                            Update Invoice <i class="material-icons">file_upload</i>
                        </button>
                    </div>
                    <div id="comPago" >
                        <button class="btn btn-primary btn-sm d-flex align-items-center justify-content-center viewIPay" title="Cargar comprbante de pago" id="loadPago">
                            Load payment voucher <i class="material-icons">file_upload</i>
                        </button>
                    </div>
                    <div id="upComPago" >
                        <button class="btn btn-success btn-sm d-flex align-items-center justify-content-center UpdatePAy" title="Actualizar comprbante de pago" id="UpdPago">
                            Update payment voucher <i class="material-icons">file_upload</i>
                        </button>
                    </div>
                    <div class="me-auto">
                        <!-- <button class="btn btn-success btn-sm d-flex align-items-center justify-content-center viewExcel" title="Descargar Liquidación">
                            Liquidacion xlsx <i class="material-icons">file_download</i> 
                        </button> -->
                    </div>
                    <div style="height: 1em;" id="avisoEdit"></div>
                    <button type="button" class="btn btn-secondary " data-bs-dismiss="modal">Close</button>
                    <button id="save" type="button" class="btn btn-dark save ">Save</button>
                </div>
            </div>
        </div>
    </div>

    <!--Modal para Enviar correo Interno-->
    <div class="modal fade" id="modalMail" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="formEmail" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <input type="text" class="form-control usuario" id="usuario" hidden>
                                    <input type="text" class="form-control usuario" id="LiqID" hidden>
                                    <input type="text" class="form-control usuario" id="tLiq" hidden>
                                    <div class="input-group mt-3">
                                        <label class="input-group-text" for="mails" >Mails:  </label>
                                        <textarea  class="form-control mails" id="mails"rows="4" ></textarea>
                                    </div>
                                    <div class="input-group mt-3">
                                        <label class="input-group-text" for="subject">Subject:</label>
                                        <input type="text" class="form-control subject" id="subject" value="">
                                    </div>
                                    <div class="strike">
                                        <span>Body Mail</span>
                                    </div>
                                    <div class="form-group"> 
                                        <textarea class="form-control datosLiq" id="datosLiq" rows="7"></textarea>
                                    </div>
                                    <div class="input-group mt-3">
                                        <label class="input-group-text" for="comentarios">Notas:</label>
                                        <textarea  class="form-control comentarios" id="comentarios" rows="2" ></textarea>
                                    </div>
                                    <div id="FileExtra" style="display:none">
                                        <br>
                                        <label for="exInv" class="col-form-label">Select Excel Invoice:</label>
                                        <input type="file" class="form-control" id="exInv" name="exInv" accept=".xlsx, .xls" onchange="">
                                        <br>
                                        <label for="fileZip" class="col-form-label">Select ZIP Invoice:</label>
                                        <input type="file" class="form-control" id="fileZip" name="fileZip" accept=".pdf,.zip" onchange="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div style="height: 1em;" id="avisomail"></div>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button id="sendEmail" type="button" class="btn btn-primary sendEmail">Send Email</button>
                </div>
            </div>
        </div>
    </div>

    <!---------------------------FORMULARIO PARA SUBIR FACTURA PDF y XML--------------------->
    <div class="modal fade" id="modal_Invoice" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Load Invoice</h5>
                    <button id ="closeX"  class="btn-close"  aria-label="Close" >
                    </button>
                </div>
                <form id="formInv">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="fileInvoice" class="col-form-label">Select file Invoice:</label>
                                    <input type="file"  accept=".Pdf,.pdf" class="form-control" name="archivo" id="fileInvoice" required onchange="LoadFileInv()" >
                                    <input type="hidden" id="ExtPdf" name="ExtPdf" />
                                </div>
                                <div class="form-group"><br>
                                    <label for="fileXml" class="col-form-label">Select XML File:</label>
                                    <input type="file" class="form-control" id="fileXml" name="fileXml" accept=".xml,.XML" required onchange="LoadFileInv()">
                                    <input type="hidden" id="ExtXml" name="ExtXml" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div style="height: 1em;" id="avisoInv"></div>
                        <button id="close_Inv" class="btn btn-light" >Cancelar</button>
                        <button id="load_Inv"   class="btn btn-primary" disabled>Load Invoice</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!---------------------------FORMULARIO PARA SUBIR COMPROBANTE DE PAGO--------------------->
    <div class="modal fade" id="modal_VoucherPay" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Load payment voucher</h5>
                    <button id ="closeXC"  class="btn-close"  aria-label="Close" style="background-color: white;">
                    </button>
                </div>
                <form id="formCompPay">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="fileVoucher" class="col-form-label">Select file payment voucher:</label>
                                    <input type="file"  accept=".Pdf,.pdf,.JPG,.PNG,.jpg,.png" class="form-control" name="archivo" id="fileVoucher" required onchange="LoadFileVoucher()" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div style="height: 1em;" id="avisoP"></div>
                        <button id="closeVocher" class="btn btn-light" >Cancelar</button>
                        <button id="loadVoucher"   class="btn btn-primary" disabled>Load voucher</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--Modal para Enviar correo a Prov-->
    <div class="modal fade" id="modalMailP" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="formEmailP" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="text" class="form-control TipoLiqP" id="TipoLiqP" hidden>
                                <input type="text" class="form-control LiqIDP" id="LiqIDP" hidden> 
                                <div class="input-group mt-3">
                                    <label class="input-group-text" for="mailsP" >Mails:  </label>
                                    <textarea  class="form-control mailsP" id="mailsP"rows="3" ></textarea>
                                </div>
                                <div class="input-group mt-3">
                                    <label class="input-group-text" for="subject">Subject:</label>
                                    <input type="text" class="form-control subjectP" id="subjectP" value="">
                                </div>
                                <div class="input-group mt-3">
                                    <label class="input-group-text" for="subject">MÉTODO/FORMA DE PAGO:</label>
                                    <select class="form-control form-control-sm" id="payMetod" name="payMetod" style="">                           
                                        <option selected disabled>Select</option>
                                        <option value="1">PPD - 99</option>
                                        <option value="2">PUE - TRANSFERENCIA</option>
                                        <option value="3">PUE - COMPENSACION</option>
                                    </select>
                                </div>
                                <div class="strike">
                                    <span>Body Mail</span>
                                </div>
                                <div class="form-group"> 
                                    <textarea class="form-control datosLiqP" id="datosLiqP" rows="10"></textarea>
                                </div>
                                <!-- <div id="FileExtra" style="display:none">
                                    <br>
                                    <label for="exInvP" class="col-form-label">Select Excel Invoice:</label>
                                    <input type="file" class="form-control" id="exInvP" name="exInvP" accept=".xlsx, .xls" onchange="">
                                    <br>
                                    <label for="fileZipP" class="col-form-label">Select ZIP Invoice:</label>
                                    <input type="file" class="form-control" id="fileZipP" name="fileZipP" accept=".zip" onchange="">
                                </div> -->
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div style="height: 1em;" id="avisomailP"></div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="addCSF" value="1">
                        <label class="form-check-label" for="addCSF"><b>Adjuntar CSF</b></label>
                    </div>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button id="sendEmailP" type="button" class="btn btn-primary sendEmailP">Send Email</button>
                </div>
            </div>
        </div>
    </div>

    <!--Modal para agregar Constancia de Situacion fiscal-->
    <div class="modal fade" id="modalCSF" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="formCSF" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <br>
                                    <input type="file" class="form-control" id="fileCsf" name="fileCsf" accept=".pdf" required onchange="ext_CSF()">
                                    <input type="hidden" id="extC" name="extC"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div style="height: 1em;" id="AvisoR"></div>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        <button id="saveC"   class="btn btn-primary" disabled>Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>

</html>

<?php 
    endif;
?>
