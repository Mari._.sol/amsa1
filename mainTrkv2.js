var idglobaltruck = 0;

$(document).ready(function() {
    
    $('#tablaTrk tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    } );

    var TrkID, opcion;
    opcion = 4;

    tablaTrk = $('#tablaTrk').DataTable({
        //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			{
			    extend:    'excelHtml5',
			    text:      '<i class="fas fa-file-excel"></i> ',
			    titleAttr: 'Export to Excel',
			    className: 'btn btn-success',
                download: 'open',
			    exportOptions: {
			        columns: ":not(.no-exportar)"
			    }
			},
			{
			    extend:    'pdfHtml5',
			    text:      '<i class="fas fa-file-pdf"></i> ',
			    titleAttr: 'Export to PDF',
			    orientation: 'landscape',
			    className: 'btn btn-danger',
                download: 'open',
			    exportOptions: {
			        columns: ":not(.no-exportar)"
			    }
			},
            {
                text: '<i class="bi bi-funnel-fill"></i>',
                titleAttr: 'Filter by',
                orientation: 'landscape',
                className: 'btn btn-info btn-filtrar',
                attr:{
                        id:'filtraboton',
                        "data-toggle": 'modal tooltip',
                        "disabled": false
                }
            },
			/*{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},*/
        ],
    
        initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                        .search( this.value, true, false ).draw();
                            
                    }
                } );
            } );
        },
        "order": [ 0, 'desc' ],
        "scrollX": true, 
        "scrollY": '50vh', //"340px",
        "scrollCollapse": true,
        "lengthMenu": [100,200,300,500],
        fixedColumns:   {
            left: 0,
            right: 1
        },
        "ajax":{            
            "url": "bd/crudTrkv2.php",
            "method": 'POST', //usamos el metodo POST
            "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
            "dataSrc":""
        },
        "columns":[
            {"data": "TrkID"},
            {"data": "DO"},
            {"data": "Typ"},
            {"data": "LotsAssc"}, //LotsAssc
            {"data": "LotQty"}, //CrgQty
            {"data": "SchOutDate"},
            {"data": "OutDat"},
            {"data": "SchOutTime"},
            {"data": "OutTime"},
            {"data": "SchInDate"},
            {"data": "InDat"},
            {"data": "SchInTime"},
            {"data": "InTime"},
            {"data": "GinName"},
            {"data": "RegNameOut"},
            {"data": "RegNameIn"}, //Sigue contrato
            {"data": "Ctc"},
            {"data": "PlcNameIn"}, //Consulta con DO 
            {"data": "TNam"},
            {"data": "WBill"},
            {"data": "DrvLcs"},
            {"data": "TrkLPlt"},
            {"data": "TraLPlt"},
            {"data": "DrvNam"},
            {"data": "DrvTel", width: 200, render: function ( toFormat ) {
                return toFormat.toString().replace(
                  /(\d\d\d)(\d\d\d)(\d\d\d\d)/g, '$1-$2-$3'
                ); }},
            {"data": "LiqWgh", render: $.fn.dataTable.render.number( ',')}, //PWgh  
            {"data": "OutWgh", render: $.fn.dataTable.render.number( ',')},
            {"data": "InWgh", render: $.fn.dataTable.render.number( ',')},
            {"data": "RqstID"},
            {"data": "PO"},
            {"data": "RO"},
            {"data": "Cost"},
            {"data": "Comments"},
            {"data": "Status"},
            {"data": "IrrDat"}, //<button class='btn btn-success btn-sm btnBuscar'><i class='material-icons'>manage_search</i></button>
            {"data": "DateCreated", width: 200},
            {"data": "RemisionPDF"},
            {"data": "XML"},
            {"data": "Samples"},
            {"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-success btn-sm btnremision'><i class='material-icons'>local_shipping</i></button><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button><button class='btn btn-info btn-sm btnPDF-REM'><i class='material-icons'>picture_as_pdf</i></button><button class='btn btn-primary btn-sm btnTag'><i class='material-icons'>local_offer</i></button><button class='btn btn-primary btn-sm previewEmail'><i class='bi bi-envelope-check-fill'></i></button></div></div>"}
        ],
        columnDefs : [
            { targets : [38],
              render : function (data, type, row) {
                return data == '1' ? 'True' : 'False'
              }
            }
       ]
   
    });     

    $('.dataTables_length').addClass('bs-select');

    /*function LoadLots(){
            $("#LotsAssc").empty();
            $('#AsscLots').val("");
            $('#QtyDO').val("");
            $('#PWgh').val("");
            var opts = "";
            opcion = 6;
            DO = $.trim($('#DOrd').val());
            $.ajax({
                url: "bd/crudTrkv2.php",
                type: "POST",
                datatype:"json",
                data:  {DO:DO ,opcion:opcion},    
                success: function(data){
                    opts = JSON.parse(data);
                    for (var i = 0; i< opts.length; i++){
                        $('#LotsAssc').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '-' + opts[i].Qty + ' bc </option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });
    };*/
    
    
    var fila; //captura la fila, para editar o eliminar -- submit para el Alta y Actualización
    $('#formTrk').submit(function(e){
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
        DOrd = $.trim($('#DOrd').val());
        TrkLPlt = $.trim($('#TrkLPlt').val().toUpperCase());    
        TraLPlt = $.trim($('#TraLPlt').val().toUpperCase());
        TNam = $.trim($('#TNam').val());    
        WBill = $.trim($('#WBill').val().toUpperCase());
        DrvLcs = $.trim($('#DrvLcs').val().toUpperCase());
        DrvNam = $.trim($('#DrvNam').val().toUpperCase());
        DrvTel = $.trim($('#DrvTel').val()).replace(/-/g, "");
        SchOutDat = $.trim($('#SchOutDat').val());
        OutDat = $.trim($('#OutDat').val());
        SchOutTime = $.trim($('#SchOutTime').val());
        OutTime = $.trim($('#OutTime').val());
        OutWgh = $.trim($('#OutWgh').val()).replace(/,/g, "");
        SchInDat = $.trim($('#SchInDat').val());
        InDat = $.trim($('#InDat').val());
        SchInTime = $.trim($('#SchInTime').val());
        InTime = $.trim($('#InTime').val());
        InWgh = $.trim($('#InWgh').val()).replace(/,/g, "");
        TrkCmt = $.trim($('#TrkCmt').val().toUpperCase());
        RqstID = $.trim($('#RqstID').val());
        RO = $.trim($('#RO').val());
        // = $.trim($('#FreightC').val());
        AsscLots = $.trim($('#AsscLots').val().toUpperCase());
        CrgQty = $.trim($('#QtyDO').val());
        PWgh = $.trim($('#PWgh').val()).replace(/,/g, "");
        Status = $.trim($('#Status').val());
        IrrDat = $.trim($('#IrrDat').val());

        if( $('#samples').prop('checked') ) {
            samples = 1;
        }  else {
            samples = 0;
        }
        //alert (Status2);
        //alert (Status);
        
        /*if(FreightC == ""){
            alert ("This truck has no assigned cost.");
        }*/
        
        if (Status != Status2){
            ban = 1;
        }else{
            ban = 0;
        }
        //val = $.isNumeric(TNam);
        if (opcion == 2){
            $.ajax({
                url: "bd/crudTrkv2.php",
                type: "POST",
                datatype:"json",    
                data:  {ban:ban, TrkID:TrkID, TrkLPlt:TrkLPlt, TraLPlt:TraLPlt, TNam:TNam, WBill:WBill, DrvLcs:DrvLcs, DrvNam:DrvNam, DrvTel:DrvTel, CrgQty:CrgQty, PWgh:PWgh, SchOutDat:SchOutDat, SchOutTime:SchOutTime, OutDat:OutDat, OutTime:OutTime, OutWgh:OutWgh, SchInDat:SchInDat, SchInTime:SchInTime, InDat:InDat, InTime:InTime, InWgh:InWgh, TrkCmt:TrkCmt, DOrd:DOrd, RqstID:RqstID, RqstID1:RqstID1, RO:RO, AsscLots:AsscLots, Status:Status, IrrDat:IrrDat, opcion:opcion,samples:samples}, //AsscLots:AsscLots,
                success: function(data) {
                    tablaTrk.ajax.reload(null,false);
                    $('#modalCRUD').modal('hide');
                }
            });
        }else{
            $.ajax({
                url: "bd/crudTrkv2.php",
                type: "POST",
                datatype:"json",    
                data:  {ban:ban, TrkID:TrkID, TrkLPlt:TrkLPlt, TraLPlt:TraLPlt, TNam:TNam, WBill:WBill, DrvLcs:DrvLcs, DrvNam:DrvNam, DrvTel:DrvTel, CrgQty:CrgQty, PWgh:PWgh, SchOutDat:SchOutDat, SchOutTime:SchOutTime, OutDat:OutDat, OutTime:OutTime, OutWgh:OutWgh, SchInDat:SchInDat, SchInTime:SchInTime, InDat:InDat, InTime:InTime, InWgh:InWgh, TrkCmt:TrkCmt, DOrd:DOrd, RqstID:RqstID, RqstID1:RqstID1, RO:RO, AsscLots:AsscLots, Status:Status, IrrDat:IrrDat, opcion:opcion,samples:samples}, //AsscLots:AsscLots,
                success: function(data) {
                    tablaTrk.ajax.reload(null,false);
                    $('#divCreate').hide();
                    $('#LotsTrk').show();
                    $('#divAsociar').show();
                    document.getElementById('btnupdate').style.display = 'none';
                    document.getElementById('btnloadfile').style.display = 'none';
                    var opts = JSON.parse(data);
                    TrkID = opts[0].TrkID;
                    opcion = 2; 
                    $('#exampleModalLabel').val(TrkID);
                }
            });
        }
    });
    
    
    //submit para Carga masiva PO's
$('#formFile').submit(function(e){
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
    var comprobar = $('#csv').val().length;
    var extension = $('#csv').val().split(".").pop().toLowerCase();
    //validar extension
    /*if($.inArray(extension, ['csv']) == -1)
	{
		alert("Invalid File");
		retornarError = true;
		$('#csv').val("");
	}else{*/
    if (comprobar>0){
        var formulario = $("#formFile");
        var archivos = new FormData();
        var url = "bd/procesarPO.php";
            for (var i=0; i<(formulario.find('input[type=file]').length); i++){
                archivos.append((formulario.find('input[type="file"]:eq('+i+')').attr("name")),((formulario.find('input[type="file"]:eq('+i+')'))[0].files[0]));
            }
    $.ajax({
          url: "bd/procesarPO.php",
          type: "POST",
          contentType: false,
          data:  archivos,
          processData: false,
          success: function(data) {
            //var opts = JSON.parse(data);
            //alert (opts);
            //alert (data);
            if (data == 'OK'){
                alert("Successful Import");
                $('#modalFile').modal('hide');
                //location.reload();
                return false;
            } else if (data == 'Columns'){
                alert("Check the number of columns.");
                return false;
            }else if (data == 'Exist'){
                alert ("Existen los lotes");
                return false;
            }else{
                alert("Successful Import"); //alert("Import Error");
                $('#modalFile').modal('hide');
                //location.reload();
                return false;
           }
          }
        });
        return false;    
    }else{
        alert ("Select Excel file to import");
        return false;
    }
    //}   	        
    //$('#modalFile').modal('hide');
    //location.reload();
});
    
    
    
    // Formulario Buscar OE y Lots  *comentario 4 junio****
    $('#formOEL').submit(function(e){                         
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
        TrkLPlt = $.trim($('#TrkLPlt').val());    
        TraLPlt = $.trim($('#TraLPlt').val());
        TNam = $.trim($('#TNam').val());                             
        $.ajax({
            url: "bd/crudTrkv2.php",
            type: "POST",
            datatype:"json",    
            data:  {TrkID:TrkID, TrkLPlt:TrkLPlt,opcion:opcion},    
            success: function(data) {
                tablaTrk.ajax.reload(null, false);
            }
        });			        
        //$('#modalOEL').modal('hide');											     			
    });


    // Formulario Splits ***************************************************************************
    $('#Split-Lots').submit(function(e){                         
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
        var ban = 0;
        LocSplit = $.trim($('#LocSplit').val());    
        DOSplit = $.trim($('#DOSplit').val());
        // Obtener texto del selector y la cantidad de pacas totales del lote ******
        var SelectLot = document.getElementById("LotsSplit");
        var LotTxt = SelectLot.options[SelectLot.selectedIndex].text;
        LotIDSplit = $.trim($('#LotsSplit').val());
        BC = LotTxt.split(" - ");
        BC = BC[1];
        Bal = LotTxt.split(" - ");
        Bal = Bal[0];
        // ***************************************************************************
        Split = $.trim($('#Split').val());
        Split = Split.split(",");
        tam = Split.length;
        //validar que las particiones sean positivas
        for (i=0; i<=tam; i++){
            if (Split[i] < 0){
                ban = 1;
            }
        }
        if (ban == 1){
            alert ("Partitions must be positive");
        }else{
        
            sum = 0;
            $.each(Split,function(){sum+=parseFloat(this) || 0; }); // suma de las particiones
            if (sum>BC || sum<BC){
                alert ("The sum total of the partitions must match the number of bales in the Lot");    
            }else{
                opcion = 14;
                var respuesta = confirm("Are you sure you want to partition the Lot "+Bal+" in "+Split+"?");                
                if (respuesta) {            
                    $.ajax({
                        url: "bd/crudTrkv2.php",
                        type: "POST",
                        datatype:"json",    
                        data:  {LotIDSplit:LotIDSplit, Split:Split, opcion:opcion},    
                        success: function(data) {
                            tablaTrk.ajax.reload(null,false);
                        $('#SplitLots').modal('hide');
                        }
                    });
                   // $('#avisoLot').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');      
	            
                }
            }
        }
        
        /*$.ajax({
            url: "bd/crudTrkv2.php",
            type: "POST",
            datatype:"json",    
            data:  {TrkID:TrkID, TrkLPlt:TrkLPlt,opcion:opcion},    
            success: function(data) {
                tablaTrk.ajax.reload(null, false);
            }
        });*/			        
        //$('#SplitLots').modal('hide');											     			
    });


    //Borrar PR y PO
    $("#RemPOPR").click(function(){ //$('#RemPOPR').submit(function(e){
        //e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
        TrkID = $.trim($('#RTrkID').val());
        opcion = 17;
        var respuesta = confirm("Are you sure you want to remove the PO and PR from this TrkID "+TrkID+"?");                
        if (respuesta) {
            var RemPO =  $.ajax({
                type: 'POST',
                url: 'bd/crudTrkv2.php',
                data: { TrkID:TrkID, opcion:opcion}
            }) 

            RemPO.done(function(data){
        
               // console.log(data);
                tablaTrk.ajax.reload(function(){
                        $('#avisoPOPR').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Datos eliminados con éxito</div>')
                        setTimeout(function() {
                            $('#alert1').fadeOut(1000);
                            $('#modalPOPR').modal('hide');
                        },1000);
                });
            });
        }
    });




        
    //para limpiar los campos antes de dar de Alta una Persona
    $("#btnNuevo").click(function(){
        localStorage.clear();
        $('#SchOutDat').prop('disabled', false);
        $('#SchOutTime').prop('disabled', false);
        $('#OutDat').prop('disabled', false);
        $('#OutTime').prop('disabled', false);
        $('#OutWgh').prop('disabled', false);
        $('#SchInDat').prop('disabled', false);
        $('#SchInTime').prop('disabled', false);
        $('#InDat').prop('disabled', false);
        $('#InTime').prop('disabled', false);
        $('#InWgh').prop('disabled', false);
        $("#Status").prop("disabled", false);
        $("#IrrDat").prop("disabled", false);
        $("#NoPWgh").hide();
        $("#NoWgh").hide();
        $("#OutWghMin").hide();
        $("#OutWghMax").hide();
        $("#InWghMin").hide();
        $("#InWghMax").hide();
        $("#LotsTrk").hide(); //Ocultar lotes asociados
        $("#divCreate").show(); //Ocultar lotes asociados
        $("#divAsociar").hide(); //Ocultar lotes asociados
        $("#TNam").empty();
        $("#RqstID").empty();
        $("#LotsAssc").empty();
        $("#DOrd").prop("disabled", false);
         $('#btnGuardar').attr('disabled',false);
        //ValidateWgh();
        Status2 = "Programmed";
        ban = 0;
        opcion = 1; //alta
        TrkID=null;
        RqstID1=null;
        $("#formTrk").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("New Truck");
        $('#modalCRUD').modal('show');	    
        document.getElementById('documento').style.display = 'none';
        document.getElementById('documentoxml').style.display = 'none';
        document.getElementById('btntarifario').style.display = 'none';
    });
    
    
    //carga PO's
    $("#btnFile").click(function(){
        $("#formFile").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Import Data");
        $('#modalFile').modal('show');	    
    });


    //Boton para split de lotes
    $("#btnSplit").click(function(){
        $("#LocSplit").empty();
        $("#DOrdSplit").empty();
        $("#LotsSplit").empty();
        LoadRegion();
        $('#LocSplit').append('<option value="" disabled selected >- Select -</option>');
        //ValidateWgh();
        //opcion = 1; //alta
        //TrkID=null;
        $("#Split-Lots").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Split a Lot");
        $('#SplitLots').modal('show');	    
    });

    //Editar        
    $(document).on("click", ".btnEditar", function(){
        $("#samples").prop("checked", false);
        //Privilegios = $.trim($('#Priv').val());
        //alert (Privilegios);
        $("#NoPWgh").hide();
        $("#NoWgh").hide();
        $("#OutWghMin").hide();
        $("#OutWghMax").hide();
        $("#InWghMin").hide();
        $("#InWghMax").hide();
        $("#LotsTrk").show(); //Mostrar lotes asociados
        $("#divCreate").hide(); //Mostrar lotes asociados
        $("#divAsociar").show(); //Mostrar lotes asociados
        //$("#formTrk").trigger("reset");
        $("#TNam").empty();
        $("#RqstID").empty();
        //$("#FreightC").val("");
        $('#btnGuardar').attr('disabled',false);
        //ValidateWgh();  
        ban = 0;
        opcion = 2;//editar
        fila = $(this).closest("tr");	  
        idglobaltruck = parseInt(fila.find('td:eq(0)').text());           
        TrkID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID		            
        DOrd = parseFloat(fila.find('td:eq(1)').text());
        Typ = fila.find('td:eq(2)').text();
        LotsAssc = fila.find('td:eq(3)').text();
        Qty = parseFloat(fila.find('td:eq(4)').text());
        SchOutDat = fila.find('td:eq(5)').text();
        OutDat = fila.find('td:eq(6)').text();
        SchOutTime = fila.find('td:eq(7)').text();
        OutTime = fila.find('td:eq(8)').text();
        SchInDat = fila.find('td:eq(9)').text();
        InDat = fila.find('td:eq(10)').text();
        SchInTime = fila.find('td:eq(11)').text();
        InTime = fila.find('td:eq(12)').text();
        DepReg = fila.find('td:eq(14)').text();
        ArrReg = fila.find('td:eq(15)').text();
        Cli = fila.find('td:eq(17)').text();
        //TNam = fila.find('td:eq(12)').text(); 
        WBill = fila.find('td:eq(19)').text();
        DrvLcs = fila.find('td:eq(20)').text();
        TrkLPlt = fila.find('td:eq(21)').text();
        TraLPlt = fila.find('td:eq(22)').text();
        DrvNam = fila.find('td:eq(23)').text();
        DrvTel = fila.find('td:eq(24)').text();
        PWgh = fila.find('td:eq(25)').text();
        OutWgh = fila.find('td:eq(26)').text();
        InWgh = fila.find('td:eq(27)').text();
        PO = fila.find('td:eq(29)').text();
        RO = parseFloat(fila.find('td:eq(30)').text());
        TrkCmt = fila.find('td:eq(32)').text();
        Status = fila.find('td:eq(33)').text();
        Status2 = fila.find('td:eq(33)').text(); //Estatus auxiliar
        Costo = fila.find('td:eq(31)').text();
        IrrDat = fila.find('td:eq(34)').text();   
        pdfload = parseInt(fila.find('td:eq(36)').text());
        xmlload = parseInt(fila.find('td:eq(37)').text());
        samples = fila.find('td:eq(38)').text();
        document.getElementById('btntarifario').style.display = 'block';

        if (pdfload == 0 ){
            document.getElementById('btnupdate').style.display = 'none';
            document.getElementById('btnloadfile').style.display = 'block';
            document.getElementById('documento').style.display = 'none';
   
        //ValidateWgh();  
        } else {
            document.getElementById('btnupdate').style.display = 'block';
            document.getElementById('btnloadfile').style.display = 'none';
            document.getElementById('documento').style.display = 'block';
            $('#actualfile').val(idglobaltruck + '.pdf');
   
        }
//----------------------------VALIDAR QUE EL XML SE SUBIO PARA HABILITAR BOTONES------------------------------------
        

        if(xmlload == 0){
            document.getElementById('btnupdatexml').style.display = 'none';
            document.getElementById('btnloadxml').style.display = 'block';
            document.getElementById('documentoxml').style.display = 'none';

        } else{

            document.getElementById('btnupdatexml').style.display = 'block';
            document.getElementById('btnloadxml').style.display = 'none';
            document.getElementById('documentoxml').style.display = 'block';
            $("#actualxml").val( TrkID+".xml");



        }
        

        
        /*
        if (Costo == "SI"){
            $("#FreightC").prop("disabled", true);
            $("#EditCost").show();
        }else{
            $("#FreightC").prop("disabled", false);
            $("#EditCost").hide();
        }

        */

        if (Typ == "CON"){
            $("#IrrDat").prop("disabled", true);
        }else{
            $("#IrrDat").prop("disabled", false);
        }

        if (Status == "Received" && Typ == "CON"){
            $("#Status").prop("disabled", true);
        }else{
            $("#Status").prop("disabled", false);
        }
        //Qty = parseFloat(fila.find('td:eq(3)').text());
        $("#TrkID").val(TrkID);
        $("#TrkLPlt").val(TrkLPlt);
        $("#OutRegDO").val(DepReg);
        $("#InRegDO").val(ArrReg);
        $("#TypDO").val(Typ);
        $("#Client").val(Cli);
        $("#TraLPlt").val(TraLPlt);
        $('#TNam').append('<option class="form-control selected">' + fila.find('td:eq(18)').text() + '</option>'); //$("#TNam").val(TNam);
        $('#RqstID').append('<option class="form-control selected">' + fila.find('td:eq(28)').text() + '</option>'); //$("#TNam").val(TNam);
        $("#WBill").val(WBill);
        $("#DrvLcs").val(DrvLcs);
        $("#DrvNam").val(DrvNam);
        $("#DrvTel").val(DrvTel);
        $("#SchOutDat").val(SchOutDat);
        $("#OutDat").val(OutDat);
        $("#OutTime").val(OutTime);
        $("#SchOutTime").val(SchOutTime);
        $("#OutWgh").val(OutWgh);
        $("#SchInDat").val(SchInDat);
        $("#InDat").val(InDat);
        $("#SchInTime").val(SchInTime);
        $("#InTime").val(InTime);
        $("#RO").val(RO);
        $("#PO").val(PO);
        $("#InWgh").val(InWgh);
        $("#TrkCmt").val(TrkCmt);
        $("#DOrd").val(DOrd);
        //$("#RqstID").val(RqstID);
        $("#Status").val(Status);
        $("#IrrDat").val(IrrDat);
        $("#LotsAssc").empty();
        $('#AsscLots').val(LotsAssc);
        $('#QtyDO').val(Qty);
        $('#PWgh').val(PWgh);

        //Check de Samples 
         
        if (samples == 'True'){
            $("#samples").prop("checked", true);            
         }

        //****************calcular peso promedio de pacas************************************* 
        pesosalida = OutWgh.replace(/,/g, ''); 
        pesollegada = InWgh.replace(/,/g, '');        
        pesosalida=parseInt(pesosalida);        
        pesollegada = parseInt(pesollegada);
        cantidad = parseInt(Qty);       

        if (cantidad != '0' && pesosalida != '0'){
            promdiosalida = (pesosalida)/(cantidad);
            promdiosalida = promdiosalida.toFixed(2)
        
            //promdiosalida = parseFloat(promdiosalida);
            $('#promsalida').val(promdiosalida);   

        }
        else{
            $('#promsalida').val(0);  
        
        }

        if (cantidad != '0' && pesollegada!= '0'){
        
            promllegada = (pesollegada)/(cantidad)
            promllegada = promllegada.toFixed(2)
            //promdiosalida = parseFloat(promdiosalida);
        
            $('#promllegada').val(promllegada);   

        }
        else{
        
            $('#promllegada').val(0);
        }

        //***************** */ AQUI TERMINA PESOS PROMEDIO***************************************************/




        RqstID1 = $.trim($('#RqstID').val());
        RqstID = $.trim($('#RqstID').val());
        $("#RqstID").change( function() {
            RqstID1 = $.trim($('#RqstID').val());
            if (RqstID != RqstID1){
                RqstID1 = RqstID;
            }
        });
        $("#Status").change( function() {
            ban = 1;
        });
        
        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $(".modal-title").text("Edit Truck");		
        $('#modalCRUD').modal('show');		   
    });

    //Borrar
    $(document).on("click", ".btnBorrar", function(){
        fila = $(this);           
        TrkID = parseInt($(this).closest('tr').find('td:eq(0)').text()) ;		
        opcion = 3; //eliminar        
        var respuesta = confirm("¿Está seguro que deseas deshabilitar el registro "+TrkID+"?");                
        if (respuesta) {            
            $.ajax({
                url: "bd/crudTrkv2.php",
                type: "POST",
                datatype:"json",    
                data:  {opcion:opcion, TrkID:TrkID},    
                success: function() {
                    tablaTrk.row(fila.parents('tr')).remove().draw();                  
                }
            });	
        }
    });
    
    //Buscar OE y lotes
    $(document).on("click", ".btnBuscar", function(){
        //$("#Lot").empty();
        //opcion = 5; //
        fila = $(this);
        TrkID = parseInt($(this).closest('tr').find('td:eq(0)').text()); //Obtener el ID del camion a asociar
        $("#formOEL").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Search OE y Lots");
        $('#modalOEL').modal('show');
    });
    
    //Actualizar lot
    /*function update(){
        $("#Lot").empty();
        $("#QtyLot").val("");
        var opts = "";
        opcion = 5;
        DO = $.trim($('#DO').val());
        //id = TrkID;
        $.ajax({
            url: "bd/crudTrkv2.php",
            type: "POST",
            datatype:"json",
            data:  {DO:DO ,opcion:opcion},    
            success: function(data){
                opts = JSON.parse(data);
                for (var i = 0; i< opts.length; i++){
                    $('#Lot').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '</option>');
                    $('#QtyLot').val(opts[0].Qty);
                }
            },
            error: function(data) {
                alert('error');
            }
        });
    }*/
    
    //Vaciar lotes en select html dependientes de la OE ingresada 
    //$(document).on("click", ".btnBuscarLot", function(){
    //    update();
        /*opcion = 5;
        DO = $.trim($('#DO').val());
        //id = TrkID;
        $.ajax({
              url: "bd/crudTrkv2.php",
              type: "POST",
              datatype:"json",
              data:  {DO:DO ,opcion:opcion},    
              success: function(data){
                  $("#Lot").empty();
                  //$("#QtyLot").empty();
                  var opts = JSON.parse(data);
                  for (var i = 0; i< opts.length; i++){
                      $('#Lot').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '</option>');
                      $('#QtyLot').val(opts[0].Qty);
                  }
               },
            error: function(data) {
                alert('error');
            }
        });*/
    //});



    $('#btnloadfile').on('click', function(e) {
            e.preventDefault(); 
     
            if (pdfload == 0 ){
                document.getElementById('update').style.display = 'none';
                document.getElementById('loadfile').style.display = 'block';
       
            //ValidateWgh();  
            } else {
                document.getElementById('update').style.display = 'block';
                document.getElementById('loadfile').style.display = 'none';
       
            }
            var $el = $('#file');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
            $('#loadfile').prop('disabled', true);
            $(".modal-header").css("background-color", "#17562c");
            $(".modal-header").css("color", "white" );
            $(".modal-title").text("Load File");
            $('#filesload').modal('show');
                
            });
    
    //BOTON ACTUALIZAR CARTA PORTE PDF
        $('#btnupdate').on('click', function(e) {
            e.preventDefault(); 
            if (pdfload == 0 ){
                document.getElementById('update').style.display = 'none';
                document.getElementById('loadfile').style.display = 'block';
       
            //ValidateWgh();  
            } else {
                document.getElementById('update').style.display = 'block';
                document.getElementById('loadfile').style.display = 'none';
       
            }
    
            var $el = $('#file');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
            
            $('#update').prop('disabled', true);
               
                $(".modal-header").css("background-color", "#17562c");
                $(".modal-header").css("color", "white" );
                $(".modal-title").text("Update File");
                $('#filesload').modal('show');
                    
         });
    
    
    
    
        //CARGAR PDF EN AWS S3
        $('#loadfile').on('click', function(e) {
            e.preventDefault(); 
            //let frm = document.getElementById("uploadFile");
            var file_data = $('#file').prop('files')[0];   
           // let form_data = new FormData(frm);    
            var form_data = new FormData();             
            form_data.append('file', file_data);
            form_data.append('idtruck', idglobaltruck);
            $('#loadfile').prop('disabled', true);
            //console.log(idglobaltruck);
           // form_data.append("info", JSON.stringify(idglobaltruck));
           $('#avisofile').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>'); 
           //alert(form_data);                             
            $.ajax({
                url: 'inputfile.php', // <-- point to server-side PHP script 
                dataType: 'text',  // <-- what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(){
                    $('#filesload').modal('hide'); // <-- display response from the PHP script, if any
                    $('#avisofile').html("");
                  
                    alert("Archivo SUBIDO con exito");
                    document.getElementById('documento').style.display = 'block';
                    $('#actualfile').val(idglobaltruck + '.pdf');
    
                }
             });
        });
    
     //ACTUALIZAR PDF EN AWS S3
        $('#update').on('click', function(e) {
            e.preventDefault(); 
            $('#update').prop('disabled', true);
            var file_data = $('#file').prop('files')[0];   
            $('#avisofile').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');  
            var form_data = new FormData();             
            form_data.append('file', file_data);
            form_data.append('idtruck', idglobaltruck);
           // console.log(idglobaltruck);
           // form_data.append("info", JSON.stringify(idglobaltruck));
          // alert(form_data);                             
            $.ajax({
                url: 'inputfile.php', // <-- point to server-side PHP script 
                dataType: 'text',  // <-- what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(){
                
                   
                    $('#filesload').modal('hide');
                    $('#avisofile').html("");
                    alert("Archivo ACTUALIZADO con exito"); // <-- display response from the PHP script, if any
                 
                }
             });
        });
    
    
    //VER CARTA PORTE PDF
        $(document).on("click", ".viewfile", function(e){
          
                e.preventDefault(); 
                window.open("opens3.php?TrkID="+idglobaltruck);
              
          
    
                
            });
    
    

    //-----------------FUNCION PARA ABRIR MODAL DE  SUBIR XML --------------- 

    $('#btnloadxml').on('click', function(e) {
        e.preventDefault(); 
        $('#loadxml').prop('disabled', true);

        $("#filexml").val("");
 
        if (xmlload == 0 ){
            document.getElementById('updatexml').style.display = 'none';
            document.getElementById('loadxml').style.display = 'block';
   
        //ValidateWgh();  
        } else {
            document.getElementById('updatexml').style.display = 'block';
            document.getElementById('loadxml').style.display = 'none';
   
        }
        var $el = $('#filexml');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        
        $('#xml_load').prop('disabled', true);
        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $(".modal-title").text("Load XML");
        $('#xml_load').modal('show');
            
        });



    //----------------AQUI TERMINA FUNCION DE ABRIR MODAL PARA SUBIR XML


    //----------CARGAR XML EN AWS S3 ------------------------------------------------
    $('#loadxml').on('click', function(e) {
        e.preventDefault(); 
        //let frm = document.getElementById("uploadFile");
        var file_data = $('#filexml').prop('files')[0];   
       // let form_data = new FormData(frm);    
        var form_data = new FormData();             
        form_data.append('file', file_data);
        form_data.append('idtruck', idglobaltruck);
        $('#loadxml').prop('disabled', true);
        //console.log(idglobaltruck);
       // form_data.append("info", JSON.stringify(idglobaltruck));
       $('#avisoxml').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>'); 
       //alert(form_data);                             
        $.ajax({
            url: 'inputxml.php', // <-- point to server-side PHP script 
            dataType: 'text',  // <-- what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(){
                $('#xml_load').modal('hide'); // <-- display response from the PHP script, if any
                $('#avisoxml').html("");              
                alert("Archivo XML SUBIDO con exito");
                document.getElementById('documentoxml').style.display = 'block';
                $('#actualxml').val(idglobaltruck + '.xml');
                document.getElementById('btnupdatexml').style.display = 'block';
                document.getElementById('btnloadxml').style.display = 'none';
             

            }
         });
    });



    //*****BOTON PARA DESCARGAR XML************** */

    $(document).on("click", ".btndownloadxml", function(e){
          
        e.preventDefault(); 
        window.open("descxml.php?TrkID="+idglobaltruck);
      
  

        
    });

      //***** TERMINA FUNCION DE BOTON PARA DESCARGAR XML************** */

///**************BOTON PARA MOSTRAR EL MODAL UPDATE****************** */
$('#btnupdatexml').on('click', function(e) {
    e.preventDefault(); 

   
            if(xmlload == 0){
                document.getElementById('updatexml').style.display = 'none';
                document.getElementById('loadxml').style.display = 'block';

            } else{

                document.getElementById('updatexml').style.display = 'block';
                document.getElementById('loadxml').style.display = 'none';
            }         

    var $el = $('#filexml');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap();
    
    $('#updatexml').prop('disabled', true);
       
        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $(".modal-title").text("Update XML");
        $('#xml_load').modal('show');
            
 });



///***************AQUI TERMINA FUNCION DE BOTON MOSTRAR MODAL UPDATE */






//**********************FUNCION PARA ACTUALIZAR XML **************************


     //ACTUALIZAR PDF EN AWS S3
     $('#updatexml').on('click', function(e) {
        e.preventDefault(); 
        $('#updatexml').prop('disabled', true);
        var file_data = $('#filexml').prop('files')[0];   
        $('#avisoxml').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');  
        var form_data = new FormData();             
        form_data.append('file', file_data);
        form_data.append('idtruck', idglobaltruck);
       // console.log(idglobaltruck);
       // form_data.append("info", JSON.stringify(idglobaltruck));
      // alert(form_data);                             
        $.ajax({
            url: 'inputxml.php', // <-- point to server-side PHP script 
            dataType: 'text',  // <-- what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(){
            
               
                $('#xml_load').modal('hide');
                $('#avisoxml').html("");
                alert("Archivo ACTUALIZADO con exito"); // <-- display response from the PHP script, if any
                document.getElementById('btnupdatexml').style.display = 'block';
                document.getElementById('btnloadxml').style.display = 'none';
             
            }
         });
    });





//************************TERMINA FUNCION PARA ACTUALIZAR  *******************/
    
    
    
    
    //Qty dependiente de select de Lotes
    $("#Lot").on("change", function(){
        opcion = 6;
        id = $("#Lot").val();
        $.ajax({
            url: "bd/crudTrkv2.php",
            type: "POST",
            datatype:"json",
            data:  {id:id ,opcion:opcion},    
            success: function(data){
                var opts = JSON.parse(data);
                $('#QtyLot').val(opts[0].Qty);
            },
            error: function(data) {
                alert('error');
            }
        });
    });
    
    //Cargar lotes comentado 27 jul
    function LoadLots(){
            $("#LotsAssc").empty();
            $('#AsscLots').val("");
            $('#QtyDO').val("");
            $('#PWgh').val("");
            var opts = "";
            opcion = 6;
            DO = $.trim($('#DOrd').val());
            $.ajax({
                url: "bd/crudTrkv2.php",
                type: "POST",
                datatype:"json",
                data:  {DO:DO ,opcion:opcion},    
                success: function(data){
                    opts = JSON.parse(data);
                    for (var i = 0; i< opts.length; i++){
                        $('#LotsAssc').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '-' + opts[i].Qty + ' bc </option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });
    }
    
    //$("#DOrd").on("change", function(){
    //        LoadLots();
            //val = document.getElementsByClassName("modal-title"); //.value; //$.trim($(".modal-title"));
            /*$("#LotsAssc").empty();
            $('#AsscLots').val("");
            $('#QtyDO').val("");
            $('#PWgh').val("");
            var opts = "";
            opcion = 6;
            DO = $.trim($('#DOrd').val());
            $.ajax({
                url: "bd/crudTrkv2.php",
                type: "POST",
                datatype:"json",
                data:  {DO:DO ,opcion:opcion},    
                success: function(data){
                    opts = JSON.parse(data);
                    for (var i = 0; i< opts.length; i++){
                        $('#LotsAssc').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '-' + opts[i].Qty + '</option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });*/
    //});

    /*function addLot(){
        opcion = 7;
        id = parseInt($.trim($('#Lot').val()));
        CrgQty = parseInt($.trim($('#QtyLot').val()));
        TrkID = parseInt(TrkID);
        $.ajax({
            url: "bd/crudTrkv2.php",
            type: "POST",
            datatype:"json",
            data:  {id:id, TrkID:TrkID , CrgQty:CrgQty, opcion:opcion},    
            success: function(data){
                tablaTrk.ajax.reload(null, false);
                alert ("Actualizado con exito.");
                //tablaTrk.ajax.reload(null, false);
            },
            error: function(data) {
                alert('error');
            }
        });
    }*/

    //Agregar IDTruck a tabla de Lotes
    /*$(document).on("click", ".btnAdd", function(){
        addLot();
        update();
        /*opcion = 7;
        id = parseInt($.trim($('#Lot').val()));
        TrkID = parseInt(TrkID);
        //alert (id);
        //alert (TrkID);
        //DO = $.trim($('#DO').val());
        $.ajax({
              url: "bd/crudTrkv2.php",
              type: "POST",
              datatype:"json",
              data:  {id:id, TrkID:TrkID , opcion:opcion},    
              success: function(data){
                tablaTrk.ajax.reload(null, false);
                alert ("Actualizado con exito.");
                //tablaUsuarios.ajax.reload(null, false);
              },
            error: function(data) {
                alert('error');
            }
        });
    });*/
    
    $('#btnLimpiar').on('click', function(){
        tablaTrk
        .search( '' )
        .columns().search( '' )
        .draw();
        //tablaTrk
        //    .search("").draw();
        //$("#tablaTrk").DataTable().search('').draw();
        //$('#tablaTrk tfoot th').find('input[type=search]').val('');
        //$("#tablaTrk").DataTable().search('').draw();
    }); 

});

//Buscar lotes asociados a la DO
$(document).ready(function(){
    
    function LoadLots(){
            $("#LotsAssc").empty();
            $('#AsscLots').val("");
            $('#QtyDO').val("");
            $('#PWgh').val("");
            var opts = "";
            opcion = 6;
            DO = $.trim($('#DOrd').val());
            $.ajax({
                url: "bd/crudTrkv2.php",
                type: "POST",
                datatype:"json",
                data:  {DO:DO ,opcion:opcion},    
                success: function(data){
                    opts = JSON.parse(data);
                    $('#LotsAssc').append('<option value="">-Select Lot-</option disabled>');
                    for (var i = 0; i< opts.length; i++){
                        $('#LotsAssc').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '-' + opts[i].Qty + 'bc </option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });
    }
    
    function LoadReq(){
            $("#RqstID").empty();
            var opts = "";
            opcion = 15;
            //RqstID = $.trim($('#RqstID').val());
            DO = $.trim($('#DOrd').val());
            //alert (DO);
            $.ajax({
                url: "bd/crudTrkv2.php",
                type: "POST",
                datatype:"json",
                data:  { DO:DO, opcion:opcion },    
                success: function(data){
                    opts = JSON.parse(data);
                    $('#RqstID').append('<option value=0>- Select -</option disabled>');
                    for (var i = 0; i< opts.length; i++){
                        $('#RqstID').append('<option value="' + opts[i].ReqID + '">' + opts[i].ReqID + '</option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });
    }
    
    function update() {
        $("#TNam").empty();
        $('#OutRegDO').val("");
        $('#InRegDO').val("");
        $('#TypDO').val("");
        $('#Client').val("");
        var opts = "";
        opcion = 5;
        DO = $.trim($('#DOrd').val());
        $.ajax({
            url: "bd/crudTrkv2.php",
            type: "POST",
            datatype: "json",
            data: {
                DO: DO,
                opcion: opcion
            },
            success: function(data) {
               
                opts = JSON.parse(data);
                if(opts.length > 0){
                
                $('#btnGuardar').attr('disabled',false);

                $('#TNam').append('<option class="form-control" value="">- Select -</option>');
                for (var i = 0; i < opts.length; i++) {
                    $('#TNam').append('<option value="' + opts[i].TptID + '">' + opts[i].BnName + '</option>');
                }
                $('#OutRegDO').val(opts[0].PlcOut);
                $('#InRegDO').val(opts[0].RegIn);
                $('#TypDO').val(opts[0].Typ);
                $('#Client').val(opts[0].PlcIn);
                LoadLots();
                LoadReq();
                if ($('#TypDO').val()=="CON"){
                    $("#IrrDat").prop("disabled", true);
                }else{
                    $("#IrrDat").prop("disabled", false);
                }
            }else{
                $('#btnGuardar').attr('disabled',true);
                alert('non-existent Delivery Order, please verify your data.');
            }
            },
            error: function(data) {
                alert('error');
            }
        });
    }
    
    $("#DOrd").on("change", function(){
            update();
           /* LoadLots();
            LoadReq(); */
    }); 
    
    $('#btnCancel').on('click', function () {
        $('#btnGuardar').attr('disabled',false);
    });
    
    $(document).on("click", ".btnEditar", function(){
            fila = $(this).closest("tr");	        
            TrkID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID	
            $("#LotsAssc").empty();
            var opts = "";
            opcion = 12;
            DO = $.trim($('#DOrd').val());
            $.ajax({
                url: "bd/crudTrkv2.php",
                type: "POST",
                datatype:"json",
                data:  {TrkID:TrkID, DO:DO ,opcion:opcion},    
                success: function(data){
                    opts = JSON.parse(data);
                    $('#LotsAssc').append('<option value="">-Select Lot-</option>');
                    for (var i = 0; i< opts.length; i++){
                        $('#LotsAssc').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '-' + opts[i].Qty + 'bc </option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });
        
            //area para requisicion
            opcion = 15;
            //RqstID = $.trim($('#RqstID').val());
            DO = $.trim($('#DOrd').val());
            //alert (DO);
            $.ajax({
                url: "bd/crudTrkv2.php",
                type: "POST",
                datatype:"json",
                data:  { DO:DO, opcion:opcion },    
                success: function(data){
                    opts = JSON.parse(data);
                    $('#RqstID').append('<option value=0>- Select -</option disabled>');
                    for (var i = 0; i< opts.length; i++){
                        $('#RqstID').append('<option value="' + opts[i].ReqID + '">' + opts[i].ReqID + '</option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });
            //RqstID = fila.find('td:eq(24)').text();
            //alert(RqstID);
    });
        
});

//Funcion para asociar lotes a truck desde el formulario -- quitar se acciona el boton dentro del modal
$(document).ready(function(){
    
    /*$("#btnNuevo").click(function(){
        $("#LotsAssc").change(function(){
            opcion = 8;
            LotID = $.trim($('#LotsAssc').val());*/
//            var comboLots = document.getElementById("LotsAssc");
//            var selectedLots = comboLots.options[comboLots.selectedIndex].text;
//            var valNew=selectedLots.split('-');
//            var NewLot = valNew[0];
//            Lots = $.trim($('#AsscLots').val());
//            if (Lots == ""){
//                $('#AsscLots').val(NewLot);//(selectedLots);
//            }else{
//                concat = $.trim($('#AsscLots').val())+"|"+NewLot;//selectedLots;
//                $('#AsscLots').val(concat);
//            }
            /*$.ajax({
                    url: "bd/crudTrkv2.php",
                    type: "POST",
                    datatype:"json",
                    data:  {LotID:LotID , opcion:opcion},    
                    success: function(data){
                        opts = JSON.parse(data);
                        $('#QtyDO').val(opts[0].CrgQty);
                        $('#PWgh').val(opts[0].PWgh);
                        tam = opts.length;
                        //alert (tam);
                        //var concat = "";
                        if (tam > 1){*/
                            /*for(var i=1;i<=tam;i++){
                                concat += opts[i].Lot+"| ";//+concat;//alert(opts[i].Lot);
                            }
                            alert (concat);
                            $('#AsscLots').val(concat);*/
                            /*$('#AsscLots').val(opts[0].Lot+"|"+opts[1].Lot);
                        }else{
                            $('#AsscLots').val(opts[0].Lot);
                        }
                        //alert (opts);
                    },
                    error: function(data) {
                        alert('error');
                    }
                });
        });
    });
    */
    
    
    /*$(document).on("click", ".btnEditar", function(){
        fila = $(this).closest("tr");	        
        TrkID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID	
        
        $("#LotsAssc").change(function(){
            opcion = 9;
            LotID = $.trim($('#LotsAssc').val());*/
            /*var comboLots = document.getElementById("LotsAssc");
            var selectedLots = comboLots.options[comboLots.selectedIndex].text;
            var valNew=selectedLots.split('-');
            var NewLot = valNew[0];
            Lots = $.trim($('#AsscLots').val());
            if (Lots == ""){
                $('#AsscLots').val(selectedLots);//(NewLot);//(selectedLots);
            }else{
                concat = $.trim($('#AsscLots').val())+"|"+selectedLots;//NewLot;//selectedLots;
                $('#AsscLots').val(concat);
            }*/
            /*$.ajax({
                    url: "bd/crudTrkv2.php",
                    type: "POST",
                    datatype:"json",
                    data:  {TrkID:TrkID, LotID:LotID , opcion:opcion},    
                    success: function(data){
                        opts = JSON.parse(data);
                        $('#QtyDO').val(opts[0].CrgQty);
                        $('#PWgh').val(opts[0].PWgh);
                        tam = opts.length;
                        //alert (tam);
                        //var concat = "";
                        if (tam > 1){
                            $('#AsscLots').val(opts[0].Lot+"|"+opts[1].Lot);*/
                            /*for(var i=1;i<=tam;i++){
                                concat += opts[i].Lot+"| ";//+concat;//alert(opts[i].Lot);
                            }
                            alert (concat);
                            $('#AsscLots').val(concat);*/
                        /*}else{
                            $('#AsscLots').val(opts[0].Lot);
                        }
                        //alert (opts);
                    },
                    error: function(data) {
                        alert('error');
                    }
                });
        });
    });*/
    
});

//Lista de camiones a selecinar cuando se requiere editar un camion
$(document).ready(function(){
    $(document).on("click", ".btnEditar", function(){
            var opts = "";
            opcion = 5;
            DO = $.trim($('#DOrd').val());
            $.ajax({
                url: "bd/crudTrkv2.php",
                type: "POST",
                datatype:"json",
                data:  {DO:DO ,opcion:opcion},    
                success: function(data){
                    opts = JSON.parse(data);
                    $('#TNam').append('<option class="form-control" value="">- Select -</option>');
                    for (var i = 0; i< opts.length; i++){
                        $('#TNam').append('<option value="' + opts[i].TptID + '">' + opts[i].BnName + '</option>');
                    }
                },
                error: function(data) {
                    alert('error');
                }
            });
        });
});


//Funcion para agregar lotes con boton dentro del modal
$(document).ready(function(){
    
    $("#btnNuevo").click(function(){
        $("#btnAddLot").click(function(){
            opcion = 8;
            TrkID = $.trim($('#exampleModalLabel').val());
            LotID = $.trim($('#LotsAssc').val());
            $.ajax({
                    url: "bd/crudTrkv2.php",
                    type: "POST",
                    datatype:"json",
                    data:  {TrkID:TrkID, LotID:LotID , opcion:opcion},    
                    success: function(data){
                        opts = JSON.parse(data);
                        $('#QtyDO').val(opts[0].CrgQty);
                        $('#PWgh').val(opts[0].PWgh);
                        tam = opts.length;
                        Lotes = ""; 
                        for (var i=0; i<tam; i++){
                            Lotes = Lotes+""+opts[i].Lot+"|";//$('#AsscLots').val(opts[i].Lot+"|");
                        }
                        Lotes2 = Lotes.substring(0, Lotes.length -1);
                        $('#AsscLots').val(Lotes2);
                    },
                    error: function(data) {
                        alert('error');
                    }
                });
        });
    });
}); //funciones separadas edit y new "add lot"

$(document).ready(function(){    
    $(document).on("click", ".btnEditar", function(){
        fila = $(this).closest("tr");	        
        TrkID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID	
        $("#btnAddLot").click(function(){
            opcion = 9;
            LotID = $.trim($('#LotsAssc').val());
            $.ajax({
                    url: "bd/crudTrkv2.php",
                    type: "POST",
                    datatype:"json",
                    data:  {TrkID:TrkID, LotID:LotID , opcion:opcion},    
                    success: function(data){
                        opts = JSON.parse(data);
                        $('#QtyDO').val(opts[0].CrgQty);
                        $('#PWgh').val(opts[0].PWgh);
                        tam = opts.length;
                        Lotes = ""; 
                        for (var i=0; i<tam; i++){
                            Lotes = Lotes+""+opts[i].Lot+"|";//$('#AsscLots').val(opts[i].Lot+"|");
                        }
                        Lotes2 = Lotes.substring(0, Lotes.length -1);
                        $('#AsscLots').val(Lotes2);
                    },
                    error: function(data) {
                        alert('error');
                    }
                });
        });
    });
    
});

//Funcion eliminar lotes con boton dentro del modal
$(document).ready(function(){
    
    $("#btnNuevo").click(function(){
        $("#btnDelLot").click(function(){
            LotID = $.trim($('#LotsAssc').val());
            opcion = 11;
            $.ajax({
                url: "bd/crudTrkv2.php",
                type: "POST",
                datatype:"json",    
                data:  {TrkID:TrkID, LotID:LotID, opcion:opcion},    
                success: function(data) {
                    opts = JSON.parse(data);
                        //$('#QtyDO').val(opts[0].CrgQty);
                        //$('#PWgh').val(opts[0].PWgh);
                        tam = opts.length;
                        Lotes = ""; 
                        for (var i=0; i<tam; i++){
                            Lotes = Lotes+""+opts[i].Lot+"|";//$('#AsscLots').val(opts[i].Lot+"|");
                        }
                        Lotes2 = Lotes.substring(0, Lotes.length -1);
                        $('#AsscLots').val(Lotes2);
                        if (!opts.length){
                            $('#QtyDO').val("0");
                            $('#PWgh').val("0");
                        }else{
                        $('#QtyDO').val(opts[0].CrgQty);
                        $('#PWgh').val(opts[0].PWgh);
                        }
                        /*if (tam < 1){
                            $('#AsscLots').val("");
                            $('#QtyDO').val("0");
                            $('#PWgh').val("0");
                        }else if (tam > 1){
                            $('#AsscLots').val(opts[0].Lot+"|"+opts[1].Lot);
                            $('#QtyDO').val(opts[0].CrgQty);
                            $('#PWgh').val(opts[0].PWgh);
                        }else{
                            $('#AsscLots').val(opts[0].Lot);
                            $('#QtyDO').val(opts[0].CrgQty);
                            $('#PWgh').val(opts[0].PWgh);
                        }*/
                    },
                    error: function(data) {
                        alert('error');
                    }                 
            });
        });
    });
    
    
    $(document).on("click", ".btnEditar", function(){
        fila = $(this).closest("tr");	        
        TrkID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID	
        
        $("#btnDelLot").click(function(){
            opcion = 10;
            LotID = $.trim($('#LotsAssc').val());
            $.ajax({
                    url: "bd/crudTrkv2.php",
                    type: "POST",
                    datatype:"json",
                    data:  {TrkID:TrkID, LotID:LotID , opcion:opcion},    
                    success: function(data){
                        opts = JSON.parse(data);
                        //$('#QtyDO').val(opts[0].CrgQty);
                        //$('#PWgh').val(opts[0].PWgh);
                        tam = opts.length;
                        Lotes = ""; 
                        for (var i=0; i<tam; i++){
                            Lotes = Lotes+""+opts[i].Lot+"|";//$('#AsscLots').val(opts[i].Lot+"|");
                        }
                        Lotes2 = Lotes.substring(0, Lotes.length -1);
                        $('#AsscLots').val(Lotes2);
                        if (!opts.length){
                            $('#QtyDO').val("0");
                            $('#PWgh').val("0");
                        }else{
                        $('#QtyDO').val(opts[0].CrgQty);
                        $('#PWgh').val(opts[0].PWgh);
                        }
                        /*if (tam < 1){
                            $('#AsscLots').val("");
                            $('#QtyDO').val("0");
                            $('#PWgh').val("0");
                        }else if (tam > 1){
                            $('#AsscLots').val(opts[0].Lot+"|"+opts[1].Lot);
                            $('#QtyDO').val(opts[0].CrgQty);
                            $('#PWgh').val(opts[0].PWgh);
                        }else{
                            $('#AsscLots').val(opts[0].Lot);
                            $('#QtyDO').val(opts[0].CrgQty);
                            $('#PWgh').val(opts[0].PWgh);
                        }*/
                    },
                    error: function(data) {
                        alert('error');
                    }
                });
        });
    });
    
});


//Cargar Region
function LoadRegion(){
    $("#LocSplit").empty();   
    var opts = "";
    opcion = 5;
    $.ajax({
          url: "bd/crudDO.php",
          type: "POST",
          datatype:"json",
          data:  {opcion:opcion},    
          success: function(data){
              opts = JSON.parse(data);
              //$('#OutPlc').append('<option class="form-control" >- Select -</option>');
              //$('#InReg').append('<option class="form-control" >- Select -</option>');
              for (var i = 0; i< opts.length; i++){
                  $('#LocSplit').append('<option value="' + opts[i].RegNam + '">' + opts[i].RegNam + '</option>');
                  //$('#InReg').append('<option value="' + opts[i].RegNam + '">' + opts[i].RegNam + '</option>');
              }
           },
        error: function(data) {
            alert('error');
        }
    });
}


//Funcion para buscar lotes para realizar el split  
$(document).ready(function(){
    
    //$("#btnSplit").click(function(){
        $("#btnFndLot").click(function(){
            $("#LotsSplit").empty();
            $("#Split").val("");
            opcion = 13;
            LocSplit = $.trim($('#LocSplit').val());
            DOSplit = $.trim($('#DOSplit').val());
            $.ajax({
                    url: "bd/crudTrkv2.php",
                    type: "POST",
                    datatype:"json",
                    data:  {LocSplit:LocSplit, DOSplit:DOSplit, opcion:opcion},    
                    success: function(data){
                        opts = JSON.parse(data);
                        $('#LotsSplit').append('<option class="form-control" value="" disabled selected>- Select -</option>');
                        for (var i = 0; i< opts.length; i++){
                            $('#LotsSplit').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + ' - ' + opts[i].Qty + '</option>');
                        }
                    },
                    error: function(data) {
                        alert('error');
                    }
                });
        });
    //});
});



//Validar Pesos
$(document).ready(function(){
    //Validar peso salida
    function ValidateOutWgh(){
        PWgh = parseInt($.trim($('#PWgh').val()));
        OutWgh = parseInt($.trim($('#OutWgh').val()));
        Qty = parseInt($.trim($('#QtyDO').val()));
        if (PWgh == 0 || PWgh == ""){
            $("#NoPWgh").show();
        }else{
            $("#NoPWgh").hide();
            if (OutWgh<PWgh){
                $("#OutWghMin").show();
            }else{
                $("#OutWghMin").hide();
            }if (OutWgh>(PWgh+(2*Qty))){
                $("#OutWghMax").show();
            }else{
                $("#OutWghMax").hide();
            }
        }
    }
    //Validar Peso entrada
    function ValidateInWgh(){
        OutWgh = parseInt($.trim($('#OutWgh').val()));
        InWgh = parseInt($.trim($('#InWgh').val()));
        if (OutWgh == 0 || PWgh == ""){
            $("#NoWgh").show();
        }else{
            $("#NoWgh").hide();
            Qty = parseInt($.trim($('#QtyDO').val()));
            if (InWgh<OutWgh){
                $("#InWghMin").show();
            }else{
                $("#InWghMin").hide();
            }if (InWgh>(PWgh+(0.5*Qty))){
                $("#InWghMax").show();
            }else{
                $("#InWghMax").hide();
            }
        }
    }
    $("#OutWgh").change(function(){
        ValidateOutWgh();
    });
    
    $("#InWgh").change(function(){
        ValidateInWgh();
    });
});

$(document).ready(function(){
$(document).on("click", ".btnPDF-REM", function(){
    fila = $(this);           
    TrkID = $(this).closest('tr').find('td:eq(0)').text();
    TrkDO = $(this).closest('tr').find('td:eq(1)').text();
    TrkTyp = $(this).closest('tr').find('td:eq(2)').text();
    Reg = $(this).closest('tr').find('td:eq(15)').text();
    Cli = $(this).closest('tr').find('td:eq(17)').text();
    window.open("./bd/PDFREM.php?TrkID="+TrkID+"&TrkDO="+TrkDO+"&TrkTyp="+TrkTyp+"&Reg="+Reg+"&Cli="+Cli+"");
    tablaTrk.ajax.reload(null, false);
});
});

$(document).ready(function(){
$(document).on("click", ".btnTag", function(){
    fila = $(this);
    TrkID = $(this).closest('tr').find('td:eq(0)').text();
    TrkDO = $(this).closest('tr').find('td:eq(1)').text();
    TrkTyp = $(this).closest('tr').find('td:eq(2)').text();
    TrkQty = $(this).closest('tr').find('td:eq(4)').text();
    Gin = $(this).closest('tr').find('td:eq(13)').text();
    DepReg = $(this).closest('tr').find('td:eq(14)').text();
    Reg = $(this).closest('tr').find('td:eq(15)').text();
    Cli = $(this).closest('tr').find('td:eq(17)').text();
    window.open("./bd/CartaPorte.php?TrkID="+TrkID+"&TrkDO="+TrkDO+"&TrkTyp="+TrkTyp+"&TrkQty="+TrkQty+"&Gin="+Gin+"&Reg="+Reg+"&DepReg="+DepReg+"&Cli="+Cli+"");
    tablaTrk.ajax.reload(null, false);
});
});

$(document).ready(function(){
$(document).on("click", ".export", function(){
    window.open("./bd/export.php");
    tablaTrk.ajax.reload(null, false);
});
});

$(document).ready(function(){
    $(document).on("click", ".btnEditar", function(){
            CrgQty = $.trim($('#QtyDO').val());
            RqstID = $.trim($('#RqstID').val());
            if (CrgQty != 0 || RqstID !=0) {
                $("#DOrd").prop("disabled", true);
            } else {
                $("#DOrd").prop("disabled", false);
            }
    })
});

//Validar estatus del camion sin lotes asociados
$( function() {
    $("#Status").change( function() {
        CrgQty = $.trim($('#QtyDO').val());
        if ($(this).val() === "Transit" && CrgQty === "") {
            alert ("You cannot change the status without associated Lots");
            $("#Status").val("Programmed");
        } else if ($(this).val() === "Received" && CrgQty === "") {
            alert ("You cannot change the status without associated Lots");
            $("#Status").val("Programmed");
        }
    });

    // Boton eliminar PO's and PR's

    $("#btnPOPR").click(function(){
        $("#POPR").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $('#modalPOPR').modal('show');
    });

    // botón filtros

$("#filtraboton").click(function(){
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    DepArrDate = $('#DepArrDate').val();
    if(fromdate || todate || DepArrDate){
        $('#fromdate').attr('disabled', false);
        $('#todate').attr('disabled', false);
        $('#timeSelect').attr('disabled', false);
    }else{
        $('#fromdate').attr('disabled', true);
        $('#todate').attr('disabled', true);
        $('#timeSelect').attr('disabled', true);
    }

    $('#filtrarmodal').modal('show');
});

$('#DepArrDate').on('change keyup',function(){
    DepArrDate = $('#DepArrDate').val();


    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    muestras = $('select[name=muestras] option').filter(':selected').val();
    muestratext = $( "#muestras option:selected" ).text();
    DepRegFil = $('select[name=DepRegFil] option').filter(':selected').val();
    ArrRegFil = $('select[name=ArrRegFil] option').filter(':selected').val();
    timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
    idGlobal = $('#idDO').val();
    asLots = $('#asLots').val();
    if(DepArrDate){
        $('#fromdate').attr('disabled', false);
        $('#todate').attr('disabled', false);
        $('#timeSelect').attr('disabled', false);
        $('#muestras').attr('disabled',true);
        if(fromdate || todate)
            $('#buscafiltro').attr('disabled',false);
        else
            $('#buscafiltro').attr('disabled',true);
    }else{
        $('#fromdate').attr('disabled', true);
        $('#todate').attr('disabled', true);
        $('#timeSelect').attr('disabled', true);
        if(idGlobal || asLots || DepRegFil || ArrRegFil)
            $('#muestras').attr('disabled',true);
        else    
            $('#muestras').attr('disabled',false);
        $('#fromdate').val("");
        $('#todate').val("");
        if(muestratext != "200 (default)" || DepRegFil || ArrRegFil ||idGlobal || asLots ){
            $('#buscafiltro').attr('disabled',false);
        }else
            $('#buscafiltro').attr('disabled',true);
        $('#timeSelect').prop('selectedIndex',0);
    }
});

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

    $('#fromdate, #todate').on('change keyup', function () {
        DepArrDate = $('#DepArrDate').val();


        fromdate = $('#fromdate').val();
        todate = $('#todate').val();
        muestras = $('select[name=muestras] option').filter(':selected').val();
        muestratext = $( "#muestras option:selected" ).text();
        DepRegFil = $('select[name=DepRegFil] option').filter(':selected').val();
        ArrRegFil = $('select[name=ArrRegFil] option').filter(':selected').val();
        timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
        idGlobal = $('#idDO').val();
        asLots = $('#asLots').val();

        if(fromdate || todate){
            $('#buscafiltro').attr('disabled', false);
        }else if(DepArrDate){
            $('#timeSelect').prop('selectedIndex',0);
            $('#buscafiltro').attr('disabled', true);
        }else if(muestratext != "200 (default)" || DepRegFil || ArrRegFil ||idGlobal || asLots){
            $('#buscafiltro').attr('disabled', false);
        } else $('#buscafiltro').attr('disabled', true);
    });

$('#timeSelect').on('change', function () {
    timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
    if(timeSelect){
    timeSelect = parseInt(timeSelect);
    let today = new Date();
    //today = today.toLocaleTimeString('es-MX');
    let dateEnMilisegundos = 1000 * 60* 60* 24* timeSelect;
    let suma = today.getTime() - dateEnMilisegundos;
    let fechainicial = new Date(suma);
    //console.log(today);
    datetoday =  formatDate(today);
    datetosearch = formatDate(fechainicial);
    $('#fromdate').val(datetosearch);
    $('#todate').val(datetoday);
    $('#buscafiltro').attr('disabled',false);
    $
    }else{
        $('#fromdate').val("");
        $('#todate').val("");
        $('#buscafiltro').attr('disabled',true);
    }

    //console.log(datetoday); //today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()
});

// Si cambia el contenido de los input se habilita o bloquea el botón de apply 
$('#muestras,#DepRegFil,#ArrRegFil,#idDO,#asLots').on('change keyup',function(){
    DepArrDate = $('select[name=DepArrDate] option').filter(':selected').val();
    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    muestras = $('select[name=muestras] option').filter(':selected').val();
    muestratext = $( "#muestras option:selected" ).text();
    DepRegFil = $('select[name=DepRegFil] option').filter(':selected').val();
    ArrRegFil = $('select[name=ArrRegFil] option').filter(':selected').val();
    timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
    idGlobal = $('#idDO').val();
    asLots = $('#asLots').val();

    
    if( muestratext != "200 (default)" || DepRegFil || ArrRegFil || idGlobal || asLots){
        if(DepArrDate && fromdate || todate)
             $('#buscafiltro').attr('disabled', false);
        else 
            if(DepArrDate == "")
                $('#buscafiltro').attr('disabled', false);
            else
                $('#buscafiltro').attr('disabled', true);
    }else {
        if(DepArrDate && fromdate || todate)
             $('#buscafiltro').attr('disabled', false);
        else
            $('#buscafiltro').attr('disabled', true);
    }
});


$('#idDO, #asLots').on('change keyup', function(){
    idGlobal = $('#idDO').val();
    asLots = $('#asLots').val();
    if(idGlobal || asLots){
        $('#DepRegFil').attr('disabled', true);
        $('#ArrRegFil').attr('disabled', true);
        $('#muestras').attr('disabled', true);
        $('#muestras').prop('selectedIndex',0);
        $('#DepRegFil').prop('selectedIndex',0);
        $('#ArrRegFil').prop('selectedIndex',0);
    }else{
        $('#DepRegFil').attr('disabled', false);
        $('#ArrRegFil').attr('disabled', false);
        $('#muestras').attr('disabled', false);
    }
});

$('#DepRegFil,#ArrRegFil').on('change keyup', function(){
    DepRegFil = $('#DepRegFil').val();
    ArrRegFil= $('#ArrRegFil').val();
    DepArrDate = $('select[name=DepArrDate] option').filter(':selected').val();
    if(DepRegFil || ArrRegFil){
        $('#idDO').attr('disabled', true);
        $('#asLots').attr('disabled', true);
        $('#muestras').attr('disabled', true);
        $('#muestras').prop('selectedIndex',0);
        $('#idDO').val("");
        $('#asLots').val("");
         if(DepArrDate == ""){
            $('#DepArrDate').prop('selectedIndex',1);
            $('#timeSelect').attr('disabled', false);
            $('#timeSelect').prop('selectedIndex',3);
            timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
            timeSelect = parseInt(timeSelect);
            let today = new Date();
            //today = today.toLocaleTimeString('es-MX');
            let dateEnMilisegundos = 1000 * 60* 60* 24* timeSelect;
            let suma = today.getTime() - dateEnMilisegundos;
            let fechainicial = new Date(suma);
            //console.log(today);
            datetoday =  formatDate(today);
            datetosearch = formatDate(fechainicial);
            $('#fromdate').val(datetosearch);
            $('#fromdate').attr('disabled', false);
            $('#todate').attr('disabled',false);
            $('#todate').val(datetoday);
            $('#buscafiltro').attr('disabled',false);
        }
    }else{
        if(DepArrDate){
            $('#muestras').attr('disabled', true);
        }else
            $('#muestras').attr('disabled', false);
        $('#idDO').attr('disabled', false);
        $('#asLots').attr('disabled', false);
        
    }
});

//Last trucks deshabilita todo
$('#muestras').on('change', function () {
    muestras = $('select[name=muestras] option').filter(':selected').val();
    muestratext = $( "#muestras option:selected" ).text();
    if(muestratext != "200 (default)"){
        $('#idDO').attr('disabled', true);
        $('#asLots').attr('disabled', true);
        $('#DepRegFil').attr('disabled', true);
        $('#ArrRegFil').attr('disabled', true);
        $('#DepArrDate').attr('disabled', true);
        $('#fromdate').attr('disabled', true);
        $('#todate').attr('disabled', true);
        $('#timeSelect').attr('disabled', true);
        $('#fromdate').val("");
        $('#todate').val("");
        $('#timeSelect').val("");
        $('#timeSelect').prop('selectedIndex',0);
        $('#DepArrDate').prop('selectedIndex',0);
        $('#DepRegFil').prop('selectedIndex',0);
        $('#ArrRegFil').prop('selectedIndex',0);
        $('#idDO').val("");
        $('#asLots').val("");
    }else{
        $('#idDO').attr('disabled', false);
        $('#asLots').attr('disabled', false);
        $('#DepRegFil').attr('disabled', false);
        $('#ArrRegFil').attr('disabled', false);
        $('#DepArrDate').attr('disabled', false);
      

    }
});


//id trucks deshabilita todo
$('#idTrk').on('change', function () {
    idTrk = $('#idTrk').val();
    if(idTrk != ""){
        $('#muestras').attr('disabled', true);
        $('#muestras').prop('selectedIndex',0);
        $('#idDO').attr('disabled', true);
        $('#asLots').attr('disabled', true);
        $('#DepRegFil').attr('disabled', true);
        $('#ArrRegFil').attr('disabled', true);
        $('#DepArrDate').attr('disabled', true);
        $('#fromdate').attr('disabled', true);
        $('#todate').attr('disabled', true);
        $('#timeSelect').attr('disabled', true);
        $('#fromdate').val("");
        $('#todate').val("");
        $('#timeSelect').val("");
        $('#timeSelect').prop('selectedIndex',0);
        $('#DepArrDate').prop('selectedIndex',0);
        $('#DepRegFil').prop('selectedIndex',0);
        $('#ArrRegFil').prop('selectedIndex',0);
        $('#idDO').val("");
        $('#asLots').val("");
        $('#buscafiltro').attr('disabled',false);
    }else{
        $('#muestras').attr('disabled', false);
        $('#idDO').attr('disabled', false);
        $('#asLots').attr('disabled', false);
        $('#DepRegFil').attr('disabled', false);
        $('#ArrRegFil').attr('disabled', false);
        $('#DepArrDate').attr('disabled', false);
      

    }
});



var botonglobal = 0;

$("#buscafiltro, #borrarFiltro, #cer").click(function(){
    boton = $(this).val();
    if(boton == 1 || boton == 0)
        botonglobal = boton;
    if(boton != 3){
  //  muestra = Number.isNaN($('#muestras').val()) ? " " : $('#muestras').val();
    muestras =  $('select[name=muestras] option').filter(':selected').val();
    DepRegFil = $('select[name=DepRegFil] option').filter(':selected').val();
    ArrRegFil = $('select[name=ArrRegFil] option').filter(':selected').val();
    DepArrDate = $('select[name=DepArrDate] option').filter(':selected').val();
    idGlobal = $('#idDO').val();
    
    asLots = $('#asLots').val();
    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    
    idTrk = $('#idTrk').val();
    
    //filters = {"muestras":muestras,"regionfil":regionfil};
    opcion = 4;
    var applyFilter =  $.ajax({
        type: 'POST',
        url: 'bd/assignFilters.php',
        data: {
            boton: boton,
            idGlobal:idGlobal,
            asLots: asLots,
            muestras: muestras,
            DepRegFil: DepRegFil,
            ArrRegFil: ArrRegFil,
            DepArrDate: DepArrDate,
            fromdate: fromdate,
            todate: todate,
            idTrk: idTrk 
        }
        
    }) 
    applyFilter.done(function(data){

       // console.log(data);
        tablaTrk.ajax.reload(function(){

            if(boton == 1){
                $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Filtros asignados con éxito</div>')
                setTimeout(function() {
                    $('#alert1').fadeOut(1000);
                    $('#filtrarmodal').modal('hide');
                },1000);
            }
            else{
                // Habilitar botones que pueden estar deshabilitados
                $('#DepRegFil').attr('disabled', false);
                $('#ArrRegFil').attr('disabled', false);
                $('#muestras').attr('disabled', false);
                $('#asLots').attr('disabled', false);
                $('#idDO').attr('disabled', false);
                $('#DepArrDate').attr('disabled', false);
                $('#timeSelect').attr('disabled', true);
                $('#fromdate').attr('disabled', true);
                $('#todate').attr('disabled', true);
    
                $('#muestras').prop('selectedIndex',0);
                $('#DepRegFil').prop('selectedIndex',0);
                $('#ArrRegFil').prop('selectedIndex',0);
                $('#DepArrDate').prop('selectedIndex',0);
                $('#timeSelect').prop('selectedIndex',0);
                $('#idTrk').val("");
                $('#idDO').val("");
                $('#fromdate').val("");
                $('#todate').val("");
                $('#asLots').val("");
                $('#buscafiltro').prop('disabled', true);
                $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"> <i class="bi bi-trash-fill"></i> Filtros borrados con éxito</div>')
                setTimeout(function() {
                    $('#alert1').fadeOut(1500);
                },1500);
            }

        });
        $('#aviso').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');      
    });  
}else{
    if(botonglobal == 0){
        $('#DepRegFil').attr('disabled', false);
        $('#ArrRegFil').attr('disabled', false);
        $('#muestras').attr('disabled', false);
        $('#asLots').attr('disabled', false);
        $('#idDO').attr('disabled', false);
        $('#DepArrDate').attr('disabled', false);
        $('#timeSelect').attr('disabled', true);
        $('#fromdate').attr('disabled', true);
        $('#todate').attr('disabled', true);

        $('#muestras').prop('selectedIndex',0);
        $('#DepRegFil').prop('selectedIndex',0);
        $('#ArrRegFil').prop('selectedIndex',0);
        $('#DepArrDate').prop('selectedIndex',0);
        $('#timeSelect').prop('selectedIndex',0);
        $('#idTrk').val("");
        $('#idDO').val("");
        $('#fromdate').val("");
        $('#todate').val("");
        $('#asLots').val("");
        $('#buscafiltro').prop('disabled', true);
    }}
});

///Boton trucks in transit

$('#transit').on("click", function(){
    window.open("./bd/pdfTransit2.php");
    tablaTrk.ajax.reload(null, false);
});

// Habilitar edicion Costo de flete
/*
$('#btnFreightC').on("click", function(){
    $("#FreightC").prop("disabled", false);
});
*/
// asignar mismo valor a Departure y Arrival Date REAL
$('#SchOutDat, #SchInDat').on('change', function () {
    SchOutDat = $.trim($('#SchOutDat').val());
    SchInDat = $.trim($('#SchInDat').val());
    $("#OutDat").val(SchOutDat);
    $("#InDat").val(SchInDat);
});

// Buscar info de TrkID del boton para remover PO's y PR's
$('#RTrkID').on('keyup', function () {
    RTrkID = $.trim($('#RTrkID').val());
    var Info = $.ajax({ //verificamos que exista el transport o no en la bd
        type: "POST",
        url: "./bd/correo-model.php?m=getInfTrkID", //hago referencia al archivo correo model.php y le mando el método getLastPrice
        data: {
            RTrkID: RTrkID // aquí igual se pordría pasar el parámetro m 
        }
    });

    Info.done(function(d){
        if (d == "false"){
            $('#avisoPOPR').html('<div id="alert1" class="alert alert-warning" style="width: 12rem; height: 3rem;" role="alert"><i class="bi bi-x-square-fill"></i> TrkID no existe </div>')
            setTimeout(function() {
                $('#alert1').fadeOut(1000);
            },1000);
            $('#RDO').val("");
            $('#RTyp').val("");
            $('#RPR').val("");
            $('#RPO').val("");
            $('#RLots').val("");
            $('#RTNam').val("");
            $('#RemPOPR').prop('disabled', true);
        }else{
            data = JSON.parse(d);
            $('#RDO').val(data['DO']);
            $('#RPR').val(data['RqstID']);
            $('#RPO').val(data['PO']);
            $('#RLots').val(data['LotsAssc']);
            $('#RTNam').val(data['TNam']);
            $('#RTyp').val(data['Typ']);
            $('#RemPOPR').prop('disabled', false);
        }
    });

});

// habilitar y deshabilitar botones para asociar lotes
$('#LotsAssc').on('change', function () {
    LotID = $.trim($('#LotsAssc').val());
    var Info = $.ajax({ //verificamos que exista el transport o no en la bd
        type: "POST",
        url: "./bd/correo-model.php?m=getTrkID", //hago referencia al archivo correo model.php y le mando el método getLastPrice
        data: {
            LotID: LotID // aquí igual se pordría pasar el parámetro m 
        }
    });

    Info.done(function(d){
        data = JSON.parse(d);
        TrkIDL = data['TrkID'];
        if (TrkIDL == 0){
            $('#btnAddLot').prop('disabled', false);
            $('#btnDelLot').prop('disabled', true);
        }else{
            $('#btnAddLot').prop('disabled', true);
            $('#btnDelLot').prop('disabled', false);
        }
    });

});


// habilitar y deshabilitar botones segun la region del usuario
$(document).on("click", ".btnEditar", function(){
    $("#InData :input").prop("disabled", false);
    fila = $(this).closest("tr");
    usuario = $.trim($('#dropdownMenuLink2').val());
    Salida = fila.find('td:eq(14)').text();
    Entrada = fila.find('td:eq(15)').text();
    Status = fila.find('td:eq(33)').text();
    Priv = $.trim($('#Priv').val());
    var Info = $.ajax({ //verificamos que exista el transport o no en la bd
        type: "POST",
        url: "./bd/correo-model.php?m=getPrivReg", //hago referencia al archivo correo model.php y le mando el método getLastPrice
        data: {
            Priv: Priv // aquí igual se pordría pasar el parámetro m 
        }
    });

    Info.done(function(d){
            data = JSON.parse(d);
            StrReg = [];
            for (i=0; i<data.length; i++){
                StrReg[i] = data[i].RegNam;
            }
            S = jQuery.inArray( Salida, StrReg );
            E = jQuery.inArray( Entrada, StrReg );
            
            if (Status == "Programmed"){
                $("#SchOutDat, #SchOutTime, #OutDat, #OutTime, #OutWgh, #SchInDat, #SchInTime, #InDat, #InTime, #InWgh").prop("disabled", false);
            }else if (S == -1 && E != -1){
                $('#SchOutDat').prop('disabled', true);
                $('#SchOutTime').prop('disabled', true);
                $('#OutDat').prop('disabled', true);
                $('#OutTime').prop('disabled', true);
                $('#OutWgh').prop('disabled', true);
                $('#SchInDat').prop('disabled', false);
                $('#SchInTime').prop('disabled', false);
                $('#InDat').prop('disabled', false);
                $('#InTime').prop('disabled', false);
                $('#InWgh').prop('disabled', false);
                // bloquer botón tarifario solo para entradas
                $('#btntarifario').prop('disabled', false);
                
              

            }else if (S != -1 && E == -1){
                $('#SchOutDat').prop('disabled', false);
                $('#SchOutTime').prop('disabled', false);
                $('#OutDat').prop('disabled', false);
                $('#OutTime').prop('disabled', false);
                $('#OutWgh').prop('disabled', false);
                $('#SchInDat').prop('disabled', true);
                $('#SchInTime').prop('disabled', true);
                $('#InDat').prop('disabled', true);
                $('#InTime').prop('disabled', true);
                $('#InWgh').prop('disabled', true);
                //desbloquear  botón tarifario solo para salidas
                $('#btntarifario').prop('disabled', false);
            }else if (S == -1 && E == -1 && usuario != "IVAN FELIX"){
                $('#SchOutDat').prop('disabled', true);
                $('#SchOutTime').prop('disabled', true);
                $('#OutDat').prop('disabled', true);
                $('#OutTime').prop('disabled', true);
                $('#OutWgh').prop('disabled', true);
                $('#SchInDat').prop('disabled', true);
                $('#SchInTime').prop('disabled', true);
                $('#InDat').prop('disabled', true);
                $('#InTime').prop('disabled', true);
                $('#InWgh').prop('disabled', true);
                //deshabilitar botones
                $('#btnAddLot').prop('disabled', true);
                $('#btnDelLot').prop('disabled', true);
               // $('#btnFreightC').prop('disabled', true);
                $('#divAsociar').hide();
                 //bloquer botón tarifario
                $('#btntarifario').prop('disabled', true);
            }else if (S == -1 && E == -1 && usuario == "IVAN FELIX"){
                $("#InData :input").prop("disabled", true);
                $('#IrrDat').prop('disabled', false);
            }else if (S != -1 && E != -1){
                $('#SchOutDat').prop('disabled', false);
                $('#SchOutTime').prop('disabled', false);
                $('#OutDat').prop('disabled', false);
                $('#OutTime').prop('disabled', false);
                $('#OutWgh').prop('disabled', false);
                $('#SchInDat').prop('disabled', false);
                $('#SchInTime').prop('disabled', false);
                $('#InDat').prop('disabled', false);
                $('#InTime').prop('disabled', false);
                $('#InWgh').prop('disabled', false);
                 //desbloquear bloquer botón tarifario
                $('#btntarifario').prop('disabled', false);
            }
    });

});

// formato miles (#,###) de manera automatica
$("#OutWgh, #InWgh").on({
    "focus": function(event) {
      $(event.target).select();
    },
    "keyup": function(event) {
      $(event.target).val(function(index, value) {
        return value.replace(/\D/g, "")
          .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
      });
    }
  });

   //formato xxx-xxx-xxxx automatico
  $("#DrvTel").on({
    "focus": function(event) {
      $(event.target).select();
    },
    "keyup": function(event) {
      $(event.target).val(function(index, value) {
        return value.replace(/\D/g, "")
          //.replace(/([0-9])([0-9]{2})$/, '$1.$2')
          .replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/g, '$1-$2-$3')
      });
    }
  });


});


// VER LA REMISION SUBIDA A AWS S3 
$(document).ready(function(){
   
    $(document).on("click", ".btnremision", function(){
        fila = $(this).closest("tr");	        
        truck =  fila.find('td:eq(0)').text(); //capturo el ID
        upload = parseInt(fila.find('td:eq(36)').text());// bandera para saber si la remision ya se encuentra almasenada en AWS S3
  
      
        if(upload == 1){
            window.open("opens3.php?TrkID="+truck);
          
        } else {
            alert('The referral has not yet been uploaded to the server.');	
        }

            
        });



       
        

     
});

function upfile(){
    
    var filename = $("#file").val();
  //  console.log(filename);
    //si es null muestra un mensaje de error
    if(filename == null)
        alert('No ha seleccionado una imagen');
        
    else{// si se eligio un archivo correcto obtiene la extension para vlidarla
        var extension = filename.replace(/^.*\./, '');               
        $('#loadfile').prop('disabled', false);
        $('#update').prop('disabled', false);
        if (extension == filename)
            extension = '';
        else{
            extension = extension.toLowerCase();
          
            //aqui puedes incluir todas las extensiones que quieres permitir
            if(extension != 'pdf' ){
                alert("extencion no valida");
            }
    }
    }


}

/////////AGREGAR DATOS A PREVIEW MAIL///////////////////     
$(document).ready(function(){


    


    


    $(document).on("click", ".previewEmail", function (e) {

        e.preventDefault();
        $('#comentarios').val("");
        $('#link').val("");
       var fila = $(this);
       var usuario = $('#dropdownMenuLink2').text();
       var delivery = $(this).closest('tr').find('td:eq(1)').text();
       var typeDO = $(this).closest('tr').find('td:eq(2)').text();
       var trkid = $(this).closest('tr').find('td:eq(0)').text();
       var qty = $(this).closest('tr').find('td:eq(4)').text();
       var regsalida = $(this).closest('tr').find('td:eq(14)').text();
       var regarribo = $(this).closest('tr').find('td:eq(15)').text();
       var fechasalida = $(this).closest('tr').find('td:eq(6)').text();
       var horasalida = $(this).closest('tr').find('td:eq(8)').text();
       var fechallegada = $(this).closest('tr').find('td:eq(10)').text();
       var horallegada = $(this).closest('tr').find('td:eq(12)').text();
       var transporte = $(this).closest('tr').find('td:eq(18)').text();
       var talon = $(this).closest('tr').find('td:eq(19)').text();
       var truckplate = $(this).closest('tr').find('td:eq(21)').text();
       var cajaplate = $(this).closest('tr').find('td:eq(22)').text();
       var chofer = $(this).closest('tr').find('td:eq(23)').text();
       var telchofer = $(this).closest('tr').find('td:eq(24)').text();
       var client = $(this).closest('tr').find('td:eq(17)').text();
       var regarrival = $(this).closest('tr').find('td:eq(15)').text();
       var cartaporte = parseInt(fila.find('td:eq(36)').text());
      // uso el idioma en español para la fecha
       moment.locale('es');       
       var dateTime =moment(fechallegada);
       var dateTime2 =moment(fechasalida);
       var dateTime3 =moment(fechasalida);
     // formato de fecha miercoles 1, junio 2016
      fecha_descarga = dateTime.format('dddd D, MMMM YYYY');
      fecha_carga = dateTime2.format('dddd D, MMMM YYYY');
      fecha_asunto = dateTime3.format('DD/MM');


      
        var cliente = $(this).closest('tr').find('td:eq(17)').text();
      

       var Info = $.ajax({ //verificamos que exista el transport o no en la bd
        type: "POST",
        url: "bd/getCorreo-truck.php",
            data: {
                   usuario:usuario, 
                   delivery:delivery,
                   typeDO:typeDO, 
                   trkid:trkid, 
                   qty:qty, 
                   regsalida:regsalida, 
                   regarribo:regarribo, 
                   fechasalida:fechasalida,
                   fechallegada:fechallegada,
                   transporte:transporte,
                   cliente:cliente                
            }, success: function(data){
                datos = JSON.parse(data);
                $(".modal-header").css( "background-color", "#17562c");
                $(".modal-header").css( "color", "white" );
               // const resultado = Object.keys(datos)
               // console.log(resultado);
               // console.log(datos['descarga']);
                //const resultado2 = Object.keys(datos['descarga']['0']);
                descarga = datos['descarga']['0'].Drctn;
                regdescargar = datos['descarga']['0'].BnName;
                refdescarga = datos['descarga']['0'].Ref;
                Maps = datos['descarga']['0'].Maps;

                carga = datos['lugarcarga']['0'].BnName;
                dircarga = datos['lugarcarga']['0'].Drctn;
                refcarga = datos['lugarcarga']['0'].Ref;
                asunto_reg = datos['lugarcarga']['0'].nombre;
                mailsorigen = datos['mailorigen']['0'].mails;
                mailtransporte = datos['mailtransport']['0'].mails;
                mailamsa= datos['mailamsa']['0'].mails;

               // console.log(mailsorigen);



                 lugar_carga = "Lugar:    " + carga + "\n";
                 direccion_carga ="Direccion:    " + dircarga + "\n";
                 referencia_carga="Referencia:  " + refcarga + "\n";
                 fecha_salida = "Fecha: " + fecha_carga + "\n"
                




                direccion_descarga = "Direccion:    " + descarga + "\n"
                lugar_descarga = "Lugar:    "+ regdescargar + "\n" 
                referencia_descarga = "Referencia:  " + refdescarga + "\n"
                fecha_llegada = "Fecha: " + fecha_descarga + "\n"



                datos_trasporte = "Linea:   " + transporte + "\n"
                placastruck = "Placas Tracto:  " + truckplate + "\n"
                placascaja =  "Placas Caja:  " + cajaplate + "\n"
                operadortruck = "Operador:  " + chofer + "\n"
                talontruck= "Talon: "  + talon + "\n"
                cantidad_asunto= datos['cantidad']['0'].cantidad;
                cartaporte= datos['cartaporte']['0'].RemisionPDF;
                lotesmail = datos['lotesmail']

                
                link = "https://www.google.com/maps/search/?api=1&query=" + Maps +"&zoom=20";

               

            


                $('#tablahtml').val(datos['tabla']);
                $('#usuario').val(usuario);
                $('#lugar_carga').val(carga);
                $('#direccion_carga').val(dircarga);
                $('#referencia_carga').val(refcarga);
                $('#fecha_salida').val(fecha_carga);
                $('#direccion_descarga').val(descarga);
                $('#lugar_descarga').val(regdescargar);
                $('#referencia_descarga').val(refdescarga);
                $('#fecha_llegada').val(fecha_descarga);
              //  $('#datos_trasporte').val(transporte);
               // $('#placastruck').val(truckplate);
               // $('#placascaja').val(cajaplate);
              //  $('#operadortruck').val(chofer);
              //  $('#talontruck').val(talon);
              //  $('#telchofer').val(telchofer);
                $('#delivery').val(delivery);
                $('#client').val(client);
                $('#regarrival').val(regarrival);
                $('#trkid').val(trkid);
                $('#typeDO').val(typeDO);
                $('#horasalida').val(horasalida);
                $('#horallegada').val(horallegada);
                $('#mailtransporte').val(mailtransporte);
                
                if (Maps != ""){               
                $('#link').val(link);
                } 
                linea1 = "\n"+"Datos de Carga:"+ "\n"
                linea2 = "\n"+"Datos de Descarga:"+  "\n"
                linea3 = "\n"+"Datos de Transporte"+ "\n"
                linea4 = "\n"+"Lotes"+ "\n"

                
                $('#mailamsa').val(mailamsa);

              
                
                if(mailsorigen !=0){
                document.getElementById('origenes').style.display = 'block';
                $('#mailorigen').val(mailsorigen);
                 }
                 else{

                    document.getElementById('origenes').style.display = 'none';
                    $('#mailorigen').val("");

                 }
                 
                



                 



                //console.log(datos['descarga']['0'].Drctn);


                $('#datoscarga').val(linea1+"   "+fecha_salida+"   "+lugar_carga+"   "+direccion_carga+"   "+referencia_carga + linea2 +"   "+ fecha_llegada+"   "+lugar_descarga+"   "+direccion_descarga+"   "+referencia_descarga+linea3+"   "+datos_trasporte+"   "+operadortruck+"   "+placastruck+"   "+placascaja+"   "+talontruck + linea4 + lotesmail);
                ///$('#datosdescarga').html(fecha_llegada+lugar_descarga+direccion_descarga+referencia_descarga);
                
               // $('#datostransport').html(datos_trasporte+operadortruck+placastruck+placascaja+talontruck);
               // $('#tablahtml').val(datos['tabla']);
               $('#datoscarga').html(datos['tabla']);
               $('#tablamue').html(datos['tabla']);
               $('#subject').val("UNIDAD AUTORIZADA PARA CARGA "+ fecha_asunto +", "+asunto_reg+" ("+cantidad_asunto+" bc)"  )

              //console.table(datos);
               // $('#datoscarga').val(datos['lugarcarga'].BnName);

            if (cantidad_asunto == null){
                document.getElementById("estatus").innerHTML = "the lot has not been assigned a TRUK ID!";
                
             }
             else {
              document.getElementById("estatus").innerHTML ="";
              
             }

         
                
                //$('#datosdescarga').val(datos["descarga"]);
          
                $('#Correo').modal('show');
////if(cartaporte == 0 || cantidad_asunto == null || XML ==0){
                if(cartaporte == 0 || cantidad_asunto == null){
                    $('#sendEmail').prop('disabled', true);
                }
                else{
                    $('#sendEmail').prop('disabled', false);
                }
            }
        });




      
    })



    ///FUNCION PARA EL BOTON ENVIAR CORREO ELECTRONICO

    $(document).on("click",".sendEmail",function(){
        tablahtml = $('#tablahtml').val();
        lugar_carga    = $('#lugar_carga').val();
        direccion_carga     = $('#direccion_carga').val();       
        referencia_carga   = $('#referencia_carga').val();
        fecha_salida    = $('#fecha_salida').val();
       // datos_trasporte   = $('#datos_trasporte').val();
       // placastruck    = $('#placastruck').val();
       // placascaja    = $('#placascaja').val();
       // operadortruck    = $('#operadortruck').val();
       // talontruck    = $('#talontruck').val();
       // telchofer    = $('#telchofer').val();
        usuario    = $('#usuario').val();
      //  telchofer    = $('#telchofer').val();
        asunto = $('#subject').val();
        fecha_llegada=$('#fecha_llegada').val();
        lugar_descarga=$('#lugar_descarga').val();
        direccion_descarga = $('#direccion_descarga').val();
        referencia_descarga = $('#referencia_descarga').val();
        delivery = $('#delivery').val();
        client = $('#client').val();
        regarrival = $('#regarrival').val();
        trkid= $('#trkid').val();
        typeDO= $('#typeDO').val();
        link= $('#link').val();
        horallegada= $('#horallegada').val();
        horasalida= $('#horasalida').val();
        comentarios= $('#comentarios').val();
        mailamsa = $('#mailamsa').val();
        mailorigen=$('#mailorigen').val();
        mailtransporte= $('#mailtransporte').val();
        
        

        $('#sendEmail').prop('disabled', true);


      
        //totalQty  = $('#totalQty').val();
        subject   = $('#subject').val();
        $('#avisomail').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');  
        $.ajax({
            type: "POST",
            url: "bd/correo-truck.php",
                data: {
                    tablahtml:tablahtml, 
                    lugar_carga:lugar_carga,
                    direccion_carga:direccion_carga, 
                    referencia_carga:referencia_carga, 
                    fecha_salida:fecha_salida,           
                    usuario:usuario,                          
                    asunto:asunto,
                    fecha_llegada:fecha_llegada,
                    lugar_descarga:lugar_descarga,
                    direccion_descarga:direccion_descarga,
                    referencia_descarga:referencia_descarga,
                    delivery:delivery,
                    client:client,
                    regarrival:regarrival,
                    typeDO:typeDO,
                    trkid:trkid,
                    link:link,
                    horallegada:horallegada,
                    horasalida:horasalida,
                    comentarios:comentarios,
                    mailamsa:mailamsa,
                    mailorigen:mailorigen,
                    mailtransporte:mailtransporte

                      

                }, success: function(data){
                    alert("Correo enviado con exito");
                    $('#Correo').modal('hide');
                    $('#avisomail').html("");

                },
                error: function (data){
                    alert("Error al enviar datos");
                    $('#Correo').modal('hide');

                }

            })
       
      
    
        
     });
    







})

//----------------Validar que se cargo archivo XML------------
function upfilexml(){
    
    var filename = $("#filexml").val();
  //  console.log(filename);
    //si es null muestra un mensaje de error
    if(filename == null){
        alert('No ha seleccionado una imagen');
        $('#loadxml').prop('disabled', true);
        $('#updatexml').prop('disabled', true);
    }
        
    else{// si se eligio un archivo correcto obtiene la extension para vlidarla
              
        $('#loadxml').prop('disabled', false);
        $('#updatexml').prop('disabled', false);
        
       
    }


}



//----------------DESPLEGAR MODAL DE TARIFARIO------------
$(document).ready(function(){


    internationalNumberFormat = new Intl.NumberFormat('en-US')
    

    $("#btntarifario").click(function(e){ 
        e.preventDefault(e);  
        $('#tarifario-costototal').val("");  
        $('#tarifario-costoprom').val(0);
        $('#tarifario-comentarios').val("");
        $("#tarifario-costototal").attr("readonly", false); 
        $("#tarifario-comentarios").attr("readonly", false); 
        document.getElementById('alertacosto').style.display = 'none';
        $('#tarifario-save').prop('disabled', false);
        document.getElementById('tarifa_inactiva').style.display = 'none';
       
        

        var combo = document.getElementById("TNam");
        var transporte = combo.options[combo.selectedIndex].text;
        dor = $('#DOrd').val();
        regsal = $('#OutRegDO').val();
        regllegada = $('#InRegDO').val();
        qty = $('#QtyDO').val();
        TrkID = idglobaltruck;

    

        //console.log(dor);

        $.ajax({
            url: "bd/crudtarifario.php",
            type: "POST",
            datatype:"json",
            data:  {dor:dor, regsal:regsal, regllegada:regllegada,transporte:transporte,qty:qty,TrkID:TrkID},    
            success: function(data){

                opts = JSON.parse(data);
                city = opts['city'];
                zone = opts['Zone'];
                ginname = opts['NameGine'];
                costopaca = opts['costo'];
                costototal = opts['costos_total'];
                TotalCost=opts['TotalCost'];
                AverageCost=opts['AverageCost'];
                FreighCostNote=opts['FreighCostNote'];
                estado=opts['estado'];

               // console.log(zone);
               // console.log(ginname);
               // console.log(city);

                $('#tarifario-gin').val(ginname);
                $('#tarifario-zonasalida').val(zone);
                $('#tarifario-cityllegada').val(city);
                $('#tarifario-costopaca').val(costopaca);
                costototal= separator(costototal)
                $('#tarifario-costoauto').val(costototal);
               // $('#tarifario').modal('show');

               if(estado =='inactivo'){
                document.getElementById('tarifa_inactiva').style.display = 'block';
                document.getElementById('mensaje').innerHTML='La tarifa Autorizada ya no es vigente desde '+  opts['fechafin'];;
               }

                //PREGUNTAR SI DESEA MODIFICAR LA TARIFA YA CAPTURADA
                if(TotalCost!=0){
                    let text = "Tarifa ya capturada.\n ¿Desea modificar la tarifa?";
                    if (confirm(text) == true) {

                            
                        $(".modal-header").css("background-color", "#17562c");
                        $(".modal-header").css("color", "white" );       
                        $('#tarifario').modal('show'); 		
                        
                        $("#tarifario-costototal").attr("readonly", false); 
                        $("#tarifario-comentarios").attr("readonly", false); 
                        if( $('#samples').prop('checked') ) {
                            $('#tarifario-muestras').val("TRUE");
                          
                        }  else {
                            $('#tarifario-muestras').val("FALSE");
                        }             
                        
                     
                        //console.log(costo_autorizado);
                
                    
                        $('#tarifario-trasnport').val(transporte);
                        $('#tarifario-regsalida').val($('#OutRegDO').val());
                        $('#tarifario-regllegada').val($('#InRegDO').val());
                        $('#tarifario-cantidad').val($('#QtyDO').val());
                       // TotalCost = separator(TotalCost);
                        //$('#tarifario-costoprom').val(AverageCost);
                        //$('#tarifario-costototal').val(TotalCost);
                        $('#tarifario-comentarios').val(FreighCostNote);

                    } else {
                        $('#tarifario').modal("hide");

                    }
                }

                else{

                    $(".modal-header").css("background-color", "#17562c");
                        $(".modal-header").css("color", "white" );       
                        $('#tarifario').modal('show'); 		
                        
                        $("#tarifario-costototal").attr("readonly", false); 
                        $("#tarifario-comentarios").attr("readonly", false); 
                        if( $('#samples').prop('checked') ) {
                            $('#tarifario-muestras').val("TRUE");
                          
                        }  else {
                            $('#tarifario-muestras').val("FALSE");
                        }             
                        
                     
                        //console.log(costo_autorizado);
                
                    
                        $('#tarifario-trasnport').val(transporte);
                        $('#tarifario-regsalida').val($('#OutRegDO').val());
                        $('#tarifario-regllegada').val($('#InRegDO').val());
                        $('#tarifario-cantidad').val($('#QtyDO').val());
                        //TotalCost = separator(TotalCost);
                        //$('#tarifario-costoprom').val(AverageCost);
                        //$('#tarifario-costototal').val(TotalCost);
                        $('#tarifario-comentarios').val(FreighCostNote);



                }
               
                
               
            },
            error: function(data) {
                alert('error');
            }
        });

        

    })

//Funcion para guardar cambios en tarifario
    $("#tarifario-save").click(function(e){ 
        e.preventDefault();
       costoprom= $('#tarifario-costoprom').val();
       costot=$('#tarifario-costototal').val();
       notas= $('#tarifario-comentarios').val();
       costoprom = costoprom.replace(/,/g, ''); 
       costot = costot.replace(/,/g, ''); 
       TrkID = idglobaltruck;
       opcion=18;
        $.ajax({ //verificamos que exista el transport o no en la bd
        url: "bd/crudTrkv2.php",
        type: "POST",
        datatype:"json",
        data:  {costoprom:costoprom, costot:costot, notas:notas,opcion:opcion,TrkID:TrkID}, 

        success: function(data){
            alert("Datos guardados");
            $('#tarifario').modal('hide');
        }

       });
       

       

    })


    //FORMATO ##,###

    $("#tarifario-costoprom").on({

        "focus": function(event) {
          $(event.target).select();
        },
        "keyup": function(event) {
          $(event.target).val(function(index, value) {
            return value.replace(/\D/g, "")
              //.replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
              .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          });
        }
      });

/*
      $("#tarifario-costototal").on({

        "focus": function(event) {
          $(event.target).select();
        },
        "keyup": function(event) {
          $(event.target).val(function(index, value) {
            return value.replace(/\D/g, "")
              //.replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
              .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          });
        }
      });

      */
   

 })



 

 function peso_salida(){
    //peso_sal = $("#OutWgh").val();
    peso_sal=document.getElementById("OutWgh").value;
    cantidad=document.getElementById("QtyDO").value;

   // peso_llehada = $("#InWgh").val();
   // cantidad = $('#QtyDO').val();
   if (peso_sal.includes(",")) {
    pesosalida = peso_sal.replace(/,/g, ''); 
   // console.log(pesosalida)
   }
   else{
    pesosalida = peso_sal;
   }
  //  pesollegada = InWgh.replace(/,/g, ''); 

    if (cantidad != '0' && pesosalida != '0'){
        promdiosalida = (pesosalida)/(cantidad);
        //console.log(pesosalida)
        promdiosalida = promdiosalida.toFixed(2)
    
        promdiosalida = parseFloat(promdiosalida);
        $('#promsalida').val(promdiosalida);   

    }
    else{
        $('#promsalida').val(0);  
    
    }

 }

 function peso_llegada(){
    //peso_sal = $("#OutWgh").val();
    peso_lleg=document.getElementById("InWgh").value;
    cantidad=document.getElementById("QtyDO").value;

   // peso_llehada = $("#InWgh").val();
   // cantidad = $('#QtyDO').val();
   if (peso_lleg.includes(",")) {
    pesosllegada = peso_lleg.replace(/,/g, ''); 
  //  console.log(pesosllegada)
   }
   else{
    pesosllegada = peso_lleg;
   }
  //  pesollegada = InWgh.replace(/,/g, ''); 

    if (cantidad != '0' && pesosllegada != '0'){
        
        promdiollegada = (pesosllegada)/(cantidad);
        //console.log(pesosalida)
        promdiollegada = promdiollegada.toFixed(2)
    
        promdiollegada = parseFloat(promdiollegada);
        $('#promllegada').val(promdiollegada);   

    }
    else{
        $('#promllegada').val(0);  
    
    }

 }
 

// formato miles (#,###) de manera automatica

function separator(numb) {
    var str = numb.toString().split(".");
    str[0] = str[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return str.join(".");
}

 function cost_total(){
    
    costo_total=document.getElementById("tarifario-costototal").value; 
    qty=document.getElementById("QtyDO").value;
    ban = document.getElementById("tarifario-muestras").value;
    costo_total = costo_total.replace(/,/g, ''); 
   

    if(costo_total != 0 || costo_total!="" ){
        if(ban =="TRUE"){
            promedio_total = costo_total / 120;
        }
        else{
        promedio_total = costo_total / qty;
         }
        promedio_total = promedio_total.toFixed(2)

        $('#tarifario-costoprom').val(promedio_total);
        
    }
    else{
        $('#tarifario-costoprom').val(0);
    }

    separator($('#tarifario-costototal').val());

    //mostrar alerta si el costo capturado supera al costo autorizado


 }


 function alerta(){
    costo_total=document.getElementById("tarifario-costototal").value; 
    costo_auto=document.getElementById("tarifario-costoauto").value; 
    costo_total = costo_total.replace(/,/g, ''); 
    costo_auto = costo_auto.replace(/,/g, ''); 


    if((costo_total != 0 || costo_total!="") && costo_auto > 1 ){
            costo_auto= parseFloat(costo_auto);
            costo_total=parseFloat(costo_total);
           // dif=costo_total-costo_auto
            if((costo_auto+10) < costo_total){
                document.getElementById('alertacosto').style.display = 'block';
                //document.getElementById('alertacostotext').innerHTML=' El costo total supera el costo autorizado por $'+dif+'. Agregar comentario'; 
                $('#tarifario-save').prop('disabled', true);

            }

            else{
                document.getElementById('alertacosto').style.display = 'none';
                $('#tarifario-save').prop('disabled', false);
                
            }
    }


 }

 function boxcomentario(){
    boxcoment=document.getElementById("tarifario-comentarios").value; 

    
    if(boxcoment !== '' && boxcoment !==' '){
        $('#tarifario-save').prop('disabled', false);

    }
    else{
        $('#tarifario-save').prop('disabled', true);
    }
    

 }