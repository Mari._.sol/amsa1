<?php
session_start();
if (!isset($_SESSION['username'])) :
    include_once('index.php');
else :
    $Typ_Cli = $_SESSION['Typ_Cli'];
    $permiso = $_SESSION['Admin'];
?>
<!doctype html>
<html lang="es en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="img/ecom.png" />
        <title>Fixations</title>

        <!-- CSS bootstrap -->
        <link rel="stylesheet" href=".//assets/bootstrap/css/bootstrap.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="./main.css">
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>
    <body>
        <!-- Scripts -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>

        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>

        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>

        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>

        <?php if ($Typ_Cli == 1) { ?>
            <script type="text/javascript" src="fixations.js"></script>
        <?php } else { ?>
            <script type="text/javascript" src="fixations.js"></script>
        <?php } ?>
        <!-- Terminan Scripts -->

        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>

                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <li><a class="dropdown-item" href="trucks.php">Trucks</a></li>
                        <li><a class="dropdown-item" href="lots.php">Lots</a></li>
                        <?php if ($_SESSION['username'] == 'ISELA LARA' || $_SESSION['username'] == 'DARIO ROMERO' || $_SESSION['username'] == 'CESAR ARMAS' || $_SESSION['username'] == 'LUIS ANGEL RENDON TARIN'){?>
                        <li><a class="dropdown-item" href="bales.php">Bales</a></li>
                        <?php } ?>
                        <li><a class="dropdown-item" href="do.php">Delivery Order</a></li>
                        <li><a class="dropdown-item" href="adjustments.php">Adjustments</a></li>
                        <li><a class="dropdown-item" href="inv.php">Invoice</a></li>
                        <li><a class="dropdown-item" href="export.php">Exports</a></li>
                        <li><a class="dropdown-item" href="clients.php">Clients</a></li>
                        <li><a class="dropdown-item" href="routes.php">Routes</a></li>
                        <li><a class="dropdown-item" href="requisition.php">Requisition</a></li>
                        <li><a class="dropdown-item" href="CMS.php">CMS</a></li>
                        <?php if ($permiso == 1){?>
                        <li><a class="dropdown-item" href="adminusers.php">User Management</a></li>
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="#" >Fixations</a></li>
                        <?php } ?>
                    </ul>
                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-2" href="main.php">
                    <div class="logos"><img class="log" src="img/logo1.png"> AMSA </div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <p> / Fixations</p>
                </div>
                <div class="container-fluid container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                    <!-- Botones -->
                    <?php if ($Typ_Cli == 1) : ?>
                        <button id="btnNueva" type="button" class="btn btn-success" data-toggle="modal tooltip" data-placement="bottom" title="Add Fixation"><i class="bi bi-plus-square"></i></button>
                    <?php endif ?>
                </div>
                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                    <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"><?php echo $_SESSION['username']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Aquí inicia todo código de tablas etc -->
        <!-- Inicio de la tabla -->
        <div class="card card-body" style="opacity:100%;">
            <div class="table-responsive" style="opacity:100%;">
                <table id="tablaFixations" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;">
                    <thead style="background-color: #65ac7c;" style="opacity:100%;">
                        <tr>
                            <th class="th-sm">Id Fix</th>
                            <th class="th-sm">Date</th>
                            <th class="th-sm">S/P</th>
                            <th class="th-sm">Name</th>
                            <th class="th-sm">BC</th>
                            <th class="th-sm">Futures</th>
                            <th class="th-sm">Cover Month</th>
			                <th class="th-sm">Fixed</th>
                            <th class="th-sm">Confirmed</th>
                            <th class="th-sm">Contract</th>   
                            <th class="th-sm">Pos</th>
                            <th class="th-sm">Nav</th>
                            <?php if ($Typ_Cli == 1) { ?>
                                <th class="no-exportar"></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="th-sm">Id Fix</th>
                            <th class="th-sm">Date</th>
                            <th class="th-sm">S/P</th>
                            <th class="th-sm">Name</th>
                            <th class="th-sm">BC</th>
                            <th class="th-sm">Futures</th>
                            <th class="th-sm">Cover Month</th>
			                <th class="th-sm">Fixed</th>
                            <th class="th-sm">Confirmed</th>
                            <th class="th-sm">Contract</th>   
                            <th class="th-sm">Pos</th>
                            <th class="th-sm">Nav</th>
                            <?php if ($Typ_Cli == 1) { ?>
                                <th class="no-exportar"></th>
                            <?php } ?>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!--Modal para CRUD-->
        <div class="modal fade in" data-bs-backdrop="static" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formFix">
                        <div class="modal-body">
                            <div class="">
                                <div class="row ">
                                    <div class="col-lg-3">
                                        <div class="input-group mb-3">
                                            <label class="input-group-text" for="asLots">Date *</label>
                                            <input  class="form-control me-2" type="date" name="fecha" id="fecha" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="s_p" id="sale" value="0" required>
                                            <label class="form-check-label" for="inlineRadio1">Sale</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="s_p" id="purchase" value="1">
                                            <label class="form-check-label" for="inlineRadio2">Purchase</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Name *</label>
                                            <select class="form-control form-control-sm" style="" name="name" id="name" disabled required>
                                                <option value="">-SELECT-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">BC *</label>  
                                            <input type="number" min="0" class="form-control form-control-sm"  style="" name="bc" id="bc"  required >
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Futures</label>
                                            <div class="input-group">
                                                <button type="button" id="disminuir" class="input-group-prepend">-</button>
                                                <input type="number" min="0" class="form-control form-control-sm" name="futures" id="futures" form="formFix" disabled>
                                                <button type="button" id="aumentar" class="input-group-append">+</button>
                                            </div>
                                            <div id="Aviso"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Month *</label>
                                            <select class="form-control form-control-sm" style="text-transform:uppercase;" name="mes" id="mes">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Previous price</label>
                                            <input type="number" class="form-control form-control-sm"  style="" name="ant" id="ant" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Fixed *</label>
                                            <input type="number" step="0.01" class="form-control form-control-sm"  name="fixed" id="fixed" oninput="calcularDiferencia()" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Confirmed *</label>
                                            <input type="number" step="0.01" class="form-control form-control-sm"  style="" name="confirmed" id="confirmed" oninput="calcularDiferencia()" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Difference</label>
                                            <input type="number" class="form-control form-control-sm"  style="" name="difference" id="difference" disabled>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Earned</label>
                                            <input type="number" class="form-control form-control-sm" style="" name="earned" id="earned" disabled>
                                        </div>
                                        <div id="AvisoE"></div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Contract</label>
                                            <input type="text" class="form-control form-control-sm"  style="text-transform:uppercase;" name="contract" id="contract">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row ">
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <input class="form-check-input" type="checkbox" value="" name="pos" id="pos">
                                            <label class="form-check-label" for="flexCheckDefault">
                                                Pos
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                        <input class="form-check-input" type="checkbox" value="" name="nav" id="nav">
                                            <label class="form-check-label" for="flexCheckDefault">
                                                Nav
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <font size=2>*Required Fields</font>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" id="btnSend" class="btn btn-dark">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <style>
    .input-group {
        display: flex;
        align-items: center;
    }

    .input-group button {
        background-color: #17562c;
        color: white;
        border: none;
        padding: 3.5px 8px;
        cursor: pointer;
    }

    .input-group input {
        text-align: center; /* Centrar el texto en el input */
    }
    </style>
</html>
<?php
endif;
?>
