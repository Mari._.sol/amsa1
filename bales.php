<?php
session_start();
header("Cache-Control: no-cache, must-revalidate"); //para borrar cache
if (!isset($_SESSION['username'])) :
    include_once('index.php');
else :
    //$_SESSION['Reg'];
    $Typ_Trk = $_SESSION['Typ_Trk'];
    $permiso = $_SESSION['Admin'];
    $Priv = $_SESSION['Priv'];
    $filtersSes = $_SESSION['Filters'];
    include_once('bd/correo-model.php');
    $regiones = getRegiones();
    $gines = getGines();
    $supplier = getSupplier();
    $clientes = getnamecliente();
    
    $usuario=$_SESSION['username'];
    $_SESSION['Filters'] = array("muestras" => "0",
                                "DO" => "",
                                "Reg" => "",
                                "Gin" => "",
                                "Lot" => "",
								                "Bale" => "",
                                "Crp" => "",
                                "Status" => ""
                                //"LotU" => "",
								//"CrpU" => "",
								//"CliU" => "",
								//"TypCliU" => ""
								);
    foreach($regiones as $reg){
        $regsID[] = $reg['IDReg'];
        $regs[] = $reg['RegNam'];
    }

    foreach($gines as $gin){
        $ginID[] = $gin['IDGin'];
        $gins[] = $gin['GinName'];
    }
    
    foreach($supplier as $sup){
        $SupID[] = $sup['SupID'];
        $SupName[] = $sup['SupName'];
    }

?>


    <!doctype html>
    <html lang="es en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        
        <!--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>-->

        <link rel="shortcut icon" href="img/ecom.png" />

         <title>Bales</title>
        
        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="./assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="./assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="./main.css">
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>

    <body>

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
        
        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

       <!-- librerias necesarias para finalizar sesion por inactividad -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

        <script type="text/javascript" src="timer.js"></script>
        
        
        <!-- Codigo para realizar funciones relacionadas con la DO dentro del formulario-->
        <script type="text/javascript">



            $(document).ready(function() {
                $("#DOrd").keyup(function() {})

                function update() {
                    $("#TNam").empty();
                    $('#OutRegDO').val("");
                    $('#InRegDO').val("");
                    $('#TypDO').val("");
                    $('#Client').val("");
                    var opts = "";
                    opcion = 5;
                    DO = $.trim($('#DOrd').val());
                    $.ajax({
                        url: "bd/crudTrk.php",
                        type: "POST",
                        datatype: "json",
                        data: {
                            DO: DO,
                            opcion: opcion
                        },
                        success: function(data) {
                            opts = JSON.parse(data);
                            $('#TNam').append('<option class="form-control" value="">- Select -</option>');
                            for (var i = 0; i < opts.length; i++) {
                                $('#TNam').append('<option value="' + opts[i].TptID + '">' + opts[i].BnName + '</option>');
                            }
                            $('#OutRegDO').val(opts[0].PlcOut);
                            $('#InRegDO').val(opts[0].RegIn);
                            $('#TypDO').val(opts[0].Typ);
                            $('#Client').val(opts[0].PlcIn);
                        },
                        error: function(data) {
                            alert('error');
                        }
                    });
                }

                $("#DOrd").on("change", function() {
                    update();
                })
            })
        </script>

        

        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>

        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>

        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>

        <?php if ($Typ_Trk == 1) { ?>
            <script type="text/javascript" src="mainBales.js?v=<?php echo time(); ?>"></script>
        <?php } else {  ?>
            <script type="text/javascript" src="mainBales.js?v=<?php echo time(); ?>"></script>
        <?php } ?>


        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item nav-item col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>

                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <li><a class="dropdown-item" href="trucks.php">Trucks</a></li>
                        <li><a class="dropdown-item" href="lots.php">Lots</a></li>
                        <?php if ($_SESSION['username'] == 'ISELA LARA' || $_SESSION['username'] == 'DARIO ROMERO' || $_SESSION['username'] == 'CESAR ARMAS' || $_SESSION['username'] == 'LUIS ANGEL RENDON TARIN' || $_SESSION['username'] == 'JOSE RENDON') { ?>
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="#">Bales</a></li>
                        <?php } ?>
                        <li><a class="dropdown-item" href="do.php">Delivery Order</a></li>
                        <li><a class="dropdown-item" href="adjustments.php">Adjustments</a></li>
                        <li><a class="dropdown-item" href="inv.php">Invoice</a></li>
                        <li><a class="dropdown-item" href="export.php">Exports</a></li>
                        <li><a class="dropdown-item" href="clients.php">Clients</a></li>
                        <li><a class="dropdown-item" href="routes.php">Routes</a></li>
                        <li><a class="dropdown-item" href="requisition.php">Requisition</a></li>
                        <li><a class="dropdown-item" href="CMS.php">CMS</a></li>
                        <?php if ($permiso == 1){?>
                        <li><a class="dropdown-item" href="adminusers.php">User Management</a></li>
                        <?php } ?>
                    </ul>
                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-2 col-md-2 col-sm-1 col-xs-2" href="main.php">
                    <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <p>/ Bales</p>
                </div>
                <div class="container-fluid col-xl-7 col-lg-4 col-md-4 col-sm-2 col-xs-1">
                    <!-- Botones -->
                    
                      <?php if ($usuario =="JOSE RENDON") : ?>                       
                        <button id="HVIExportPDF" type="button" class="btn btn-danger" data-toggle="modal" data-placement="bottom" title="Export to PDF"><i class="bi bi-file-earmark-pdf-fill"></i></button>
                    <?php endif ?>
                    
                    <?php if ($Typ_Trk == 0 && $usuario !="JOSE RENDON") : ?>
                        <button id="HVIRecap" type="button" class="btn btn-info" data-toggle="modal" data-placement="bottom" title="Recap"><i class="bi bi-file-earmark-bar-graph"></i></button>
                        <button id="HVIExportExcel" type="button" class="btn btn-info" data-toggle="modal" title="Export to Excel"><i class="bi bi-file-earmark-excel-fill"></i></button>
                        <button id="HVIExportPDF" type="button" class="btn btn-danger" data-toggle="modal" data-placement="bottom" title="Export to PDF"><i class="bi bi-file-earmark-pdf-fill"></i></button>
                    <?php endif ?>
                    <?php if ($Typ_Trk == 1) : ?>
                        <button id="NewBales" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Upload HVI"><i class="bi bi-plus-square"></i></button>
                        <button id="UpdateBales" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Upload Lots"><i class="bi bi-file-earmark-arrow-up"></i></button>
                        <button id="UpdateLiq" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Upload Liqs"><i class="bi bi-tags"></i></button>
                        <button id="UpdateHVI" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Update HVI"><i class="bi bi-gear"></i></button>
                        <button id="HVIRecap" type="button" class="btn btn-info" data-toggle="modal" data-placement="bottom" title="Recap"><i class="bi bi-file-earmark-bar-graph"></i></button>
                        <button id="HVIExportExcel" type="button" class="btn btn-info" data-toggle="modal" title="Export to Excel"><i class="bi bi-file-earmark-excel-fill"></i></button>
                        <button id="HVIExportPDF" type="button" class="btn btn-danger" data-toggle="modal" data-placement="bottom" title="Export to PDF"><i class="bi bi-file-earmark-pdf-fill"></i></button>
                        <button id="Provisional" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Upload Provisional Data"><i class="bi bi-file-earmark-diff-fill"></i></button>
                        <button id="loadhvi" type="button" class="btn btn-success" data-toggle="modal tooltip" data-placement="bottom" title="Asign HVI to Client"><i class="bi bi-cloud-upload-fill"></i></button>
                    <?php endif ?>
                    <?php if ($Typ_Trk == 2) : ?>
                        <button onclick="'window.open(./bd/exportExcel.php'" id="export" type="submit" class="btn btn-info" data-toggle="modal" title="Export to Excel"><i class="bi bi-file-earmark-excel-fill"></i></button>
                        <button id="btnFile" type="button" class="btn btn-info" data-toggle="modal" title="Import CSV"><i class="bi bi-filetype-csv"></i></button>
                    <?php endif ?>		    
                </div>
                </div>
                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-4 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                    <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"><?php echo $_SESSION['username']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->

        <!-- Aquí inicia todo código de tablas etc -->

        <!-- Inicio de la tabla -->
        <div class="card card-body" style="opacity:100%;">

        <!-- Identificadores para los recaps  -->  
            <div class="d-flex justify-content-end">
                <div class="col-xs-3">
                    <div class="input-group mb-1">
                        <label class="input-group-text" >DO</label>
                        <input type="text" class="form-control" id="DOCli" style="text-transform:uppercase;" disabled>
                        <label class="input-group-text" >Lots</label>
                        <input type="text" class="form-control" id="LotsCli" style="text-transform:uppercase;" disabled>
                        <label class="input-group-text" >Crop</label>
                        <input type="text" class="form-control" id="CrpCli" style="text-transform:uppercase;" disabled>
                        <label class="input-group-text" >Cliente</label>
                        <input type="text" class="form-control" id="CliHVI" style="text-transform:uppercase;">
                        <label class="input-group-text" >HVI</label>
                        <select class="form-select me-2" name="TypHVI" id="TypHVI">
                            <option value="O">Original</option>
                            <option value="M" selected>Modified</option>
                        </select>
                        
                    </div>
                </div>
            </div>

            <div class="table-responsive" style="opacity:100%;">
            <!-- Boton filtrar por -->
            <div>
                <button id="filtraboton" type="button" class="btn btn-info btn-filtrar" data-toggle="modal tooltip" data-placement="bottom" title="Filter by"><i class="bi bi-funnel-fill"></i></button>
            </div>
            <!-- Nav tabs  HVI original/Modificado -->
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Original HVI</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Modified HVI</button>
                </li>
            </ul> 

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <!-- Resumen de busqueda  -->  
                    <!--<div class="d-flex justify-content-end">
                        <div class="col-xs-3">
                            <div class="input-group mb-1">
                                <label class="input-group-text" >Total Lots</label>
                                <label class="input-group-text"  id="totallotes" >0</label>
                                <label class="input-group-text" >Total Bales</label>
                                <label class="input-group-text"  id="monto" >0</label>
                                <label class="input-group-text" >Overall Weight</label>
                                <label class="input-group-text"  id="totalpeso" >0</label>
                            </div>
                        </div>
                    </div>-->
                    <table id="tablaBales" class="table display bg-white table-striped row-border order-column table-hover" style="opacity:100%;">
                        <thead style="background-color: #65ac7c;" style="opacity:100%;">
                            <tr>
                                <th class="th-sm">Crp</th>
                                <th class="th-sm">GinID</th>
                                <th class="th-sm">Bal</th>
                                <th class="th-sm">Rcv</th>
                                <th class="th-sm">BuyIt</th>
                                <th class="th-sm">Grp</th>
                                <th class="th-sm">Mic</th>
                                <th class="th-sm">Len</th>
                                <th class="th-sm">Str</th>
                                <th class="th-sm">Rd</th>
                                <th class="th-sm">b</th>
                                <th class="th-sm">Tcnt</th>
                                <th class="th-sm">Tar</th>
                                <th class="th-sm">Col1</th>
                                <th class="th-sm">Col2</th>
                                <th class="th-sm">Tgrd</th>
                                <th class="th-sm">Mst</th>
                                <th class="th-sm">Sfi</th>
                                <th class="th-sm">Sci</th>
                                <th class="th-sm">Mat</th>
                                <th class="th-sm">Elg</th>
                                <th class="th-sm">Unf</th>
                                <th class="th-sm">Cnt</th>
                                <th class="th-sm">Wgh</th>
                                <th class="th-sm">Lot</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="th-sm">Crp</th>
                                <th class="th-sm">GinID</th>
                                <th class="th-sm">Bal</th>
                                <th class="th-sm">Rcv</th>
                                <th class="th-sm">BuyIt</th>
                                <th class="th-sm">Grp</th>
                                <th class="th-sm">Mic</th>
                                <th class="th-sm">Len</th>
                                <th class="th-sm">Str</th>
                                <th class="th-sm">Rd</th>
                                <th class="th-sm">b</th>
                                <th class="th-sm">Tcnt</th>
                                <th class="th-sm">Tar</th>
                                <th class="th-sm">Col1</th>
                                <th class="th-sm">Col2</th>
                                <th class="th-sm">Tgrd</th>
                                <th class="th-sm">Mst</th>
                                <th class="th-sm">Sfi</th>
                                <th class="th-sm">Sci</th>
                                <th class="th-sm">Mat</th>
                                <th class="th-sm">Elg</th>
                                <th class="th-sm">Unf</th>
                                <th class="th-sm">Cnt</th>
                                <th class="th-sm">Wgh</th>
                                <th class="th-sm">Lot</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <table id="tablaBalesMod" class="table display bg-white table-striped row-border order-column table-hover" style="opacity:100%;">
                        <thead style="background-color: #65ac7c;" style="opacity:100%;">
                            <tr>
                                <th class="th-sm">Crp</th>
                                <th class="th-sm">GinID</th>
                                <th class="th-sm">Bal</th>
                                <th class="th-sm">Rcv</th>
                                <th class="th-sm">BuyIt</th>
                                <th class="th-sm">Typ</th>
                                <th class="th-sm">Mic</th>
                                <th class="th-sm">Len</th>
                                <th class="th-sm">Str</th>
                                <th class="th-sm">Rd</th>
                                <th class="th-sm">b</th>
                                <th class="th-sm">Tcnt</th>
                                <th class="th-sm">Tar</th>
                                <th class="th-sm">Col1</th>
                                <th class="th-sm">Col2</th>
                                <th class="th-sm">Tgrd</th>
                                <th class="th-sm">Mst</th>
                                <th class="th-sm">Sfi</th>
                                <th class="th-sm">Sci</th>
                                <th class="th-sm">Mat</th>
                                <th class="th-sm">Elg</th>
                                <th class="th-sm">Unf</th>
                                <th class="th-sm">Cnt</th>
                                <th class="th-sm">Wgh</th>
                                <th class="th-sm">Lot</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="th-sm">Crp</th>
                                <th class="th-sm">GinID</th>
                                <th class="th-sm">Bal</th>
                                <th class="th-sm">Rcv</th>
                                <th class="th-sm">BuyIt</th>
                                <th class="th-sm">Typ</th>
                                <th class="th-sm">Mic</th>
                                <th class="th-sm">Len</th>
                                <th class="th-sm">Str</th>
                                <th class="th-sm">Rd</th>
                                <th class="th-sm">b</th>
                                <th class="th-sm">Tcnt</th>
                                <th class="th-sm">Tar</th>
                                <th class="th-sm">Col1</th>
                                <th class="th-sm">Col2</th>
                                <th class="th-sm">Tgrd</th>
                                <th class="th-sm">Mst</th>
                                <th class="th-sm">Sfi</th>
                                <th class="th-sm">Sci</th>
                                <th class="th-sm">Mat</th>
                                <th class="th-sm">Elg</th>
                                <th class="th-sm">Unf</th>
                                <th class="th-sm">Cnt</th>
                                <th class="th-sm">Wgh</th>
                                <th class="th-sm">Lot</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!--<ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#home">Original HVI</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#menu1">Modified HVI</a>
            </li>
            </ul>

                <div class="form-check form-switch ms-2 mt-2">
                    <label class="form-check-label" for="esMuestra">Original</label>
                    <input class="form-check-input" type="checkbox" id="esMuestra">
                    <label class="form-check-label" for="esMuestra">Modified</label>
                </div>
                <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="ListHVI" id="ListHVIO" value="ListHVIO" checked>
                <label class="form-check-label" for="inlineRadio1"><font color="white">Original HVI</font></label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="ListHVI" id="ListHVIM" value="ListHVIM">
                    <label class="form-check-label" for="inlineRadio2"><font color="white">Modified HVI</font></label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="ListHVI" id="ListLots" value="ListLots">
                    <label class="form-check-label" for="inlineRadio2"><font color="white">Lots</font></label>
                </div>-->

                <!--tabla funcional ------
                <table id="tablaBales" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;">
                    <thead style="background-color: #65ac7c;" style="opacity:100%;">
                        <tr>
                            <th class="th-sm">Crp</th>
                            <th class="th-sm">GinID</th>
                            <th class="th-sm">Bal</th>
                            <th class="th-sm">Rcv</th>
                            <th class="th-sm">BuyIt</th>
                            <th class="th-sm">Typ</th>
                            <th class="th-sm">Mic</th>
                            <th class="th-sm">Len</th>
                            <th class="th-sm">Str</th>
                            <th class="th-sm">Rd</th>
                            <th class="th-sm">b</th>
                            <th class="th-sm">Tcnt</th>
                            <th class="th-sm">Tar</th>
                            <th class="th-sm">Col1</th>
                            <th class="th-sm">Col2</th>
                            <th class="th-sm">Tgrd</th>
                            <th class="th-sm">Mst</th>
                            <th class="th-sm">Sfi</th>
                            <th class="th-sm">Sci</th>
                            <th class="th-sm">Mat</th>
                            <th class="th-sm">Elg</th>
                            <th class="th-sm">Unf</th>
                            <th class="th-sm">Cnt</th>
                            <th class="th-sm">Wgh</th>
                            <th class="th-sm">Lot</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="th-sm">Crp</th>
                            <th class="th-sm">GinID</th>
                            <th class="th-sm">Bal</th>
                            <th class="th-sm">Rcv</th>
                            <th class="th-sm">BuyIt</th>
                            <th class="th-sm">Typ</th>
                            <th class="th-sm">Mic</th>
                            <th class="th-sm">Len</th>
                            <th class="th-sm">Str</th>
                            <th class="th-sm">Rd</th>
                            <th class="th-sm">b</th>
                            <th class="th-sm">Tcnt</th>
                            <th class="th-sm">Tar</th>
                            <th class="th-sm">Col1</th>
                            <th class="th-sm">Col2</th>
                            <th class="th-sm">Tgrd</th>
                            <th class="th-sm">Mst</th>
                            <th class="th-sm">Sfi</th>
                            <th class="th-sm">Sci</th>
                            <th class="th-sm">Mat</th>
                            <th class="th-sm">Elg</th>
                            <th class="th-sm">Unf</th>
                            <th class="th-sm">Cnt</th>
                            <th class="th-sm">Wgh</th>
                            <th class="th-sm">Lot</th>
                        </tr>
                    </tfoot>
                </table>  TABLA FUNCIONA -->
            </div>
        </div>

        <!-- Modal filtros bales -->
        <div class="modal hide fade" id="filtrarmodal" data-bs-backdrop="static" tabindex="-1" role="dialog" data-bs-keyboard="false" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-tittle">Filter table by</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="filtros" class="filtros">
                            <div class="input-group mb-3">
                                <label class="input-group-text" for="DepRegFil">Region</label>
                                <select class="form-select me-2" name="Reg" id="Reg">
                                    <option value="">Choose...</option>
                                    <?php $i = 0; while($i < count($regs)): ?>
                                    <option value="<?php echo $regsID[$i]; ?>"><?php echo $regs[$i]; ?></option>
                                    <?php $i++; endwhile; ?>
                            
                                </select>
                                <label class="input-group-text" for="Gin">Planta</label>
                                <select class="form-select me-2" id="Gin" name="Gin">
                                </select>
                                <label class="input-group-text" for="idDO">DO</label>
                                <input  class="form-control me-2" type="text" name="DO" id="DO" placeholder="Write Delivery Order" >
                            </div>

                            <div class="input-group mb-3">
                                <label class="input-group-text" for="idDO">Lot</label>
                                <input  class="form-control me-2" type="text" name="Lot" id="Lot" placeholder="Write Lots" >
                            </div>
                            
                            <div class="input-group mb-3">
                                <label class="input-group-text" for="asLots">Bale</label>
                                <input  class="form-control me-2" type="text" name="Bale" id="Bale" placeholder="Write Bales" >
                                <label class="input-group-text" for="timeSelect">Crop</label>
                                    <select class="form-select me-2" id="Crp" name="Crp">
                                        <option value="" selected >All...</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                    </select>
                                <label class="input-group-text" for="timeSelect">Status</label>
                                    <select class="form-select me-2" id="Status" name="Status">
                                        <option value="All" selected >All...</option>
                                        <option value="SL">Without Lot</option>
                                        <option value="CL">With Lot</option>
                                    </select>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <div style="height: 2.5em;" id="aviso"></div>
                    <button id="cer" type="button" class="btn btn-light" value="3" data-bs-dismiss="modal" >Close</button>
                        <button id="borrarFiltro" type="button" value="0" class="btn btn-danger Filtro" >Clear filters</button>
                        <button id="buscafiltro" type="button" value="1" class="btn btn-primary Filtro" >Apply filters</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal update HVI -->
        <div class="modal hide fade" id="modal-UpdateHVI" data-bs-backdrop="static" tabindex="-1" role="dialog" data-bs-keyboard="false" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-tittle">Update HVI</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="HVI" class="filtros">
                            <div class="input-group mb-3">
                                <label class="input-group-text" for="idDO">Lot</label>
                                <input  class="form-control me-2" type="text" name="LotU" id="LotU" placeholder="Write Lots" >
                                <label class="input-group-text" for="timeSelect">Crop</label>
                                    <select class="form-select me-2" id="CrpU" name="CrpU">
                                        <option value="" selected >All...</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                    </select>
                                <label class="input-group-text" for="idDO">Client</label>
                                <input  class="form-control me-2" type="text" name="CliU" id="CliU" placeholder="Write Client" >
                                <label class="input-group-text" for="idDO">Type</label>
                                <input  class="form-control me-2" type="text" name="TypCliU" id="TypCliU" placeholder="Write Type" >
                                <button id="Search" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Search"><i class="bi bi-search"></i></button>
                                <button id="Apply" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Save Changes"><i class="bi bi-sd-card-fill"></i></button>
                                <button id="desmarcarTodo" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Uncheck All"><i class="bi bi-bag-x"></i></button>
                                <button id="ValorCero" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Send to Zero"><i class="bi bi-arrow-down-square"></i></button>
                            </div>
                            <!-- |Check parametros HVI| -->
                            <fieldset class="scheduler-border">
                                <legend class="scheduler-border">Select your parameters:</legend>
                                <div class="input-group input-group mb-3">
                                    <div class="col-2">
                                        <div>
                                            <input type="checkbox" id="Grd" name="GrdP">
                                            <label for="scales">Grade</label>
                                        </div>
                                        <div>
                                            <input type="checkbox" id="TA" name="TA">
                                            <label for="horns">T. Area</label>
                                        </div>
                                        <div>
                                            <input type="checkbox" id="TC" name="TC">
                                            <label for="horns">T. Code</label>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div>
                                            <input type="checkbox" id="PC" name="Grd">
                                            <label for="scales">P. Count</label>
                                        </div>
                                        <div>
                                            <input type="checkbox" id="Len" name="Len">
                                            <label for="horns">Length</label>
                                        </div>
                                        <div>
                                            <input type="checkbox" id="UI" name="UI">
                                            <label for="horns">Uniformity</label>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div>
                                            <input type="checkbox" id="SFI" name="SFI">
                                            <label for="scales">Short Fiber Index</label>
                                        </div>
                                        <div>
                                            <input type="checkbox" id="Str" name="Str">
                                            <label for="horns">Strength</label>
                                        </div>
                                        <div>
                                            <input type="checkbox" id="Elg" name="Elg">
                                            <label for="horns">Elongation</label>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div>
                                            <input type="checkbox" id="Mic" name="Mic">
                                            <label for="scales">Micronaire</label>
                                        </div>
                                        <div>
                                            <input type="checkbox" id="Mat" name="Mat">
                                            <label for="horns">Maturity</label>
                                        </div>
                                        <div>
                                            <input type="checkbox" id="Mst" name="Mst">
                                            <label for="horns">Moist</label>
                                        </div>
                                        <!--<div>
                                            <input type="checkbox" id="Fnn" name="Fnn">
                                            <label for="horns">Fineness</label>
                                        </div>-->
                                    </div>
                                    <div class="col-2">
                                        <!--<div>
                                            <input type="checkbox" id="Sgr" name="Sgr">
                                            <label for="scales">Sugar</label>
                                        </div>-->
                                        <div>
                                            <input type="checkbox" id="RD" name="RD">
                                            <label for="horns">RD</label>
                                        </div>
                                        <div>
                                            <input type="checkbox" id="B" name="B">
                                            <label for="horns">b</label>
                                        </div>
                                        <div>
                                            <input type="checkbox" id="Col" name="Col">
                                            <label for="scales">Color</label>
                                        </div>
                                    </div>
                                    <!--<div class="col-2">
                                        
                                        <div>
                                            <input type="checkbox" id="Temp" name="Temp">
                                            <label for="horns">Temperature</label>
                                        </div>
                                        <div>
                                            <input type="checkbox" id="Mst" name="Mst">
                                            <label for="horns">Moist</label>
                                        </div>
                                    </div>-->
                                </div>
                                <!--<p style = "text-align: right">
                                    <a href="#" id="Apply">Apply Changes</a> |
                                    <a href="#" id="desmarcarTodo">Uncheck All</a>  
                                </p>-->
                            </fieldset>

                            <!-- Div dimamicos dependiedo del checkbox seleccionado-->
                            <div id="parametros">
                                <div class="input-group" id="divGrd">
                                    <span class="input-group-text">Grade</span>
                                    <input type="text" class="form-control" id="GrdMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="GrdMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="GrdMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="GrdMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="GrdRange" placeholder="Range">
                                </div>
                                <div class="input-group" id="divTA">
                                    <span class="input-group-text">T. Area</span>
                                    <input type="text" class="form-control" id="TAMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="TAMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="TAMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="TAMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="TARange" placeholder="Range">
                                </div>
                                <div class="input-group" id="divTC">
                                    <span class="input-group-text">T. Code</span>
                                    <input type="text" class="form-control" id="TCMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="TCMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="TCMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="TCMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="TCRange" placeholder="Range">
                                </div>
                                <div class="input-group" id="divPC">
                                    <span class="input-group-text">P. Count</span>
                                    <input type="text" class="form-control" id="PCMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="PCMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="PCMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="PCMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="PCRange" placeholder="Range">
                                </div>
                                <div class="input-group" id="divLen">
                                    <span class="input-group-text">Length</span>
                                    <input type="text" class="form-control" id="LenMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="LenMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="LenMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="LenMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="LenRange" placeholder="Range">
                                </div>
                                <div class="input-group" id="divUI">
                                    <span class="input-group-text">Uniformity</span>
                                    <input type="text" class="form-control" id="UIMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="UIMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="UIMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="UIMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="UIRange" placeholder="Range">
                                </div>
                                <div class="input-group" id="divSFI">
                                    <span class="input-group-text">Short Fiber Index</span>
                                    <input type="text" class="form-control" id="SFIMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="SFIMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="SFIMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="SFIMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="SFIRange" placeholder="Range">
                                </div>
                                <div class="input-group" id="divStr">
                                    <span class="input-group-text">Strength</span>
                                    <input type="text" class="form-control" id="StrMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="StrMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="StrMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="StrMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="StrRange" placeholder="Range">
                                </div>
                                <div class="input-group" id="divElg">
                                    <span class="input-group-text">Elongation</span>
                                    <input type="text" class="form-control" id="ElgMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="ElgMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="ElgMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="ElgMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="ElgRange" placeholder="Range">
                                </div>
                                <div class="input-group" id="divMic">
                                    <span class="input-group-text">Micronaire</span>
                                    <input type="text" class="form-control" id="MicMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="MicMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="MicMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="MicMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="MicRange" placeholder="Range">
                                </div>
                                <div class="input-group" id="divMat">
                                    <span class="input-group-text">Maturity</span>
                                    <input type="text" class="form-control" id="MatMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="MatMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="MatMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="MatMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="MatRange" placeholder="Range">
                                </div>
                                <div class="input-group" id="divFnn">
                                    <span class="input-group-text">Fineness</span>
                                    <input type="text" class="form-control" id="FnnMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="FnnMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="FnnMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="FnnMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="FnnRange" placeholder="Range">
                                </div>
                                <div class="input-group" id="divSgr">
                                    <span class="input-group-text">Sugar</span>
                                    <input type="text" class="form-control" id="SgrMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="SgrMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="SgrMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="SgrMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="SgrRange" placeholder="Range">
                                </div>
                                <div class="input-group" id="divRD">
                                    <span class="input-group-text">RD</span>
                                    <input type="text" class="form-control" id="RDMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="RDMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="RDMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="RDMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="RDRange" placeholder="Range">
                                </div>
                                <div class="input-group" id="divB">
                                    <span class="input-group-text">B</span>
                                    <input type="text" class="form-control" id="BMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="BMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="BMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="BMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="BRange" placeholder="Range">
                                </div>
                                <div class="input-group" id="divCol">
                                    <span class="input-group-text">Color</span>
                                    <input type="text" class="form-control" id="ColMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="ColMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="ColMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="ColMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="ColRange" placeholder="Range">
                                </div>
                                <div class="input-group" id="divTemp">
                                    <span class="input-group-text">Temperature</span>
                                    <input type="text" class="form-control" id="TempMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="TempMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="TempMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="TempMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="TempRange" placeholder="Range">
                                </div>
                                <div class="input-group" id="divMst">
                                    <span class="input-group-text">Moist</span>
                                    <input type="text" class="form-control" id="MstMinO" placeholder="Min Original">
                                    <input type="text" class="form-control" id="MstMaxO" placeholder="Max Original">
                                    <input type="text" class="form-control" id="MstMinM" placeholder="Min Modificado">
                                    <input type="text" class="form-control" id="MstMaxM" placeholder="Max Modificado">
                                    <input type="text" class="form-control" id="MstRange" placeholder="Range">
                                </div>
                            </div>
                        </form>
                        <br>
                        <!--<div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="HVIO" value="HVIO" checked>
                                <label class="form-check-label" for="inlineRadio1">Original HVI</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="HVIM" value="HVIM">
                                <label class="form-check-label" for="inlineRadio2">Modified HVI</label>
                            </div>
                        </div>-->

                        <div id="HVIs" class="input-group input-group mb-3">
                            <!--Tablas HVI original-->
                            <div id="HVIOri">
                                <div style="width:550px; height:200px; overflow:auto;" id="AreaOriginal" ></div>
                            </div>
                            <!--Tablas HVI modificado-->
                            <div id="HVIMod">
                                <div style="width:550px; height:200px; overflow:auto;" id="AreaModificada" ></div>
                            </div>
                        </div>
                        <!--<table id="TablaRecap" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;" onscroll="myFunction()">
                            <thead style="background-color: #65ac7c;" style="opacity:100%;">
                                <tr>
                                    <th class="th-sm">HVI</th>
                                    <th class="th-sm">Valor</th>
                                    <th class="th-sm">Conteo</th>
                                </tr>
                            </thead>
                            <tbody id="TextAreaHVIO">
                            </tbody>
                        </table>
                        <table>
                        <thead><tr><td>Fecha</td><td>Título</td><td>Enlace</td></tr></thead>
                        <tbody id="TextAreaHVIO"></tbody>
                        </table>
                        <div id="HVIOri"><textarea class="form-control" id="TextAreaHVIO" rows="3" disabled></textarea></div>
                        <div id="HVIMod"><textarea class="form-control" id="TextAreaHVIM" rows="3" disabled>HVI Modificado</textarea></div>-->
                    </div>
                    <!--<div class="modal-footer">
                        <div style="height: 2.5em;" id="aviso"></div>
                        <button id="cer" type="button" class="btn btn-light" value="3" data-bs-dismiss="modal" >Close</button>
                        <button id="borrarFiltroU" type="button" value="4" class="btn btn-danger Filtro" >Clear filters</button>
                        <button id="buscafiltro" type="button" value="1" class="btn btn-primary Filtro" >Apply filters</button>
                    </div>-->
                    <div class="modal-footer">
                      <div style="height: 2.5em;" id="avisoUpdHVI"></div>
                        <!--<button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" id="btnGuardarFile" class="btn btn-dark">Import</button>-->
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal nuevas pacas -->
        <div class="modal fade" id="modalNewBales" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formFile">
                        <div class="modal-body">
                            <div class="input-group mb-3">
                                <label for="" class="col-form-label"></label>
                                <input type="file" class="form-control" id="csv" name="csv">
                            </div>
                            <div class="input-group mb-3">
                                  <label class="input-group-text" for="idprov">Supplier ID</label>
                                  <input  class="form-control me-2" type="text" name="SupIDNew" id="SupIDNew" maxlength="3" style="text-transform:uppercase;">
                                  <label class="input-group-text" for="DepRegFil">Supplier</label>
                                  <select class="form-control" name="Reg" id="Sup" disabled>
                                      <option value="">Choose...</option>
                                      <?php $i = 0; while($i < count($SupID)): ?>
                                      <option value="<?php echo $SupID[$i]; ?>"><?php echo $SupName[$i]; ?></option>
                                      <?php $i++; endwhile; ?>
                                  </select>
                            </div>
                            <div class="input-group mb-3">
                                  <label class="input-group-text" for="DepRegFil">Gin</label>
                                  <select class="form-control" name="GinCB" id="GinCB" disabled>
                                      <option value=""> </option>
                                      <?php $i = 0; while($i < count($gins)): ?>
                                      <option value="<?php echo $ginID[$i]; ?>"><?php echo $gins[$i]; ?></option>
                                      <?php $i++; endwhile; ?>
                                  </select>
                            </div>
                            <div class="input-group input-group mb-12">
                                <div id = SDO class="col-3">
                                    <input type="checkbox" id="CheckDev" value = 1 name="CheckDev">
                                    <label for="horns">Returns</label>
                                </div>
                            </div>
                        </div>
                        
                        <!-- ALERTA EN CASO DE QUE EL SUPPLIER NO EXISTA -->                                 
                        <div id="AlertaSupNew" style="display:none">
                            <div class="alert alert-warning d-flex align-items-center" role="alert">
                                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Warning:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                                <div>
                                    EL Supplier no existe
                                </div>
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                            <div style="height: 2.5em;" id="avisoNew"></div>
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" id="btnGuardarFile" class="btn btn-dark">Import</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Modal actualizar pacas -->
        <div class="modal fade" id="modalUpdateBales" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formUBales">
                        <div class="modal-body">

                            <label class="col-form-label" for="DepRegFil">Lots File</label>
                            <div class="input-group mb-3">
                                <label for="" class="col-form-label"></label>
                                <input type="file" class="form-control" id="csvU" name="csvU" required>
                            </div>
                            <label class="col-form-label" for="DepRegFil">Average File</label>
                            <div class="input-group mb-3">
                                <label for="" class="col-form-label"></label>
                                <input type="file" class="form-control" id="csvULots" name="csvULots" required>
                            </div>
                            <div class="input-group mb-3">
                                <label class="input-group-text" for="DepRegFil">Location</label>
                                <select class="form-select me-2" name="Reg" id="LocLot">
                                    <option value="">Choose...</option>
                                    <?php $i = 0; while($i < count($regs)): ?>
                                    <option value="<?php echo $regsID[$i]; ?>"><?php echo $regs[$i]; ?></option>
                                    <?php $i++; endwhile; ?>
                            
                                </select>
                            </div>
                            
                            <div class="row"> 
                                <div class="col-6 col-md-8">
                                
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="pacasID">
                                        <label class="form-check-label" for="inlineCheckbox1">File ID´s</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="pacasNum">
                                        <label class="form-check-label" for="inlineCheckbox2">File Complet</label>
                                    </div>        
                                </div> 
                            </div>             


                            <div id="FailFiles">
                                
                            </div>
                        </div>
                        <div class="modal-footer">
                        <div style="height: 2.5em;" id="avisoUpdate"></div>
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" id="btnGuardarFileU" class="btn btn-dark">Import</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        
        <!-- Modal actualizar liquidaciones -->
        <div class="modal fade" id="modalUpdateLiq" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formLiq">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="" class="col-form-label"></label>
                                        <input type="file" class="form-control" id="csvLiq" name="csvLiq">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                        <div style="height: 2.5em;" id="avisoLiq"></div>
                        <button id = "exportlist" type="button" class="btn btn-success" style="display:none;" data-bs-dismiss="modal">Export Excell</button>
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" id="btnGuardarFile" class="btn btn-dark">Import</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

         <!-- NUEVO MODAL PARA COMPLEMENTO DE LIQUIDACIONES PROVISIONALES -->
         <div class="modal fade" id="Liqprov" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formLiqprov">
                        <div class="modal-body">
                            <div class="row">
                                <label class="col-form-label" for="DepRegFil">Bales File</label>
                                <div class="input-group mb-3">
                                    <label for="" class="col-form-label"></label>
                                    <input type="file" class="form-control" id="csvprov" name="csvprov" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-group mb-3">
                                    <label class="input-group-text" for="idprov">Supplier ID</label>
                                    <input  class="form-control me-2" type="text" name="SupID" id="SupID" maxlength="3" style="text-transform:uppercase;">
                                   <!-- <label class="input-group-text" for="nameprov">Supplier Name</label>
                                    <input  class="form-control me-2" type="text" name="Supnom" id="Supnom" readonly> -->

                                   
                                    <label class="input-group-text" for="DepRegFil">Supplier Name</label>
                                    <select class="form-control" name="Reg" id="Supnom" disabled>
                                      <!--  <option value="">Choose...</option>-->
                                        <?php $i = 0; while($i < count($SupID)): ?>
                                        <option value="<?php echo $SupID[$i]; ?>"><?php echo $SupName[$i]; ?></option>
                                        <?php $i++; endwhile; ?>
                                    </select>
                           

                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-group mb-3">
                                    <label class="input-group-text" for="DepRegFil">Gin</label>
                                    <select class="form-control" name="Gin" id="GinName" disabled>
                                        <option value=""> </option>
                                        <?php $i = 0; while($i < count($gins)): ?>
                                        <option value="<?php echo $ginID[$i]; ?>"><?php echo $gins[$i]; ?></option>
                                        <?php $i++; endwhile; ?>
                                    </select>
                                </div>
                            </div>
                            <div id="FailFiles">
                                
                            </div>
                        </div>
                        <!-- ALERTA EN CASO DE QUE EL SUPPLIER NO EXISTA -->                                 
                        <div id="AlertaSup" style="display:none">
                            <div class="alert alert-warning d-flex align-items-center" role="alert">
                                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Warning:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                                <div>
                                    EL Supplier no existe
                                </div>
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                        <div style="height: 2.5em;" id="avisoTemp"></div>                            
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" id="btnGuardarFileU" class="btn btn-dark">Import</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
 

   <!-- Modal para asignar HVI a un cliente -->
   <div class="modal fade" id="modalcargahvi" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Asign HVI</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formcargahvi">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">                             
                                        <label for="" class="col-form-label">Planta<font size=2></font></label>
                                        <select class="form-control form-control-sm" style="text-transform:uppercase;" id="planta" required>                                                                      
                                                <?php $i = 0; while($i < count($clientes)): ?>
                                                <option value="<?php echo $clientes[$i]['CliID']; ?>"><?php echo $clientes[$i]['Cli'] ?></option>
                                                <?php $i++; endwhile; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Lotes</label>
                                        <input type="text" class="form-control form-control-sm" id="loteshvi">
                                    </div>
                                </div> 
                               <br>                     
                               <!-- <div class="col-lg-12">                         
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Ubicación Lote</label>
                                        <select class="form-select me-2" name="Reg" id="Regionlote">
                                            <option value="">Choose...</option>
                                            <?php $i = 0; while($i < count($regs)): ?>
                                            <option value="<?php echo $regsID[$i]; ?>"><?php echo $regs[$i]; ?></option>
                                            <?php $i++; endwhile; ?>
                                    
                                        </select>
                                    </div> 
                                </div> -->
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Correos AMSA</label>
                                        <input type="text" class="form-control form-control-sm" id="correoamsa">
                                    </div>
                                </div> 
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Correos Cliente</label>
                                        <input type="text" class="form-control form-control-sm" id="correocliente">
                                    </div>
                                </div> 
                            </div>
                            <br>
                            <div class="row"> 
                                <div class="col-6 col-md-8">
                                
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="adjuntarpdf">
                                        <label class="form-check-label" for="inlineCheckbox1">Adjuntar PDF</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="adjuntarexcel">
                                        <label class="form-check-label" for="inlineCheckbox2">Adjuntar Excel</label>
                                    </div>        
                                </div> 
                            </div>     
                            <br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Comentarios Correo</label>
                                        <textarea rows="4" cols="50" class="form-control form-control-sm" id="comentarioscorreo"></textarea>
                                    </div>
                                </div>
                            </div>

                                   
                        </div>
                        <div class="modal-footer">
                            <div style="height: 2.5em;" id="avisocargahvi"></div>
                            <button id = "asignarlote" type="button" class="btn btn-dark" >Assign Lots</button>   
                            <!-- <button id = "loadexcel" type="button" class="btn btn-success">Load HVI Excel</button>                            
                            <button type="button" id="loadpdf" class="btn btn-danger" >Load HVI PDF</button>-->
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Aquí termina -->
        
        <!-- <script>
            function myFunction() {
                var elmnt = document.getElementById("TextAreaHVIO");
                var elmnt2 = document.getElementById("TextAreaHVIM");
                var x = elmnt.scrollLeft;
                var y = elmnt.scrollTop;
                elmnt2.scrollLeft = x;
                elmnt2.scrollTop = y;
                
            }
        </script> -->
        
    </body>

    </html>
<?php
endif;
?>