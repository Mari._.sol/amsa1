<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="img/ecom.png" />  
    <title>Lots</title>
      
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <!-- CSS personalizado --> 
    <link rel="stylesheet" href="main.css">  
      
      
    <!--datables CSS básico-->
    <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css">
    <!--datables estilo bootstrap 4 CSS-->  
    <link rel="stylesheet"  type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">  
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link rel="" href="https://cdn.datatables.net/fixedheader/3.1.6/css/fixedHeader.dataTables.min.css">
    <link rel="" href="https://cdn.datatables.net/fixedcolumns/3.3.2/js/dataTables.fixedColumns.min.js">
    
    <!--font awesome con CDN-->  
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    
  </head>
    
  <body>
<?php session_start();
if(!isset($_SESSION['username'])) header("Location:index.php");
?>
     <div class="main-pos"> 	
     <div class="container">
      <div class="mx-auto col-lg-12 main-section" id="myTab" role="tablist">
        <div class="w3-bar w3-light-gray">
         <a href="#" class="w3-bar-item w3-button">Home</a>
         <a href="trucks.php" class="w3-bar-item w3-button">Trucks</a>
         <a href="lots.php" class="w3-bar-item w3-button">Lots</a>
         <a href="do.php" class="w3-bar-item w3-button">Delivery Order</a>
         <a href="#" class="w3-bar-item w3-button">Invoice</a>
         <div class="w3-dropdown-hover w3-right">
          <button class="w3-button"><?php echo $_SESSION['username']; ?></button>
          <div class="w3-dropdown-content w3-bar-block w3-card-4">
           <a href="logout.php" class="w3-bar-item w3-button w3-right">Logout</a>
          </div>
         </div> 
        </div> 
      </div>
     </div> 
     </div>
     <br><br><br>
 
     <header>
     <h3 class='text-center'></h3>
     </header>    
    
  
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <!--<button id="btnEXC" type="button" class="btn btn-success" ><i class="material-icons far fa-file-excel"></i></button>
            <button id="btnPDF" type="button" class="btn btn-danger" ><i class="material-icons far fa-file-pdf"></i></button>-->
            <!--<button id="btnNuevo" type="button" class="btn btn-info" data-toggle="modal"><i class="material-icons">library_add</i></button>-->    
            </div>    
        </div>    
    </div>    
    <br>  

    <div class="container caja">
        <div class="row">
            <div class="col-lg-12">
	    <div class=" card card-body">
            <div class="table-responsive">        
                <table id="tablaUsuarios" class="table table-striped table-bordered table-sm display nowrap" cellspacing="0" style="width:100%" >
                    <thead class="text-center thead-light">
                        <tr>
                            <th class="th-sm">Lot ID</th>
                            <th class="th-sm">Lot</th>
                            <th class="th-sm">Gin ID</th>                                
                            <th class="th-sm">Qty</th>  
                            <th class="th-sm">LiqWgh</th>
                            <th class="th-sm">DO</th>
			    <th class="th-sm">Truck ID</th>
			    <th class="th-sm">Inv ID</th>
                            <th class="th-sm">Status</th>
                            <!--<th class="th-sm">Edit</th>-->
                        </tr>
                    </thead>
                    <tbody>                          
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="th-sm">Lot ID</th>
                            <th class="th-sm">Lot</th>
                            <th class="th-sm">Gin ID</th>                                
                            <th class="th-sm">Qty</th>  
                            <th class="th-sm">LiqWgh</th>
                            <th class="th-sm">DO</th>
			                <th class="th-sm">Truck ID</th>
			                <th class="th-sm">Inv ID</th>
                            <th class="th-sm">Status</th>
                            <!--<th class="th-sm">Edit</th>-->
                        </tr>
                    </tfoot>      
                </table>               
            </div>
	    </div> <!--card-->
            </div>
        </div>  
    </div>   

<!--Modal para CRUD-->
<div class="modal fade" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
        <form id="formUsuarios">    
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Lot:</label>
                    <input type="text" class="form-control" id="Lot">
                    </div>
                    </div>
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Gin ID</label>
                    <input type="text" class="form-control" id="GinID">
                    </div> 
                    </div>    
                </div>
                <div class="row"> 
                    <div class="col-lg-4">
                    <div class="form-group">
                    <label for="" class="col-form-label">Quantity</label>
                    <input type="text" class="form-control" id="Qty" pattern="[0-9]+">
                    </div>               
                    </div>
                    <div class="col-lg-4">
                    <div class="form-group">
                    <label for="" class="col-form-label">Liq Wgh</label>
                    <input type="text" class="form-control" id="LiqWgh">
                    </div>
                    </div>
		    <div class="col-lg-4">
                    <div class="form-group">
                    <label fro="" class="col-form-label">Delivery Order</label>
                    <input type="text" class="form-control" id="DOrd"> 
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                        <label for="" class="col-form-label">Truck ID</label>
                        <input type="text" class="form-control" id="TrkID">
                        </div>
                    </div>    
                    <div class="col-lg-5">    
                        <div class="form-group">
                        <label for="" class="col-form-label">Invoice</label>
                        <input type="text" class="form-control" id="InvID">
                        </div>            
                    </div>
		    <div class="col-lg-4">
			<div class="form-group">
			<label for="" class="col-form-label">Status</label>
			<!--<input type="text" class="form-control" id="Status">-->
			<select class="form-control" id="Status" name="Status">
			 <option value="Enable">Enable</option>
			 <option value="Disable">Disable</option>
			</select>
			</div>
		    </div>    
                </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                <button type="submit" id="btnGuardar" class="btn btn-dark">Save</button>
            </div>
        </form>    
        </div>
    </div>
</div>  
      
    <!-- jQuery, Popper.js, Bootstrap JS -->
    <script src="assets/jquery/jquery-3.3.1.min.js"></script>
    <script src="assets/popper/popper.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <!--<script src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js"></script>-->
    
    <!-- para usar botones en datatables JS
    <script src="assets/datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>  
    <script src="assets/datatables/JSZip-2.5.0/jszip.min.js"></script>    
    <script src="assets/datatables/pdfmake-0.1.36/pdfmake.min.js"></script>    
    <script src="assets/datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
    <script src="assets/datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
    <!--<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>-->
    
    <!-- datatables JS -->
    <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>
    <!-- extension responsive -->
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>  
     
    <script type="text/javascript" src="mainLots.js"></script>  
    
    
  </body>
</html>
