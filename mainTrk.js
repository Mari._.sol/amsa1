var idglobaltruck = 0;
var LotID=0;
var activado = 0;
var bandera = "";
var ValOutWgh = 0;
var ValInWgh = 0;
var DepReg,ArrReg,Cli

$(document).ready(function() {
    //iniciamos el multi selector
    $('.selectpicker').selectpicker({
        liveSearch: false
    });
    
    $('#tablaTrk tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    } );

    var TrkID = "", opcion;
    opcion = 4;

    tablaTrk = $('#tablaTrk').DataTable({
        //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			{
			    extend:    'excelHtml5',
			    text:      '<i class="fas fa-file-excel"></i> ',
			    titleAttr: 'Export to Excel',
			    className: 'btn btn-success',
                download: 'open',
			    exportOptions: {
			        columns: ":not(.no-exportar)"
			    }
			},
			{
			    extend:    'pdfHtml5',
			    text:      '<i class="fas fa-file-pdf"></i> ',
			    titleAttr: 'Export to PDF',
			    orientation: 'landscape',
			    className: 'btn btn-danger',
                download: 'open',
			    exportOptions: {
			        columns: ":not(.no-exportar)"
			    }
			},
            {
                text: '<i class="bi bi-funnel-fill"></i>',
                titleAttr: 'Filter by',
                orientation: 'landscape',
                className: 'btn btn-info btn-filtrar',
                attr:{
                        id:'filtraboton',
                        "data-toggle": 'modal tooltip',
                        "disabled": false
                }
            },
			/*{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},*/
        ],
    
        initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                        .search( this.value, true, false ).draw();
                            
                    }
                } );
            } );
        },
        "order": [ 0, 'desc' ],
        "scrollX": true, 
        "scrollY": '50vh', //"340px",
        "scrollCollapse": true,
        "lengthMenu": [100,200,300,500],
        fixedColumns:   {
            left: 0,
            right: 1
        },
        "ajax":{            
            "url": "bd/crudTrk.php",
            "method": 'POST', //usamos el metodo POST
            "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
            "dataSrc":""
        },
        "columns":[
            {"data": "TrkID"},
            {"data": "DO"},
            {"data": "Typ"},
            {"data": "LotsAssc"}, //LotsAssc
            {"data": "LotQty"}, //CrgQty
            {"data": "SchOutDate"},
            {"data": "OutDat"},
            {"data": "SchOutTime"},
            {"data": "OutTime"},
            {"data": "SchInDate"},
            {"data": "InDat"},
            {"data": "SchInTime"},
            {"data": "InTime"},
            {"data": "GinName"},
            {"data": "RegNameOut"},
            {"data": "RegNameIn"}, //Sigue contrato
            {"data": "Ctc"},
            {"data": "PlcNameIn"}, //Consulta con DO 
            {"data": "TNam"},
            {"data": "WBill"},
            {"data": "DrvLcs"},
            {"data": "TrkLPlt"},
            {"data": "TraLPlt"},
            {"data": "DrvNam"},
            {"data": "DrvTel", width: 200, render: function ( toFormat ) {
                return toFormat.toString().replace(
                  /(\d\d\d)(\d\d\d)(\d\d\d\d)/g, '$1-$2-$3'
                ); }},
            {"data": "LiqWgh", render: $.fn.dataTable.render.number( ',')}, //render: $.fn.dataTable.render.number( ',', '.', 2)}, //PWgh  
            {"data": "OutWgh", render: $.fn.dataTable.render.number( ',')}, //render: $.fn.dataTable.render.number( ',', '.', 2)}, //render: $.fn.dataTable.render.number( ',')},
            {"data": "InWgh", render: $.fn.dataTable.render.number( ',')}, //render: $.fn.dataTable.render.number( ',', '.', 2)},
            {"data": "RqstID"},
            {"data": "PO"},
            {"data": "RO"},
            {"data": "Cost"},
            {"data": "Comments"},
            {"data": "Status"},
            {"data": "IrrDat"}, //<button class='btn btn-success btn-sm btnBuscar'><i class='material-icons'>manage_search</i></button>
            {"data": "DateCreated", width: 200},
            {"data": "RemisionPDF"},
            {"data": "XML"},
            {"data": "Samples"},
            {"data":"Incident"},
            {"data":"Invoice"},
            {"data":"TicketPeso"},
            {"data":"NumEcon"},  
            {"data": "Cert", // aqui 
            "render": function(data) { 
                return data == "1" ? "YES" : "NO"; 
                } 
            }, 
            {"data":"FileCertificados",
                "render": function(data) { 
                    return data == '' ||data == null ? 'NO' : 'YES'
                }
            }, 
            {"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-success btn-sm btnremision' title='Remission'><i class='material-icons'>local_shipping</i></button><button class='btn btn-primary btn-sm btnEditar' title='Edit'><i class='material-icons'>edit</i></button><button class='btn btn-info btn-sm btnPDF-REM' title='Letter Remission'><i class='material-icons'>picture_as_pdf</i></button><button class='btn btn-primary btn-sm btnTag' title='Hashtag'><i class='material-icons'>local_offer</i></button><button class='btn btn-primary btn-sm previewEmail' title='send mail'><i class='bi bi-envelope-check-fill'></i></button><button class='btn btn-warning btn-sm btnincidente' title='Add incident'><i class='material-icons'>textsms</i></button></div></div>"}
        ],
       
       
       columnDefs : [
       
            //COST
            { targets : [31],
                render : function (data, type, row) {
                  return data == '1' ? 'YES' : 'NO'
                }
              },

              //PDF CARTA PORTE
              { targets : [36],
                render : function (data, type, row) {
                  return data == '1' ? 'YES' : 'NO'
                }
              },
              //XML
              { targets : [37],
                render : function (data, type, row) {
                  return data == '1' ? 'YES' : 'NO'
                }
              },

              //Muestras
              { targets : [38],
                render : function (data, type, row) {
                  return data == '1' ? 'True' : 'False'
                }
              },
            //INCIDENT
            { targets : [39],
                render : function (data, type, row) {
                    return data == '1' ? 'YES' : 'NO'
                }
            },
            //INVOICE
            { targets : [40],
                render : function (data, type, row) {
                    return data == '' ||data == null ? '0' : data
                }
            },

            //TICKET PESO
            { targets : [41],
                render : function (data, type, row) {
                    return data == '' ||data == null ? 'NO' : 'YES'
                }
            }
       ]
   
    });     

    $('.dataTables_length').addClass('bs-select');

    /*function LoadLots(){
            $("#LotsAssc").empty();
            $('#AsscLots').val("");
            $('#QtyDO').val("");
            $('#PWgh').val("");
            var opts = "";
            opcion = 6;
            DO = $.trim($('#DOrd').val());
            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",
                data:  {DO:DO ,opcion:opcion},    
                success: function(data){
                    opts = JSON.parse(data);
                    for (var i = 0; i< opts.length; i++){
                        $('#LotsAssc').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '-' + opts[i].Qty + ' bc </option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });
    };*/
    
    
    var fila; //captura la fila, para editar o eliminar -- submit para el Alta y Actualización
    $('#formTrk').submit(function(e){
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
        DOrd = $.trim($('#DOrd').val());
        RegDO = $.trim($('#OutRegDO').val()); //Obtener region de salida
        TrkLPlt = $.trim($('#TrkLPlt').val().toUpperCase());    
        TraLPlt = $.trim($('#TraLPlt').val().toUpperCase());
        TNam = $.trim($('#TNam').val());    
        WBill = $.trim($('#WBill').val().toUpperCase());
        DrvLcs = $.trim($('#DrvLcs').val().toUpperCase());
        DrvNam = $.trim($('#DrvNam').val().toUpperCase());
        DrvTel = $.trim($('#DrvTel').val()).replace(/-/g, "");
        OutDat = $.trim($('#OutDat').val());
        OutTime = $.trim($('#OutTime').val());
        OutWgh = $.trim($('#OutWgh').val()).replace(/,/g, "");
        InDat = $.trim($('#InDat').val());
        InTime = $.trim($('#InTime').val());
        InWgh = $.trim($('#InWgh').val()).replace(/,/g, "");
        TrkCmt = $.trim($('#TrkCmt').val().toUpperCase());
        RqstID = $.trim($('#RqstID').val());
        RO = $.trim($('#RO').val());
        AsscLots = $.trim($('#AsscLots').val().toUpperCase());
        CrgQty = $.trim($('#QtyDO').val());
        PWgh = $.trim($('#PWgh').val()).replace(/,/g, "");
        Status = $.trim($('#Status').val());
        IrrDat = $.trim($('#IrrDat').val());
        NumEcon = $.trim($('#NumEcon').val());
        //Datos de maniobras
        ManiobraLlegada=$('#ManiobraLlegada').val();    
        RespManLleg=$('#RespManLleg').val();   
        ManiobraSalida=$('#ManiobraSalida').val();  
        RespManSalida=$('#RespManSalida').val();   
        NotaSalDep=$('#NotaSalida').val();  
        NotaSalArr = $('#NotaLlegada').val();  

        
        //Stop si promedio son menor a 200 y mayor a 240
        promsalida = $.trim($('#promsalida').val());
        promllegada = $.trim($('#promllegada').val());
        User = $.trim($('#dropdownMenuLink2').val());
        
        if (ValOutWgh != 0 && OutWgh != 0 && promsalida < 200 && User != "ACENCION PANI" && User != "JOSUE ABISAI REYES" && User != "DARIO ROMERO"){
          alert ("Verify that the departure average weight is greater than 200 kg");
        }else if (ValOutWgh != 0 && OutWgh != 0 && promsalida > 240 && User != "ACENCION PANI" && User != "JOSUE ABISAI REYES" && User != "DARIO ROMERO"){
          alert ("Verify that the departure average weight is not greater than 240 kg");
        }else if (ValInWgh != 0 && InWgh != 0 && promllegada < 200 && User != "ACENCION PANI" && User != "JOSUE ABISAI REYES" && User != "DARIO ROMERO"){
          alert ("Verify that the arrival average weight is greater than 200 kg"); 
        }else if (ValInWgh != 0 && InWgh != 0 && promllegada > 240 && User != "ACENCION PANI" && User != "JOSUE ABISAI REYES" && User != "DARIO ROMERO"){
          alert ("Verify that the arrival average weight is not greater than 240 kg");
        }else{      
        
            if (RegDO == "USA"){
                OutWgh = PWgh;
            }

            if( $('#samples').prop('checked') ) {
                samples = 1;
            }  else {
                samples = 0;
            }
            if (Status != Status2){
                ban = 1;
            }else{
                ban = 0;
            }

        //validar que el tki tenga un lote antes de asiganr una PR
        if(CrgQty ==0 && RqstID !="" && RqstID!=0 ){ 
            alert("You must assign a lot to allocate a PR");
        }
        else{
        $(".btnEditar").prop("disabled", true);
        if (opcion == 2){
        //usuario que actualiza estatus
        usermod= $("#dropdownMenuLink2").val();
        $("#btnGuardar").prop("disabled", true);
     
            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",    
                data:  {ban:ban, TrkID:TrkID, TrkLPlt:TrkLPlt, TraLPlt:TraLPlt, TNam:TNam, WBill:WBill, DrvLcs:DrvLcs, DrvNam:DrvNam, DrvTel:DrvTel, CrgQty:CrgQty, PWgh:PWgh, OutDat:OutDat, OutTime:OutTime, OutWgh:OutWgh,InDat:InDat, InTime:InTime, InWgh:InWgh, TrkCmt:TrkCmt, DOrd:DOrd, RqstID:RqstID, RqstID1:RqstID1, RO:RO, AsscLots:AsscLots, Status:Status, IrrDat:IrrDat, opcion:opcion,samples:samples,lot_intrk:lot_intrk,usermod:usermod,NumEcon:NumEcon,ManiobraLlegada:ManiobraLlegada,RespManLleg:RespManLleg,ManiobraSalida:ManiobraSalida,RespManSalida:RespManSalida,NotaSalDep:NotaSalDep,NotaSalArr:NotaSalArr}, //AsscLots:AsscLots,
                success: function(data) {
                    $('#tablaTrk').on('draw.dt', function () {
                        $(".btnEditar").prop("disabled", false);
                    });
                    tablaTrk.ajax.reload(null,false);
                    ban=0;
                    $("#btnGuardar").prop("disabled", false);
                    $('#modalCRUD').modal('hide');
                    $('#exampleModalLabel').val("");
                }
            });
        }else{
            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",    
                data:  {ban:ban, TrkID:TrkID, TrkLPlt:TrkLPlt, TraLPlt:TraLPlt, TNam:TNam, WBill:WBill, DrvLcs:DrvLcs, DrvNam:DrvNam, DrvTel:DrvTel, CrgQty:CrgQty, PWgh:PWgh, OutDat:OutDat, OutTime:OutTime, OutWgh:OutWgh,InDat:InDat, InTime:InTime, InWgh:InWgh, TrkCmt:TrkCmt, DOrd:DOrd, RqstID:RqstID, RqstID1:RqstID1, RO:RO, AsscLots:AsscLots, Status:Status, IrrDat:IrrDat, opcion:opcion,samples:samples,lot_intrk:lot_intrk,NumEcon:NumEcon,     ManiobraLlegada:ManiobraLlegada,RespManLleg:RespManLleg,ManiobraSalida:ManiobraSalida,RespManSalida:RespManSalida,NotaSalDep:NotaSalDep,NotaSalArr:NotaSalArr}, //AsscLots:AsscLots,
                success: function(data) {

                    $('#tablaTrk').on('draw.dt', function () {
                        $(".btnEditar").prop("disabled", false);
                    });
                    tablaTrk.ajax.reload(null,false);
                    $('#divCreate').hide();
                    $('#LotsTrk').show();
                    $('#divAsociar').show();
                    $("#OutWgh").prop("disabled", false);
                    $("#InWgh").prop("disabled", false);      
                    var opts = JSON.parse(data);
                    TrkID = opts[0].TrkID;

                        opcion = 2; 
                        $('#exampleModalLabel').val(TrkID);
                        document.getElementById('removelots').style.display = 'none';
                        document.getElementById('btnupdate').style.display = 'none';
                        document.getElementById('btnloadfile').style.display = 'none';

                        document.getElementById('btnloadxml').style.display = 'none';
                        document.getElementById('btnupdatexml').style.display = 'none';

                        document.getElementById('load_cert').style.display = 'none';
                        document.getElementById('btnupdateCert').style.display = 'none';

                        document.getElementById('load_ticket').style.display = 'none';
                        document.getElementById('update_ticket').style.display = 'none';   
                        document.getElementById('btntarifario').style.display = 'block';           
                        //$("#RqstID").prop("disabled", true);
                    }
                });
            }
            }
        }
    });
    
    
    //submit para Carga masiva PO's
$('#formFile').submit(function(e){
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
    var comprobar = $('#csv').val().length;
    var extension = $('#csv').val().split(".").pop().toLowerCase();
    //validar extension
    /*if($.inArray(extension, ['csv']) == -1)
	{
		alert("Invalid File");
		retornarError = true;
		$('#csv').val("");
	}else{*/
    if (comprobar>0){
        var formulario = $("#formFile");
        var archivos = new FormData();
        var url = "bd/procesarPO.php";
            for (var i=0; i<(formulario.find('input[type=file]').length); i++){
                archivos.append((formulario.find('input[type="file"]:eq('+i+')').attr("name")),((formulario.find('input[type="file"]:eq('+i+')'))[0].files[0]));
            }
    $.ajax({
          url: "bd/procesarPO.php",
          type: "POST",
          contentType: false,
          data:  archivos,
          processData: false,
          success: function(data) {
            //var opts = JSON.parse(data);
            //alert (opts);
            //alert (data);
            if (data == 'OK'){
                alert("Successful Import");
                $('#modalFile').modal('hide');
                //location.reload();
                return false;
            } else if (data == 'Columns'){
                alert("Check the number of columns.");
                return false;
            }else if (data == 'Exist'){
                alert ("Existen los lotes");
                return false;
            }else{
                alert("Successful Import"); //alert("Import Error");
                $('#modalFile').modal('hide');
                //location.reload();
                return false;
           }
          }
        });
        return false;    
    }else{
        alert ("Select Excel file to import");
        return false;
    }
    //}   	        
    //$('#modalFile').modal('hide');
    //location.reload();
});
    
    
    
    // Formulario Buscar OE y Lots  *comentario 4 junio****
    $('#formOEL').submit(function(e){                         
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
        TrkLPlt = $.trim($('#TrkLPlt').val());    
        TraLPlt = $.trim($('#TraLPlt').val());
        TNam = $.trim($('#TNam').val());                             
        $.ajax({
            url: "bd/crudTrk.php",
            type: "POST",
            datatype:"json",    
            data:  {TrkID:TrkID, TrkLPlt:TrkLPlt,opcion:opcion},    
            success: function(data) {
                tablaTrk.ajax.reload(null, false);
            }
        });			        
        //$('#modalOEL').modal('hide');											     			
    });


    // Formulario Splits ***************************************************************************
    $('#Split-Lots').submit(function(e){                         
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
        var ban = 0;
        LocSplit = $.trim($('#LocSplit').val());    
        DOSplit = $.trim($('#DOSplit').val());
        // Obtener texto del selector y la cantidad de pacas totales del lote ******
        var SelectLot = document.getElementById("LotsSplit");
        var LotTxt = SelectLot.options[SelectLot.selectedIndex].text;
        LotIDSplit = $.trim($('#LotsSplit').val());
        BC = LotTxt.split(" - ");
        BC = BC[1];
        Bal = LotTxt.split(" - ");
        Bal = Bal[0];
        // ***************************************************************************
        Split = $.trim($('#Split').val());
        Split = Split.split(",");
        tam = Split.length;
        //validar que las particiones sean positivas
        for (i=0; i<=tam; i++){
            if (Split[i] < 0){
                ban = 1;
            }
        }
        if (ban == 1){
            alert ("Partitions must be positive");
        }else{
        
            sum = 0;
            $.each(Split,function(){sum+=parseFloat(this) || 0; }); // suma de las particiones
            if (sum>BC || sum<BC){
                alert ("The sum total of the partitions must match the number of bales in the Lot");    
            }else{
                opcion = 14;
                var respuesta = confirm("Are you sure you want to partition the Lot "+Bal+" in "+Split+"?");                
                if (respuesta) {            
                    $.ajax({
                        url: "bd/crudTrk.php",
                        type: "POST",
                        datatype:"json",    
                        data:  {LotIDSplit:LotIDSplit, Split:Split, opcion:opcion},    
                        success: function(data) {
                            tablaTrk.ajax.reload(null,false);
                        $('#SplitLots').modal('hide');
                        }
                    });
                   // $('#avisoLot').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');      
	            
                }
            }
        }
        
        /*$.ajax({
            url: "bd/crudTrk.php",
            type: "POST",
            datatype:"json",    
            data:  {TrkID:TrkID, TrkLPlt:TrkLPlt,opcion:opcion},    
            success: function(data) {
                tablaTrk.ajax.reload(null, false);
            }
        });*/			        
        //$('#SplitLots').modal('hide');											     			
    });


   //eliminar PO y PR
   $("#RemPOPR").click(function(){ //$('#RemPOPR').submit(function(e){
    //e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
    TrkID = $.trim($('#RTrkID').val());
    comentario = $.trim($('#RComent').val());

    if($('#checkPO').prop('checked') || $('#checkPR').prop('checked') ){
            if($('#checkPO').prop('checked')){
                opcion = 17;
            }
            if($('#checkPR').prop('checked')){
                opcion = 21;

            }
            if($('#checkPO').prop('checked') && $('#checkPR').prop('checked')){
                opcion = 22;
            }
            
            if(comentario !=""){
                var respuesta = confirm("Are you sure you want to remove the PO or PR from this TrkID "+TrkID+"?");                
                if (respuesta) {
                    var RemPO =  $.ajax({
                        type: 'POST',
                        url: 'bd/crudTrk.php',
                        data: { TrkID:TrkID, comentario:comentario, opcion:opcion}
                    }) 

                    RemPO.done(function(data){
                
                    // console.log(data);
                        tablaTrk.ajax.reload(function(){
                                $('#avisoPOPR').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Datos eliminados con éxito</div>')
                                setTimeout(function() {
                                    $('#alert1').fadeOut(1000);
                                    $("#POPR")[0].reset()                                
                                    $("#checkPO").prop("checked", false);
                                    $("#checkPR").prop("checked", false);
                                    $('#RemPOPR').prop('disabled', true);
                                    $('#checkPO').prop('disabled', true);
                                    $('#checkPR').prop('disabled', true);
                                },1000);
                        });
                    });
                }

                document.getElementById('alertaremove').style.display = 'none';     


            }
            else{
                document.getElementById('alertaremove').style.display = 'block';
            }
        }
        else{
            alert("check one option");
        }
});



        
    //para limpiar los campos antes de dar de Alta una Persona
    $("#btnNuevo").click(function(){
        $('#AvisoWgh').html("");
        $('#AvisoWghIn').html("");
        localStorage.clear();   
        clear_maniobras();
        $('#OutDat').prop('disabled', false);
        $('#OutTime').prop('disabled', false);
        $('#OutWgh').prop('disabled', false);        
        $('#InDat').prop('disabled', false);
        $('#InTime').prop('disabled', false);
        $('#InWgh').prop('disabled', false);
        $("#Status").prop("disabled", false);
        $("#IrrDat").prop("disabled", false);
        $("#NoPWgh").hide();
        $("#NoWgh").hide();
        $("#OutWghMin").hide();
        $("#OutWghMax").hide();
        $("#InWghMin").hide();
        $("#InWghMax").hide();
        $("#LotsTrk").hide(); //Ocultar lotes asociados
        $("#divCreate").show(); //Ocultar lotes asociados
        $("#divAsociar").hide(); //Ocultar lotes asociados
        $("#TNam").empty();
        $("#RqstID").empty();
        $("#LotsAssc").empty();
        $("#DOrd").prop("disabled", false);
        $('#btnGuardar').attr('disabled',false);
         
        $("#OutWgh").prop("disabled", true);
        $("#InWgh").prop("disabled", true);
        //ValidateWgh();
        Status2 = "Programmed";
        ban = 0;
        opcion = 1; //alta
        TrkID=null;
        RqstID1=null;
        $("#formTrk").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("New Truck");
        $('#modalCRUD').modal('show');	    
        document.getElementById('documento').style.display = 'none';
        document.getElementById('documentoxml').style.display = 'none';
        document.getElementById('btntarifario').style.display = 'none';
        document.getElementById('descarga_cert').style.display = 'none';
        document.getElementById('actualfile_certicados').style.display = 'none';   
    });
    
    
    //carga PO's
    $("#btnFile").click(function(){
        $("#formFile").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Import Data");
        $('#modalFile').modal('show');	    
    });


    //Boton para split de lotes
    $("#btnSplit").click(function(){
        $("#LocSplit").empty();
        $("#DOrdSplit").empty();
        $("#LotsSplit").empty();
        LoadRegion();
        $('#LocSplit').append('<option value="" disabled selected >- Select -</option>');
        //ValidateWgh();
        //opcion = 1; //alta
        //TrkID=null;
        $("#Split-Lots").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Split a Lot");
        $('#SplitLots').modal('show');	    
    });

    //Bloquear maniobras en MP2 y MG2
    /*$("#ManiobraSalida").change(function () {

        if( $(this).val() =="MP2" ||  $(this).val() == "MG2"){
            $("#ManiobraLlegada").prop("disabled", true);
            $("#RespManLleg").prop("disabled", true);
            $("#ManiobraLlegada").val("");   
            $("#RespManLleg").val(""); 
        }
        else{       

            $("#ManiobraLlegada").prop("disabled", false);
            $("#RespManLleg").prop("disabled", false);
          
        }
      }); */

    //Editar        
    $(document).on("click", ".btnEditar", function(){
        $('#AvisoWgh').html("");
        $('#AvisoWghIn').html("");
        TrkID = "";
        $("#samples").prop("checked", false);
        clear_maniobras()
        //Privilegios = $.trim($('#Priv').val());
        //alert (Privilegios);
        $("#NoPWgh").hide();
        $("#NoWgh").hide();
        $("#OutWghMin").hide();
        $("#OutWghMax").hide();
        $("#InWghMin").hide();
        $("#InWghMax").hide();
        $("#LotsTrk").show(); //Mostrar lotes asociados
        $("#divCreate").hide(); //Mostrar lotes asociados
        $("#divAsociar").show(); //Mostrar lotes asociados
        //$("#formTrk").trigger("reset");
        $("#TNam").empty();
        $("#RqstID").empty();
        //$("#FreightC").val("");
        $('#btnGuardar').attr('disabled',false);
        //ValidateWgh();  
        ban = 0;
        opcion = 2;//editar
        fila = $(this).closest("tr");	  
        idglobaltruck = parseInt(fila.find('td:eq(0)').text());           
        TrkID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID		            
        DOrd = parseFloat(fila.find('td:eq(1)').text());
        Typ = fila.find('td:eq(2)').text();
        LotsAssc = fila.find('td:eq(3)').text();
        Qty = parseFloat(fila.find('td:eq(4)').text());       
        OutDat = fila.find('td:eq(6)').text();     
        OutTime = fila.find('td:eq(8)').text();     
        InDat = fila.find('td:eq(10)').text();
        InTime = fila.find('td:eq(12)').text();
        DepReg = fila.find('td:eq(14)').text();
        ArrReg = fila.find('td:eq(15)').text();
        Cli = fila.find('td:eq(17)').text();
        //TNam = fila.find('td:eq(12)').text(); 
        WBill = fila.find('td:eq(19)').text();
        DrvLcs = fila.find('td:eq(20)').text();
        TrkLPlt = fila.find('td:eq(21)').text();
        TraLPlt = fila.find('td:eq(22)').text();
        DrvNam = fila.find('td:eq(23)').text();
        DrvTel = fila.find('td:eq(24)').text();
        PWgh = fila.find('td:eq(25)').text();
        OutWgh = fila.find('td:eq(26)').text();
        InWgh = fila.find('td:eq(27)').text();
        PO = fila.find('td:eq(29)').text();
        RO = parseFloat(fila.find('td:eq(30)').text());
        TrkCmt = fila.find('td:eq(32)').text();
        Status = fila.find('td:eq(33)').text();
        Status2 = fila.find('td:eq(33)').text(); //Estatus auxiliar
        Costo = fila.find('td:eq(31)').text();
        IrrDat = fila.find('td:eq(34)').text();   
        pdfload = fila.find('td:eq(36)').text();
        xmlload = fila.find('td:eq(37)').text();
        samples = fila.find('td:eq(38)').text();
        ticket = fila.find('td:eq(41)').text();
        NumEcon = fila.find('td:eq(42)').text();
        ManiobraSal = fila.find('td:eq(43)').text();
        ManiobraLleg = fila.find('td:eq(44)').text();
        CertOrigen = fila.find('td:eq(44)').text();
        console.log("Certificado de origen",CertOrigen)
        document.getElementById('btntarifario').style.display = 'block';
        document.getElementById('ticket').style.display = 'none';
        //cargamos maniobras
        maniobras(DepReg,ArrReg,Cli,TrkID);   
        if(Status!="Programmed"){
            document.getElementById('removelots').style.display = 'none'; 
        }
        else{
            document.getElementById('removelots').style.display = 'block';
        }

        if (pdfload == "NO" ){
            document.getElementById('btnupdate').style.display = 'none';
            document.getElementById('btnloadfile').style.display = 'block';
            document.getElementById('documento').style.display = 'none';
        } else {
            document.getElementById('btnupdate').style.display = 'block';
            document.getElementById('btnloadfile').style.display = 'none';
            document.getElementById('documento').style.display = 'block';
            $('#actualfile').val(idglobaltruck + '.pdf');
        }
//----------------------------VALIDAR QUE EL XML SE SUBIO PARA HABILITAR BOTONES------------------------------------
        
        if(xmlload == "NO"){
            document.getElementById('btnupdatexml').style.display = 'none';
            document.getElementById('btnloadxml').style.display = 'block';
            document.getElementById('documentoxml').style.display = 'none';
        } else{
            document.getElementById('btnupdatexml').style.display = 'block';
            document.getElementById('btnloadxml').style.display = 'none';
            document.getElementById('documentoxml').style.display = 'block';
            $("#actualxml").val( TrkID+".xml");
        }

        //----------------------------VALIDAR QUE EL TCIKET DE PESO SE SUBIO PARA HABILITAR BOTONES------------------------------------
        
        if(ticket == "NO"){
            document.getElementById('load_ticket').style.display = 'block';
            document.getElementById('update_ticket').style.display = 'none';
        } else{
            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",
                data:  {opcion:25,TrkID:TrkID},    
                success: function(data){
                    opts = JSON.parse(data);
                    if (opts[0].TicketPeso !=0 && opts[0].TicketPeso !="" ){
                        document.getElementById('ticket').style.display = 'block';
                        $("#nameticket").val("Ticket-"+TrkID+"."+ opts[0].TicketPeso);
                        document.getElementById('load_ticket').style.display = 'none';
                        document.getElementById('update_ticket').style.display = 'block';                    
                    }
                    else{
                        document.getElementById('ticket').style.display = 'none';
                        document.getElementById('load_ticket').style.display = 'block';
                        document.getElementById('update_ticket').style.display = 'none';
                        $("#nameticket").val("");         
                    }

                }
            });
        }

        // boton para subir o actualizar certificado de origen
        if(Qty == 0){
            document.getElementById('load_cert').style.display = 'none';
            document.getElementById('btnupdateCert').style.display = 'none';
        }
        else{
            if(CertOrigen == "NO"){
                document.getElementById('btnupdateCert').style.display = 'none';
                document.getElementById('load_cert').style.display = 'block';
                document.getElementById('documentoCert').style.display = 'none';
            } else{
                document.getElementById('btnupdateCert').style.display = 'block';
                document.getElementById('load_cert').style.display = 'none';
                document.getElementById('documentoCert').style.display = 'block';
            }
        }
        

        $.ajax({
            url: "bd/crudTrk.php",
            type: "POST",
            datatype:"json",
            data:  {opcion:30,TrkID:TrkID},    
            success: function(data){
                opts = JSON.parse(data);
                if (opts[0].FileCertificados === null){
                    $("#load_cert").text("Cargar Certificado");
                    document.getElementById('descarga_cert').style.display = 'none';
                    document.getElementById('actualfile_certicados').style.display = 'none';                   
                }
                else{
                    $("#actualfile_certicados").val("Certificado-"+ TrkID +"."+ opts[0].FileCertificados)
                    document.getElementById('descarga_cert').style.display = 'block';
                    document.getElementById('actualfile_certicados').style.display = 'block';
                    $("#load_certificado").text("Actualizar Certificado");                  
                }
            },
            error: function(data) {
                alert('error');
            }
        });
        
        if (Typ == "CON"){
            $("#IrrDat").prop("disabled", true);
        }else{
            $("#IrrDat").prop("disabled", false);
        }

        if (Status == "Received"){  //&& Typ == "CON"
            $("#Status").prop("disabled", true);
        }else{
            $("#Status").prop("disabled", false);
        }
        //Qty = parseFloat(fila.find('td:eq(3)').text());
        $("#TrkID").val(TrkID);
        $("#TrkLPlt").val(TrkLPlt);
        $("#OutRegDO").val(DepReg);
        $("#InRegDO").val(ArrReg);
        $("#TypDO").val(Typ);
        $("#Client").val(Cli);
        $("#TraLPlt").val(TraLPlt);
        $('#TNam').append('<option class="form-control selected">' + fila.find('td:eq(18)').text() + '</option>'); //$("#TNam").val(TNam);
        $('#RqstID').append('<option class="form-control selected">' + fila.find('td:eq(28)').text() + '</option>'); //$("#TNam").val(TNam);
        $("#WBill").val(WBill);
        $("#DrvLcs").val(DrvLcs);
        $("#DrvNam").val(DrvNam);
        $("#DrvTel").val(DrvTel);
        $("#OutDat").val(OutDat);
        $("#OutTime").val(OutTime);      
        $("#OutWgh").val(OutWgh);
        $("#InDat").val(InDat);    
        $("#InTime").val(InTime);
        $("#RO").val(RO);
        $("#PO").val(PO);
        $("#InWgh").val(InWgh);
        $("#TrkCmt").val(TrkCmt);
        $("#DOrd").val(DOrd);
        //$("#RqstID").val(RqstID);
        $("#Status").val(Status);
        $("#IrrDat").val(IrrDat);
        $("#LotsAssc").empty();
        $('#AsscLots').val(LotsAssc);
        $('#QtyDO').val(Qty);
        $('#PWgh').val(PWgh);
        $('#NumEcon').val(NumEcon);


        //Check de Samples 
        if (samples == 'True'){
            $("#samples").prop("checked", true);            
         }

        //****************calcular peso promedio de pacas************************************* 
        pesosalida = OutWgh.replace(/,/g, ''); 
        pesollegada = InWgh.replace(/,/g, '');        
        pesosalida=parseInt(pesosalida);        
        pesollegada = parseInt(pesollegada);
        cantidad = parseInt(Qty);       

        if (cantidad != '0' && pesosalida != '0'){
            promdiosalida = (pesosalida)/(cantidad);
            promdiosalida = promdiosalida.toFixed(2)
            //promdiosalida = parseFloat(promdiosalida);
            $('#promsalida').val(promdiosalida);   

        }
        else{
            $('#promsalida').val(0);  
        
        }
        if (cantidad != '0' && pesollegada!= '0'){
            promllegada = (pesollegada)/(cantidad)
            promllegada = promllegada.toFixed(2)
            //promdiosalida = parseFloat(promdiosalida);
            $('#promllegada').val(promllegada);   
        }
        else{
            $('#promllegada').val(0);
        }

        //***************** */ AQUI TERMINA PESOS PROMEDIO***************************************************/

        RqstID1 = $.trim($('#RqstID').val());
        RqstID = $.trim($('#RqstID').val());
        $("#RqstID").change( function() {
            RqstID1 = $.trim($('#RqstID').val());
            if (RqstID != RqstID1){
                RqstID1 = RqstID;
            }
        });
        $("#Status").change( function() {
            ban = 1;
        });
        
        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $(".modal-title").text("Edit Truck " + TrkID);		
        $('#modalCRUD').modal('show');		   
    });

    //Borrar
    $(document).on("click", ".btnBorrar", function(){
        fila = $(this);           
        TrkID = parseInt($(this).closest('tr').find('td:eq(0)').text());		
        opcion = 3; //eliminar        
        var respuesta = confirm("¿Está seguro que deseas deshabilitar el registro "+TrkID+"?");                
        if (respuesta) {            
            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",    
                data:  {opcion:opcion, TrkID:TrkID},    
                success: function() {
                    tablaTrk.row(fila.parents('tr')).remove().draw();                  
                }
            });	
        }
    });
    
    //Buscar OE y lotes
    $(document).on("click", ".btnBuscar", function(){
        //$("#Lot").empty();
        //opcion = 5; //
        fila = $(this);
        TrkID = parseInt($(this).closest('tr').find('td:eq(0)').text()); //Obtener el ID del camion a asociar
        $("#formOEL").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Search OE y Lots");
        $('#modalOEL').modal('show');
    });
    
    //Actualizar lot
    /*function update(){
        $("#Lot").empty();
        $("#QtyLot").val("");
        var opts = "";
        opcion = 5;
        DO = $.trim($('#DO').val());
        //id = TrkID;
        $.ajax({
            url: "bd/crudTrk.php",
            type: "POST",
            datatype:"json",
            data:  {DO:DO ,opcion:opcion},    
            success: function(data){
                opts = JSON.parse(data);
                for (var i = 0; i< opts.length; i++){
                    $('#Lot').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '</option>');
                    $('#QtyLot').val(opts[0].Qty);
                }
            },
            error: function(data) {
                alert('error');
            }
        });
    }*/
    
    //Vaciar lotes en select html dependientes de la OE ingresada 
    //$(document).on("click", ".btnBuscarLot", function(){
    //    update();
        /*opcion = 5;
        DO = $.trim($('#DO').val());
        //id = TrkID;
        $.ajax({
              url: "bd/crudTrk.php",
              type: "POST",
              datatype:"json",
              data:  {DO:DO ,opcion:opcion},    
              success: function(data){
                  $("#Lot").empty();
                  //$("#QtyLot").empty();
                  var opts = JSON.parse(data);
                  for (var i = 0; i< opts.length; i++){
                      $('#Lot').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '</option>');
                      $('#QtyLot').val(opts[0].Qty);
                  }
               },
            error: function(data) {
                alert('error');
            }
        });*/
    //});



    $('#btnloadfile').on('click', function(e) {
            e.preventDefault(); 
     
            if (pdfload == "NO" ){
                document.getElementById('update').style.display = 'none';
                document.getElementById('loadfile').style.display = 'block';
       
            //ValidateWgh();  
            } else {
                document.getElementById('update').style.display = 'block';
                document.getElementById('loadfile').style.display = 'none';
       
            }
            var $el = $('#file');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
            $('#loadfile').prop('disabled', true);
            $(".modal-header").css("background-color", "#17562c");
            $(".modal-header").css("color", "white" );
            $(".modal-title").text("Load File");
            $('#filesload').modal('show');
                
            });
    
    //BOTON ACTUALIZAR CARTA PORTE PDF
        $('#btnupdate').on('click', function(e) {
            e.preventDefault(); 
            if (pdfload == "NO" ){
                document.getElementById('update').style.display = 'none';
                document.getElementById('loadfile').style.display = 'block';
       
            //ValidateWgh();  
            } else {
                document.getElementById('update').style.display = 'block';
                document.getElementById('loadfile').style.display = 'none';
       
            }
    
            var $el = $('#file');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
            
            $('#update').prop('disabled', true);
               
                $(".modal-header").css("background-color", "#17562c");
                $(".modal-header").css("color", "white" );
                $(".modal-title").text("Update File");
                $('#filesload').modal('show');
                    
         });
    
    
    
    
        //CARGAR PDF EN AWS S3
        $('#loadfile').on('click', function(e) {
            e.preventDefault(); 
            //let frm = document.getElementById("uploadFile");
            var file_data = $('#file').prop('files')[0];   
           // let form_data = new FormData(frm);    
            var form_data = new FormData();             
            form_data.append('file', file_data);
            form_data.append('idtruck', idglobaltruck);
            $('#loadfile').prop('disabled', true);
            //console.log(idglobaltruck);
           // form_data.append("info", JSON.stringify(idglobaltruck));
           $('#avisofile').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>'); 
           //alert(form_data);                             
            $.ajax({
                url: 'inputfile.php', // <-- point to server-side PHP script 
                dataType: 'text',  // <-- what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(){
                    $('#filesload').modal('hide'); // <-- display response from the PHP script, if any
                    $(".modal-title").text("Edit Truck " + TrkID);
                    $('#avisofile').html("");
                  
                    alert("Archivo SUBIDO con exito");
                    document.getElementById('documento').style.display = 'block';
                    document.getElementById('btnupdate').style.display = 'block';
                    document.getElementById('btnloadfile').style.display = 'none';
                    $('#actualfile').val(idglobaltruck + '.pdf');
                    tablaTrk.ajax.reload(null,false);
    
                }
             });
        });
    
     //ACTUALIZAR PDF EN AWS S3
        $('#update').on('click', function(e) {
            e.preventDefault(); 
            $('#update').prop('disabled', true);
            var file_data = $('#file').prop('files')[0];   
            $('#avisofile').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');  
            var form_data = new FormData();             
            form_data.append('file', file_data);
            form_data.append('idtruck', idglobaltruck);
            $(".modal-title").text("Update PDF")
           // console.log(idglobaltruck);
           // form_data.append("info", JSON.stringify(idglobaltruck));
          // alert(form_data);                             
            $.ajax({
                url: 'inputfile.php', // <-- point to server-side PHP script 
                dataType: 'text',  // <-- what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(){
                
                   
                    $('#filesload').modal('hide');
                    $('#avisofile').html("");
                    alert("Archivo ACTUALIZADO con exito"); // <-- display response from the PHP script, if any
                    $(".modal-title").text("Edit Truck " + TrkID);
                }
             });
        });
    
    
    //VER CARTA PORTE PDF
    $(document).on("click", ".viewfile", function(e){
        e.preventDefault(); 
        window.open("opens3.php?TrkID="+idglobaltruck);
    });
    
    

    //-----------------FUNCION PARA ABRIR MODAL DE  SUBIR XML --------------- 
    $('#btnloadxml').on('click', function(e) {
        e.preventDefault(); 
        $('#loadxml').prop('disabled', true);

        $("#filexml").val("");
 
        if (xmlload == "NO" ){
            document.getElementById('updatexml').style.display = 'none';
            document.getElementById('loadxml').style.display = 'block';
   
        //ValidateWgh();  
        } else {
            document.getElementById('updatexml').style.display = 'block';
            document.getElementById('loadxml').style.display = 'none';
   
        }
        var $el = $('#filexml');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        
        $('#xml_load').prop('disabled', true);
        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $(".modal-title").text("Load XML");
        $('#xml_load').modal('show');
            
    });
    //----------------AQUI TERMINA FUNCION DE ABRIR MODAL PARA SUBIR XML


    //----------CARGAR XML EN AWS S3 ------------------------------------------------
    $('#loadxml').on('click', function(e) {
        e.preventDefault(); 
        //let frm = document.getElementById("uploadFile");
        var file_data = $('#filexml').prop('files')[0];   
       // let form_data = new FormData(frm);    
        var form_data = new FormData();             
        form_data.append('file', file_data);
        form_data.append('idtruck', idglobaltruck);
        $('#loadxml').prop('disabled', true);
        //console.log(idglobaltruck);
       // form_data.append("info", JSON.stringify(idglobaltruck));
       $('#avisoxml').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>'); 
       //alert(form_data);                             
        $.ajax({
            url: 'inputxml.php', // <-- point to server-side PHP script 
            dataType: 'text',  // <-- what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(){
                $('#xml_load').modal('hide'); // <-- display response from the PHP script, if any
                $('#avisoxml').html("");        
                      
                alert("Archivo XML SUBIDO con exito");
                document.getElementById('documentoxml').style.display = 'block';
                $('#actualxml').val(idglobaltruck + '.xml');
                document.getElementById('btnupdatexml').style.display = 'block';
                document.getElementById('btnloadxml').style.display = 'none';
                $(".modal-title").text("Edit Truck " + TrkID);
                tablaTrk.ajax.reload(null,false);

            }
         });
    });



    //*****BOTON PARA DESCARGAR XML************** */

    $(document).on("click", ".btndownloadxml", function(e){      
        e.preventDefault(); 
        window.open("descxml.php?TrkID="+idglobaltruck);
    });

    //***** TERMINA FUNCION DE BOTON PARA DESCARGAR XML************** */

    ///**************BOTON PARA MOSTRAR EL MODAL UPDATE****************** */
    $('#btnupdatexml').on('click', function(e) {
        e.preventDefault(); 
   
        if(xmlload == "NO"){
            document.getElementById('updatexml').style.display = 'none';
            document.getElementById('loadxml').style.display = 'block';

        } else{
            document.getElementById('updatexml').style.display = 'block';
            document.getElementById('loadxml').style.display = 'none';
        }         

        var $el = $('#filexml');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        
        $('#updatexml').prop('disabled', true);
        
        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $(".modal-title").text("Update XML");
        $('#xml_load').modal('show');
    });

///***************AQUI TERMINA FUNCION DE BOTON MOSTRAR MODAL UPDATE */

//-----------------FUNCION PARA ABRIR MODAL DE CERTIFICADO DE ORIGEN --------------- 
//MOSTRAR EL MODAL PARA CARGAR CERTIFICADOS 
$('#load_cert').on('click', function(e) {
    e.preventDefault(); 
    $("#load_certificado").text("Cargar Certificado");
    $('#avisoticket').html("");
    TrkID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID				
    console.log(TrkID)
    $('#load_certificado').prop('disabled', true);
    var $el = $('#filecertificado');
    $el.wrap('<form>').closest('form').get(0).reset(); //Limpiar el apartado de cargar archivo
    $el.unwrap();          
    $(".modal-header").css("background-color", "#17562c");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Cargar Certificado");
    $('#modal_certificados').modal('show');
    //VALIDAR SI YA SE SUBIERON LOS CERTIFICADOS DE ORIGEN 
    $.ajax({
        url: "bd/crudTrk.php",
        type: "POST",
        datatype:"json",
        data:  {opcion:30, TrkID:TrkID},    
        success: function(data){
            console.log(data)
            opts = JSON.parse(data);             
            if (opts[0].FileCertificados === null){
                $("#load_certificado").text("Cargar Certificado");
            }
            else
            {
                $("#load_certificado").text("Actualizar Certificado");
            }
        },
        error: function(data) {
            alert('error');
        }
    });   
});

$('#btnupdateCert').on('click', function(e) {
    e.preventDefault(); 

    if(CertOrigen == "NO"){
        document.getElementById('btnupdateCert').style.display = 'none';
        document.getElementById('load_cert').style.display = 'block';
    } else{
        document.getElementById('btnupdateCert').style.display = 'block';
        document.getElementById('load_cert').style.display = 'none';
    }
    var $el = $('#filecertificado');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap();
    
    $('#updateCert').prop('disabled', true);
    
    $(".modal-header").css("background-color", "#17562c");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Actualizar Certificado de Origen");
    $('#modal_certificados').modal('show');
});

//SUBIR O ACTUALIZAR CERTIFICADOS DE ORIGEN
$('#load_certificado').on('click', function() {
    // e.preventDefault(); 
    TrkID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID		
    $('#load_certificado').prop('disabled', true);
    var file_data = $('#filecertificado').prop('files')[0];   
    $('#avisocertificado').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');  
    var form_data = new FormData();             
    form_data.append('file', file_data);
    form_data.append('idtruck', TrkID);
    
    $.ajax({
        url: 'LoadCertificados.php', // <-- point to server-side PHP script 
        dataType: 'text',  // <-- what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        success: function(data){              
            opts = JSON.parse(data);
            $('#modal_certificados').modal('hide');
            $('#avisocertificado').html("");
            alert("Certificado subido con exito"); // <-- display response from the PHP script, if any
            console.log(opts)
            $("#actualfile_certicados").val(opts);
            document.getElementById('descarga_cert').style.display = 'block';
            document.getElementById('actualfile_certicados').style.display = 'block';
            document.getElementById('documentoCert').style.display = 'block';
            document.getElementById('btnupdateCert').style.display = 'block';
            document.getElementById('load_cert').style.display = 'none';
        }
    });
});
// aqui termina la funcion de subir certificados de origen

//*************FUNCION DE BOTON REMOVE ALL LOTS*********************************
           $("#removelots").on('click', function (e) {
            e.preventDefault();
            if (confirm("you want to remove all Lots from trukid " + TrkID + "?") == true) {
            liberatrk(TrkID);
            //LoadReq_buton(0);
            }
            else{
                return false;
            }

        });




//**********************FUNCION PARA ACTUALIZAR XML **************************

     //ACTUALIZAR PDF EN AWS S3
     $('#updatexml').on('click', function(e) {
        e.preventDefault(); 
        $('#updatexml').prop('disabled', true);
        var file_data = $('#filexml').prop('files')[0];   
        $('#avisoxml').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');  
        var form_data = new FormData();             
        form_data.append('file', file_data);
        form_data.append('idtruck', idglobaltruck);
       // console.log(idglobaltruck);
       // form_data.append("info", JSON.stringify(idglobaltruck));
      // alert(form_data);                             
        $.ajax({
            url: 'inputxml.php', // <-- point to server-side PHP script 
            dataType: 'text',  // <-- what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(){
            
               
                $('#xml_load').modal('hide');
                $('#avisoxml').html("");
                alert("Archivo ACTUALIZADO con exito"); // <-- display response from the PHP script, if any
                document.getElementById('btnupdatexml').style.display = 'block';
                document.getElementById('btnloadxml').style.display = 'none';
                $(".modal-title").text("Edit Truck " + TrkID);
                tablaTrk.ajax.reload(null,false);
             
            }
         });
    });

//************************TERMINA FUNCION PARA ACTUALIZAR  *******************/

//************************FUNCIONES PARA CARGAR;DESCARGAR Y ACTUALIZAR EL TICKET DE PERSO *******************/

$(document).on("click", ".btndownticket", function(e){
    e.preventDefault();     
    window.open("descarga_ticket.php?TrkID="+TrkID);

});


//MOSTRAR EL MODAL PARA CARGAR EL TICKET DE PESO 
$('#load_ticket').on('click', function(e) {
    e.preventDefault(); 
   // $("#load").text("Subir Ticket");
    $('#avisoticket').html("");
    TrkID=$("#TrkID").val();
    $('#load').prop('disabled', true);
    var $el = $('#fileticket');
    $el.wrap('<form>').closest('form').get(0).reset(); //Limpiar el apartado de cargar archivo
    $el.unwrap();          
    $(".modal-header").css("background-color", "#17562c");
    $(".modal-header").css("color", "white" );
    $('#modal_ticket').modal('show');
    $(".modal-title").text("Load File");
    //opcion = 4;	
    //VALIDAR SI YA SE SUBIO EL TICKET
    $.ajax({
        url: "bd/crudTrk.php",
        type: "POST",
        datatype:"json",
        data:  {opcion:25,TrkID:TrkID},       
        success: function(data){
            opts = JSON.parse(data);             
            if (opts[0].TicketPeso != "" && opts[0].TicketPeso != null){
                $("#Ticketload").text("Actualizar Ticket");
            }
            else
            {
                $("#Ticketload").text("Subir Ticket");
            }
        },
        error: function(data) {
            alert('error');
        }
    });
    


});


//MOSTRAR EL MODAL PARA ACTUALIZAR EL TICKET DE PESO 
$('#update_ticket').on('click', function(e) {
    e.preventDefault(); 
   // $("#load").text("Subir Ticket");
    $('#avisoticket').html("");
    TrkID=$("#TrkID").val();
    $('#load').prop('disabled', true);
    var $el = $('#fileticket');
    $el.wrap('<form>').closest('form').get(0).reset(); //Limpiar el apartado de cargar archivo
    $el.unwrap();          
    $(".modal-header").css("background-color", "#17562c");
    $(".modal-header").css("color", "white" );
    $('#modal_ticket').modal('show');
    //opcion = 4;	
    //VALIDAR SI YA SE SUBIO EL TICKET
    $.ajax({
        url: "bd/crudTrk.php",
        type: "POST",
        datatype:"json",
        data:  {opcion:25,TrkID:TrkID},       
        success: function(data){
            opts = JSON.parse(data);             
            if (opts[0].TicketPeso != "" && opts[0].TicketPeso != null){
                $("#Ticketload").text("Actualizar Ticket");
            }
            else
            {
                $("#Ticketload").text("Subir Ticket");
            }
        },
        error: function(data) {
            alert('error');
        }
    });
    
});





//SUBIR O ACTUALIZAR EL TICKET DE PESO 
$('#Ticketload').on('click', function() {


   // e.preventDefault(); 
    //TrkID=$("#TrkID").val();
    $('#Ticketload').prop('disabled', true);
    var file_data = $('#fileticket').prop('files')[0];   
    $('#avisoticket').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');  
    var form_data = new FormData();             
    form_data.append('file', file_data);
    form_data.append('idtruck', TrkID);
    
    
    $.ajax({
        url: 'LoadTicket.php', // <-- point to server-side PHP script 
        dataType: 'text',  // <-- what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        success: function(data){              
            opts = JSON.parse(data);
            $('#modal_ticket').modal('hide');
            $('#avisoticket').html("");
            alert("Ticket subido con exito"); // <-- display response from the PHP script, if any
            document.getElementById('ticket').style.display = 'block';
            $("#nameticket").val(opts);
            $("#load_ticket").text("Update Ticket");
            $("#load_ticket").removeClass('btn-primary ').addClass('btn-success');     
            $(".modal-title").text("Edit Truck " + TrkID);   
            tablaTrk.ajax.reload(null,false);        
          
        }
     });


});


//BOTONES PARA CERRAR MODAL DE LOAD FILE SE BORRA EL ARCHIVO QUE CARGARON
$('#close').on('click', function(e) {
    e.preventDefault();
    var $el = $('#file');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap(); 
    $('#modal_ticket').modal('hide');	
    $(".modal-title").text("Edit Truck " + TrkID);
});

$('#closeX').on('click', function(e) {
    e.preventDefault();
    var $el = $('#file');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap(); 
    $('#modal_ticket').modal('hide');
    $(".modal-title").text("Edit Truck " + TrkID);
});


$('#closeX1').on('click', function(e) {
    e.preventDefault();
    var $el = $('#file');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap(); 
    $('#filesload').modal('hide');	
    $(".modal-title").text("Edit Truck " + TrkID);
});


$('#close1').on('click', function(e) {
    e.preventDefault();
    var $el = $('#file');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap(); 
    $('#filesload').modal('hide');	
    $(".modal-title").text("Edit Truck " + TrkID);
});


$('#closeX2').on('click', function(e) {
    var $el = $('#filexml');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap(); 
    
    $('#xml_load').modal('hide');	
    
    $(".modal-title").text("Edit Truck " + TrkID);
});


$('#close2').on('click', function(e) {
    var $el = $('#filexml');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap(); 
    
    $('#xml_load').modal('hide');	
    
    $(".modal-title").text("Edit Truck " + TrkID);
});
// cerrar certificado de origen
$('#close_cert').on('click', function(e) {
    e.preventDefault();
    var $el = $('#filecertificado');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap(); 
    $('#modal_certificados').modal('hide');	
});

$('#closeX_cert').on('click', function(e) {
    e.preventDefault();
    var $el = $('#filecertificado');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap(); 
    $('#modal_certificados').modal('hide');
});


//GUARDAR PRESIONAR BOTÓN CERRAR/CANCEL
$('#btnCancel,#cerrarmodal').on('click', function(e) {
    guarda(activado);
});


    
    
 //************************TERMINA FUNCIONES PARA CARGAR; DESCARGAR Y ACTUALIZAR EL TICKET DE PESO  *******************/   
    

 //********************FUNCION PARA VER LOTES Y CANTIDADES ASOCIADOS A UN TRUCK ************************************/
$('#InfLot').on('click', function(e) {   
    e.preventDefault();  
    infolote(TrkID);

});

$('#closeInfoLots').on('click', function(e) {
    $('#modalInfLots').modal('hide');	
    $(".modal-title").text("Edit Truck " + TrkID);
    //$('#Inf').html("");
    $('#Inf').html(' ')
    
});   


//********************FUNCION PARA MODAL ADD CONTAINER ************************************/

$('#container').on('click', function(e) {   
    e.preventDefault();  
    showcontainer();

});




$("#container_id").on("change", function(){
    if($(this).val() != 0  ){
        $('#addcont').prop('disabled', false);
    }
    else{
        $('#addcont').prop('disabled', true);
    }
});


$('#addcont').on('click', function(e) {  

    ExpID=$('#container_id').val();
    Lots=$('#AsscLots').val();
    DO=$('#DOrd').val();
    ligarcontainer(Lots,DO,TrkID,ExpID);
});


$('#btnClosecont , #closecont').on('click', function(e) {
    $('#containers').modal('hide');	
    $(".modal-title").text("Edit Truck " + TrkID);
    //$('#Inf').html("");
    
});




    //Qty dependiente de select de Lotes
    $("#Lot").on("change", function(){
        opcion = 6;
        id = $("#Lot").val();
        $.ajax({
            url: "bd/crudTrk.php",
            type: "POST",
            datatype:"json",
            data:  {id:id ,opcion:opcion},    
            success: function(data){
                var opts = JSON.parse(data);
                $('#QtyLot').val(opts[0].Qty);
            },
            error: function(data) {
                alert('error');
            }
        });
    });
    
    //Cargar lotes comentado 27 jul
    function LoadLots(){
            $("#LotsAssc").empty();
            $('#AsscLots').val("");
            $('#QtyDO').val("");
            $('#PWgh').val("");
            var opts = "";
            opcion = 6;
            DO = $.trim($('#DOrd').val());
            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",
                data:  {DO:DO ,opcion:opcion},    
                success: function(data){
                    opts = JSON.parse(data);
                    for (var i = 0; i< opts.length; i++){
                        $('#LotsAssc').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '-' + opts[i].Qty + ' bc </option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });
    }
    
    //$("#DOrd").on("change", function(){
    //        LoadLots();
            //val = document.getElementsByClassName("modal-title"); //.value; //$.trim($(".modal-title"));
            /*$("#LotsAssc").empty();
            $('#AsscLots').val("");
            $('#QtyDO').val("");
            $('#PWgh').val("");
            var opts = "";
            opcion = 6;
            DO = $.trim($('#DOrd').val());
            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",
                data:  {DO:DO ,opcion:opcion},    
                success: function(data){
                    opts = JSON.parse(data);
                    for (var i = 0; i< opts.length; i++){
                        $('#LotsAssc').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '-' + opts[i].Qty + '</option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });*/
    //});

    /*function addLot(){
        opcion = 7;
        id = parseInt($.trim($('#Lot').val()));
        CrgQty = parseInt($.trim($('#QtyLot').val()));
        TrkID = parseInt(TrkID);
        $.ajax({
            url: "bd/crudTrk.php",
            type: "POST",
            datatype:"json",
            data:  {id:id, TrkID:TrkID , CrgQty:CrgQty, opcion:opcion},    
            success: function(data){
                tablaTrk.ajax.reload(null, false);
                alert ("Actualizado con exito.");
                //tablaTrk.ajax.reload(null, false);
            },
            error: function(data) {
                alert('error');
            }
        });
    }*/

    //Agregar IDTruck a tabla de Lotes
    /*$(document).on("click", ".btnAdd", function(){
        addLot();
        update();
        /*opcion = 7;
        id = parseInt($.trim($('#Lot').val()));
        TrkID = parseInt(TrkID);
        //alert (id);
        //alert (TrkID);
        //DO = $.trim($('#DO').val());
        $.ajax({
              url: "bd/crudTrk.php",
              type: "POST",
              datatype:"json",
              data:  {id:id, TrkID:TrkID , opcion:opcion},    
              success: function(data){
                tablaTrk.ajax.reload(null, false);
                alert ("Actualizado con exito.");
                //tablaUsuarios.ajax.reload(null, false);
              },
            error: function(data) {
                alert('error');
            }
        });
    });*/
    
    $('#btnLimpiar').on('click', function(){
        tablaTrk
        .search( '' )
        .columns().search( '' )
        .draw();
        //tablaTrk
        //    .search("").draw();
        //$("#tablaTrk").DataTable().search('').draw();
        //$('#tablaTrk tfoot th').find('input[type=search]').val('');
        //$("#tablaTrk").DataTable().search('').draw();
    }); 
   
});

//Buscar lotes asociados a la DO
$(document).ready(function(){
    
    function LoadLots(){
            $("#LotsAssc").empty();
            $('#AsscLots').val("");
            $('#QtyDO').val("");
            $('#PWgh').val("");
            var opts = "";
            opcion = 6;
            DO = $.trim($('#DOrd').val());
            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",
                data:  {DO:DO ,opcion:opcion},    
                success: function(data){
                    opts = JSON.parse(data);
                    $('#LotsAssc').append('<option value="">-Select Lot-</option disabled>');
                    for (var i = 0; i< opts.length; i++){
                        $('#LotsAssc').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '-' + opts[i].Qty + 'bc </option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });
    }
    /*
    function LoadReq(){
            $("#RqstID").empty();
            var opts = "";
            opcion = 15;
            //RqstID = $.trim($('#RqstID').val());
            DO = $.trim($('#DOrd').val());
            //alert (DO);
            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",
                data:  { DO:DO, opcion:opcion },    
                success: function(data){
                    opts = JSON.parse(data);
                    $('#RqstID').append('<option value=0>- Select -</option disabled>');
                    for (var i = 0; i< opts.length; i++){
                        $('#RqstID').append('<option value="' + opts[i].ReqID + '">' + opts[i].ReqID + '</option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });
    }
    */
    
    function update() {
        $("#TNam").empty();
        $('#OutRegDO').val("");
        $('#InRegDO').val("");
        $('#TypDO').val("");
        $('#Client').val("");
        //clear_maniobras();
        var opts = "";
        opcion = 5;
        DO = $.trim($('#DOrd').val());
        $.ajax({
            url: "bd/crudTrk.php",
            type: "POST",
            datatype: "json",
            data: {
                DO: DO,
                opcion: opcion
            },
            success: function(data) {
               
                opts = JSON.parse(data);
                if(opts.length > 0){
                
                $('#btnGuardar').attr('disabled',false);

                $('#TNam').append('<option class="form-control" value="">- Select -</option>');
                for (var i = 0; i < opts.length; i++) {
                    $('#TNam').append('<option value="' + opts[i].TptID + '">' + opts[i].BnName + '</option>');
                }
                $('#OutRegDO').val(opts[0].PlcOut);
                $('#InRegDO').val(opts[0].RegIn);
                $('#TypDO').val(opts[0].Typ);
                $('#Client').val(opts[0].PlcIn);
                
                 if (opts[0].Samp === 1){
                    $("#samples").prop("checked", true);
                }
                else {
                    $("#samples").prop("checked", false);
                }
                
                // ejecutamos la funcion de maniobras con el botón nuevo 
                
                maniobras(opts[0].PlcOut, opts[0].RegIn, opts[0].PlcIn,"");

          

                LoadLots();
                LoadReq();
                if ($('#TypDO').val()=="CON"){
                    $("#IrrDat").prop("disabled", true);
                }else{
                    $("#IrrDat").prop("disabled", false);
                }
            }else{
                $('#btnGuardar').attr('disabled',true);
                alert('non-existent Delivery Order, please verify your data.');
            }
            },
            error: function(data) {
                alert('error');
            }
        });
    }
    
    $("#DOrd").on("change", function(){
            update();
            //Mostrar maniobras de la  DO
        
            
           
           
           /* LoadLots();
            LoadReq(); */
    }); 
    
    $('#btnCancel').on('click', function () {
        //$('#btnGuardar').attr('disabled',false);
    });
    
    $(document).on("click", ".btnEditar", function(){
            fila = $(this).closest("tr");	        
            TrkID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID	
            $("#LotsAssc").empty();
            var opts = "";
            opcion = 12;
            DO = $.trim($('#DOrd').val());
            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",
                data:  {TrkID:TrkID, DO:DO ,opcion:opcion},    
                success: function(data){
                    opts = JSON.parse(data);
                    $('#LotsAssc').append('<option value="">-Select Lot-</option>');
                    for (var i = 0; i< opts.length; i++){
                        $('#LotsAssc').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '-' + opts[i].Qty + 'bc </option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });
        
            //area para requisicion

            //CARGAR PR DISPONIBLES Y PR ACTUAL
            LoadReq();
            PRD=fila.find('td:eq(28)').text();
           
           $('#RqstID').append('<option class="form-control selected">' + fila.find('td:eq(28)').text() + '</option>');
            
            /*
            opcion = 15;
            //RqstID = $.trim($('#RqstID').val());
            DO = $.trim($('#DOrd').val());
            //alert (DO);
            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",
                data:  { DO:DO, opcion:opcion },    
                success: function(data){
                    opts = JSON.parse(data);
                    $('#RqstID').append('<option value=0>- Select -</option disabled>');
                    for (var i = 0; i< opts.length; i++){
                        $('#RqstID').append('<option value="' + opts[i].ReqID + '">' + opts[i].ReqID + '</option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });

            */
            //RqstID = fila.find('td:eq(24)').text();
            //alert(RqstID);
    });
        
});

//Funcion para asociar lotes a truck desde el formulario -- quitar se acciona el boton dentro del modal
$(document).ready(function(){
});

//Lista de camiones a selecinar cuando se requiere editar un camion
$(document).ready(function(){
    $(document).on("click", ".btnEditar", function(){
            var opts = "";
            opcion = 5;
            DO = $.trim($('#DOrd').val());
            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",
                data:  {DO:DO ,opcion:opcion},    
                success: function(data){
                    opts = JSON.parse(data);
                    $('#TNam').append('<option class="form-control" value="">- Select -</option>');
                    for (var i = 0; i< opts.length; i++){
                        $('#TNam').append('<option value="' + opts[i].TptID + '">' + opts[i].BnName + '</option>');
                    }
                },
                error: function(data) {
                    alert('error');
                }
            });

        });
   


});


//Funcion para agregar lotes con boton dentro del modal
$(document).ready(function(){
    
  
        $("#btnAddLot").click(function(){
         opcion = 8;
            
            bandera =  $.trim($('#exampleModalLabel').val());
            if(bandera != ""){
                activado=1;
                TrkID = $.trim($('#exampleModalLabel').val());
                LotID = $.trim($('#LotsAssc').val());
                //  alert("EL TRKID ES " + TrkID);
                $.ajax({
                        url: "bd/crudTrk.php",
                        type: "POST",
                        datatype:"json",
                        data:  {TrkID:TrkID, LotID:LotID , opcion:opcion},    
                        success: function(data){
                            opts = JSON.parse(data);
                            $('#QtyDO').val(opts[0].CrgQty);
                            $('#PWgh').val(opts[0].PWgh);
                            tam = opts.length;
                            Lotes = ""; 
                            for (var i=0; i<tam; i++){
                                Lotes = Lotes+""+opts[i].Lot+"|";//$('#AsscLots').val(opts[i].Lot+"|");
                            }
                            Lotes2 = Lotes.substring(0, Lotes.length -1);
                            $('#AsscLots').val(Lotes2);
                            $('#RqstID').empty();
                            LoadReq_buton(tam);            
                            //$('#RqstID').append('<option class="form-control selected">' + PRD + '</option>');
                        },
                        error: function(data) {
                            alert('error');
                        }
                    });
                
            }
        });

   
}); //funciones separadas edit y new "add lot"

$(document).ready(function(){           
        
    // TrkID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID	
     $("#btnAddLot").click(function(){
         bandera =  $.trim($('#exampleModalLabel').val());
     if (bandera ==""){
         activado=1;
         opcion = 9;
         LotID = $.trim($('#LotsAssc').val());
         $.ajax({
                 url: "bd/crudTrk.php",
                 type: "POST",
                 datatype:"json",
                 data:  {TrkID:TrkID, LotID:LotID , opcion:opcion},    
                 success: function(data){
                    // alert("EL TRKID ES " + TrkID);
                     opts = JSON.parse(data);
                     $('#QtyDO').val(opts[0].CrgQty);
                     $('#PWgh').val(opts[0].PWgh);
                     tam = opts.length;
                     Lotes = ""; 
                     for (var i=0; i<tam; i++){
                         Lotes = Lotes+""+opts[i].Lot+"|";//$('#AsscLots').val(opts[i].Lot+"|");
                     }
                     Lotes2 = Lotes.substring(0, Lotes.length -1);
                     $('#AsscLots').val(Lotes2);
                     $('#RqstID').empty();
                     LoadReq_buton(tam);            
                     $('#RqstID').append('<option class="form-control selected">' + PRD + '</option>');
                 },
                 error: function(data) {
                     alert('error');
                 }
             });
     }
     });
 });


//Funcion eliminar lotes con boton dentro del modal
$(document).ready(function(){
    
   
        $("#btnDelLot").click(function(){
            LotID = $.trim($('#LotsAssc').val());
            activado=1;
            opcion = 11;
            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",    
                data:  {TrkID:TrkID, LotID:LotID, opcion:opcion},    
                success: function(data) {
                    opts = JSON.parse(data);
                        //$('#QtyDO').val(opts[0].CrgQty);
                        //$('#PWgh').val(opts[0].PWgh);
                        tam = opts.length;
                        Lotes = ""; 
                        for (var i=0; i<tam; i++){
                            Lotes = Lotes+""+opts[i].Lot+"|";//$('#AsscLots').val(opts[i].Lot+"|");
                        }
                        Lotes2 = Lotes.substring(0, Lotes.length -1);
                        $('#AsscLots').val(Lotes2);
                        $('#RqstID').empty();
                        LoadReq_buton(tam);            
                        //$('#RqstID').append('<option class="form-control selected">' + PRD + '</option>');
                        if (!opts.length){
                            $('#QtyDO').val("0");
                            $('#PWgh').val("0");
                        }else{
                        $('#QtyDO').val(opts[0].CrgQty);
                        $('#PWgh').val(opts[0].PWgh);
                        }
                        /*if (tam < 1){
                            $('#AsscLots').val("");
                            $('#QtyDO').val("0");
                            $('#PWgh').val("0");
                        }else if (tam > 1){
                            $('#AsscLots').val(opts[0].Lot+"|"+opts[1].Lot);
                            $('#QtyDO').val(opts[0].CrgQty);
                            $('#PWgh').val(opts[0].PWgh);
                        }else{
                            $('#AsscLots').val(opts[0].Lot);
                            $('#QtyDO').val(opts[0].CrgQty);
                            $('#PWgh').val(opts[0].PWgh);
                        }*/
                    },
                    error: function(data) {
                        alert('error');
                    }                 
            });
       
    });
    
    
    $(document).on("click", ".btnEditar", function(){
        fila = $(this).closest("tr");	        
        TrkID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID	
        
        $("#btnDelLot").click(function(){
            opcion = 10;
            activado=1;
            LotID = $.trim($('#LotsAssc').val());
            $.ajax({
                    url: "bd/crudTrk.php",
                    type: "POST",
                    datatype:"json",
                    data:  {TrkID:TrkID, LotID:LotID , opcion:opcion},    
                    success: function(data){
                        opts = JSON.parse(data);
                        //$('#QtyDO').val(opts[0].CrgQty);
                        //$('#PWgh').val(opts[0].PWgh);
                        tam = opts.length;
                        Lotes = ""; 
                        for (var i=0; i<tam; i++){
                            Lotes = Lotes+""+opts[i].Lot+"|";//$('#AsscLots').val(opts[i].Lot+"|");
                        }
                        Lotes2 = Lotes.substring(0, Lotes.length -1);
                        $('#AsscLots').val(Lotes2);
                        $('#RqstID').empty();
                        LoadReq_buton(tam);            
                        $('#RqstID').append('<option class="form-control selected">' + PRD + '</option>');
                        if (!opts.length){
                            $('#QtyDO').val("0");
                            $('#PWgh').val("0");
                        }else{
                        $('#QtyDO').val(opts[0].CrgQty);
                        $('#PWgh').val(opts[0].PWgh);
                        }
                        /*if (tam < 1){
                            $('#AsscLots').val("");
                            $('#QtyDO').val("0");
                            $('#PWgh').val("0");
                        }else if (tam > 1){
                            $('#AsscLots').val(opts[0].Lot+"|"+opts[1].Lot);
                            $('#QtyDO').val(opts[0].CrgQty);
                            $('#PWgh').val(opts[0].PWgh);
                        }else{
                            $('#AsscLots').val(opts[0].Lot);
                            $('#QtyDO').val(opts[0].CrgQty);
                            $('#PWgh').val(opts[0].PWgh);
                        }*/
                    },
                    error: function(data) {
                        alert('error');
                    }
                });
        });
    });
    
});


//Cargar Region
function LoadRegion(){
    $("#LocSplit").empty();   
    var opts = "";
    opcion = 5;
    $.ajax({
          url: "bd/crudDO.php",
          type: "POST",
          datatype:"json",
          data:  {opcion:opcion},    
          success: function(data){
              opts = JSON.parse(data);
              //$('#OutPlc').append('<option class="form-control" >- Select -</option>');
              //$('#InReg').append('<option class="form-control" >- Select -</option>');
              for (var i = 0; i< opts.length; i++){
                  $('#LocSplit').append('<option value="' + opts[i].RegNam + '">' + opts[i].RegNam + '</option>');
                  //$('#InReg').append('<option value="' + opts[i].RegNam + '">' + opts[i].RegNam + '</option>');
              }
           },
        error: function(data) {
            alert('error');
        }
    });
}


//Funcion para buscar lotes para realizar el split  
$(document).ready(function(){
    
    //$("#btnSplit").click(function(){
        $("#btnFndLot").click(function(){
            $("#LotsSplit").empty();
            $("#Split").val("");
            opcion = 13;
            LocSplit = $.trim($('#LocSplit').val());
            DOSplit = $.trim($('#DOSplit').val());
            $.ajax({
                    url: "bd/crudTrk.php",
                    type: "POST",
                    datatype:"json",
                    data:  {LocSplit:LocSplit, DOSplit:DOSplit, opcion:opcion},    
                    success: function(data){
                        opts = JSON.parse(data);
                        $('#LotsSplit').append('<option class="form-control" value="" disabled selected>- Select -</option>');
                        for (var i = 0; i< opts.length; i++){
                            $('#LotsSplit').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + ' - ' + opts[i].Qty + '</option>');
                        }
                    },
                    error: function(data) {
                        alert('error');
                    }
                });
        });
    //});
});



//Validar Pesos
$(document).ready(function(){
    //Validar peso salida
    function ValidateOutWgh(){
        PWgh = document.getElementById("PWgh").value; //parseInt($.trim($('#PWgh').val()));
        OutWgh = document.getElementById("OutWgh").value;
        if (PWgh.includes(",")) {
          PWgh = PWgh.replace(/,/g, ''); 
         }else{ PWgh = PWgh; }
        if (OutWgh.includes(",")) {
          OutWgh = OutWgh.replace(/,/g, ''); 
         }else{ OutWgh = OutWgh; }

        Qty = parseInt($.trim($('#QtyDO').val()));
        Avg = parseFloat($.trim($('#promsalida').val()));
        min = PWgh - Qty;
        max = parseFloat(PWgh) + parseFloat(Qty);
        
        if (Qty == 0){
            $("#NoPWgh").show();
            $("#Invalido").hide();
        }else{
            if (Avg<200 || Avg>240){ 
                $('#AvisoWgh').html('<small id="emailHelp" class="badge rounded-pill bg-danger">Weight should be between '+Qty*200+' and '+Qty*240+'</small>');
            }else{ 
              if (OutWgh < min){
                  $('#AvisoWgh').html('<small id="emailHelp" class="form-text text-muted">Departure Weight less than Purchase Weight.</small>');
              }else if (OutWgh > max){
                  $('#AvisoWgh').html('<small id="emailHelp" class="form-text text-muted">Departure Weight too high compared to Purchase Weight</small>');
              }else{
                  $('#AvisoWgh').html('');
              }
            }
        }
    }
    //Validar Peso entrada
    function ValidateInWgh(){
        PWghIn = document.getElementById("PWgh").value; //parseInt($.trim($('#PWgh').val()));
        OutWghIn = document.getElementById("OutWgh").value;
        InWgh = document.getElementById("InWgh").value;
        
        if (PWghIn.includes(",")) {
          PWghIn = PWghIn.replace(/,/g, ''); 
         }else{ PWghIn = PWghIn; }
         
        if (OutWghIn.includes(",")) {
          OutWghIn = OutWghIn.replace(/,/g, ''); 
         }else{ OutWghIn = OutWghIn; }
        
        if (InWgh.includes(",")) {
          InWgh = InWgh.replace(/,/g, ''); 
         }else{ InWgh = InWgh; }

        Qty = parseInt($.trim($('#QtyDO').val()));
        AvgLl = parseFloat($.trim($('#promllegada').val()));
        minIn = PWghIn - Qty;
        minSal = parseFloat(OutWghIn) - parseFloat(Qty*0.5);
        maxIn = parseFloat(PWghIn) + parseFloat(Qty);
        maxSal = parseFloat(OutWghIn) + parseFloat(Qty*0.5);
        
        
        if (PWghIn == 0){
            $("#NoPWgh").show();
            $("#Invalido").hide();
        }else{
            if (AvgLl<200 || AvgLl>240){ 
                $('#AvisoWghIn').html('<small id="emailHelp" class="badge rounded-pill bg-danger">Weight should be between '+Qty*200+' and '+Qty*240+'</small>');
            }else{ 
              if (InWgh < minIn){
                  $('#AvisoWghIn').html('<small id="emailHelp" class="form-text text-muted">Arrival Weight less than Purchase Weight.</small>');
              }else if (InWgh < minSal){
                  $('#AvisoWghIn').html('<small id="emailHelp" class="form-text text-muted">Arrival Weight less than Departure Weight.</small>');
              }else if (InWgh > maxIn){
                  $('#AvisoWghIn').html('<small id="emailHelp" class="form-text text-muted">Arrival Weight too high compared to Purchase Weight.</small>');
              }else if (InWgh > maxSal){
                  $('#AvisoWghIn').html('<small id="emailHelp" class="form-text text-muted">Arrival Weight too high compared to Departure Weight.</small>');
              }else{
                  $('#AvisoWghIn').html('');
              }
            }
        }
    }
    
    $("#OutWgh").keyup(function(){
        ValidateOutWgh();
    });
    
    $("#InWgh").keyup(function(){
        ValidateInWgh();
    });
    
    //para cuando copian y pegan un valor
    
    $("#OutWgh").change(function(){
        ValidateOutWgh();
    });
    
    $("#InWgh").change(function(){
        ValidateInWgh();
    });
    
});

$(document).ready(function(){
$(document).on("click", ".btnPDF-REM", function(){
    fila = $(this);           
    TrkID = $(this).closest('tr').find('td:eq(0)').text();
    TrkDO = $(this).closest('tr').find('td:eq(1)').text();
    TrkTyp = $(this).closest('tr').find('td:eq(2)').text();
    Reg = $(this).closest('tr').find('td:eq(15)').text();
    Cli = $(this).closest('tr').find('td:eq(17)').text();
    Cert = $(this).closest('tr').find('td:eq(43)').text(); 
    window.open("./bd/PDFREM.php?TrkID="+TrkID+"&TrkDO="+TrkDO+"&TrkTyp="+TrkTyp+"&Reg="+Reg+"&Cli="+Cli+"&Cert="+Cert);
    tablaTrk.ajax.reload(null, false);
});
});

$(document).ready(function(){
$(document).on("click", ".btnTag", function(){
    fila = $(this);
    TrkID = $(this).closest('tr').find('td:eq(0)').text();
    TrkDO = $(this).closest('tr').find('td:eq(1)').text();
    TrkTyp = $(this).closest('tr').find('td:eq(2)').text();
    TrkQty = $(this).closest('tr').find('td:eq(4)').text();
    Gin = $(this).closest('tr').find('td:eq(13)').text();
    DepReg = $(this).closest('tr').find('td:eq(14)').text();
    Reg = $(this).closest('tr').find('td:eq(15)').text();
    Cli = $(this).closest('tr').find('td:eq(17)').text();
    window.open("./bd/CartaPorte.php?TrkID="+TrkID+"&TrkDO="+TrkDO+"&TrkTyp="+TrkTyp+"&TrkQty="+TrkQty+"&Gin="+Gin+"&Reg="+Reg+"&DepReg="+DepReg+"&Cli="+Cli+"");
    tablaTrk.ajax.reload(null, false);
});
});

$(document).ready(function(){
$(document).on("click", ".export", function(){
    window.open("./bd/export.php");
    tablaTrk.ajax.reload(null, false);
});
});

$(document).ready(function(){
    $(document).on("click", ".btnEditar", function(){
            CrgQty = $.trim($('#QtyDO').val());
            RqstID = $.trim($('#RqstID').val());
            if (CrgQty != 0 || RqstID !=0) {
                $("#DOrd").prop("disabled", true);
            } else {
                $("#DOrd").prop("disabled", false);
            }
    })
});

//Validar estatus del camion sin lotes asociados
$( function() {
    $("#Status").change( function() {
        CrgQty = $.trim($('#QtyDO').val());
        if ($(this).val() === "Transit" && CrgQty === "") {
            alert ("You cannot change the status without associated Lots");
            $("#Status").val("Programmed");
        } else if ($(this).val() === "Received" && CrgQty === "") {
            alert ("You cannot change the status without associated Lots");
            $("#Status").val("Programmed");
        }
    });

    // Boton eliminar PO's and PR's



    $("#btnPOPR").click(function(){
        $("#POPR").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $('#modalPOPR').modal('show');
        //$('#RemPO').prop('disabled', true);
        //  $('#RemPR').prop('disabled', true);
        document.getElementById('alertaremove').style.display = 'none';
        $("#checkPO").prop("checked", false);
        $("#checkPR").prop("checked", false);
        $('#RemPOPR').prop('disabled', true);
        $('#checkPO').prop('disabled', true);
        $('#checkPR').prop('disabled', true);
    });


    // botón filtros

$("#filtraboton").click(function(){
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    DepArrDate = $('#DepArrDate').val();
    if(fromdate || todate || DepArrDate){
        $('#fromdate').attr('disabled', false);
        $('#todate').attr('disabled', false);
        $('#timeSelect').attr('disabled', false);
    }else{
        $('#fromdate').attr('disabled', true);
        $('#todate').attr('disabled', true);
        $('#timeSelect').attr('disabled', true);
    }

    $('#filtrarmodal').modal('show');
});

$('#DepArrDate').on('change keyup',function(){
    DepArrDate = $('#DepArrDate').val();


    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    muestras = $('select[name=muestras] option').filter(':selected').val();
    muestratext = $( "#muestras option:selected" ).text();
    DepRegFil = $('select[name=DepRegFil] option').filter(':selected').val();
    ArrRegFil = $('select[name=ArrRegFil] option').filter(':selected').val();
    timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
    idGlobal = $('#idDO').val();
    asLots = $('#asLots').val();
    if(DepArrDate){
        $('#fromdate').attr('disabled', false);
        $('#todate').attr('disabled', false);
        $('#timeSelect').attr('disabled', false);
        $('#muestras').attr('disabled',true);
        if(fromdate || todate)
            $('#buscafiltro').attr('disabled',false);
        else
            $('#buscafiltro').attr('disabled',true);
    }else{
        $('#fromdate').attr('disabled', true);
        $('#todate').attr('disabled', true);
        $('#timeSelect').attr('disabled', true);
        if(idGlobal || asLots || DepRegFil || ArrRegFil)
            $('#muestras').attr('disabled',true);
        else    
            $('#muestras').attr('disabled',false);
        $('#fromdate').val("");
        $('#todate').val("");
        if(muestratext != "200 (default)" || DepRegFil || ArrRegFil ||idGlobal || asLots ){
            $('#buscafiltro').attr('disabled',false);
        }else
            $('#buscafiltro').attr('disabled',true);
        $('#timeSelect').prop('selectedIndex',0);
    }
});

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

    $('#fromdate, #todate').on('change keyup', function () {
        DepArrDate = $('#DepArrDate').val();


        fromdate = $('#fromdate').val();
        todate = $('#todate').val();
        muestras = $('select[name=muestras] option').filter(':selected').val();
        muestratext = $( "#muestras option:selected" ).text();
        DepRegFil = $('select[name=DepRegFil] option').filter(':selected').val();
        ArrRegFil = $('select[name=ArrRegFil] option').filter(':selected').val();
        timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
        idGlobal = $('#idDO').val();
        asLots = $('#asLots').val();

        if(fromdate || todate){
            $('#buscafiltro').attr('disabled', false);
        }else if(DepArrDate){
            $('#timeSelect').prop('selectedIndex',0);
            $('#buscafiltro').attr('disabled', true);
        }else if(muestratext != "200 (default)" || DepRegFil || ArrRegFil ||idGlobal || asLots){
            $('#buscafiltro').attr('disabled', false);
        } else $('#buscafiltro').attr('disabled', true);
    });

$('#timeSelect').on('change', function () {
    timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
    if(timeSelect){
        timeSelect = parseInt(timeSelect);
        if(timeSelect != 0){
            let today = new Date();
            //today = today.toLocaleTimeString('es-MX');
            let dateEnMilisegundos = 1000 * 60* 60* 24* timeSelect;
            let suma = today.getTime() - dateEnMilisegundos;
            let fechainicial = new Date(suma);
            //console.log(today);
            datetoday =  formatDate(today);
            datetosearch = formatDate(fechainicial);
            $('#fromdate').val(datetosearch);
            $('#todate').val(datetoday);
            $('#buscafiltro').attr('disabled',false);
        }
        else{
            var date = new Date();
            var primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
            var ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);
      
            primerDia = formatDate(primerDia);
            ultimoDia = formatDate(ultimoDia);
            $('#fromdate').val(primerDia);
            $('#todate').val(ultimoDia);
            $('#buscafiltro').attr('disabled',false);

        }
    
    }else{
        $('#fromdate').val("");
        $('#todate').val("");
        $('#buscafiltro').attr('disabled',true);
    }

    //console.log(datetoday); //today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()
});

// Si cambia el contenido de los input se habilita o bloquea el botón de apply 
$('#muestras,#DepRegFil,#ArrRegFil,#idDO,#asLots').on('change keyup',function(){
    DepArrDate = $('select[name=DepArrDate] option').filter(':selected').val();
    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    muestras = $('select[name=muestras] option').filter(':selected').val();
    muestratext = $( "#muestras option:selected" ).text();
    DepRegFil = $('select[name=DepRegFil] option').filter(':selected').val();
    ArrRegFil = $('select[name=ArrRegFil] option').filter(':selected').val();
    timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
    idGlobal = $('#idDO').val();
    asLots = $('#asLots').val();

    
    if( muestratext != "200 (default)" || DepRegFil || ArrRegFil || idGlobal || asLots){
        if(DepArrDate && fromdate || todate)
             $('#buscafiltro').attr('disabled', false);
        else 
            if(DepArrDate == "")
                $('#buscafiltro').attr('disabled', false);
            else
                $('#buscafiltro').attr('disabled', true);
    }else {
        if(DepArrDate && fromdate || todate)
             $('#buscafiltro').attr('disabled', false);
        else
            $('#buscafiltro').attr('disabled', true);
    }
});


$('#idDO, #asLots').on('change keyup', function(){
    idGlobal = $('#idDO').val();
    asLots = $('#asLots').val();
    if(idGlobal || asLots){
        $('#DepRegFil').attr('disabled', true);
        $('#ArrRegFil').attr('disabled', true);
        $('#muestras').attr('disabled', true);
        $('#muestras').prop('selectedIndex',0);
        $('#DepRegFil').prop('selectedIndex',0);
        $('#ArrRegFil').prop('selectedIndex',0);
    }else{
        $('#DepRegFil').attr('disabled', false);
        $('#ArrRegFil').attr('disabled', false);
        $('#muestras').attr('disabled', false);
    }
});

$('#DepRegFil,#ArrRegFil').on('change keyup', function(){
    DepRegFil = $('#DepRegFil').val();
    ArrRegFil= $('#ArrRegFil').val();
    DepArrDate = $('select[name=DepArrDate] option').filter(':selected').val();
    if(DepRegFil || ArrRegFil){
        $('#idDO').attr('disabled', true);
        $('#asLots').attr('disabled', true);
        $('#muestras').attr('disabled', true);
        $('#muestras').prop('selectedIndex',0);
        $('#idDO').val("");
        $('#asLots').val("");
         if(DepArrDate == ""){
            $('#DepArrDate').prop('selectedIndex',1);
            $('#timeSelect').attr('disabled', false);
            $('#timeSelect').prop('selectedIndex',3);
            timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
            timeSelect = parseInt(timeSelect);
            let today = new Date();
            //today = today.toLocaleTimeString('es-MX');
            let dateEnMilisegundos = 1000 * 60* 60* 24* timeSelect;
            let suma = today.getTime() - dateEnMilisegundos;
            let fechainicial = new Date(suma);
            //console.log(today);
            datetoday =  formatDate(today);
            datetosearch = formatDate(fechainicial);
            $('#fromdate').val(datetosearch);
            $('#fromdate').attr('disabled', false);
            $('#todate').attr('disabled',false);
            $('#todate').val(datetoday);
            $('#buscafiltro').attr('disabled',false);
        }
    }else{
        if(DepArrDate){
            $('#muestras').attr('disabled', true);
        }else
            $('#muestras').attr('disabled', false);
        $('#idDO').attr('disabled', false);
        $('#asLots').attr('disabled', false);
        
    }
});

//Last trucks deshabilita todo
$('#muestras').on('change', function () {
    muestras = $('select[name=muestras] option').filter(':selected').val();
    muestratext = $( "#muestras option:selected" ).text();
    if(muestratext != "200 (default)"){
        $('#idDO').attr('disabled', true);
        $('#asLots').attr('disabled', true);
        $('#DepRegFil').attr('disabled', true);
        $('#ArrRegFil').attr('disabled', true);
        $('#DepArrDate').attr('disabled', true);
        $('#fromdate').attr('disabled', true);
        $('#todate').attr('disabled', true);
        $('#timeSelect').attr('disabled', true);
        $('#fromdate').val("");
        $('#todate').val("");
        $('#timeSelect').val("");
        $('#timeSelect').prop('selectedIndex',0);
        $('#DepArrDate').prop('selectedIndex',0);
        $('#DepRegFil').prop('selectedIndex',0);
        $('#ArrRegFil').prop('selectedIndex',0);
        $('#idDO').val("");
        $('#asLots').val("");
    }else{
        $('#idDO').attr('disabled', false);
        $('#asLots').attr('disabled', false);
        $('#DepRegFil').attr('disabled', false);
        $('#ArrRegFil').attr('disabled', false);
        $('#DepArrDate').attr('disabled', false);
      

    }
});


//id trucks deshabilita todo
$('#idTrk').on('change', function () {
    idTrk = $('#idTrk').val();
    if(idTrk != ""){
        $('#muestras').attr('disabled', true);
        $('#muestras').prop('selectedIndex',0);
        $('#idDO').attr('disabled', true);
        $('#asLots').attr('disabled', true);
        $('#DepRegFil').attr('disabled', true);
        $('#ArrRegFil').attr('disabled', true);
        $('#DepArrDate').attr('disabled', true);
        $('#fromdate').attr('disabled', true);
        $('#todate').attr('disabled', true);
        $('#timeSelect').attr('disabled', true);
        $('#fromdate').val("");
        $('#todate').val("");
        $('#timeSelect').val("");
        $('#timeSelect').prop('selectedIndex',0);
        $('#DepArrDate').prop('selectedIndex',0);
        $('#DepRegFil').prop('selectedIndex',0);
        $('#ArrRegFil').prop('selectedIndex',0);
        $('#idDO').val("");
        $('#asLots').val("");
        $('#buscafiltro').attr('disabled',false);
    }else{
        $('#muestras').attr('disabled', false);
        $('#idDO').attr('disabled', false);
        $('#asLots').attr('disabled', false);
        $('#DepRegFil').attr('disabled', false);
        $('#ArrRegFil').attr('disabled', false);
        $('#DepArrDate').attr('disabled', false);
      

    }
});



var botonglobal = 0;

$("#buscafiltro, #borrarFiltro, #cer").click(function(){
    boton = $(this).val();
    if(boton == 1 || boton == 0)
        botonglobal = boton;
    if(boton != 3){
  //  muestra = Number.isNaN($('#muestras').val()) ? " " : $('#muestras').val();
    muestras =  $('select[name=muestras] option').filter(':selected').val();
    DepRegFil = $('select[name=DepRegFil] option').filter(':selected').val();
    ArrRegFil = $('select[name=ArrRegFil] option').filter(':selected').val();
    DepArrDate = $('select[name=DepArrDate] option').filter(':selected').val();
    idGlobal = $('#idDO').val();
    
    asLots = $('#asLots').val();
    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    
    idTrk = $('#idTrk').val();
    
    //filters = {"muestras":muestras,"regionfil":regionfil};
    opcion = 4;
    var applyFilter =  $.ajax({
        type: 'POST',
        url: 'bd/assignFilters.php',
        data: {
            boton: boton,
            idGlobal:idGlobal,
            asLots: asLots,
            muestras: muestras,
            DepRegFil: DepRegFil,
            ArrRegFil: ArrRegFil,
            DepArrDate: DepArrDate,
            fromdate: fromdate,
            todate: todate,
            idTrk: idTrk 
        }
        
    }) 
    applyFilter.done(function(data){

       // console.log(data);
        tablaTrk.ajax.reload(function(){

            if(boton == 1){
                $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Filtros asignados con éxito</div>')
                setTimeout(function() {
                    $('#alert1').fadeOut(1000);
                    $('#filtrarmodal').modal('hide');
                },1000);
            }
            else{
                // Habilitar botones que pueden estar deshabilitados
                $('#DepRegFil').attr('disabled', false);
                $('#ArrRegFil').attr('disabled', false);
                $('#muestras').attr('disabled', false);
                $('#asLots').attr('disabled', false);
                $('#idDO').attr('disabled', false);
                $('#DepArrDate').attr('disabled', false);
                $('#timeSelect').attr('disabled', true);
                $('#fromdate').attr('disabled', true);
                $('#todate').attr('disabled', true);
    
                $('#muestras').prop('selectedIndex',0);
                $('#DepRegFil').prop('selectedIndex',0);
                $('#ArrRegFil').prop('selectedIndex',0);
                $('#DepArrDate').prop('selectedIndex',0);
                $('#timeSelect').prop('selectedIndex',0);
                $('#idTrk').val("");
                $('#idDO').val("");
                $('#fromdate').val("");
                $('#todate').val("");
                $('#asLots').val("");
                $('#buscafiltro').prop('disabled', true);
                $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"> <i class="bi bi-trash-fill"></i> Filtros borrados con éxito</div>')
                setTimeout(function() {
                    $('#alert1').fadeOut(1500);
                },1500);
            }

        });
        $('#aviso').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');      
    });  
}else{
    if(botonglobal == 0){
        $('#DepRegFil').attr('disabled', false);
        $('#ArrRegFil').attr('disabled', false);
        $('#muestras').attr('disabled', false);
        $('#asLots').attr('disabled', false);
        $('#idDO').attr('disabled', false);
        $('#DepArrDate').attr('disabled', false);
        $('#timeSelect').attr('disabled', true);
        $('#fromdate').attr('disabled', true);
        $('#todate').attr('disabled', true);

        $('#muestras').prop('selectedIndex',0);
        $('#DepRegFil').prop('selectedIndex',0);
        $('#ArrRegFil').prop('selectedIndex',0);
        $('#DepArrDate').prop('selectedIndex',0);
        $('#timeSelect').prop('selectedIndex',0);
        $('#idTrk').val("");
        $('#idDO').val("");
        $('#fromdate').val("");
        $('#todate').val("");
        $('#asLots').val("");
        $('#buscafiltro').prop('disabled', true);
    }}
});

///Boton trucks in transit

$('#transit').on("click", function(){
    window.open("./bd/pdfTransit3.php");
    tablaTrk.ajax.reload(null, false);
});

// reporte de transito modificado
$('#transit2').on("click", function(){
    window.open("./bd/pdfTransit3.php");
    tablaTrk.ajax.reload(null, false);
});

// Habilitar edicion Costo de flete
/*
$('#btnFreightC').on("click", function(){
    $("#FreightC").prop("disabled", false);
});
*/
// asignar mismo valor a Departure y Arrival Date REAL
/*$('#SchOutDat, #SchInDat').on('change', function () {
    SchOutDat = $.trim($('#SchOutDat').val());
    SchInDat = $.trim($('#SchInDat').val());
    $("#OutDat").val(SchOutDat);
    $("#InDat").val(SchInDat);
});*/

// Buscar info de TrkID del boton para eliminar PO's y PR's

$('#RTrkID').on('keyup', function () {
    RTrkID = $.trim($('#RTrkID').val());
    var Info = $.ajax({ //verificamos que exista el transport o no en la bd
        type: "POST",
        url: "./bd/correo-model.php?m=getInfTrkID", //hago referencia al archivo correo model.php y le mando el método getLastPrice
        data: {
            RTrkID: RTrkID // aquí igual se pordría pasar el parámetro m 
        }
    });

    Info.done(function(d){
        if (d == "false"){
            $('#avisoPOPR').html('<div id="alert1" class="alert alert-warning" style="width: 12rem; height: 3rem;" role="alert"><i class="bi bi-x-square-fill"></i> TrkID no existe </div>')
            setTimeout(function() {
                $('#alert1').fadeOut(1000);
            },1000);
            $('#RDO').val("");
            $('#RTyp').val("");
            $('#RPR').val("");
            $('#RPO').val("");
            $('#RLots').val("");
            $('#RTNam').val("");
            $('#RQty').val("");
            $('#RRegSalida').val("");
            $('#RRegLlegada').val("");
            $('#Rfechalleg').val("");
          //  $('#RemPOPR').prop('disabled', true);
            document.getElementById('alertaremove').style.display = 'none';
            $("#checkPO").prop("checked", false);
            $("#checkPR").prop("checked", false);
            $('#RemPOPR').prop('disabled', true);
            $('#checkPO').prop('disabled', true);
            $('#checkPR').prop('disabled', true);
            $('#RemPOPR').prop('disabled', true);
            //$('#RemPR').prop('disabled', true);
        }else{
            data = JSON.parse(d);
            $('#RemPOPR').prop('disabled', false);
            //$('#RemPR').prop('disabled', true);
            $('#RDO').val(data['DO']);
            $('#RPR').val(data['RqstID']);
            $('#RPO').val(data['PO']);
            $('#RLots').val(data['LotsAssc']);
            $('#RTNam').val(data['TNam']);
            $('#RTyp').val(data['Typ']);
            $('#RQty').val(data['CrgQty']);
            $('#RRegSalida').val(data['RegSalida']);
            $('#RRegLlegada').val(data['RegLlegada']);
            $('#Rfechalleg').val(data['OutDat']); 
            if(data['RqstID'] !=0 ){    
            
                $('#checkPR').prop('disabled', false);
            }
            else{
                $('#checkPR').prop('disabled', true);
                $('#RemPOPR').prop('disabled', true);
            }

            if(data['PO'] != 0){
                $('#checkPO').prop('disabled', false);
            }
            else{
                $('#checkPO').prop('disabled', true);
                $('#RemPOPR').prop('disabled', true);
            }
            
        }
    });

});
// habilitar y deshabilitar botones para asociar lotes
$('#LotsAssc').on('change', function () {
    LotID = $.trim($('#LotsAssc').val());
    var Info = $.ajax({ //verificamos que exista el transport o no en la bd
        type: "POST",
        url: "./bd/correo-model.php?m=getTrkID", //hago referencia al archivo correo model.php y le mando el método getLastPrice
        data: {
            LotID: LotID // aquí igual se pordría pasar el parámetro m 
        }
    });

    Info.done(function(d){
        data = JSON.parse(d);
        TrkIDL = data['TrkID'];
        if (TrkIDL == 0){
            $('#btnAddLot').prop('disabled', false);
            $('#btnDelLot').prop('disabled', true);
        }else{
            $('#btnAddLot').prop('disabled', true);
            $('#btnDelLot').prop('disabled', false);
        }
    });

});


// habilitar y deshabilitar botones segun la region del usuario
$(document).on("click", ".btnEditar", function(){
    $("#InData :input").prop("disabled", false);
    fila = $(this).closest("tr");
    usuario = $.trim($('#dropdownMenuLink2').val());
    Salida = fila.find('td:eq(14)').text();
    Entrada = fila.find('td:eq(15)').text();
    Status = fila.find('td:eq(33)').text();
    Priv = $.trim($('#Priv').val());
    
    //validar el esattus
    if (Status == "Received"){
      $('#Status').prop('disabled', true);
      $('#LotsAssc').prop('disabled', true);
    }
    
    var Info = $.ajax({ //verificamos que exista el transport o no en la bd
        type: "POST",
        url: "./bd/correo-model.php?m=getPrivReg", //hago referencia al archivo correo model.php y le mando el método getLastPrice
        data: {
            Priv: Priv // aquí igual se pordría pasar el parámetro m 
        }
    });

    Info.done(function(d){
            data = JSON.parse(d);
            StrReg = [];
            for (i=0; i<data.length; i++){
                StrReg[i] = data[i].RegNam;
            }
            S = jQuery.inArray( Salida, StrReg );
            E = jQuery.inArray( Entrada, StrReg );
            
            if (Status == "Programmed"){
                $("#OutDat, #OutTime, #OutWgh,#InDat, #InTime, #InWgh").prop("disabled", false);
            }else if (S == -1 && E != -1){               
                $('#OutDat').prop('disabled', true);
                $('#OutTime').prop('disabled', true);
                $('#OutWgh').prop('disabled', true);             
                $('#InDat').prop('disabled', false);
                $('#InTime').prop('disabled', false);
                $('#InWgh').prop('disabled', false);
                // bloquer botón tarifario solo para entradas
                $('#btntarifario').prop('disabled', false);
                
              

            }else if (S != -1 && E == -1){              
                $('#OutDat').prop('disabled', false);
                $('#OutTime').prop('disabled', false);
                $('#OutWgh').prop('disabled', false);
              //  $('#SchInDat').prop('disabled', true);
               // $('#SchInTime').prop('disabled', true);
                $('#InDat').prop('disabled', true);
                $('#InTime').prop('disabled', true);
                $('#InWgh').prop('disabled', true);
                //desbloquear  botón tarifario solo para salidas
                $('#btntarifario').prop('disabled', false);
            }else if (S == -1 && E == -1 && usuario != "IVAN FELIX"){               
                $('#OutDat').prop('disabled', true);
                $('#OutTime').prop('disabled', true);
                $('#OutWgh').prop('disabled', true);
              
                $('#InDat').prop('disabled', true);
                $('#InTime').prop('disabled', true);
                $('#InWgh').prop('disabled', true);
                //deshabilitar botones
                $('#btnAddLot').prop('disabled', true);
                $('#btnDelLot').prop('disabled', true);
               // $('#btnFreightC').prop('disabled', true);
                $('#divAsociar').hide();
                 //bloquer botón tarifario
                $('#btntarifario').prop('disabled', true);
            }else if (S == -1 && E == -1 && usuario == "IVAN FELIX"){
                $("#InData :input").prop("disabled", true);
                $('#IrrDat').prop('disabled', false);
            }else if (S != -1 && E != -1){
                $('#OutDat').prop('disabled', false);
                $('#OutTime').prop('disabled', false);
                $('#OutWgh').prop('disabled', false);               
                $('#InDat').prop('disabled', false);
                $('#InTime').prop('disabled', false);
                $('#InWgh').prop('disabled', false);
                 //desbloquear bloquer botón tarifario
                $('#btntarifario').prop('disabled', false);
            }
    });

});

// formato miles (#,###) de manera automatica
$("#OutWgh, #InWgh").on({
    "focus": function(event) {
      $(event.target).select();
    },
    "keyup": function(event) {
      $(event.target).val(function(index, value) {
        return value.replace(/\D/g, "")
          //.replace(/([0-9])([0-9]{2})$/, '$1.$2')
          .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
      });
    }
  });

   //formato xxx-xxx-xxxx automatico
  $("#DrvTel").on({
    "focus": function(event) {
      $(event.target).select();
    },
    "keyup": function(event) {
      $(event.target).val(function(index, value) {
        return value.replace(/\D/g, "")
          //.replace(/([0-9])([0-9]{2})$/, '$1.$2')
          .replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/g, '$1-$2-$3')
      });
    }
  });


});


// VER LA REMISION SUBIDA A AWS S3 
$(document).ready(function(){   
   
    $(document).on("click", ".btnremision", function(){
        fila = $(this).closest("tr");	        
        truck =  fila.find('td:eq(0)').text(); //capturo el ID
        upload = fila.find('td:eq(36)').text();// bandera para saber si la remision ya se encuentra almasenada en AWS S3
  
      
        if(upload == "YES"){
            window.open("opens3.php?TrkID="+truck);
          
        } else {
            alert('The Waybill has not yet been uploaded to the server.');	
        }
    });     
});

function upfile(){
    
    var filename = $("#file").val();
  //  console.log(filename);
    //si es null muestra un mensaje de error
    if(filename == null)
        alert('No ha seleccionado una imagen');
        
    else{// si se eligio un archivo correcto obtiene la extension para vlidarla
        var extension = filename.replace(/^.*\./, '');               
        $('#loadfile').prop('disabled', false);
        $('#update').prop('disabled', false);
        if (extension == filename)
            extension = '';
        else{
            extension = extension.toLowerCase();
          
            //aqui puedes incluir todas las extensiones que quieres permitir
            if(extension != 'pdf' ){
                alert("extencion no valida");
            }
    }
    }


}

/////////AGREGAR DATOS A PREVIEW MAIL///////////////////     
$(document).ready(function(){
    $(document).on("click", ".previewEmail", function (e) {

        e.preventDefault();
        $('#comentarios').val("");
        $('#link').val("");
       var fila = $(this);
       complemetosubjet="";
       var usuario = $('#dropdownMenuLink2').text();
       var delivery = $(this).closest('tr').find('td:eq(1)').text();
       var typeDO = $(this).closest('tr').find('td:eq(2)').text();
       var trkid = $(this).closest('tr').find('td:eq(0)').text();
       var qty = $(this).closest('tr').find('td:eq(4)').text();
       var regsalida = $(this).closest('tr').find('td:eq(14)').text();
       var regarribo = $(this).closest('tr').find('td:eq(15)').text();
       var fechasalida = $(this).closest('tr').find('td:eq(6)').text();
       var horasalida = $(this).closest('tr').find('td:eq(8)').text();
       var fechallegada = $(this).closest('tr').find('td:eq(10)').text();
       var horallegada = $(this).closest('tr').find('td:eq(12)').text();
       var transporte = $(this).closest('tr').find('td:eq(18)').text();
       var talon = $(this).closest('tr').find('td:eq(19)').text();
       var truckplate = $(this).closest('tr').find('td:eq(21)').text();
       var cajaplate = $(this).closest('tr').find('td:eq(22)').text();
       var chofer = $(this).closest('tr').find('td:eq(23)').text();
       var telchofer = $(this).closest('tr').find('td:eq(24)').text();
       var client = $(this).closest('tr').find('td:eq(17)').text();
       var regarrival = $(this).closest('tr').find('td:eq(15)').text();
       var cartaporte = fila.find('td:eq(36)').text(); 
       var samples = $(this).closest('tr').find('td:eq(38)').text();
      // uso el idioma en español para la fecha
       moment.locale('es');       
       var dateTime =moment(fechallegada);
       var dateTime2 =moment(fechasalida);
       var dateTime3 =moment(fechasalida);
     // formato de fecha miercoles 1, junio 2016
      fecha_descarga = dateTime.format('dddd D, MMMM YYYY');
      fecha_carga = dateTime2.format('dddd D, MMMM YYYY');
      fecha_asunto = dateTime3.format('DD/MM');


      
        var cliente = $(this).closest('tr').find('td:eq(17)').text();
      

       var Info = $.ajax({ //verificamos que exista el transport o no en la bd
        type: "POST",
        url: "bd/getCorreo-truck.php",
            data: {
                   usuario:usuario, 
                   delivery:delivery,
                   typeDO:typeDO, 
                   trkid:trkid, 
                   qty:qty, 
                   regsalida:regsalida, 
                   regarribo:regarribo, 
                   fechasalida:fechasalida,
                   fechallegada:fechallegada,
                   transporte:transporte,
                   samples:samples,
                   cliente:cliente                
            }, success: function(data){
                datos = JSON.parse(data);
                $(".modal-header").css( "background-color", "#17562c");
                $(".modal-header").css( "color", "white" );
               // const resultado = Object.keys(datos)
               // console.log(resultado);
               // console.log(datos['descarga']);
                //const resultado2 = Object.keys(datos['descarga']['0']);
                descarga = datos['descarga']['0'].Drctn;
                regdescargar = datos['descarga']['0'].BnName;
                refdescarga = datos['descarga']['0'].Ref;
                Maps = datos['descarga']['0'].Maps;

                carga = datos['lugarcarga']['0'].BnName;
                dircarga = datos['lugarcarga']['0'].Drctn;
                refcarga = datos['lugarcarga']['0'].Ref;
                asunto_reg = datos['lugarcarga']['0'].nombre;
                mailsorigen = datos['mailorigen']['0'].mails;
                mailtransporte = datos['mailtransport']['0'].mails;
                mailamsa= datos['mailamsa']['0'].mails;
                mailHVI= datos['mailHVI'];
                certificado = datos['certificate']['0'].Certificate;

               // console.log(mailsorigen);



                 lugar_carga = "Lugar:    " + carga + "\n";
                 direccion_carga ="Direccion:    " + dircarga + "\n";
                 referencia_carga="Referencia:  " + refcarga + "\n";
                 fecha_salida = "Fecha: " + fecha_carga + "\n"
                




                direccion_descarga = "Direccion:    " + descarga + "\n"
                lugar_descarga = "Lugar:    "+ regdescargar + "\n" 
                referencia_descarga = "Referencia:  " + refdescarga + "\n"
                fecha_llegada = "Fecha: " + fecha_descarga + "\n"



                datos_trasporte = "Linea:   " + transporte + "\n"
                placastruck = "Placas Tracto:  " + truckplate + "\n"
                placascaja =  "Placas Caja:  " + cajaplate + "\n"
                operadortruck = "Operador:  " + chofer + "\n"
                talontruck= "Talon: "  + talon + "\n"
                cantidad_asunto= datos['cantidad']['0'].cantidad;
                cartaporte= datos['cartaporte']['0'].RemisionPDF;
                lotesmail = datos['lotesmail']

                
                link = "https://www.google.com/maps/search/?api=1&query=" + Maps +"&zoom=20";

               

            


                $('#tablahtml').val(datos['tabla']);
                $('#usuario').val(usuario);
                $('#lugar_carga').val(carga);
                $('#direccion_carga').val(dircarga);
                $('#referencia_carga').val(refcarga);
                $('#fecha_salida').val(fecha_carga);
                $('#direccion_descarga').val(descarga);
                $('#lugar_descarga').val(regdescargar);
                $('#referencia_descarga').val(refdescarga);
                $('#fecha_llegada').val(fecha_descarga);
              //  $('#datos_trasporte').val(transporte);
               // $('#placastruck').val(truckplate);
               // $('#placascaja').val(cajaplate);
              //  $('#operadortruck').val(chofer);
              //  $('#talontruck').val(talon);
              //  $('#telchofer').val(telchofer);
                $('#delivery').val(delivery);
                $('#client').val(client);
                $('#regarrival').val(regarrival);
                $('#trkid').val(trkid);
                $('#typeDO').val(typeDO);
                $('#horasalida').val(horasalida);
                $('#horallegada').val(horallegada);
                $('#mailtransporte').val(mailtransporte);
                
                if (Maps != ""){               
                $('#link').val(link);
                } 
                linea1 = "\n"+"Datos de Carga:"+ "\n"
                linea2 = "\n"+"Datos de Descarga:"+  "\n"
                linea3 = "\n"+"Datos de Transporte"+ "\n"
                linea4 = "\n"+"Lotes"+ "\n"

                
                //$('#mailamsa').val(mailamsa);
                
                if(mailHVI !=""){
                     $('#mailamsa').val(mailamsa +  mailHVI);
                }
                else{
                    $('#mailamsa').val(mailamsa);
                }

                //AGREGAR CERTIFICADO DE ORIGEN SI SE REQUIERE

                if (certificado == 1){
                    complemetosubjet = " - CERTIFICADOS DE ORIGEN";

                 }
                 else{
                    complemetosubjet = "";
                 }

              
                
                if(mailsorigen !=0){
                document.getElementById('origenes').style.display = 'block';
                $('#mailorigen').val(mailsorigen);
                 }
                 else{

                    document.getElementById('origenes').style.display = 'none';
                    $('#mailorigen').val("");

                 }
                 
                



                 



                //console.log(datos['descarga']['0'].Drctn);


                $('#datoscarga').val(linea1+"   "+fecha_salida+"   "+lugar_carga+"   "+direccion_carga+"   "+referencia_carga + linea2 +"   "+ fecha_llegada+"   "+lugar_descarga+"   "+direccion_descarga+"   "+referencia_descarga+linea3+"   "+datos_trasporte+"   "+operadortruck+"   "+placastruck+"   "+placascaja+"   "+talontruck + linea4 + lotesmail);
                ///$('#datosdescarga').html(fecha_llegada+lugar_descarga+direccion_descarga+referencia_descarga);
                
               // $('#datostransport').html(datos_trasporte+operadortruck+placastruck+placascaja+talontruck);
               // $('#tablahtml').val(datos['tabla']);
               $('#datoscarga').html(datos['tabla']);
               $('#tablamue').html(datos['tabla']);
               //$('#subject').val("UNIDAD AUTORIZADA PARA CARGA "+ fecha_asunto +", "+asunto_reg+" ("+cantidad_asunto+" bc)"  )
               if (samples=="True"){
                    $('#subject').val("UNIDAD AUTORIZADA PARA CARGA "+ fecha_asunto +", "+asunto_reg+" ("+cantidad_asunto+" bc + muestras) " +complemetosubjet   )
                }
                else{
                    $('#subject').val("UNIDAD AUTORIZADA PARA CARGA "+ fecha_asunto +", "+asunto_reg+" ("+cantidad_asunto+" bc)" + complemetosubjet )
                }

              //console.table(datos);
               // $('#datoscarga').val(datos['lugarcarga'].BnName);

            if (cantidad_asunto == null){
                document.getElementById("estatus").innerHTML = "the lot has not been assigned a TRUK ID!";
                
             }
             else {
              document.getElementById("estatus").innerHTML ="";
              
             }

         
                
                //$('#datosdescarga').val(datos["descarga"]);
          
                $('#Correo').modal('show');
////if(cartaporte == 0 || cantidad_asunto == null || XML ==0){
                if(cartaporte == 0 || cantidad_asunto == null){
                    $('#sendEmail').prop('disabled', true);
                }
                else{
                    $('#sendEmail').prop('disabled', false);
                }
            }
        });




      
    })



    ///FUNCION PARA EL BOTON ENVIAR CORREO ELECTRONICO

    $(document).on("click",".sendEmail",function(){
        tablahtml = $('#tablahtml').val();
        lugar_carga    = $('#lugar_carga').val();
        direccion_carga     = $('#direccion_carga').val();       
        referencia_carga   = $('#referencia_carga').val();
        fecha_salida    = $('#fecha_salida').val();
       // datos_trasporte   = $('#datos_trasporte').val();
       // placastruck    = $('#placastruck').val();
       // placascaja    = $('#placascaja').val();
       // operadortruck    = $('#operadortruck').val();
       // talontruck    = $('#talontruck').val();
       // telchofer    = $('#telchofer').val();
        usuario    = $('#usuario').val();
      //  telchofer    = $('#telchofer').val();
        asunto = $('#subject').val();
        fecha_llegada=$('#fecha_llegada').val();
        lugar_descarga=$('#lugar_descarga').val();
        direccion_descarga = $('#direccion_descarga').val();
        referencia_descarga = $('#referencia_descarga').val();
        delivery = $('#delivery').val();
        client = $('#client').val();
        regarrival = $('#regarrival').val();
        trkid= $('#trkid').val();
        typeDO= $('#typeDO').val();
        link= $('#link').val();
        horallegada= $('#horallegada').val();
        horasalida= $('#horasalida').val();
        comentarios= $('#comentarios').val();
        mailamsa = $('#mailamsa').val();
        mailorigen=$('#mailorigen').val();
        mailtransporte= $('#mailtransporte').val();

        $('#sendEmail').prop('disabled', true);

        //totalQty  = $('#totalQty').val();
        subject   = $('#subject').val();
        $('#avisomail').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');  
        $.ajax({
            type: "POST",
            url: "bd/correo-truck.php",
                data: {
                    tablahtml:tablahtml, 
                    lugar_carga:lugar_carga,
                    direccion_carga:direccion_carga, 
                    referencia_carga:referencia_carga, 
                    fecha_salida:fecha_salida,           
                    usuario:usuario,                          
                    asunto:asunto,
                    fecha_llegada:fecha_llegada,
                    lugar_descarga:lugar_descarga,
                    direccion_descarga:direccion_descarga,
                    referencia_descarga:referencia_descarga,
                    delivery:delivery,
                    client:client,
                    regarrival:regarrival,
                    typeDO:typeDO,
                    trkid:trkid,
                    link:link,
                    horallegada:horallegada,
                    horasalida:horasalida,
                    comentarios:comentarios,
                    mailamsa:mailamsa,
                    mailorigen:mailorigen,
                    mailtransporte:mailtransporte

                      

                }, success: function(data){
                    alert("Correo enviado con exito");
                    $('#Correo').modal('hide');
                    $('#avisomail').html("");

                },
                error: function (data){
                    alert("Error al enviar datos");
                    $('#Correo').modal('hide');

                }

            })
       
      
    
        
     });
     
     
      //REPORTE DE PESOS
     $("#pesosTrk").click(function(e){
        e.preventDefault();
        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $('#reportepesos').modal('show');
        $("#form_pesos")[0].reset();  
        $('#cliente_peso').prop('disabled', true);

        rango_fechas_peso(30) 


        $('#timeSelect_pesos').change(function () {
          //  timeSelect = $("#timeSelect_report").val();
            rango_fechas_peso($(this).val())               
            
        });


        $('#regllegada_pesos').change(function () {
            if($(this).val() == "CLIENTE"){
                $('#cliente_peso').prop('disabled', false);
            }
            else{
                $('#cliente_peso').prop('disabled', true);
            }
        });


        $("#generar_report_peso").click(function(e){ 
                e.preventDefault();
                salidareg= $('#regsalida_pesos').val();
                llegadareg=$('#regllegada_pesos').val();
                cliente= $('#cliente_peso').val();
                fromdate_pesos= $('#fromdate_pesos').val();
                todate_pesos= $('#todate_pesos').val();    
                origen= $('#gin_peso').val();               
                window.open("./bd/weightreport.php?salidareg="+salidareg+"&llegadareg="+llegadareg+"&fromdate_pesos="+fromdate_pesos+"&todate_pesos="+todate_pesos+"&cliente="+cliente+"&origen="+origen);
        });

        $("#limpiar_report_peso").click(function(e){ 
            $("#form_pesos")[0].reset(); 
            $('#cliente_peso').prop('disabled', true); 
            rango_fechas_peso(30) 
           
         });

        
     });
     
     
     
     $("#reportpagos").click(function(e){
        e.preventDefault();
        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        //$(".modal-title").text("Missing PR/PO/RO");
        $('#reportePO-PR-RO').modal('show');
        $("#form-PR-PO-RO")[0].reset();  
        $("#transport_report").empty();
        $('#fromdate_report').prop('disabled', true);
        $('#todate_report').prop('disabled', true);
        transportes_all();
        $("#filterAll").prop("checked", true);
        $("#filterPR").prop("checked", false);
        $("#filterPO").prop("checked", false);
        $("#filterRO").prop("checked", false);  
        rango_fechas(60) 
    
        //variables para los checks 
        var fpo = 0;
        var fro= 0;
        var fpr = 0;
        var fall = 0;
      
        //mostrar solo las leneas de transporte activas en la region de salida
        $("#regsalida_report").change(function () {
            if($("#regsalida_report").val()==""){
                transportes_all();
            }
            else{
                transportes_reg_salida();
            }
        });
    
        $("#regllegada_report").change(function () {
            if($("#regsalida_report").val()=="" && $("#transport_report").val()==""){
                transportes_all();
            }
           
        });
    
        //validar los checks 
        
        $('#filterAll').on('change',function(){
            if (this.checked) {
                $("#filterPR").prop("checked", false);
                $("#filterPO").prop("checked", false);
                $("#filterRO").prop("checked", false); 
                fall = 1;
                fpr = 0;
                fpo= 0;
                fro=0;       
            }
            else{
                ffall = 0;
            }
    

        });
    
        $('#filterPR').on('change',function(){
            
            if (this.checked) {
                $("#filterAll").prop("checked", false); 
                fpr = 1;
                fall = 0;
                if($('#filterPO').prop('checked') && $('#filterRO').prop('checked')){
                    $("#filterPR").prop("checked", false);
                    $("#filterPO").prop("checked", false);
                    $("#filterRO").prop("checked", false); 
                    $("#filterAll").prop("checked", true);    
                    fall = 1;
                    fpr = 0;
                    fpo= 0;
                    fro=0;            
                }
     
            }
            else{
                fpr=0;
            }
    
    
        });
    
        $('#filterPO').on('change',function(){
            
            if (this.checked) {
                $("#filterAll").prop("checked", false); 
                fpo = 1;
                fall = 0;
                if($('#filterPR').prop('checked') && $('#filterRO').prop('checked')){
                    $("#filterPR").prop("checked", false);
                    $("#filterPO").prop("checked", false);
                    $("#filterRO").prop("checked", false); 
                    $("#filterAll").prop("checked", true);   
                    fall = 1;
                    fpr = 0;
                    fpo= 0;
                    fro=0;             
                }
     
            }
            else{
                fpo=0;
            }    
  
        });
    
        $('#filterRO').on('change',function(){
            
            if (this.checked) {
                $("#filterAll").prop("checked", false); 
                fro = 1;
                fall = 0;
                if($('#filterPR').prop('checked') && $('#filterPO').prop('checked')){
                    $("#filterPR").prop("checked", false);
                    $("#filterPO").prop("checked", false);
                    $("#filterRO").prop("checked", false); 
                    $("#filterAll").prop("checked", true);
                    fall = 1;
                    fpr = 0;
                    fpo= 0;
                    fro=0;                
                }
     
            }
            else{
                fro=0;
            }
   
        });
    
    
    
    
    
        $('#timeSelect_report').change(function () {
            timeSelect = $("#timeSelect_report").val();
            rango_fechas(timeSelect)       
        
            //console.log(datetoday); //today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()
        });
        
        $('#generar_report').click(function(e){
            e.preventDefault();
            regsalida=$('#regsalida_report').val();
            regllegada=$('#regllegada_report').val();
            linea=$('#transport_report').val();
            fromdate=$('#fromdate_report').val();
            todate= $('#todate_report').val();
            window.open("./bd/ReportMissingPO_PR_RO.php?regsalida="+regsalida+"&regllegada="+regllegada+"&fromdate="+fromdate+"&todate="+todate+"&linea="+linea+"&all="+fall+"&pr="+fpr+"&po="+fpo+"&ro="+fro);
            link="./bd/ReportMissingPO_PR_RO.php?regsalida="+regsalida+"&regllegada="+regllegada+"&fromdate="+fromdate+"&todate="+todate+"&linea="+linea+"&all="+fall+"&pr="+fpr+"&po="+fpo+"&ro="+fro;
            console.log("LINK:" + link);
        });
    
        $('#limpiar_report').click(function(e){
            transportes_all()
            $("#form-PR-PO-RO")[0].reset()  
            //$('#fromdate_report').prop('disabled', true);
            //$('#todate_report').prop('disabled', true);
            rango_fechas(60) 
        
        });
    
    
    });




   
})

//----------------Validar que se cargo archivo XML------------
function upfilexml(){
    
    var filename = $("#filexml").val();
  //  console.log(filename);
    //si es null muestra un mensaje de error
    if(filename == null){
        alert('No ha seleccionado una imagen');
        $('#loadxml').prop('disabled', true);
        $('#updatexml').prop('disabled', true);
    }
    else{// si se eligio un archivo correcto obtiene la extension para vlidarla   
        $('#loadxml').prop('disabled', false);
        $('#updatexml').prop('disabled', false);
    }
}



    //MODAL PARA REPORTE TRANSITS
    $(document).ready(function(){
        var date=new Date();  
        date.setDate(date.getDate() + 3);
        var day=String(date.getDate()).padStart(2, '0');          
        var month=("0" + (date.getMonth() + 1)).slice(-2); 
        var year=date.getFullYear();  
        fechadef=year+"-"+month+"-"+day 

        var date2=new Date();  
        date2.setDate(date2.getDate() - 60);
        var day2=String(date2.getDate()).padStart(2, '0');          
        var month2=("0" + (date2.getMonth() + 1)).slice(-2); 
        var year2=date2.getFullYear();  
        fechadef2=year2+"-"+month2+"-"+day2 ;

    $("#reportetransit").click(function(){

       
        //console.log(fechadef);
        //reset multiselect
        $('.selectpicker').val('');
        $('.selectpicker').selectpicker('refresh');

        $("#filtrosreporte")[0].reset();
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Generate Report");
        $("#modalTitle").html("Generate Report");
        $("#modalTitle").html("New Truck");
        $('#modalreporte').modal("show");
        $('#timeSelect_transits').attr('disabled', true);
        $('#fromdate_transits').attr('disabled', true);
        $('#todate_transits').attr('disabled', true);
        $('#generar_transits').attr('disabled', true);
        document.getElementById('filtros_reporteNAV').style.display = 'block';
        document.getElementById('subir_reporteNAV').style.display = 'none';
        document.getElementById('limpiar_transits').style.display = 'block';
        document.getElementById('generar_transits').style.display = 'block';
        document.getElementById('btnloadfile_transit').style.display = 'none';
        $("#pendientesnav").prop("checked", true); 
        $("#muestras_transito").prop("checked", false);
        $("#allnav").prop("checked", false);

        $('#todate_transits').val(fechadef)
        $('#fromdate_transits').val(fechadef2)
    });


    $("#limpiar_transits").click(function(){
        $("#filtrosreporte")[0].reset();    
        $('#timeSelect_transits').attr('disabled', true);
        $('#fromdate_transits').attr('disabled', true);
        $('#todate_transits').attr('disabled', true);
        $('#generar_transits').attr('disabled', true);
        $("#pendientesnav").prop("checked", true);
        $("#muestras_transito").prop("checked", false);
        $('#todate_transits').val(fechadef)
        $('#fromdate_transits').val(fechadef2)
        //reset multiselect
        $('.selectpicker').val('');
        $('.selectpicker').selectpicker('refresh');

    });

    //validar los check box
    $('#pendientesnav').on('change',function(){
        if (this.checked) {
            $("#allnav").prop("checked", false);
            $("#muestras_transito").prop("checked", false);
        
        }
    });

    $('#allnav').on('change',function(){
        if (this.checked) {
            $("#pendientesnav").prop("checked", false);           
            $("#muestras_transito").prop("checked", false);
        }

    });

    $('#muestras_transito').on('change',function(){
        if (this.checked) {
            $("#pendientesnav").prop("checked", false); 
            $("#allnav").prop("checked", false);    
            //console.log("muestras");      
            
        }

    });






    $('#generar_transits').click(function(e){
        e.preventDefault();
        var pendiente =0;
        var muestras = 0;
        if($('#allnav').prop('checked')){
            pendiente = 0;
        }

        if($('#pendientesnav').prop('checked')){
            pendiente = 1;
        }

        if($('#muestras_transito').prop('checked')){
            muestras = 1;
        }


        regsalida=$('#regsalida_transits').val();
        regllegada=$('#regllegada_transits').val();
        fromdate=$('#fromdate_transits').val();
        todate= $('#todate_transits').val();
        statustrk = $('#statustrk').val();

        if(regsalida==null){
            regsalida="";
        }

        if(regllegada==null){
            regllegada="";
        }
   
        window.open("./bd/reporte-transit.php?regsalida="+regsalida+"&regllegada="+regllegada+"&fromdate="+fromdate+"&todate="+todate+"&statustrk="+statustrk+"&pendiente="+pendiente+"&muestras="+muestras);
        //link = "./bd/reporte-transit.php?regsalida="+regsalida+"&regllegada="+regllegada+"&fromdate="+fromdate+"&todate="+todate+"&statustrk="+statustrk+"&pendiente="+pendiente+"&muestras="+muestras;
        //console.log(link)

    });


    $('#regsalida_transits,#regllegada_transits').on('change keyup', function(){
        regsalida = $('#regsalida_transits').val();
        regllegada= $('#regllegada_transits').val();
        if(regsalida || regllegada){

            $('#timeSelect_transits').attr('disabled', false);
            $('#fromdate_transits').attr('disabled', false);
            $('#todate_transits').attr('disabled', false);
            $('#generar_transits').attr('disabled', false);
            //$('#generar').attr('disabled', false);
        
        }
        else{

            $('#timeSelect_transits').val("");
            $('#fromdate_transits').val("");
            $('#todate_transits').val("");
            $('#todate_transits').val(fechadef)
            $('#fromdate_transits').val(fechadef2)

            $('#timeSelect_transits').attr('disabled', true);
            $('#fromdate_transits').attr('disabled', true);
            $('#todate_transits').attr('disabled', true);
            $('#generar_transits').attr('disabled', true);

                        //reset multiselect
            $('.selectpicker').val('');
            $('.selectpicker').selectpicker('refresh');
        

        }

    });

    //botones para mostrar el tipo de modal 

    $('#btnreport_transit').click(function(e){
        e.preventDefault();
        document.getElementById('filtros_reporteNAV').style.display = 'block';
        document.getElementById('subir_reporteNAV').style.display = 'none';
        document.getElementById('limpiar_transits').style.display = 'block';
        document.getElementById('generar_transits').style.display = 'block';
        document.getElementById('btnloadfile_transit').style.display = 'none';
        $(".modal-title").text("Generate Report");
        //$("#modalreporte").html("Generate Report");
        

        


    });
    //para subir el csv de los trucks

    $('#btnsube_reporte').click(function(e){
        e.preventDefault();
        document.getElementById('filtros_reporteNAV').style.display = 'none';
        document.getElementById('subir_reporteNAV').style.display = 'block';
        document.getElementById('limpiar_transits').style.display = 'none';
        document.getElementById('generar_transits').style.display = 'none';
        document.getElementById('btnloadfile_transit').style.display = 'block';       
        $('#btnloadfile_transit').prop('disabled', true);   
        //$("#modalreporte").html("Load CSV");
        //$('#modalreporte').text($(".modal-title").attr('title'));

        //desbloquear botón cuando seleccione un file 
        $("#subir_reporteNAV").change(function(){           
            $('#btnloadfile_transit').prop('disabled', false); 
            
        });


    });
    //activar botón para generar reporte 
   /* $('#fromdate_transits').on('change', function () {
        $('#generar_transits').attr('disabled', false);

    });*/

    $('#timeSelect_transits').on('change', function () {
    timeSelect = $('select[name=timeSelect_transits] option').filter(':selected').val();
    if(timeSelect){
        timeSelect = parseInt(timeSelect);
        if(timeSelect != 0){
            var today = new Date();

            //today = today.toLocaleTimeString('es-MX');
            let dateEnMilisegundos = 1000 * 60* 60* 24* timeSelect;
            let suma = today.getTime() - dateEnMilisegundos;
            let fechainicial = new Date(suma);
            //console.log(today);
            datetoday =  formatDate(today);
            datetosearch = formatDate(fechainicial);
            $('#fromdate_transits').val(datetosearch);
            $('#todate_transits').val(datetoday);
            $('#buscafiltro').attr('disabled',false);

            $('#generar_transits').attr('disabled', false);
        }
        else{

            var date = new Date();
            var primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
            var ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);      
            primerDia = formatDate(primerDia);
            ultimoDia = formatDate(ultimoDia);
            $('#fromdate_transits').val(primerDia);
            $('#todate_transits').val(ultimoDia);
            $('#buscafiltro').attr('disabled',false);
            $('#generar_transits').attr('disabled', false);
        }

    }else{
        $('#fromdate_transits').val("");
        $('#todate_transits').val("");
    // $('#buscafiltro').attr('disabled',true);
        $('#generar_transits').attr('disabled', true);
    }

    //console.log(datetoday); //today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()
    });


    //enviar csv para acutalizar el estatus de los trucks

    $('#btnloadfile_transit').on('click', function(e) {
    e.preventDefault();    
    var file_data = $('#filecsv_transit').prop('files')[0];  
    var form_data = new FormData();             
    form_data.append('file', file_data);   

    $('#avisofiletransit').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>'); 
    //alert(form_data);                             
    $.ajax({
        url: 'bd/loadtransits.php', // <-- point to server-side PHP script 
        dataType: 'text',  // <-- what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        success: function(data){
            datos = JSON.parse(data);
            data = datos['Status'];
            cont = datos['cont'];
            alert("file load success")
            $('#avisofiletransit').html("");
            $('#avisofiletransit').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> File load</div>')
            setTimeout(function() {
                $('#alert1').fadeOut(50000);
                //$('#filtrarmodal').modal('hide');
            },10000);
            $('#avisofiletransit').html("");
            var $el = $('#filecsv_transit');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();  
            $('#btnloadfile_transit').prop('disabled', true);            
                    

        },
        error: function(data){
            $('#avisofiletransit').html("");
            $('#avisofiletransit').html('<div id="alert1" class="alert alert-warning" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i>Error to load file</div>')
            setTimeout(function() {
                $('#alert1').fadeOut(50000);
                //$('#filtrarmodal').modal('hide');
            },10000);
            $('#avisofiletransit').html("");
            var $el = $('#filecsv_transit');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();        
            $('#btnloadfile_transit').prop('disabled', true);   
        }

    });




    });
});





//MODAL PARA REPORTE INVOICE
$(document).ready(function(){
    $("#reporteinvoice").click(function(){
        var filterDOM=0;
        var filterEXP=0;
        $("#forminvoices")[0].reset();
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Report Invoices");        
        $('#modalinvoices').modal("show");
        document.getElementById('filtros_invoice').style.display = 'block';
        document.getElementById('subir_reporteInv').style.display = 'none';
        //document.getElementById('limpiar_transits').style.display = 'block';
        document.getElementById('generar_invoices').style.display = 'block';
        document.getElementById('btnloadfile_invoice').style.display = 'none';
        $("#filterDOM").prop("checked", true);
        $("#filterEXP").prop("checked", true);

        
    });

    //vista para descargar reporte
    $('#btnreport_invoice').click(function(e){
        e.preventDefault();
        document.getElementById('filtros_invoice').style.display = 'block';
        document.getElementById('subir_reporteInv').style.display = 'none';
        //document.getElementById('limpiar_transits').style.display = 'block';
        document.getElementById('generar_invoices').style.display = 'block';
        document.getElementById('btnloadfile_invoice').style.display = 'none';
        });


    //vista para subir el csv de los trucks

    $('#btnsube_invoice').click(function(e){
        e.preventDefault();
  
        document.getElementById('filtros_invoice').style.display = 'none';
        document.getElementById('subir_reporteInv').style.display = 'block';
        //document.getElementById('limpiar_transits').style.display = 'none';
        document.getElementById('generar_invoices').style.display = 'none';
        document.getElementById('btnloadfile_invoice').style.display = 'block';  
        var $el = $('#filecsv_invoice');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();       
        $('#btnloadfile_invoice').prop('disabled', true);   
        //$("#modalreporte").html("Load CSV");
        //$('#modalreporte').text($(".modal-title").attr('title'));
        

        //Hacer la validacion del archivo seleccionado 
        $("#filecsv_invoice").change(function(){           
            valida_csv_invoice();
        });
    });

    $('#generar_invoices').click(function(e){
        e.preventDefault();
        if($('#filterDOM').prop('checked')){
            filterDOM = 1;

        }
        else{
            filterDOM = 0;
        }

        if($('#filterEXP').prop('checked')){
            filterEXP = 1;
        }

        else{
            filterEXP = 0;
        }

        
        if(filterDOM!=0 || filterEXP !=0){
         window.open("./bd/invoices.php?filterDOM="+filterDOM+"&filterEXP="+filterEXP);   
        }
        else{
            alert("you must check one of the following options.")
        }
         
    });



    $('#btnloadfile_invoice').on('click', function(e) {
        e.preventDefault();    
        var file_data = $('#filecsv_invoice').prop('files')[0];  
        var form_data = new FormData();             
        form_data.append('file', file_data);   

        $.ajax({
            url: 'bd/loadinvoice.php', // <-- point to server-side PHP script 
            dataType: 'text',  // <-- what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(data){
               // datos = JSON.parse(data);
               // cont = datos['cont'];
                //cont = data['cont'];

                alert("Successful file upload" + '\n' +"Modified Trucks: "+ data);
                var $el = $('#filecsv_invoice');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();  
                $('#btnloadfile_invoice').prop('disabled', true);      
                tablaTrk.ajax.reload(null,false);     
            },
            error(data){
                alert("Error to load file");
                var $el = $('#filecsv_invoice');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();  
                $('#btnloadfile_invoice').prop('disabled', true);    
            }

        });
    });
    

});








//----------------DESPLEGAR MODAL DE TARIFARIO------------
$(document).ready(function(){


    internationalNumberFormat = new Intl.NumberFormat('en-US')
    

    $("#btntarifario").click(function(e){ 
        e.preventDefault(e);  
        $('#tarifario-costototal').val("");  
        $('#tarifario-costoprom').val(0);
        $('#tarifario-comentarios').val("");
        $("#tarifario-costototal").attr("readonly", false); 
        $("#tarifario-comentarios").attr("readonly", false); 
        document.getElementById('alertacosto').style.display = 'none';
        $('#tarifario-save').prop('disabled', false);
        document.getElementById('tarifa_inactiva').style.display = 'none';
        document.getElementById('sin_transporte').style.display = 'none';
       
        

        var combo = document.getElementById("TNam");
        var transporte = combo.options[combo.selectedIndex].text;
        dor = $('#DOrd').val();
        regsal = $('#OutRegDO').val();
        regllegada = $('#InRegDO').val();
        qty = $('#QtyDO').val();
        if(idglobaltruck ===0 || idglobaltruck ==="" || idglobaltruck === null){
            TrkID= $('#exampleModalLabel').val()
        }
        else{
            TrkID = idglobaltruck;
        }

    

        //console.log(dor);

        $.ajax({
            url: "bd/crudtarifario.php",
            type: "POST",
            datatype:"json",
            data:  {dor:dor, regsal:regsal, regllegada:regllegada,transporte:transporte,qty:qty,TrkID:TrkID},    
            success: function(data){

                opts = JSON.parse(data);
                city = opts['city'];
                zone = opts['Zone'];
                ginname = opts['NameGine'];
                costopaca = opts['costo'];
                costototal = opts['costos_total'];
                TotalCost=opts['TotalCost'];
                AverageCost=opts['AverageCost'];
                FreighCostNote=opts['FreighCostNote'];
                estado=opts['estado'];

               // console.log(zone);
               // console.log(ginname);
               // console.log(city);

                $('#tarifario-gin').val(ginname);
                $('#tarifario-zonasalida').val(zone);
                $('#tarifario-cityllegada').val(city);
                $('#tarifario-costopaca').val(costopaca);
                costototal= separator(costototal)
                $('#tarifario-costoauto').val(costototal);
               // $('#tarifario').modal('show');

               if(estado =='inactivo'){
                document.getElementById('tarifa_inactiva').style.display = 'block';
                document.getElementById('mensaje').innerHTML='La tarifa Autorizada ya no es vigente desde '+  opts['fechafin'];;
               }

                //PREGUNTAR SI DESEA MODIFICAR LA TARIFA YA CAPTURADA
                if(TotalCost!=0){
                    let text = "Tarifa ya capturada.\n ¿Desea modificar la tarifa?";
                    if (confirm(text) == true) {

                            
                        $(".modal-header").css("background-color", "#17562c");
                        $(".modal-header").css("color", "white" );       
                        $('#tarifario').modal('show'); 		
                        
                        $("#tarifario-costototal").attr("readonly", false); 
                        $("#tarifario-comentarios").attr("readonly", false); 
                        if( $('#samples').prop('checked') ) {
                            $('#tarifario-muestras').val("TRUE");
                          
                        }  else {
                            $('#tarifario-muestras').val("FALSE");
                        }             
                        
                     
                        //console.log(costo_autorizado);
                
                    
                        $('#tarifario-trasnport').val(transporte);
                        $('#tarifario-regsalida').val($('#OutRegDO').val());
                        $('#tarifario-regllegada').val($('#InRegDO').val());
                        $('#tarifario-cantidad').val($('#QtyDO').val());
                       // TotalCost = separator(TotalCost);
                        //$('#tarifario-costoprom').val(AverageCost);
                        //$('#tarifario-costototal').val(TotalCost);
                        $('#tarifario-comentarios').val(FreighCostNote);

                    } else {
                        $('#tarifario').modal("hide");

                    }
                }

                else{

                    if(qty>0){

                        $(".modal-header").css("background-color", "#17562c");
                        $(".modal-header").css("color", "white" );       
                        $('#tarifario').modal('show'); 		
                        
                        $("#tarifario-costototal").attr("readonly", false); 
                        $("#tarifario-comentarios").attr("readonly", false); 
                        if( $('#samples').prop('checked') ) {
                            $('#tarifario-muestras').val("TRUE");
                          
                        }  else {
                            $('#tarifario-muestras').val("FALSE");
                        }             
                        
                     
                        //console.log(costo_autorizado);
                
                    
                        $('#tarifario-trasnport').val(transporte);
                        $('#tarifario-regsalida').val($('#OutRegDO').val());
                        $('#tarifario-regllegada').val($('#InRegDO').val());
                        $('#tarifario-cantidad').val($('#QtyDO').val());
                        //TotalCost = separator(TotalCost);
                        //$('#tarifario-costoprom').val(AverageCost);
                        //$('#tarifario-costototal').val(TotalCost);
                        $('#tarifario-comentarios').val(FreighCostNote);
                        
                        
                         if (transporte ==""|| transporte=="- Select -"){
                            document.getElementById('sin_transporte').style.display = 'block';
                            $('#tarifario-save').prop('disabled', true);
                        }
                    }else{
                        alert("No hay lotes asociados al Truck")
                        $('#tarifario').modal("hide");
                    }



                }
               
                
               
            },
            error: function(data) {
                alert('error');
            }
        });

        

    })

//Funcion para guardar cambios en tarifario
    $("#tarifario-save").click(function(e){ 
        e.preventDefault();
       costoprom= $('#tarifario-costoprom').val();
       costot=$('#tarifario-costototal').val();
       notas= $('#tarifario-comentarios').val();
       costoprom = costoprom.replace(/,/g, ''); 
       costot = costot.replace(/,/g, ''); 
       if(idglobaltruck ===0 || idglobaltruck ==="" || idglobaltruck === null){
            TrkID= $('#exampleModalLabel').val()
       }
       else{
            TrkID = idglobaltruck;
        }
        //TrkID = idglobaltruck;
       opcion=18;
        $.ajax({ //verificamos que exista el transport o no en la bd
        url: "bd/crudTrk.php",
        type: "POST",
        datatype:"json",
        data:  {costoprom:costoprom, costot:costot, notas:notas,opcion:opcion,TrkID:TrkID}, 

        success: function(data){
            alert("Datos guardados");
            $('#tarifario').modal('hide');
        }

       });
       

       

    })


    //FORMATO ##,###

    $("#tarifario-costoprom").on({

        "focus": function(event) {
          $(event.target).select();
        },
        "keyup": function(event) {
          $(event.target).val(function(index, value) {
            return value.replace(/\D/g, "")
              //.replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
              .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          });
        }
      });

/*
      $("#tarifario-costototal").on({

        "focus": function(event) {
          $(event.target).select();
        },
        "keyup": function(event) {
          $(event.target).val(function(index, value) {
            return value.replace(/\D/g, "")
              //.replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
              .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          });
        }
      });

      */
   

 })

//----------------DESPLEGAR MODAL REPORTE DE MANIOBRAS------------
$(document).ready(function(){
    $("#btnreporte_maniobras").click(function(e){ 
        User = $.trim($('#dropdownMenuLink2').val());
        e.preventDefault();
        $("#repman").trigger("reset");
        var date=new Date();  
        date.setDate(date.getDate());
        var day=String(date.getDate()).padStart(2, '0');          
        var month=("0" + (date.getMonth() + 1)).slice(-2); 
        var year=date.getFullYear();  
        fechadef=year+"-"+month+"-"+day 

        var date2=new Date();  
        date2.setDate(date2.getDate() - 7);
        var day2=String(date2.getDate()).padStart(2, '0');          
        var month2=("0" + (date2.getMonth() + 1)).slice(-2); 
        var year2=date2.getFullYear();  
        fechadef2=year2+"-"+month2+"-"+day2 ;

        $('#fromdate_man').val(fechadef2)
        $('#todate_man').val(fechadef)

        $('#timeselect_man').on('change',function(){
            rango_fechas_maniobras($(this).val(),fechadef2,fechadef)
            $('#reporte_man_PDF').prop('disabled', true);
        });

        
        $('#fromdate_man').on('change',function(){
            $('#todate_man').attr('min', $(this).val());

            if ($(this).val()== $('#todate_man').val()){
                $('#reporte_man_PDF').prop('disabled', false);
            }
            else{
                $('#reporte_man_PDF').prop('disabled', true);
            }

            if($(this).val()>$('#todate_man').val()){
                $('#todate_man').val($(this).val())
            }
        });

        $('#todate_man').on('change',function(){
            if ($(this).val()== $('#fromdate_man').val()){
                $('#reporte_man_PDF').prop('disabled', false);
            }
            else{
                $('#reporte_man_PDF').prop('disabled', true);
            }
        });


       

        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $(".modal-title").text("Reporte Maniobras");		
        $('#reporte_maniobras').modal('show'); 	
        
        //Mostrar PDF a personas que no seas del SS

        
        if(User=="MANOLO PEÑA"){
            document.getElementById('reporte_man_PDF').style.display = 'none';               
        }
        else{
            document.getElementById('reporte_man_PDF').style.display = 'block';          
        }


        //funcion botón limpiar
        $("#limpiar_man").click(function(e){ 
            e.preventDefault();
            $("#repman").trigger("reset");
            $('#fromdate_man').val(fechadef2);
            $('#todate_man').val(fechadef);
            $('#reporte_man_PDF').prop('disabled', true);
    
        })

        //FUNCION BOTON GENERAR EXCEL
        $("#reporte_man_excel").click(function(e){ 
            e.preventDefault();
            fromdate_man=$('#fromdate_man').val();
            todate_man=$('#todate_man').val();
            region_man =$('#region_man').val();
            console.log(region_man);

            //GENERAR REPORTE DE MANOLO 
                        
            if(User=="MANOLO PEÑA"){
                window.open("./bd/ManiobrasExcel_SS.php");   
            }
            else{
                if(region_man=="PUEBLA"){
                window.open("./bd/ManiobrasExcel.php?fechainicio="+fromdate_man+"&fechafin="+todate_man+"&region="+region_man);   
                }
                else {
                    window.open("./bd/ManiobrasExcelGomez.php?fechainicio="+fromdate_man+"&fechafin="+todate_man+"&region="+region_man);   
                }
            }
        })


         //FUNCION BOTON GENERAR PDF
         $("#reporte_man_PDF").click(function(e){ 
            e.preventDefault();
            fromdate_man=$('#fromdate_man').val();
            todate_man=$('#todate_man').val();
            region_man =$('#region_man').val();
            if(region_man=="PUEBLA"){
                window.open("./bd/ManiobrasPDF.php?fechainicio="+fromdate_man+"&fechafin="+todate_man+"&region="+region_man);   
            }
            else{
                window.open("./bd/ManiobrasPDFGomez.php?fechainicio="+fromdate_man+"&fechafin="+todate_man+"&region="+region_man);   
            }
        })

         //boton para mostrar cargar de csv

         //botón para mostrar filtros de reporte 

         $("#btnreport_maniobras").click(function(e){ 
            e.preventDefault();        
            
            
            document.getElementById('limpiar_man').style.display = 'block';
            document.getElementById('reporte_man_excel').style.display = 'block';
            document.getElementById('reporte_man_PDF').style.display = 'block';
            document.getElementById('filtros_maniobras').style.display = 'block';
           // document.getElementById('btn_reportman').style.display = 'block';
            document.getElementById('carga_csv_man').style.display = 'none';   
            document.getElementById('subir_reporteManiobras').style.display = 'none';     

            if(User=="MANOLO PEÑA"){
                document.getElementById('reporte_man_PDF').style.display = 'none'; 
              
            }
        })

        $("#btnsube_maniobras").click(function(e){ 
            e.preventDefault();
            document.getElementById('filtros_maniobras').style.display = 'none';
           // document.getElementById('btn_reportman').style.display = 'none';
           document.getElementById('limpiar_man').style.display = 'none';
           document.getElementById('reporte_man_excel').style.display = 'none';
           document.getElementById('reporte_man_PDF').style.display = 'none';
            document.getElementById('carga_csv_man').style.display = 'block';   
            document.getElementById('subir_reporteManiobras').style.display = 'block';

            if(User=="MANOLO PEÑA"){
                document.getElementById('reporte_man_PDF').style.display = 'none'; 
              
            }
        });

        $("#reporte_SS").click(function(e){ 
            e.preventDefault();      
            window.open("./bd/ManiobrasExcel_SS.php"); 
        
        });



        $("#filecsv_maniobras").change(function(){
            valida_csv_maniobras();
        });


        //boton load csv maniobras

        
        

        
    });

    $('#carga_csv_man').on('click', function(e) {
        e.preventDefault();    
        var file_data = $('#filecsv_maniobras').prop('files')[0];  
        var form_data = new FormData();             
        form_data.append('file', file_data);   
        $('#carga_csv_man').prop('disabled', true); 

        if(User=="MANOLO PEÑA"){
            Ruta='bd/load_csv_maniobras_SS.php';
        }
        else{
            Ruta='bd/load_csv_maniobras.php';
        }
        $.ajax({
            url: Ruta, // <-- point to server-side PHP script 
            dataType: 'text',  // <-- what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(data){
               // datos = JSON.parse(data);
               // cont = datos['cont'];
                //cont = data['cont'];

                alert("Successful file upload" + '\n' +"Modified Trucks: "+ data);
                var $el = $('#filecsv_maniobras');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();  
                $('#carga_csv_man').prop('disabled', true);      
                //tablaTrk.ajax.reload(null,false);     
            },
            error(data){
                alert("Error to load file");
                var $el = $('#filecsv_maniobras');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();  
                $('#carga_csv_man').prop('disabled', true);    
            }

        });
    });

    
})

//cargar maniobras boton editar
$(document).ready(function(){
    $(document).on("click", ".btnEditar", function(){
        /*$("#ManiobraSalida").empty();   
        $("#RespManSalida").empty();   
        $("#ManiobraLlegada").empty();   
        $("#RespManLleg").empty();   */
  
    
    getManiobras(DepReg,ArrReg,Cli,TrkID)
        //bloquear maniobra
   /* $("#ManiobraSalida").change(function () {
        if( $(this).val() =="MP2" ||  $(this).val() == "MG2"){
           // $("#ManiobraLlegada").prop("disabled", true);
           // $("#RespManLleg").prop("disabled", true);
            $("#ManiobraLlegada").val("");   
            $("#RespManLleg").val(""); 
            
        }   
      }); */
    
    });
});
 
//FUNCIONES EN VISTA TRUCKS
 function peso_salida(){
 
    ValOutWgh = 0;
    $("#OutWgh").keyup(function(){
		  ValOutWgh = 1;
	  });
    //peso_sal = $("#OutWgh").val();
    peso_sal=document.getElementById("OutWgh").value;
    cantidad=document.getElementById("QtyDO").value;

   // peso_llehada = $("#InWgh").val();
   // cantidad = $('#QtyDO').val();
   if (peso_sal.includes(",")) {
    pesosalida = peso_sal.replace(/,/g, ''); 
   // console.log(pesosalida)
   }
   else{
    pesosalida = peso_sal;
   }
  //  pesollegada = InWgh.replace(/,/g, ''); 
    /*if (cantidad == '0'){
        $("#OutWgh").attr('disabled', true);
    }else{
        $("#OutWgh").attr('disabled', false);
    }*/
     
    if (cantidad != '0' && pesosalida != '0'){
        promdiosalida = (pesosalida)/(cantidad);
        //console.log(pesosalida)
        promdiosalida = promdiosalida.toFixed(2)
        
    
        promdiosalida = parseFloat(promdiosalida);
        $('#promsalida').val(promdiosalida);
        
    }
    else{
        $('#promsalida').val(0);
        $("#OutWgh").val(0); 
    }

 }

 function peso_llegada(){
 
     ValInWgh = 0;
     $("#InWgh").keyup(function(){
       ValInWgh = 1;
     });
    //peso_sal = $("#OutWgh").val();
    peso_lleg=document.getElementById("InWgh").value;
    cantidad=document.getElementById("QtyDO").value;

   // peso_llehada = $("#InWgh").val();
   // cantidad = $('#QtyDO').val();
   if (peso_lleg.includes(",")) {
    pesosllegada = peso_lleg.replace(/,/g, ''); 
  //  console.log(pesosllegada)
   }
   else{
    pesosllegada = peso_lleg;
   }
  //  pesollegada = InWgh.replace(/,/g, ''); 

    if (cantidad != '0' && pesosllegada != '0'){
        
        promdiollegada = (pesosllegada)/(cantidad);
        //console.log(pesosalida)
        promdiollegada = promdiollegada.toFixed(2)
    
        promdiollegada = parseFloat(promdiollegada);
        $('#promllegada').val(promdiollegada);   

    }
    else{
        $('#promllegada').val(0);  
    
    }

 }
 

// formato miles (#,###) de manera automatica

function separator(numb) {
    var str = numb.toString().split(".");
    str[0] = str[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return str.join(".");
}

 function cost_total(){
    
    costo_total=document.getElementById("tarifario-costototal").value; 
    qty=document.getElementById("QtyDO").value;
    ban = document.getElementById("tarifario-muestras").value;
    costo_total = costo_total.replace(/,/g, ''); 
   

    if(costo_total != 0 || costo_total!="" ){
        if(ban =="TRUE"){
            promedio_total = costo_total / 120;
        }
        else{
        promedio_total = costo_total / qty;
         }
        promedio_total = promedio_total.toFixed(2)

        $('#tarifario-costoprom').val(promedio_total);
        
    }
    else{
        $('#tarifario-costoprom').val(0);
    }

    separator($('#tarifario-costototal').val());

    //mostrar alerta si el costo capturado supera al costo autorizado


 }


 function alerta(){
    costo_total=document.getElementById("tarifario-costototal").value; 
    costo_auto=document.getElementById("tarifario-costoauto").value; 
    costo_total = costo_total.replace(/,/g, ''); 
    costo_auto = costo_auto.replace(/,/g, ''); 


    if((costo_total != 0 || costo_total!="") && costo_auto > 1 ){
            costo_auto= parseFloat(costo_auto);
            costo_total=parseFloat(costo_total);
           // dif=costo_total-costo_auto
            if((costo_auto+10) < costo_total){
                document.getElementById('alertacosto').style.display = 'block';
                //document.getElementById('alertacostotext').innerHTML=' El costo total supera el costo autorizado por $'+dif+'. Agregar comentario'; 
                $('#tarifario-save').prop('disabled', true);

            }

            else{
                document.getElementById('alertacosto').style.display = 'none';
                $('#tarifario-save').prop('disabled', false);
                
            }
    }


 }

 function boxcomentario(){
    boxcoment=document.getElementById("tarifario-comentarios").value; 

    
    if(boxcoment !== '' && boxcoment !==' '){
        $('#tarifario-save').prop('disabled', false);

    }
    else{
        $('#tarifario-save').prop('disabled', true);
    }
    

 }


 function guarda(){
    //alerta(activado)
    if (activado==1){
        opcion = 19;
        LotsAssc = $.trim($('#AsscLots').val().toUpperCase());
        CrgQty = $.trim($('#QtyDO').val());
        PWgh = $.trim($('#PWgh').val()).replace(/,/g, "");
        if(bandera !=""){
        TrkID = $.trim($('#exampleModalLabel').val());
        }
        $.ajax({
            url: "bd/crudTrk.php",
            type: "POST",
            datatype:"json",
            data:  {TrkID:TrkID, LotsAssc:LotsAssc , CrgQty:CrgQty, PWgh,PWgh, opcion:opcion},  
            success: function(data) {
                $('#modalCRUD').modal('hide');
                $('#exampleModalLabel').val("");
                activado = 0;
                tablaTrk.ajax.reload(null,false);
            }  
        })

    }
    else{
        $('#modalCRUD').modal('hide');
    }
}


function cuenta_lotes(){
    array_lotasoc=$("#AsscLots").val().split('|');
    return array_lotasoc.length;

   // console.log(array_lotasoc.length);
}


function LoadReq(){
    $("#RqstID").empty();
    //lotes_asoc=;
  //  array_lotasoc=$("#AsscLots").val().split('|');

   // console.log(array_lotasoc.length);
    var opts = "";
    opcion = 15;
    //RqstID = $.trim($('#RqstID').val());
    DO = $.trim($('#DOrd').val());
    lot_intrk=cuenta_lotes();
    //alert (DO);
    $.ajax({
        url: "bd/crudTrk.php",
        type: "POST",
        datatype:"json",
        data:  { DO:DO,lot_intrk:lot_intrk, opcion:opcion },    
        success: function(data){
            opts = JSON.parse(data);
            $('#RqstID').append('<option value=0>- Select -</option disabled>');
            for (var i = 0; i< opts.length; i++){
               
                $('#RqstID').append('<option value="' + opts[i].ReqID + '">' + opts[i].ReqID+'-('+ opts[i].Restante +')'+ '</option>');
            }        
        },
        error: function(data) {
            alert('error');
        }
    });

}

function LoadReq_buton(cantidad){
    $("#RqstID").empty();
    var opts = "";
    opcion = 15;
    //RqstID = $.trim($('#RqstID').val());
    DO = $.trim($('#DOrd').val());
    setTimeout(50000);
    lot_intrk=cantidad;
    //alert (DO);
    $.ajax({
        url: "bd/crudTrk.php",
        type: "POST",
        datatype:"json",
        data:  { DO:DO,lot_intrk:lot_intrk, opcion:opcion },    
        success: function(data){
            opts = JSON.parse(data);
            $('#RqstID').append('<option value=0>- Select -</option disabled>');
            for (var i = 0; i< opts.length; i++){
               
                $('#RqstID').append('<option value="' + opts[i].ReqID + '">' + opts[i].ReqID+'-('+ opts[i].Restante +')'+ '</option>');
            }        
        },
        error: function(data) {
            alert('error');
        }
    });

}

function liberatrk(TrkID){
    estado = $("#Status").val();
    if(estado == "Programmed"){
            $("#AsscLots").val("");
            $("#QtyDO").val("0");
            $("#PWgh").val("0");

            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",
                data:  { TrkID:TrkID, opcion:20 },    
                success: function(data){
                    
                },
                error: function(data) {
                    alert('error');
                }
            });
    }
    else{
        alert("status must be progammed")
    }


}



function transportes_reg_salida(){

    regionname = $("#regsalida_report").val() 

    $.ajax({
        url: "bd/crudTrk.php",
        type: "POST",
        datatype:"json",
        data:  { regionname:regionname, opcion:23 },    
        success: function(data){

            $("#transport_report").empty();
            $('#transport_report').append($('<option>').val("").text("Choose..."));
            opts = JSON.parse(data);
            for (var i = 0; i< opts.length; i++){ 
                $('#transport_report').append($('<option>').val(opts[i].TptID).text(opts[i].BnName));   
            
            }
            
        },
        error: function(data) {
            alert('error');
        }
    });


}

//bajar la funcion format date para que sea global 
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}



function transportes_all(){

   
    $.ajax({
        url: "bd/crudTrk.php",
        type: "POST",
        datatype:"json",
        data:  { opcion:24 },    
        success: function(data){

            $("#transport_report").empty();
            opts = JSON.parse(data);
            $('#transport_report').append($('<option>').val("").text("Choose..."));
            for (var i = 0; i< opts.length; i++){ 

                $('#transport_report').append($('<option>').val(opts[i].TptID).text(opts[i].BnName));   
            
            }
            
        },
        error: function(data) {
            alert('error');
        }
    });


}


function rango_fechas(timeSelect){
        //timeSelect = $("#timeSelect_report").val();
        if(timeSelect){
            timeSelect = parseInt(timeSelect);
            if(timeSelect != 0){
                var today = new Date();
                let time = today.getTime();
                $('#fromdate_report').prop('disabled', false);
                $('#todate_report').prop('disabled', false);
                
                //today = today.toLocaleTimeString('es-MX');
                let dateEnMilisegundos = 1000 * 60* 60* 24* timeSelect;
                let suma = time - dateEnMilisegundos;
                let fechainicial = new Date(suma);
                //console.log(today);
                datetoday =  formatDate(today);
                datetosearch = formatDate(fechainicial);
                $('#fromdate_report').val(datetosearch);
                $('#todate_report').val(datetoday);
                //$('#buscafiltro').attr('disabled',false);
            
                // $('#generar').attr('disabled', false);

            }
            else{
                var date = new Date();
                var primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
                var ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);          
                primerDia = formatDate(primerDia);
                ultimoDia = formatDate(ultimoDia);
                $('#fromdate_report').val(primerDia);
                $('#todate_report').val(ultimoDia);
            }
       
        }else{
            $('#fromdate_report').val("");
            $('#todate_report').val("");
            $('#fromdate_report').prop('disabled', true);
            $('#todate_report').prop('disabled', true);
           // $('#buscafiltro').attr('disabled',true);
           // $('#generar').attr('disabled', true);
        }
}


function valida_csv_invoice(){
    
    var filename = $("#filecsv_invoice").val();
  //  console.log(filename);
    //si es null muestra un mensaje de error
    if(filename == null)
        alert('You have not selected a file');
        
    else{// si se eligio un archivo correcto obtiene la extension para validarla
        var extension = filename.replace(/^.*\./, '');               
        
        //$('#update').prop('disabled', false);
        if (extension == filename)
            extension = '';
        else{
            extension = extension.toLowerCase();
          
            //aqui puedes incluir todas las extensiones que quieres permitir
            if(extension == 'CSV' || extension == 'csv' || extension == 'Csv' ){
                $('#btnloadfile_invoice').prop('disabled', false);
            }
            else{
                alert("The selected file is not a CSV file");
                var $el = $('#filecsv_invoice');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();  
                $('#btnloadfile_invoice').prop('disabled', true);    
               // $('#load').prop('disabled', true);
                
            }
        }
    }


}

function rango_fechas_peso(timeS){

    time_Select = parseInt(timeS);
    if(time_Select != 0){
        var today = new Date();
        let time = today.getTime();

        let dateEnMilisegundos = 1000 * 60* 60* 24* time_Select;
        let suma = time - dateEnMilisegundos;
        let fechainicial = new Date(suma);
        //console.log(today);
        datetoday =  formatDate(today);
        datetosearch = formatDate(fechainicial);
        $('#fromdate_pesos').val(datetosearch);
        $('#todate_pesos').val(datetoday);
    }
    else{
        var date = new Date();
        var primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
        var ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);  
        primerDia = formatDate(primerDia);
        ultimoDia = formatDate(ultimoDia);
        $('#fromdate_pesos').val(primerDia);
        $('#todate_pesos').val(ultimoDia);
    }

    
}
function upfileticket(){
    
    var filename = $("#fileticket").val();
  //  console.log(filename);
    //si es null muestra un mensaje de error
    if(filename == null)
        alert('No ha seleccionado una imagen');
        
    else{// si se eligio un archivo correcto obtiene la extension para validarla
        var extension = filename.replace(/^.*\./, '');               
        
        //$('#update').prop('disabled', false);
        if (extension == filename)
            extension = '';
        else{
            extension = extension.toLowerCase();
          
            //aqui puedes incluir todas las extensiones que quieres permitir
            if(extension == 'pdf' || extension == 'JPG' || extension == 'PNG'|| extension == 'jpg' || extension == 'png' ){
                $('#Ticketload').prop('disabled', false);
            }
            else{
                alert("extencion no valida");
                $('#Ticketload').prop('disabled', true);
                
            }
        }
    }


}

function infolote(trk){
    opcion = 26;
    var htmlData = "<br>";
        
    TrkID =trk; //capturo el ID del camion asociado
        $.ajax({
          url: "bd/crudTrk.php",
          type: "POST",
          datatype:"json",    
          data:  {TrkID:TrkID, opcion:opcion},    
          success: function(data) {
            opts = JSON.parse(data);
            if (opts.length >0){
                for (var i = 0; i< opts.length; i++){
                    htmlData =   htmlData.concat('<ul><li>'+opts[i].Lot+'-'+opts[i].Qty+' bc'+'</li></ul>');
                   
                } 
                $('#Inf').html(htmlData);
            }

            else{
                $('#Inf').html("No assigned lots");
            }
        }
    });
    $(".modal-header").css("background-color", "#17562c");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Info Lots");		
    $('#modalInfLots').modal('show');

   
}



function showcontainer(){
    opcion = 8;
    DO = $('#DOrd').val(); 
    tipo=$('#TypDO').val();    
    cantidadpacas = $('#QtyDO').val();
    if(cantidadpacas > 0 && tipo =="EXP"){    
        $('#addcont').prop('disabled', true);
        $("#container_id").empty();    
        $("#container_id").append($('<option>').val(0).text("Choose..."));
            $.ajax({
            url: "bd/crudExport.php",
            type: "POST",
            datatype:"json",    
            data:  {DO:DO, opcion:opcion},    
            success: function(data) {
                opts = JSON.parse(data);          
                    for (var i = 0; i< opts.length; i++){                 
                        $('#container_id').append('<option value="' + opts[i].ExpID + '">' + opts[i].Ctr + '</option>');
                    } 
            }
        });
        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $(".modal-title").text("Add Lot in Container");		
        $('#containers').modal('show');
    }
    else{
        mensaje1 ="";
        if(cantidadpacas==0)
            mensaje1 = mensaje1.concat("No lot assigned " + '\n');
        
        if(tipo !="EXP")
            mensaje1 =mensaje1.concat("DO is not EXP ");
        
    
        alert(mensaje1);
      
    }

   
}


function ligarcontainer(Lots,DO,TrkID,ExpID){

    opcion = 9;
    $.ajax({
        url: "bd/crudExport.php",
        type: "POST",
        datatype:"json",    
        data:  {DO:DO,Lots:Lots,TrkID:TrkID,ExpID:ExpID,opcion:opcion},    
        success: function(data) {
            opts = JSON.parse(data);  
            if(opts=='Ok'){
                alert("Lot assigment");
            }
            else{
                alert("Error al asignar lote");
            }

            
            $('#containers').modal('hide');	
            $(".modal-title").text("Edit Truck " + TrkID);
      }

    });

}


function valida_container(TrkID){
    opcion = 10;
    $.ajax({
        url: "bd/crudExport.php",
        type: "POST",
        datatype:"json",    
        data:  {TrkID:TrkID,opcion:opcion},    
        success: function(data) {
            opts = JSON.parse(data);  
            console.log(opts)
            if(opts==0){
                alert("EL truck aun no se asigna a un container");
            }           
          
      }

    });


}


function valida_csv_maniobras(){
    
    var filename = $("#filecsv_maniobras").val();
  //  console.log(filename);
    //si es null muestra un mensaje de error
    if(filename == null)
        alert('You have not selected a file');
        
    else{// si se eligio un archivo correcto obtiene la extension para validarla
        var extension = filename.replace(/^.*\./, '');               
        
        //$('#update').prop('disabled', false);
        if (extension == filename)
            extension = '';
        else{
            extension = extension.toLowerCase();
          
            //aqui puedes incluir todas las extensiones que quieres permitir
            if(extension == 'CSV' || extension == 'csv' || extension == 'Csv' ){
                $('#carga_csv_man').prop('disabled', false);
            }
            else{
                alert("The selected file is not a CSV file");
                var $el = $('#filecsv_maniobras');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();  
                $('#carga_csv_man').prop('disabled', true);    
               // $('#load').prop('disabled', true);
                
            }
        }
    }


}


function rango_fechas_maniobras(timeSelect,fechadef2,fechadef){
    //timeSelect = $("#timeSelect_report").val();
    if(timeSelect){
        timeSelect = parseInt(timeSelect);
        if(timeSelect != 0){
            var today = new Date();
            let time = today.getTime();
            $('#fromdate_man').prop('disabled', false);
            $('#todate_man').prop('disabled', false);
            
            //today = today.toLocaleTimeString('es-MX');
            let dateEnMilisegundos = 1000 * 60* 60* 24* timeSelect;
            let suma = time - dateEnMilisegundos;
            let fechainicial = new Date(suma);
            //console.log(today);
            datetoday =  formatDate(today);
            datetosearch = formatDate(fechainicial);
            $('#fromdate_man').val(datetosearch);
            $('#todate_man').val(datetoday);
            //$('#buscafiltro').attr('disabled',false);

            // $('#generar').attr('disabled', false);
        }
        else{
            var date = new Date();
            var primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
            var ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);      
            primerDia = formatDate(primerDia);
            ultimoDia = formatDate(ultimoDia);
            $('#fromdate_man').val(primerDia);
            $('#todate_man').val(ultimoDia);
        }
    
    }else{
        $('#fromdate_man').val(fechadef2);
        $('#todate_man').val(fechadef);
       // $('#fromdate_report').prop('disabled', true);
       // $('#todate_report').prop('disabled', true);
       // $('#buscafiltro').attr('disabled',true);
       // $('#generar').attr('disabled', true);
    }
}




function maniobras(regionsalida,regionllegada,cliente,TrkID){
      
    $('#ManiobraLlegada').prop('disabled', false);    
    $('#RespManLleg').prop('disabled', false);    
    $('#ManiobraSalida').prop('disabled', false);    
    $('#RespManSalida').prop('disabled', false);
    $('#NotaLlegada').prop('disabled', false); 
    $('#NotaSalida').prop('disabled', false);     

    switch (regionsalida) {
        case "PUEBLA":
            responsables_maniobras_puebla_salida();
            if(regionllegada == "CLIENTE"){

                cliente_maniobra(cliente).done(function(objetoRetornado) {
                    datos=JSON.parse(objetoRetornado); // Guardar el resultado en la variable global
                    if(datos[0].State=="PUEBLA" && datos[0].Directo==1){    
                        responsables_maniobras_puebla_llegada();                    
                        //console.log("Caso1");            
                        $("#ManiobraSalida").append($('<option>').val("MP1").text("MP1"));    
                        $("#ManiobraSalida").append($('<option>').val("MP2").text("MP2"));    
                        $("#ManiobraSalida").append($('<option>').val('').text("N/A"));    
    
                       
                        $("#ManiobraLlegada").append($('<option>').val("MP3").text("MP3"));    
                        $("#ManiobraLlegada").append($('<option>').val('').text("N/A"));

                        $("select#ManiobraSalida").val("MP2");
                        $("select#ManiobraLlegada").val("");
    
                    }
    
                    else if(datos[0].State=="PUEBLA" && datos[0].Directo==0){
                        responsables_maniobras_puebla_llegada();  
                      
                      //  console.log("Caso2");
                       
                        $("#ManiobraSalida").append($('<option>').val("MP1").text("MP1"));    
                        $("#ManiobraSalida").append($('<option>').val("MP2").text("MP2"));    
                        $("#ManiobraSalida").append($('<option>').val('').text("N/A"));   
    
                       
                        $("#ManiobraLlegada").append($('<option>').val("MP3").text("MP3"));    
                        $("#ManiobraLlegada").append($('<option>').val('').text("N/A"));

                        $("select#ManiobraSalida").val("MP1");
                        $("select#ManiobraLlegada").val("");
    
                    }
    
                    else if(datos[0].State!="PUEBLA" && datos[0].Directo==0){
                        
                        console.log("Caso3");
                        //llenar maniobras de salida
                        //$("#ManiobraSalida").empty();
                        $("#ManiobraSalida").append($('<option>').val("MP1").text("MP1"));    
                        $("#ManiobraSalida").append($('<option>').val('').text("N/A"));
    
                        //Se bloquea maniobras de llegada
                        //$("#ManiobraLlegada").empty();
                     
                        $('#ManiobraLlegada').prop('disabled', true);    
                        $('#RespManLleg').prop('disabled', true);  
                        $('#NotaLlegada').prop('disabled', true); 
                       
                        
                        $("select#ManiobraSalida").val("MP1");
                       //$("ManiobraLlegada#Unit").val("");
                        
                 
                    }

                   
                 })

             
               
            } 
            else if(regionllegada == "GOMEZ"){
                responsables_maniobras_gomez_llegada()
                $("#ManiobraSalida").append($('<option>').val("MP1").text("MP1"));   
                $("#ManiobraSalida").append($('<option>').val('').text("N/A")); 
                $("#ManiobraLlegada").append($('<option>').val("MG1").text("MG1"));    
                $("#ManiobraLlegada").append($('<option>').val('').text("N/A"));


                $("select#ManiobraSalida").val("MP1");
                $("select#ManiobraLlegada").val("MG1");
            }  

            else {
                $("#ManiobraSalida").append($('<option>').val("MP1").text("MP1"));   
                $("#ManiobraSalida").append($('<option>').val('').text("N/A")); 
                $('#ManiobraLlegada').prop('disabled', true);    
                $('#RespManLleg').prop('disabled', true); 
                $('#NotaLlegada').prop('disabled', true); 
 
                
                $("select#ManiobraSalida").val("MP1");
                $("select#ManiobraLlegada").val("");
            }

        break;

        case "GOMEZ":
            responsables_maniobras_gomez_salida()
            if(regionllegada == "CLIENTE"){

                cliente_maniobra(cliente).done(function(objetoRetornado) {
                    datos=JSON.parse(objetoRetornado); 
                    // Caso llegada a cliente directo en la ciudad de puebla
                    if(datos[0].State=="PUEBLA" && datos[0].Directo==1){    
                        responsables_maniobras_puebla_llegada();                  
                        console.log("Caso1");            
                        $("#ManiobraSalida").append($('<option>').val("MG1").text("MG1"));       
                        $("#ManiobraSalida").append($('<option>').val('').text("N/A"));    
    
                       
                        $("#ManiobraLlegada").append($('<option>').val("MP3").text("MP3"));    
                        $("#ManiobraLlegada").append($('<option>').val('').text("N/A"));

                        $("select#ManiobraSalida").val("MG1");
                        $("select#ManiobraLlegada").val("MP3");
    
                    }
                    else if(datos[0].State!="PUEBLA" && datos[0].Directo==0){
                        responsables_maniobras_puebla_llegada();
                        
                        //llenar maniobras de salida
                        //$("#ManiobraSalida").empty();
                        $("#ManiobraSalida").append($('<option>').val("MG1").text("MG1"));       
                        $("#ManiobraSalida").append($('<option>').val('').text("N/A"));    
          
                        $("#ManiobraLlegada").append($('<option>').val('').text("N/A"));

                        $("select#ManiobraSalida").val("MG1");
                        $("select#ManiobraLlegada").val("");
                        //Se bloquea maniobras de llegada
                        //$("#ManiobraLlegada").empty();
                     
                        //$('#ManiobraLlegada').prop('disabled', true);    
                        //$('#RespManLleg').prop('disabled', true);    
                        
                 
                    }

                    else{

                        $("#ManiobraSalida").append($('<option>').val("MG1").text("MG1"));       
                        $("#ManiobraSalida").append($('<option>').val('').text("N/A"));   
                        $("select#ManiobraSalida").val("MG1");
                        
                        //$("#ManiobraLlegada").empty();
                     
                        $('#ManiobraLlegada').prop('disabled', true);    
                        $('#RespManLleg').prop('disabled', true);    
                        $('#NotaLlegada').prop('disabled', true); 
                      
                    }

                   
                 });

            }

            else if(regionllegada=="PUEBLA"){
                responsables_maniobras_puebla_llegada();
                $("#ManiobraSalida").append($('<option>').val("MG1").text("MG1"));       
                $("#ManiobraSalida").append($('<option>').val('').text("N/A"));
                $("#ManiobraLlegada").append($('<option>').val("MP1").text("MP1"));    
                $("#ManiobraLlegada").append($('<option>').val('').text("N/A"));
                $("select#ManiobraSalida").val("MG1");
                $("select#ManiobraLlegada").val("MP1");

            }
            
            // default otro destino saliendo de gomez
            else{

                $("#ManiobraSalida").append($('<option>').val("MG1").text("MG1"));       
                $("#ManiobraSalida").append($('<option>').val('').text("N/A"));   
                $("select#ManiobraSalida").val("MG1");
                
                //$("#ManiobraLlegada").empty();
             
                $('#ManiobraLlegada').prop('disabled', true);    
                $('#RespManLleg').prop('disabled', true);    
                $('#NotaLlegada').prop('disabled', true); 
                    
          }

        break;

        case "LAGUNA":
            responsables_maniobras_gomez_salida()
            if(regionllegada == "CLIENTE"){

                cliente_maniobra(cliente).done(function(objetoRetornado) {
                    datos=JSON.parse(objetoRetornado); 
                    // Caso llegada a cliente directo en la ciudad de puebla
                    if(datos[0].State=="PUEBLA" && datos[0].Directo==1){    
                        responsables_maniobras_puebla_llegada();                  
                        console.log("Caso1");            
                        $("#ManiobraSalida").append($('<option>').val("MG3").text("MG3"));       
                        $("#ManiobraSalida").append($('<option>').val('').text("N/A"));                         
                        $("#ManiobraLlegada").append($('<option>').val("MP3").text("MP3"));    
                        $("#ManiobraLlegada").append($('<option>').val('').text("N/A"));
                        $("select#ManiobraSalida").val("");
                        $("select#ManiobraLlegada").val("MP3");
    
                    }

                    else{

                        $("#ManiobraSalida").append($('<option>').val("MG1").text("MG1"));       
                        $("#ManiobraSalida").append($('<option>').val("MG3").text("MG3"));
                        $("#ManiobraSalida").append($('<option>').val("MG4 3ero").text("MG4 3ero")); 
                        $("#ManiobraSalida").append($('<option>').val("MG5 3ero").text("MG5 3ero")); 
                        $("#ManiobraSalida").append($('<option>').val("MG6 3ero").text("MG6 3ero"));   
                        $("#ManiobraSalida").append($('<option>').val('').text("N/A")); 
                        $('#ManiobraLlegada').prop('disabled', true);    
                        $('#RespManLleg').prop('disabled', true);    
                        $('#NotaLlegada').prop('disabled', true); 
                        $("select#ManiobraSalida").val("");
                        $("select#ManiobraLlegada").val("");

                    }

                   
                 });

            }

            else if(regionllegada == "GOMEZ"){
                responsables_maniobras_gomez_llegada()
                $("#ManiobraSalida").append($('<option>').val("MG2").text("MG2"));       
                $("#ManiobraSalida").append($('<option>').val("MG3").text("MG3"));    
                $("#ManiobraSalida").append($('<option>').val("MG4 3ero").text("MG4 3ero")); 
                $("#ManiobraSalida").append($('<option>').val("MG5 3ero").text("MG5 3ero"));    
                $("#ManiobraSalida").append($('<option>').val("MG6 3ero").text("MG6 3ero")); 
                $("#ManiobraSalida").append($('<option>').val('').text("N/A"));
                $("#ManiobraLlegada").append($('<option>').val("MG1").text("MG1"));    
                $("#ManiobraLlegada").append($('<option>').val('').text("N/A"));
                $("select#ManiobraSalida").val("");
                $("select#ManiobraLlegada").val("MG1");
            }

            else if(regionllegada == "PUEBLA"){
                responsables_maniobras_puebla_llegada()
                $("#ManiobraSalida").append($('<option>').val("MG3").text("MG3"));       
                $("#ManiobraSalida").append($('<option>').val('').text("N/A")); 
                $("#ManiobraLlegada").append($('<option>').val("MP1").text("MP1"));    
                $("#ManiobraLlegada").append($('<option>').val('').text("N/A"));
                $("select#ManiobraSalida").val("MG3");
                $("select#ManiobraLlegada").val("MP1");

            }

            else{

                $("#ManiobraSalida").append($('<option>').val("MG1").text("MG1"));       
                $("#ManiobraSalida").append($('<option>').val('').text("N/A")); 
                $('#ManiobraLlegada').prop('disabled', true);    
                $('#RespManLleg').prop('disabled', true);   
                $('#NotaLlegada').prop('disabled', true);
                $("select#ManiobraSalida").val("");
              

            }




        break;

        default:
            if(regionllegada == "CLIENTE"){

                cliente_maniobra(cliente).done(function(objetoRetornado) {
                    datos=JSON.parse(objetoRetornado); 
                    // Caso llegada a cliente directo en la ciudad de puebla
                    if(datos[0].State=="PUEBLA" && datos[0].Directo==1){    
                        responsables_maniobras_puebla_llegada();                  
                        console.log("Caso1");            
 
                        $('#ManiobraSalida').prop('disabled', true);    
                        $('#RespManSalida').prop('disabled', true);  
                        $('#NotaSalida').prop('disabled', true);   

                        $("#ManiobraLlegada").append($('<option>').val("MP3").text("MP3"));    
                        $("#ManiobraLlegada").append($('<option>').val('').text("N/A"));

                       // $("select#ManiobraSalida").val("");
                        $("select#ManiobraLlegada").val("MP3");
    
                    }

                    else{

                        $('#ManiobraSalida').prop('disabled', true);    
                        $('#RespManSalida').prop('disabled', true);    
                        $('#ManiobraLlegada').prop('disabled', true);    
                        $('#RespManLleg').prop('disabled', true);  
                        $('#NotaLlegada').prop('disabled', true); 
                        $('#NotaSalida').prop('disabled', true); 

                        //$("#ManiobraLlegada").append($('<option>').val("MP3").text("MP3"));    
                        //$("#ManiobraLlegada").append($('<option>').val('').text("N/A"));

                        //$("select#ManiobraSalida").val("");
                        $("select#ManiobraLlegada").val("");
    
                    }

                   
                 });

            }
            else if(regionllegada == "GOMEZ"){
                responsables_maniobras_gomez_llegada()
                $("#ManiobraLlegada").append($('<option>').val("MG1").text("MG1"));    
                $("#ManiobraLlegada").append($('<option>').val('').text("N/A"));
                $('#ManiobraSalida').prop('disabled', true);    
                $('#RespManSalida').prop('disabled', true); 
                $('#NotaSalida').prop('disabled', true); 
                
                $("select#ManiobraSalida").val("");
                $("select#ManiobraLlegada").val("MG1");


            }

            else if(regionllegada == "PUEBLA"){    
                responsables_maniobras_puebla_llegada()                            
                $("#ManiobraLlegada").append($('<option>').val("MP1").text("MP1"));    
                $("#ManiobraLlegada").append($('<option>').val('').text("N/A"));
                $('#ManiobraSalida').prop('disabled', true);    
                $('#RespManSalida').prop('disabled', true);  
                $('#NotaSalida').prop('disabled', true);   
                $("select#ManiobraSalida").val("");
                $("select#ManiobraLlegada").val("MP1");



            }

            else{

                $('#ManiobraSalida').prop('disabled', true);    
                $('#RespManSalida').prop('disabled', true);                
                $('#ManiobraLlegada').prop('disabled', true);    
                $('#RespManLleg').prop('disabled', true); 
                $('#NotaLlegada').prop('disabled', true); 
                $('#NotaSalida').prop('disabled', true); 
            }



    }
    /*if(TrkID!="" && TrkID !=0){
        $("#ManiobraSalida").empty(); 
        $("#ManiobraLlegada").empty(); 
        getManiobras(TrkID);
    }*/
}

function responsables_maniobras_puebla_salida(){
    $("#RespManSalida").empty();    
    $("#RespManSalida").append($('<option>').val("").text("Choose..."));
    $("#RespManSalida").append($('<option>').val("IVAN").text("IVAN"));
    $("#RespManSalida").append($('<option>').val("JUAN C.").text("JUAN CARLOS"));
    $("#RespManSalida").append($('<option>').val("LUIS").text("LUIS"));
    $("#RespManSalida").append($('<option>').val("RUFINO").text("RUFINO"));
}


function responsables_maniobras_puebla_llegada(){
    $("#RespManLleg").empty(); 
    $("#RespManLleg").append($('<option>').val("").text("Choose..."));
    $("#RespManLleg").append($('<option>').val("IVAN").text("IVAN"));
    $("#RespManLleg").append($('<option>').val("JUAN C.").text("JUAN CARLOS"));
    $("#RespManLleg").append($('<option>').val("LUIS").text("LUIS")); 
    $("#RespManLleg").append($('<option>').val("RUFINO").text("RUFINO"));
}


function responsables_maniobras_gomez_llegada(){
    $("#RespManLleg").empty();    
    $("#RespManLleg").append($('<option>').val("").text("Choose..."));
    $("#RespManLleg").append($('<option>').val("ROBERTO").text("ROBERTO"));

}


function responsables_maniobras_gomez_salida(){
    $("#RespManSalida").empty();    
    $("#RespManSalida").append($('<option>').val("").text("Choose..."));
    $("#RespManSalida").append($('<option>').val("ROBERTO").text("ROBERTO"));

}

function cliente_maniobra(cliente){
 
    opcion = 27;
    return $.ajax({
        url: "bd/crudTrk.php",
        type: "POST",
        datatype:"json",    
        data:  {opcion:opcion,cliente:cliente},    
          

    });

}

function clear_maniobras(){
    $("#ManiobraSalida").empty();   
    $("#RespManSalida").empty();   
    $("#ManiobraLlegada").empty();   
    $("#RespManLleg").empty();   
}

function getManiobras(DepReg,ArrReg,Cli,TrkID){
//maniobras(DepReg,ArrReg,Cli,TrkID)
    opcion = 29;
     $.ajax({
        url: "bd/crudTrk.php",
        type: "POST",
        datatype:"json",    
        data:  {opcion:opcion,TrkID:TrkID},    
        success: function(data) {
            opts = JSON.parse(data);  
            //datos maniobras salida
            $("#ManiobraSalida").val(opts[0]['TipoManDep']); 
            $("#RespManSalida").val(opts[0]['RespManDep']);
            $("#NotaSalida").val(opts[0]['NotaSalDep']);
            $("#PQsalida").val(opts[0]['PQDep']);               
            
            //datos maniobras llegada
            $("#ManiobraLlegada").val(opts[0]['TipoManArr']);
            $("#RespManLleg").val(opts[0]['RespManArr']);
            $("#NotaLlegada").val(opts[0]['NotaSalArr']);
            $("#PQllegada").val(opts[0]['PQArr']);

            if(opts[0]['PQArr']!=0){

                $('#ManiobraLlegada').prop('disabled', true); 
                $('#RespManLleg').prop('disabled', true);
                $('#NotaLlegada').prop('disabled', true); 
                
            }
            else{
                $('#ManiobraLlegada').prop('disabled', false); 
                $('#RespManLleg').prop('disabled', false); 
                $('#NotaLlegada').prop('disabled', false); 
            }

            if(opts[0]['PQDep']!=0){
                $('#ManiobraSalida').prop('disabled', true); 
                $('#RespManSalida').prop('disabled', true); 
                $('#NotaSalida').prop('disabled', true); 
            }
            else{
                $('#ManiobraSalida').prop('disabled', false); 
                $('#RespManSalida').prop('disabled', false); 
                $('#NotaSalida').prop('disabled', false); 

            }
        }
    });
}
$(document).on("click", ".viewfile_certificado", function(e){
    e.preventDefault(); 
    truckid = parseInt(fila.find('td:eq(0)').text()); //capturo el ID
    window.open("descarga_certificado.php?TrkID="+truckid);
});

function upfile_certificados(){
    var filename = $("#filecertificado").val();
  //  console.log(filename);
    //si es null muestra un mensaje de error
    if(filename === null)
        alert('No ha seleccionado un archivo');  
    else{// si se eligio un archivo correcto obtiene la extension para validarla
        var extension = filename.replace(/^.*\./, '');  
        if (extension == filename)
            extension = '';
        else{
            extension = extension.toLowerCase();
            //aqui puedes incluir todas las extensiones que quieres permitir
            if(extension == 'pdf' ||extension == 'PDF' || extension == 'JPG' || extension == 'PNG'|| extension == 'jpg' || extension == 'png' ){
                $('#load_certificado').prop('disabled', false);
            }
            else{
                alert("extencion no valida");
                $('#load_certificado').prop('disabled', true);
            }
        }
    }
}