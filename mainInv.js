$(document).ready(function() {

$('#tablaInv tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );


var opcion;
opcion = 4;
    
tablaInv = $('#tablaInv').DataTable({
    
    //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Export to Excel',
				className: 'btn btn-success',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			{
				extend:    'pdfHtml5',
				text:      '<i class="fas fa-file-pdf"></i> ',
				titleAttr: 'Export to PDF',
                className: 'btn btn-danger',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			/*{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},*/
		],
    
    initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
    },
    "order": [ 0, 'desc' ],
    "scrollX": true,
    "scrollY": "50vh",
    "scrollCollapse": true,
    "lengthMenu": [100,200,300,500],
    fixedColumns:   {
            left: 0,
            right: 1
        },
    "ajax":{            
        "url": "bd/crudInv.php", 
        "method": 'POST', //usamos el metodo POST
        "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
        "dataSrc":""
    },
    "columns":[
        {"data": "InvID"},
        {"data": "InvDat"},
        {"data": "Inv"},
        {"data": "Contract"},
        {"data": "DO"},
        {"data": "Lot"},
        {"data": "Qty"},
        {"data": "OutReg"},
        //{"data": "InReg"},
        //{"data": "Cli"},
        {"data": "DOCtc"},
        {"data": "Cli"}, //"DOClt"
        {"data" : "PosDat"},
        {"data": "InvWgh"},
        {"data" : "Tare"},
        {"data": "InvWghNet"},
        {"data" : "Price", render: $.fn.dataTable.render.number( ',', '.', 2)},
        {"data" : "PriceUnit"},
        {"data": "InvAmt", render: $.fn.dataTable.render.number( ',', '.', 2)},
        {"data" : "InvCmt"},
        {"data" : "InvSta"},
        {"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button><button class='btn btn-danger btn-sm btnBorrar'><i class='material-icons'>delete</i></button></div></div>"}
    ]
});     

$('.dataTables_length').addClass('bs-select');

var fila; //captura la fila, para editar o eliminar
//submit para el Alta y Actualización
$('#formInv').submit(function(e){
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
    Inv = $.trim($('#Inv').val().toUpperCase());    
    DO = $.trim($('#DO').val());
    CtcInv = $.trim($('#CtcInv').val().toUpperCase());
    Lot = $.trim($('#Lot').val().toUpperCase());
    Qty = $.trim($('#Qty').val());
    InvDat = $.trim($('#InvDat').val());    
    WghGss = $.trim($('#WghGss').val());
    WghNet = $.trim($('#WghNet').val());
    //Tare = $.trim($('#Tare').val());
    Amt = $.trim($('#Amt').val()).replace(/,/g, "");
    Price = $.trim($('#Price').val());
    PriceUnit = $.trim($('#PriceUnit').val());
    PosDat = $.trim($('#PosDat').val());
    InvSts = $.trim($('#Status').val());
    InvCmt = $.trim($('#InvCmt').val().toUpperCase());                            
        $.ajax({
          url: "bd/crudInv.php",
          type: "POST",
          datatype:"json",    
          data:  { InvID:InvID, Inv:Inv, DO:DO, CtcInv:CtcInv, Lot:Lot, Qty:Qty, InvDat:InvDat, WghGss:WghGss, WghNet:WghNet, Amt:Amt, Price:Price, PriceUnit:PriceUnit, PosDat:PosDat, InvSts:InvSts, InvCmt:InvCmt, opcion:opcion},    
          success: function(data) {
            tablaInv.ajax.reload(null, false);
           }
        });			        
    $('#modalCRUD').modal('hide');											     			
});
        
 

//para limpiar los campos antes de dar de Alta una Persona
$("#btnNuevo").click(function(){

    var lastPrice = $.ajax({ //verificamos que exista el transport o no en la bd
        type: "POST",
        url: "./bd/correo-model.php", //hago referencia al archivo correo model.php y le mando el método getLastPrice
        data: {
            m: "getLastPrice" // aquí igual se pordría pasar el parámetro m 
        }
    });

    lastPrice.done(function(d){
        data = JSON.parse(d);
        //console.log(data['Price']);
        $('#Price').val(data['Price']);

    });


    $('#DO').empty();
    $("#TypeDO").hide();
    $("#InfDO").hide();
    opcion = 1; //alta           
    InvID=null;
    $("#formInv").trigger("reset");
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    $(".modal-title").text("New Invoice");
    $('#modalCRUD').modal('show');	    
});

//Editar        
$(document).on("click", ".btnEditar", function(){
    $("#DO").empty();
    $("#TypeDO").hide();
    $("#InfDO").hide();
    opcion = 2;//editar
    fila = $(this).closest("tr");	        
    InvID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID		            
    Inv = fila.find('td:eq(2)').text();
    //DO = parseFloat(fila.find('td:eq(4)').text());
    CtcInv = fila.find('td:eq(3)').text();
    Lot = fila.find('td:eq(5)').text();
    Qty = parseFloat(fila.find('td:eq(6)').text());
    InvDat = fila.find('td:eq(1)').text();
    WghGss = parseFloat(fila.find('td:eq(11)').text());
    Tara = parseFloat(fila.find('td:eq(12)').text());
    WghNet = parseFloat(fila.find('td:eq(13)').text());
    Price = parseFloat(fila.find('td:eq(14)').text());
    PriceUnit = fila.find('td:eq(15)').text();
    Amt = fila.find('td:eq(16)').text();
    PosDat = fila.find('td:eq(10)').text();
    InvSts = fila.find('td:eq(18)').text();
    InvCmt = fila.find('td:eq(17)').text();
    $("#InvID").val(InvID);
    $("#Inv").val(Inv);
    $('#DO').append('<option class="form-control selected">' + fila.find('td:eq(4)').text() + '</option>');
    //$("#DO").val(DO);
    $("#CtcInv").val(CtcInv);
    $("#Lot").val(Lot);
    $("#Qty").val(Qty);
    $("#InvDat").val(InvDat);
    $("#WghGss").val(WghGss);
    $("#Tare").val(Tara);
    $("#WghNet").val(WghNet);
    $("#Amt").val(Amt);
    $("#Price").val(Price);
    $("#PriceUnit").val(PriceUnit);
    $("#PosDat").val(PosDat);
    $("#Status").val(InvSts);
    $("#InvCmt").val(InvCmt);
    $(".modal-header").css("background-color", "#17562c");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Edit Invoice");		
    $('#modalCRUD').modal('show');		   
});

//Borrar
$(document).on("click", ".btnBorrar", function(){
    fila = $(this);           
    InvID = $(this).closest('tr').find('td:eq(0)').text();		
    opcion = 3; //eliminar        
    var respuesta = confirm("¿Está seguro que deseas deshabilitar el registro "+InvID+"?");                
    if (respuesta) {            
        $.ajax({
          url: "bd/crudInv.php",
          type: "POST",
          datatype:"json",    
          data:  {opcion:opcion, InvID:InvID},    
          success: function() {
              tablaInv.ajax.reload(null, false); //tablaInv.row(fila.parents('tr')).remove().draw();                  
           }
        });	
    }
 });
    
/*function Tare(){
    Qty = $.trim($('#Qty').val());
    //WghGss = $.trim($('#WghGss').val());
    //WghNet = $.trim($('#WghNet').val());
    $("#Tare").val(Qty*2);
}*/

$("#Qty").change(function(){
    Qty = $.trim($('#Qty').val());
    WghGss = $.trim($('#WghGss').val());
    $("#Tare").val(Qty*2);
    $("#WghNet").val(WghGss-(Qty*2));
});
    
$("#WghGss").change(function(){
    Price = $.trim($('#Price').val());
    PriceUnit = $.trim($('#PriceUnit').val());
    Tara = $.trim($('#Tare').val());
    WghGss = $.trim($('#WghGss').val());
    $("#WghNet").val(WghGss-Tara);
    WghNet = $.trim($('#WghNet').val());
    
    if (PriceUnit == "CLbN"){
      num = WghNet * Price * 0.022046;
      n = num.toFixed(2);
      $("#Amt").val(n);
    }else if (PriceUnit == "DQtl"){
      num = WghGss * Price / 46.02;
      n = num.toFixed(2);
      $("#Amt").val(n);
    }else if (PriceUnit == "CKg"){
      num = WghNet * Price / 100;
      n = num.toFixed(2);
      $("#Amt").val(n);
    }else if (PriceUnit == "EKg"){
      num = WghNet * Price;
      n = num.toFixed(2);
      $("#Amt").val(n);
    }
});

$("#Tare,#Price,#PriceUnit").on('change keyup',function(){ //change(function(){
    Price = $.trim($('#Price').val());
    PriceUnit = $.trim($('#PriceUnit').val());
    WghGss = $.trim($('#WghGss').val());
    Tara = $.trim($('#Tare').val());
    $("#WghNet").val(WghGss-Tara);
    WghNet = $.trim($('#WghNet').val());
    
    if (PriceUnit == "CLbN"){
      num = WghNet * Price * 0.022046;
      n = num.toFixed(2);
      $("#Amt").val(n);
    }else if (PriceUnit == "DQtl"){
      num = WghGss * Price / 46.02;
      n = num.toFixed(2);
      $("#Amt").val(n);
    }else if (PriceUnit == "CKg"){
      num = WghNet * Price / 100;
      n = num.toFixed(2);
      $("#Amt").val(n);
    }else if (PriceUnit == "EKg"){
      num = WghNet * Price;
      n = num.toFixed(2);
      $("#Amt").val(n);
    };
    
});
     
});

$(document).ready(function() {
    //buscar DO asociadas al lote
    function FindDO(){
        Lot = $.trim($('#Lot').val());
        opcion = 6;
        $.ajax({
            url: "bd/crudInv.php",
            type: "POST",
            datatype:"json",
            data:  {Lot:Lot ,opcion:opcion},    
            success: function(data){
                opts = JSON.parse(data);
                $('#DO').append('<option value="0">- Select -</option>');
                for (var i = 0; i< opts.length; i++){
                    $('#DO').append('<option value="' + opts[i].DOrd + '">' + opts[i].DOrd + '</option>');
                }
            },
            error: function(data) {
                alert('error');
            }
        });
    };
    
    //$("#btnNuevo").click(function(){
        //$('#DO').empty();
        $("#Lot").on("change", function(){
            $('#DO').empty();
            FindDO();
        });
   // });
    
    $(document).on("click", ".btnEditar", function(){
        //$('#DO').empty();
        FindDO();
    });
    
});
