$(document).ready(function() {

$('#tablaExports tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );


var opcion;
opcion = 4;
    
tablaExp = $('#tablaExports').DataTable({
    
    //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Export to Excel',
				className: 'btn btn-success',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			{
				extend:    'pdfHtml5',
				text:      '<i class="fas fa-file-pdf"></i> ',
				titleAttr: 'Export to PDF',
                className: 'btn btn-danger',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			/*{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},*/
		],
    
    initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
    },
    "order": [ 0, 'desc' ],
    "scrollX": true,
    "scrollY": "50vh",
    "scrollCollapse": true,
    "lengthMenu": [100,200,300,500],
    fixedColumns:   {
            left: 0,
            right: 1
        },
    "ajax":{            
        "url": "bd/crudExport.php", 
        "method": 'POST', //usamos el metodo POST
        "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
        "dataSrc":""
    },
    "columns":[
        {"data": "ExpID"},
        {"data": "Port"},
        {"data": "ShpCo"},
        {"data": "Bkg"},
        {"data": "Ctr"},
        {"data": "Seal"},
        {"data": "DO"},
        {"data": "Lot"},
        //{"data": "TrkID"}, <button class='btn btn-danger btn-sm btnBorrar'><i class='material-icons'>delete</i></button>
        {"data": "Qty"},
        {"data" : "SchDate"},
        {"data": "InDate"},
        {"data": "InTime"},
        {"data" : "DepDate"},
        {"data" : "OutTime"},
        {"data": "GinName"},
        {"data" : "DepReg"},
        {"data": "DOCtc"},
        {"data" : "Ctl"},
        {"data" : "Status"},
        {"data" : "TName"},
        {"data" : "CAAT"},
        {"data" : "WBill"},
        {"data" : "TrkPlt"},
        {"data" : "TraPlt"},
        {"data" : "DrvName"},
        {"data" : "LiqWgh" , render: $.fn.dataTable.render.number( ',')},
        {"data" : "DepWgh" , render: $.fn.dataTable.render.number( ',')},
        {"data" : "InWgh" , render: $.fn.dataTable.render.number( ',')},
        {"data" : "InvNo"},
        {"data" : "Cmt"},
        {"data" : "Statustrk"},
        {"data" : "TrkID"},
        {"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button></div></div>"}
    ]
});     

$('.dataTables_length').addClass('bs-select');

var fila; //captura la fila, para editar o eliminar
//submit para el Alta y Actualización
$('#formExp').submit(function(e){
    var comboLots = document.getElementById("Lots");
    var selectedLots = comboLots.options[comboLots.selectedIndex].text;
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
    Port = $.trim($('#Port').val().toUpperCase());    
    ShpCo = $.trim($('#ShpCo').val().toUpperCase());
    Bkg = $.trim($('#Bkg').val().toUpperCase());
    Ctr = $.trim($('#Ctr').val().toUpperCase());
    Seal = $.trim($('#Seal').val().toUpperCase());
    DO = $.trim($('#DO').val());    
    LotID = $.trim($('#Lots').val());
    if(LotID != 0){
        Lots = LotID;//$.trim($('#Lots').val());
    }   
    else{
        Lots = 0;
    }
    SchDate = $.trim($('#SchDate').val());
    InDate = $.trim($('#InDate').val());
    InTime = $.trim($('#InTime').val());
    Status = $.trim($('#Status').val());
    InWgh = $.trim($('#InWgh').val());
    InvNo = $.trim($('#InvNo').val());
    Cmt = $.trim($('#Cmt').val().toUpperCase());
        $.ajax({
          url: "bd/crudExport.php",
          type: "POST",
          datatype:"json",    
          data:  {ExpID:ExpID, Port:Port, ShpCo:ShpCo, Bkg:Bkg, Ctr:Ctr, Seal:Seal, DO:DO, Lots:Lots, LotID:LotID, SchDate:SchDate, InDate:InDate, InTime:InTime, Status:Status, InWgh:InWgh, InvNo:InvNo, Cmt:Cmt, opcion:opcion},    
          success: function(data) {
            tablaExp.ajax.reload(null, false);
           }
        });			        
    $('#modalCRUD').modal('hide');											     			
});
        
 

//para limpiar los campos antes de dar de Alta una Persona
$("#btnNuevo").click(function(){
    $('#Lots').empty();
    opcion = 1; //alta           
    ExpID=null;
    $("#formExp").trigger("reset");
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    $(".modal-title").text("New Export");
    $('#modalCRUD').modal('show');	
   // puerto();    
    navieras();
    $("#Port").empty();  
    $("#Portdestino").change(function () {
        navi=$("#ShpCo").val();
        dest=$(this).val();
        if(navi!="" && dest!=""){
            getpuertos(dest,navi);
        }
    });

    $("#ShpCo").change(function () {
        navi=$(this).val();
        dest=$("#Portdestino").val();
        if(navi!="" && dest!=""){
            getpuertos(dest,navi);
        }
    });


    $("#Port").change(function () {
        getcityport($(this).val());
    });
    
    

   
});

//Editar        
$(document).on("click", ".btnEditar", function(){
    $('#Lots').empty();
    opcion = 2;//editar
    fila = $(this).closest("tr");	        
    ExpID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID		            
    Port = fila.find('td:eq(1)').text();
    ShpCo = fila.find('td:eq(2)').text();
    Bkg = fila.find('td:eq(3)').text();
    Ctr = fila.find('td:eq(4)').text();
    Seal = fila.find('td:eq(5)').text();
    DO = fila.find('td:eq(6)').text();
    Lots = fila.find('td:eq(7)').text();
    Qty = parseFloat(fila.find('td:eq(8)').text());
    SchDate = fila.find('td:eq(9)').text();
    InDate = fila.find('td:eq(10)').text();
    InTime = fila.find('td:eq(11)').text();
    OutDate = fila.find('td:eq(12)').text();
    OutTime = fila.find('td:eq(13)').text();
    Gin = fila.find('td:eq(14)').text();
    OutReg = fila.find('td:eq(15)').text();
    Ctc = fila.find('td:eq(16)').text();
    Cli = fila.find('td:eq(17)').text();
    Status = fila.find('td:eq(18)').text();
    TNam = fila.find('td:eq(19)').text();
    CAAT = fila.find('td:eq(20)').text();
    WBill = fila.find('td:eq(21)').text();
    TrkPlt = fila.find('td:eq(22)').text();
    TraPlt = fila.find('td:eq(23)').text();
    DrvNam = fila.find('td:eq(24)').text();
    PWgh = fila.find('td:eq(25)').text();
    OutWgh = fila.find('td:eq(26)').text();
    InWgh = fila.find('td:eq(27)').text();
    InvNo = fila.find('td:eq(28)').text();
    Cmt = fila.find('td:eq(29)').text();
    Statustrk = fila.find('td:eq(30)').text();
    TrkID = fila.find('td:eq(31)').text();
    //$("#Port").val(Port);
   
    puerto(Port);  
    navieras(ShpCo);
    getcityport(Port);
    $("#ShpCo").val(ShpCo);
    $("#Bkg").val(Bkg);
    $("#Ctr").val(Ctr);
    $("#Seal").val(Seal);
    $("#DO").val(DO);
    $('#Lots').append('<option class="form-control selected">' + fila.find('td:eq(7)').text() + '</option>');
    $("#Qty").val(Qty);
    $("#SchDate").val(SchDate);
    $("#InDate").val(InDate);
    $("#InTime").val(InTime);
    $("#OutDate").val(OutDate);
    $("#OutTime").val(OutTime);
    $("#Gin").val(Gin);
    $("#OutReg").val(OutReg);
    $("#Ctc").val(Ctc);
    $("#Cli").val(Cli);
    $("#Status").val(Status);
    $("#TNam").val(TNam);
    $("#CAAT").val(CAAT);
    $("#WBill").val(WBill);
    $("#TrkPlt").val(TrkPlt);
    $("#TraPlt").val(TraPlt);
    $("#DrvNam").val(DrvNam);
    $("#PWgh").val(PWgh);
    $("#OutWgh").val(OutWgh);
    $("#InWgh").val(InWgh);
    $("#Inv").val(InvNo);
    $("#Cmt").val(Cmt);
    $("#statustrk").val(Statustrk);
    $("#TrkID").val(TrkID);

    Statustrk
    $(".modal-header").css("background-color", "#17562c");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Edit Export");		
    $('#modalCRUD').modal('show');		   
});

//Borrar
$(document).on("click", ".btnBorrar", function(){
    fila = $(this);           
    InvID = $(this).closest('tr').find('td:eq(1)').text();		
    opcion = 3; //eliminar        
    var respuesta = confirm("¿Está seguro que deseas deshabilitar el registro "+InvID+"?");                
    if (respuesta) {            
        $.ajax({
          url: "bd/crudExport.php",
          type: "POST",
          datatype:"json",    
          data:  {opcion:opcion, InvID:InvID},    
          success: function() {
              tablaExp.ajax.reload(null, false); //tablaInv.row(fila.parents('tr')).remove().draw();                  
           }
        });	
    }
 });
    

     
});

//Lista de Lotes a selecinar cuando se requiere editar una export
$(document).ready(function(){
    $(document).on("click", ".btnEditar", function(){
            var opts = "";
            opcion = 5;
            DO = $.trim($('#DO').val());
            $.ajax({
                url: "bd/crudExport.php",
                type: "POST",
                datatype:"json",
                data:  {DO:DO ,opcion:opcion},    
                success: function(data){
                    opts = JSON.parse(data);
                    $('#Lots').append('<option value="0">-Select Lot-</option disabled>');
                    for (var i = 0; i< opts.length; i++){
                        $('#Lots').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot +"-" +opts[i].Qty+ " bc"+'</option>');
                    }
                },
                error: function(data) {
                    alert('error');
                }
            });
        });
});

//Cambiar datos correspondientes a lote seleccionado
$(document).ready(function(){
    $("#Lots").on("change", function(){
        $('#Qty').val("");
        $('#OutDate').val("");
        $('#OutTime').val("");
        $('#TNam').val("");
        $('#CAAT').val("");
        $('#WBill').val("");
        $('#TrkPlt').val("");
        $('#TraPlt').val("");
        $('#DrvNam').val("");
        $('#PWgh').val("");
        $('#OutWgh').val("");
        $("#InWgh").val("");
        $("#Inv").val("");
        $("#statustrk").val("");
        $("#TrkID").val("");
        opcion = 6;
        DO = $.trim($('#DO').val());
        LotID = $.trim($('#Lots').val());
        if(LotID != 0){
        $.ajax({
            url: "bd/crudExport.php",
            type: "POST",
            datatype:"json",
            data:  {DO:DO, LotID:LotID , opcion:opcion},    
            success: function(data){
                opts = JSON.parse(data);
                $('#Qty').val(opts[0].Qty);
                $('#OutDate').val(opts[0].OutDat);
                $('#OutTime').val(opts[0].OutTime);
                $('#TNam').val(opts[0].TNam);
                $('#CAAT').val(opts[0].CAAT);
                $('#WBill').val(opts[0].WBill);
                $('#TrkPlt').val(opts[0].TrkLPlt);
                $('#TraPlt').val(opts[0].TraLPlt);
                $('#DrvNam').val(opts[0].DrvNam);
                $('#PWgh').val(opts[0].LiqWgh);
                $('#OutWgh').val(opts[0].OutWgh);
                $("#InWgh").val(opts[0].InWgh);
                $("#Inv").val(opts[0].Invoice);
                $("#statustrk").val(opts[0].Status);
                 $("#TrkID").val(opts[0].TrkID);

                
                
                
            },
            error: function(data) {
                alert('error');
            }
        });
     }
 });
});


//Generar listados de lotes
$(document).ready(function(){
    var datosbook=null;
    $("#btnReporte").click(function(){
        ///asiganr fecha en capo fecha de reporte
        var date=new Date();  
        var day=String(date.getDate()).padStart(2, '0');          
        var month=("0" + (date.getMonth() + 1)).slice(-2); 
        var year=date.getFullYear();  
        fechahoy=year+"-"+month+"-"+day
       
        

        $("#formbook").trigger("reset");
        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $(".modal-title").text("Generate Packing List");		
        $('#RepBook').modal('show');	
        $('#Francbal').val(4);
        $('#TareBale').val(2);
        $('#fecha').val(fechahoy);
        $('#GenerarRep').prop('disabled', true);
        document.getElementById('alerta_lista').style.display = 'none';	  
        //document.getElementById('infotrucks').style.display = 'none'; 
        
       

    });
    $("#limpiar").click(function(){
        $("#formbook").trigger("reset");
        $('#Francbal').val(4);
        $('#TareBale').val(2);
        $('#fecha').val(fechahoy);
        $('#GenerarRep').prop('disabled', true);
        document.getElementById('alerta_lista').style.display = 'none';
       // document.getElementById('infotrucks').style.display = 'none'; 
        datosbook = null;
        catidad = null;
        PesoBrutoReal = null;
        TotalFran = null;
        GrossWght = null;
        TotalTare = null;
        NetWght = null;
        
        
    });
    

    $("#buscar").click(function(){
        
        Booking = $('#Booking').val(); 
        Francbal=$('#Francbal').val();
        TareBale=$('#TareBale').val();

        $.ajax({
            url: "bd/crudExport.php",
            type: "POST",
            datatype:"json",
            data:  {opcion:14,Booking:Booking,Francbal:Francbal,TareBale:TareBale},    
            success: function(data){         
                
                datosbook = JSON.parse(data); 
               
                if(datosbook.CantidadBales != null && datosbook.RealGross != null && datosbook.Lotpartido != 1 && datosbook.pesoscap==0){
              
                    catidad=datosbook.CantidadBales;   
                    PesoBrutoReal= datosbook.RealGross;              
                    
                  /*  TotalFran=catidad*Francbal;                   
                    GrossWght=parseInt(TotalFran)+parseInt(PesoBrutoReal);
                    TotalTare=catidad*TareBale;
                    NetWght= parseInt(GrossWght) - parseInt(TotalTare);
                    $('#TotalBales').val(catidad);   
                    $('#PesoBrutoReal').val(PesoBrutoReal);           
                    $('#TotalFran').val(TotalFran);
                    $('#GrossWght').val(GrossWght);
                    $('#TotalTare').val(TotalTare);
                    $('#NetWght').val(NetWght); 
                    $('#GenerarRep').prop('disabled', false); 
                    */
                    llenar_campos(Francbal,TareBale,catidad,PesoBrutoReal);
                    document.getElementById('alerta_lista').style.display = 'none';  
                    //document.getElementById('infotrucks').style.display = 'block'; 
                    

                    $("#Francbal").keyup(function(evt){
                        //validar que solo sean numeros enteros y decimales
                        var self = $(this);
                        self.val(self.val().replace(/[^0-9\.]/g, ''));
                        if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
                        {
                            evt.preventDefault();
                        }
                        
                       
                        llenar_campos($(this).val(),TareBale,catidad,PesoBrutoReal); 
                        tara = $('#TareBale').val();
                        pesosreporte($(this).val(),tara)                  

        
                        
                       
                    });

                    $("#TareBale").keyup(function(evt){
                        //validar que solo sean numeros enteros y decimales
                        var self = $(this);
                        self.val(self.val().replace(/[^0-9\.]/g, ''));
                        if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
                        {
                            evt.preventDefault();
                        }                    
                        llenar_campos(Francbal,$(this).val(),catidad,PesoBrutoReal);
                        Francbal=$('#Francbal').val();
                       
                        pesosreporte(Francbal,$(this).val())
                       
                    });

                    
                } 
                else{
                    //document.getElementById('infotrucks').style.display = 'none'; 
                    document.getElementById('alerta_lista').style.display = 'block';
                     $('#GenerarRep').prop('disabled', true);
                    if(datosbook.CantidadBales === null && datosbook.RealGross === null){
                        //$('#alrta_lista').html("Booking no encontrado"); 
                        document.getElementById("texto_alerta").textContent="Booking not found";
                    }
                    if((datosbook.Lotpartido==1 && datosbook.pesoscap!=0) || (datosbook.Lotpartido==1 && datosbook.pesoscap==0)){
                       // $('#alrta_lista').html("Booking tiene lotes divididos"); 
                        document.getElementById("texto_alerta").textContent="Booking has split lots";
                    }

                    if(datosbook.pesoscap!=0 && datosbook.Lotpartido==0){
                        document.getElementById("texto_alerta").textContent="No output weight capture in a truck";
                    }
                }       


               
            },
            error: function(data) {
                alert('error');
            }
        });

    });


  

    $("#GenerarRep").click(function(){
         
        Franq=$('#Francbal').val();
        Tara=$("#TareBale").val();
        label=$("#etiqueta").val();
        fecha=$("#fecha").val();
        window.open("./bd/BookingPDF.php?Booking="+Booking+"&Tara="+Tara+"&Franq="+Franq+"&fecha="+fecha+"&label="+label);
    });


    $("#btnCloselist,#closelist").click(function(){
        datosbook = null;
        catidad = null;
        PesoBrutoReal = null;
        TotalFran = null;
        GrossWght = null;
        TotalTare = null;
        NetWght = null;
       
        $('#RepBook').modal('hide');	
    });

    //modal que muestra detalles de trucks
    $("#infotrucks").on('click', function (e) {
        e.preventDefault();
        Booking = $('#Booking').val(); 
        informaciontruks(Booking);
    });

    $("#closeInfoTrk,#closeinfo").click(function(){
        $('#modalInTrucks').modal('hide');
        $(".modal-title").text("Generate Packing List");	
    });
});




function puerto(valor){
    idport= "";
    $("#Port").empty();
    $.ajax({
      url: "bd/crudExport.php",
      type: "POST",
      datatype:"json",
      data:  {opcion:7,Port:valor},    
      success: function(data){         
          //$("#Port").append($('<option>').val("").text("Choose..."));
          opts = JSON.parse(data);
          for (var i = 0; i< opts.length; i++){ 
                $("#Port").append($('<option>').val(opts[i].IDPort).text(opts[i].Port)); 
              if(valor!="" && valor == opts[i].Port){
                idport=opts[i].IDPort;
                $("select#Portdestino").val(opts[i].City);
              }           
          } 
          
          $("select#Port").val(idport);
      },
      error: function(data) {
          alert('error');
      }
    });

  }

function navieras(nav){
    idnav= "";
    $("#ShpCo").empty();
    $.ajax({
      url: "bd/crudExport.php",
      type: "POST",
      datatype:"json",
      data:  {opcion:11 },    
      success: function(data){         
          $("#ShpCo").append($('<option>').val("").text("Choose..."));
          opts = JSON.parse(data);
          for (var i = 0; i< opts.length; i++){ 
              $("#ShpCo").append($('<option>').val(opts[i].IDNaviera).text(opts[i].Naviera));  
              if(nav!="" && nav == opts[i].Naviera){
                idnav = opts[i].IDNaviera;
              }           
          } 
          
          $("select#ShpCo").val(idnav);
      },
      error: function(data) {
          alert('error');
      }
    });

}

function getpuertos(destino,ShpCo){
    $("#DirPort").val('');
    $.ajax({
        url: "bd/crudExport.php",
        type: "POST",
        datatype:"json",
        data:  {destino:destino,ShpCo:ShpCo,opcion:12},    
        success: function(data){ 
            $("#Port").empty();        
            $("#Port").append($('<option>').val(0).text("Choose..."));
            opts = JSON.parse(data);
            for (var i = 0; i< opts.length; i++){ 
                $("#Port").append($('<option>').val(opts[i].IDPort).text(opts[i].Port));                
            } 

            $("select#Port").val(opts[0].selectdef);
            if(opts[0].selectdef!=0){
                getcityport(opts[0].selectdef)

            }
        },
        error: function(data) {
            alert('error');
        }
    });

    
}

function getcityport(puerto){
    $.ajax({
        url: "bd/crudExport.php",
        type: "POST",
        datatype:"json",
        data:  {Port:puerto,opcion:13},    
        success: function(data){ 
            $("#DirPort").val('');
            opts = JSON.parse(data);
            direccion=opts[0].Direction + ' C.P. ' + opts[0].PostalCode;
            $("#DirPort").val(direccion); 
           
        },
        error: function(data) {
            alert('error');
        }
    });

}


function llenar_campos(Francbal,TareBale,catidad,PesoBrutoReal){
    TotalFran=catidad*Francbal;                   
    GrossWght=parseInt(TotalFran)+parseInt(PesoBrutoReal);
    TotalTare=catidad*TareBale;
    NetWght= parseInt(GrossWght) - parseInt(TotalTare);
    $('#TotalBales').val(catidad);   
    $('#PesoBrutoReal').val(PesoBrutoReal);           
    $('#TotalFran').val(TotalFran);
    $('#GrossWght').val(GrossWght);
    $('#TotalTare').val(TotalTare);
    $('#NetWght').val(NetWght); 

    if(catidad!=null && PesoBrutoReal !=null){
        $('#promedioreal').val(dosDecimales(PesoBrutoReal/catidad)); 
        $('#promediofranc').val(dosDecimales(GrossWght/catidad));
        $('#promediotara').val(dosDecimales(NetWght/catidad));             
        
        $('#GenerarRep').prop('disabled', false); 
    }
}


function dosDecimales(n) {
    let t=n.toString();
    let regex=/(\d*.\d{0,2})/;
    return t.match(regex)[0];
}

function informaciontruks(Bkg){
    var htmlData="";
    opcion = 15;
    var tabla = $('#tablainfo').find('tbody');
    tabla.empty();
    //htmlData =   htmlData.concat('<ul><li>'+'TrkID'+'   '+'Lot'+'   '+'Bales'+' '+'Out Weight'+'    '+'AVG Weight'+'</li></ul><br>');        
   // htmlData = "<br>";
        

        $.ajax({
          url: "bd/crudExport.php",
          type: "POST",
          datatype:"json",    
          data:  {Bkg:Bkg, opcion:opcion},    
          success: function(data) {
            opts = JSON.parse(data);
            if (opts.length >0){
                for (var i = 0; i< opts.length; i++){

                    fila = $('<tr>');
                    truck = $('<td>').text(opts[i].TrkID);
                    lote = $('<td>').text(opts[i].Lot);
                    cantidad = $('<td>').text(opts[i].cantidad);
                    peso = $('<td>').text(opts[i].OutWgh);
                    pesoprom = $('<td>').text(opts[i].promedio);

                    fila.append(truck, lote, cantidad, peso, pesoprom);
                    tabla.append(fila);

                   // htmlData =   htmlData.concat('<ul><li>'+'TrkID:'+opts[i].TrkID+'&nbsp;&nbsp Lot:'+opts[i].TrkID+'&nbsp;&nbsp Quantity:'+opts[i].cantidad+' bc'+'&nbsp;&nbsp Out Weight:'+opts[i].OutWgh+'&nbsp;&nbsp AVG Weight:'+opts[i].promedio+'</li></ul>');                   
                } 
                //$('#InfTrk').html(htmlData);
            }

            else{
               // $('#InfTrk').html("No assigned lots");
            }
        }
    });
    $(".modal-header").css("background-color", "#17562c");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Info Truks");		
    $('#modalInTrucks').modal('show');

   
}



function pesosreporte(fran,tara){
    document.getElementById('alerta_lista').style.display = 'block';
    if(fran === null || fran === ""){
        //$('#alrta_lista').html("Booking no encontrado"); 
        document.getElementById("texto_alerta").textContent="Enter Franchise Per Bal";
        $('#GenerarRep').prop('disabled', true); 
    }
    if(tara==null || tara==""){
       // $('#alrta_lista').html("Booking tiene lotes divididos"); 
        document.getElementById("texto_alerta").textContent="Enter Tare per Bale";
        $('#GenerarRep').prop('disabled', true); 
    }

    if((tara==null && fran === null)|| ( tara=="" && fran === "")){
        document.getElementById("texto_alerta").textContent="Enter Tare per Bale and Franchise Per Bal";
        $('#GenerarRep').prop('disabled', true); 
    }

    if(  tara!="" && fran != ""){
        document.getElementById('alerta_lista').style.display = 'none';
        $('#GenerarRep').prop('disabled', false); 
    }

}