<?php
    require '../vendor/autoload.php';
  
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use Aws\S3\S3Client; 
    use Aws\Exception\AwsException;

    include_once '../bd/conexion.php';
    $objeto = new Conexion();
    $conexion = $objeto->Conectar();

    require './vendor/phpmailer/phpmailer/src/PHPMailer.php';
    require './vendor/phpmailer/phpmailer/src/SMTP.php';
    require './vendor/autoload.php';
    date_default_timezone_set('America/Los_Angeles');

    $usuarioP= (isset($_POST['usuarioP'])) ? $_POST['usuarioP'] : '';
    $LiqIDP= (isset($_POST['LiqIDP'])) ? $_POST['LiqIDP'] : '';
    $TipoLiqP= (isset($_POST['TipoLiqP'])) ? $_POST['TipoLiqP'] : '';
    $mailsP= (isset($_POST['mailsP'])) ? $_POST['mailsP'] : '';
    $subjectP= (isset($_POST['subjectP'])) ? $_POST['subjectP'] : '';
    $bodyP= (isset($_POST['bodyP'])) ? $_POST['bodyP'] : '';
    $payMetod= (isset($_POST['payMetod'])) ? $_POST['payMetod'] : '';
    $ConSF= (isset($_POST['ConSF'])) ? $_POST['ConSF'] : '';


    $bucket = 'pruebasportal';
    //$bucket = 'portal-liq';
    $keyname = 'Liq'.$TipoLiqP."_".$LiqIDP.'.xlsx';
    $filepath = './temp/'.$keyname;

    $keyname1 = 'constancia_Fiscal.pdf';
    $filepath1 = './temp/'.$keyname1;

    $s3 = new S3Client([
    'version'     => 'latest',
    'region'      => 'us-east-2', //bucket de pruebas
    //'region'      => 'us-east-1',
    'credentials' => [
        'key'    => 'AKIAT442VUCJQXPEFVZW',
        'secret' => '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',
    ],
    ]);

    try {
        $result = $s3->getObject([
            'Bucket' => $bucket,
            'Key'    => $keyname,
            'SaveAs' => $filepath
        ]);
    }
    catch (S3Exception $e) {
        echo $e->getMessage() . PHP_EOL;
    }
    try {
        $result = $s3->getObject([
            'Bucket' => $bucket,
            'Key'    => $keyname1,
            'SaveAs' => $filepath1
        ]);
    }
    catch (S3Exception $e) {
        echo $e->getMessage() . PHP_EOL;
    }


    $query = 'SELECT User, email, passApp FROM amsadb1.Users WHERE User = "'.$usuarioP.'"';
    $result =$conexion->prepare($query);
    $result->execute();
    $datosuser=$result->fetch(PDO::FETCH_ASSOC);

    $email=$datosuser['email'];
    $pass =$datosuser['passApp'];
    $usuario = ucwords(strtolower($datosuser['User']));

    // Intancia de PHPMailer
    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->SMTPDebug = 0; 
    $mail->Host = 'smtp.gmail.com';
    $mail->Port  = 465; 
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS; 
    $mail->SMTPAuth = true;
    $mail->Username = $email;
    $mail->Password = $pass;
    // Quien envía este mensaje
    $mail->setFrom($email, $usuario);
    $correos = "marisol.hernandez@ecomtrading.com";
    $mail->addAddress($correos);
    /* $arraycorreo = explode(",",$mailsP);
    $tam = sizeof($arraycorreo);
    for($i=0; $i<$tam; $i++){
        if (trim($arraycorreo[$i]) !== $email) { // Evitar añadir el remitente
            $mail->addAddress(trim($arraycorreo[$i]));
        }
    } */

    // Asunto del correo
    $mail->Subject = $subjectP;
    // Contenido
    $mail->IsHTML(true);
    $mail->CharSet = 'UTF-8';
    
    $bodyy =  $bodyP ;
    $bodyy = str_replace(
        ["USO CFDI:", "MÉTODO DE PAGO:", "FORMA DE PAGO:", "CLAVE SERVICIO:", "MONEDA:", "TIPO DE CAMBIO:", "OBJETO DE IMPUESTO:", "TIPO FACTOR:"],
        ["<b>USO CFDI:</b>", "<b>MÉTODO DE PAGO:</b>", "<b>FORMA DE PAGO:</b>", "<b>CLAVE SERVICIO:</b>", "<b>MONEDA:</b>", "<b>TIPO DE CAMBIO:</b>", "<b>OBJETO DE IMPUESTO:</b>", "<b>TIPO FACTOR:</b>"],
        $bodyy
    );

    $bodyy .= '<br><br><br><b>Saludos</b><br> '.$usuario;
 

    $mail->Body = $bodyy;

    // crear el archivo de excel
    $filename = 'Liq'.$TipoLiqP."_".$LiqIDP.'.xlsx';
    $rem='./temp/'.$filename;
    $mail->addAttachment($rem); 
    $mail->addAttachment($filepath);

    // crear la constancia de situacion fiscal
    if ($ConSF == '1'){
        $filename1 = 'constancia_Fiscal.pdf';
        $rem1='./temp/'.$filename1;
        $mail->addAttachment($rem1); 
        $mail->addAttachment($filepath1);
    }

    if ($mail->send()) {
    $Hoy = date('Y-m-d');
    $consulta = "UPDATE amsadb1.Liquidation SET datProv ='$Hoy', prov = 1, payMetod = '$payMetod' WHERE LiqID='$LiqIDP' and TipoLiq='$TipoLiqP'";

    $resultado = $conexion->prepare($consulta);
    if ($resultado->execute()) {
        echo json_encode(["exists" => true, "message" => "Correo enviado correctamente"]);
    } else {
        echo json_encode(["exists" => false, "error" => "Error al actualizar la fecha en la base de datos."]);
    }
    } else {
        echo json_encode(["exists" => false, "error" => "Error al enviar el correo: " . $mail->ErrorInfo]);
    }

    //Borrado de archivos de carpeta temp
    function eliminarArch   ($path, $TipoLiqP, $LiqIDP) {
        $file = $path . '/Liq' . $TipoLiqP . "_" . $LiqIDP . '.xlsx';
        $file1 = $path . '/constancia_Fiscal.pdf';
        if (is_file($file)) {
            unlink($file);
            unlink($file1);
        } 
    }
    
    // Llamada a la función para eliminar el archivo específico
    eliminarArch    ('./temp', $TipoLiqP, $LiqIDP);

?>