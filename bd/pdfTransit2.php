<?php 
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set("America/Mexico_City");


// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

//Query
$query= "SELECT DISTINCT Truks.TrkID, TrkLPlt, TraLPlt, WBill, DrvLcs, DO, DrvNam, DrvTel, CrgQty, OutDat, OutTime, InDat, InTime, LotsAssc, Comments, Truks.Status, 
(SELECT Typ FROM DOrds WHERE DO=Dord) as Typ,
(Select BnName From Transports Where Truks.TNam = Transports.TptID) as TNam,
(Select RegNam From DOrds, Region Where Truks.DO = DOrds.DOrd and OutPlc = IDReg) as RegNameOut,
(Select RegNam From DOrds, Region Where Truks.DO = DOrds.DOrd and InReg = IDReg) as RegNameIn,
(Select Cli From DOrds, Clients Where Truks.DO = DOrds.DOrd and InPlc = CliID) as PlcNameIn,
(Select IFNULL(SUM(Lots.Qty),0) From Lots Where Truks.TrkID = Lots.TrkID) as LotQty,
ifnull(GinName, (SELECT GinName FROM DOrds, Gines WHERE Truks.DO = DOrds.DOrd and DOrds.Gin = Gines.IDGin)) as GinName 
FROM Truks 
left join Lots on Truks.DO = Lots.DOrd and Truks.LotsAssc = Lots.Lot
left join Gines on Lots.GinID = Gines.IDGin
WHERE Truks.Status = 'Transit' OR Truks.Status = 'Programmed'  order by Truks.TrkID DESC;";

$result = $conexion->prepare($query);
$result->execute();
$siexiste=0; //Para verificar que hayan datos

//Define the filename with current date
$fileName = "TrucksStatus-".date('d-m-Y').".xlsx";

$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Trucks in Transit");
//$hojaActiva->freezePane("A2");




$hojaActiva->getColumnDimension('A')->setWidth(10);
$hojaActiva->setCellValue('A1','DO');
$hojaActiva->getColumnDimension('B')->setWidth(16);
$hojaActiva->setCellValue('B1','Lot');
$hojaActiva->getColumnDimension('C')->setWidth(12);
$hojaActiva->setCellValue('C1','BC Transit');
$hojaActiva->getColumnDimension('D')->setWidth(18);
$hojaActiva->setCellValue('D1','Gin');
$hojaActiva->getColumnDimension('E')->setWidth(15);
$hojaActiva->setCellValue('E1','Departure Region');
$hojaActiva->getColumnDimension('F')->setWidth(15);
$hojaActiva->setCellValue('F1','Arrival Region');
$hojaActiva->getColumnDimension('G')->setWidth(25);
$hojaActiva->setCellValue('G1','Client');
$hojaActiva->getColumnDimension('H')->setWidth(25);
$hojaActiva->setCellValue('H1','Departure Date');
$hojaActiva->getColumnDimension('I')->setWidth(25);
$hojaActiva->setCellValue('I1','Arrival Date');
$hojaActiva->getColumnDimension('J')->setWidth(10);
$hojaActiva->setCellValue('J1','Days Late');
$hojaActiva->getColumnDimension('K')->setWidth(10);
$hojaActiva->setCellValue('K1','Truck ID');
$hojaActiva->getColumnDimension('L')->setWidth(40);
$hojaActiva->setCellValue('L1','Bussiness Name Transport');
$hojaActiva->getColumnDimension('M')->setWidth(15);
$hojaActiva->setCellValue('M1','License Plate');
$hojaActiva->getColumnDimension('N')->setWidth(35);
$hojaActiva->setCellValue('N1','Driver');
$hojaActiva->getColumnDimension('O')->setWidth(15);
$hojaActiva->setCellValue('O1','Phone Number');
$hojaActiva->getColumnDimension('P')->setWidth(15);
$hojaActiva->setCellValue('P1','WayBill');
$hojaActiva->getColumnDimension('Q')->setWidth(20);
$hojaActiva->setCellValue('Q1','Status');
$hojaActiva->getColumnDimension('R')->setWidth(35);
$hojaActiva->setCellValue('R1','Comments');

$fila = 2;

$today = new DateTime();//DateTime("2022-04-06"); //echo $today->format('Y-m-d');

while($row = $result->fetch(PDO::FETCH_ASSOC)){
    if($siexiste==0){
        if(!empty($row['TrkID']))
            $siexiste=1;
    }
    $dlate = 0;
    $textlate = "";
    if($row['Status'] == "Transit"){
        $arrivalDate = new DateTime($row['InDat']); //echo $second->format('Y-m-d');
        $intvl = $arrivalDate->diff($today);
        
        if(($today > $arrivalDate)&&($intvl->days > 0)){
            $dlate = $intvl->days;
            $textlate = "Delayed ";
    }}

    $hojaActiva->setCellValue('A' . $fila,$row['DO']);
    $hojaActiva->setCellValue('B' . $fila,$row['LotsAssc']);
    $hojaActiva->setCellValue('C' . $fila,$row['LotQty']);
    $hojaActiva->setCellValue('D' . $fila,$row['GinName']);
    $hojaActiva->setCellValue('E' . $fila,$row['RegNameOut']);
    $hojaActiva->setCellValue('F' . $fila,$row['RegNameIn']);
    $hojaActiva->setCellValue('G'. $fila,$row['PlcNameIn']);
    $hojaActiva->setCellValue('H'. $fila,$row['OutDat'].' '.$row['OutTime']);
    $hojaActiva->setCellValue('I'. $fila,$row['InDat'].' '.$row['InTime']);
    $hojaActiva->setCellValue('J'. $fila,$dlate);
    $hojaActiva->setCellValue('K'. $fila,$row['TrkID']);
    $hojaActiva->setCellValue('L'. $fila,$row['TNam']);
    $hojaActiva->setCellValue('M'. $fila,$row['DrvLcs']);
    $hojaActiva->setCellValue('N'. $fila,$row['DrvNam']);
    $hojaActiva->setCellValue('O'. $fila,$row['DrvTel']);
    $hojaActiva->setCellValue('P'. $fila,$row['WBill']);
    $hojaActiva->setCellValue('Q'. $fila,$textlate.$row['Status']);
    $hojaActiva->setCellValue('R' . $fila,$row['Comments']);

    $fila++;
} 

$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => Border::BORDER_THIN,
            'color' => ['rgb' => 'FF000000'],
        ],
    ],
];
$fila--;
$hojaActiva->getStyle('A1:R1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

$hojaActiva->getStyle('A1:R'.$fila)->applyFromArray($styleArray);


// redirect output to client browser
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');

$writer = IOFactory::createWriter($excel, 'Xlsx');
$writer->save('php://output');
exit;

/*
while($row = $result->fetch(PDO::FETCH_ASSOC)){
    if($siexiste==0){
        if(!empty($row['TrkID']))
            $siexiste=1;
    }
    $arrivalDate = new DateTime($row['InDat']); //echo $second->format('Y-m-d');
    $intvl = $arrivalDate->diff($today);
    $dlate = 0;
    $textlate = "";
    if($today > $arrivalDate){
        $dlate = $intvl->days;
        $textlate = "Delayed ";
    }
    $linedata = array($row['DO'],$row['LotsAssc'],$row['LotQty'],$row['GinName'],$row['RegNameOut'],$row['RegNameIn'],$row['PlcNameIn'],$row['OutDat'].' '.$row['OutTime'],$row['InDat'].' '.$row['InTime'],$dlate,$row['TrkID'],$row['TNam'],$row['DrvLcs'],$row['DrvNam'],$row['DrvTel'],$row['WBill'],$textlate.$row['Status'],$row['Comments'] );
    array_walk($linedata, 'filterData'); 
    $excelData .= implode("\t", array_values($linedata)) . "\n"; 
} 

// Filter the excel data 
function filterData(&$str){ 
    $str = preg_replace("/\t/", "\\t", $str); 
    $str = preg_replace("/\r?\n/", "\\n", $str); 
    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"'; 
} 


$fields = array('DO','Lot','BC Transit','Gin','Departure region','Arrival region','Client','Departure date','Arrival Date',
'Days late', 'Truck ID','Bussiness name transport','License plate','Driver','Phone Number','Waybill','Status','Comments');

// Display column names as first row 
$excelData = implode("\t", array_values($fields)) . "\n"; 

/* POR SI DESPUÉS VEMOS QUE ES MEJOR CONSULTA
$queryIds = 'SELECT T.TrkID FROM Truks T where T.Status = "Transit";';
$result = $conexion->prepare($queryIds);
$result->execute();
$data = $result->fetchAll(PDO::FETCH_ASSOC);


$ids = "";
foreach($data as $dat){
    $ids.= $dat['TrkID'].",";
}
$ids = substr($ids, 0, -1);

$array=array_map('intval', explode(',', $ids));
$array = implode("','",$array); */




/*$query= "SELECT DISTINCT Truks.TrkID, TrkLPlt, TraLPlt, WBill, DrvLcs, DO, DrvNam, DrvTel, CrgQty, OutDat, OutTime, InDat, InTime, LotsAssc, Comments, Truks.Status, 
(SELECT Typ FROM DOrds WHERE DO=Dord) as Typ,
(Select BnName From Transports Where Truks.TNam = Transports.TptID) as TNam,
(Select RegNam From DOrds, Region Where Truks.DO = DOrds.DOrd and OutPlc = IDReg) as RegNameOut,
(Select RegNam From DOrds, Region Where Truks.DO = DOrds.DOrd and InReg = IDReg) as RegNameIn,
(Select Cli From DOrds, Clients Where Truks.DO = DOrds.DOrd and InPlc = CliID) as PlcNameIn,
(Select IFNULL(SUM(Lots.Qty),0) From Lots Where Truks.TrkID = Lots.TrkID) as LotQty,
ifnull(GinName, (SELECT GinName FROM DOrds, Gines WHERE Truks.DO = DOrds.DOrd and DOrds.Gin = Gines.IDGin)) as GinName 
FROM Truks 
left join Lots on Truks.DO = Lots.DOrd and Truks.LotsAssc = Lots.Lot
left join Gines on Lots.GinID = Gines.IDGin
WHERE Truks.TrkID IN ('".$array."');"; 9.10*/






/*
if($siexiste == 0){
    $excelData .= 'No records found...'. "\n"; 
}
 
// Render excel data 
echo $excelData; 
 
exit;
*/




?>