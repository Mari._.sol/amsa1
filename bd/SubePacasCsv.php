<?php 
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set("America/Mexico_City");

function lowercase($element)
{
    return "'".$element."'";
}

$DO = $_GET['DOCMS'];
$LotsCMS = $_GET['LotsCMS'];
$DateCMS = $_GET['DateCMS'];

//$array = array_map('lowercase', explode(',', $LotsCMS));
//$array = implode(",",$array);

//Si la busqueda es por DO
if ($DO != ""){
    $Lots = array();
    $i = 0;
    $consulta = 'SELECT distinct Bales.Lot FROM Bales, DOrds, Lots WHERE DOrds.DOrd = '.$DO.' and DOrds.DOrd = Lots.DOrd and Bales.Lot = Lots.Lot;'; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataDO=$resultado->fetchALL(PDO::FETCH_ASSOC);
    foreach($dataDO as $dat){
        $Lots[$i] = $dat['Lot'];
        $i++;
    }
    $array = array_map('lowercase',$Lots);
    $array = implode(",",$array);
}else{
    $array = array_map('lowercase', explode(',', $LotsCMS));
    $array = implode(",",$array);
}


// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\CSV;


//Define the filename with current date
$fileName = "SubePacas_".date('d-M-Y').".csv";

$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Hoja 1");
//$hojaActiva->freezePane("A2");




/*$hojaActiva->getColumnDimension('A')->setWidth(8);
$hojaActiva->setCellValue('A1','Gin ID');
$hojaActiva->getColumnDimension('B')->setWidth(9);
$hojaActiva->setCellValue('B1','Bale ID');
$hojaActiva->getColumnDimension('C')->setWidth(7);
$hojaActiva->setCellValue('C1','Crop Year');
$hojaActiva->getColumnDimension('D')->setWidth(7);
$hojaActiva->setCellValue('D1','Quantity');
$hojaActiva->getColumnDimension('E')->setWidth(7);
$hojaActiva->setCellValue('E1','Grower Code');
$hojaActiva->getColumnDimension('F')->setWidth(7);
$hojaActiva->setCellValue('F1','CMS Dimension 1 Code');
$hojaActiva->getColumnDimension('G')->setWidth(7);
$hojaActiva->setCellValue('G1','CMS Dimension 2 Code');
$hojaActiva->getColumnDimension('H')->setWidth(7);
$hojaActiva->setCellValue('H1','Ginning Date');
$hojaActiva->getColumnDimension('I')->setWidth(7);
$hojaActiva->setCellValue('I1','Warehouse Code');
$hojaActiva->getColumnDimension('J')->setWidth(7);
$hojaActiva->setCellValue('J1','Quality (Type)');
$hojaActiva->getColumnDimension('K')->setWidth(7);
$hojaActiva->setCellValue('K1','Net Weight in Kgs');
$hojaActiva->getColumnDimension('L')->setWidth(7);
$hojaActiva->setCellValue('L1','Purchase No.');
$hojaActiva->getColumnDimension('M')->setWidth(7);
$hojaActiva->setCellValue('M1','Mark');
$hojaActiva->getColumnDimension('N')->setWidth(7);
$hojaActiva->setCellValue('N1','Lot Code');
$hojaActiva->getColumnDimension('O')->setWidth(7);
$hojaActiva->setCellValue('O1','Lot');
$hojaActiva->getColumnDimension('P')->setWidth(7);
$hojaActiva->setCellValue('P1','HVI Lenght');
$hojaActiva->getColumnDimension('Q')->setWidth(7);
$hojaActiva->setCellValue('Q1','HVI Uniformity');
$hojaActiva->getColumnDimension('R')->setWidth(7);
$hojaActiva->setCellValue('R1','HVI Strength');
$hojaActiva->getColumnDimension('S')->setWidth(7);
$hojaActiva->setCellValue('S1','HVI Micronaire');
$hojaActiva->getColumnDimension('T')->setWidth(7);
$hojaActiva->setCellValue('T1','HVI SFI');
$hojaActiva->getColumnDimension('U')->setWidth(7);
$hojaActiva->setCellValue('U1','HVI Elongation');
$hojaActiva->getColumnDimension('V')->setWidth(7);
$hojaActiva->setCellValue('V1','HVI Rd');
$hojaActiva->getColumnDimension('W')->setWidth(7);
$hojaActiva->setCellValue('W1','HVI b');
$hojaActiva->getColumnDimension('X')->setWidth(7);
$hojaActiva->setCellValue('X1','Tare (kgs)');
$hojaActiva->getColumnDimension('Y')->setWidth(7);
$hojaActiva->setCellValue('Y1','Ginning Status');
$hojaActiva->getColumnDimension('Z')->setWidth(7);
$hojaActiva->setCellValue('Z1','No. Series');
$hojaActiva->getColumnDimension('AA')->setWidth(7);
$hojaActiva->setCellValue('AA1','Production No.');
$hojaActiva->getColumnDimension('AB')->setWidth(7);
$hojaActiva->setCellValue('AB1','Purchase Period No.');
$hojaActiva->getColumnDimension('AC')->setWidth(7);
$hojaActiva->setCellValue('AC1','Local Code (removed)');*/

$fila = 1;

$today = new DateTime();//DateTime("2022-04-06"); //echo $today->format('Y-m-d');

//Consulta para seleccionar lotes completos
$query= 'SELECT Crp, Gines.IDReg, Gines.CMSGin, Lot, Count(Bal) as Qty, CEILING(Sum(Wgh)) as Wgh, (Sum(if(LiqID!="",1,0))) as LiqBal 
         FROM Bales
         join Gines on Gines.IDGin = Bales.GinID
         WHERE Bales.Lot IN ('.$array.')
         Group by Lot;';
$result = $conexion->prepare($query);
$result->execute();

$i = 0;
//$LotsALL = array();
while($row = $result->fetch(PDO::FETCH_ASSOC)){
    if ($row['LiqBal'] == $row['Qty']){
        $LotsAll[$i] = $row['Lot'];
        $i++;
    }
    $fila++;
}

$array2 = array_map('lowercase',$LotsAll);
$array2 = implode(",",$array2);

$query= 'SELECT Crp, Gines.IDReg, Gines.IDReg, Gines.CMSGin, Lot, Bal 
         FROM Bales
         join Gines on Gines.IDGin = Bales.GinID
         WHERE Bales.Lot IN ('.$array2.');';

$result = $conexion->prepare($query);
$result->execute();
$siexiste=0; //Para verificar que hayan datos

$fila = 1;

while($row = $result->fetch(PDO::FETCH_ASSOC)){

    $hojaActiva->setCellValue('A' . $fila,$row['CMSGin']);
    $hojaActiva->setCellValue('B' . $fila,$row['Bal']);
    $hojaActiva->setCellValue('C' . $fila,$row['Crp']);
    $hojaActiva->setCellValue('D' . $fila,"1");
    $hojaActiva->setCellValue('E' . $fila,"");
    $hojaActiva->setCellValue('F' . $fila,"31");
    $hojaActiva->setCellValue('G'. $fila,$row['IDReg']);
    $hojaActiva->setCellValue('H'. $fila,"");
    $hojaActiva->setCellValue('I'. $fila,$row['CMSGin']);
    $hojaActiva->setCellValue('J'. $fila,"");
    $hojaActiva->setCellValue('K'. $fila,"");
    $hojaActiva->setCellValue('L'. $fila,"");
    $hojaActiva->setCellValue('M'. $fila,$row['Lot']);
    $hojaActiva->setCellValue('N'. $fila,"");
    $hojaActiva->setCellValue('O'. $fila,$row['Lot']);
    $hojaActiva->setCellValue('P'. $fila,"");
    $hojaActiva->setCellValue('Q' . $fila,"");
    $hojaActiva->setCellValue('R' . $fila,"");
    $hojaActiva->setCellValue('S' . $fila,"");
    $hojaActiva->setCellValue('T' . $fila,"");
    $hojaActiva->setCellValue('U' . $fila,"");
    $hojaActiva->setCellValue('V' . $fila,"");
    $hojaActiva->setCellValue('W' . $fila,"");
    $hojaActiva->setCellValue('X' . $fila,"");
    $hojaActiva->setCellValue('Y' . $fila,"Ginned");
    $hojaActiva->setCellValue('Z' . $fila,"BALES");
    $hojaActiva->setCellValue('AA' . $fila,"");
    $hojaActiva->setCellValue('AB' . $fila,"110000");
    $hojaActiva->setCellValue('AC' . $fila,"");

    $fila++;
}


// redirect output to client browser
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');

$writer = IOFactory::createWriter($excel, 'Csv')->setEnclosure(false);
$writer->setUseBOM(false);
$writer->setLineEnding("\r\n");
$writer->save('php://output');
exit;

?>