
<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set('America/Los_Angeles');
setlocale(LC_TIME, "spanish");

$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$responsable = (isset($_POST['responsable'])) ? $_POST['responsable'] : '';
$trkid = (isset($_POST['trkid'])) ? $_POST['trkid'] : '';
$tipoincidente = (isset($_POST['tipoincidente'])) ? $_POST['tipoincidente'] : '';
$incidentes = (isset($_POST['incidentes'])) ? $_POST['incidentes'] : '';


$fecha = date("d/m/Y");

switch($opcion){
    case 1:

        //CONSULTAR LA COLUMNA RESPONSABLES DE LA BD
        $consulta = "SELECT DISTINCT Responsible FROM Incidents";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
      
    case 2:        
        //Consultar los tipos de incidente
        $consulta = "SELECT  Typeincident FROM Incidents where Responsible='$responsable' ORDER BY Typeincident ASC";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;

  
    case 3:        
            $aux = [];
            //Consultar incidentes de un TRUCK
            $consulta = "SELECT IncidentID FROM Recordincidents WHERE TrkID= '$trkid'";        
            $resultado = $conexion->prepare($consulta);                  
            $resultado->execute();    
            $resultado= $resultado->fetchAll(PDO::FETCH_ASSOC);

            foreach($resultado as $row){
                array_push($aux, $row['IncidentID']);
            }

            
            if(!empty($resultado)){
            $ids = implode(",",$aux);
           
            $consulta = "SELECT Responsible,Typeincident FROM Incidents where IdIncident IN('$ids')";
            $consulta = str_replace("'","", $consulta);
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();        
       
           // print_r($consulta);
       
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
            
            }
            else{
                $data=0;
            }

            
          
    
     break;

     case 4:

        //OBTENER EL ID DEL INCIDENTE
        $consulta = "SELECT IdIncident FROM Incidents WHERE Responsible= '$responsable' AND Typeincident = '$tipoincidente' ";        
        $resultado = $conexion->prepare($consulta);                  
        $resultado->execute();    
        $resultado= $resultado->fetch();
        $incidenteid=$resultado['IdIncident'];

        // VALIDAR SI YA SE TIENE REGISTRADO ESE INCIDENTE

        $consulta = "SELECT IDRecord FROM Recordincidents WHERE TrkID= '$trkid' AND IncidentID= '$incidenteid'";        
        $resultado = $conexion->prepare($consulta);                  
        $resultado->execute();    
        $resultado= $resultado->fetch();
       
        
        // SI NO HAY REGISTROS CON ESE TRKID Y EL INCIDENTE SE AGREGA 
       if(empty($resultado)){
        $consulta = "INSERT INTO Recordincidents (TrkID,IncidentID) VALUES ('$trkid',$incidenteid);";    
        print_r($consulta);    
        $resultado = $conexion->prepare($consulta);                  
        $resultado->execute();
        $consulta = "UPDATE Truks SET Incident= '1' WHERE TrkID='$trkid'";
        $resultado = $conexion->prepare($consulta);                  
        $resultado->execute();    
        $data="OK";
       }
       
       else{
        $data=0;
       }


     break;

     case 5:

        $array = explode(",", $incidentes);
        $longitud = count($array);

        for($i=0; $i<$longitud; $i++){
            $array2 = explode("-", $array[$i]);
            $respon = $array2[0];
            $tipincident=$array2[1];

            $consulta = "SELECT IdIncident FROM Incidents WHERE Responsible= '$respon' AND Typeincident = '$tipincident';";        
            $resultado = $conexion->prepare($consulta);                  
            $resultado->execute();    
            $resultado= $resultado->fetch();
            $incidenteid=$resultado['IdIncident'];

            $consulta = "SELECT IDRecord FROM Recordincidents WHERE  TrkID = '$trkid' AND  IncidentID = '$incidenteid';";        
            $resultado = $conexion->prepare($consulta);                  
            $resultado->execute();    
            $resultado= $resultado->fetch();
            $idrecord=$resultado['IDRecord'];

            $consulta = "DELETE FROM Recordincidents WHERE IDRecord = '$idrecord';";
            $resultado = $conexion->prepare($consulta);                  
            $resultado->execute();    
            //$data="OK";          
         

        }
        
        $consulta = "SELECT IDRecord FROM Recordincidents WHERE TrkID= '$trkid' ";        
        $resultado = $conexion->prepare($consulta);                  
        $resultado->execute();    
        $datos= $resultado->fetch();

        if(empty($datos)){            
        $consulta = "UPDATE  Truks SET Incident= '0' WHERE TrkID='$trkid'";
        $resultado = $conexion->prepare($consulta);                  
        $resultado->execute();    
        }

        $data="OK"; 

        
    
     break;
    




   
}

print json_encode($data, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX
$conexion=null;
