<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
$myarray = [];
$fecha_hoy=date("Y-m-d");


date_default_timezone_set("America/Mexico_City");
// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill, Alignment};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//use PhpOffice\PhpSpreadsheet\Style\Alignment as alignment;
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
$fromdate = "2023-04-01";
$todate = date('Y-m-d');

$filterDOM = (!empty($_GET['filterDOM'])) ? $_GET['filterDOM'] : '';
$filterEXP = (!empty($_GET['filterEXP'])) ? $_GET['filterEXP'] : '';

$heading = false;
$complemeto="";

if($filterDOM == 1 && $filterEXP==""){
    $complemeto="AND  Typ ='DOM' ";
}

if($filterDOM == "" && $filterEXP==1){
    $complemeto="AND  Typ ='EXP' ";
    
}


if(($filterDOM == "" && $filterEXP=="") || ($filterDOM == 1 && $filterEXP==1)){
    $complemeto="";    
}




$consulta="SELECT Lots.TrkID,Lots.DOrd,Lots.Lot,Lots.Qty,Region.RegNam as RegionSalida,Gines.GinName as Despepite,Truks.OutDat AS FechaSalida,Truks.InDat AS Fechallegada,Truks.Status,Clients.BnName as Cliente,
Transports.BnName as Linea,Truks.DrvNam AS CHOFER,ROUND(((Truks.OutWgh/Truks.CrgQty)*Lots.Qty),2) as pesosalida,ROUND(((Truks.InWgh/Truks.CrgQty)*Lots.Qty),2) as pesollegada,Clients.Cli,
DOrds.Ctc AS Contrato,Truks.InvoiceComment,DOrds.Typ,Truks.TicketPeso as Ticket,
IF(((Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd))=99001,
(Select Town From amsadb1.Clients Where  Clients.CliID = (Select InPlc From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd)),
((Select Town From amsadb1.Region Where Region.IDReg =(Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd) ))) as CiudadLlegada,R1.RegNam as RegionLlegada
From amsadb1.Lots
LEFT JOIN amsadb1.Truks
ON Lots.TrkID = Truks.TrkID
LEFT JOIN amsadb1.DOrds
ON  Lots.DOrd = DOrds.DOrd
LEFT JOIN amsadb1.Clients
ON Clients.CliID = DOrds.InPlc
LEFT JOIN amsadb1.Gines
ON  Lots.GinID=Gines.IDGin 
LEFT JOIN amsadb1.Region
ON  DOrds.OutPlc=Region.IDReg 
LEFT JOIN amsadb1.Transports
ON Truks.TNam = Transports.TptID
LEFT JOIN amsadb1.Region R1
ON  DOrds.InReg=R1.IDReg 
WHERE (Truks.Invoice='' OR Truks.Invoice is null) AND DOrds.Typ !='CON' AND Truks.TNam !=137  AND  Truks.Status !='Cancelled' AND  ((SELECT TIMESTAMPDIFF(DAY, '$fecha_hoy',Truks.InDat) <=3) OR (Truks.Status='Received') )". $complemeto.  "ORDER BY Truks.OutDat,Lots.TrkID  ASC;";


$resultado = $conexion->prepare($consulta);
$resultado->execute();      


$siexiste=0; //$data=$resultado->fetchAll(PDO::FETCH_ASSOC);
$fileName = "InvoiceReport-".date('d-m-Y').".xlsx";
$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Invoices");
//$hojaActiva->freezePane("A2");



$hojaActiva->getColumnDimension('A')->setWidth(12);
$hojaActiva->setCellValue('A1','TruckID');
$hojaActiva->getColumnDimension('B')->setWidth(12);
$hojaActiva->setCellValue('B1','Type');
$hojaActiva->getColumnDimension('C')->setWidth(12);
$hojaActiva->setCellValue('C1','DO');
$hojaActiva->getColumnDimension('D')->setWidth(12);
$hojaActiva->setCellValue('D1','Lot');
$hojaActiva->getColumnDimension('E')->setWidth(12);
$hojaActiva->setCellValue('E1','Bales');
$hojaActiva->getColumnDimension('F')->setWidth(12);
$hojaActiva->setCellValue('F1','Departure Date');
$hojaActiva->getColumnDimension('G')->setWidth(12);
$hojaActiva->setCellValue('G1','Arrival Date');
$hojaActiva->getColumnDimension('H')->setWidth(12);
$hojaActiva->setCellValue('H1','Gin');
$hojaActiva->getColumnDimension('I')->setWidth(12);
$hojaActiva->setCellValue('I1','Departure Region');
$hojaActiva->getColumnDimension('J')->setWidth(12);
$hojaActiva->setCellValue('J1','Client');
$hojaActiva->getColumnDimension('K')->setWidth(12);
$hojaActiva->setCellValue('K1','Bussiness Name');
$hojaActiva->getColumnDimension('L')->setWidth(12);
$hojaActiva->setCellValue('L1','DO Contract');
$hojaActiva->getColumnDimension('M')->setWidth(12);
$hojaActiva->setCellValue('M1','Departure Weight');
$hojaActiva->getColumnDimension('N')->setWidth(12);
$hojaActiva->setCellValue('N1','Arrival Weight');
$hojaActiva->getColumnDimension('O')->setWidth(12);
$hojaActiva->setCellValue('O1','Ticket');
$hojaActiva->getColumnDimension('P')->setWidth(12);
$hojaActiva->setCellValue('P1','Status');
$hojaActiva->getColumnDimension('Q')->setWidth(12);
$hojaActiva->setCellValue('Q1',' Invoice');
$hojaActiva->getColumnDimension('R')->setWidth(12);
$hojaActiva->setCellValue('R1','Invoice Comment');



//ESTILO DE CELDA

//negritas en encabezado

$hojaActiva->getStyle('A1:R1')->getFont()->setBold( true );

//relleno de celda encabezado
$hojaActiva->getStyle('A1:R1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFE1E1E1');


//Inmovilizar encabezado
$hojaActiva->freezePane('A2');


$fila = 2;
$fecha_salida="";
$fecha_llegada="";
$ticket="";
$trkanterior=0;
while($row = $resultado->fetch(PDO::FETCH_ASSOC)){
    if($siexiste==0){
        if(!empty($row['TrkID']))
            $siexiste=1;
    }

    if($row['FechaSalida']!=""){
       // $fecha_salida = date("d/m/Y", ($row['OutDat']));


        $fecha_salida = strtotime($row['FechaSalida']);

        $fecha_salida =25569 + ($fecha_salida / 86400);
        
    }
    else{
        $fecha_salida = "";

    }

    if($row['Fechallegada']!=""){
       // $fecha_llegada = date("d/m/Y", ($row['InDat']));
        $fecha_llegada =strtotime($row['Fechallegada']);
        $fecha_llegada =25569 + ($fecha_llegada / 86400);
        
    }
    else{
        $fecha_llegada = "";

    }    

    if($trkanterior == $row['TrkID']){
    
        array_push($myarray,($fila-1));
        array_push($myarray,$fila);
    
   // $filaant = $fila-1
    
    /*
   
      $hojaActiva->getStyle('A' . $fila-1)->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFFFFF00');

        $hojaActiva->getStyle('A' . $fila)->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFFFFF00');
    */

 
    

    }

    if($row['Ticket'] == null || $row['Ticket'] == "" || $row['Ticket'] == 0){
        $ticket ="No";
    }
    else{
        $ticket ="Yes";
    }

    $hojaActiva->getStyle('F' . $fila)->getNumberFormat()//formato de fecha separado por /
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    $hojaActiva->getStyle('G' . $fila)->getNumberFormat()//formato de fecha separado por /
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    //date("d-m-Y", strtotime('2018-02-02'))
    

    $hojaActiva->setCellValue('A' . $fila,$row['TrkID']);
    $hojaActiva->setCellValue('B' . $fila,$row['Typ']);
    $hojaActiva->setCellValue('C' . $fila,$row['DOrd']);
   // $hojaActiva->setCellValue('D' . $fila,$row['Lot']);
    $hojaActiva->setCellValue('E' . $fila,$row['Qty']);
    $hojaActiva->setCellValue('F' . $fila,$fecha_salida);
    $hojaActiva->setCellValue('G' . $fila,$fecha_llegada);
    $hojaActiva->setCellValue('H' . $fila,$row['Despepite']);
    $hojaActiva->setCellValue('I'.  $fila,$row['RegionSalida']);
    $hojaActiva->setCellValue('J'.  $fila,$row['Cli']);
    $hojaActiva->setCellValue('K'.  $fila,$row['Cliente']);
    $hojaActiva->setCellValue('L'.  $fila,$row['Contrato']);
    $hojaActiva->setCellValue('M'.  $fila,$row['pesosalida']);
    $hojaActiva->setCellValue('N'.  $fila,$row['pesollegada']);
    $hojaActiva->setCellValue('O'.  $fila,$ticket);
    $hojaActiva->setCellValue('P'.  $fila,$row['Status']);   
    $hojaActiva->setCellValue('R'.  $fila,$row['InvoiceComment']); 
    
    
      //quitar funcion exponencial en excel para lotes americanos 23E02 
    
    $style = $hojaActiva->getStyle('D'. $fila);

    $style->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
    
    $hojaActiva->getCell('D'. $fila)->setValueExplicit($row['Lot'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);

    $trkanterior = $row['TrkID'];
    $fila++;
}



$longitud = count($myarray);
for($i=0; $i<$longitud; $i++){

    $hojaActiva->getStyle('A' . $myarray[$i])->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFFFFF00');

}

$hojaActiva->getStyle('M2:M'.$fila)->getNumberFormat()->setFormatCode('###0.00');
$hojaActiva->getStyle('N2:N'.$fila)->getNumberFormat()->setFormatCode('###0.00');


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');
ob_end_clean();
$writer = IOFactory::createWriter($excel, 'Xlsx');
$writer->save('php://output');
exit;

?>