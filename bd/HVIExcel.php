<?php 
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set("America/Mexico_City");

function lowercase($element)
{
    return "'".$element."'";
}

$DO = $_GET['DO'];
$Lots = $_GET['Lots'];
$Crp = $_GET['Crp'];
$TypHVI = $_GET['TypHVI'];
$Cli = $_GET['Cli'];

//Si la busqueda es por DO
if ($DO != ""){
    $consulta = 'SELECT distinct Bales.Lot FROM Bales, DOrds, Lots WHERE DOrds.DOrd = '.$DO.' and DOrds.DOrd = Lots.DOrd and Bales.Lot = Lots.Lot;'; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataDO=$resultado->fetchALL(PDO::FETCH_ASSOC);
}else{
    //$array = array_map('lowercase', explode(',', $Lots));
    $array = explode(",",$Lots); //$array = implode(",",$array);
}

// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

//Query Lotes
/*$query= "SELECT Bal, Col1, Tgrd, Mic, Len, Str, Unf, Rd, b, Tar, Col2, Tcnt, Sfi, Elg, Mat, Mst, Lot FROM Bales WHERE Lot = '230008';";


$result = $conexion->prepare($query);
$result->execute();
$siexiste=0; //Para verificar que hayan datos

//promedios Lotes
$consulta = "SELECT count(Bal) as SumBal, avg(Mic) as avgMic, avg(Tgrd) as avgTgrd, avg(Tar) as avgTar, avg(Tcnt) as avgTcnt, avg(Unf) as avgUnf, avg(Len) as avgLen, avg(Sfi) as avgSfi, avg(Str) as avgStr, avg(Elg) as avgElg, avg(Mat) as avgMat, avg(Rd) as avgRd, avg(b) as avgb FROM Bales WHERE Lot = '230008'"; 
$resultado = $conexion->prepare($consulta);
$resultado->execute();*/


//Define the filename with current date
$fileName = "HVI ".$Cli." ".date('d-M-Y').".xlsx";

$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Hoja 1");
//$hojaActiva->freezePane("A2");




$hojaActiva->getColumnDimension('A')->setWidth(8);
$hojaActiva->setCellValue('A1','Lot');
$hojaActiva->getColumnDimension('B')->setWidth(9);
$hojaActiva->setCellValue('B1','Bales');
$hojaActiva->getColumnDimension('C')->setWidth(7);
$hojaActiva->setCellValue('C1','Grd');
$hojaActiva->getColumnDimension('D')->setWidth(7);
$hojaActiva->setCellValue('D1','Trs');
$hojaActiva->getColumnDimension('E')->setWidth(7);
$hojaActiva->setCellValue('E1','Mic');
$hojaActiva->getColumnDimension('F')->setWidth(7);
$hojaActiva->setCellValue('F1','Len');
$hojaActiva->getColumnDimension('G')->setWidth(7);
$hojaActiva->setCellValue('G1','Str');
$hojaActiva->getColumnDimension('H')->setWidth(7);
$hojaActiva->setCellValue('H1','Unf');
$hojaActiva->getColumnDimension('I')->setWidth(7);
$hojaActiva->setCellValue('I1','Rd');
$hojaActiva->getColumnDimension('J')->setWidth(7);
$hojaActiva->setCellValue('J1','b');
$hojaActiva->getColumnDimension('K')->setWidth(7);
$hojaActiva->setCellValue('K1','Col');
$hojaActiva->getColumnDimension('L')->setWidth(7);
$hojaActiva->setCellValue('L1','Tar');
$hojaActiva->getColumnDimension('M')->setWidth(7);
$hojaActiva->setCellValue('M1','Tcnt');
$hojaActiva->getColumnDimension('N')->setWidth(7);
$hojaActiva->setCellValue('N1','Sfi');
$hojaActiva->getColumnDimension('O')->setWidth(7);
$hojaActiva->setCellValue('O1','Elg');
$hojaActiva->getColumnDimension('P')->setWidth(7);
$hojaActiva->setCellValue('P1','Mat');
$hojaActiva->getColumnDimension('Q')->setWidth(7);
$hojaActiva->setCellValue('Q1','Mst');

$fila = 2;

$today = new DateTime();//DateTime("2022-04-06"); //echo $today->format('Y-m-d');

if ($DO == ""){
    foreach ($array as $dato){

        if ($TypHVI == 'O'){
            $query= "SELECT Bal, Col1, Tgrd, Mic, Len, Str, Unf, Rd, b, Tar, Col2, Tcnt, Sfi, Elg, Mat, Mst, Lot FROM Bales WHERE Lot = '$dato' ORDER BY Bal ASC;";

            $result = $conexion->prepare($query);
            $result->execute();
            $siexiste=0; //Para verificar que hayan datos

            //promedios Lotes
            $consulta = "SELECT count(Bal) as SumBal, avg(Mic) as avgMic, avg(Tgrd) as avgTgrd, avg(Tar) as avgTar, avg(Tcnt) as avgTcnt, avg(Unf) as avgUnf, avg(Len) as avgLen, avg(Sfi) as avgSfi, avg(Str) as avgStr, avg(Elg) as avgElg, avg(Mat) as avgMat, avg(Rd) as avgRd, avg(b) as avgb FROM Bales WHERE Lot = '$dato';"; 
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
        }else{
            $query= "SELECT Bal, Col1_Mod as Col1, Tgrd_Mod as Tgrd, Mic_Mod as Mic, Len_Mod as Len, Str_Mod as Str, Unf_Mod as Unf, Rd_Mod as Rd, b_Mod as b, Tar_Mod as Tar, Col2_Mod as Col2, Tcnt_Mod as Tcnt, Sfi_Mod as Sfi, Elg_Mod as Elg, Mat_Mod as Mat, Mst_Mod as Mst, Lot FROM Bales WHERE Lot = '$dato'  ORDER BY Bal ASC;";

            $result = $conexion->prepare($query);
            $result->execute();
            $siexiste=0; //Para verificar que hayan datos

            //promedios Lotes
            $consulta = "SELECT count(Bal) as SumBal, avg(Mic_Mod) as avgMic, avg(Tgrd_Mod) as avgTgrd, avg(Tar_Mod) as avgTar, avg(Tcnt_Mod) as avgTcnt, avg(Unf_Mod) as avgUnf, avg(Len_Mod) as avgLen, avg(Sfi_Mod) as avgSfi, avg(Str_Mod) as avgStr, avg(Elg_Mod) as avgElg, avg(Mat_Mod) as avgMat, avg(Rd_Mod) as avgRd, avg(b_Mod) as avgb FROM Bales WHERE Lot = '$dato';"; 
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
        }


    while($row = $result->fetch(PDO::FETCH_ASSOC)){
    
        $style = $hojaActiva->getStyle('A'. $fila);
        $style->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
        $hojaActiva->getCell('A'. $fila)->setValueExplicit($row['Lot'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);


       // $hojaActiva->setCellValue('A' . $fila,$row['Lot']);
        $hojaActiva->setCellValue('B' . $fila,$row['Bal']);
        $hojaActiva->setCellValue('C' . $fila,$row['Col1']);
        $hojaActiva->setCellValue('D' . $fila,$row['Tgrd']);
        $hojaActiva->setCellValue('E' . $fila,number_format(Round($row['Mic'],1),1));
        $hojaActiva->setCellValue('F' . $fila,number_format(Round($row['Len'],2),2));
        $hojaActiva->setCellValue('G'. $fila,number_format(Round($row['Str'],1),1));
        $hojaActiva->setCellValue('H'. $fila,number_format(Round($row['Unf'],1),1));
        $hojaActiva->setCellValue('I'. $fila,number_format(Round($row['Rd'],1),1));
        $hojaActiva->setCellValue('J'. $fila,number_format(Round($row['b'],1),1));
        $hojaActiva->setCellValue('K'. $fila,$row['Col1']."-".$row['Col2']);
        $hojaActiva->setCellValue('L'. $fila,number_format(Round($row['Tar'],2),2));
        $hojaActiva->setCellValue('M'. $fila,$row['Tcnt']);
        $hojaActiva->setCellValue('N'. $fila,number_format(Round($row['Sfi'],1),1));
        $hojaActiva->setCellValue('O'. $fila,number_format(Round($row['Elg'],1),1));
        $hojaActiva->setCellValue('P'. $fila,number_format(Round($row['Mat'],2),2));
        $hojaActiva->setCellValue('Q' . $fila,number_format(Round($row['Mst'],1),1));

        $fila++;
    }


    while($row = $resultado->fetch(PDO::FETCH_ASSOC)){
        $hojaActiva->setCellValue('B' . $fila,$row['SumBal']);
        $hojaActiva->setCellValue('D' . $fila,Round($row['avgTgrd'],0));
        $hojaActiva->setCellValue('E' . $fila,number_format(Round($row['avgMic'],2),2));
        $hojaActiva->setCellValue('F' . $fila,number_format(Round($row['avgLen'],2),2));
        $hojaActiva->setCellValue('G'. $fila,number_format(Round($row['avgStr'],2),2));
        $hojaActiva->setCellValue('H'. $fila,number_format(Round($row['avgUnf'],2),2));
        $hojaActiva->setCellValue('I'. $fila,number_format(Round($row['avgRd'],2),2));
        $hojaActiva->setCellValue('J'. $fila,number_format(Round($row['avgb'],2),2));
        $hojaActiva->setCellValue('L'. $fila,number_format(Round($row['avgTar'],2),2));
        $hojaActiva->setCellValue('M'. $fila,number_format(Round($row['avgTcnt'],2),2));
        $hojaActiva->setCellValue('N'. $fila,number_format(Round($row['avgSfi'],2),2));
        $hojaActiva->setCellValue('O'. $fila,number_format(Round($row['avgElg'],2),2));
        $hojaActiva->setCellValue('P'. $fila,number_format(Round($row['avgMat'],2),2));
        
        $fila++;
    }


    $hojaActiva->getStyle('G2:J'.$fila)->getNumberFormat()->setFormatCode('#,##0.0');
    $hojaActiva->getStyle('N2:O'.$fila)->getNumberFormat()->setFormatCode('#,##0.0');
    $hojaActiva->getStyle('Q2:Q'.$fila)->getNumberFormat()->setFormatCode('#,##0.0');

    $hojaActiva->getStyle('F2:F'.$fila)->getNumberFormat()->setFormatCode('#,##0.00');
    $hojaActiva->getStyle('P2:P'.$fila)->getNumberFormat()->setFormatCode('#,##0.00');

    }
}else{
    foreach ($dataDO as $dato){

        if ($TypHVI == 'O'){
            $query= 'SELECT Bal, Col1, Tgrd, Mic, Len, Str, Unf, Rd, b, Tar, Col2, Tcnt, Sfi, Elg, Mat, Mst, Lot FROM Bales WHERE Lot = '.$dato['Lot'].'  ORDER BY Bal ASC;';

            $result = $conexion->prepare($query);
            $result->execute();
            $siexiste=0; //Para verificar que hayan datos

            //promedios Lotes
            $consulta = 'SELECT count(Bal) as SumBal, avg(Mic) as avgMic, avg(Tgrd) as avgTgrd, avg(Tar) as avgTar, avg(Tcnt) as avgTcnt, avg(Unf) as avgUnf, avg(Len) as avgLen, avg(Sfi) as avgSfi, avg(Str) as avgStr, avg(Elg) as avgElg, avg(Mat) as avgMat, avg(Rd) as avgRd, avg(b) as avgb FROM Bales WHERE Lot = '.$dato['Lot'].';'; 
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
        }else{
            $query= 'SELECT Bal, Col1_Mod as Col1, Tgrd_Mod as Tgrd, Mic_Mod as Mic, Len_Mod as Len, Str_Mod as Str, Unf_Mod as Unf, Rd_Mod as Rd, b_Mod as b, Tar_Mod as Tar, Col2_Mod as Col2, Tcnt_Mod as Tcnt, Sfi_Mod as Sfi, Elg_Mod as Elg, Mat_Mod as Mat, Mst_Mod as Mst, Lot FROM Bales WHERE Lot = '.$dato['Lot'].'  ORDER BY Bal ASC;';

            $result = $conexion->prepare($query);
            $result->execute();
            $siexiste=0; //Para verificar que hayan datos

            //promedios Lotes
            $consulta = 'SELECT count(Bal) as SumBal, avg(Mic_Mod) as avgMic, avg(Tgrd_Mod) as avgTgrd, avg(Tar_Mod) as avgTar, avg(Tcnt_Mod) as avgTcnt, avg(Unf_Mod) as avgUnf, avg(Len_Mod) as avgLen, avg(Sfi_Mod) as avgSfi, avg(Str_Mod) as avgStr, avg(Elg_Mod) as avgElg, avg(Mat_Mod) as avgMat, avg(Rd_Mod) as avgRd, avg(b_Mod) as avgb FROM Bales WHERE Lot = '.$dato['Lot'].';'; 
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
        }


    while($row = $result->fetch(PDO::FETCH_ASSOC)){
    
        $style = $hojaActiva->getStyle('A'. $fila);
        $style->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
        $hojaActiva->getCell('A'. $fila)->setValueExplicit($row['Lot'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);


        //$hojaActiva->setCellValue('A' . $fila,$row['Lot']);
        $hojaActiva->setCellValue('B' . $fila,$row['Bal']);
        $hojaActiva->setCellValue('C' . $fila,$row['Col1']);
        $hojaActiva->setCellValue('D' . $fila,$row['Tgrd']);
        $hojaActiva->setCellValue('E' . $fila,number_format(Round($row['Mic'],1),1));
        $hojaActiva->setCellValue('F' . $fila,number_format(Round($row['Len'],2),2));
        $hojaActiva->setCellValue('G'. $fila,number_format(Round($row['Str'],1),1));
        $hojaActiva->setCellValue('H'. $fila,number_format(Round($row['Unf'],1),1));
        $hojaActiva->setCellValue('I'. $fila,number_format(Round($row['Rd'],1),1));
        $hojaActiva->setCellValue('J'. $fila,number_format(Round($row['b'],1),1));
        $hojaActiva->setCellValue('K'. $fila,$row['Col1']."-".$row['Col2']);
        $hojaActiva->setCellValue('L'. $fila,number_format(Round($row['Tar'],2),2));
        $hojaActiva->setCellValue('M'. $fila,$row['Tcnt']);
        $hojaActiva->setCellValue('N'. $fila,number_format(Round($row['Sfi'],1),1));
        $hojaActiva->setCellValue('O'. $fila,number_format(Round($row['Elg'],1),1));
        $hojaActiva->setCellValue('P'. $fila,number_format(Round($row['Mat'],2),2));
        $hojaActiva->setCellValue('Q' . $fila,number_format(Round($row['Mst'],1),1));

        $fila++;
    }


    while($row = $resultado->fetch(PDO::FETCH_ASSOC)){
        $hojaActiva->setCellValue('B' . $fila,$row['SumBal']);
        $hojaActiva->setCellValue('D' . $fila,Round($row['avgTgrd'],0));
        $hojaActiva->setCellValue('E' . $fila,number_format(Round($row['avgMic'],2),2));
        $hojaActiva->setCellValue('F' . $fila,number_format(Round($row['avgLen'],2),2));
        $hojaActiva->setCellValue('G'. $fila,number_format(Round($row['avgStr'],2),2));
        $hojaActiva->setCellValue('H'. $fila,number_format(Round($row['avgUnf'],2),2));
        $hojaActiva->setCellValue('I'. $fila,number_format(Round($row['avgRd'],2),2));
        $hojaActiva->setCellValue('J'. $fila,number_format(Round($row['avgb'],2),2));
        $hojaActiva->setCellValue('L'. $fila,number_format(Round($row['avgTar'],2),2));
        $hojaActiva->setCellValue('M'. $fila,number_format(Round($row['avgTcnt'],2),2));
        $hojaActiva->setCellValue('N'. $fila,number_format(Round($row['avgSfi'],2),2));
        $hojaActiva->setCellValue('O'. $fila,number_format(Round($row['avgElg'],2),2));
        $hojaActiva->setCellValue('P'. $fila,number_format(Round($row['avgMat'],2),2));
        
        $fila++;
    }


    $hojaActiva->getStyle('G2:J'.$fila)->getNumberFormat()->setFormatCode('#,##0.0');
    $hojaActiva->getStyle('N2:O'.$fila)->getNumberFormat()->setFormatCode('#,##0.0');
    $hojaActiva->getStyle('Q2:Q'.$fila)->getNumberFormat()->setFormatCode('#,##0.0');

    $hojaActiva->getStyle('F2:F'.$fila)->getNumberFormat()->setFormatCode('#,##0.00');
    $hojaActiva->getStyle('P2:P'.$fila)->getNumberFormat()->setFormatCode('#,##0.00');

    }
}

// redirect output to client browser
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');

$writer = IOFactory::createWriter($excel, 'Xlsx');
$writer->save('php://output');
exit;

?>