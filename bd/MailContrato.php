<?php
require '../vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

require '../bd/vendor/phpmailer/phpmailer/src/PHPMailer.php';
require '../bd/vendor/phpmailer/phpmailer/src/SMTP.php';
require 'vendor/autoload.php';


$asunto= (isset($_POST['asunto'])) ? $_POST['asunto'] : '';
$solicita = (isset($_POST['solicita'])) ? $_POST['solicita'] : '';
$noSolicitud = (isset($_POST['noSolicitud'])) ? $_POST['noSolicitud'] : '';
$fecha = (isset($_POST['fecha'])) ? $_POST['fecha'] : '';
$cliente = (isset($_POST['cliente'])) ? $_POST['cliente'] : '';
$subCliente = (isset($_POST['subCliente'])) ? $_POST['subCliente'] : '';
$lugarEntrega = (isset($_POST['lugarEntrega'])) ? $_POST['lugarEntrega'] : '';
$de = (isset($_POST['de'])) ? $_POST['de'] : '';
$al = (isset($_POST['al'])) ? $_POST['al'] : '';
$incoterm = (isset($_POST['incoterm'])) ? $_POST['incoterm'] : '';
$cantidad = (isset($_POST['cantidad'])) ? $_POST['cantidad'] : '';
$cosecha = (isset($_POST['cosecha'])) ? $_POST['cosecha'] : '';
$mesCobertura = (isset($_POST['mesCobertura'])) ? $_POST['mesCobertura'] : '';
$region = (isset($_POST['region'])) ? $_POST['region'] : '';
$destino = (isset($_POST['destino'])) ? $_POST['destino'] : '';
$grado = (isset($_POST['grado'])) ? $_POST['grado'] : '';
$fibra = (isset($_POST['fibra'])) ? $_POST['fibra'] : '';
$micronara = (isset($_POST['micronara'])) ? $_POST['micronara'] : '';
$resistencia = (isset($_POST['resistencia'])) ? $_POST['resistencia'] : '';
$uniformidad = (isset($_POST['uniformidad'])) ? $_POST['uniformidad'] : '';
$fijoCall = (isset($_POST['fijoCall'])) ? $_POST['fijoCall'] : '';
$precioNY = (isset($_POST['precioNY'])) ? $_POST['precioNY'] : '';
$puntos = (isset($_POST['puntos'])) ? $_POST['puntos'] : '';
$precioFinal = (isset($_POST['precioFinal'])) ? $_POST['precioFinal'] : '';
$uMedida = (isset($_POST['uMedida'])) ? $_POST['uMedida'] : '';
$condPago = (isset($_POST['condPago'])) ? $_POST['condPago'] : '';
$intNormal = (isset($_POST['intNormal'])) ? $_POST['intNormal'] : '';
$intMoral = (isset($_POST['intMoral'])) ? $_POST['intMoral'] : '';
$contStatus = (isset($_POST['contStatus'])) ? $_POST['contStatus'] : '';
$comentario = (isset($_POST['comentario'])) ? $_POST['comentario'] : '';
$nota = (isset($_POST['nota'])) ? $_POST['nota'] : '';
$aprueba = (isset($_POST['aprueba'])) ? $_POST['aprueba'] : '';

//$token = uniqid('', true); 
$confirmarContrato = "http://localhost/portal/confirmContract.php?noSolicitud=$noSolicitud";

$email="marisol.hernandez@ecomtrading.com";
$pass ="bgxbbgiyobochcsc";
$usuario ="Marisol Hernandez";


 // Intancia de PHPMailer
 $mail = new PHPMailer();
 // Es necesario para poder usar un servidor SMTP como gmail
 $mail->isSMTP();
 // Si estamos en desarrollo podemos utilizar esta propiedad para ver mensajes de error
 $mail->SMTPDebug     = 0; //SMTP::DEBUG_SERVER;
 //Set the hostname of the mail server
 $mail->Host = 'smtp.gmail.com';
 $mail->Port = 465; // 465 o 587
 // Propiedad para establecer la seguridad de encripción de la comunicación
 $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS; // tls o ssl para gmail obligado
 // Para activar la autenticación smtp del servidor
 $mail->SMTPAuth = true;
 // Credenciales de la cuenta
 $mail->Username = $email;
 $mail->Password = $pass;
 // Quien envía este mensaje
 $mail->setFrom($email, $usuario);
 // Si queremos una dirección de respuesta
 //$mail->addReplyTo('replyto@panchos.com', 'Pancho Doe');
 // Destinatario
 $correos = "marisol.hernandez@ecomtrading.com"; //,josue.reyes@ecomtrading.com
// $arraycorreo = explode(",",$correos);
/* $tam = sizeof($arraycorreo);
for($i=0; $i<$tam; $i++){
  $mail->addAddress($arraycorreo[$i]);
}  */
$mail->addAddress($correos);
//$mail->addAddress("martin.romero@ecomtrading.com");

// Asunto del correo
$mail->Subject = $asunto;

// Contenido
$mail->IsHTML(true);
$mail->CharSet = 'UTF-8';

$bodyy ='
<div style="text-align: center;">
    <b style="color: #17562c; font-size: 18px;">Agroindustrias Unidas de México SA de CV</b><br>
    <b style="color: black; font-size: 16px;">
      Algodón México<br>
      Solicitud de aprobación de venta pacas de algodón
    </b>
</div>

<div style="border: 2px solid #fff; padding: 5px;  max-width: 900px; margin: 0 auto;">
  <p>
    <b style="color: #17562c; font-size: 15px;">Solicita:</b> '.$solicita.'
    <span style="float: right;">
      <b style="color: #17562c; font-size: 15px;">Fecha de solicitud:</b> '.$fecha.'<br>
      <b style="color: #17562c; font-size: 15px;">Número de solicitud:</b> '.$noSolicitud.'<br><br>
      <b style="color: #17562c; font-size: 15px;">Lugar de entrega:</b> '.$lugarEntrega.'<br>
      <b style="color: #17562c; font-size: 15px;">Periodo de entrega de:</b> '.$de.' 
      <b style="color: #17562c; font-size: 15px;">al:</b> '.$al.'
    </span>
  </p>
  <p>
    <b style="color: #17562c; font-size: 15px;">Cliente:</b> '.$cliente.'<br>
    <b style="color: #17562c; font-size: 15px;">Sub Cliente:</b> '.$subCliente.'<br>
    <b style="color: #17562c; font-size: 15px;">Incoterm:</b> '.$incoterm.'<br>
  </p>

  <table border="1" width="28%" cellpadding="1" cellspacing="0">
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Cantidad</td>
      <td>'.$cantidad.' Pacas</td>
    </tr>
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Cosecha</td>
      <td>'.$cosecha.'</td>
    </tr>
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Periodo de cobertura</td>
      <td>'.$mesCobertura.'</td>
    </tr>
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Región origen</td>
      <td>'.$region.'</td>
    </tr>
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Destino</td>
      <td>'.$destino.'</td>
    </tr>
  </table>
  <br>

  <table border="1" width="28%" cellpadding="1" cellspacing="0">
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Grado</td>
      <td>'.$grado.' SLM</td>
    </tr>
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Fibra</td>
      <td>'.$fibra.'</td>
    </tr>
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Micronare</td>
      <td>'.$micronara.'</td>
    </tr>
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Resistencia</td>
      <td>'.$resistencia.'</td>
    </tr>
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Uniformidad</td>
      <td>'.$uniformidad.'</td>
    </tr>
  </table>
  <br>

  <table border="1" width="28%" cellpadding="1" cellspacing="0">
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Fijo/ On call</td>
      <td>'.$fijoCall.'</td>
    </tr>
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Precio base NY</td>
      <td>'.$precioNY.'</td>
    </tr>
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Puntos</td>
      <td>'.$puntos.'</td>
    </tr>
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Precio final</td>
      <td>'.$precioFinal.'</td>
    </tr>
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Unidad de Medida</td>
      <td>'.$uMedida.'</td>
    </tr>
  </table>
  <br>

  <table border="1" width="28%" cellpadding="1" cellspacing="0">
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Condiciones de pago</td>
      <td>'.$condPago.'</td>
    </tr>
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Intereses normal</td>
      <td>'.$intNormal.'% anual</td>
    </tr>
    <tr>
      <td bgcolor="#17562c" style="color: white; font-weight: bold;">Intereses moral</td>
      <td>'.$intMoral.'% anual</td>
    </tr>
  </table>
  <br>

</div>
<br>
Para confirmar la aprobación del contrato, haga clic en el siguiente enlace: 
<a href="' . $confirmarContrato . '">CONFIRMAR CONTRATO</a>
';

/* <table border="1" width=40%" cellpadding="1" cellspacing="0">
<tr>
  <td bgcolor="#17562c" style="color: white; font-weight: bold;">Nota</td>
  <td>'.$nota.'</td>
</tr>
<tr>
  <td bgcolor="#17562c" style="color: white; font-weight: bold;">Aprueba</td>
  <td>'.$aprueba.'</td>
</tr>
</table> */
$mail->Body = $bodyy;

if ($mail->send()) {
  echo 'Correo enviado';
} else {
  echo 'Error al enviar el correo: ' . $mail->ErrorInfo;
}
?>
