<?php 
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set("America/Mexico_City");

function lowercase($element)
{
    return "'".$element."'";
}

$DO = $_GET['DOCMS'];
$LotsCMS = $_GET['LotsCMS'];
$DateCMS = $_GET['DateCMS'];

$newDate = date("d/m/Y", strtotime($DateCMS));

//$array = array_map('lowercase', explode(',', $LotsCMS));
//$array = implode(",",$array);

//Si la busqueda es por DO
if ($DO != ""){
    $Lots = array();
    $i = 0;
    $consulta = 'SELECT distinct Bales.Lot FROM Bales, DOrds, Lots WHERE DOrds.DOrd = '.$DO.' and DOrds.DOrd = Lots.DOrd and Bales.Lot = Lots.Lot;'; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataDO=$resultado->fetchALL(PDO::FETCH_ASSOC);
    foreach($dataDO as $dat){
        $Lots[$i] = $dat['Lot'];
        $i++;
    }
    $array = array_map('lowercase',$Lots);
    $array = implode(",",$array);
}else{
    $array = array_map('lowercase', explode(',', $LotsCMS));
    $array = implode(",",$array);
}

// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Csv;


//Define the filename with current date
$fileName = "SubeLotes_".date('d-M-Y').".csv";

$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Hoja 1");
//$hojaActiva->freezePane("A2");




/*$hojaActiva->getColumnDimension('A')->setWidth(8);
$hojaActiva->setCellValue('A1','Crop Year');
$hojaActiva->getColumnDimension('B')->setWidth(9);
$hojaActiva->setCellValue('B1','Gin ID');
$hojaActiva->getColumnDimension('C')->setWidth(7);
$hojaActiva->setCellValue('C1','Lot Number');
$hojaActiva->getColumnDimension('D')->setWidth(7);
$hojaActiva->setCellValue('D1','Quantity');
$hojaActiva->getColumnDimension('E')->setWidth(7);
$hojaActiva->setCellValue('E1','Quantity Unit');
$hojaActiva->getColumnDimension('F')->setWidth(7);
$hojaActiva->setCellValue('F1','Net Weight');
$hojaActiva->getColumnDimension('G')->setWidth(7);
$hojaActiva->setCellValue('G1','Weight Unit');
$hojaActiva->getColumnDimension('H')->setWidth(7);
$hojaActiva->setCellValue('H1','Marks');
$hojaActiva->getColumnDimension('I')->setWidth(7);
$hojaActiva->setCellValue('I1','Commodity Code');
$hojaActiva->getColumnDimension('J')->setWidth(7);
$hojaActiva->setCellValue('J1','Local Code');
$hojaActiva->getColumnDimension('K')->setWidth(7);
$hojaActiva->setCellValue('K1','Origin');
$hojaActiva->getColumnDimension('L')->setWidth(7);
$hojaActiva->setCellValue('L1','Category');
$hojaActiva->getColumnDimension('M')->setWidth(7);
$hojaActiva->setCellValue('M1','Ecoms Purchase No.');
$hojaActiva->getColumnDimension('N')->setWidth(7);
$hojaActiva->setCellValue('N1','Acquisition Date');
$hojaActiva->getColumnDimension('O')->setWidth(7);
$hojaActiva->setCellValue('O1','Production Date');
$hojaActiva->getColumnDimension('P')->setWidth(7);
$hojaActiva->setCellValue('P1','Gross Weight');
$hojaActiva->getColumnDimension('Q')->setWidth(7);
$hojaActiva->setCellValue('Q1','Shipped/Landen');
$hojaActiva->getColumnDimension('R')->setWidth(7);
$hojaActiva->setCellValue('R1','Warehouse Code');
$hojaActiva->getColumnDimension('S')->setWidth(7);
$hojaActiva->setCellValue('S1','Date of Storage');
$hojaActiva->getColumnDimension('T')->setWidth(7);
$hojaActiva->setCellValue('T1','Dispach Date');
$hojaActiva->getColumnDimension('U')->setWidth(7);
$hojaActiva->setCellValue('U1','Acquisition');*/

$fila = 1;

$today = new DateTime();//DateTime("2022-04-06"); //echo $today->format('Y-m-d');

$query= 'SELECT Crp, Gines.IDReg, Gines.CMSGin, Lot, Count(Bal) as Qty, CEILING(Sum(Wgh)) as Wgh, (Sum(if(LiqID!="",1,0))) as LiqBal 
         FROM Bales
         join Gines on Gines.IDGin = Bales.GinID
         WHERE Bales.Lot IN ('.$array.')
         Group by Lot;';

$result = $conexion->prepare($query);
$result->execute();
$siexiste=0; //Para verificar que hayan datos


while($row = $result->fetch(PDO::FETCH_ASSOC)){

    if ($row['LiqBal'] == $row['Qty']){

        $hojaActiva->setCellValue('A' . $fila,$row['Crp']);
        $hojaActiva->setCellValue('B' . $fila,$row['CMSGin']);
        $hojaActiva->setCellValue('C' . $fila,$row['Lot']);
        $hojaActiva->setCellValue('D' . $fila,$row['Qty']);
        $hojaActiva->setCellValue('E' . $fila,"BC");
        $hojaActiva->setCellValue('F' . $fila,$row['Wgh']);
        $hojaActiva->setCellValue('G'. $fila,"KGS");
        $hojaActiva->setCellValue('H'. $fila,$row['Lot']);
        $hojaActiva->setCellValue('I'. $fila,"31");
        $hojaActiva->setCellValue('J'. $fila,$row['IDReg']);
        $hojaActiva->setCellValue('K'. $fila,"");
        $hojaActiva->setCellValue('L'. $fila,"");
        $hojaActiva->setCellValue('M'. $fila,"");
        $hojaActiva->setCellValue('N'. $fila,$newDate);
        $hojaActiva->setCellValue('O'. $fila,"");
        $hojaActiva->setCellValue('P'. $fila,$row['Wgh']);
        $hojaActiva->setCellValue('Q' . $fila,$row['Wgh']);
        $hojaActiva->setCellValue('R' . $fila,$row['CMSGin']);
        $hojaActiva->setCellValue('S' . $fila,$newDate);
        $hojaActiva->setCellValue('T' . $fila,"");
        $hojaActiva->setCellValue('U' . $fila,"Yes");

        $fila++;
    
    }
}


// redirect output to client browser
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');

$writer = IOFactory::createWriter($excel, 'Csv')->setEnclosure(false);
$writer->setUseBOM(false);
$writer->setLineEnding("\r\n");
//$writer->setOutputEncoding('UTF-8');
$writer->save('php://output');

exit;

?>