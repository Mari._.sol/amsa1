<?php

setlocale(LC_TIME, "spanish");

function lowercase($element)
{
    return "'".$element."'";
}

$DO = $_GET['DO'];
$Lots = $_GET['Lots'];
$Crp = $_GET['Crp'];
$TypHVI = $_GET['TypHVI'];
$Cli = $_GET['Cli'];

include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

//Si la busqueda es por DO
if ($DO != ""){
    $Lots = array();
    $i = 0;
    $consulta = 'SELECT distinct Bales.Lot FROM Bales, DOrds, Lots WHERE DOrds.DOrd = '.$DO.' and DOrds.DOrd = Lots.DOrd and Bales.Lot = Lots.Lot;'; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataDO=$resultado->fetchALL(PDO::FETCH_ASSOC);
    foreach($dataDO as $dat){
        $Lots[$i] = $dat['Lot'];
        $i++;
    }
    $array = array_map('lowercase',$Lots);
    $array = implode(",",$array);
}else{
    $array = array_map('lowercase', explode(',', $Lots));
    $array = implode(",",$array);
}


include_once '../fpdf/fpdf.php';

class PDF extends FPDF
{
   //Cabecera de página
   function Header()
   {
    $this->SetFont('Arial','B',10);
    $this->Image('../img/logo1.png', 15, 8, 13, 13, 'PNG');
    $this->SetTextColor(1,100,46);
    $this->Cell(0,6, utf8_decode('AGROINDUSTRIAS UNIDAS DE MEXICO S.A. DE C.V.'),0,0,'C');
    $this->Ln(4);
    $this->SetTextColor(0,0,0);
    $this->SetFont('Arial','B',9);
    $this->Cell(0,7,'COTTON DIVISION', 0, 0,'C');
    $this->Ln(4);
    $this->SetFont('Arial','B',9);
    $this->SetLineWidth(0.6);
    $this->Cell(0,5,date('d/m/Y'), 'B', 0,'R');
    //$this->Ln();
    //$this->Cell(0,6, utf8_decode(''),0,0,'C');
    //$this->Ln();
   }

   //Pie de página
   function Footer()
   {
       // Position at 1.5 cm from bottom
       $this->SetY(-15);
       // Arial italic 8
       $this->SetFont('Arial','I',8);
       // Page number
       $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
   }
}


$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('Landscape','Letter');
$pdf->SetTitle("Recap ".$Cli);
$pdf->Ln();
if ($DO == ""){
    $pdf->Cell(0,5, utf8_decode('Lots: '.$Lots),0,0,'L');
}else{
    $Lots = implode( ',', $Lots);
    $pdf->Cell(0,5, utf8_decode('Lots: '.$Lots),0,0,'L');
}$pdf->Ln();
$pdf->Cell(0,2, utf8_decode(''),0,0,'C');
$pdf->Ln();

//Arreglo con los valores definidos en Recap
$TCol = array('11','12','13','21','22','23','24','25','31','32','33','34','35','41','42','43','44','51','52','53','54','61','62','63','71');
$TTrs = array('1','2','3','4','5','6','7','','','','','','','','','','','','','','','','','','');
$TMic = array('2.8','2.9','3.0','3.1','3.2','3.3','3.4','3.5','3.6','3.7','3.8','3.9','4.0','4.1','4.2','4.3','4.4','4.5','4.6','4.7','4.8','4.9','5.0','5.1','5.2');
$TStr = array('16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40');
$TLen = array('0.98','0.99','1.00','1.01','1.02','1.03','1.04','1.05','1.06','1.07','1.08','1.09','1.10','1.11','1.12','1.13','1.14','1.15','1.16','1.17','1.18','1.19','1.20','1.21','1.22');
$TUnf = array('68','69','70','71','72','73','74','75','76','77','78','79','80','81','82','83','84','85','86','87','88','89','90','91','92');
$TRd = array('66','67','68','69','70','71','72','73','74','75','76','77','78','79','80','81','82','83','84','85','86','87','88','89','90');
$Tb = array('6.8','6.9','7.0','7.1','7.2','7.3','7.4','7.5','7.6','7.7','7.8','7.9','8.0','8.1','8.2','8.3','8.4','8.5','8.6','8.7','8.8','8.9','9.0','9.1','9.2');


//------- Col
$sum = 0;
$pdf->Ln();
$pdf->SetXY(10,35);
$pdf->SetFillColor(173,232,150);
$pdf->Rect(10, 35, 28, 5, 'F');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(14,5, utf8_decode('Col'),1,0,'C');
$pdf->Cell(14,5, utf8_decode('Bal'),1,0,'C');

if ($TypHVI == 'O'){
    if ($Crp != ""){
        $consulta = 'SELECT Round(Col1,0) as Col1s, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Col1s'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Col1,0) as Col1s, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Col1s'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}else{
    if ($Crp != ""){
        $consulta = 'SELECT Round(Col1_Mod,0) as Col1s, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Col1s'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Col1_Mod,0) as Col1s, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Col1s'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}

foreach ($TCol as $row) {
    $pdf->Ln();
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(14,5, utf8_decode($row),1,0,'C');

    foreach ($data as $dat){
        if ($dat['Col1s'] == $row){
            $valor = $dat['Bal'];
            $sum = $sum + $dat['Bal'];
            break;
        }else {
            $valor = '';
        }
    }
    
    $pdf->Cell(14,5, utf8_decode($valor),1,0,'C'); 

}

$pdf->Ln();
$pdf->Cell(14,5, utf8_decode('Total'),1,0,'C');
$pdf->Cell(14,5, utf8_decode($sum),1,0,'C');

// ------ Trs
$pdf->Ln();
$pdf->SetXY(43,35);
$pdf->SetFillColor(169,231,189);
$pdf->Rect(43, 35, 28, 5, 'F');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(14,5, utf8_decode('Trs'),1,0,'C');
$pdf->Cell(14,5, utf8_decode('Bal'),1,0,'C');

if ($TypHVI == 'O'){
    if ($Crp != ""){
        $consulta = 'SELECT Round(Tgrd,0) as Tgrds, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Tgrds'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Tgrd,0) as Tgrds, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Tgrds'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}else{
    if ($Crp != ""){
        $consulta = 'SELECT Round(Tgrd_Mod,0) as Tgrds, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Tgrds'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Tgrd_Mod,0) as Tgrds, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Tgrds'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}

$y=40;
foreach ($TTrs as $row) {
    $pdf->Ln();
    $pdf->SetXY(43,$y);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(14,5, utf8_decode($row),1,0,'C');

    foreach ($data as $dat){
        if ($dat['Tgrds'] == $row){
            $valor = $dat['Bal'];
            break;
        }else { 
            $valor = '';
        }
    }

    $pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    $y = $y + 5;
}

$pdf->Ln();
$pdf->SetXY(43,$y);
$pdf->Cell(14,5, utf8_decode('Total'),1,0,'C');
$pdf->Cell(14,5, utf8_decode($sum),1,0,'C');

// ------ Mic
$pdf->Ln();
$pdf->SetXY(76,35);
$pdf->SetFillColor(169,231,189);
$pdf->Rect(76, 35, 28, 5, 'F');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(14,5, utf8_decode('Mic'),1,0,'C');
$pdf->Cell(14,5, utf8_decode('Bal'),1,0,'C');

if ($TypHVI == 'O'){
    if ($Crp != ""){
        $consulta = 'SELECT Round(Mic,1) as Mics, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Mics'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Mic,1) as Mics, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Mics'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}else{
    if ($Crp != ""){
        $consulta = 'SELECT Round(Mic_Mod,1) as Mics, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Mics'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Mic_Mod,1) as Mics, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Mics'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}

$y=40;
$valorMin = 0;
$valorMax = 0;
//$valor = 0;
foreach ($TMic as $row) {
    $valorMin = 0;
    $valorMax = 0;
    $pdf->Ln();
    $pdf->SetXY(76,$y);
    $pdf->SetFont('Arial','B',9);
    if ($row == 2.8){
        $pdf->Cell(14,5, utf8_decode($row."-"),1,0,'C');     
    }elseif($row == 5.2){
        $pdf->Cell(14,5, utf8_decode($row."+"),1,0,'C');
    }else{
        $pdf->Cell(14,5, utf8_decode($row),1,0,'C');
    }

    foreach ($data as $dat){
        if ($dat['Mics'] <= 2.8){
            //$dat['Mics'] = 2.8;
            $valorMin = $valorMin + $dat['Bal'];
        }elseif ($dat['Mics'] >= 5.2){
            //$dat['Mics'] = 5.2;
            $valorMax = $valorMax + $dat['Bal'];
        }elseif ($dat['Mics'] == $row && $row != 2.8 && $row != 5.2){
            $valor = $dat['Bal'];
            break;
        }else{ 
            $valor = '';
        }
    }

    if ($row == 2.8 && $valorMin != 0){
        $pdf->Cell(14,5, utf8_decode($valorMin),1,0,'C');       
    }elseif ($row == 2.8 && $valorMin == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }elseif ($row == 5.2 && $valorMax != 0){
        $pdf->Cell(14,5, utf8_decode($valorMax),1,0,'C');       
    }elseif ($row == 5.2 && $valorMax == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }else{
        $pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    }
    $y = $y + 5;
}

$pdf->Ln();
$pdf->SetXY(76,$y);
$pdf->Cell(14,5, utf8_decode('Total'),1,0,'C');
$pdf->Cell(14,5, utf8_decode($sum),1,0,'C');

// ------ Len
$pdf->Ln();
$pdf->SetXY(109,35);
$pdf->SetFillColor(169,231,189);
$pdf->Rect(109, 35, 28, 5, 'F');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(14,5, utf8_decode('Len'),1,0,'C');
$pdf->Cell(14,5, utf8_decode('Bal'),1,0,'C');

if ($TypHVI == 'O'){
    if($Crp != ""){
        $consulta = 'SELECT Round(Len,2) as Lens, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Lens'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Len,2) as Lens, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Lens'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}else{
    if($Crp != ""){
        $consulta = 'SELECT Round(Len_Mod,2) as Lens, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Lens'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Len_Mod,2) as Lens, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Lens'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}

$y=40;
$valorMin = 0;
$valorMax = 0;
foreach ($TLen as $row) {
    $valorMin = 0;
    $valorMax = 0;
    $pdf->Ln();
    $pdf->SetXY(109,$y);
    $pdf->SetFont('Arial','B',9);
    if ($row == 0.98){
        $pdf->Cell(14,5, utf8_decode($row."-"),1,0,'C');
    }elseif ($row == 1.22){
        $pdf->Cell(14,5, utf8_decode($row."+"),1,0,'C');
    }else{
        $pdf->Cell(14,5, utf8_decode($row),1,0,'C');
    }

    foreach ($data as $dat){
        if ($dat['Lens'] <= 0.98){
            $valorMin = $valorMin + $dat['Bal']; //$dat['Lens'] = 0.98;
        }elseif ($dat['Lens'] >= 1.22){
            $valorMax = $valorMax + $dat['Bal']; //$dat['Lens'] = 1.22;
        }elseif ($dat['Lens'] == $row){
            $valor = $dat['Bal'];
            break;
        }else{ 
            $valor = '';
        }
    }

    if ($row == 0.98 && $valorMin != 0){
        $pdf->Cell(14,5, utf8_decode($valorMin),1,0,'C');       
    }elseif ($row == 0.98 && $valorMin == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }elseif ($row == 1.22 && $valorMax != 0){
        $pdf->Cell(14,5, utf8_decode($valorMax),1,0,'C');       
    }elseif ($row == 1.22 && $valorMax == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }else{
        $pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    }
    $y = $y + 5;
}

$pdf->Ln();
$pdf->SetXY(109,$y);
$pdf->Cell(14,5, utf8_decode('Total'),1,0,'C');
$pdf->Cell(14,5, utf8_decode($sum),1,0,'C');

// ------ Str
$pdf->Ln();
$pdf->SetXY(142,35);
$pdf->SetFillColor(169,231,189);
$pdf->Rect(142, 35, 28, 5, 'F');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(14,5, utf8_decode('Str'),1,0,'C');
$pdf->Cell(14,5, utf8_decode('Bal'),1,0,'C');

if ($TypHVI == 'O'){
    if ($Crp != ""){
        $consulta = 'SELECT Round(Str,0) as Strs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Strs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Str,0) as Strs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Strs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}else{
    if ($Crp != ""){
        $consulta = 'SELECT Round(Str_Mod,0) as Strs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Strs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Str_Mod,0) as Strs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Strs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}

$y=40;
$valorMin = 0;
$valorMax = 0;
foreach ($TStr as $row) {
    $valorMin = 0;
    $valorMax = 0;
    $pdf->Ln();
    $pdf->SetXY(142,$y);
    $pdf->SetFont('Arial','B',9);
    if ($row == 16){
        $pdf->Cell(14,5, utf8_decode($row."-"),1,0,'C');
    }elseif ($row == 40){
        $pdf->Cell(14,5, utf8_decode($row."+"),1,0,'C');
    }else{
        $pdf->Cell(14,5, utf8_decode($row),1,0,'C');
    }

    foreach ($data as $dat){
        if ($dat['Strs'] <= 16){
            $valorMin = $valorMin + $dat['Bal']; //$dat['Strs'] = 16;
        }elseif ($dat['Strs'] >= 40){
            $valorMax = $valorMax + $dat['Bal']; //$dat['Strs'] = 40;
        }elseif ($dat['Strs'] == $row){
            $valor = $dat['Bal'];
            break;
        }else{ 
            $valor = '';
        }
    }

    if ($row == 16 && $valorMin != 0){
        $pdf->Cell(14,5, utf8_decode($valorMin),1,0,'C');       
    }elseif ($row == 16 && $valorMin == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }elseif ($row == 40 && $valorMax != 0){
        $pdf->Cell(14,5, utf8_decode($valorMax),1,0,'C');       
    }elseif ($row == 40 && $valorMax == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }else{
        $pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    }
    //$pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    $y = $y + 5;
}

$pdf->Ln();
$pdf->SetXY(142,$y);
$pdf->Cell(14,5, utf8_decode('Total'),1,0,'C');
$pdf->Cell(14,5, utf8_decode($sum),1,0,'C');

// ------ Unf
$pdf->Ln();
$pdf->SetXY(175,35);
$pdf->SetFillColor(169,231,189);
$pdf->Rect(175, 35, 28, 5, 'F');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(14,5, utf8_decode('Unf'),1,0,'C');
$pdf->Cell(14,5, utf8_decode('Bal'),1,0,'C');

if ($TypHVI == 'O'){
    if ($Crp != ""){
        $consulta = 'SELECT Round(Unf,0) as Unfs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Unfs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Unf,0) as Unfs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Unfs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}else{
    if ($Crp != ""){
        $consulta = 'SELECT Round(Unf_Mod,0) as Unfs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Unfs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Unf_Mod,0) as Unfs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Unfs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}

$y=40;
$valorMin = 0;
$valorMax = 0;
foreach ($TUnf as $row) {
    $valorMin = 0;
    $valorMax = 0;
    $pdf->Ln();
    $pdf->SetXY(175,$y);
    $pdf->SetFont('Arial','B',9);
    if($row == 68){
        $pdf->Cell(14,5, utf8_decode($row."-"),1,0,'C');
    }elseif($row == 92){
        $pdf->Cell(14,5, utf8_decode($row."+"),1,0,'C');
    }else{
        $pdf->Cell(14,5, utf8_decode($row),1,0,'C');
    }

    foreach ($data as $dat){
        if ($dat['Unfs'] <= 68){
            $valorMin = $valorMin + $dat['Bal']; //$dat['Unfs'] = 68;
        }elseif ($dat['Unfs'] >= 92){
            $valorMax = $valorMax + $dat['Bal']; //$dat['Unfs'] = 92;
        }elseif ($dat['Unfs'] == $row){
            $valor = $dat['Bal'];
            break;
        }else{ 
            $valor = '';
        }
    }

    if ($row == 68 && $valorMin != 0){
        $pdf->Cell(14,5, utf8_decode($valorMin),1,0,'C');       
    }elseif ($row == 68 && $valorMin == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }elseif ($row == 92 && $valorMax != 0){
        $pdf->Cell(14,5, utf8_decode($valorMax),1,0,'C');       
    }elseif ($row == 92 && $valorMax == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }else{
        $pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    }
    //$pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    $y = $y + 5;
}

$pdf->Ln();
$pdf->SetXY(175,$y);
$pdf->Cell(14,5, utf8_decode('Total'),1,0,'C');
$pdf->Cell(14,5, utf8_decode($sum),1,0,'C');

// ------ Rd
$pdf->Ln();
$pdf->SetXY(208,35);
$pdf->SetFillColor(169,231,189);
$pdf->Rect(208, 35, 28, 5, 'F');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(14,5, utf8_decode('Rd'),1,0,'C');
$pdf->Cell(14,5, utf8_decode('Bal'),1,0,'C');

if ($TypHVI == 'O'){
    if ($Crp != ""){
        $consulta = 'SELECT Round(Rd,0) as Rds, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Rds'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Rd,0) as Rds, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Rds'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}else{
    if ($Crp != ""){
        $consulta = 'SELECT Round(Rd_Mod,0) as Rds, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Rds'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Rd_Mod,0) as Rds, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Rds'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}

$y=40;
$valorMin = 0;
$valorMax = 0;
foreach ($TRd as $row) {
    $valorMin = 0;
    $valorMax = 0;
    $pdf->Ln();
    $pdf->SetXY(208,$y);
    $pdf->SetFont('Arial','B',9);
    if ($row == 66){
        $pdf->Cell(14,5, utf8_decode($row."-"),1,0,'C');
    }elseif ($row == 90){
        $pdf->Cell(14,5, utf8_decode($row."+"),1,0,'C');
    }else{
        $pdf->Cell(14,5, utf8_decode($row),1,0,'C');
    }

    foreach ($data as $dat){
        if ($dat['Rds'] <= 66){
            $valorMin = $valorMin + $dat['Bal']; //$dat['Rds'] = 66;
        }elseif ($dat['Rds'] >= 90){
            $valorMax = $valorMax + $dat['Bal']; //$dat['Rds'] = 90;
        }elseif ($dat['Rds'] == $row){
            $valor = $dat['Bal'];
            break;
        }else{ 
            $valor = '';
        }
    }

    if ($row == 66 && $valorMin != 0){
        $pdf->Cell(14,5, utf8_decode($valorMin),1,0,'C');       
    }elseif ($row == 66 && $valorMin == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }elseif ($row == 90 && $valorMax != 0){
        $pdf->Cell(14,5, utf8_decode($valorMax),1,0,'C');       
    }elseif ($row == 90 && $valorMax == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }else{
        $pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    }
    //$pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    $y = $y + 5;
}

$pdf->Ln();
$pdf->SetXY(208,$y);
$pdf->Cell(14,5, utf8_decode('Total'),1,0,'C');
$pdf->Cell(14,5, utf8_decode($sum),1,0,'C');

// ------ b
$pdf->Ln();
$pdf->SetXY(241,35);
$pdf->SetFillColor(169,231,189);
$pdf->Rect(241, 35, 28, 5, 'F');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(14,5, utf8_decode('b'),1,0,'C');
$pdf->Cell(14,5, utf8_decode('Bal'),1,0,'C');

if ($TypHVI == 'O'){
    if ($Crp != ""){
        $consulta = 'SELECT Round(b,1) as bs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By bs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(b,1) as bs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By bs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}else{
    if ($Crp != ""){
        $consulta = 'SELECT Round(b_Mod,1) as bs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By bs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(b_Mod,1) as bs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By bs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}

$y=40;
$valorMin = 0;
$valorMax = 0;
foreach ($Tb as $row) {
    $valorMin = 0;
    $valorMax = 0;
    $pdf->Ln();
    $pdf->SetXY(241,$y);
    $pdf->SetFont('Arial','B',9);
    if ($row == 6.8){
        $pdf->Cell(14,5, utf8_decode($row."-"),1,0,'C');
    }elseif ($row == 9.2){
        $pdf->Cell(14,5, utf8_decode($row."+"),1,0,'C');
    }else{
        $pdf->Cell(14,5, utf8_decode($row),1,0,'C');
    }

    foreach ($data as $dat){
        if ($dat['bs'] <= 6.8){
            $valorMin = $valorMin + $dat['Bal']; //$dat['bs'] = 6.8;
        }elseif ($dat['bs'] >= 9.2){
            $valorMax = $valorMax + $dat['Bal']; //$dat['bs'] = 9.2;
        }elseif ($dat['bs'] == $row){
            $valor = $dat['Bal'];
            break;
        }else{ 
            $valor = '';
        }
    }

    if ($row == 6.8 && $valorMin != 0){
        $pdf->Cell(14,5, utf8_decode($valorMin),1,0,'C');       
    }elseif ($row == 6.8 && $valorMin == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }elseif ($row == 9.2 && $valorMax != 0){
        $pdf->Cell(14,5, utf8_decode($valorMax),1,0,'C');       
    }elseif ($row == 9.2 && $valorMax == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }else{
        $pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    }
    //$pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    $y = $y + 5;
}

$pdf->Ln();
$pdf->SetXY(241,$y);
$pdf->Cell(14,5, utf8_decode('Total'),1,0,'C');
$pdf->Cell(14,5, utf8_decode($sum),1,0,'C');


$pdf->Output('I', "RECAP ".$Cli." ".date('d-M-Y').".pdf");

$conexion=null;

?>