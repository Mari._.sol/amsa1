<?php

include_once ('conexion.php');
$objeto = new Conexion();
$conexion = $objeto->Conectar();

$fromdate = (!empty($_GET['fechainicio'])) ? $_GET['fechainicio'] : '';
$todate = (!empty($_GET['fechafin'])) ? $_GET['fechafin'] : '';
$region = (!empty($_GET['region'])) ? $_GET['region'] : '';


$clave=660;


$fecha=date('Y-m-d');
// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill, Alignment};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//use PhpOffice\PhpSpreadsheet\Style\Alignment as alignment;
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
$heading = false;

$fileName = "Maniobras-Puebla-".date('Y-m-d').".xlsx";

//SE CREA Y SE EMPIEZA A LLENAR LA HOJA
$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Maniobras");

//negritas en encabezado

$hojaActiva->getStyle('A1:S1')->getFont()->setBold( true );

//relleno de celda encabezado
$hojaActiva->getStyle('A1:S1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFE1E1E1');
//fijar primera fila
$hojaActiva->freezePane('A2');


//poner en texto los lotes

//$hojaActiva->getStyle('E2:E')->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);

$hojaActiva->getColumnDimension('A')->setWidth(12);
$hojaActiva->setCellValue('A1','TruckID');
$hojaActiva->getColumnDimension('B')->setWidth(12);
$hojaActiva->setCellValue('B1','Movimiento');
$hojaActiva->getColumnDimension('C')->setWidth(12);
$hojaActiva->setCellValue('C1','Destino');
$hojaActiva->getColumnDimension('D')->setWidth(12);
$hojaActiva->setCellValue('D1','DO');
$hojaActiva->getColumnDimension('E')->setWidth(12);
$hojaActiva->setCellValue('E1','Lot');
$hojaActiva->getColumnDimension('F')->setWidth(12);
$hojaActiva->setCellValue('F1','Bales');
$hojaActiva->getColumnDimension('G')->setWidth(12);
$hojaActiva->setCellValue('G1','Gin');
$hojaActiva->getColumnDimension('H')->setWidth(12);
$hojaActiva->setCellValue('H1','Date');
$hojaActiva->getColumnDimension('I')->setWidth(12);
$hojaActiva->setCellValue('I1','Transport');
$hojaActiva->getColumnDimension('J')->setWidth(12);
$hojaActiva->setCellValue('J1','Way Bill');
$hojaActiva->getColumnDimension('K')->setWidth(12);
$hojaActiva->setCellValue('K1','Status Truck');
$hojaActiva->getColumnDimension('L')->setWidth(12);
$hojaActiva->setCellValue('L1',' Maniobra');
$hojaActiva->getColumnDimension('M')->setWidth(12);
$hojaActiva->setCellValue('M1','Costo x Paca');
$hojaActiva->getColumnDimension('N')->setWidth(12);
$hojaActiva->setCellValue('N1','Costo x Lote');
$hojaActiva->getColumnDimension('O')->setWidth(12);
$hojaActiva->setCellValue('O1','Encargardo Maniobra');
$hojaActiva->getColumnDimension('P')->setWidth(12);
$hojaActiva->setCellValue('P1','Nota de Salida');
$hojaActiva->getColumnDimension('Q')->setWidth(12);
$hojaActiva->setCellValue('Q1','PQ');
$hojaActiva->getColumnDimension('R')->setWidth(12);
$hojaActiva->setCellValue('R1','Factura');
$hojaActiva->getColumnDimension('S')->setWidth(30);
$hojaActiva->setCellValue('S1','Comentario');

//centrar celdas unidas horizontalmente 
$hojaActiva->getStyle('A1:S1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


//LLEGADAS Y DIRECTOS
$query="SELECT Truks.TrkID,Truks.DO,Transports.BnName,Truks.WBill,Lots.Lot,Lots.Qty,Gines.GinName,Region.RegNam,Truks.Status,
        Truks.InDat as fecha,Truks.TipoManArr as TipoMan,Truks.RespManArr as RespMan,Truks.NotaSalArr as NotaSal,/*Maniobras.CostoMan as costoxpaca,*/Truks.ComentarioManArr as ComentarioMan,
        /*(Maniobras.CostoMan * Lots.Qty )as costoxlote,*/Truks.FacManArr AS Factura,Truks.PQArr as PQ,
    
        (CASE 
            WHEN Maniobras.TipoMan = Truks.TipoManArr AND (Truks.InDat between Maniobras.FechaInicio and FechaFin) THEN (select  Maniobras.CostoMan WHERE Maniobras.TipoMan = Truks.TipoManArr)
            else ''

        END)as costoxpaca,

        (CASE 
            when DOrds.InReg= '$clave'    then 'ENTRADA'             
            when  DOrds.InReg = 99001  and  DOrds.OutPlc != '$clave' AND Clients.Directo = 1 AND Clients.State ='$region'  then 'DIRECTO' 
            else ''
        END) as TipoMov,
        (CASE 
            when DOrds.InReg= '$clave'    then 'AMSA PUEBLA'             
            when DOrds.InReg = 99001  and  DOrds.OutPlc != '$clave' AND Clients.Directo = 1 AND Clients.State ='$region' then (select Clients.Cli where DOrds.InPlc = Clients.CliID )
            else ''
        END) as Lugar

        FROM amsadb1.Truks
        LEFT JOIN amsadb1.DOrds 
        ON Truks.DO = DOrds.DOrd
        LEFT JOIN amsadb1.Clients 
        ON DOrds.InPlc = Clients.CliID
        LEFT JOIN amsadb1.Transports
        ON Truks.TNam = Transports.TptID 

        left JOIN amsadb1.Lots 
        ON Lots.TrkID = Truks.TrkID

        LEFT JOIN amsadb1.Gines 
        ON Lots.GinID = Gines.IDGin

        LEFT JOIN amsadb1.Region 
        ON Lots.Reg = Region.IDReg

        LEFT JOIN amsadb1.Maniobras 
        ON Truks.TipoManArr = Maniobras.TipoMan  AND Truks.InDat between Maniobras.FechaInicio and FechaFin


        where (DOrds.InReg= '$clave'  or ( DOrds.InReg = 99001  AND Clients.State ='$region' and Clients.Directo = 1 )) and DOrds.OutPlc != '$clave'  and (Truks.InDat between '$fromdate' and '$todate') AND Truks.Status !='Cancelled'
        order By Truks.TrkId Desc;"
;


$resultado = $conexion->prepare($query);
$resultado->execute();        
$trkanterior=0;
$myarray = [];
$fila = 2;
while($row = $resultado->fetch(PDO::FETCH_ASSOC)){

    //formato de fecha sin hora 

    $fechaMan = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel( $row['fecha'] );
    //strtotime($row['fecha']);
    //$fechaMan =25569 + ($fechaMan / 86400);


    if($trkanterior == $row['TrkID']){

        array_push($myarray,($fila-1));
        array_push($myarray,$fila);
    }

    //FORMATO DE FECHA EN LA COLUMNA
    $hojaActiva->getStyle('H' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    
     // $hojaActiva->getStyle('E'. $fila)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
    //definir el costo por lote 
     $costoxpaca=$row['costoxpaca'];
     if($costoxpaca!=""){
        $costoxlote = $costoxpaca * $row['Qty'];
     }
     else{
        $costoxlote = 0;
        $costoxpaca= 0; 
     }

    $hojaActiva->setCellValue('A' . $fila,$row['TrkID']);
    $hojaActiva->setCellValue('B' . $fila,$row['TipoMov']);
    $hojaActiva->setCellValue('C' . $fila,$row['Lugar']);
    $hojaActiva->setCellValue('D' . $fila, $row['DO']);
    //$hojaActiva->setCellValue('E' . $fila,'"'.$row['Lot'].'"');
    $hojaActiva->setCellValue('F' . $fila,$row['Qty']);   
    $hojaActiva->setCellValue('G'. $fila,$row['GinName']);   
    $hojaActiva->setCellValue('H'. $fila,$fechaMan);
    $hojaActiva->setCellValue('I'. $fila,$row['BnName']);
    $hojaActiva->setCellValue('J'. $fila,$row['WBill']);
    $hojaActiva->setCellValue('K'. $fila,$row['Status']);
    $hojaActiva->setCellValue('L'. $fila,$row['TipoMan']);
    $hojaActiva->setCellValue('M'. $fila,$costoxpaca);
    $hojaActiva->setCellValue('N'. $fila,$costoxlote);
    $hojaActiva->setCellValue('O'. $fila,$row['RespMan']);
    $hojaActiva->setCellValue('P'. $fila,$row['NotaSal']);
    $hojaActiva->setCellValue('Q'. $fila,$row['PQ']);
    $hojaActiva->setCellValue('R'. $fila,$row['Factura']);
    $hojaActiva->setCellValue('S'. $fila,$row['ComentarioMan']);
    
     $style = $hojaActiva->getStyle('E'. $fila);

    $style->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
    
    $hojaActiva->getCell('E'. $fila)->setValueExplicit($row['Lot'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
    
    /*$hojaActiva->setCellValue('M'. $fila,$row['BnName']);
    $hojaActiva->setCellValue('N'. $fila,$row['Cli']);*/

    $trkanterior = $row['TrkID'];
    $fila++;
   



}



//SALIDAS
$query2="SELECT Truks.TrkID,Truks.DO,Transports.BnName,Truks.WBill,Lots.Lot,Lots.Qty,Gines.GinName,Region.RegNam,Truks.Status,
        Truks.OutDat as fecha,Truks.TipoManDep as TipoMan,Truks.RespManDep as RespMan,Truks.NotaSalDep as NotaSal,/*Maniobras.CostoMan as costoxpaca,*/Truks.ComentarioManDep as ComentarioMan,
        /*(Maniobras.CostoMan * Lots.Qty)as costoxlote,*/Truks.FacManDep AS Factura,Truks.PQDep as PQ,

        (CASE 
            WHEN Maniobras.TipoMan = Truks.TipoManDep AND (Truks.OutDat between Maniobras.FechaInicio and FechaFin) THEN (select  Maniobras.CostoMan WHERE Maniobras.TipoMan = Truks.TipoManDep)
            else ''

        END)as costoxpaca,

        IF(DOrds.InReg = 99001,(select Clients.Cli where DOrds.InPlc = Clients.CliID),(select R1.RegNam from Region as R1  where DOrds.InReg = R1.IDReg)) as Lugar

        FROM amsadb1.Truks
        LEFT JOIN amsadb1.DOrds 
        ON Truks.DO = DOrds.DOrd
        LEFT JOIN amsadb1.Clients 
        ON DOrds.InPlc = Clients.CliID
        LEFT JOIN amsadb1.Transports
        ON Truks.TNam = Transports.TptID 

        left JOIN amsadb1.Lots 
        ON Lots.TrkID = Truks.TrkID

        LEFT JOIN amsadb1.Gines 
        ON Lots.GinID = Gines.IDGin

        LEFT JOIN amsadb1.Region 
        ON Lots.Reg = Region.IDReg

        LEFT JOIN amsadb1.Maniobras 
        ON Truks.TipoManDep = Maniobras.TipoMan  AND Truks.OutDat between Maniobras.FechaInicio and FechaFin

        where DOrds.OutPlc= '$clave'    and (Truks.OutDat between '$fromdate' and '$todate') AND Truks.Status !='Cancelled'
        order By Truks.TrkId Desc;"
;


$resultado2 = $conexion->prepare($query2);
$resultado2->execute();     


while($row = $resultado2->fetch(PDO::FETCH_ASSOC)){

    //formato de fecha sin hora 

    $fechaMan = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel( $row['fecha'] );
    //strtotime($row['fecha']);
    //$fechaMan =25569 + ($fechaMan / 86400);


    if($trkanterior == $row['TrkID']){

        array_push($myarray,($fila-1));
        array_push($myarray,$fila);
    }

     //definir el costo por lote 
     $costoxpaca=$row['costoxpaca'];
     if($costoxpaca!=''){
        $costoxlote = $costoxpaca * $row['Qty'];
     }
     else{
        $costoxlote = 0;
        $costoxpaca= 0; 
     }
   

    //FORMATO DE FECHA EN LA COLUMNA
    $hojaActiva->getStyle('H' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    
    
    $hojaActiva->setCellValue('A' . $fila,$row['TrkID']);
    $hojaActiva->setCellValue('B' . $fila,'SALIDA');
    $hojaActiva->setCellValue('C' . $fila,$row['Lugar']);
    $hojaActiva->setCellValue('D' . $fila, $row['DO']);
    //$hojaActiva->setCellValue('E' . $fila,'"'.$row['Lot'].'"');
    $hojaActiva->setCellValue('F' . $fila,$row['Qty']);
    $hojaActiva->setCellValue('G'. $fila,$row['GinName']);
    $hojaActiva->setCellValue('H'. $fila,$fechaMan);
    $hojaActiva->setCellValue('I'. $fila,$row['BnName']);
    $hojaActiva->setCellValue('J'. $fila,$row['WBill']);
    $hojaActiva->setCellValue('K'. $fila,$row['Status']);
    $hojaActiva->setCellValue('L'. $fila,$row['TipoMan']);
    $hojaActiva->setCellValue('M'. $fila,$costoxpaca);
    $hojaActiva->setCellValue('N'. $fila,$costoxlote);
    $hojaActiva->setCellValue('O'. $fila,$row['RespMan']);
    $hojaActiva->setCellValue('P'. $fila,$row['NotaSal']);
    $hojaActiva->setCellValue('Q'. $fila,$row['PQ']);
    $hojaActiva->setCellValue('R'. $fila,$row['Factura']);
    $hojaActiva->setCellValue('S'. $fila,$row['ComentarioMan']);

    /*$hojaActiva->setCellValue('M'. $fila,$row['BnName']);
    $hojaActiva->setCellValue('N'. $fila,$row['Cli']);*/
    $style = $hojaActiva->getStyle('E'. $fila);

    $style->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
    $hojaActiva->getCell('E'. $fila)->setValueExplicit($row['Lot'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
    $trkanterior = $row['TrkID'];
    $fila++;
}








//$hojaActiva->getStyle('I2:I'.$fila)->getNumberFormat()->setFormatCode('###0.00');

$longitud = count($myarray);
for($i=0; $i<$longitud; $i++){

    $hojaActiva->getStyle('A' . $myarray[$i])->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFFFFF00');

}

  //FORMATO DOS DIGITOS DESPUES DEL PUNTO
  $hojaActiva->getStyle('N2:N'.$fila)->getNumberFormat()->setFormatCode('###0.00');
  $hojaActiva->getStyle('M2:M'.$fila)->getNumberFormat()->setFormatCode('###0.00');
  



header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');
ob_end_clean();
$writer = IOFactory::createWriter($excel, 'Xlsx');
$writer->save('php://output');
exit;


?>