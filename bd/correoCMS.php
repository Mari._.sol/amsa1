<?php

include_once('correo-model.php');
/*include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();*/
date_default_timezone_set("America/Mexico_City");

$numDO = (!empty($_REQUEST['numDO'])) ? $_REQUEST['numDO'] : '';
$regOri = (!empty($_REQUEST['regOri'])) ? $_REQUEST['regOri'] : '';

//fecha de envio de listados
$DateMail=date('Y-m-d');

// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


//Define the filename with current date 
//$fileName = "Requisition-".date('d-m-Y').".xls";

//Set header information to export data in excel format
//header('Content-Type: application/vnd.ms-excel');
//header('Content-Disposition: attachment; filename='.$fileName);

//Set variable to false for heading
$heading = false;

$consulta = "UPDATE DOrds SET Date_Mail='$DateMail' WHERE DOrd='$numDO'";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();

//VALIDAR SI SALE DE UN GIN O DE UNA BODEGA QUE ESTA EN EL GIN
$tiporigen=get_tiporigen($regOri);


//SI SALE DE UNA BODEGA DE UN ORIGEN NO SE DEBE ADJUNTAR RL LISTADO DE PACAS 
$isbodegaorigen=$tiporigen[0]['IsWHOrigin'];

// VALIDAR SI LA DO ES DE PACAS CERTIFICADAS

$consulta = "SELECT Cert FROM DOrds WHERE DOrd='$numDO'"; 
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$dataLoc=$resultado->fetch();
$cert = $dataLoc['Cert'];





if($isbodegaorigen==0){//SI NO ES BODEGA DE UN ORIGEN SE GENERA EL EXCEL CON EL LISTADO
      $consulta = 'SELECT Bales.Bal as Bal, Bales.Lot as Lot, Gines.GinName as Gin, Bales.DO as DO
                  FROM Bales
                  JOIN Gines on  Gines.IDGin = Bales.GinID
                  Where Bales.DO = '.$numDO.' order by Lot, Bal;';
      $resultado = $conexion->prepare($consulta);
      $resultado->execute();        
      $siexiste=0; //$data=$resultado->fetchAll(PDO::FETCH_ASSOC);

      //Define the filename with current date
      $fileName = "Listado_de_Pacas.xlsx";

      $excel = new Spreadsheet();
      $hojaActiva = $excel->getActiveSheet();
      $hojaActiva->setTitle("Lotes");
      //$hojaActiva->freezePane("A2");

      $hojaActiva->getColumnDimension('A')->setWidth(17);
      $hojaActiva->setCellValue('A1','No. Paca');
      $hojaActiva->getColumnDimension('B')->setWidth(15);
      $hojaActiva->setCellValue('B1','Lote');
      $hojaActiva->getColumnDimension('C')->setWidth(15);
      $hojaActiva->setCellValue('C1','Gin');
      $hojaActiva->getColumnDimension('D')->setWidth(15);
      $hojaActiva->setCellValue('D1','DO');
      
        $hojaActiva->getColumnDimension('E')->setWidth(20);
        $hojaActiva->setCellValue('E1','Pacas Certificadas');
   

      $fila = 2;

      while($row = $resultado->fetch(PDO::FETCH_ASSOC)){
          if($siexiste==0){
              if(!empty($row['Bal']))
                  $siexiste=1;
          }
          
          $hojaActiva->setCellValue('A' . $fila,$row['Bal']);
          $hojaActiva->setCellValue('B' . $fila,$row['Lot']);
          $hojaActiva->setCellValue('C' . $fila,$row['Gin']);
          $hojaActiva->setCellValue('D' . $fila,$row['DO']);
          if ($cert == 1){
            $hojaActiva->setCellValue('E' . $fila, 'SI');
          }
          else{
            $hojaActiva->setCellValue('E' . $fila, 'NO');
          }

          $fila++;
      }


      // redirect output to client browser
      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      header('Content-Disposition: attachment;filename="'.$fileName.'"');
      header('Cache-Control: max-age=0');

      $path = '../files/Listado_de_Pacas.xlsx';
      $writer = IOFactory::createWriter($excel, 'Xlsx');
      $writer->save($path);
}
// ----------------------------------------------------------------------------------------------------------
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

require './vendor/phpmailer/phpmailer/src/PHPMailer.php';
require './vendor/phpmailer/phpmailer/src/SMTP.php';

require './vendor/autoload.php';

//include_once('correo-model.php');


try {
  $usuario = (!empty($_REQUEST['usuario'])) ? $_REQUEST['usuario'] : '';
  $typeDO = (!empty($_REQUEST['typeDO'])) ? $_REQUEST['typeDO'] : '';
  $numDO = (!empty($_REQUEST['numDO'])) ? $_REQUEST['numDO'] : '';
  $salSucu = (!empty($_REQUEST['salSucu'])) ? $_REQUEST['salSucu'] : '';
 
  $arrReg = (!empty($_REQUEST['arrReg'])) ? $_REQUEST['arrReg'] : '';
  $client = (!empty($_REQUEST['client'])) ? $_REQUEST['client'] : '';
  $observ = (!empty($_REQUEST['observ'])) ? $_REQUEST['observ'] : '';
  $contra = (!empty($_REQUEST['contra'])) ? $_REQUEST['contra'] : '';
  $totalQty = (!empty($_REQUEST['totalQty'])) ? $_REQUEST['totalQty'] : '';
  $esMuestra = (!empty($_REQUEST['esMuestra'])) ? $_REQUEST['esMuestra'] : '';
  $asunto = (!empty($_REQUEST['subject'])) ? $_REQUEST['subject'] : '';
  $tabla = (!empty($_REQUEST['tablahtml'])) ? $_REQUEST['tablahtml'] : '';
  $emailsD = (!empty($_REQUEST['emails'])) ? $_REQUEST['emails'] : '';
  $emailsCC = (!empty($_REQUEST['emailsCC'])) ? $_REQUEST['emailsCC'] : '';

  //Traer email y contraseña de la bd
  $AuthRow = getAuth($usuario);
  $email = $AuthRow['email'];
  $pass = $AuthRow['passApp'];

  //if($esMuestra == 1)
  //  $asunto.=" | MUESTRAS";
  //Traer el destinatario y los CC de la bd dependiendo del cliente ||join Gines on Gines.IDGin = Bales.GinID
  $emailDestino= array_unique($emailDestino = explode(",", $emailsD) , SORT_STRING);
  $emailCC= array_unique($emailCC = explode(",", $emailsCC) , SORT_STRING);

  // Intancia de PHPMailer
  $mail                = new PHPMailer();
  // Es necesario para poder usar un servidor SMTP como gmail
  $mail->isSMTP();
  // Si estamos en desarrollo podemos utilizar esta propiedad para ver mensajes de error
  //SMTP::DEBUG_OFF    = off (for production use) 0
  //SMTP::DEBUG_CLIENT = client messages 1 
  //SMTP::DEBUG_SERVER = client and server messages 2
  $mail->SMTPDebug     = 0; //SMTP::DEBUG_SERVER;
  //Set the hostname of the mail server
  $mail->Host          = 'smtp.gmail.com';
  $mail->Port          = 465; // o 587
  // Propiedad para establecer la seguridad de encripción de la comunicación
  $mail->SMTPSecure    = PHPMailer::ENCRYPTION_SMTPS; // tls o ssl para gmail obligado
  // Para activar la autenticación smtp del servidor
  $mail->SMTPAuth      = true;
  // Credenciales de la cuenta
  $mail->Username     = $email;
  $mail->Password     = $pass;
  // Quien envía este mensaje
  $mail->setFrom($email, $usuario);
  // Si queremos una dirección de respuesta
  //$mail->addReplyTo('replyto@panchos.com', 'Pancho Doe');
  // Destinatario
  //$mail->addAddress("acencion.pani@ecomtrading.com");
  // CC
  //$mail->addCC("christian.becerra@ecomtrading.com ");
  $mail->addCC("josue.reyes@ecomtrading.com");
  
  $mail->addCC("jdelmoral@ecomtrading.com");
  

  $m = 0;
  while ($m < count($emailDestino)) {
    $mail->addAddress($emailDestino[$m]);
    $m++;
  }
  
  $m = 0;
  while ($m < count($emailCC)) {
    $mail->addCC($emailCC[$m]);
    $m++;
  }


  // Asunto del correo
  $mail->Subject = $asunto;
  // Contenido
  $mail->IsHTML(true);
  $mail->CharSet = 'UTF-8';
  
  $bodyy ='
    <style>
    table, th, td {
      border: 1px solid;
    }    
    </style>
    
    <p>Buen día<br>
    Se anexa listado de los siguientes lotes:</p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Orden de embarque:</b> ' . $numDO . '<br>';
     if(!empty($tabla)){
     $bodyy .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Lotes:</b><dd><div>' . $tabla . '</div></dd> ';}
     if(!empty($observ)){
     $bodyy.= ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Comentarios:</b> ' . $observ . '<br><br>';}
   $bodyy .=' 
    Avisar cualquier contratiempo o anomalía.<br>
    Saludos.<br>';

    if($isbodegaorigen==1){

          $bodyy ='
        <style>
        table, th, td {
          border: 1px solid;
        }    
        </style>
        
        <p>Buen día<br>
        Se anexa detalle de lotes para la siguiente DO:</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Orden de embarque:</b> ' . $numDO . '<br>';
        if(!empty($tabla)){
        $bodyy .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Lotes:</b><dd><div>' . $tabla . '</div></dd> ';}
        if(!empty($observ)){
        $bodyy.= ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Comentarios:</b> ' . $observ . '<br><br>';}
        $bodyy .=' 
        Avisar cualquier contratiempo o anomalía.<br>
        Saludos.<br>';

    }

    $mail->Body    = $bodyy;

  // Texto alternativo
  //$mail->AltBody = 'No olvides suscribirte a nuestro canal.';
  
  // Agregar algún adjunto si sale de un origen de lo contrario solo es el mensaje 
  if($isbodegaorigen==0){
      $mail->addAttachment($path);
  }
  
  
  // Enviar el correo
  if (!$mail->send()) {
    $ma = "Error";
    throw new Exception($mail->ErrorInfo);
  } else {
    $ma = "Message sent successfully";
  }
    print json_encode($ma, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK); //envio el array final el formato json a AJAX
   
} catch (Exception $e) {
  echo $e->getMessage();
}