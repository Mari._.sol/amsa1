<?php

include_once ('conexion.php');
$objeto = new Conexion();
$conexion = $objeto->Conectar();
$myarray = [];
date_default_timezone_set("America/Mexico_City");
//Doc. de texto para guardar consultas de arribo de lotes 
//$filetext =fopen("querys.txt", "a");

// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill, Alignment};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//use PhpOffice\PhpSpreadsheet\Style\Alignment as alignment;
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
$heading = false;
$regsalida = (!empty($_GET['regsalida'])) ? $_GET['regsalida'] : '';
$regllegada = (!empty($_GET['regllegada'])) ? $_GET['regllegada'] : '';
$fromdate = (!empty($_GET['fromdate'])) ? $_GET['fromdate'] : '';
$todate = (!empty($_GET['todate'])) ? $_GET['todate'] : '';
$StatusTrk = (!empty($_GET['statustrk'])) ? $_GET['statustrk'] : '';
$pendiente = (!empty($_GET['pendiente'])) ? $_GET['pendiente'] : '';

$muestras = (!empty($_GET['muestras'])) ? $_GET['muestras'] : '';

$aux1 = explode(",", $regsalida);
$aux2 = explode(",", $regllegada);

if(count($aux1) > 1){
    $regionessalida = implode("', '", $aux1);
}
else{
    $regionessalida = implode("''", $aux1);
}

if(count($aux2) > 1){
    $regionesllegada = implode("', '", $aux2);
}
else{
    $regionesllegada = implode("''", $aux2);
}


$complemento =" ORDER BY TrkID DESC;";

if ($pendiente=="" && $muestras  == ""){
    $complemento=" ORDER BY TrkID DESC;";
}
else{
    if ($pendiente == 1 && $muestras == ""){
        $complemento=" AND StatusNav ='0' ORDER BY TrkID DESC;";    
    }
    else if($pendiente == "" && $muestras == 1){
        $complemento=" AND Samples = 1 ORDER BY TrkID DESC;";    
    }
    else{
        $complemento=" ORDER BY TrkID DESC;";
    }
}

//$fecha=date($outdate,'');

//$fecha = date('Y-m-d', strtotime($fechasalida));


//Filtra por region de llegada
if($regsalida == "" && $regllegada !="" && $StatusTrk =="All"){

    $consulta="SELECT Lots.TrkID,Lots.DOrd,Lots.Lot,Lots.Qty,Region.RegNam as RegionSalida,Gines.GinName as Despepite,Truks.OutDat AS FechaSalida,Truks.InDat AS FechaLlegada,Truks.Status,Clients.BnName as Cliente,
    Transports.BnName as Linea,Truks.DrvNam AS CHOFER,ROUND(((Truks.OutWgh/Truks.CrgQty)*Lots.Qty),2) as Bascula,DOrds.Ctc AS Contrato,Truks.StatusNav,Truks.CommentNav,DOrds.Typ,Truks.Samples,Truks.ComentarioProveed as comentario,
    IF(((Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd))=99001,
    (Select Town From amsadb1.Clients Where  Clients.CliID = (Select InPlc From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd)),
    ((Select Town From amsadb1.Region Where Region.IDReg =(Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd) ))) as CiudadLlegada,R1.RegNam as RegionLlegada,
    ROUND((Lots.LiqWgh/Lots.Qty),2) as AVGPwg, ROUND((Truks.OutWgh/Truks.CrgQty),2) as AVGOutWgh
    From amsadb1.Lots
    LEFT JOIN amsadb1.Truks
    ON Lots.TrkID = Truks.TrkID
    LEFT JOIN amsadb1.DOrds
    ON  Lots.DOrd = DOrds.DOrd
    LEFT JOIN amsadb1.Clients
    ON Clients.CliID = DOrds.InPlc
    LEFT JOIN amsadb1.Gines
    ON  Lots.GinID=Gines.IDGin 
    LEFT JOIN amsadb1.Region
    ON  DOrds.OutPlc=Region.IDReg 
    LEFT JOIN amsadb1.Transports
    ON Truks.TNam = Transports .TptID
    LEFT JOIN amsadb1.Region R1
    ON  DOrds.InReg=R1.IDReg 
    where Truks.Status IN ('Programmed','Transit','Received')  AND  R1.RegNam IN ('$regionesllegada') AND  Truks.OutDat BETWEEN '$fromdate' AND '$todate'".$complemento;
    //,'Transit','Received'
}
//Filtra por region de salida
if($regsalida != "" && $regllegada=="" && $StatusTrk =="All"){    
    $consulta="SELECT Lots.TrkID,Lots.DOrd,Lots.Lot,Lots.Qty,Region.RegNam as RegionSalida,Gines.GinName as Despepite,Truks.OutDat AS FechaSalida, Truks.InDat AS FechaLlegada,Truks.Status,Clients.BnName as Cliente,
    Transports.BnName as Linea,Truks.DrvNam AS CHOFER,ROUND(((Truks.OutWgh/Truks.CrgQty)*Lots.Qty),2) as Bascula,DOrds.Ctc AS Contrato,Truks.StatusNav,Truks.CommentNav,DOrds.Typ,Truks.Samples,Truks.ComentarioProveed as comentario,
    IF(((Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd))=99001,
    (Select Town From amsadb1.Clients Where  Clients.CliID = (Select InPlc From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd)),
    ((Select Town From amsadb1.Region Where Region.IDReg =(Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd) ))) as CiudadLlegada,R1.RegNam as RegionLlegada,
    ROUND((Lots.LiqWgh/Lots.Qty),2) as AVGPwg, ROUND((Truks.OutWgh/Truks.CrgQty),2) as AVGOutWgh
    From amsadb1.Lots
    LEFT JOIN amsadb1.Truks
    ON Lots.TrkID = Truks.TrkID
    LEFT JOIN amsadb1.DOrds
    ON  Lots.DOrd = DOrds.DOrd
    LEFT JOIN amsadb1.Clients
    ON Clients.CliID = DOrds.InPlc
    LEFT JOIN amsadb1.Gines
    ON  Lots.GinID=Gines.IDGin 
    LEFT JOIN amsadb1.Region
    ON  DOrds.OutPlc=Region.IDReg 
    LEFT JOIN amsadb1.Transports
    ON Truks.TNam = Transports .TptID
    LEFT JOIN amsadb1.Region R1
    ON  DOrds.InReg=R1.IDReg 
    where Truks.Status IN ('Programmed','Transit','Received') AND  Region.RegNam IN ('$regionessalida') AND Truks.OutDat BETWEEN '$fromdate' AND '$todate'".$complemento;
    //,'Transit','Received'
}


//Filtra por region de salida y region de llegada 
if($regsalida !="" && $regllegada!="" && $StatusTrk =="All"){    
    $consulta="SELECT Lots.TrkID,Lots.DOrd,Lots.Lot,Lots.Qty,Region.RegNam as RegionSalida,Gines.GinName as Despepite,Truks.OutDat AS FechaSalida,Truks.InDat AS FechaLlegada,Truks.Status,Clients.BnName as Cliente,
    Transports.BnName as Linea,Truks.DrvNam AS CHOFER,ROUND(((Truks.OutWgh/Truks.CrgQty)*Lots.Qty),2) as Bascula,DOrds.Ctc AS Contrato,Truks.StatusNav,Truks.CommentNav,DOrds.Typ,Truks.Samples,Truks.ComentarioProveed as comentario,
    IF(((Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd))=99001,
    (Select Town From amsadb1.Clients Where  Clients.CliID = (Select InPlc From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd)),
    ((Select Town From amsadb1.Region Where Region.IDReg =(Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd) ))) as CiudadLlegada,R1.RegNam as RegionLlegada,
    ROUND((Lots.LiqWgh/Lots.Qty),2) as AVGPwg, ROUND((Truks.OutWgh/Truks.CrgQty),2) as AVGOutWgh
    From amsadb1.Lots
    LEFT JOIN amsadb1.Truks
    ON Lots.TrkID = Truks.TrkID
    LEFT JOIN amsadb1.DOrds
    ON  Lots.DOrd = DOrds.DOrd
    LEFT JOIN amsadb1.Clients
    ON Clients.CliID = DOrds.InPlc
    LEFT JOIN amsadb1.Gines
    ON  Lots.GinID=Gines.IDGin 
    LEFT JOIN amsadb1.Region
    ON  DOrds.OutPlc=Region.IDReg 
    LEFT JOIN amsadb1.Transports
    ON Truks.TNam = Transports .TptID
    LEFT JOIN amsadb1.Region R1
    ON  DOrds.InReg=R1.IDReg 
    where Truks.Status IN ('Programmed','Transit','Received') AND  Region.RegNam IN ('$regionessalida') AND R1.RegNam IN ('$regionesllegada') AND Truks.OutDat BETWEEN '$fromdate' AND '$todate'".$complemento;
//'Transit','Received'

}


//Filtra por region de salida y status de truck
if($regsalida !="" && $regllegada=="" && $StatusTrk !="All"){    
    $consulta="SELECT Lots.TrkID,Lots.DOrd,Lots.Lot,Lots.Qty,Region.RegNam as RegionSalida,Gines.GinName as Despepite,Truks.OutDat AS FechaSalida,Truks.InDat AS FechaLlegada,Truks.Status,Clients.BnName as Cliente,
    Transports.BnName as Linea,Truks.DrvNam AS CHOFER,ROUND(((Truks.OutWgh/Truks.CrgQty)*Lots.Qty),2) as Bascula,DOrds.Ctc AS Contrato,Truks.StatusNav,Truks.CommentNav,DOrds.Typ,Truks.Samples,Truks.ComentarioProveed as comentario,
    IF(((Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd))=99001,
    (Select Town From amsadb1.Clients Where  Clients.CliID = (Select InPlc From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd)),
    ((Select Town From amsadb1.Region Where Region.IDReg =(Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd) ))) as CiudadLlegada,R1.RegNam as RegionLlegada,
    ROUND((Lots.LiqWgh/Lots.Qty),2) as AVGPwg, ROUND((Truks.OutWgh/Truks.CrgQty),2) as AVGOutWgh
    From amsadb1.Lots
    LEFT JOIN amsadb1.Truks
    ON Lots.TrkID = Truks.TrkID
    LEFT JOIN amsadb1.DOrds
    ON  Lots.DOrd = DOrds.DOrd
    LEFT JOIN amsadb1.Clients
    ON Clients.CliID = DOrds.InPlc
    LEFT JOIN amsadb1.Gines
    ON  Lots.GinID=Gines.IDGin 
    LEFT JOIN amsadb1.Region
    ON  DOrds.OutPlc=Region.IDReg 
    LEFT JOIN amsadb1.Transports
    ON Truks.TNam = Transports .TptID
    LEFT JOIN amsadb1.Region R1
    ON  DOrds.InReg=R1.IDReg 
    where Truks.Status ='$StatusTrk' AND  Region.RegNam IN ('$regionessalida') AND Truks.OutDat BETWEEN '$fromdate' AND '$todate'".$complemento;
    //,'Transit','Received'
}


//Filtra por region de llegada y status de transporte
if($regsalida == "" && $regllegada != "" && $StatusTrk !="All"){

    $consulta="SELECT Lots.TrkID,Lots.DOrd,Lots.Lot,Lots.Qty,Region.RegNam as RegionSalida,Gines.GinName as Despepite,Truks.OutDat AS FechaSalida,Truks.InDat AS FechaLlegada,Truks.Status,Clients.BnName as Cliente,
    Transports.BnName as Linea,Truks.DrvNam AS CHOFER,ROUND(((Truks.OutWgh/Truks.CrgQty)*Lots.Qty),2) as Bascula,DOrds.Ctc AS Contrato,Truks.StatusNav,Truks.CommentNav,DOrds.Typ,Truks.Samples,Truks.ComentarioProveed as comentario,
    IF(((Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd))=99001,
    (Select Town From amsadb1.Clients Where  Clients.CliID = (Select InPlc From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd)),
    ((Select Town From amsadb1.Region Where Region.IDReg =(Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd) ))) as CiudadLlegada,R1.RegNam as RegionLlegada,
    ROUND((Lots.LiqWgh/Lots.Qty),2) as AVGPwg, ROUND((Truks.OutWgh/Truks.CrgQty),2) as AVGOutWgh
    From amsadb1.Lots
    LEFT JOIN amsadb1.Truks
    ON Lots.TrkID = Truks.TrkID
    LEFT JOIN amsadb1.DOrds
    ON  Lots.DOrd = DOrds.DOrd
    LEFT JOIN amsadb1.Clients
    ON Clients.CliID = DOrds.InPlc
    LEFT JOIN amsadb1.Gines
    ON  Lots.GinID=Gines.IDGin 
    LEFT JOIN amsadb1.Region
    ON  DOrds.OutPlc=Region.IDReg 
    LEFT JOIN amsadb1.Transports
    ON Truks.TNam = Transports .TptID
    LEFT JOIN amsadb1.Region R1
    ON  DOrds.InReg=R1.IDReg 
    where Truks.Status ='$StatusTrk'  AND  R1.RegNam IN ('$regionesllegada') AND  Truks.OutDat BETWEEN '$fromdate' AND '$todate'".$complemento;
    //,'Transit','Received'
}



//Filtra por region de salida,region de llegada  y estatus de truck  
if($regsalida != "" && $regllegada!= "" && $StatusTrk !="All"){    
    $consulta="SELECT Lots.TrkID,Lots.DOrd,Lots.Lot,Lots.Qty,Region.RegNam as RegionSalida,Gines.GinName as Despepite,Truks.OutDat AS FechaSalida,Truks.InDat AS FechaLlegada,Truks.Status,Clients.BnName as Cliente,
    Transports.BnName as Linea,Truks.DrvNam AS CHOFER,ROUND(((Truks.OutWgh/Truks.CrgQty)*Lots.Qty),2) as Bascula,DOrds.Ctc AS Contrato,Truks.StatusNav,Truks.CommentNav,DOrds.Typ,Truks.Samples,Truks.ComentarioProveed as comentario,
    IF(((Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd))=99001,
    (Select Town From amsadb1.Clients Where  Clients.CliID = (Select InPlc From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd)),
    ((Select Town From amsadb1.Region Where Region.IDReg =(Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd) ))) as CiudadLlegada,R1.RegNam as RegionLlegada,
    ROUND((Lots.LiqWgh/Lots.Qty),2) as AVGPwg, ROUND((Truks.OutWgh/Truks.CrgQty),2) as AVGOutWgh
    From amsadb1.Lots
    LEFT JOIN amsadb1.Truks
    ON Lots.TrkID = Truks.TrkID
    LEFT JOIN amsadb1.DOrds
    ON  Lots.DOrd = DOrds.DOrd
    LEFT JOIN amsadb1.Clients
    ON Clients.CliID = DOrds.InPlc
    LEFT JOIN amsadb1.Gines
    ON  Lots.GinID=Gines.IDGin 
    LEFT JOIN amsadb1.Region
    ON  DOrds.OutPlc=Region.IDReg 
    LEFT JOIN amsadb1.Transports
    ON Truks.TNam = Transports .TptID
    LEFT JOIN amsadb1.Region R1
    ON  DOrds.InReg=R1.IDReg 
    where Truks.Status ='$StatusTrk' AND  Region.RegNam IN ('$regionessalida') AND R1.RegNam IN ('$regionesllegada') AND Truks.OutDat BETWEEN '$fromdate' AND '$todate'".$complemento;
//'Transit','Received'

}


//filtrar solo por estatus de truck  
if($regsalida =="" && $regllegada== "" && $StatusTrk !="All"){    
    $consulta="SELECT Lots.TrkID,Lots.DOrd,Lots.Lot,Lots.Qty,Region.RegNam as RegionSalida,Gines.GinName as Despepite,Truks.OutDat AS FechaSalida,Truks.InDat AS FechaLlegada,Truks.Status,Clients.BnName as Cliente,
    Transports.BnName as Linea,Truks.DrvNam AS CHOFER,ROUND(((Truks.OutWgh/Truks.CrgQty)*Lots.Qty),2) as Bascula,DOrds.Ctc AS Contrato,Truks.StatusNav,Truks.CommentNav,DOrds.Typ,Truks.Samples,Truks.ComentarioProveed as comentario,
    IF(((Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd))=99001,
    (Select Town From amsadb1.Clients Where  Clients.CliID = (Select InPlc From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd)),
    ((Select Town From amsadb1.Region Where Region.IDReg =(Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd) ))) as CiudadLlegada,R1.RegNam as RegionLlegada,
    ROUND((Lots.LiqWgh/Lots.Qty),2) as AVGPwg, ROUND((Truks.OutWgh/Truks.CrgQty),2) as AVGOutWgh
    From amsadb1.Lots
    LEFT JOIN amsadb1.Truks
    ON Lots.TrkID = Truks.TrkID
    LEFT JOIN amsadb1.DOrds
    ON  Lots.DOrd = DOrds.DOrd
    LEFT JOIN amsadb1.Clients
    ON Clients.CliID = DOrds.InPlc
    LEFT JOIN amsadb1.Gines
    ON  Lots.GinID=Gines.IDGin 
    LEFT JOIN amsadb1.Region
    ON  DOrds.OutPlc=Region.IDReg 
    LEFT JOIN amsadb1.Transports
    ON Truks.TNam = Transports .TptID
    LEFT JOIN amsadb1.Region R1
    ON  DOrds.InReg=R1.IDReg 
    where Truks.Status ='$StatusTrk'  AND Truks.OutDat BETWEEN '$fromdate' AND '$todate'".$complemento;
//'Transit','Received'

}
//fwrite($filetext, $consulta. PHP_EOL);
//fclose($filetext);

$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$siexiste=0; //$data=$resultado->fetchAll(PDO::FETCH_ASSOC);

//Define the filename with current date
$fileName = "Transits-".date('d-m-Y').".xlsx";

$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Transits");
//$hojaActiva->freezePane("A2");

$hojaActiva->getColumnDimension('A')->setWidth(12);
$hojaActiva->setCellValue('A1','TruckID');
$hojaActiva->getColumnDimension('B')->setWidth(12);
$hojaActiva->setCellValue('B1','Type');
$hojaActiva->getColumnDimension('C')->setWidth(12);
$hojaActiva->setCellValue('C1','DO');
$hojaActiva->getColumnDimension('D')->setWidth(12);
$hojaActiva->setCellValue('D1','Lot');
$hojaActiva->getColumnDimension('E')->setWidth(12);
$hojaActiva->setCellValue('E1','Bales');
$hojaActiva->getColumnDimension('F')->setWidth(12);
$hojaActiva->setCellValue('F1','Departure Date');
$hojaActiva->getColumnDimension('G')->setWidth(12);
$hojaActiva->setCellValue('G1','Arrival Date');
$hojaActiva->getColumnDimension('H')->setWidth(12);
$hojaActiva->setCellValue('H1','Gin');
$hojaActiva->getColumnDimension('I')->setWidth(12);
$hojaActiva->setCellValue('I1','Departure Region');
$hojaActiva->getColumnDimension('J')->setWidth(12);
$hojaActiva->setCellValue('J1','Arrival Region');
$hojaActiva->getColumnDimension('K')->setWidth(12);
$hojaActiva->setCellValue('K1','Arrival City');
$hojaActiva->getColumnDimension('L')->setWidth(12);
$hojaActiva->setCellValue('L1','Cliente');
$hojaActiva->getColumnDimension('M')->setWidth(12);
$hojaActiva->setCellValue('M1','DO Contract');
$hojaActiva->getColumnDimension('N')->setWidth(12);
$hojaActiva->setCellValue('N1','Departure Weight');
$hojaActiva->getColumnDimension('O')->setWidth(12);
$hojaActiva->setCellValue('O1','Departure Weight AVG');
$hojaActiva->getColumnDimension('P')->setWidth(12);
$hojaActiva->setCellValue('P1','Purchase Weigth AVG');
$hojaActiva->getColumnDimension('Q')->setWidth(12);
$hojaActiva->setCellValue('Q1','Dif weight');
$hojaActiva->getColumnDimension('R')->setWidth(12);
$hojaActiva->setCellValue('R1','Transport');
$hojaActiva->getColumnDimension('S')->setWidth(12);
$hojaActiva->setCellValue('S1','Driver Name');
$hojaActiva->getColumnDimension('T')->setWidth(12);
$hojaActiva->setCellValue('T1','Status');
$hojaActiva->getColumnDimension('U')->setWidth(12);
$hojaActiva->setCellValue('U1','In NAV');
$hojaActiva->getColumnDimension('V')->setWidth(12);
$hojaActiva->setCellValue('V1','NAV Coment');
$hojaActiva->getColumnDimension('W')->setWidth(12);
$hojaActiva->setCellValue('W1','Samples');
$hojaActiva->getColumnDimension('X')->setWidth(12);
$hojaActiva->setCellValue('X1','Proveed Coment');
//$hojaActiva->getColumnDimension('S')->setWidth(12);
//$hojaActiva->setCellValue('R1','Status NAV');

$hojaActiva->getStyle('A1:X1')->getFont()->setBold( true );

//relleno de celda encabezado
$hojaActiva->getStyle('A1:X1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFE1E1E1');

//fijar primera fila
$hojaActiva->freezePane('A2');


$fila = 2;
$fecha_salida = "";
$trkanterior = 0;
while($row = $resultado->fetch(PDO::FETCH_ASSOC)){
    if($siexiste==0){
        if(!empty($row['TrkID']))
            $siexiste=1;
    }

    $fecha_salida = strtotime($row['FechaSalida']);
    $fecha_llegada = strtotime($row['FechaLlegada']);

    $fecha_salida =25569 + ($fecha_salida / 86400);
    $fecha_llegada =25569 + ($fecha_llegada / 86400);


    $hojaActiva->getStyle('F' . $fila)->getNumberFormat()//formato de fecha separado por /
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    $hojaActiva->getStyle('G' . $fila)->getNumberFormat()//formato de fecha separado por /
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);

    //$fecha = str_replace("-","/",$row['FechaSalida']);
    if($row['StatusNav']==1){
        $valor = "Y";
    }
    else{
        $valor = "";
    }
    //GUARDAR LOS TRUCKS QUE SON IGUALES
    if($trkanterior == $row['TrkID']){
    
        array_push($myarray,($fila-1));
        array_push($myarray,$fila);
    }


    $difpeso = $row['AVGOutWgh']-$row['AVGPwg'];

    
    //si diferencia de pesos es mayor a -0.5 pintamos de rojo la celda
    if($difpeso < -0.5 && $row['Bascula'] > 0){
        $hojaActiva->getStyle('Q' . $fila)->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('FFFF1A07');
    }
    $samples="NO";
    if($row['Samples']== 1){
        $samples="YES";
    }


    $hojaActiva->setCellValue('A' . $fila,$row['TrkID']);
    $hojaActiva->setCellValue('B' . $fila,$row['Typ']);
    $hojaActiva->setCellValue('C' . $fila,$row['DOrd']);
    //$hojaActiva->setCellValue('D'. $fila,$row['Lot']); 
    $hojaActiva->setCellValue('E' . $fila,$row['Qty']);
    $hojaActiva->setCellValue('F' . $fila,$fecha_salida);
    $hojaActiva->setCellValue('G' . $fila,$fecha_llegada);
    $hojaActiva->setCellValue('H' . $fila,$row['Despepite']);
    $hojaActiva->setCellValue('I' . $fila,$row['RegionSalida']);    
    $hojaActiva->setCellValue('J'. $fila,$row['RegionLlegada']);
    $hojaActiva->setCellValue('K'. $fila,$row['CiudadLlegada']);
    $hojaActiva->setCellValue('L'. $fila,$row['Cliente']);
    $hojaActiva->setCellValue('M'. $fila,$row['Contrato']);
    $hojaActiva->setCellValue('N'. $fila,$row['Bascula']);
    $hojaActiva->setCellValue('O'. $fila,$row['AVGOutWgh']);  
    $hojaActiva->setCellValue('P'. $fila,$row['AVGPwg']);  
    $hojaActiva->setCellValue('Q'. $fila,$difpeso);  
    $hojaActiva->setCellValue('R'. $fila,$row['Linea']);  
    $hojaActiva->setCellValue('S'. $fila,$row['CHOFER']);  
    $hojaActiva->setCellValue('T'. $fila,$row['Status']);  
    $hojaActiva->setCellValue('U'. $fila,$valor);  
    $hojaActiva->setCellValue('V'. $fila,$row['CommentNav']); 
    $hojaActiva->setCellValue('W'. $fila,$samples); 
    $hojaActiva->setCellValue('X'. $fila,$row['comentario']); 

    $style = $hojaActiva->getStyle('D'. $fila);

    $style->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
    
    $hojaActiva->getCell('D'. $fila)->setValueExplicit($row['Lot'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
    
    $trkanterior = $row['TrkID'];
    $fila++;
}

$hojaActiva->getStyle('N2:N'.$fila)->getNumberFormat()->setFormatCode('###0.00');

//COLOREAR LA CELDAS QUE TIENEN EL MISMO TRKID
$longitud = count($myarray);
for($i=0; $i<$longitud; $i++){

    $hojaActiva->getStyle('A' . $myarray[$i])->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFFFFF00');

}



// redirect output to client browser
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');
//print json_encode($data, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX
$writer = IOFactory::createWriter($excel, 'Xlsx');

$writer->save('php://output');
exit;
$conexion=null;





?>