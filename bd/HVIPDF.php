<?php

setlocale(LC_TIME, "spanish");

function lowercase($element)
{
    return "'".$element."'";
}

$DO = $_GET['DO'];
$Lots = $_GET['Lots'];
$Crp = $_GET['Crp'];
$TypHVI = $_GET['TypHVI'];
$Cli = $_GET['Cli'];


include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

include_once '../fpdf/fpdf.php';

class PDF extends FPDF
{
   //Cabecera de página
   function Header()
   {
    $this->SetFont('Arial','B',10);
    $this->Image('../img/logo1.png', 11, 10, 10, 10, 'PNG');
    $this->Cell(0,6, utf8_decode('AGROINDUSTRIAS UNIDAS DE MEXICO S.A. DE C.V.'),0,0,'C');
    $this->Ln(4);
    $this->SetFont('Arial','B',9);
    $this->Cell(0,7,'COTTON DIVISION', 0, 0,'C');
    $this->Ln();
    $this->SetFont('Arial','B', 9);
    $this->Cell(26,5, utf8_decode('Bales'),'TB',0,'C');
    $this->Cell(15,5, utf8_decode('Grd'),'TB',0,'C');
    $this->Cell(15,5, utf8_decode('Trs'),'TB',0,'C');
    $this->Cell(19,5, utf8_decode('Mic'),'TB',0,'C');
    $this->Cell(15,5, utf8_decode('Len'),'TB',0,'C');
    $this->Cell(17,5, utf8_decode('Str'),'TB',0,'C');
    $this->Cell(19,5, utf8_decode('Unf'),'TB',0,'C');
    $this->Cell(13,5, utf8_decode('Rd'),'TB',0,'C');
    $this->Cell(13,5, utf8_decode('b'),'TB',0,'C');
    $this->Cell(15,5, utf8_decode('Col'),'TB',0,'C');
    $this->Cell(15,5, utf8_decode('Tar'),'TB',0,'C');
    $this->Cell(15,5, utf8_decode('Tcnt'),'TB',0,'C');
    $this->Cell(13,5, utf8_decode('Sfi'),'TB',0,'C');
    $this->Cell(19,5, utf8_decode('Elg'),'TB',0,'C');
    $this->Cell(17,5, utf8_decode('Mat'),'TB',0,'C');
    $this->Cell(15,5, utf8_decode('Mst'),'TB',0,'C');
    $this->Ln();
   }

   //Pie de página
   function Footer()
   {
       // Position at 1.5 cm from bottom
       $this->SetY(-15);
       // Arial italic 8
       $this->SetFont('Arial','I',8);
       // Page number
       $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
   }
}


/*$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('Landscape','Letter');
$pdf->SetTitle("HVI ".$Cli);*/

//Si la busqueda es por DO
if ($DO != ""){
    $Lots = array();
    $i = 0;
    $consulta = 'SELECT distinct Bales.Lot FROM Bales, DOrds, Lots WHERE DOrds.DOrd = '.$DO.' and DOrds.DOrd = Lots.DOrd and Bales.Lot = Lots.Lot;'; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataDO=$resultado->fetchALL(PDO::FETCH_ASSOC);
    
}else{
    //$array = array_map('lowercase', explode(',', $Lots)); 
    $array = explode(",",$Lots);
}

$pdf = new PDF();

if ($DO == ""){

    //Consultas preparadas
    $cols = ['Bal', 'Col1', 'Tgrd', 'Mic', 'Len', 'Str', 'Unf', 'Rd', 'b', 'Tar', 'Col2', 'Tcnt', 'Sfi', 'Elg', 'Mat', 'Mst'];
    if ($TypHVI != 'O') {
        $cols = ['Bal', 'Col1_Mod as Col1', 'Tgrd_Mod as Tgrd', 'Mic_Mod as Mic', 'Len_Mod as Len', 'Str_Mod as Str', 'Unf_Mod as Unf', 'Rd_Mod as Rd', 'b_Mod as b', 'Tar_Mod as Tar', 'Col2_Mod as Col2', 'Tcnt_Mod as Tcnt', 'Sfi_Mod as Sfi', 'Elg_Mod as Elg', 'Mat_Mod as Mat', 'Mst_Mod as Mst'];
    }
    
    $placeholders = implode(",", array_fill(0, count($cols), "?"));

    $consulta = "SELECT " . implode(",", $cols) . " FROM Bales WHERE Lot = ? ORDER BY Bal ASC";
    $consultaMedia = "SELECT count(Bal) as SumBal, avg(Mic) as avgMic, avg(Tgrd) as avgTgrd, avg(Tar) as avgTar, avg(Tcnt) as avgTcnt, avg(Unf) as avgUnf, avg(Len) as avgLen, avg(Sfi) as avgSfi, avg(Str) as avgStr, avg(Elg) as avgElg, avg(Mat) as avgMat, avg(Rd) as avgRd, avg(b) as avgb FROM Bales WHERE Lot = ?";
    if ($TypHVI != 'O') {
    $consultaMedia = "SELECT count(Bal) as SumBal, avg(Mic_Mod) as avgMic, avg(Tgrd_Mod) as avgTgrd, avg(Tar_Mod) as avgTar, avg(Tcnt_Mod) as avgTcnt, avg(Unf_Mod) as avgUnf, avg(Len_Mod) as avgLen, avg(Sfi_Mod) as avgSfi, avg(Str_Mod) as avgStr, avg(Elg_Mod) as avgElg, avg(Mat_Mod) as avgMat, avg(Rd_Mod) as avgRd, avg(b_Mod) as avgb FROM Bales WHERE Lot = ?";
    }

    $stmt = $conexion->prepare($consulta);
    $stmtMedia = $conexion->prepare($consultaMedia);
    
    foreach ($array as $dato){ //for ($i= 0; $i < array_count_values($array); $i++){ //
    
    $pdf->AliasNbPages();
    $pdf->AddPage('Landscape','Letter');
    $pdf->SetTitle("HVI ".$Cli);
    
    $stmt->execute([$dato]);
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
    $stmtMedia->execute([$dato]);
    $dataAvg = $stmtMedia->fetchAll(PDO::FETCH_ASSOC);
    
    /*if ($TypHVI == 'O'){
        $consulta = "SELECT Bal, Col1, Tgrd, Mic, Len, Str, Unf, Rd, b, Tar, Col2, Tcnt, Sfi, Elg, Mat, Mst FROM Bales WHERE Lot = '$dato';"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);

        $consulta = "SELECT count(Bal) as SumBal, avg(Mic) as avgMic, avg(Tgrd) as avgTgrd, avg(Tar) as avgTar, avg(Tcnt) as avgTcnt, avg(Unf) as avgUnf, avg(Len) as avgLen, avg(Sfi) as avgSfi, avg(Str) as avgStr, avg(Elg) as avgElg, avg(Mat) as avgMat, avg(Rd) as avgRd, avg(b) as avgb FROM Bales WHERE Lot = '$dato';"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataAvg=$resultado->fetchAll(PDO::FETCH_ASSOC);
    }else{
        $consulta = "SELECT Bal, Col1_Mod as Col1, Tgrd_Mod as Tgrd, Mic_Mod as Mic, Len_Mod as Len, Str_Mod as Str, Unf_Mod as Unf, Rd_Mod as Rd, b_Mod as b, Tar_Mod as Tar, Col2_Mod as Col2, Tcnt_Mod as Tcnt, Sfi_Mod as Sfi, Elg_Mod as Elg, Mat_Mod as Mat, Mst_Mod as Mst FROM Bales WHERE Lot = '$dato';"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);

        $consulta = "SELECT count(Bal) as SumBal, avg(Mic_Mod) as avgMic, avg(Tgrd_Mod) as avgTgrd, avg(Tar_Mod) as avgTar, avg(Tcnt_Mod) as avgTcnt, avg(Unf_Mod) as avgUnf, avg(Len_Mod) as avgLen, avg(Sfi_Mod) as avgSfi, avg(Str_Mod) as avgStr, avg(Elg_Mod) as avgElg, avg(Mat_Mod) as avgMat, avg(Rd_Mod) as avgRd, avg(b_Mod) as avgb FROM Bales WHERE Lot = '$dato';"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataAvg=$resultado->fetchAll(PDO::FETCH_ASSOC);
    }*/


    $pdf->Cell(26,4, utf8_decode($dato),'TB',0,'C');
    $pdf->Cell(0,4, utf8_decode(''),'TB',0,'C');

    foreach ($data as $row) { //foreach($data as $row){
        $pdf->Ln();
        $pdf->SetFont('Arial','', 9);
        $pdf->Cell(26,3,($row['Bal']),0,0,'C');
        $pdf->Cell(15,3, ($row['Col1']),0,0,'C');
        $pdf->Cell(15,3, ($row['Tgrd']),0,0,'C');
        $pdf->Cell(19,3, (number_format(Round($row['Mic'],1),1)),0,0,'C');
        $pdf->Cell(15,3, (number_format(Round($row['Len'],2),2)),0,0,'C');
        $pdf->Cell(17,3, (number_format(Round($row['Str'],1),1)),0,0,'C');
        $pdf->Cell(19,3, (number_format(Round($row['Unf'],1),1)),0,0,'C');
        $pdf->Cell(13,3, (number_format(Round($row['Rd'],1),1)),0,0,'C');
        $pdf->Cell(13,3, (number_format(Round($row['b'],1),1)),0,0,'C');
        $pdf->Cell(15,3, ($row['Col1']."-".$row['Col2']),0,0,'C');
        $pdf->Cell(15,3, (number_format(Round($row['Tar'],2),2)),0,0,'C');
        $pdf->Cell(15,3, ($row['Tcnt']),0,0,'C');
        $pdf->Cell(13,3, (number_format(Round($row['Sfi'],1),1)),0,0,'C');
        $pdf->Cell(19,3, (number_format(Round($row['Elg'],1),1)),0,0,'C');
        $pdf->Cell(17,3, (number_format(Round($row['Mat'],2),2)),0,0,'C');
        $pdf->Cell(15,3, (number_format(Round($row['Mst'],1),1)),0,0,'C');
    }
    foreach ($dataAvg as $rowAvg) {
        $pdf->Ln();
        $pdf->SetFont('Arial','B', 9);
        $pdf->Cell(26,4, utf8_decode($rowAvg['SumBal']." Bales"),'T',0,'C');
        $pdf->Cell(15,4, utf8_decode(''),'T',0,'C');
        $pdf->Cell(15,4, utf8_decode(Round($rowAvg['avgTgrd'],0)),'T',0,'C');
        $pdf->Cell(19,4, utf8_decode(number_format(Round($rowAvg['avgMic'],2),2)),'T',0,'C');
        $pdf->Cell(15,4, utf8_decode(number_format(Round($rowAvg['avgLen'],2),2)),'T',0,'C');
        $pdf->Cell(17,4, utf8_decode(number_format(Round($rowAvg['avgStr'],2),2)),'T',0,'C');
        $pdf->Cell(19,4, utf8_decode(number_format(Round($rowAvg['avgUnf'],2),2)),'T',0,'C');
        $pdf->Cell(13,4, utf8_decode(number_format(Round($rowAvg['avgRd'],2),2)),'T',0,'C');
        $pdf->Cell(13,4, utf8_decode(number_format(Round($rowAvg['avgb'],2),2)),'T',0,'C');
        $pdf->Cell(15,4, utf8_decode(''),'T',0,'C');
        $pdf->Cell(15,4, utf8_decode(number_format(Round($rowAvg['avgTar'],2),2)),'T',0,'C');
        $pdf->Cell(15,4, utf8_decode(number_format(Round($rowAvg['avgTcnt'],2),2)),'T',0,'C');
        $pdf->Cell(13,4, utf8_decode(number_format(Round($rowAvg['avgSfi'],2),2)),'T',0,'C');
        $pdf->Cell(19,4, utf8_decode(number_format(Round($rowAvg['avgElg'],2),2)),'T',0,'C');
        $pdf->Cell(17,4, utf8_decode(number_format(Round($rowAvg['avgMat'],2),2)),'T',0,'C');
        $pdf->Cell(15,4, utf8_decode(''),'T',0,'C');
    }

    }
}else{
    
    foreach ($dataDO as $dato){ //for ($i= 0; $i < array_count_values($array); $i++){ //

        $pdf->AliasNbPages();
        $pdf->AddPage('Landscape','Letter');
        $pdf->SetTitle("HVI ".$Cli);
        
        

    if ($TypHVI == 'O'){
            $consulta = 'SELECT Bal, Col1, Tgrd, Mic, Len, Str, Unf, Rd, b, Tar, Col2, Tcnt, Sfi, Elg, Mat, Mst FROM Bales WHERE Lot = '.$dato['Lot'].' ORDER BY Bal ASC;'; 
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    
            $consulta = 'SELECT count(Bal) as SumBal, avg(Mic) as avgMic, avg(Tgrd) as avgTgrd, avg(Tar) as avgTar, avg(Tcnt) as avgTcnt, avg(Unf) as avgUnf, avg(Len) as avgLen, avg(Sfi) as avgSfi, avg(Str) as avgStr, avg(Elg) as avgElg, avg(Mat) as avgMat, avg(Rd) as avgRd, avg(b) as avgb FROM Bales WHERE Lot = '.$dato['Lot'].';'; 
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $dataAvg=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }else{
            $consulta = 'SELECT Bal, Col1_Mod as Col1, Tgrd_Mod as Tgrd, Mic_Mod as Mic, Len_Mod as Len, Str_Mod as Str, Unf_Mod as Unf, Rd_Mod as Rd, b_Mod as b, Tar_Mod as Tar, Col2_Mod as Col2, Tcnt_Mod as Tcnt, Sfi_Mod as Sfi, Elg_Mod as Elg, Mat_Mod as Mat, Mst_Mod as Mst FROM Bales WHERE Lot = '.$dato['Lot'].' ORDER BY Bal ASC;'; 
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    
            $consulta = 'SELECT count(Bal) as SumBal, avg(Mic_Mod) as avgMic, avg(Tgrd_Mod) as avgTgrd, avg(Tar_Mod) as avgTar, avg(Tcnt_Mod) as avgTcnt, avg(Unf_Mod) as avgUnf, avg(Len_Mod) as avgLen, avg(Sfi_Mod) as avgSfi, avg(Str_Mod) as avgStr, avg(Elg_Mod) as avgElg, avg(Mat_Mod) as avgMat, avg(Rd_Mod) as avgRd, avg(b_Mod) as avgb FROM Bales WHERE Lot = '.$dato['Lot'].';'; 
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $dataAvg=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }
    
    
        $pdf->Cell(26,4, utf8_decode($dato['Lot']),'TB',0,'C');
        $pdf->Cell(0,4, utf8_decode(''),'TB',0,'C');
    
        foreach ($data as $row) { //foreach($data as $row){
            $pdf->Ln();
            $pdf->SetFont('Arial','', 9);
            $pdf->Cell(26,3,($row['Bal']),0,0,'C');
            $pdf->Cell(15,3, ($row['Col1']),0,0,'C');
            $pdf->Cell(15,3, ($row['Tgrd']),0,0,'C');
            $pdf->Cell(19,3, (number_format(Round($row['Mic'],1),1)),0,0,'C');
            $pdf->Cell(15,3, (number_format(Round($row['Len'],2),2)),0,0,'C');
            $pdf->Cell(17,3, (number_format(Round($row['Str'],1),1)),0,0,'C');
            $pdf->Cell(19,3, (number_format(Round($row['Unf'],1),1)),0,0,'C');
            $pdf->Cell(13,3, (number_format(Round($row['Rd'],1),1)),0,0,'C');
            $pdf->Cell(13,3, (number_format(Round($row['b'],1),1)),0,0,'C');
            $pdf->Cell(15,3, ($row['Col1']."-".$row['Col2']),0,0,'C');
            $pdf->Cell(15,3, (number_format(Round($row['Tar'],2),2)),0,0,'C');
            $pdf->Cell(15,3, ($row['Tcnt']),0,0,'C');
            $pdf->Cell(13,3, (number_format(Round($row['Sfi'],1),1)),0,0,'C');
            $pdf->Cell(19,3, (number_format(Round($row['Elg'],1),1)),0,0,'C');
            $pdf->Cell(17,3, (number_format(Round($row['Mat'],2),2)),0,0,'C');
            $pdf->Cell(15,3, (number_format(Round($row['Mst'],1),1)),0,0,'C');
        }
        foreach ($dataAvg as $rowAvg) {
            $pdf->Ln();
            $pdf->SetFont('Arial','B', 9);
            $pdf->Cell(26,4, utf8_decode($rowAvg['SumBal']." Bales"),'T',0,'C');
            $pdf->Cell(15,4, utf8_decode(''),'T',0,'C');
            $pdf->Cell(15,4, utf8_decode(Round($rowAvg['avgTgrd'],0)),'T',0,'C');
            $pdf->Cell(19,4, utf8_decode(number_format(Round($rowAvg['avgMic'],2),2)),'T',0,'C');
            $pdf->Cell(15,4, utf8_decode(number_format(Round($rowAvg['avgLen'],2),2)),'T',0,'C');
            $pdf->Cell(17,4, utf8_decode(number_format(Round($rowAvg['avgStr'],2),2)),'T',0,'C');
            $pdf->Cell(19,4, utf8_decode(number_format(Round($rowAvg['avgUnf'],2),2)),'T',0,'C');
            $pdf->Cell(13,4, utf8_decode(number_format(Round($rowAvg['avgRd'],2),2)),'T',0,'C');
            $pdf->Cell(13,4, utf8_decode(number_format(Round($rowAvg['avgb'],2),2)),'T',0,'C');
            $pdf->Cell(15,4, utf8_decode(''),'T',0,'C');
            $pdf->Cell(15,4, utf8_decode(number_format(Round($rowAvg['avgTar'],2),2)),'T',0,'C');
            $pdf->Cell(15,4, utf8_decode(number_format(Round($rowAvg['avgTcnt'],2),2)),'T',0,'C');
            $pdf->Cell(13,4, utf8_decode(number_format(Round($rowAvg['avgSfi'],2),2)),'T',0,'C');
            $pdf->Cell(19,4, utf8_decode(number_format(Round($rowAvg['avgElg'],2),2)),'T',0,'C');
            $pdf->Cell(17,4, utf8_decode(number_format(Round($rowAvg['avgMat'],2),2)),'T',0,'C');
            $pdf->Cell(15,4, utf8_decode(''),'T',0,'C');
        }
    
        }
}

$pdf->Output('I', "HVI ".$Cli." ".date('d-M-Y').".pdf");

$conexion=null;

?>