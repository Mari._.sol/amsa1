<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

$Cli = (isset($_POST['Cli'])) ? $_POST['Cli'] : '';
$BnName = (isset($_POST['BnName'])) ? $_POST['BnName'] : '';
$Cty = (isset($_POST['Cty'])) ? $_POST['Cty'] : '';
$State = (isset($_POST['State'])) ? $_POST['State'] : '';
$Town = (isset($_POST['Town'])) ? $_POST['Town'] : '';
$CP = (isset($_POST['CP'])) ? $_POST['CP'] : '';
$Drctn = (isset($_POST['Drctn'])) ? $_POST['Drctn'] : '';
$Ref = (isset($_POST['Ref'])) ? $_POST['Ref'] : '';
$Ct1 = (isset($_POST['Ct1'])) ? $_POST['Ct1'] : '';
$Tel1 = (isset($_POST['Tel1'])) ? $_POST['Tel1'] : '';
$Ext1 = (isset($_POST['Ext1'])) ? $_POST['Ext1'] : '';
$Ct2 = (isset($_POST['Ct2'])) ? $_POST['Ct2'] : '';
$Tel2 = (isset($_POST['Tel2'])) ? $_POST['Tel2'] : '';
$Ext2 = (isset($_POST['Ext2'])) ? $_POST['Ext2'] : '';
$Ct3 = (isset($_POST['Ct3'])) ? $_POST['Ct3'] : '';
$Tel3 = (isset($_POST['Tel3'])) ? $_POST['Tel3'] : '';
$Ext3 = (isset($_POST['Ext3'])) ? $_POST['Ext3'] : '';
$Cde = (isset($_POST['Cde'])) ? $_POST['Cde'] : '';
$Cmt = (isset($_POST['Cmt'])) ? $_POST['Cmt'] : '';
$RFC = (isset($_POST['RFC'])) ? $_POST['RFC'] : '';
$ClientGrp = (isset($_POST['ClientGrp'])) ? $_POST['ClientGrp'] : '';
$coordenadas = (isset($_POST['maps'])) ? $_POST['maps'] : '';

$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$CliID = (isset($_POST['CliID'])) ? $_POST['CliID'] : '';

$estado = (isset($_POST['estado'])) ? $_POST['estado'] : '';
switch($opcion){
    case 1:
        $consulta = "INSERT INTO Clients (Cli, BnName, RFC, Cty, State, Town, CP, Drctn, Ref, Ct1, Tel1, Ext1, Ct2, Tel2, Ext2, Ct3, Tel3, Ext3, Cmt, Cde, ClientGrp, Maps) VALUES('$Cli', '$BnName', '$RFC', '$Cty', '$State', '$Town', '$CP', '$Drctn', '$Ref', '$Ct1', '$Tel1', '$Ext1', '$Ct2', '$Tel2', '$Ext2', '$Ct3', '$Tel3', '$Ext3', '$Cmt', '$Cde','$ClientGrp','$coordenadas') ";			
        $resultado = $conexion->prepare($consulta);
        $resultado->execute(); 
        
        $consulta = "SELECT CliID, Cli, BnName, Cty, State, Town, CP, Drctn, Ref, Ct1, Tel1, Ext1, Ct2, Tel2, Ext2, Ct3, Tel3, Ext3, Cmt, Cde, ClientGrp, Maps FROM Clients ORDER BY CliID DESC LIMIT 1";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;    
    case 2:        
        $consulta = "UPDATE Clients SET Cli='$Cli', BnName='$BnName', RFC='$RFC', Cty='$Cty', State='$State', Town='$Town', CP='$CP', Drctn='$Drctn', Ref='$Ref', Ct1='$Ct1', Tel1='$Tel1', Ext1='$Ext1', Ct2='$Ct2', Tel2='$Tel2', Ext2='$Ext2', Ct3='$Ct3', Tel3='$Tel3', Ext3='$Ext3', Cmt='$Cmt', Cde='$Cde',ClientGrp = '$ClientGrp', Maps = '$coordenadas' WHERE CliID='$CliID' ";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        
        $consulta = "SELECT CliID, Cli, BnName, Cty, State, Town, CP, Drctn, Ref, Ct1, Tel1, Ext1, Ct2, Tel2, Ext2, Ct3, Tel3, Ext3, Cmt, Cde, ClientGrp, Maps FROM Clients WHERE CliID='$CliID' ";       
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 3:        
        $consulta = "UPDATE Invs Set InvSta='Cancelled' WHERE InvID='$InvID'"; //"DELETE FROM Lots WHERE LotID='$LotID' ";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();                           
        break;
    case 4:    
        $consulta = "SELECT * FROM Clients ORDER BY Cli;";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 5:    
        $consulta = "SELECT Ctc FROM DOrds WHERE DOrd='$DO'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 6:
        $consulta = "SELECT  City FROM Cities where State = '$estado'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();  
          
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        sort($data);
        break;
}

print json_encode($data, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX
$conexion=null;
