<?php

include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

//Define the filename with current date
$fileName = "Requisition-".date('d-m-Y').".xls";

//Set header information to export data in excel format
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename='.$fileName);

//Set variable to false for heading
$heading = false;

$consulta = "Select Truks.TrkID, WBill,
concat(concat((IF((Select IsOrigin From Region Where DOrds.OutPlc = IDReg)=1,(Select Cde from Gines where Truks.TrkID = Lots.TrkID and Lots.GinID = Gines.IDGin),(Select Cde from Region where Truks.TrkID = Lots.TrkID and DOrds.OutPlc = Region.IDReg))),'-',
(IF((Select DOrds.Typ From DOrds Where Truks.DO = DOrds.DOrd)='CON',(Select Cde From Region Where DOrds.InReg = Region.IDReg),(Select Cde From Clients Where DOrds.InPlc = Clients.CliID)))),'/',Truks.DO) as DO, Lots.Lot, Lots.Qty, (Select BnName From Transports Where Truks.TNam = Transports.TptID) as Transport, (Truks.FreightCost/Truks.CrgQty*Lots.Qty) as Cost, RqstID as PR, PO
From Truks, Lots, DOrds
Where Truks.DO = DOrds.DOrd and Truks.TrkID = Lots.TrkID and RqstID<>0 and PO =0 ORDER BY Transport ASC, TrkID ASC;";
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$data=$resultado->fetchAll(PDO::FETCH_ASSOC);

if(!empty($data)) {
foreach($data as $item) {
if(!$heading) {
echo implode("\t", array_keys($item)) . "\n";
$heading = true;
}
echo implode("\t", array_values($item)) . "\n";
}
}
exit();


?>




<!--
Select Truks.TrkID, Truks.DO as DO, Lots.Lot, Lots.Qty, (Select BnName From Transports Where Truks.TNam = Transports.TptID) as Transport, (Truks.FreightCost/Truks.CrgQty*Lots.Qty) as Cost, RqstID as PR, PO
From Truks, Lots
Where Truks.TrkID = Lots.TrkID and RqstID<>0 and PO =0 ORDER BY TrkID;
-->
