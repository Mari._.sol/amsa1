<?php
  require '../vendor/autoload.php';
  
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\SMTP;
  use Aws\S3\S3Client; 
  use Aws\Exception\AwsException;

  include_once '../bd/conexion.php';
  $objeto = new Conexion();
  $conexion = $objeto->Conectar();

  require './vendor/phpmailer/phpmailer/src/PHPMailer.php';
  require './vendor/phpmailer/phpmailer/src/SMTP.php';
  require './vendor/autoload.php';
  date_default_timezone_set('America/Los_Angeles');
  
  $usuario= (isset($_POST['usuario'])) ? $_POST['usuario'] : '';
  $LiqID= (isset($_POST['LiqID'])) ? $_POST['LiqID'] : '';
  $TipoLiq= (isset($_POST['TipoLiq'])) ? $_POST['TipoLiq'] : '';
  $mails= (isset($_POST['mails'])) ? $_POST['mails'] : '';
  $subject= (isset($_POST['subject'])) ? $_POST['subject'] : '';
  $body= (isset($_POST['body'])) ? $_POST['body'] : '';
  $comentarios= (isset($_POST['comentarios'])) ? $_POST['comentarios'] : '';


  $bucket = 'pruebasportal';
  $keyname = 'Liq'.$TipoLiq."_".$LiqID.'.xlsx';
  $keyname2 = 'Liq'.$TipoLiq."_".$LiqID.'.png';
  $keyname3 = 'Price'.$TipoLiq."_".$LiqID.'.png';
  $filepath = './temp/'.$keyname;
  $filepath2 = './temp/'.$keyname2;
  $filepath3 = './temp/'.$keyname3;

  $s3 = new S3Client([
    'version'     => 'latest',
    'region'      => 'us-east-2', //bucket de pruebas
    //'region'      => 'us-east-1',
    'credentials' => [
      'key'    => 'AKIAT442VUCJQXPEFVZW',
        'secret' => '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',
    ],
  ]);

  try {
    $result = $s3->getObject([
        'Bucket' => $bucket,
        'Key'    => $keyname,
        'SaveAs' => $filepath
    ]);
  }
  catch (S3Exception $e) {
      echo $e->getMessage() . PHP_EOL;
  }

  try {
    $result = $s3->getObject([
        'Bucket' => $bucket,
        'Key'    => $keyname2,
        'SaveAs' => $filepath2
    ]);
  }
  catch (S3Exception $e) {
      echo $e->getMessage() . PHP_EOL;
  }

  try {
    $result = $s3->getObject([
        'Bucket' => $bucket,
        'Key'    => $keyname3,
        'SaveAs' => $filepath3
    ]);
  }
  catch (S3Exception $e) {
      echo $e->getMessage() . PHP_EOL;
  }

  $query = 'SELECT User, email, passApp FROM amsadb1.Users WHERE User = "'.$usuario.'"';
  $result =$conexion->prepare($query);
  $result->execute();
  $datosuser=$result->fetch(PDO::FETCH_ASSOC);

  $email=$datosuser['email'];
  $pass =$datosuser['passApp'];
  //$usuario =$datosuser['User'];
  $usuario = ucwords(strtolower($datosuser['User']));


  
 // Intancia de PHPMailer
 $mail = new PHPMailer();
 $mail->isSMTP();
 $mail->SMTPDebug = 0; 
 $mail->Host = 'smtp.gmail.com';
 $mail->Port  = 465; 
 $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS; 
 $mail->SMTPAuth = true;
 $mail->Username = $email;
 $mail->Password = $pass;
 // Quien envía este mensaje
 $mail->setFrom($email, $usuario);
 $correos = "marisol.hernandez@ecomtrading.com";
 $mail->addAddress($correos);
 /* $arraycorreo = explode(",",$mails);
 $tam = sizeof($arraycorreo);
 for($i=0; $i<$tam; $i++){
  if (trim($arraycorreo[$i]) !== $email) { // Evitar añadir el remitente
        $mail->addAddress(trim($arraycorreo[$i]));
    }
 } */

  // Asunto del correo
  $mail->Subject = $subject;
  // Contenido
  $mail->IsHTML(true);
  $mail->CharSet = 'UTF-8';

 

  // Procesar la imagen de liquidación
  $liq_image = './temp/Liq'.$TipoLiq."_".$LiqID.'.png';
  $price_image = './temp/Price'.$TipoLiq."_".$LiqID.'.png';
  if (file_exists($liq_image)) {
    $liq_cid = md5(uniqid(time() . '_liq'));
    $mail->addEmbeddedImage($liq_image, $liq_cid, 'Liquidacion.png');

    $cid = md5(uniqid(time() . '_liq'));
    $mail->addEmbeddedImage($price_image, $cid, 'Price.png');
    
    $bodyy = $body;
    if (!empty($comentarios)) {
      $bodyy .= '<br><br><u>' . $comentarios . '</u>';
    }
    $bodyy .= 
    '<br><br><img src="cid:' . $liq_cid . '" alt="Imagen de Liquidación" style="display: block; margin-left: auto; margin-right: auto; width: 1080px;" /><br>' ;
    if ($TipoLiq == 'Ant') {
      $bodyy .= '<br> Adjunto detalle de precio <u>provisional</u>: <br>';
    } else {
      $bodyy .= '<br> Adjunto detalle de precio: <br>';
    }

    // imagen extra 
    if (isset($cid)) {
      $bodyy .= '<img src="cid:'.$cid .'" alt="Imagen Adjunta" style="width: 980px;" /><br>';
    } 

    $bodyy .= '<br><br>Quedo al pendiente por cualquier duda o comentario.
    <br><br><br>--<br><br>Saludos,<br><br> Ing. ' . $usuario . '<br>
    <span style="color: gray;">Laboratorio de HVI.<br>AMSA Puebla</span>';

  }

  $mail->Body = $bodyy;

  // crear el archivo de excel
  $filename = 'Liq'.$TipoLiq."_".$LiqID.'.xlsx';
  $rem='./temp/'.$filename;
  $mail->addAttachment($rem); 
  $mail->addAttachment($filepath);
  
  // cargar la imagen de la liquidacion 
  /* $filename2 = 'Liq-'.$LiqID.'.png';
  $rem2='./temp/'.$filename2;
  $mail->addAttachment($rem2); 
  $mail->addAttachment($filepath2);
 */
   
  if (isset($_FILES['fileInv']) && $_FILES['fileInv']['error'] == UPLOAD_ERR_OK) {
    $file_tmp_path = $_FILES['fileInv']['tmp_name'];
    $file_name = $_FILES['fileInv']['name'];
    $mail->addAttachment($file_tmp_path, $file_name);
  }

  if (isset($_FILES['fileZip']) && $_FILES['fileZip']['error'] == UPLOAD_ERR_OK) {
    $file_tmp_path = $_FILES['fileZip']['tmp_name'];
    $file_name = $_FILES['fileZip']['name'];
    $mail->addAttachment($file_tmp_path, $file_name);
  }
  
  if ($mail->send()) {
    $Hoy = date('Y-m-d');
    $consulta = "UPDATE amsadb1.Liquidation SET FechaEnv ='$Hoy' WHERE LiqID='$LiqID' and TipoLiq='$TipoLiq'";
    
    $resultado = $conexion->prepare($consulta);
    if ($resultado->execute()) {
      echo json_encode(["exists" => true, "message" => "Correo enviado correctamente"]);
    } else {
      echo json_encode(["exists" => false, "error" => "Error al actualizar la fecha en la base de datos."]);
    }
  } else {
      echo json_encode(["exists" => false, "error" => "Error al enviar el correo: " . $mail->ErrorInfo]);
  }

  //Borrado de archivos de carpeta temp
  function eliminarArchivos($path, $nombreArchivo) {
    $file = $path . '/' . $nombreArchivo;
    if (is_file($file)) {
      unlink($file);
    } 
  }

  $archivos = [
    'Liq' . $TipoLiq . "_" . $LiqID . '.xlsx',
    'Liq' . $TipoLiq . "_" . $LiqID . '.png',
    'Price' . $TipoLiq . "_" . $LiqID . '.png'
  ];

  foreach ($archivos as $archivo) {
    eliminarArchivos('./temp', $archivo);
  }
?>