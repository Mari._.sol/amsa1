<?php

setlocale(LC_TIME, "spanish");

function lowercase($element)
{
    return "'".$element."'";
}

$DO = $_GET['DOCMS'];
$LotsCMS = $_GET['LotsCMS'];


include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

include_once '../fpdf/fpdf.php';

class PDF extends FPDF
{
   //Cabecera de p�gina
   function Header()
   {
    $this->Ln(7);
    $this->SetFont('Arial','B',10);
    $this->Image('../img/logo1.png', 14, 13, 13, 13, 'PNG');
    $this->Cell(0,6, utf8_decode('AGROINDUSTRIAS UNIDAS DE MEXICO S.A. DE C.V.'),0,0,'C');
    $this->Ln(4);
    $this->SetFont('Arial','B',9);
    $this->Cell(0,7,'COTTON DIVISION', 0, 0,'C');
    $this->Ln();
   }

   //Pie de p�gina
   function Footer()
   {
       // Position at 1.5 cm from bottom
       $this->SetY(-15);
       // Arial italic 8
       $this->SetFont('Arial','I',8);
       // Page number
       $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
   }
}


//Si la busqueda es por DO
if ($DO != ""){
    $Lots = array();
    $i = 0;
    $consulta = 'SELECT distinct Bales.Lot, Bales.DO, Gines.GinName, Gines.IDGin
                 FROM Bales, DOrds, Lots
                 JOIN Gines on GinID = Gines.IDGin
                 WHERE DOrds.DOrd = '.$DO.' and DOrds.DOrd = Lots.DOrd and Bales.Lot = Lots.Lot Order by Lot;'; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataDO=$resultado->fetchALL(PDO::FETCH_ASSOC);
    
}else{
   $lotesarr=explode(",", $LotsCMS);
$lotesbuscar=implode("','", $lotesarr);
    $consulta = "SELECT distinct Bales.Lot, Bales.DO, Gines.GinName,  Gines.IDGin
                 FROM Bales
                 JOIN Gines on Bales.GinID = Gines.IDGin 
                 WHERE Bales.Lot in ('".$lotesbuscar."') Order by Lot;";  
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataLotsCMS=$resultado->fetchALL(PDO::FETCH_ASSOC);
        
    //$array = array_map('lowercase', explode(',', $Lots)); 
    /*$array = explode(",",$LotsCMS);*/
}

$pdf = new PDF();

if ($DO == ""){
    foreach ($dataLotsCMS as $dato){
      $pdf->AliasNbPages();
      $pdf->AddPage('Portrait','Letter'); //Landscape
      $pdf->SetTitle("Listado: ".$LotsCMS);
      
      $consulta =  "SELECT Bal, Lot, DO FROM Bales WHERE Lot = '".$dato['Lot']."' Order by Bal;"; 
      $resultado = $conexion->prepare($consulta);
      $resultado->execute();
      $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
      $idgin = $dato['IDGin'];

      $consulta = "SELECT Cert FROM Lots WHERE Lot = '".$dato['Lot']."'"; 
      $resultado = $conexion->prepare($consulta);
      $resultado->execute();        
      $dataDO=$resultado->fetch();
      $Cert = $dataDO['Cert'];


      if($idgin == '99003' || $idgin == '99002' || $idgin == '99001'){
        $gin = "USA";
      }
      else {
        if($idgin == '99999'){
          $gin="";
        }
        else{
          $gin =$dato['GinName'];
        }
      }

      $pdf->SetFont('Arial','B', 10);
      $pdf->Cell(1,5, utf8_decode($dato['Lot']),'TB',0,'L');
      $pdf->Cell(0,5, utf8_decode($gin),'TB',0,'C');
      if ($Cert == 1){
        $pdf->Cell(0,5, 'CERTIFICADAS', 0, 0,'R');
      }      
      //$pdf->Cell(0,4, utf8_decode(' ','TB',0,'R');
      $pdf->Ln();
      $i=0;
      foreach ($data as $row) { //foreach($data as $row){
          $i=$i+1;
          if ($i <= 50){
            $pdf->Ln(4);
            $pdf->SetFont('Arial','', 10);
            $pdf->Cell(26,3,$i.".",0,0,'R');
            $pdf->Cell(26,3,($row['Bal']),0,0,'L');
            $x = 36;
          }else if ($i > 50 && $i <= 100){
            $pdf->Ln(4);
            $pdf->SetXY(73,$x);
            $pdf->SetFont('Arial','', 10);
            $pdf->Cell(26,3,$i.".",0,0,'R');
            $pdf->Cell(26,3,($row['Bal']),0,0,'L');
            $x=$x+4;
            $z = 36;
          }else if ($i > 100){
            $pdf->Ln(4);
            $pdf->SetXY(137,$z);
            $pdf->SetFont('Arial','', 10);
            $pdf->Cell(26,3,$i.".",0,0,'R');
            $pdf->Cell(26,3,($row['Bal']),0,0,'L');
            $z=$z+4;
          }
      }
      
    }
}else{
    foreach ($dataDO as $dato){
      $pdf->AliasNbPages();
      $pdf->AddPage('Portrait','Letter');
      $pdf->SetTitle("Listado: ".$DO);
      
      $consulta = "SELECT Bal, Lot, DO FROM Bales WHERE Lot = '".$dato['Lot']."' Order by Bal;"; 
      $resultado = $conexion->prepare($consulta);
      $resultado->execute();
      $data=$resultado->fetchAll(PDO::FETCH_ASSOC);

      $consulta = "SELECT Cert FROM DOrds WHERE DOrd=".$DO.""; 
      $resultado = $conexion->prepare($consulta);
      $resultado->execute();        
      $dataDO = $resultado->fetch();
      $Cert = $dataDO['Cert'];

      $idgin = $dato['IDGin'];

      if($idgin == '99003' || $idgin == '99002' || $idgin == '99001'){
        $gin = "USA";
      }
      else {
        if($idgin == '99999'){
          $gin="";
        }
        else{
          $gin =$dato['GinName'];
        }
      }
      
      if ($Cert == 1){
        $pdf->SetFont('Arial','B', 10);
        $pdf->Cell(55,5, utf8_decode($dato['Lot']),'TB',0,0);
        $pdf->Cell(55,5, utf8_decode($gin),'TB',0,0);
        $pdf->Cell(0,5, utf8_decode($dato['DO']),'TB',0,0);
        $pdf->Cell(0,5, 'CERTIFICADAS', 0, 0,'R');
        }else{
          $pdf->SetFont('Arial','B', 10);
        $pdf->Cell(65,5, utf8_decode($dato['Lot']),'TB',0,'L');
        $pdf->Cell(65,5, utf8_decode($gin),'TB',0,'C');
        $pdf->Cell(0,5, utf8_decode($dato['DO']),'TB',0,'R');
        }
      $pdf->Ln();
      $i=0;
      foreach ($data as $row) { //foreach($data as $row){
          $i=$i+1;
          if ($i <= 50){
            $pdf->Ln(4);
            $pdf->SetFont('Arial','', 10);
            $pdf->Cell(26,3,$i.".",0,0,'R');
            $pdf->Cell(26,3,($row['Bal']),0,0,'L');
            $x = 36;
          }else if ($i > 50 && $i <= 100){
            $pdf->Ln(4);
            $pdf->SetXY(73,$x);
            $pdf->SetFont('Arial','', 10);
            $pdf->Cell(26,3,$i.".",0,0,'R');
            $pdf->Cell(26,3,($row['Bal']),0,0,'L');
            $x=$x+4;
            $z = 36;
          }else if ($i > 100){
            $pdf->Ln(4);
            $pdf->SetXY(137,$z);
            $pdf->SetFont('Arial','', 10);
            $pdf->Cell(26,3,$i.".",0,0,'R');
            $pdf->Cell(26,3,($row['Bal']),0,0,'L');
            $z=$z+4;
          }
      }
    }
}

$pdf->Output('I', "Listado.pdf"); //"Listado_".$GinName."_".date('d-m-Y').".pdf"

$conexion=null;

?>