<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

require './vendor/phpmailer/phpmailer/src/PHPMailer.php';
require './vendor/phpmailer/phpmailer/src/SMTP.php';

require './vendor/autoload.php';

include_once('correo-model.php');


try {
  $usuario = (!empty($_REQUEST['usuario'])) ? $_REQUEST['usuario'] : '';
  $typeDO = (!empty($_REQUEST['typeDO'])) ? $_REQUEST['typeDO'] : '';
  $numDO = (!empty($_REQUEST['numDO'])) ? $_REQUEST['numDO'] : '';
  $salSucu = (!empty($_REQUEST['salSucu'])) ? $_REQUEST['salSucu'] : '';
  $regOri = (!empty($_REQUEST['regOri'])) ? $_REQUEST['regOri'] : '';
  $arrReg = (!empty($_REQUEST['arrReg'])) ? $_REQUEST['arrReg'] : '';
  $client = (!empty($_REQUEST['client'])) ? $_REQUEST['client'] : '';
  $observ = (!empty($_REQUEST['observ'])) ? $_REQUEST['observ'] : '';
  $contra = (!empty($_REQUEST['contra'])) ? $_REQUEST['contra'] : '';
  $totalQty = (!empty($_REQUEST['totalQty'])) ? $_REQUEST['totalQty'] : '';
 // $esMuestra = (!empty($_REQUEST['esMuestra'])) ? $_REQUEST['esMuestra'] : '';
  $asunto = (!empty($_REQUEST['subject'])) ? $_REQUEST['subject'] : '';
  $tabla = (!empty($_REQUEST['tablahtml'])) ? $_REQUEST['tablahtml'] : '';
  $emailsD = (!empty($_REQUEST['emails'])) ? $_REQUEST['emails'] : '';
 
  //$date_pdf = (!empty($_REQUEST['date_pdf'])) ? $_REQUEST['date_pdf'] : '';
  //$Crss= (!empty($_REQUEST['Crss'])) ? $_REQUEST['Crss'] : '';
  $PurNo= (!empty($_REQUEST['PurNo'])) ? $_REQUEST['PurNo'] : '';
  
  
  
  $fechado = getfechaDO($numDO);
  $date_pdf=  $fechado['Date'];
  $Crss= $fechado['Crss'];


  //Traer email y contraseña de la bd
  $AuthRow = getAuth($usuario);
  $email = $AuthRow['email'];
  $pass = $AuthRow['passApp'];

  $sam= muestrasDO($numDO);
  $samples = $sam['Samp'];
  //Traer el destinatario y los CC de la bd dependiendo del cliente
  $emailDestino= array_unique($emailDestino = explode(",", $emailsD) , SORT_STRING);

  // Intancia de PHPMailer
  $mail                = new PHPMailer();
  // Es necesario para poder usar un servidor SMTP como gmail
  $mail->isSMTP();
  // Si estamos en desarrollo podemos utilizar esta propiedad para ver mensajes de error
  //SMTP::DEBUG_OFF    = off (for production use) 0
  //SMTP::DEBUG_CLIENT = client messages 1 
  //SMTP::DEBUG_SERVER = client and server messages 2
  $mail->SMTPDebug     = 0; //SMTP::DEBUG_SERVER;
  //Set the hostname of the mail server
  $mail->Host          = 'smtp.gmail.com';
  $mail->Port          = 465; // 465 o 587
  // Propiedad para establecer la seguridad de encripción de la comunicación
  $mail->SMTPSecure    = PHPMailer::ENCRYPTION_SMTPS; // tls o ssl para gmail obligado
  // Para activar la autenticación smtp del servidor
  $mail->SMTPAuth      = true;
  //$mail->SMTPAuthTLS      = false;
  // Credenciales de la cuenta
  $mail->Username     = $email;
  $mail->Password     = $pass;
  // Quien envía este mensaje
  $mail->setFrom($email, $usuario);
  // Si queremos una dirección de respuesta
  //$mail->addReplyTo('replyto@panchos.com', 'Pancho Doe');
  // Destinatario
  $mail->addAddress("christian.becerra@ecomtrading.com"); //(acencion.pani@ecomtrading.com); 
  // CC
  $mail->addCC("jvizcaya@ecomtrading.com");
  $mail->addCC("jdelmoral@ecomtrading.com");
  $m = 0;
  while ($m < count($emailDestino)) {
    $mail->addCC($emailDestino[$m]);
    $m++;
  }

 if($samples == 1){
    $mail->addCC("martin.romero@ecomtrading.com");
    $mail->addCC("i.lara@ecomtrading.com");
    
  }
  if($typeDO == "EXP"){
    $mail->addCC("maria.molina@ecomtrading.com");
    //$mail->addCC("ivan.felix@ecomtrading.com");
  }
  // Asunto del correo
  $mail->Subject = $asunto;
  // Contenido
  $mail->IsHTML(true);
  $mail->CharSet = 'UTF-8';
 if($typeDO != "CON"){
  
  $bodyy ='
    <style>
    table, th, td {
      border: 1px solid;
    }    
    </style>
    
    <p>Buen día<br>
    La siguiente DO requiere previa autorización de finanzas para su embarque.</p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Número:</b> ' . $numDO . '<br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Tipo:</b> ' . $typeDO . '<br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Cantidad</b>: ' . $totalQty . ' bc <br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Salida:</b> ' . $salSucu . '<br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Destino: </b> ' . $arrReg . '<br>
     <dd>  <b>Cliente:</b> ' . $client . '</dd>
     <dd>     <b>Contrato:</b> ' . $contra . '</dd>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Planta de Origen:</b> ' . $regOri . '<br>';
     if(!empty($tabla)){
     $bodyy .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Lotes:</b><dd><div>' . $tabla . '</div></dd> ';}
     if(!empty($observ)){
     $bodyy.= ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Comentarios:</b> ' . $observ . '<br><br>';}
   $bodyy .=' 
    Avisar cualquier contratiempo o anomalía.<br>
    Capturar los pesos de salida en CMS<br>
    Saludos.<br>';

    $mail->Body    = $bodyy;

  } else {
    $bodyy= '
    <style>
    table, th, td {
      border: 1px solid;
    }    
    </style>
    
    <p>Buen día<br><br>
    La siguiente DO está autorizada, proceder a su embarque conforme a lo solicitado.</p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Número:</b> ' . $numDO . '<br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Tipo:</b> ' . $typeDO . '<br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Cantidad</b>: ' . $totalQty . ' bc <br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Salida:</b> ' . $salSucu . '<br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Destino: </b> ' . $arrReg . '<br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Planta de Origen:</b> ' . $regOri . '<br>';
     if(!empty($tabla)){
     $bodyy .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Lotes:</b><dd><div>' . $tabla . '</div></dd> ';}
     if(!empty($observ)){
     $bodyy.= ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Comentarios:</b> ' . $observ . '<br><br>';}
   $bodyy .=' 
    Avisar cualquier contratiempo o anomalía.<br>
    Capturar los pesos de salida en CMS<br><br>
    Saludos.<br>';

    $mail->Body    = $bodyy;
  }
  //CREAMOS EL PDF SI LA SALIDA ES DE USA 
  if($salSucu =="USA"){
     
    crearPDFDO($date_pdf,$typeDO,$numDO,$contra,$salSucu,$arrReg,$regOri,$client,$Crss,$PurNo);

    $DOPDF='./temp/'.$numDO.".pdf";
    $mail->addAttachment($DOPDF); 

  

  }


  // Texto alternativo
  //$mail->AltBody = 'No olvides suscribirte a nuestro canal.';
  // Agregar algún adjunto
  //$mail->addAttachment(IMAGES_PATH.'logo.png');.
  // Enviar el correo
  if (!$mail->send()) {
    $ma = "Error";
    throw new Exception($mail->ErrorInfo);
  } else
    $ma = "Message sent successfully";

    

    print json_encode($ma, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);//envio el array final el formato json a AJAX
    if($salSucu =="USA"){
      unlink($DOPDF);
    }

} catch (Exception $e) {
  echo $e->getMessage();
}
