<?php
date_default_timezone_set('America/Mexico_City');
//setlocale(LC_TIME, "");
setlocale(LC_TIME, "spanish");
include_once '../bd/conexion.php';
include_once '../fpdf/fpdf.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

$fromdate = (!empty($_GET['fechainicio'])) ? $_GET['fechainicio'] : '';
$todate = (!empty($_GET['fechafin'])) ? $_GET['fechafin'] : '';
$region = (!empty($_GET['region'])) ? $_GET['region'] : '';
$fileName = "Maniobras-Puebla-".date('Y-m-d').'.pdf';
$pdf = new FPDF();


$fecha=strftime("%d-%B-%G", strtotime($fromdate));

$clave=660;

$autorizo='Christian Uriel Becerra Allende';


$pdf->AddPage('L', 'letter');
$pdf->SetTitle($fileName);
$pdf->SetFont('Arial','B',8);
$pdf->Image('../img/logo1.png', 15, 10, 13, 13, 'PNG');
$pdf->Cell(0,10, utf8_decode('AGROINDUSTRIAS UNIDAS DE MEXICO S.A. DE C.V.'),0,0,'C');
$pdf->Ln(4);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(0,10,'COTTON DIVISION', 0, 0,'C');
$pdf->Ln(4);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(0,10,'CONTROL DE MANIOBRAS', 0, 0,'C');
$pdf->Ln(4);
$pdf->Cell(0,10,'FECHA: '. $fecha, 0, 0,'R');
$pdf->Ln(12);
$pdf->SetFont('Arial','B',7);
$pdf->SetFillColor(216, 213, 213 );
//TABLA DE ENTRADAS A PUEBLA

$query1="SELECT Truks.TrkID,Truks.DO,Transports.BnName,Truks.WBill,Lots.Lot,Lots.Qty,Gines.GinName,Region.RegNam,Truks.Status,
        TRUNCATE((Truks.InWgh/Truks.CrgQty * Lots.Qty),2) as PesoEntrega,TRUNCATE((Truks.OutWgh/Truks.CrgQty * Lots.Qty),2) as PesoSalida,
        Truks.InDat AS Fecha,Truks.TipoManArr as TipoMan,Truks.RespManArr as RespMan,Truks.NotaSalArr as NotaSal

        FROM amsadb1.Truks
        LEFT JOIN amsadb1.DOrds 
        ON Truks.DO = DOrds.DOrd
        LEFT JOIN amsadb1.Clients 
        ON DOrds.InPlc = Clients.CliID
        LEFT JOIN amsadb1.Transports
        ON Truks.TNam = Transports.TptID 

        left JOIN amsadb1.Lots 
        ON Lots.TrkID = Truks.TrkID

        LEFT JOIN amsadb1.Gines 
        ON Lots.GinID = Gines.IDGin

        LEFT JOIN amsadb1.Region 
        ON Lots.Reg = Region.IDReg

        where DOrds.InReg= '$clave'  and (Truks.InDat between '$fromdate' and '$todate' ) AND Truks.Status ='Received'
        order By Truks.TrkId Desc;"
;

$resultado1 = $conexion->prepare($query1);
$resultado1->execute();

$entradas=$resultado1->fetchAll();
$tam1 = count($entradas);


if($tam1 >0 ){
    $pdf->Cell(256, 4, 'ENTRADAS', 1, '', 'C');
    $pdf->Ln(4);
  
    $pdf->SetFont('Arial','B',6);
    $pdf->Cell(5, 4, 'No.', 1, '', 'C','True');
    //$pdf->Cell(15, 4, 'FECHA', 1, '', 'C','True');
    $pdf->Cell(18, 4, 'O/E', 1, '', 'C','True');
    $pdf->Cell(15, 4, 'LOTE', 1, '', 'C','True');
    $pdf->Cell(15, 4, 'PACAS', 1, '', 'C','True');
    $pdf->Cell(25, 4, 'REGION', 1, '', 'C','True');
    $pdf->Cell(25, 4, 'DESCARGO', 1, '', 'C','True');
    $pdf->Cell(20, 4, 'NOTA', 1, '', 'C','True');
    $pdf->Cell(20, 4, 'PESO LLEGADA', 1, '', 'C','True');
    $pdf->Cell(78, 4, 'LINEA TRASPORTISTA', 1, '', 'C','True');
    $pdf->Cell(15, 4, 'PORTE', 1, '', 'C','True');
    $pdf->Cell(20, 4, 'MANIOBRA', 1, '', 'C','True');
  

    //consultar las entradas a puebla
    $contador=1;
    $sumapacas=0;
    for($i=0; $i<$tam1 ; $i++){
        $pdf->Ln(4);
        
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(5, 4, $contador, 1, '', 'C','');
      //  $pdf->Cell(15, 4, $entradas[$i]['Fecha'], 1, '', 'C','');
        $pdf->Cell(18, 4, $entradas[$i]['DO'], 1, '', 'C','');
        $pdf->Cell(15, 4, $entradas[$i]['Lot'], 1, '', 'C','');
        $pdf->Cell(15, 4, $entradas[$i]['Qty'], 1, '', 'C','');
        $pdf->Cell(25, 4, $entradas[$i]['RegNam'], 1, '', 'C','');
        $pdf->Cell(25, 4, $entradas[$i]['RespMan'], 1, '', 'C','');
        $pdf->Cell(20, 4, $entradas[$i]['NotaSal'], 1, '', 'C','');
        $pdf->Cell(20, 4, $entradas[$i]['PesoEntrega'], 1, '', 'C','');
        $pdf->SetFont('Arial','',6);
        $pdf->Cell(78, 4, $entradas[$i]['BnName'], 1, '', 'C','');
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(15, 4, $entradas[$i]['WBill'], 1, '', 'C','');
        $pdf->Cell(20, 4, $entradas[$i]['TipoMan'], 1, '', 'C','');
        $sumapacas=$entradas[$i]['Qty']+$sumapacas;
        $contador++;
    }
    $pdf->Ln(4);
    $pdf->SetFont('Arial','B',6.5);
    $pdf->Cell(23, 4, '', '', '', 'C','');
    $pdf->Cell(15, 4, 'TOTAL', 1, '', 'C','True');
    $pdf->Cell(15, 4, $sumapacas, 1, '', 'C','');
}

//TABLA DE SALIDAS

$query2="SELECT Truks.TrkID,Truks.DO,Transports.BnName,Truks.WBill,Lots.Lot,Lots.Qty,Gines.GinName,Region.RegNam,Truks.Status,
        TRUNCATE((Truks.InWgh/Truks.CrgQty * Lots.Qty),2) as PesoEntrega,TRUNCATE((Truks.OutWgh/Truks.CrgQty * Lots.Qty),2) as PesoSalida,Clients.Cli as Cliente,Truks.OutDat as Fecha,
        IF(DOrds.InReg = 99001,(select Clients.Cli where DOrds.InPlc = Clients.CliID),(select R1.RegNam from Region as R1  where DOrds.InReg = R1.IDReg)) as Lugar,
        Truks.TipoManDep as TipoMan,Truks.RespManDep as RespMan,Truks.NotaSalDep as NotaSal

   
        FROM amsadb1.Truks
        LEFT JOIN amsadb1.DOrds 
        ON Truks.DO = DOrds.DOrd
        LEFT JOIN amsadb1.Clients 
        ON DOrds.InPlc = Clients.CliID
        LEFT JOIN amsadb1.Transports
        ON Truks.TNam = Transports.TptID 

        left JOIN amsadb1.Lots 
        ON Lots.TrkID = Truks.TrkID

        LEFT JOIN amsadb1.Gines 
        ON Lots.GinID = Gines.IDGin

        LEFT JOIN amsadb1.Region 
        ON Lots.Reg = Region.IDReg

        where  DOrds.OutPlc= '$clave'  and (Truks.OutDat between '$fromdate' and '$todate') AND Truks.Status !='Cancelled'
        order By Truks.TrkId Desc;"
;


$resultado2 = $conexion->prepare($query2);
$resultado2->execute();
$salidas=$resultado2->fetchAll();
$tam2 = count($salidas);


if($tam2 > 0){

    $pdf->Ln(10);
    $pdf->SetFont('Arial','B',7);
    $pdf->Cell(256,4,'SALIDAS', 1, '','C','');
    $pdf->Ln(4);
    $pdf->SetFont('Arial','B',6);
    $pdf->Cell(5, 4, 'No.', 1, '', 'C','True');
    $pdf->Cell(32, 4, 'DESTINO', 1, '', 'C','True');
    $pdf->Cell(15, 4, 'O/E', 1, '', 'C','True');
    $pdf->Cell(13, 4, 'LOTE', 1, '', 'C','True');
    $pdf->Cell(10, 4, 'PACAS', 1, '', 'C','True');
    $pdf->Cell(23, 4, 'REGION', 1, '', 'C','True');
    $pdf->Cell(15, 4, 'DESCARGO', 1, '', 'C','True');
    $pdf->Cell(18, 4, 'NOTA', 1, '', 'C','True');
    $pdf->Cell(18, 4, 'PESO LLEGADA', 1, '', 'C','True');
  //  $pdf->Cell(15, 4, 'FECHA', 1, '', 'C','True');
    $pdf->Cell(74, 4, 'LINEA TRANSPORTISTA', 1, '', 'C','True');
    $pdf->Cell(20, 4, 'PORTE', 1, '', 'C','True');
    $pdf->Cell(13, 4, 'MANIOBRA', 1, '', 'C','True');

    $contador=1;
    $sumapacas=0;
    $pdf->SetFont('Arial','',6.5);
    for($i=0; $i<$tam2 ; $i++){
        $pdf->Ln(4);
        $pdf->Cell(5, 4, $contador, 1, '', 'C');
        $pdf->Cell(32, 4, $salidas[$i]['Lugar'], 1, '', 'C');
        $pdf->Cell(15, 4, $salidas[$i]['DO'], 1, '', 'C');
        $pdf->Cell(13, 4, $salidas[$i]['Lot'], 1, '', 'C');
        $pdf->Cell(10, 4, $salidas[$i]['Qty'], 1, '', 'C');
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(23, 4, $salidas[$i]['RegNam'], 1, '', 'C');
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(15, 4, $salidas[$i]['RespMan'], 1, '', 'C');
        $pdf->Cell(18, 4, $salidas[$i]['NotaSal'], 1, '', 'C');
        $pdf->Cell(18, 4, $salidas[$i]['PesoEntrega'], 1, '', 'C');
      //  $pdf->Cell(15, 4, $salidas[$i]['Fecha'], 1, '', 'C');
        $pdf->SetFont('Arial','',6);
        $pdf->Cell(74, 4, $salidas[$i]['BnName'], 1, '', 'C');
        $pdf->SetFont('Arial','',6.5);
        $pdf->Cell(20, 4, $salidas[$i]['WBill'], 1, '', 'C');
        $pdf->Cell(13, 4,  $salidas[$i]['TipoMan'], 1, '', 'C');
        $sumapacas=$sumapacas+ $salidas[$i]['Qty'];
        $contador++;
    }
    $pdf->Ln(4);
    $pdf->Cell(52, 4, '', '', '', 'C','');
    $pdf->SetFont('Arial','B',6.5);
    $pdf->Cell(13, 4, 'TOTAL', 1, '', 'C','True');
    $pdf->Cell(10, 4, $sumapacas, 1, '', 'C','');

}
//TABLA DE DIRECTOS

$query3="SELECT Truks.TrkID,Truks.DO,Transports.BnName,Truks.WBill,Lots.Lot,Lots.Qty,Gines.GinName,Region.RegNam,Truks.Status,
        TRUNCATE((Truks.InWgh/Truks.CrgQty * Lots.Qty),2) as PesoEntrega,TRUNCATE((Truks.OutWgh/Truks.CrgQty * Lots.Qty),2) as PesoSalida,Clients.Cli as Cliente,Truks.InDat as Fecha,
        Truks.TipoManArr as TipoMan,Truks.RespManArr as RespMan,Truks.NotaSalArr as NotaSal


        FROM amsadb1.Truks
        LEFT JOIN amsadb1.DOrds 
        ON Truks.DO = DOrds.DOrd
        LEFT JOIN amsadb1.Clients 
        ON DOrds.InPlc = Clients.CliID
        LEFT JOIN amsadb1.Transports
        ON Truks.TNam = Transports.TptID 

        left JOIN amsadb1.Lots 
        ON Lots.TrkID = Truks.TrkID

        LEFT JOIN amsadb1.Gines 
        ON Lots.GinID = Gines.IDGin

        LEFT JOIN amsadb1.Region 
        ON Lots.Reg = Region.IDReg

        where ( DOrds.InReg = 99001 AND Clients.State ='$region' AND Clients.Directo = 1 ) AND (Truks.InDat between '$fromdate' and '$todate') AND Truks.Status ='Received' and DOrds.OutPlc != 660
        order By Truks.TrkId Desc;"
;


$resultado3 = $conexion->prepare($query3);
$resultado3->execute(); 
$directos=$resultado3->fetchAll();
$tam3 = count($directos);

if($tam3 > 0){

    $pdf->Ln(10);
    $pdf->SetFont('Arial','B',7);
    $pdf->Cell(256,4,'DIRECTOS', 1, '','C','');
    $pdf->Ln(4);



    $pdf->SetFont('Arial','B',6);
    $pdf->Cell(5, 4, 'No.', 1, '', 'C','True');
    $pdf->Cell(32, 4, 'DESTINO', 1, '', 'C','True');
    $pdf->Cell(13, 4, 'O/E', 1, '', 'C','True');
    $pdf->Cell(14, 4, 'LOTE', 1, '', 'C','True');
    $pdf->Cell(11, 4, 'PACAS', 1, '', 'C','True');
    $pdf->Cell(23, 4, 'REGION', 1, '', 'C','True');
    $pdf->Cell(18, 4, 'ENTREGO', 1, '', 'C','True');
    //$pdf->Cell(15, 4, 'FECHA', 1, '', 'C','True');
    $pdf->Cell(73, 4, 'LINEA TRANSPORTISTA', 1, '', 'C','True');
    $pdf->Cell(18, 4, 'PESO SALIDA', 1, '', 'C','True');
    $pdf->Cell(18, 4, 'PESO LLEGADA', 1, '', 'C','True');
    $pdf->Cell(18, 4, 'PORTE', 1, '', 'C','True');
    $pdf->Cell(13, 4, 'MANIOBRA', 1, '', 'C','True');
    
    $contador=1;
    $sumapacas=0;
    $pdf->SetFont('Arial','',6.5);
    for($i=0; $i<$tam3 ; $i++){

        $pdf->Ln(4);
        $pdf->Cell(5, 4, $contador, 1, '', 'C','');
        $pdf->Cell(32, 4, $directos[$i]['Cliente'], 1, '', 'C','');
        $pdf->Cell(13, 4, $directos[$i]['DO'] , 1, '', 'C','');
        $pdf->Cell(14, 4, $directos[$i]['Lot'], 1, '', 'C','');
        $pdf->Cell(11, 4, $directos[$i]['Qty'], 1, '', 'C','');
        $pdf->Cell(23, 4, $directos[$i]['RegNam'], 1, '', 'C','');
        $pdf->Cell(18, 4, $directos[$i]['RespMan'], 1, '', 'C','');
       // $pdf->Cell(15, 4, $directos[$i]['Fecha'], 1, '', 'C','');
        $pdf->Cell(73, 4, $directos[$i]['BnName'], 1, '', 'C','');
        $pdf->Cell(18, 4,  $directos[$i]['PesoSalida'], 1, '', 'C','');
        $pdf->Cell(18, 4, $directos[$i]['PesoEntrega'], 1, '', 'C','');
        $pdf->Cell(18, 4, $directos[$i]['WBill'], 1, '', 'C','');
        $pdf->Cell(13, 4, $directos[$i]['TipoMan'], 1, '', 'C','');
        $sumapacas=$sumapacas+$directos[$i]['Qty'];
    
        
        $contador++;
    }

    $pdf->Ln(4);
    $pdf->SetFont('Arial','B',6.5);
    $pdf->Cell(50, 4, '', '', '', 'C','');
    $pdf->Cell(14, 4, 'TOTAL', 1, '', 'C','True');
    $pdf->Cell(11, 4, $sumapacas, 1, '', 'C','');

}

$pdf->SetY(-36); // Posición a 2.5 cm desde el final de la página
$pdf->SetFont('Arial','B',8);
$pdf->Cell(256, 4, 'AUTORIZO', '', '', 'C','');
$pdf->Ln(6);
$pdf->Cell(256, 4, '________________________________', '', '', 'C','');
$pdf->Ln(5);
$pdf->Cell(256, 4,$autorizo, '', '', 'C','');
$pdf->Output('I', $fileName);


?>