<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

$ReqID = (isset($_POST['ReqID'])) ? $_POST['ReqID'] : '';
$Serv = (isset($_POST['Serv'])) ? $_POST['Serv'] : '';
$Typ = (isset($_POST['Typ'])) ? $_POST['Typ'] : '';
$OutReg = (isset($_POST['OutReg'])) ? $_POST['OutReg'] : '';
$InReg = (isset($_POST['InReg'])) ? $_POST['InReg'] : '';

$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';


switch($opcion){
    case 1:
        $consulta = "SELECT IDReg FROM Region WHERE RegNam='$OutReg'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataOutReg = $resultado->fetch();
        $OutReg = $dataOutReg['IDReg'];
        
        $consulta = "SELECT IDReg FROM Region WHERE RegNam='$InReg'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataInReg = $resultado->fetch();
        $InReg = $dataInReg['IDReg'];
        
        $consulta = "INSERT INTO Requisition (ReqID, Services, Typ, OutReg, InReg) VALUES('$ReqID', '$Serv', '$Typ', '$OutReg', '$InReg')";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        
        $consulta = "SELECT * FROM Requisition ORDER BY ReqID DESC LIMIT 1";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);       
        break;    
    case 2:
        $consulta = "SELECT IDReg FROM Region WHERE RegNam='$OutReg'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataOutReg = $resultado->fetch();
        $OutReg = $dataOutReg['IDReg'];
        
        $consulta = "SELECT IDReg FROM Region WHERE RegNam='$InReg'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataInReg = $resultado->fetch();
        $InReg = $dataInReg['IDReg'];
        
        $consulta = "UPDATE Requisition SET ReqID='$ReqID', Services='$Serv', Typ='$Typ', OutReg='$OutReg', InReg='$InReg' WHERE ReqID='$ReqID'";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        
        $consulta = "SELECT * FROM Requisition WHERE ReqID='$ReqID' ";       
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 3:        
        $consulta = "SELECT * FROM Transports Order By TptCo"; //"DELETE FROM Lots WHERE LotID='$LotID' ";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 4:    
        $consulta = "SELECT ReqID, Services, Typ, (Services - Cont) as Total, Cont,
        (SELECT RegNam FROM Region WHERE Region.IDReg = Requisition.OutReg) as OutReg,
        (SELECT RegNam FROM Region WHERE Region.IDReg = Requisition.InReg) as InReg
        FROM Requisition;";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 5:    
        $consulta = "SELECT RteID, OutReg, RegNam FROM Routes, Region WHERE TptID='$TptCo' and IDReg=OutReg Order by RegNam;";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 6:    
        $consulta = "SELECT IDReg, RegNam FROM Region WHERE IsOrigin='1' Order By RegNam;";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 7:    
        $consulta = "INSERT INTO Routes (OutReg, InReg, TptID, Dtime) VALUES('$OutReg', '0', '$TptCo', '0');";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 8:        
        $consulta = "DELETE FROM Requisition WHERE ReqID='$ReqID' ";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 9:        
        $consulta = "UPDATE Requisition Set Cont = '$ReqAll' Where ReqID = '$ReqID'";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
}

print json_encode($data, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX
$conexion=null;