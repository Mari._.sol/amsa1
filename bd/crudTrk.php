<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
//archivo de texto temporal
$filetext =fopen("registros.txt", "a");
function truncar($numero, $digitos)
{
    $truncar = 10**$digitos;
    return intval($numero * $truncar) / $truncar;
}

$ban = (isset($_POST['ban'])) ? $_POST['ban'] : '';
$TrkLPlt = (isset($_POST['TrkLPlt'])) ? $_POST['TrkLPlt'] : '';
$TraLPlt = (isset($_POST['TraLPlt'])) ? $_POST['TraLPlt'] : '';
$TNam = (isset($_POST['TNam'])) ? $_POST['TNam'] : '';
$WBill = (isset($_POST['WBill'])) ? $_POST['WBill'] : '';
$DrvLcs = (isset($_POST['DrvLcs'])) ? $_POST['DrvLcs'] : '';
$DrvNam = (isset($_POST['DrvNam'])) ? $_POST['DrvNam'] : '';
$DrvTel = (isset($_POST['DrvTel'])) ? $_POST['DrvTel'] : '';
$CrgQty = (isset($_POST['CrgQty'])) ? $_POST['CrgQty'] : '';
$SchOutDat = (isset($_POST['SchOutDat'])) ? $_POST['SchOutDat'] : '';
$OutDat = (isset($_POST['OutDat'])) ? $_POST['OutDat'] : '';
$SchOutTime = (isset($_POST['SchOutTime'])) ? $_POST['SchOutTime'] : '';
$OutTime = (isset($_POST['OutTime'])) ? $_POST['OutTime'] : '';
$OutWgh = (isset($_POST['OutWgh'])) ? $_POST['OutWgh'] : '';
$SchInDat = (isset($_POST['SchInDat'])) ? $_POST['SchInDat'] : '';
$InDat = (isset($_POST['InDat'])) ? $_POST['InDat'] : '';
$SchInTime = (isset($_POST['SchInTime'])) ? $_POST['SchInTime'] : '';
$InTime = (isset($_POST['InTime'])) ? $_POST['InTime'] : '';
$InWgh = (isset($_POST['InWgh'])) ? $_POST['InWgh'] : '';
$RqstID = (isset($_POST['RqstID'])) ? $_POST['RqstID'] : '';
$RO = (isset($_POST['RO'])) ? $_POST['RO'] : '';
$FreightC = (isset($_POST['FreightC'])) ? $_POST['FreightC'] : '';
$LotsAssc = (isset($_POST['LotsAssc'])) ? $_POST['LotsAssc'] : '';
$AsscLots = (isset($_POST['AsscLots'])) ? $_POST['AsscLots'] : '';
$PWgh = (isset($_POST['PWgh'])) ? $_POST['PWgh'] : '';
$TrkCmt = (isset($_POST['TrkCmt'])) ? $_POST['TrkCmt'] : '';
$Status = (isset($_POST['Status'])) ? $_POST['Status'] : '';
$IrrDat = (isset($_POST['IrrDat'])) ? $_POST['IrrDat'] : '';
$DO = (isset($_POST['DO'])) ? $_POST['DO'] : '';
$DOrd = (isset($_POST['DOrd'])) ? $_POST['DOrd'] : '';
$id = (isset($_POST['id'])) ? $_POST['id'] : '';

$OutReg = (isset($_POST['OutReg'])) ? $_POST['OutReg'] : '';

$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$TrkID = (isset($_POST['TrkID'])) ? $_POST['TrkID'] : '';
$LotID = (isset($_POST['LotID'])) ? $_POST['LotID'] : '';

$LotIDSplit = (isset($_POST['LotIDSplit'])) ? $_POST['LotIDSplit'] : '';
$LocSplit = (isset($_POST['LocSplit'])) ? $_POST['LocSplit'] : '';
$DOSplit = (isset($_POST['DOSplit'])) ? $_POST['DOSplit'] : '';
$Split = (isset($_POST['Split'])) ? $_POST['Split'] : '';

$samples=(isset($_POST['samples'])) ? $_POST['samples'] : '';

//Variables para incrementar/decrementar cntador de requisicion
$RqstID1 = (isset($_POST['RqstID1'])) ? $_POST['RqstID1'] : '';
$RqstID2 = (isset($_POST['RqstID2'])) ? $_POST['RqstID2'] : '';
$totalcost =(isset($_POST['costot'])) ? $_POST['costot'] : '';
$AverageCost =(isset($_POST['costoprom'])) ? $_POST['costoprom'] : '';
$tarifnota = (isset($_POST['notas'])) ? $_POST['notas'] : ''; 
$cantidadlotes = (isset($_POST['lot_intrk'])) ? $_POST['lot_intrk'] : ''; 
$comentario = (isset($_POST['comentario'])) ? $_POST['comentario'] : ''; 
$regionname = (isset($_POST['regionname'])) ? $_POST['regionname'] : ''; 
//usuario que hace la modificacion
$usermod = $regionname = (isset($_POST['usermod'])) ? $_POST['usermod'] : ''; 
$NumEcon = (isset($_POST['NumEcon'])) ? $_POST['NumEcon'] : ''; 
$TipoManiobra = (isset($_POST['TipoManiobra'])) ? $_POST['TipoManiobra'] : ''; 
$Cliente = (isset($_POST['cliente'])) ? $_POST['cliente'] : ''; 
$regman= (isset($_POST['regman'])) ? $_POST['regman'] : '';

//variables de maniobras
$ManiobraLlegada= (isset($_POST['ManiobraLlegada'])) ? $_POST['ManiobraLlegada'] : '';
$RespManLleg= (isset($_POST['RespManLleg'])) ? $_POST['RespManLleg'] : '';
$ManiobraSalida= (isset($_POST['ManiobraSalida'])) ? $_POST['ManiobraSalida'] : '';
$RespManSalida= (isset($_POST['RespManSalida'])) ? $_POST['RespManSalida'] : '';
$NotaSalArr = (isset($_POST['NotaSalArr'])) ? $_POST['NotaSalArr'] : '';
$NotaSalDep = (isset($_POST['NotaSalDep'])) ? $_POST['NotaSalDep'] : '';

$DateCreated=date('Y-m-d'); 

//// Variables para filtros
session_start();
$filterSes = $_SESSION['Filters'];

$muestras = $filterSes['muestras'];
if($muestras == ""){
    $muestras = 200;
}
$regionfil = $filterSes['regionfil'];
$asLots = $filterSes['asLots'];
$DepRegfil = strtoupper($filterSes['DepRegFil']);
$ArrRegfil = strtoupper($filterSes['ArrRegFil']);
$idGlobal = $filterSes['idGlobal'];
$DepArrDate = $filterSes['DepArrDate'];
$fromDate = $filterSes['fromdate'];
$toDate = $filterSes['todate'];
$idTrk = $filterSes['idTrk'];
$data = array();
$consulta = "";
$regions =0;
$numMues =0; 

switch($opcion){
    case 1:

       // $consulta = "INSERT INTO Truks (`TrkLPlt`, `TraLPlt`, `TNam`, `WBill`, `DrvLcs`, `DO`, `DrvNam`, `DrvTel`, `CrgQty`, `CrgUnt`, `PWgh`, `OutDat`, `OutTime`, `OutWgh`, `InDat`, `InTime`, `InWgh`, `LotsAssc`, `Comments`, `RqstID`, `RO`, `SchOutDate`, `SchOutTime`, `SchInDate`, `SchInTime`, `Status`, `IrrDat`,`DateCreated`,`Samples`,Cost) VALUES ('$TrkLPlt', '$TraLPlt', '$TNam', '$WBill', '$DrvLcs', '$DOrd', '$DrvNam', '$DrvTel', '$CrgQty', '$CrgQty', '$PWgh', '$OutDat', '$OutTime', '$OutWgh', '$InDat', '$InTime', '$InWgh', '$AsscLots', '$TrkCmt', '$RqstID', '$RO', '$SchOutDat', '$SchOutTime', '$SchInDat', '$SchInTime', '$Status', '$IrrDat','$DateCreated','$samples','0')"; 
       
       //NO SE GUARDA EL VALOR DE PR CUANDO SE CREA EL TRUCK
        $consulta = "INSERT INTO Truks (`TrkLPlt`, `TraLPlt`, `TNam`, `WBill`, `DrvLcs`, `DO`, `DrvNam`, `DrvTel`, `CrgQty`, `CrgUnt`, `PWgh`, `OutDat`, `OutTime`, `OutWgh`, `InDat`, `InTime`, `InWgh`, `LotsAssc`, `Comments`,`RO`,`Status`, `IrrDat`,`DateCreated`,`Samples`,Cost,NumEcon,TipoManArr,RespManArr,NotaSalArr,TipoManDep,RespManDep,NotaSalDep) VALUES ('$TrkLPlt', '$TraLPlt', '$TNam', '$WBill', '$DrvLcs', '$DOrd', '$DrvNam', '$DrvTel', '$CrgQty', '$CrgQty', '$PWgh', '$OutDat', '$OutTime', '$OutWgh', '$InDat', '$InTime', '$InWgh', '$AsscLots', '$TrkCmt','$RO','$Status', '$IrrDat','$DateCreated','$samples','0','$NumEcon','$ManiobraLlegada','$RespManLleg','$NotaSalArr','$ManiobraSalida','$RespManSalida','$NotaSalDep')"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        
        $consulta = "Analyze table Truks";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        
        $consulta = "SELECT AUTO_INCREMENT as TrkID
                    FROM information_schema.TABLES
                    WHERE TABLE_SCHEMA = 'amsadb1'
                    AND TABLE_NAME = 'Truks';";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetch();
       // print_r("TRKID SELECCIONADO");
        //print_r($data);
        $TrkID = $data['TrkID']-1;
        
        
        if ($Status == "Received"){
            $consulta = "SELECT Typ, InReg FROM Lots, DOrds  WHERE Lots.DOrd=DOrds.DOrd and TrkID = '$TrkID';";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $dataDO=$resultado->fetch();
            if($dataDO['Typ'] == "CON"){
                $consulta = "SELECT * FROM Lots WHERE TrkID = '$TrkID'";
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
                $dataLot=$resultado->fetchAll(PDO::FETCH_ASSOC);
                    foreach($dataLot as $row){
                        $Crop = $row['Crop'];
                        $Lot = $row['Lot'];
                        $Reg = $row['Reg'];
                        $GinID = $row['GinID'];
                        $Loc = $dataDO['InReg'];
                        $Qty = $row['Qty'];
                        $LiqWgh = $row['LiqWgh'];
                        $Status = $row['Status'];
                        $Cert = $row['Cert'];
                        $TotalQty = $row['TotalQty'];
                        
                        //Consulta si el lote existe
                        $consulta = "SELECT * FROM Lots WHERE Lot = '$Lot' and Location = '$Loc' and DOrd = 0;";
                        $resultado = $conexion->prepare($consulta);
                        $resultado->execute();
                        $dataLote=$resultado->fetch();
                        if(!empty($dataLote)){
                            $QtyL = $dataLote['Qty'];
                            $WghL = $dataLote['LiqWgh'];
                            $Sum = $Qty + $QtyL;
                            $SumWgh = $LiqWgh + $WghL;
                            $consulta = "UPDATE Lots SET Qty='$Sum', Unt='$Sum', LiqWgh='$SumWgh' WHERE Lot='$Lot' and Location = '$Loc'";
                            $resultado = $conexion->prepare($consulta);
                            $resultado->execute();    
                             //lo que se va a guardar en el archivo de texto
                            $renglon .= "TRUCKID: ".$TrkID." "."Lote: ".$Lot."  "."cantidad en bodega: ".$QtyL."    "."cantidad de llegada: ". $Qty."   "."Query:   ".$consulta. PHP_EOL;;
                         
                        }else{
                            $consulta = "INSERT INTO Lots (Crop, Lot, Qty, Reg, GinID, Location, Unt, LiqWgh, DOrd, TrkID, InvID, Status, Cert, TotalQty) VALUES('$Crop', '$Lot', '$Qty', '$Reg', '$GinID', '$Loc',  '$Qty', '$LiqWgh', '', '', '', '$Status','$Cert', '$TotalQty') ";			
                            $resultado = $conexion->prepare($consulta);
                            $resultado->execute();
                            $renglon .= "TRUCKID: ".$TrkID." "."Lote: ".$Lot."  "."cantidad de llegada: ". $Qty."   "."Query:   ".$consulta. PHP_EOL;;
                        }
                    }
                      //lo que se va a guardar en el archivo de texto
                       fwrite($filetext, $renglon);
                       fclose($filetext);
              }
        }
        
        if ($Status != "Cancelled" and $RqstID !=0){
            $consulta = "UPDATE Requisition SET Cont=Cont + $cantidadlotes WHERE ReqID='$RqstID';";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
        }
        
        //Actualiza contadores en DO
        $consulta = "UPDATE DOrds SET Rcv = (SELECT SUM(CrgQty) FROM Truks WHERE DO = '$DOrd' and Status = 'Received'), Tran = (SELECT SUM(CrgQty) FROM Truks WHERE DO = '$DOrd' and Status = 'Transit') 
                     WHERE DOrd = '$DOrd'";       
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        //-------------------------
        
        $consulta = "SELECT TrkID, TrkLPlt, TraLPlt, TNam, WBill, DO, DrvNam, DrvTel, CrgQty, OutDat, OutTime OutWgh, InDat, InWgh, RqstID, PO, RO, LotsAssc, Comments,Samples Status FROM Truks ORDER BY TrkID DESC LIMIT 1";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);       
        break;    
    case 2:

        //consultar el estatus actual del truck
        $consulta_status = "SELECT Status from Truks WHERE TrkID='$TrkID'";
        $consulta_status = $conexion->prepare($consulta_status);
        $consulta_status->execute();
        $consulta_status= $consulta_status->fetch(); 
        $status_actual = $consulta_status['Status'];


        if (is_numeric($TNam)){
           
                $consulta = "UPDATE Truks SET TrkLPlt='$TrkLPlt', TraLPlt='$TraLPlt', TNam='$TNam', WBill='$WBill', DrvLcs='$DrvLcs', DO='$DOrd', DrvNam='$DrvNam', DrvTel='$DrvTel', CrgQty='$CrgQty', CrgUnt='$CrgQty', PWgh='$PWgh',OutDat='$OutDat',OutTime='$OutTime', OutWgh='$OutWgh',InDat='$InDat',InTime='$InTime', InWgh='$InWgh', LotsAssc='$AsscLots', Comments='$TrkCmt', RO='$RO',Status='$Status', IrrDat='$IrrDat', Samples='$samples',NumEcon='$NumEcon',TipoManArr = '$ManiobraLlegada',RespManArr = '$RespManLleg',TipoManDep = '$ManiobraSalida',RespManDep = '$RespManSalida',NotaSalArr = '$NotaSalArr',NotaSalDep='$NotaSalDep' WHERE TrkID='$TrkID'";		
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
   
        }else{
            if ($TNam!="" || $TNam!= 0  ){
            $consulta = "SELECT TptID FROM Transports WHERE BnName='$TNam'";		
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetch();
            $TNam = $data['TptID'];
            }

            else{
                $TNam="";
            }
            
            if ($FreightC!=""){
                $consulta = "UPDATE Truks SET TrkLPlt='$TrkLPlt', TraLPlt='$TraLPlt', TNam='$TNam', WBill='$WBill', DrvLcs='$DrvLcs', DO='$DOrd', DrvNam='$DrvNam', DrvTel='$DrvTel', CrgQty='$CrgQty', CrgUnt='$CrgQty', PWgh='$PWgh',OutDat='$OutDat', OutTime='$OutTime', OutWgh='$OutWgh', SchInDate='$SchInDat', SchInTime='$SchInTime', InDat='$InDat', InTime='$InTime', InWgh='$InWgh', LotsAssc='$AsscLots', FreightCost='$FreightC', Cost='1', Comments='$TrkCmt', RO='$RO', Status='$Status', IrrDat='$IrrDat',Samples= '$samples',NumEcon='$NumEcon',TipoManArr = '$ManiobraLlegada',RespManArr = '$RespManLleg',TipoManDep = '$ManiobraSalida',RespManDep = '$RespManSalida',NotaSalArr = '$NotaSalArr',NotaSalDep='$NotaSalDep' WHERE TrkID='$TrkID'";		
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
            }else{
                $consulta = "UPDATE Truks SET TrkLPlt='$TrkLPlt', TraLPlt='$TraLPlt', TNam='$TNam', WBill='$WBill', DrvLcs='$DrvLcs', DO='$DOrd', DrvNam='$DrvNam', DrvTel='$DrvTel', CrgQty='$CrgQty', CrgUnt='$CrgQty', PWgh='$PWgh', OutDat='$OutDat', OutTime='$OutTime', OutWgh='$OutWgh',InDat='$InDat', InTime='$InTime', InWgh='$InWgh', LotsAssc='$AsscLots', Comments='$TrkCmt',RO='$RO', Status='$Status', IrrDat='$IrrDat',Samples= '$samples',NumEcon='$NumEcon',TipoManArr = '$ManiobraLlegada',RespManArr = '$RespManLleg',TipoManDep = '$ManiobraSalida',RespManDep = '$RespManSalida',NotaSalArr = '$NotaSalArr',NotaSalDep='$NotaSalDep' WHERE TrkID='$TrkID'";		
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
            }
        }
        //Si estatus de turck es recibido buscar y duplicar lotes
        
	if ($ban ==1){
	$consulta = "SELECT Status FROM Truks WHERE TrkID='$TrkID'";       
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetch();
        if($data['Status'] == "Received"){
            $consulta = "SELECT Typ, InReg FROM Lots, DOrds  WHERE Lots.DOrd=DOrds.DOrd and TrkID = '$TrkID';";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $dataDO=$resultado->fetch();
            if($dataDO['Typ'] == "CON"){
                $consulta = "SELECT * FROM Lots WHERE TrkID = '$TrkID'";
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
                $dataLot=$resultado->fetchAll(PDO::FETCH_ASSOC);
                    foreach($dataLot as $row){
                        $Crop = $row['Crop'];
                        $Lot = $row['Lot'];
                        $Reg = $row['Reg'];
                        $GinID = $row['GinID'];
                        $Loc = $dataDO['InReg'];
                        $Qty = $row['Qty'];
                        $LiqWgh = $row['LiqWgh'];
                        $Status = $row['Status'];
                        $Cert = $row['Cert'];
                        $TotalQty = $row['TotalQty'];
                        
                        //Consulta si el lote existe
                        $consulta = "SELECT * FROM Lots WHERE Lot = '$Lot' and Location='$Loc' and DOrd = 0";
                        $resultado = $conexion->prepare($consulta);
                        $resultado->execute();
                        $dataLote=$resultado->fetch();
                        if(!empty($dataLote)){
                            $QtyL = $dataLote['Qty'];
                            $WghL = $dataLote['LiqWgh'];
                            $Sum = $Qty + $QtyL;
                            $SumWgh = $LiqWgh + $WghL;
                            if($status_actual != "Received"){
                                $consulta = "UPDATE Lots SET Lots.Qty='$Sum', Lots.Unt='$Sum', Lots.LiqWgh='$SumWgh' WHERE Lots.Lot='$Lot' and Lots.Location = '$Loc'";
                                $resultado = $conexion->prepare($consulta);
                                $resultado->execute();
                            }
                             //lo que se va a guardar en el archivo de texto
                            $renglon .= "TRUCKID: ".$TrkID." "."Lote: ".$Lot."  "."cantidad en bodega: ".$QtyL."    "."cantidad de llegada: ". $Qty."  Usuario: ".$usermod."   "."Query:   ".$consulta. PHP_EOL;;
                          
                        }else{
                            $consulta = "INSERT INTO Lots (Crop, Lot, Qty, Reg, GinID, Location, Unt, LiqWgh, DOrd, TrkID, InvID, Status, Cert, TotalQty) VALUES('$Crop', '$Lot', '$Qty', '$Reg', '$GinID', '$Loc',  '$Qty', '$LiqWgh', '', '', '', '$Status', '$Cert', '$TotalQty') ";			
                            $resultado = $conexion->prepare($consulta);
                            $resultado->execute();
                             //lo que se va a guardar en el archivo de texto
                            $renglon .= "TRUCKID: ".$TrkID." "."Lote: ".$Lot."  "."cantidad de llegada: ". $Qty."  Usuario: ".$usermod."   "."Query:   ".$consulta. PHP_EOL;;
                            //fwrite($filetext, $renglon);
                            //fclose($filetext);    
                        }
                    }
                    
                         fwrite($filetext, $renglon);
                         fclose($filetext); 
                    
                    
              }
          }
        }

        if ($Status != "Cancelled"){
        
            $consulta = "SELECT OutPlc,	InReg, Typ FROM DOrds WHERE DOrd='$DOrd';";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $dataOutReg = $resultado->fetch();
            $OutReg1 = $dataOutReg['OutPlc'];
            $InReg1 = $dataOutReg['InReg'];
            $Typ1 = $dataOutReg['Typ'];
        
        
        
            $consulta = "SELECT OutReg,InReg,Typ FROM Requisition WHERE ReqID='$RqstID'";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $dataOutReg2 = $resultado->fetch();
            $OutReg2 = $dataOutReg2['OutReg'];
            $InReg2 = $dataOutReg2['InReg'];
            $Typ2 = $dataOutReg2['Typ'];
          
        
            if($OutReg1 ==  $OutReg2 && $InReg1 == $InReg2 && $Typ1 == $Typ2){
            
                if ($RqstID != $RqstID1){
                    $consulta = "UPDATE Requisition SET Cont=Cont+$cantidadlotes WHERE ReqID='$RqstID';";                
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                    
                    $consulta = "UPDATE Requisition SET Cont=Cont -$cantidadlotes WHERE ReqID='$RqstID1';";               
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                    //se actualiza PR por separado 
                    $consulta = "UPDATE Truks SET RqstID='$RqstID' WHERE TrkID='$TrkID';";               
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                }
            }
            
            else{               
           
                //cuando se quita una PR de un truck 
                $consulta = "UPDATE Truks SET RqstID='$RqstID' WHERE TrkID='$TrkID';";               
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();

                $consulta = "UPDATE Requisition SET Cont=Cont+$cantidadlotes WHERE ReqID='$RqstID';";                
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();

            }
        }
        
        //Actualiza contadores en DO
        $consulta = "UPDATE DOrds SET Rcv = (SELECT SUM(CrgQty) FROM Truks WHERE DO = '$DOrd' and Status = 'Received'), Tran = (SELECT SUM(CrgQty) FROM Truks WHERE DO = '$DOrd' and Status = 'Transit') 
                     WHERE DOrd = '$DOrd'";       
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        //--------------------------
        
        $consulta = "SELECT TrkID, TrkLPlt, TraLPlt, TNam, WBill, DrvNam, DrvTel, CrgQty, OutDat, OutWgh, InDat, InWgh, RqstID, LotsAssc, Comments, Status FROM Truks WHERE TrkID='$TrkID' ";       
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 3:        
        $consulta = "UPDATE Truks Set Status='Disable' WHERE TrkID='$TrkID'"; //"DELETE FROM Lots WHERE LotID='$LotID' ";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();                           
        break;
    case 4:
         //Busqueda global sin filtros
         if($muestras == "ALL" && $asLots == "" && $DepRegfil == "" && $ArrRegfil == "" && $idGlobal == "" && $DepArrDate =="" && $fromDate =="" && $toDate ==""){
            $consulta = "SELECT DISTINCT Truks.TrkID, TrkLPlt, TraLPlt,RemisionPDF,NumEcon,Truks.TipoManArr,Truks.TipoManDep,XML,TicketPeso,Incident, WBill,Samples, DrvLcs, DO, DrvNam, DrvTel, CrgQty, SchOutDate, SchOutTime, SchInDate, SchInTime, OutDat, OutTime, OutWgh, PWgh, InDat, InTime, InWgh, RqstID, PO, RO, LotsAssc, Comments, Truks.Status, IrrDat, Transports.BnName as TNam, DOrds.Typ as Typ, DOrds.Ctc as Ctc, DOrds.Cert as Cert, Clients.Cli as PlcNameIn, R.RegNam as RegNameOut, R1.RegNam as RegNameIn, CrgQty as LotQty, PWgh as LiqWgh, Cost,DateCreated,Invoice, FileCertificados,
            ifnull(GinName, (SELECT GinName FROM DOrds, Gines WHERE Truks.DO = DOrds.DOrd and DOrds.Gin = Gines.IDGin)) as GinName 
            FROM Truks
            join DOrds on Truks.DO = DOrds.Dord
            join Region R on DOrds.OutPlc = R.IDReg
            join Region R1 on DOrds.InReg = R1.IDReg
            left join Clients on DOrds.InPlc = Clients.CliID
            left join Transports on Truks.TNam = Transports.TptID
            left join Lots on Truks.DO = Lots.DOrd and Truks.LotsAssc = Lots.Lot
            left join Gines on Lots.GinID = Gines.IDGin
            order by Truks.TrkID DESC; ";
    }elseif($idTrk != "" && $idGlobal == "" && $asLots == "" && $DepRegfil == "" && $ArrRegfil == ""  && $regionfil == ""){
        // Busqueda solo por TrkID
        $numMues =1; 
        $consulta = "SELECT DISTINCT Truks.TrkID, TrkLPlt, TraLPlt, WBill,RemisionPDF,XML,TicketPeso,NumEcon,Truks.TipoManArr,Truks.TipoManArr,Samples,Incident,DrvLcs, DO, DrvNam, DrvTel, CrgQty, SchOutDate, SchOutTime, SchInDate, SchInTime, OutDat, OutTime, OutWgh, PWgh, InDat, InTime, InWgh, RqstID, PO, RO, LotsAssc, Comments, Truks.Status, IrrDat, Transports.BnName as TNam, DOrds.Typ as Typ, DOrds.Ctc as Ctc, DOrds.Cert as Cert, Clients.Cli as PlcNameIn, R.RegNam as RegNameOut, R1.RegNam as RegNameIn, CrgQty as LotQty, PWgh as LiqWgh, Cost,DateCreated,Invoice, FileCertificados,
        ifnull(GinName, (SELECT GinName FROM DOrds, Gines WHERE Truks.DO = DOrds.DOrd and DOrds.Gin = Gines.IDGin)) as GinName 
        FROM Truks
        join DOrds on Truks.DO = DOrds.Dord
        join Region R on DOrds.OutPlc = R.IDReg
        join Region R1 on DOrds.InReg = R1.IDReg
        left join Clients on DOrds.InPlc = Clients.CliID
        left join Transports on Truks.TNam = Transports.TptID
        left join Lots on Truks.DO = Lots.DOrd and Truks.LotsAssc = Lots.Lot
        left join Gines on Lots.GinID = Gines.IDGin
            where Truks.TrkID = '".$idTrk."'";

        if($DepArrDate == "DepartureDate"){
            if($fromDate != "")
                $consulta .= '  AND Truks.OutDat >= "'.$fromDate.'"';
            if($toDate != "")
                $consulta .= '  AND Truks.OutDat <= "'.$toDate.'"';
        }
        if($DepArrDate == "ArrivalDate"){
            if($fromDate != "")
                $consulta .= '  AND Truks.InDat >= "'.$fromDate.'"';
            if($toDate != "")
                $consulta .= '  AND Truks.InDat <= "'.$toDate.'"';
        }
            $consulta .= ' order by Truks.TrkID DESC;';
    }elseif($idGlobal != "" && $asLots == "" && $DepRegfil == "" && $ArrRegfil == ""  && $regionfil == ""){
        ///Busqueda para cuando sólo se pide el idDO
        $numMues =1; 
            $consulta = "SELECT DISTINCT Truks.TrkID, TrkLPlt, TraLPlt,RemisionPDF,XML,TicketPeso,NumEcon,Truks.TipoManArr,Truks.TipoManDep,Incident,WBill,Samples,DrvLcs, DO, DrvNam, DrvTel, CrgQty, SchOutDate, SchOutTime, SchInDate, SchInTime, OutDat, OutTime, OutWgh, PWgh, InDat, InTime, InWgh, RqstID, PO, RO, LotsAssc, Comments, Truks.Status, IrrDat, Transports.BnName as TNam, DOrds.Typ as Typ, DOrds.Ctc as Ctc, DOrds.Cert as Cert, Clients.Cli as PlcNameIn, R.RegNam as RegNameOut, R1.RegNam as RegNameIn, CrgQty as LotQty, PWgh as LiqWgh, Cost,DateCreated,Invoice, FileCertificados,
            ifnull(GinName, (SELECT GinName FROM DOrds, Gines WHERE Truks.DO = DOrds.DOrd and DOrds.Gin = Gines.IDGin)) as GinName 
            FROM Truks
            join DOrds on Truks.DO = DOrds.Dord
            join Region R on DOrds.OutPlc = R.IDReg
            join Region R1 on DOrds.InReg = R1.IDReg
            left join Clients on DOrds.InPlc = Clients.CliID
            left join Transports on Truks.TNam = Transports.TptID
            left join Lots on Truks.DO = Lots.DOrd and Truks.LotsAssc = Lots.Lot
            left join Gines on Lots.GinID = Gines.IDGin
            where Truks.DO LIKE '%".$idGlobal."%' ";

            if($DepArrDate == "DepartureDate"){
                if($fromDate != "")
                    $consulta .= '  AND Truks.OutDat >= "'.$fromDate.'"';
                if($toDate != "")
                    $consulta .= '  AND Truks.OutDat <= "'.$toDate.'"';
            }
            if($DepArrDate == "ArrivalDate"){
                if($fromDate != "")
                    $consulta .= '  AND Truks.InDat >= "'.$fromDate.'"';
                if($toDate != "")
                    $consulta .= '  AND Truks.InDat <= "'.$toDate.'"';
            }
                $consulta .= ' order by Truks.TrkID DESC;';
            //echo 'Entro';

    }elseif($idGlobal == "" && $asLots != "" && $DepRegfil == "" && $ArrRegfil == "" && $regionfil == ""){
        ///Busqueda para cuando sólo se pide Associated Lots
        $numMues =1; 
        $consulta = "SELECT DISTINCT Truks.TrkID, TrkLPlt, TraLPlt,RemisionPDF,XML, TicketPeso,NumEcon,Truks.TipoManArr,Truks.TipoManDep,WBill,Incident,Samples,DrvLcs, DO, DrvNam, DrvTel, CrgQty, SchOutDate, SchOutTime, SchInDate, SchInTime, OutDat, OutTime, OutWgh, PWgh, InDat, InTime, InWgh, RqstID, PO, RO, LotsAssc, Comments, Truks.Status, IrrDat, Transports.BnName as TNam, DOrds.Typ as Typ, DOrds.Ctc as Ctc, DOrds.Cert as Cert, Clients.Cli as PlcNameIn, R.RegNam as RegNameOut, R1.RegNam as RegNameIn, CrgQty as LotQty, PWgh as LiqWgh, Cost,DateCreated,Invoice, FileCertificados,
        ifnull(GinName, (SELECT GinName FROM DOrds, Gines WHERE Truks.DO = DOrds.DOrd and DOrds.Gin = Gines.IDGin)) as GinName 
        FROM Truks
        join DOrds on Truks.DO = DOrds.Dord
        join Region R on DOrds.OutPlc = R.IDReg
        join Region R1 on DOrds.InReg = R1.IDReg
        left join Clients on DOrds.InPlc = Clients.CliID
        left join Transports on Truks.TNam = Transports.TptID
        left join Lots on Truks.DO = Lots.DOrd and Truks.LotsAssc = Lots.Lot
        left join Gines on Lots.GinID = Gines.IDGin
            where Truks.LotsAssc LIKE '%".$asLots."%'";

        if($DepArrDate == "DepartureDate"){
            if($fromDate != "")
                $consulta .= '  AND Truks.OutDat >= "'.$fromDate.'"';
            if($toDate != "")
                $consulta .= '  AND Truks.OutDat <= "'.$toDate.'"';
        }
        if($DepArrDate == "ArrivalDate"){
            if($fromDate != "")
                $consulta .= '  AND Truks.InDat >= "'.$fromDate.'"';
            if($toDate != "")
                $consulta .= '  AND Truks.InDat <= "'.$toDate.'"';
        }
            $consulta .= ' order by Truks.TrkID DESC;';
            
    }elseif($idGlobal != "" && $asLots != "" && $DepRegfil == "" && $ArrRegfil == "" && $regionfil == ""){
        ///Busqueda para cuando sólo se pide Associated Lots y ID DO
        $numMues =1; 
        $consulta = "SELECT DISTINCT Truks.TrkID, TrkLPlt, TraLPlt, WBill, DrvLcs,RemisionPDF,XML,TicketPeso,NumEcon,Truks.TipoManArr,Truks.TipoManDep,Incident,Samples,DO, DrvNam, DrvTel, CrgQty, SchOutDate, SchOutTime, SchInDate, SchInTime, OutDat, OutTime, OutWgh, PWgh, InDat, InTime, InWgh, RqstID, PO, RO, LotsAssc, Comments, Truks.Status, IrrDat, Transports.BnName as TNam, DOrds.Typ as Typ, DOrds.Ctc as Ctc, DOrds.Cert as Cert, Clients.Cli as PlcNameIn, R.RegNam as RegNameOut, R1.RegNam as RegNameIn, CrgQty as LotQty, PWgh as LiqWgh, Cost,DateCreated,Invoice, FileCertificados,
        ifnull(GinName, (SELECT GinName FROM DOrds, Gines WHERE Truks.DO = DOrds.DOrd and DOrds.Gin = Gines.IDGin)) as GinName 
        FROM Truks
        join DOrds on Truks.DO = DOrds.Dord
        join Region R on DOrds.OutPlc = R.IDReg
        join Region R1 on DOrds.InReg = R1.IDReg
        left join Clients on DOrds.InPlc = Clients.CliID
        left join Transports on Truks.TNam = Transports.TptID
        left join Lots on Truks.DO = Lots.DOrd and Truks.LotsAssc = Lots.Lot
        left join Gines on Lots.GinID = Gines.IDGin
            where Truks.LotsAssc LIKE '%".$asLots."%' AND Truks.DO LIKE '%".$idGlobal."%' ";

        if($DepArrDate == "DepartureDate"){
            if($fromDate != "")
                $consulta .= '  AND Truks.OutDat >= "'.$fromDate.'"';
            if($toDate != "")
                $consulta .= '  AND Truks.OutDat <= "'.$toDate.'"';
        }
        if($DepArrDate == "ArrivalDate"){
            if($fromDate != "")
                $consulta .= '  AND Truks.InDat >= "'.$fromDate.'"';
            if($toDate != "")
                $consulta .= '  AND Truks.InDat <= "'.$toDate.'"';
        }
            $consulta .= ' order by Truks.TrkID DESC;';
            
    }elseif($idGlobal == "" && $asLots == "" && ($DepRegfil != "" || $ArrRegfil != "") && $regionfil == ""){
        ///Busqueda para cuando sólo se pide el Departure Region o Arrival Region entre x fechas 
       /* if($muestras != 0){
            $query = "SELECT TrkID FROM Truks ORDER BY TrkID DESC LIMIT 1";
            $result = $conexion->prepare($query);
            $result->execute();
            $firstID = $result->fetch();
            $firstID = $firstID['TrkID'];
            $aux = $firstID;
            $i = 0;
            while($i < $muestras){
                $query = "SELECT DISTINCT T.TrkID as TrkID  R.RegNam as RegNameOut, R1.RegNam as RegNameIn 
                FROM Truks T 
                join DOrds on Truks.DO = DOrds.Dord
                join Region R on DOrds.OutPlc = R.IDReg
                join Region R1 on DOrds.InReg = R1.IDReg
                where TrkID = ".$aux." AND RegNameOut = '".$DepRegfil."';";
                $result = $conexion->prepare($query);
                $result->execute();
                if($fila = $result->fetch(PDO::FETCH_ASSOC)){
                   // echo $i." ";
                    $i++;
                    $last = $fila['TrkID'];
                    
                }
                $aux--;
            }}

           $RANGO = $last;
           /* if($DepRegfil != ""){
                $query = 'SELECT IDReg from Region where RegNam = "'.$DepRegfil.'"';
                $result = $conexion->prepare($query);
                $result->execute();
                $idDepReg = $result->fetch(); 
            }
            if($ArrRegfil != ""){
                $query = 'SELECT IDReg from Region where RegNam = "'.$ArrRegfil.'"';
                $result = $conexion->prepare($query);
                $result->execute();
                $idArrReg = $result->fetch(); 
            }
            $numMues =0; 
            $regions = 1;
            $consulta = "SELECT DISTINCT Truks.TrkID, TrkLPlt, TraLPlt, WBill, DrvLcs, DO, DrvNam, DrvTel, 
            CrgQty, OutDat, OutTime, OutWgh, PWgh, InDat, InTime, InWgh, 
            RqstID, PO, RO, LotsAssc, Comments, Truks.Status, Transports.BnName as TNam, DOrds.Typ as Typ, 
            DOrds.Ctc as Ctc, DOrds.Cert as Cert, Clients.Cli as PlcNameIn, R.RegNam as RegNameOut, R1.RegNam as RegNameIn, CrgQty as LotQty, PWgh as LiqWgh,
            ifnull(GinName, (SELECT GinName FROM DOrds, Gines WHERE Truks.DO = DOrds.DOrd and DOrds.Gin = Gines.IDGin)) as GinName 
            FROM Truks
            join DOrds on Truks.DO = DOrds.Dord
            join Region R on DOrds.OutPlc = R.IDReg
            join Region R1 on DOrds.InReg = R1.IDReg
            left join Clients on DOrds.InPlc = Clients.CliID
            left join Transports on Truks.TNam = Transports.TptID
            left join Lots on Truks.DO = Lots.DOrd and Truks.LotsAssc = Lots.Lot
            left join Gines on Lots.GinID = Gines.IDGin ";
            if($muestras != 0){
                $consulta .= "  where Truks.TrkID <= " . $firstID . " AND Truks.TrkID >= " . $RANGO;
                }
           /* if($DepRegfil != ""){
                $consulta .= ' DOrds.OutPlc = '.$idDepReg['IDReg'];
            }
            if($DepRegfil != "" && $ArrRegfil != ""){
                $consulta .= ' AND ';
            }
            if($ArrRegfil != ""){
                $consulta .= ' DOrds.InReg = '.$idArrReg['IDReg'];
            }

           if($DepArrDate != ""){ 
               $consulta .= ' where ';
            } 

            if($DepArrDate == "DepartureDate"){
                if($fromDate != "")
                    $consulta .= '  Truks.OutDat >= "'.$fromDate.'"';
                if($fromDate != "" && $toDate != "")
                    $consulta .= ' AND ';
                if($toDate != "")
                    $consulta .= ' Truks.OutDat <= "'.$toDate.'"';
            }
            if($DepArrDate == "ArrivalDate"){
                if($fromDate != "")
                    $consulta .= ' Truks.InDat >= "'.$fromDate.'"';
                if($fromDate != "" && $toDate != "")
                    $consulta .= ' AND ';    
                if($toDate != "")
                    $consulta .= ' Truks.InDat <= "'.$toDate.'"';
            }

            $consulta .= ';';
//echo $consulta;
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
             
            if($DepRegfil != "" && $ArrRegfil !=""){
                $i = 0;
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)){
                    if($i >= $muestras)
                            break;
                    if($row['RegNameOut'] == $DepRegfil && $row['RegNameIn']== $ArrRegfil){
                        $data[] = $row;
                        $i++;
                    }
                }
            }else{
                $i = 0;
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)){
                        if($i >= $muestras)
                             break;
                    if($row['RegNameOut'] == $DepRegfil || $row['RegNameIn']== $ArrRegfil){
                        $data[] = $row;
                        $i++;
                    }
                    
                }
            }   
            
        */
        if($DepRegfil != ""){
            $query = 'SELECT IDReg from Region where RegNam = "'.$DepRegfil.'"';
            $result = $conexion->prepare($query);
            $result->execute();
            $idDepReg = $result->fetch(); 
        }
        if($ArrRegfil != ""){
            $query = 'SELECT IDReg from Region where RegNam = "'.$ArrRegfil.'"';
            $result = $conexion->prepare($query);
            $result->execute();
            $idArrReg = $result->fetch(); 
        }

        $numMues =1;
        
        $consulta = "SELECT DISTINCT Truks.TrkID, TrkLPlt, TraLPlt, WBill, DrvLcs,RemisionPDF,XML,TicketPeso,NumEcon,Truks.TipoManArr,Truks.TipoManDep,Samples,Incident,DO, DrvNam, DrvTel, CrgQty, SchOutDate, SchOutTime, SchInDate, SchInTime, OutDat, OutTime, OutWgh, PWgh, InDat, InTime, InWgh, RqstID, PO, RO, LotsAssc, Comments, Truks.Status, IrrDat, Transports.BnName as TNam, DOrds.Typ as Typ, DOrds.Ctc as Ctc, DOrds.Cert as Cert, Clients.Cli as PlcNameIn, R.RegNam as RegNameOut, R1.RegNam as RegNameIn, CrgQty as LotQty, PWgh as LiqWgh, Cost,DateCreated,Invoice, FileCertificados,
        ifnull(GinName, (SELECT GinName FROM DOrds, Gines WHERE Truks.DO = DOrds.DOrd and DOrds.Gin = Gines.IDGin)) as GinName 
        FROM Truks
        join DOrds on Truks.DO = DOrds.Dord
        join Region R on DOrds.OutPlc = R.IDReg
        join Region R1 on DOrds.InReg = R1.IDReg
        left join Clients on DOrds.InPlc = Clients.CliID
        left join Transports on Truks.TNam = Transports.TptID
        left join Lots on Truks.DO = Lots.DOrd and Truks.LotsAssc = Lots.Lot
        left join Gines on Lots.GinID = Gines.IDGin
        where ";

        if($DepRegfil != ""){
            $consulta .= ' DOrds.OutPlc = '.$idDepReg['IDReg'];
        }
        if($DepRegfil != "" && $ArrRegfil != ""){
            $consulta .= ' AND ';
        }
        if($ArrRegfil != ""){
            $consulta .= ' DOrds.InReg = '.$idArrReg['IDReg'];
        }

        
        if($DepArrDate != ""){ 
            $consulta .= ' AND  ';
         }                        
            
            if($DepArrDate == "DepartureDate"){
                if($fromDate != "")
                    $consulta .= ' Truks.OutDat >= "'.$fromDate.'"';
                if($fromDate != "" && $toDate != "")
                    $consulta .= ' AND ';
                if($toDate != "")
                    $consulta .= ' Truks.OutDat <= "'.$toDate.'"';
            }
            if($DepArrDate == "ArrivalDate"){
                if($fromDate != "")
                    $consulta .= ' Truks.InDat >= "'.$fromDate.'"';
                if($fromDate != "" && $toDate != "")
                    $consulta .= ' AND ';    
                if($toDate != "")
                    $consulta .= ' Truks.InDat <= "'.$toDate.'"';
            }
            $consulta .= ';';
          /*  $consulta .= ' order by Truks.TrkID DESC;';
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
             
            if($DepRegfil != "" && $ArrRegfil !=""){
                $i = 0;
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)){
                   
                    if($row['RegNameOut'] == $DepRegfil && $row['RegNameIn']== $ArrRegfil){
                        $data[] = $row;
                        $i++;
                    }
                }
            }else{
                $i = 0;
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)){
                        
                    if($row['RegNameOut'] == $DepRegfil || $row['RegNameIn']== $ArrRegfil){
                        $data[] = $row;
                        $i++;
                    }
                    
                }
            }   */
    


        }elseif($idGlobal == ""  && $asLots == "" && $DepRegfil == "" && $ArrRegfil == "" && $DepArrDate =="" && $fromDate =="" && $toDate =="" && $muestras != "" && $regionfil == ""){
        ///Busqueda para cuando sólo se pide Muestras
        if($muestras != 0){
        $query = "SELECT TrkID FROM Truks ORDER BY TrkID DESC LIMIT 1";
        $result = $conexion->prepare($query);
        $result->execute();
        $firstID = $result->fetch();
        $firstID = $firstID['TrkID'];
        $aux = $firstID;
        $i = 0;
        while($i < $muestras){
            $query = "SELECT TrkID FROM Truks where TrkID = ".$aux.";";
            $result = $conexion->prepare($query);
            $result->execute();
            if($fila = $result->fetch(PDO::FETCH_ASSOC)){
               // echo $i." ";
                $i++;
                $last = $fila['TrkID'];
                
            }
            $aux--;
            if($aux == 0){
                break;
            }
        }

       $RANGO = $last;

        //    $RANGO = (int)$firstID - (int)$muestras; 
    }
        $consulta = "SELECT DISTINCT Truks.TrkID, TrkLPlt, TraLPlt, WBill, DrvLcs, RemisionPDF,XML,TicketPeso,NumEcon,Truks.TipoManArr,Truks.TipoManDep,Incident,DO,Samples,DrvNam, DrvTel, CrgQty, SchOutDate, SchOutTime, SchInDate, SchInTime, OutDat, OutTime, OutWgh, PWgh, InDat, InTime, InWgh, RqstID, PO, RO, LotsAssc, Comments, Truks.Status, IrrDat, Transports.BnName as TNam, DOrds.Typ as Typ, DOrds.Ctc as Ctc, DOrds.Cert as Cert, Clients.Cli as PlcNameIn, R.RegNam as RegNameOut, R1.RegNam as RegNameIn, CrgQty as LotQty, PWgh as LiqWgh, Cost,DateCreated,Invoice, FileCertificados,
        ifnull(GinName, (SELECT GinName FROM DOrds, Gines WHERE Truks.DO = DOrds.DOrd and DOrds.Gin = Gines.IDGin)) as GinName 
        FROM Truks
        join DOrds on Truks.DO = DOrds.Dord
        join Region R on DOrds.OutPlc = R.IDReg
        join Region R1 on DOrds.InReg = R1.IDReg
        left join Clients on DOrds.InPlc = Clients.CliID
        left join Transports on Truks.TNam = Transports.TptID
        left join Lots on Truks.DO = Lots.DOrd and Truks.LotsAssc = Lots.Lot
        left join Gines on Lots.GinID = Gines.IDGin";
        if($muestras != 0){
        $consulta .= "  where Truks.TrkID <= " . $firstID . " AND Truks.TrkID >= " . $RANGO;
        } 
        $consulta .= " order by Truks.TrkID DESC;"; 
       // echo $consulta;
    }elseif($idGlobal == "" && $asLots == "" && $DepRegfil == "" && $ArrRegfil == "" && $DepArrDate !="" && ($fromDate !="" || $toDate !="") && $regionfil == ""){
        ///Busqueda para cuando sólo se pide el rango de fechas
        $numMues =1;
        
        $consulta = "SELECT DISTINCT Truks.TrkID, TrkLPlt, TraLPlt, WBill, DrvLcs, DO,RemisionPDF,XML,Incident,TicketPeso,NumEcon,Truks.TipoManArr,Truks.TipoManDep,Samples,DrvNam, DrvTel, CrgQty, SchOutDate, SchOutTime, SchInDate, SchInTime, OutDat, OutTime, OutWgh, PWgh, InDat, InTime, InWgh, RqstID, PO, RO, LotsAssc, Comments, Truks.Status, IrrDat, Transports.BnName as TNam, DOrds.Typ as Typ, DOrds.Ctc as Ctc, DOrds.Cert as Cert, Clients.Cli as PlcNameIn, R.RegNam as RegNameOut, R1.RegNam as RegNameIn, CrgQty as LotQty, PWgh as LiqWgh, Cost,DateCreated,Invoice,FileCertificados,
        ifnull(GinName, (SELECT GinName FROM DOrds, Gines WHERE Truks.DO = DOrds.DOrd and DOrds.Gin = Gines.IDGin)) as GinName 
        FROM Truks
        join DOrds on Truks.DO = DOrds.Dord
        join Region R on DOrds.OutPlc = R.IDReg
        join Region R1 on DOrds.InReg = R1.IDReg
        left join Clients on DOrds.InPlc = Clients.CliID
        left join Transports on Truks.TNam = Transports.TptID
        left join Lots on Truks.DO = Lots.DOrd and Truks.LotsAssc = Lots.Lot
        left join Gines on Lots.GinID = Gines.IDGin
                where ";
                
            
            if($DepArrDate == "DepartureDate"){
                if($fromDate != "")
                    $consulta .= ' Truks.OutDat >= "'.$fromDate.'"';
                if($fromDate != "" && $toDate != "")
                    $consulta .= ' AND ';
                if($toDate != "")
                    $consulta .= ' Truks.OutDat <= "'.$toDate.'"';
            }
            if($DepArrDate == "ArrivalDate"){
                if($fromDate != "")
                    $consulta .= ' Truks.InDat >= "'.$fromDate.'"';
                if($fromDate != "" && $toDate != "")
                    $consulta .= ' AND ';    
                if($toDate != "")
                    $consulta .= ' Truks.InDat <= "'.$toDate.'"';
            }
            
            $consulta .= ' order by Truks.TrkID DESC;';
    }
  

if($consulta !=""){
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$data=$resultado->fetchAll(PDO::FETCH_ASSOC);
}

/* if($muestras != "All" && $muestras != "" && $numMues == 1 ){
    $i = 0; 
    $aux = array();
    foreach($data as $dat){
        if($i >= $muestras)
            break;
        $aux [] = $dat;
        $i++;
    }
    $data = $aux;
} */


  //Borramos los filtros una vez que la generamos
 /*   $_SESSION['Filters'] = array(
        "muestras" => "200",
        "idGlobal" => "",
        "asLots" => "",
        "regionfil" => "",
        "DepRegFil" => "",
        "ArrRegFil" => "",
        "DepArrDate" => "",
        "fromdate" => "",
        "todate" => ""
    );*/

break;


    case 5:    
        $consulta = "SELECT DOrds.OutPlc, DOrds.InReg, DOrds.Typ, DOrds.InPlc,Samp,
        (SELECT RegNam FROM Region WHERE IDReg = DOrds.OutPlc) as PlcOut,
        (SELECT RegNam FROM Region WHERE IDReg = DOrds.InReg) as RegIn,
        (SELECT Cli FROM Clients WHERE CliID = DOrds.InPlc) as PlcIn,
        Transports.TptID, Transports.TptCo, Transports.BnName
        FROM DOrds 
        left join Routes on DOrds.OutPlc = Routes.OutReg
        left join Transports on Routes.TptID = Transports.TptID
        WHERE DOrds.DOrd='$DO' order by Transports.BnName"; // AND Transports.Status='1' "SELECT LotID, Lot, Lots.Qty, OutPlc, InReg, Typ, InPlc FROM `lots`, DOrds WHERE Lots.Dord='$DO' and Lots.TrkID='0' and DOrds.DOrd='$DO'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 6:    
        $consulta = "SELECT distinct LotID, Lots.Lot, Lots.Qty FROM Lots, DOrds WHERE Lots.DOrd='$DO' and TrkID='0' and Status!='DISABLE' order by Lot";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 7:    
        $consulta = "UPDATE Lots Set TrkID='$TrkID' WHERE LotID='$id'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        
        $consulta = "SELECT Lot FROM Lots WHERE LotID='$id'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataT=$resultado->fetch(); //All(PDO::FETCH_ASSOC);
        
        $concat = "";
        //$consulta = "SELECT Comments FROM Truks WHERE TrkID='$TrkID'";
        //$resultado = $conexion->prepare($consulta);
        //$resultado->execute();
        //$data=$resultado->fetchAll(PDO::FETCH_ASSOC); //$resultado->fetch(); //All(PDO::FETCH_ASSOC);
        
        foreach($dataT as $row){
            $consulta = "SELECT LotsAssc FROM Truks WHERE TrkID='$TrkID'";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetch(); //All(PDO::FETCH_ASSOC);
            $concat = $dataT['Lot'].", ".$data['Comments'];
        }
        
        /*if(sizeof($data) > 0){
            foreach ($data as $row){
                $concat = $dataT['Lot'].",".$data['Comments']; //.PHP_EOL;
            }
        }else{
            $concat = $data['Lot'];
        }*/
        
        /*$data=$resultado->fetch(); //All(PDO::FETCH_ASSOC);
        if(sizeof($data) > 0){
            $concat = $data['Comments'].", ".$CrgQty;
        }else{
            $concat = $CrgQty;
        }*/
        //$concat = array_merge($data['Comments'],$dataL['Lot']);
        
        $consulta = "UPDATE Truks Set LotsAssc='$concat' WHERE TrkID='$TrkID'"; //"DELETE FROM Lots WHERE LotID='$LotID' ";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute(); 
        break;
    case 8:
        /*$consulta = "SELECT AUTO_INCREMENT as TrkID
                    FROM information_schema.TABLES
                    WHERE TABLE_SCHEMA = 'database_amsa'
                    AND TABLE_NAME = 'Truks';";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetch();
        $TrkID = $data['TrkID'];*/
        
        $consulta = "UPDATE Lots Set Lots.TrkID='$TrkID' WHERE Lots.LotID='$LotID'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        
        $consulta = "SELECT Lot,
        (SELECT SUM(Qty) FROM Lots WHERE TrkID = '$TrkID') as CrgQty,
        (SELECT SUM(LiqWgh) PWgh FROM Lots WHERE TrkID = '$TrkID') as PWgh
        FROM Lots WHERE TrkID = '$TrkID'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 9:
        $consulta = "UPDATE Lots Set Lots.TrkID='$TrkID' WHERE Lots.LotID='$LotID'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        
        $consulta = "SELECT Lot,
        (SELECT SUM(Qty) FROM Lots WHERE TrkID = '$TrkID') as CrgQty,
        (SELECT SUM(LiqWgh) PWgh FROM Lots WHERE TrkID = '$TrkID') as PWgh
        FROM Lots WHERE TrkID = '$TrkID'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 10:        
        $consulta = "UPDATE Lots Set TrkID='0' WHERE Lots.LotID = '$LotID' and TrkID='$TrkID'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        
        $consulta = "SELECT Lot,
        (SELECT SUM(Qty) FROM Lots WHERE TrkID = '$TrkID') as CrgQty,
        (SELECT SUM(LiqWgh) PWgh FROM Lots WHERE TrkID = '$TrkID') as PWgh
        FROM Lots WHERE TrkID = '$TrkID'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 11:
        /*$consulta = "SELECT AUTO_INCREMENT as TrkID
                    FROM information_schema.TABLES
                    WHERE TABLE_SCHEMA = 'database_amsa'
                    AND TABLE_NAME = 'Truks';";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetch();
        $TrkID = $data['TrkID'];*/
        
        $consulta = "UPDATE Lots Set TrkID='0' WHERE Lots.LotID = '$LotID' and TrkID='$TrkID'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        
        $consulta = "SELECT Lot,
        (SELECT SUM(Qty) FROM Lots WHERE TrkID = '$TrkID') as CrgQty,
        (SELECT SUM(LiqWgh) PWgh FROM Lots WHERE TrkID = '$TrkID') as PWgh
        FROM Lots WHERE TrkID = '$TrkID'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 12:    
        $consulta = "SELECT distinct LotID, Lots.Lot, Lots.Qty FROM Lots, DOrds WHERE Lots.Status!='DISABLE' and Lots.DOrd='$DO' and Lots.TrkID IN (0,'$TrkID') order by Lot";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
   case 13:
        $consulta = "SELECT IDReg FROM Region WHERE RegNam='$LocSplit'";
	    $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataOutPlc = $resultado->fetch();
        $Loc = $dataOutPlc['IDReg'];
        
        $consulta = "SELECT * FROM Lots WHERE Location='$Loc' and DOrd='$DOSplit' and TrkID='0' ORDER BY Lot";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
        
    case 14:
        $consulta = "SELECT * FROM Lots WHERE LotID='$LotIDSplit'";
	    $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataOutPlc = $resultado->fetch();
        $Crop = $dataOutPlc['Crop'];
        $Lot = $dataOutPlc['Lot'];
        $Reg = $dataOutPlc['Reg'];
        $Gin = $dataOutPlc['GinID'];
        $Loc = $dataOutPlc['Location'];
        $Qlty = $dataOutPlc['Qlty'];
        $Cmt = $dataOutPlc['Cmt'];
        $Recap = $dataOutPlc['Recap'];
        $CliID = $dataOutPlc['CliID'];
        $AsgmDate = $dataOutPlc['AsgmDate'];
        $DOrd = $dataOutPlc['DOrd'];
        $SchDate = $dataOutPlc['SchDate'];
        $TrkID = $dataOutPlc['TrkID'];
        $InvID = $dataOutPlc['InvID'];
        $Status = $dataOutPlc['Status'];
        $LiqWgh = $dataOutPlc['LiqWgh'];
        $Qty = $dataOutPlc['Qty'];
        $Cert = $dataOutPlc['Cert'];
        $TotalQty = $dataOutPlc['TotalQty'];
        
        $fil = count($Split);
        
        for ($i=0; $i<$fil; ++$i){
            if ($LiqWgh != 0){
              $WghSplit = round((($LiqWgh/$Qty)*$Split[$i]),2);
            }else{
              $WghSplit = 0;
            }
            if ($i==0){
                $consulta = "UPDATE Lots SET Lot='$Lot', Reg='$Reg', GinID='$Gin', Location='$Loc', Qty='$Split[$i]', Unt='$Split[$i]', LiqWgh='$WghSplit', Qlty='$Qlty', Cmt='$Cmt', Recap='$Recap', CliID='$CliID', AsgmDate='$AsgmDate', DOrd='$DOrd', SchDate='$SchDate', TrkID='$TrkID', InvID='$InvID', Crop='$Crop', Status='$Status' WHERE LotID='$LotIDSplit';";
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
            }else{
                $consulta = "INSERT INTO Lots (Crop, Lot, Reg, GinID, Location, Qty, Unt, LiqWgh, Qlty, Cmt, Recap, CliID, AsgmDate, DOrd, SchDate, TrkID, InvID, Status, Cert, TotalQty) VALUES ('$Crop', '$Lot', '$Reg', '$Gin', '$Loc', '$Split[$i]', '$Split[$i]', '$WghSplit', '$Qlty', '$Cmt', '$Recap', '$CliID', '$AsgmDate', '$DOrd', '$SchDate', '$TrkID', '$InvID', '$Status', '$Cert', '$TotalQty');";
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
            }
        }
        
        /*$consulta = "SELECT * FROM Lots WHERE Location='$Loc' and DOrd='$DOSplit' and TrkID='0'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);*/
        break;
    case 15:
        $consulta = "SELECT * FROM DOrds WHERE DOrd='$DO'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataOutReg = $resultado->fetch();
        $OutReg = $dataOutReg['OutPlc'];
        $InReg = $dataOutReg['InReg'];
        $Typ = $dataOutReg['Typ'];
        
   
        $consulta = "SELECT ReqID,Services,OutReg,InReg,Typ,Cont,(Services-Cont) as Restante  FROM Requisition WHERE Services > Cont and (Services-Cont) >= $cantidadlotes and OutReg = '$OutReg' and InReg = '$InReg' and Typ = '$Typ';";
	    $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 16:
        if ($Status != "Cancelled"){
            if ($RqstID1 != $RqstID2){
                $consulta = "UPDATE Requisition SET Cont=Cont - 1 WHERE ReqID='$RqstID1';";
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
                
                $consulta = "UPDATE Requisition SET Cont=Cont + 1 WHERE ReqID='$RqstID2';";
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
            }
        }
        break;
        case 17:
            $consulta ="SELECT FreighCostNote FROM Truks WHERE TrkID='$TrkID'";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $nota = $resultado->fetch();
            $nota=$nota['FreighCostNote'];

            if($nota==""){
                $nota=strtoupper($comentario); 
            }
            else{
                $nota.= "|". strtoupper($comentario);

            }


            $consulta = "UPDATE Truks SET PO='0', FreighCostNote = '$nota' WHERE TrkID='$TrkID';";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;


        case 18:
            if($totalcost !="" && $totalcost > 0){
            $consulta = "UPDATE Truks SET FreightCost=$totalcost, FreighCostNote ='$tarifnota',Cost='1' WHERE TrkID='$TrkID' ;";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            }
            else{
                $consulta = "UPDATE Truks SET FreighCostNote ='$tarifnota' WHERE TrkID='$TrkID' ;";
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
                }
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;

        case 19:
           
            $consulta = "UPDATE Truks SET LotsAssc='$LotsAssc', PWgh ='$PWgh',CrgQty='$CrgQty' WHERE TrkID='$TrkID' and Status != 'Received' ;";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
           
 
        break;

        case 20:
           
            $consulta = "UPDATE Truks SET LotsAssc='', PWgh =0 ,CrgQty=0 , CrgUnt=0 WHERE TrkID='$TrkID' and Status != 'Received';";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();

            $consulta = "UPDATE Lots SET TrkID='0' WHERE TrkID='$TrkID' ;";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
           
 
        break;

        //eliminar la PR del TrkID
        case 21:
            

            $consulta ="SELECT FreighCostNote FROM Truks WHERE TrkID='$TrkID'";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $nota = $resultado->fetch();
            $nota=$nota['FreighCostNote'];

            if($nota==""){
                $nota=strtoupper($comentario); 
            }
            else{
                $nota.= "|". strtoupper($comentario);

            }



            $consulta = "UPDATE Truks SET RqstID='0', FreighCostNote = '$nota' WHERE TrkID='$TrkID';";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;

        case 22:            

            $consulta ="SELECT FreighCostNote FROM Truks WHERE TrkID='$TrkID'";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $nota = $resultado->fetch();
            $nota=$nota['FreighCostNote'];

            if($nota==""){
                $nota=strtoupper($comentario); 
            }
            else{
                $nota.= "|". strtoupper($comentario);

            }



            $consulta = "UPDATE Truks SET RqstID='0',PO='0', FreighCostNote = '$nota' WHERE TrkID='$TrkID';";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
        
         case 23:
            
            // Transport ID 137 es movimiento interno 
            $consulta ="SELECT Transports.TptID, Transports.BnName
            FROM Region
            LEFT JOIN Routes on Region.IDReg = Routes.OutReg
            LEFT JOIN Transports on Routes.TptID = Transports.TptID AND  Transports.TptID !=137
            WHERE RegNam = '$regionname' 
            HAVING Transports.BnName != ''
            Order BY Transports.BnName ASC;";

            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);

            break;    
        
        case 24:

            // Transport ID 137 es movimiento interno 
            $consulta ="SELECT Transports.TptID, Transports.BnName
            FROM Transports WHERE Transports.BnName != '' AND  Transports.TptID !=137
            Order BY Transports.BnName ASC;";

            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);

            break;


            case 25:
                $consulta = "SELECT TicketPeso From Truks WHERE TrkID='$TrkID';";  
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
            break;

            
            case 26:
                $consulta = "SELECT Lot,Qty From Lots WHERE TrkID='$TrkID';";  
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
            break;

            case 27:
                //$complemento="";
                $consulta = "SELECT Clients.State,Clients.Directo FROM amsadb1.Clients where  Clients.Cli='$Cliente';";                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
            break;

            case 28:
                $consulta = "SELECT Truks.TipoMan From Truks WHERE TrkID='$TrkID';";  
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
            break;

            
            case 29:
                $consulta = "SELECT Truks.TipoManArr,Truks.RespManArr,Truks.PQArr,Truks.TipoManDep,Truks.RespManDep,Truks.NotaSalDep,Truks.NotaSalArr,Truks.PQDep  From Truks WHERE TrkID='$TrkID';";  
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
            break;

            case 30:
                $consulta = "SELECT FileCertificados From Truks WHERE TrkID='$TrkID';";  
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
            break;
        }

print json_encode($data, JSON_UNESCAPED_UNICODE); //envio el array final el formato json a AJAX
$conexion=null;
