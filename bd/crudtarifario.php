<?php 

include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set('America/Los_Angeles');
setlocale(LC_TIME, "spanish");

$delibery = (isset($_POST['dor'])) ? $_POST['dor'] : '';
$regsalida = (isset($_POST['regsal'])) ? $_POST['regsal'] : '';
$regllegada = (isset($_POST['regllegada'])) ? $_POST['regllegada'] : '';
$transporte = (isset($_POST['transporte'])) ? $_POST['transporte'] : '';
$TrkID = (isset($_POST['TrkID'])) ? $_POST['TrkID'] : '';
$qty =(isset($_POST['qty'])) ? $_POST['qty'] : '';
$data = [];

$fecha_actual = date("d-m-Y");
$fecha = date("d/m/Y");
//VALIDAR SI EL TRUCK SALE DE ORIGEN O DE BODEGA


    $query = 'SELECT IsOrigin  FROM Region WHERE RegNam= "'.$regsalida.'"';
    $result = $conexion->prepare($query);
    $result->execute();
    $isgin = $result->fetch();
	$isgin = $isgin['IsOrigin'];

    if($isgin == 1){
        $query2 = "SELECT Gin FROM DOrds WHERE Dord = '$delibery'";
        $gin = $conexion->prepare($query2);
        $gin->execute();
        $gin = $gin->fetch();
        $gin = $gin['Gin'];

        $query = "SELECT GinName,Zone FROM Gines WHERE IDGin = '$gin'";
        $result = $conexion->prepare($query);
        $result->execute();
        $result = $result->fetch();

        $ginname = $result['GinName'];
        $zone = $result['Zone'];
    }else{ 
        $query = 'SELECT Zone From Region  WHERE RegNam= "'.$regsalida.'"';
        $result = $conexion->prepare($query);
        $result->execute();
        $result = $result->fetch();

        $ginname ="";
        $zone = $result['Zone'];    
    }

if ($regllegada == "CLIENTE"){
    $query3 = "SELECT InPlc FROM DOrds WHERE Dord = '$delibery'";
    $query3 = $conexion->prepare($query3);
    $query3->execute();
    $query3= $query3->fetch();
    $cli = $query3['InPlc'];


    $query3 = "SELECT Town FROM Clients WHERE CliID = '$cli'";
    $query3 = $conexion->prepare($query3);
    $query3->execute();
    $query3 = $query3->fetch();
    $city = $query3['Town']; 

}
else{
    $query3 = "SELECT Town FROM Region WHERE RegNam = '$regllegada'";
    $query3 = $conexion->prepare($query3);
    $query3->execute();
    $query3= $query3->fetch();
    $city = $query3['Town'];  
}

//Obtener ID DEL TRANSPORT

    $query5 = "  SELECT IFNULL(( SELECT TptID  FROM Transports WHERE BnName = '$transporte' ) , 'N/A') as trasportid";
    $query5 = $conexion->prepare($query5);
    $query5->execute();
    $query5= $query5->fetch();
    $idtransport = $query5['trasportid'];  
//Obtener Costo promedio de paca 


    $query6 = "SELECT FreightCost,FreighCostNote,Samples FROM Truks WHERE TrkID  = '$TrkID'";
    $query6 = $conexion->prepare($query6);
    $query6->execute();
    $query6= $query6->fetch(); 
    $TotalCost = $query6['FreightCost']; 
    //$AverageCost= $query6['AverageCost']; 
    $FreighCostNote=$query6['FreighCostNote']; 
    $samples=$query6['Samples']; 


    $query7 = " SELECT IFNULL(( SELECT MAX(AverageCost) FROM amsadb1.Rates WHERE Zone= '$zone' AND  City= '$city' AND TransportID = '$idtransport') , 'N/A') as AverageCost";
    $query7 = $conexion->prepare($query7);
    $query7->execute();
    $query7= $query7->fetch(); 
    $costo = $query7['AverageCost']; 

    $query8 = " SELECT IFNULL((SELECT EndDate FROM amsadb1.Rates WHERE Zone= '$zone' AND  City= '$city' AND TransportID = '$idtransport' ORDER BY EndDate DESC LIMIT 1  ) , 'N/A') as Fechafin";
    $query8 = $conexion->prepare($query8);
    $query8->execute();
    $query8= $query8->fetch(); 
    $fechafin =$query8['Fechafin']; 
    $estado = "activo";
    if ($fechafin !== 'N/A') {
        $fechafin = str_replace("/", "-", $fechafin);
        if (strtotime($fechafin) <= strtotime($fecha_actual)) {
            $estado = "inactivo";
        }
    }

    if($FreighCostNote =="" &&  $costo == "N/A" && $regsalida != "CLIENTE" ){

        $FreighCostNote="SIN TARIFA AUTORIZADA";
    }

    $costo  = floatval($costo);
    $costo = bcdiv($costo, '1', 2);

$pacas=$qty;
if ($costo != "N/A" ){
    if($samples == 0 && $pacas>0){
    $costo_total = ($costo) * ($pacas);
    $costo_total = bcdiv($costo_total, '1', 2);
    $AverageCost = intval($TotalCost) / intval($pacas);
    $AverageCost = bcdiv($AverageCost, '1', 2);
    }
    else
    {
        $costo_total = ($costo) * (120);
        //DAR FORMATO DE 2 DIGITOS DEPUES DEL PUNTO DECINAL 
        $costo_total = bcdiv($costo_total, '1', 2);
        $AverageCost = $costo_total / 120;
        $AverageCost = bcdiv( $AverageCost, '1', 2);
    }
}
else{
    $costo = 0;
    $costo_total=0;
}

$data= [
    'NameGine' => $ginname,
    'Zone' => $zone,
    'city' => $city,
    'costo' => $costo,
    'costos_total'=> $costo_total,
    'TotalCost' => $TotalCost,
    'AverageCost'=> $AverageCost,
    'FreighCostNote'=> $FreighCostNote,
    'estado'=> $estado,
    'fechafin'=> $fechafin,

];

print json_encode($data, JSON_UNESCAPED_UNICODE); //envio el array final el formato json a AJAX
$conexion=null;


?>