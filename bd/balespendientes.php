<?php
include_once ('conexion.php');
$objeto = new Conexion();
$conexion = $objeto->Conectar();

$lista = (!empty($_GET['lista'])) ? $_GET['lista'] : '';
$crop = (!empty($_GET['crop'])) ? $_GET['crop'] : '';
$gin = (!empty($_GET['gin'])) ? $_GET['gin'] : '';


require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill, Alignment};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//use PhpOffice\PhpSpreadsheet\Style\Alignment as alignment;
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
$heading = false;

$fileName = "bales_pendientes".".xlsx";

//SE CREA Y SE EMPIEZA A LLENAR LA HOJA
$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Bales");

//negritas en encabezado

$hojaActiva->getStyle('A1:C1')->getFont()->setBold( true );

//relleno de celda encabezado
$hojaActiva->getStyle('A1:C1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFE1E1E1');
//fijar primera fila
$hojaActiva->freezePane('A2');

//centrar celdas  
//$hojaActiva->getStyle('A1:C1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

$hojaActiva->getColumnDimension('A')->setWidth(12);
$hojaActiva->setCellValue('A1','Crop');
$hojaActiva->getColumnDimension('B')->setWidth(12);
$hojaActiva->setCellValue('B1','GinID');
$hojaActiva->getColumnDimension('C')->setWidth(12);
$hojaActiva->setCellValue('C1','Bal');


//llenar hoja con datos de bales

$arreglo = explode(",", $lista);
$tam = count($arreglo);
$fila = 2;
for($i=0; $i<$tam; $i++){
   
    $hojaActiva->setCellValue('A' . $fila,$crop);
    $hojaActiva->setCellValue('B' . $fila,$gin);
    $hojaActiva->setCellValue('C' . $fila,$arreglo[$i]);
    $fila++;   
}

//centrar celdas  
$hojaActiva->getStyle('A:C')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');
ob_end_clean();
$writer = IOFactory::createWriter($excel, 'Xlsx');
$writer->save('php://output');
exit;
?>