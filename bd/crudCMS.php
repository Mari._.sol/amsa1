<?php

ini_set('memory_limit', '512M');
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

function lowercase($element)
{
    return "'".$element."'";
}

$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$Region = (isset($_POST['Region'])) ? $_POST['Region'] : '';
$SBGin = (isset($_POST['SBGin'])) ? $_POST['SBGin'] : '';
$SBBale = (isset($_POST['SBBale'])) ? $_POST['SBBale'] : '';
$SBCrp = (isset($_POST['SBCrp'])) ? $_POST['SBCrp'] : '';

//Variables filtros
session_start();
$filterSes = $_SESSION['Filters'];

$Lot = $filterSes['LotCMS'];
$DO = $filterSes['DOCMS'];


switch($opcion){
    case 4:
        if($DO == "" && $Lot == ""){
            $consulta = 'SELECT * FROM Bales ORDER BY Bal DESC LIMIT 0;';
            $data = 0;

        }elseif($DO != "" && $Lot == ""){
            $i = 0;
            $consulta = 'SELECT distinct Bales.Lot FROM Bales, DOrds, Lots WHERE DOrds.DOrd = '.$DO.' and DOrds.DOrd = Lots.DOrd and Bales.Lot = Lots.Lot;'; 
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $dataDO=$resultado->fetchALL(PDO::FETCH_ASSOC);
            foreach($dataDO as $dat){
                $Lotes[$i] = $dat['Lot'];
                $i++;
            }

            $array=array_map('lowercase', $Lotes);
            $array = implode(",",$array);

            $consulta = 'SELECT Crp, Gines.GinName, Lot, count(Bal) as Bal, CEILING(SUM(Wgh)) as Wgh, 
            (Sum(if(LiqID!="",1,0))) as LiqBal,
            if(count(Bal) != (Sum(if(LiqID!="",1,0))),"Notificar este lote antes de subir a CMS","") as Note
            FROM Bales
            join Gines on Gines.IDGin = Bales.GinID
            WHERE Bales.Lot IN ('.$array.') Group by Lot;';
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);

        }elseif($DO == "" && $Lot != ""){
            $array=array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, Gines.GinName, Lot, count(Bal) as Bal, CEILING(SUM(Wgh)) as Wgh, 
            (Sum(if(LiqID!="",1,0))) as LiqBal,
            if(count(Bal) != (Sum(if(LiqID!="",1,0))),"Notificar este lote antes de subir a CMS","") as Note
            FROM Bales
            join Gines on Gines.IDGin = Bales.GinID
            WHERE Bales.Lot IN ('.$array.') Group by Lot;';
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }
    break;
    case 5:
        $consulta = "SELECT IDGin, GinName FROM Gines WHERE IDReg = '$Region' ORDER BY GinName;"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
    case 6:
        $array=array_map('lowercase', explode(',', $SBBale));
        $array = implode(",",$array);
        
        if ($Region != "" && $SBGin == ""){    
            $consulta = 'SELECT DISTINCT Crp,
            CASE 
                WHEN Supplier.SupID IS NOT NULL THEN Supplier.SupName
                ELSE Bales.SupID
            END AS SupName, Region.RegNam, Gines.GinName, Bal, Wgh, Bales.Lot, BuyIt, Lots.DOrd, Bales.LiqID, Bales.Grp
            FROM Bales
            JOIN Gines ON Gines.IDGin = Bales.GinID
            LEFT JOIN Supplier ON Supplier.SupID = Bales.SupID
            JOIN Region ON Region.IDReg LIKE LEFT(Bales.GinID, 3)
            LEFT JOIN Lots ON Lots.Lot = Bales.Lot AND Lots.Location LIKE LEFT(Bales.GinID, 3)
            Where Bales.Bal in ('.$array.') and Bales.Crp = '.$SBCrp.' and Bales.GinID LIKE "%'.$Region.'%";';
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }else if ($Region != "" && $SBGin != ""){    
            $consulta = 'SELECT DISTINCT Crp,
            CASE 
                WHEN Supplier.SupID IS NOT NULL THEN Supplier.SupName
                ELSE Bales.SupID
            END AS SupName, Region.RegNam, Gines.GinName, Bal, Wgh, Bales.Lot, BuyIt, Lots.DOrd, Bales.LiqID, Bales.Grp
            FROM Bales
            JOIN Gines ON Gines.IDGin = Bales.GinID
            LEFT JOIN Supplier ON Supplier.SupID = Bales.SupID
            JOIN Region ON Region.IDReg LIKE LEFT(Bales.GinID, 3)
            LEFT JOIN Lots ON Lots.Lot = Bales.Lot AND Lots.Location LIKE LEFT(Bales.GinID, 3)
            Where Bales.Bal in ('.$array.') and Bales.Crp = '.$SBCrp.' and Bales.GinID = '.$SBGin.';';
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }else if ($Region == "" && $SBGin == ""){
            $consulta = 'SELECT DISTINCT Crp,
            CASE 
                WHEN Supplier.SupID IS NOT NULL THEN Supplier.SupName
                ELSE Bales.SupID
            END AS SupName, Region.RegNam, Gines.GinName, Bal, Wgh, Bales.Lot, BuyIt, Lots.DOrd, Bales.LiqID, Bales.Grp
            FROM Bales
            JOIN Gines ON Gines.IDGin = Bales.GinID
            LEFT JOIN Supplier ON Supplier.SupID = Bales.SupID
            JOIN Region ON Region.IDReg LIKE LEFT(Bales.GinID, 3)
            LEFT JOIN Lots ON Lots.Lot = Bales.Lot AND Lots.Location LIKE LEFT(Bales.GinID, 3)
            Where Bales.Bal in ('.$array.') and Bales.Crp = '.$SBCrp.';';
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }
    break;
    case 7:
        $consulta = "SELECT IDReg, RegNam  FROM Region WHERE IsOrigin =1; "; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
}

print json_encode($data, JSON_UNESCAPED_UNICODE);
$conexion=null;