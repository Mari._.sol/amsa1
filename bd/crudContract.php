<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

$IdContract = (isset($_POST['IdContract'])) ? $_POST['IdContract'] : '';
$solicita = (isset($_POST['solicita'])) ? $_POST['solicita'] : '';
$fecha = (isset($_POST['fecha'])) ? $_POST['fecha'] : '';
$cliente = (isset($_POST['cliente'])) ? $_POST['cliente'] : '';
$subCliente = (isset($_POST['subCliente'])) ? $_POST['subCliente'] : '';
$lugarEntrega = (isset($_POST['lugarEntrega'])) ? $_POST['lugarEntrega'] : '';
$de = (isset($_POST['de'])) ? $_POST['de'] : '';
$al = (isset($_POST['al'])) ? $_POST['al'] : '';
$incoterm = (isset($_POST['incoterm'])) ? $_POST['incoterm'] : '';
$cantidad = (isset($_POST['cantidad'])) ? $_POST['cantidad'] : '';
$cosecha = (isset($_POST['cosecha'])) ? $_POST['cosecha'] : '';
$mesCobertura = (isset($_POST['mesCobertura'])) ? $_POST['mesCobertura'] : '';
$region = (isset($_POST['region'])) ? $_POST['region'] : '';
$destino = (isset($_POST['destino'])) ? $_POST['destino'] : '';
$grado = (isset($_POST['grado'])) ? $_POST['grado'] : '';
$fibra = (isset($_POST['fibra'])) ? $_POST['fibra'] : '';
$micronara = (isset($_POST['micronara'])) ? $_POST['micronara'] : '';
$resistencia = (isset($_POST['resistencia'])) ? $_POST['resistencia'] : '';
$uniformidad = (isset($_POST['uniformidad'])) ? $_POST['uniformidad'] : '';
$fijoCall = (isset($_POST['fijoCall'])) ? $_POST['fijoCall'] : '';
$precioNY = (isset($_POST['precioNY'])) ? $_POST['precioNY'] : '';
$puntos = (isset($_POST['puntos'])) ? $_POST['puntos'] : '';
$precioFinal = (isset($_POST['precioFinal'])) ? $_POST['precioFinal'] : '';
$uMedida = (isset($_POST['uMedida'])) ? $_POST['uMedida'] : '';
$condPago = (isset($_POST['condPago'])) ? $_POST['condPago'] : '';
$intNormal = (isset($_POST['intNormal'])) ? $_POST['intNormal'] : '';
$intMoral = (isset($_POST['intMoral'])) ? $_POST['intMoral'] : '';
$contStatus = (isset($_POST['contStatus'])) ? $_POST['contStatus'] : '';
$comentario = (isset($_POST['comentario'])) ? $_POST['comentario'] : '';
$nota = (isset($_POST['nota'])) ? $_POST['nota'] : '';
$aprueba = (isset($_POST['aprueba'])) ? $_POST['aprueba'] : '';


$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';

switch($opcion){
    case 0:
        $consulta = "INSERT INTO amsadb1.Contract (IdContract, solicita, fecha, cliente, subCliente, lugarEntrega, 
        de, al, incoterm, cantidad, cosecha, mesCobertura, region, destino, grado, fibra, micronara, resistencia, 
        uniformidad, fijoCall, precioNY, puntos, precioFinal, uMedida, condPago, intNormal, intMoral, contStatus,
        comentario, nota, aprueba) 
        VALUES('$IdContract', '$solicita', '$fecha', '$cliente', '$subCliente', '$lugarEntrega', 
        '$de', '$al', '$incoterm', '$cantidad', '$cosecha', '$mesCobertura', '$region', '$destino', '$grado', '$fibra', '$micronara', '$resistencia', 
        '$uniformidad','$fijoCall', '$precioNY', '$puntos', '$precioFinal', '$uMedida', '$condPago', '$intNormal', '$intMoral', '$contStatus', 
        '$comentario', '$nota', '$aprueba') ";			
        $resultado = $conexion->prepare($consulta);
        $resultado->execute(); 
        
        $consulta = "SELECT * FROM amsadb1.Contract";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
    case 1:
        $consulta = "SELECT Cli FROM amsadb1.Clients;";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
    case 2:
        $consulta = "SELECT * FROM amsadb1.Inquiries_Covmon WHERE Available = '1';";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break;
    case 3:
        $consulta = "SELECT MAX(IdContract+ 1) as sigID FROM amsadb1.Contract;";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
    case 4:    
        $consulta = "SELECT * FROM amsadb1.Contract;";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
}
print json_encode($data, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX
$conexion=null;