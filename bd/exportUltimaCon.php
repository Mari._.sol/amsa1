<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set("America/Mexico_City");

require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill};
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';


$consulta = "SELECT
    p.ProveedID,
    DATE_FORMAT(STR_TO_DATE(p.Last_Login, '%d-%m-%Y'), '%d-%b-%y') AS Ultima_Conexion,
    CASE
        WHEN p.ProveedID = 'AGATE' THEN (
            SELECT DATE_FORMAT(
                STR_TO_DATE(
                    (SELECT MAX(t1.OutDat)
                     FROM amsadb1.DOrds d
                     LEFT JOIN amsadb1.Truks t1 ON d.DOrd = t1.DO AND (t1.Status = 'Transit' OR t1.Status = 'Received')
                     WHERE d.Gin IN (p.GinID, 65301) AND t1.CrgQty > 0
                    ), '%Y-%m-%d'
                ), '%d-%b-%y'
            )
        )
        ELSE DATE_FORMAT(
            STR_TO_DATE(
                (SELECT MAX(t1.OutDat)
                 FROM amsadb1.DOrds d
                 LEFT JOIN amsadb1.Truks t1 ON d.DOrd = t1.DO AND (t1.Status = 'Transit' OR t1.Status = 'Received')
                 LEFT JOIN amsadb1.Region r ON r.IDReg = d.OutPlc
                 WHERE d.Gin = p.GinID AND r.IsOrigin = 1
                ), '%Y-%m-%d'
            ), '%d-%b-%y'
        )
    END AS Salida_Ultimo_truk,
    CASE
        WHEN p.ProveedID = 'AGATE' THEN (
            SELECT MAX(t1.trkID)
            FROM amsadb1.DOrds d
            LEFT JOIN amsadb1.Truks t1 ON d.DOrd = t1.DO AND (t1.Status = 'Transit' OR t1.Status = 'Received')
            WHERE d.Gin IN (p.GinID, 65301) AND t1.CrgQty > 0
        )
        ELSE (
            SELECT MAX(t1.trkID)
            FROM amsadb1.DOrds d
            LEFT JOIN amsadb1.Truks t1 ON d.DOrd = t1.DO AND (t1.Status = 'Transit' OR t1.Status = 'Received')
            LEFT JOIN amsadb1.Region r ON r.IDReg = d.OutPlc
            WHERE d.Gin = p.GinID AND r.IsOrigin = 1
        )
    END AS TrukId_ul,
    CASE
        WHEN p.ProveedID = 'AGATE' THEN (
            SELECT DATE_FORMAT(STR_TO_DATE(
                (SELECT MAX(t2.OutDat)
                 FROM amsadb1.DOrds d
                 LEFT JOIN amsadb1.Truks t2 ON d.DOrd = t2.DO AND t2.Status = 'Programmed'
                 WHERE d.Gin IN (p.GinID, 65301) AND t2.CrgQty > 0
                ), '%Y-%m-%d'
            ), '%d-%b-%y')
        )
        ELSE DATE_FORMAT(STR_TO_DATE(
            (SELECT MAX(t2.OutDat)
             FROM amsadb1.DOrds d
             LEFT JOIN amsadb1.Truks t2 ON d.DOrd = t2.DO AND t2.Status = 'Programmed'
             LEFT JOIN amsadb1.Region r ON r.IDReg = d.OutPlc
             WHERE r.IsOrigin = 1 AND t2.CrgQty > 0 AND d.Gin = p.GinID
            ), '%Y-%m-%d'
        ), '%d-%b-%y')
    END AS Siguiente_salida,
    CASE
        WHEN p.ProveedID = 'AGATE' THEN (
            SELECT MAX(t2.trkID)
            FROM amsadb1.DOrds d
            LEFT JOIN amsadb1.Truks t2 ON d.DOrd = t2.DO AND t2.Status = 'Programmed'
            WHERE d.Gin IN (p.GinID, 65301) AND t2.CrgQty > 0
        )
        ELSE (
            SELECT MAX(t2.trkID)
            FROM amsadb1.DOrds d
            LEFT JOIN amsadb1.Truks t2 ON d.DOrd = t2.DO AND t2.Status = 'Programmed'
            LEFT JOIN amsadb1.Region r ON r.IDReg = d.OutPlc
            WHERE r.IsOrigin = 1 AND d.Gin = p.GinID AND t2.CrgQty > 0
        )
    END AS TrkID_siguiente
FROM amsadb1.proveed p
WHERE p.RepStatus = 1;";

$resultado = $conexion->prepare($consulta);
$resultado->execute();  
$data = $resultado->fetchAll(PDO::FETCH_ASSOC);

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$boldStyle = [
    'font' => [
        'bold' => true,
    ],
];

$sheet->getStyle('A1:G1')->applyFromArray($boldStyle); 

$sheet->getColumnDimension('A')->setWidth(22);
$sheet->getColumnDimension('B')->setWidth(18);
$sheet->getColumnDimension('C')->setWidth(20);
$sheet->getColumnDimension('D')->setWidth(15);
$sheet->getColumnDimension('E')->setWidth(20);
$sheet->getColumnDimension('F')->setWidth(15);
$sheet->getColumnDimension('G')->setWidth(20);

$columnas = ['Proveedor', 'Última conexión', 'Salida Último Truck', 'TrkID', 'Salida siguiente truck', 'TrkID', 'Salida último trk - Dias ultima conexión'];
$sheet->fromArray($columnas, NULL, 'A1');

foreach ($data as &$row) {
    if (!empty($row['Salida_Ultimo_truk']) && !empty($row['Ultima_Conexion'])) {
        $fechaSalida = new DateTime($row['Salida_Ultimo_truk']);
        $fechaConexion = new DateTime($row['Ultima_Conexion']);
        $diferenciaDias = $fechaSalida->diff($fechaConexion)->days;
        if ($fechaSalida < $fechaConexion) {
            $diferenciaDias = -$diferenciaDias;
        }

        if ($diferenciaDias > 0) {
            $row['Salida último trk - Dias ultima conexión'] = $diferenciaDias;
        } else {
            $row['Salida último trk - Dias ultima conexión'] = '0';
        }
    } else {
        $row['Salida último trk - Dias ultima conexión'] = '0'; // Establecer a 0 si no hay fechas
    }
}

function ordenarDias($a, $b) {
    if ($a['Salida último trk - Dias ultima conexión'] == $b['Salida último trk - Dias ultima conexión']) {
        return 0;
    }
    return ($a['Salida último trk - Dias ultima conexión'] > $b['Salida último trk - Dias ultima conexión']) ? -1 : 1;
}

usort($data, 'ordenarDias');

$mapeoNombres = [
    'AGROBA' => 'AGRO BA',
    'BUENAVISTA' => 'CAU',
    'BUENOSAIRES' => 'BUENOS AIRES',
    'FLORMARZO' => 'FLOR DE MARZO',
    'GOODLUCK' => 'GOOD LUCK',
    'NUEVAH1' => 'NUEVA HOLANDA 1',
    'NUEVAH2' => 'NUEVA HOLANDA 2',
    'SAUTEÃ‘A' => 'SAUTEÑA',
    'VADOSTAMARIA' => 'VADO',
    'VILLAFUERTE' => 'VILLA FUERTE',
];

// Renombra la columna de proveedores
foreach ($data as &$row) {
    if (isset($mapeoNombres[$row['ProveedID']])) {
        $row['ProveedID'] = $mapeoNombres[$row['ProveedID']];
    }
}

$sheet->fromArray($data, NULL, 'A2');

//Convertir las fechas a formato fecha 
$columnasFecha = ['B', 'C', 'E'];
foreach ($columnasFecha as $columna) {
    $sheet->getStyle($columna . '2:' . $columna . count($data))->getNumberFormat()->setFormatCode('dd-mmm-yy'); 
} 
$greenFill = [
    'fill' => [
        'fillType' => Fill::FILL_SOLID,
        'startColor' => ['rgb' => 'E2EFDA'],
    ],
];
$yellowFill = [
    'fill' => [
        'fillType' => Fill::FILL_SOLID,
        'startColor' => ['rgb' => 'FFF2CC'],
    ],
];
$redFill = [
    'fill' => [
        'fillType' => Fill::FILL_SOLID,
        'startColor' => ['rgb' => 'FCE4D6'],
    ],
];
foreach ($data as $rowIndex => $rowData) {
    $dias = $rowData['Salida último trk - Dias ultima conexión'];
    if ($dias >= 0 && $dias <= 7) {
        $sheet->getStyle('A' . ($rowIndex + 2) . ':G' . ($rowIndex + 2))->applyFromArray($greenFill);
    } elseif ($dias >= 8 && $dias <= 30) {
        $sheet->getStyle('A' . ($rowIndex + 2) . ':G' . ($rowIndex + 2))->applyFromArray($yellowFill);
    } elseif ($dias > 30) {
        $sheet->getStyle('A' . ($rowIndex + 2) . ':G' . ($rowIndex + 2))->applyFromArray($redFill);
    }
}

$columnasDerecha = ['B', 'C', 'D', 'E', 'F', 'G'];
foreach ($columnasDerecha as $columna) {
    $sheet->getStyle($columna . '2:' . $columna . count($data))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
}

$writer = new Xlsx($spreadsheet);
$filename = 'Reporte proveedores '.date('d-m-Y').'.xlsx';
$writer->save($filename);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
ob_end_clean();
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;
?>

