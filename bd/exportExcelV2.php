<?php

include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set("America/Mexico_City");


// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

//Define the filename with current date
//$fileName = "Requisition-".date('d-m-Y').".xls";

//Set header information to export data in excel format
//header('Content-Type: application/vnd.ms-excel');
//header('Content-Disposition: attachment; filename='.$fileName);

//Set variable to false for heading
$heading = false;

$consulta = "Select Truks.TrkID, WBill,FreighCostNote,	
concat(concat((IF((Select IsOrigin From Region Where DOrds.OutPlc = IDReg)=1,(Select Cde from Gines where Truks.TrkID = Lots.TrkID and Lots.GinID = Gines.IDGin),(Select Cde from Region where Truks.TrkID = Lots.TrkID and DOrds.OutPlc = Region.IDReg))),'-',
(IF((Select DOrds.Typ From DOrds Where Truks.DO = DOrds.DOrd)='CON',(Select Cde From Region Where DOrds.InReg = Region.IDReg),(Select Cde From Clients Where DOrds.InPlc = Clients.CliID)))),'/',Truks.DO) as DO, Lots.Lot, Lots.Qty, (Select BnName From Transports Where Truks.TNam = Transports.TptID) as Transport, ROUND((Truks.FreightCost/Truks.CrgQty*Lots.Qty),2) as Cost, 
ROUND((Truks.FreightCost/Truks.CrgQty),2) as CostBale,RqstID as PR, PO
From Truks, Lots, DOrds
Where Truks.DO = DOrds.DOrd and Truks.TrkID = Lots.TrkID and RqstID<>0 and PO =0 ORDER BY Transport ASC, TrkID ASC;";
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$siexiste=0; //$data=$resultado->fetchAll(PDO::FETCH_ASSOC);
$i=0;
$datatrkid=array();
$informacion =[];
$aux = [];
foreach($resultado as $linea){
    $datatrkid[$i]=$linea['TrkID'];

    $aux=[
        'TrkID'=> $linea['TrkID'],
        'WBill' => $linea['WBill'],
        'FreighCostNote' => $linea['FreighCostNote'],
        'DO' => $linea['DO'],
        'Lot' => $linea['Lot'],
        'Qty' => $linea['Qty'],
        'Transport' => $linea['Transport'],
        'Cost' => $linea['Cost'],
        'PR' => $linea['PR'],
        'PO' => $linea['PO'],
        'CostBale' => $linea['CostBale']

    ];

    array_push($informacion,$aux);




    $i++;
}

//dejar trikid unicos
$datatrkid=array_unique($datatrkid);
 //limpiar las posisiones del array convirtirndo a string
 $datatrkid=implode(",",$datatrkid);
 //limpiar las posisiones del array regresarlo a array
 $datatrkid=explode(",", $datatrkid);
 $tam = sizeof($datatrkid);
 
 $trk=implode(",",$datatrkid);

 //print_r($datatrkid);


$i=0;
$total=0;
$costos = [];
$aux=[];
$aux2=[];
$trkcosto=[];



$resultado = $conexion->prepare($consulta);
$resultado->execute();     


foreach($resultado as $row2){
        $aux2 = [
        'TrkID' => $row2['TrkID'],
        'Cost' => $row2['Cost'],      
         ];

         array_push($trkcosto,$aux2);
}



 for($j=0;$j<$tam;$j++){
        $total=0;    
        foreach($trkcosto as $row3){
            if($datatrkid[$j]==$row3['TrkID']){
                $total += $row3['Cost'];         
            }   
        }

        $aux = [
        'Truckid' => $datatrkid[$j],
        'costo' => $total,      
        ];
        
        array_push($costos,$aux);
 }


 //print_r($costos);

$consulta2="SELECT TrkID,FreightCost FROM Truks WHERE TrkID IN ($trk)";

$resultado2 = $conexion->prepare($consulta2);
$resultado2->execute(); 
$FreightCost=$resultado2->fetchAll();
////print_r($FreightCost);


$tam2 = sizeof($costos);
$aux4=[];
$diferencias=[];

foreach($costos as $fila){
    $diferencia = 0;

    foreach($FreightCost as $row4){
        if($fila['Truckid'] == $row4['TrkID']){
            $diferencia=$row4['FreightCost']-$fila['costo'];
        }
    }   

    if($diferencia >-1 && $diferencia < 1 && $diferencia !=0){
    $aux4 =[
        'Truck' => $fila['Truckid'],
        'Dif' => $diferencia,
    ];
    
    array_push($diferencias,$aux4);
    }


}

$cont=sizeof($diferencias);
$continfo=sizeof($informacion);

//print_r($diferencias);
echo("<br>");
$k=0;
if($cont > 0){
  
    foreach($diferencias as $dif){
        
        while ($k < $continfo) {
            if($dif['Truck']==$informacion[$k]['TrkID']){
                $informacion[$k]['Cost']+=$dif['Dif'];
                break;
            }
            $k++;

        }
    }


}



//print_r($informacion);




//Define the filename with current date
$fileName = "Requisition-".date('d-m-Y').".xlsx";

$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Requisition");
//$hojaActiva->freezePane("A2");


$hojaActiva->getColumnDimension('A')->setWidth(10);
$hojaActiva->setCellValue('A1','TrkID');
$hojaActiva->getColumnDimension('B')->setWidth(20);
$hojaActiva->setCellValue('B1','DO');
$hojaActiva->getColumnDimension('C')->setWidth(10);
$hojaActiva->setCellValue('C1','Lot');
$hojaActiva->getColumnDimension('D')->setWidth(10);
$hojaActiva->setCellValue('D1','Waybill');
$hojaActiva->getColumnDimension('E')->setWidth(10);
$hojaActiva->setCellValue('E1','Qty');
$hojaActiva->getColumnDimension('F')->setWidth(55);
$hojaActiva->setCellValue('F1','Transport');
$hojaActiva->getColumnDimension('G')->setWidth(12);
$hojaActiva->setCellValue('G1','Cost');
$hojaActiva->getColumnDimension('H')->setWidth(10);
$hojaActiva->setCellValue('H1','Cost Bale');
$hojaActiva->getColumnDimension('I')->setWidth(10);
$hojaActiva->setCellValue('I1','PR');
$hojaActiva->getColumnDimension('J')->setWidth(10);
$hojaActiva->setCellValue('J1','PO');
$hojaActiva->getColumnDimension('K')->setWidth(50);
$hojaActiva->setCellValue('K1','Freight Cost Note');

$fila1 = 2;
$j=0;

while($j<$continfo){
    if($siexiste==0){
        if(!empty($informacion[$j]['TrkID']))
            $siexiste=1;
    }
    
    $hojaActiva->setCellValue('A' . $fila1,$informacion[$j]['TrkID']);
    $hojaActiva->setCellValue('B' . $fila1,$informacion[$j]['DO']);
    $hojaActiva->setCellValue('C' . $fila1,$informacion[$j]['Lot']);
    $hojaActiva->setCellValue('D' . $fila1,$informacion[$j]['WBill']);
    $hojaActiva->setCellValue('E' . $fila1,$informacion[$j]['Qty']);
    $hojaActiva->setCellValue('F' . $fila1,$informacion[$j]['Transport']);
    $hojaActiva->setCellValue('G' . $fila1,$informacion[$j]['Cost']);
    $hojaActiva->setCellValue('H' . $fila1,$informacion[$j]['CostBale']);
    $hojaActiva->setCellValue('I' . $fila1,$informacion[$j]['PR']);
    $hojaActiva->setCellValue('J' . $fila1,$informacion[$j]['PO']);
    $hojaActiva->setCellValue('K' . $fila1,$informacion[$j]['FreighCostNote']);
   
    $fila1++;
    $j++;
}

$hojaActiva->getStyle('G2:G'.$fila1)->getNumberFormat()->setFormatCode('###0.00');


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');
ob_end_clean();
$writer = IOFactory::createWriter($excel, 'Xlsx');
$writer->save('php://output');
exit;


?>




<!--
Select Truks.TrkID, Truks.DO as DO, Lots.Lot, Lots.Qty, (Select BnName From Transports Where Truks.TNam = Transports.TptID) as Transport, (Truks.FreightCost/Truks.CrgQty*Lots.Qty) as Cost, RqstID as PR, PO
From Truks, Lots
Where Truks.TrkID = Lots.TrkID and RqstID<>0 and PO =0 ORDER BY TrkID;
-->
