<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

//Obtener la hora centro Mexico
$hc = 'America/Mexico_City';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($hc));
//$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
$dt = $dt->format('H:i:s');

$DOrd = (isset($_POST['DOrd'])) ? $_POST['DOrd'] : '';
$Qty = (isset($_POST['Qty'])) ? $_POST['Qty'] : '';
$Rqs = (isset($_POST['Rqs'])) ? $_POST['Rqs'] : '';
$Dat = (isset($_POST['Dat'])) ? $_POST['Dat'] : '';
$Year = (isset($_POST['Year'])) ? $_POST['Year'] : '';
$CrpDO = (isset($_POST['CrpDO'])) ? $_POST['CrpDO'] : '';
$Typ = (isset($_POST['Typ'])) ? $_POST['Typ'] : '';
$OutPlc = (isset($_POST['OutPlc'])) ? $_POST['OutPlc'] : '';
$Gin = (isset($_POST['Gin'])) ? $_POST['Gin'] : '';
$InReg = (isset($_POST['InReg'])) ? $_POST['InReg'] : '';
$InPlc = (isset($_POST['InPlc'])) ? $_POST['InPlc'] : '';
$Ctc = (isset($_POST['Ctc'])) ? $_POST['Ctc'] : '';
$DoCmt = (isset($_POST['DoCmt'])) ? $_POST['DoCmt'] : '';
$Cons = (isset($_POST['Cons'])) ? $_POST['Cons'] : '';
$Crss = (isset($_POST['Crss'])) ? $_POST['Crss'] : '';
$PurNo = (isset($_POST['PurNo'])) ? $_POST['PurNo'] : '';
$PDF = (isset($_POST['PDF'])) ? $_POST['PDF'] : '';
$clientsDO = (isset($_POST['clientsDO'])) ? $_POST['clientsDO'] : '';
$TDO= (isset($_POST['TDO'])) ? $_POST['TDO'] : '';
$FDO= (isset($_POST['FDO'])) ? $_POST['FDO'] : '';
//Var para buscar lotes por region 
$OutReg = (isset($_POST['OutReg'])) ? $_POST['OutReg'] : '';
$Lotes = (isset($_POST['Lotes'])) ? $_POST['Lotes'] : ''; //Lotes a asignar DO
$selectText = (isset($_POST['selectText'])) ? $_POST['selectText'] : ''; //Lotes txt
$SchDate = (isset($_POST['SchDate'])) ? $_POST['SchDate'] : '';
$OutGin = (isset($_POST['OutGin'])) ? $_POST['OutGin'] : '';
$CrpDO = (isset($_POST['CrpDO'])) ? $_POST['CrpDO'] : '';
$certificado = (isset($_POST['cert'])) ? $_POST['cert'] : ''; //validar si lleva certificado origen
$pacascert = (isset($_POST['pacascert'])) ? $_POST['pacascert'] : ''; //validar si son pacas certificadas
$muestrasdo = (isset($_POST['muestrasdo'])) ? $_POST['muestrasdo'] : ''; //validar si son muestras
$lotscert = (isset($_POST['lotscert'])) ? $_POST['lotscert'] : ''; //validar si se requieren lotes certificados o no



$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
//$LotID = (isset($_POST['LotID'])) ? $_POST['LotID'] : '';

//// Variables para filtros
session_start();
$filterSes = $_SESSION['Filters'];

$muestras = $filterSes['muestras'];
if($muestras == ""){
    $muestras = 200;
}
//$clientsDO = $filterSes['clientsDO'];
$DepRegfil = ($filterSes['DepRegFil']);
$ArrRegfil = ($filterSes['ArrRegFil']);
//$TDO = $filterSes['typeDO'];
$fromDate = $filterSes['fromdate'];
$toDate = $filterSes['todate'];
//$FDO = $filterSes['FDO'];
$data = array();
$consulta = "";
$regions =0;
$numMues =0;
//complemento de busqueda de lotes certificados
$complemento= ' AND Lots.Cert = 0';

if($lotscert=='YES'){
    $complemento= ' AND Lots.Cert = 1 ';
}
else {
    $complemento= ' AND Lots.Cert = 0 ';
}

switch($opcion){
    case 1:
	$consulta = "SELECT IDReg FROM Region WHERE RegNam='$OutPlc'";
	$resultado = $conexion->prepare($consulta);
	$resultado->execute();
	$dataOutPlc = $resultado->fetch();
	$OutPlc = $dataOutPlc['IDReg'];
    if ($OutPlc != '99002'){
        $Crss = "";
    }    

	$consulta = "SELECT IDGin FROM Gines WHERE GinName='$Gin'";
	$resultado = $conexion->prepare($consulta);
	$resultado->execute();
	$dataGin = $resultado->fetch();
	$Gin = $dataGin['IDGin'];

	$consulta = "SELECT IDReg FROM Region WHERE RegNam='$InReg'";
	$resultado = $conexion->prepare($consulta);
	$resultado->execute();
	$dataInReg = $resultado->fetch();
	$InReg = $dataInReg['IDReg'];

	$consulta = "SELECT CliID FROM Clients WHERE Cli='$InPlc'";
	$resultado = $conexion->prepare($consulta);
	$resultado->execute();
	$dataInPlc = $resultado->fetch();
	$InPlc = $dataInPlc['CliID'];
        
        $consulta = "INSERT INTO DOrds (DOrd, Cons, Qty, Unt, Rqs, Date, Hour, Typ, OutPlc, Gin, InReg, InPlc, Ctc, DoCmt, Year, CrpDO, Crss, PurNo, PDF,Certificate,Cert,Samp) VALUES('$DOrd', '$Cons', '$Qty', '$Qty', '$Rqs', '$Dat', '$dt', '$Typ', '$OutPlc', '$Gin', '$InReg', '$InPlc', '$Ctc', '$DoCmt', '$Year', '$CrpDO', '$Crss', '$PurNo', '$PDF','$certificado','$pacascert','$muestrasdo')";			
        $resultado = $conexion->prepare($consulta);
        $resultado->execute(); 
        
        $consulta = "SELECT Year, DOrd, Qty, Rqs, Date, Typ, OutPlc, Gin, InReg, InPlc, Ctc, DoCmt,Certificate,Cert,Samp FROM DOrds ORDER BY Typ DESC LIMIT 1";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);       
        break;    
    case 2:
	$consulta = "SELECT IDReg FROM Region WHERE RegNam='$OutPlc'";
	$resultado = $conexion->prepare($consulta);
	$resultado->execute();
	$dataOutPlc = $resultado->fetch();
	$OutPlc = $dataOutPlc['IDReg'];

	$consulta = "SELECT IDGin FROM Gines WHERE GinName='$Gin'";
	$resultado = $conexion->prepare($consulta);
	$resultado->execute();
	$dataGin = $resultado->fetch();
	$Gin = $dataGin['IDGin'];

	$consulta = "SELECT IDReg FROM Region WHERE RegNam='$InReg'";
	$resultado = $conexion->prepare($consulta);
	$resultado->execute();
	$dataInReg = $resultado->fetch();
	$InReg = $dataInReg['IDReg'];

	$consulta = "SELECT CliID FROM Clients WHERE Cli='$InPlc'";
	$resultado = $conexion->prepare($consulta);
	$resultado->execute();
	$dataInPlc = $resultado->fetch();
	$InPlc = $dataInPlc['CliID'];
        
        $consulta = "UPDATE DOrds SET DOrd='$DOrd', Qty='$Qty', Unt='$Qty', Rqs='$Rqs', Date='$Dat', Typ='$Typ', OutPlc='$OutPlc', Gin='$Gin', InReg='$InReg', InPlc='$InPlc', Ctc='$Ctc', DoCmt='$DoCmt', Year='$Year', Crss='$Crss', PurNo='$PurNo', PDF='$PDF',Certificate ='$certificado', Cert = '$pacascert', Samp = '$muestrasdo' WHERE DOrd='$DOrd'";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        
        $consulta = "SELECT Year, DOrd, Qty, Rqs, Date, Typ, OutPlc, Gin, InReg, InPlc, Ctc, DoCmt, Certificate, Cert, Samp FROM DOrds WHERE DOrd='$DOrd' ";       
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 3:        
        $consulta = "UPDATE DOrds Set Qty='0', Unt='0' WHERE DOrd='$DOrd'"; //"DELETE FROM Lots WHERE LotID='$LotID' ";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();                           
        break;
    case 4:    
       //SI pide todo
       if($muestras != "" && $muestras == 'ALL' && $DepRegfil == "" && $ArrRegfil == "" && $fromDate =="" && $toDate =="" && $clientsDO == "" && $TDO == ""){
        $consulta = "SELECT D.Year, D.DOrd,  D.Qty as TTQty, D.Crss, D.PurNo, D.Cons, D.PDF, D.Assoc as Associated, D.Inv as Invoiced, D.Rcv as Received, D.Rej as Rejected, D.Tran as Embarked, D.Adj as Adjustments,D.Certificate,D.Cert,D.Samp,

        D.Qty - D.Assoc as ToAssociate,
        
        IFNULL( D.Tran, 0 ) as Transit,
        
        IFNULL( D.Rcv, 0 ) -  IFNULL( D.Rej, 0 ) -   IFNULL(D.Inv, 0 ) as ToInv,
        
        IFNULL( D.Qty, 0 ) -  IFNULL( D.Tran, 0 ) -  IFNULL( D.Rcv, 0 ) as ToDO,
        
        D.Rqs, D.Date, D.Typ, D.OutPlc, RO.RegNam as RegNameOut, D.Gin, G.GinName as GinName, D.InReg, RI.RegNam as RegNameIn, InPlc, C.Cli as PlcNameIn, Ctc, DoCmt 
        
        FROM DOrds D
      
        left join Region RO on D.OutPlc = RO.IDReg
        left join Region RI on D.InReg = RI.IDReg
        left JOIN  Gines G ON D.Gin = G.IDGin
        left JOIN Clients C ON D.InPlc = C.CliID;  ";
        
        /*"";
        
        SELECT D.Year, D.DOrd,  D.Qty, D.Crss, D.PurNo, D.Cons, D.PDF, D.Assoc as Associated, D.Inv as Invoiced, D.Rcv as Received, D.Rej as Rejected, D.DLVD as Embarked, D.Adj as Adjustments,(D.Qty - D.Adj) as TTQty,

        (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE DO=D.DOrd and Stg.TypeStg='Adjustment') + Qty - (SELECT IFNULL(SUM(Qty),0) FROM Lots WHERE Lots.DOrd=D.Dord) as ToAssociate,
        (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=D.Dord and Truks.Status='Transit') as Transit,    
        (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=D.DOrd and  Truks.Status='Received') - (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE DO=D.Dord and Stg.TypeStg='Rejected') - (SELECT IFNULL(SUM(Qty),0) FROM Invs WHERE DO=D.Dord and Invs.InvSta='Ok') as ToInv, 
        (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE DO=D.Dord and Stg.TypeStg='Adjustment') + Qty - (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=D.Dord and Truks.Status='Transit') - (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=D.Dord and  Truks.Status='Received') as ToDO, 
        
        D.Rqs, D.Date, D.Typ, D.OutPlc, RO.RegNam as RegNameOut, D.Gin, G.GinName as GinName, D.InReg, RI.RegNam as RegNameIn, InPlc, C.Cli as PlcNameIn, Ctc, DoCmt 
        
        FROM DOrds D
        
        
        
    
        left join Region RO on D.OutPlc = RO.IDReg
        left join Region RI on D.InReg = RI.IDReg
        left JOIN  Gines G ON D.Gin = G.IDGin
        left JOIN Clients C ON D.InPlc = C.CliID; 
        
        SELECT Year, DOrd, Qty, Crss, PurNo, Cons, PDF, Assoc as Associated, Inv as Invoiced, Rcv as Received, Rej as Rejected, DLVD as Embarked, Adj as Adjustments,(Qty - Adj) as TTQty,
        (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE DO=DOrd and Stg.TypeStg='Adjustment') + Qty - (SELECT IFNULL(SUM(Qty),0) FROM Lots WHERE Lots.DOrd=DOrds.Dord) as ToAssociate,
        (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=Dord and Truks.Status='Transit') as Transit,    
        (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=DOrd and  Truks.Status='Received') - (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE DO=Dord and Stg.TypeStg='Rejected') - (SELECT IFNULL(SUM(Qty),0) FROM Invs WHERE DO=Dord and Invs.InvSta='Ok') as ToInv, 
        (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE DO=Dord and Stg.TypeStg='Adjustment') + Qty - (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=Dord and Truks.Status='Transit') - (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=Dord and  Truks.Status='Received') as ToDO, 
        Rqs, Date, Typ, OutPlc,
        (SELECT RegNam FROM Region WHERE IDReg = OutPlc) as RegNameOut, Gin,
        (SELECT GinName FROM Gines WHERE IDGin = Gin) as GinName, InReg, 
        (SELECT RegNam FROM Region WHERE IDReg = InReg) as RegNameIn, InPlc, 
        (SELECT Cli FROM Clients WHERE CliID = InPlc) as PlcNameIn, Ctc, DoCmt FROM DOrds;
        */
    }elseif($FDO != "" && $muestras != "" && $muestras != 'ALL' && $DepRegfil == "" && $ArrRegfil == "" && $fromDate =="" && $toDate =="" && $clientsDO == "" && $TDO == "" ){
        //SOLO DO
        $consulta = "SELECT D.Year, D.DOrd,  D.Qty as TTQty, D.Crss, D.PurNo, D.Cons, D.PDF, D.Assoc as Associated, D.Inv as Invoiced, D.Rcv as Received, D.Rej as Rejected, D.Tran as Embarked, D.Adj as Adjustments,D.Certificate,D.Cert, D.Samp,

         D.Qty - D.Assoc as ToAssociate,
        
        IFNULL( D.Tran, 0 ) as Transit,
        
        IFNULL( D.Rcv, 0 ) -  IFNULL( D.Rej, 0 ) -   IFNULL(D.Inv, 0 ) as ToInv,
        
        IFNULL( D.Qty, 0 ) -  IFNULL( D.Tran, 0 ) -  IFNULL( D.Rcv, 0 ) as ToDO, 
        
        D.Rqs, D.Date, D.Typ, D.OutPlc, RO.RegNam as RegNameOut, D.Gin, G.GinName as GinName, D.InReg, RI.RegNam as RegNameIn, InPlc, C.Cli as PlcNameIn, Ctc, DoCmt 
        
        FROM DOrds D
      
        left join Region RO on D.OutPlc = RO.IDReg
        left join Region RI on D.InReg = RI.IDReg
        left JOIN  Gines G ON D.Gin = G.IDGin
        left JOIN Clients C ON D.InPlc = C.CliID
        WHERE DOrd = ".$FDO.";";
    }elseif($muestras != "" && $muestras != 'ALL' && $DepRegfil == "" && $ArrRegfil == "" && $fromDate =="" && $toDate =="" && $clientsDO == "" && $TDO == "" ){
       //SI solo pide muestras 
       
        $query = 'SELECT Cons FROM DOrds order by Cons DESC limit 1 ;';
        $result = $conexion->prepare($query);
        $result->execute();
        $firstID = $result->fetch();
        $firstID = $firstID['Cons'];
        $aux = $firstID;
        $i = 0;
        while($i < $muestras){
            $query = "SELECT Cons FROM DOrds where Cons = ".$aux.";";
            $result = $conexion->prepare($query);
            $result->execute();
            if($fila = $result->fetch(PDO::FETCH_ASSOC)){
               // echo $i." ";
                $i++;
                $last = $fila['Cons'];
                
            }
            $aux--;
            if($aux == 0){
                break;
            }
        }

       $RANGO = $last;

        $consulta = "SELECT D.Year, D.DOrd,  D.Qty as TTQty, D.Crss, D.PurNo, D.Cons, D.PDF, D.Assoc as Associated, D.Inv as Invoiced, D.Rcv as Received, D.Rej as Rejected, D.Tran as Embarked, D.Adj as Adjustments,D.Certificate,D.Cert,D.Samp,D.Hour,

         D.Qty - D.Assoc as ToAssociate,
        
        IFNULL( D.Tran, 0 ) as Transit,
        
        IFNULL( D.Rcv, 0 ) -  IFNULL( D.Rej, 0 ) -   IFNULL(D.Inv, 0 ) as ToInv,
        
        IFNULL( D.Qty, 0 ) -  IFNULL( D.Tran, 0 ) -  IFNULL( D.Rcv, 0 ) as ToDO, 
        
        D.Rqs, D.Date, D.Typ, D.OutPlc, RO.RegNam as RegNameOut, D.Gin, G.GinName as GinName, D.InReg, RI.RegNam as RegNameIn, InPlc, C.Cli as PlcNameIn, Ctc, DoCmt 
        
        FROM DOrds D
      
        left join Region RO on D.OutPlc = RO.IDReg
        left join Region RI on D.InReg = RI.IDReg
        left JOIN  Gines G ON D.Gin = G.IDGin
        left JOIN Clients C ON D.InPlc = C.CliID
        ORDER BY Date DESC, Hour DESC LIMIT ".$muestras.";";
        //where D.Cons <= " . $firstID . " AND D.Cons >= " . $RANGO." order by D.Cons DESC;";
    }elseif($TDO != "" || $DepRegfil != "" || $ArrRegfil != "" || $toDate != "" || $fromDate != "") {
        //Cuando pide algún tipo, con cliente o sin él, con fecha o sin fecha
        $consulta = "SELECT D.Year, D.DOrd,  D.Qty as TTQty, D.Crss, D.PurNo, D.Cons, D.PDF, D.Assoc as Associated, D.Inv as Invoiced, D.Rcv as Received, D.Rej as Rejected, D.Tran as Embarked, D.Adj as Adjustments,D.Certificate, D.Cert, D.Samp,

         D.Qty - D.Assoc as ToAssociate,
        
        IFNULL( D.Tran, 0 ) as Transit,
        
        IFNULL( D.Rcv, 0 ) -  IFNULL( D.Rej, 0 ) -   IFNULL(D.Inv, 0 ) as ToInv,
        
        IFNULL( D.Qty, 0 ) -  IFNULL( D.Tran, 0 ) -  IFNULL( D.Rcv, 0 ) as ToDO, 
        
        D.Rqs, D.Date, D.Typ, D.OutPlc, RO.RegNam as RegNameOut, D.Gin, G.GinName as GinName, D.InReg, RI.RegNam as RegNameIn, InPlc, C.Cli as PlcNameIn, Ctc, DoCmt 
        
        FROM DOrds D
      
        left join Region RO on D.OutPlc = RO.IDReg
        left join Region RI on D.InReg = RI.IDReg
        left JOIN  Gines G ON D.Gin = G.IDGin
        left JOIN Clients C ON D.InPlc = C.CliID
         where ";
         //Si tiene Type DO
         if($TDO != ""){
         $consul[] = "D.Typ = '".$TDO."' ";
         }
        /* if($TDO != "" && $DepRegfil != "" || $ArrRegfil){
            $consul[] = " AND ";
         } */
         if($DepRegfil!= ""){
            $reg = implode("', '",$DepRegfil);
            $query = "SELECT IDReg FROM Region WHERE RegNam IN ('".$reg."')";
          //   print_r($query);
            $result = $conexion->prepare($query);
            $result->execute();
            // print_r($result);
            $idRegion = $result->fetchAll(PDO::FETCH_COLUMN, 0);
           
            $idRegion = implode (",",$idRegion);   
                     
            $consul[] = " D.OutPlc IN (".$idRegion.") ";
         }
       /*  if($DepRegfil != "" && $ArrRegfil != ""){
            $consulta .= " AND ";
         } */
         if($ArrRegfil!= ""){
            $arr = implode("', '",$ArrRegfil);
            $query = "SELECT IDReg from Region WHERE RegNam IN ('".$arr."')";
           
            $result = $conexion->prepare($query);
            $result->execute();
            $idRegion = $result->fetchAll(PDO::FETCH_COLUMN, 0);
            $idRegion = implode (",",$idRegion);
            $consul[] = " D.InReg IN (".$idRegion.") ";
         }
        //Si Tiene cliente
        if($clientsDO != ""){
            $query = 'SELECT CliID from Clients where Cli = "'.$clientsDO.'" ';
            $result = $conexion->prepare($query);
            $result->execute();
            $idCliente = $result->fetch(PDO::FETCH_ASSOC);
            $consul[] = ' D.InPlc = '.$idCliente['CliID'].' ';
            //echo $clientsDO;
        }

    /*    if($clientsDO !="" && $fromDate || $toDate){
            $consulta .= " AND ";
        } */

        if($fromDate != "")
          $consul[] = ' D.Date >= "'.$fromDate.'" ';
      /*  if($fromDate != "" && $toDate != "")
            $consulta .= ' AND '; */
        if($toDate != "")
         $consul[] = ' D.Date <= "'.$toDate.'" ';
        
        // var_dump($consul); $i++;
        $consulta .= implode(' AND ', $consul);
       
        $consulta .= "order by D.Date DESC;";

       //echo $consulta;
    }
    

  /*  $consulta = "SELECT Year, DOrd, Qty, Crss, PurNo, Cons, PDF, Assoc as Associated, Inv as Invoiced, Rcv as Received, Rej as Rejected, DLVD as Embarked, Adj as Adjustments,(Qty - Adj) as TTQty,
    (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE DO=DOrd and Stg.TypeStg='Adjustment') + Qty - (SELECT IFNULL(SUM(Qty),0) FROM Lots WHERE Lots.DOrd=DOrds.Dord) as ToAssociate,
    (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=Dord and Truks.Status='Transit') as Transit,    
    (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=DOrd and  Truks.Status='Received') - (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE DO=Dord and Stg.TypeStg='Rejected') - (SELECT IFNULL(SUM(Qty),0) FROM Invs WHERE DO=Dord and Invs.InvSta='Ok') as ToInv, 
    (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE DO=Dord and Stg.TypeStg='Adjustment') + Qty - (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=Dord and Truks.Status='Transit') - (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=Dord and  Truks.Status='Received') as ToDO, 
    Rqs, Date, Typ, OutPlc,
    (SELECT RegNam FROM Region WHERE IDReg = OutPlc) as RegNameOut, Gin,
    (SELECT GinName FROM Gines WHERE IDGin = Gin) as GinName, InReg, 
    (SELECT RegNam FROM Region WHERE IDReg = InReg) as RegNameIn, InPlc, 
    (SELECT Cli FROM Clients WHERE CliID = InPlc) as PlcNameIn, Ctc, DoCmt FROM DOrds;";
        "SELECT Year, DOrd, Qty, Crss, PurNo, Cons, PDF,
(SELECT IFNULL(SUM(Qty),0) FROM Lots WHERE Lots.DOrd=DOrds.Dord) as Associated,
    (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE DO=DOrd and Stg.TypeStg='Adjustment') + Qty - (SELECT IFNULL(SUM(Qty),0) FROM Lots WHERE Lots.DOrd=DOrds.Dord) as ToAssociate,
    (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=Dord and Truks.Status='Transit') as Transit, 
    (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=Dord and  Truks.Status='Received') as Received, 
    (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=Dord and Truks.Status='Transit') + (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=Dord and  Truks.Status='Received') as Embarked, 
    (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE DO=DOrd and Stg.TypeStg='Adjustment') as Adjustments, 
    (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE DO=DOrd and Stg.TypeStg='Rejected') as Rejected, 
    (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE DO=DOrd and Stg.TypeStg='Adjustment') + Qty as TTQty, 
    (SELECT IFNULL(SUM(Qty),0) FROM Invs WHERE DO=DOrd and Invs.InvSta='Ok') as Invoiced, 
    (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=DOrd and  Truks.Status='Received') - (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE DO=Dord and Stg.TypeStg='Rejected') - (SELECT IFNULL(SUM(Qty),0) FROM Invs WHERE DO=Dord and Invs.InvSta='Ok') as ToInv, 
    (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE DO=Dord and Stg.TypeStg='Adjustment') + Qty - (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=Dord and Truks.Status='Transit') - (SELECT IFNULL(SUM(CrgQty),0) FROM Truks WHERE DO=Dord and  Truks.Status='Received') as ToDO, Rqs, Date, Typ, OutPlc,
    (SELECT RegNam FROM Region WHERE IDReg = OutPlc) as RegNameOut, Gin,
    (SELECT GinName FROM Gines WHERE IDGin = Gin) as GinName, InReg, 
    (SELECT RegNam FROM Region WHERE IDReg = InReg) as RegNameIn, InPlc, 
    (SELECT Cli FROM Clients WHERE CliID = InPlc) as PlcNameIn, Ctc, DoCmt FROM DOrds;";*/ //WHERE Qty!='0'
    //(SELECT RegNam FROM Region WHERE IDReg = InPlc) as PlcNameIn
   // print_r($consulta);
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $data=$resultado->fetchAll(PDO::FETCH_ASSOC);


  /*  $_SESSION['Filters'] = array(
        //filtros para trucks
        "idGlobal" => "",
        "asLots" => "",
        "DepArrDate" => "",
        //filtros para DO
        "typeDO" => "",
        "clientsDO" => "",
        //filtros para trucks y DO
        "muestras" => 200,
        "regionfil" => "",
        "DepRegFil" => "",
        "ArrRegFil" => "",
        "fromdate" => "",
        "todate" => ""
    ); */
    break;
    case 5:    
        $consulta = "SELECT * FROM Region ORDER BY RegNam ASC"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 6:    
        $consulta = "SELECT * FROM Gines ORDER BY GinName ASC"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 7:    
        $consulta = "SELECT * FROM Clients ORDER BY Cli ASC"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 8:    
        $consulta = "SELECT User FROM Users WHERE Authorize=1 ORDER BY User ASC"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 9:    
        $consulta = "SELECT * FROM Region WHERE Type=0 ORDER BY RegNam ASC"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 10:    
        $consulta = "SELECT * FROM Region WHERE Type=1 ORDER BY RegNam ASC"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 11:    
        $consulta = "SELECT ifnull(MAX(DOrds.Cons)+1,1) as Cons,
                    (SELECT DOGen FROM Region WHERE RegNam = '$OutPlc') as DOGen
                    FROM DOrds
                    WHERE CrpDO = (SELECT SUBSTRING('$Year', 3, 4));";
                    /*WHERE Year = '$Year';";
                    /*"SELECT DOGen, MAX(DOrds.Cons)+1 as Cons
                    FROM Region, DOrds
                    WHERE RegNam = '$OutPlc';";*/
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 12:
        $consulta = "SELECT IDReg, IsOrigin FROM Region WHERE RegNam='$OutReg'";
	      $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataOutPlc = $resultado->fetch();
        $OutPlc = $dataOutPlc['IDReg'];
        $Origin = $dataOutPlc['IsOrigin'];
        
        if ($Origin != 0){
            $consulta = "SELECT IDGin FROM Gines WHERE GinName='$OutGin'";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $dataOutGin = $resultado->fetch();
            $Gin = $dataOutGin['IDGin'];

            $consulta = "SELECT LotID, Lot, Qty,
            if (SchDate != '' AND SchDate is not null, CONCAT(' - (',DATE_FORMAT(STR_TO_DATE(SchDate, '%Y-%m-%d'), '%d/%m/%Y'),')'), '') as fecha
            FROM Lots
            WHERE Location = '$OutPlc' and GinID = '$Gin' and DOrd = '0' and Crop = '$CrpDO'".$complemento."
            order by Lot;"; 
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();        
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }else{
            $consulta = "SELECT LotID, Lot, Qty,
            if (SchDate != '' AND SchDate is not null, CONCAT(' - (',DATE_FORMAT(STR_TO_DATE(SchDate, '%Y-%m-%d'), '%d/%m/%Y'),')'), '') as fecha
                        FROM Lots
                        WHERE Location = '$OutPlc' and DOrd = '0' and Crop = '$CrpDO'".$complemento."
                        order by Lot;"; 
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();        
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }
        break;
    case 13:
        $Bodegas = array('65301', '65302', '65201', '65202', '61301');
        
        $consulta = "SELECT IDReg, IsOrigin FROM Region WHERE RegNam='$OutReg'";
	      $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataOutPlc = $resultado->fetch();
        $OutPlc = $dataOutPlc['IDReg'];
        $Origin = $dataOutPlc['IsOrigin'];
        
        $array = explode(",", $Lotes);
        $longitud = count($array);
        for($i=0; $i<=$longitud; $i++){
        
            $consulta = "SELECT Lot, Qty, SchDate FROM Lots WHERE LotID='$array[$i]'";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $dataQty = $resultado->fetch();
            $Lot = $dataQty['Lot'];
            $Qty = $dataQty['Qty'];
            $fechalot = $dataQty['SchDate'];
            
            //validar si cuando asocian el lote a la DO colocan una fecha de llegada
            
            if($SchDate !=0 && $SchDate !=""){
              $fecha_lote = $SchDate;
            }
            else{
              $fecha_lote = $fechalot;
              
            }
            
            $consulta = "UPDATE Lots SET  SchDate='$fecha_lote',DOrd='$DOrd' WHERE LotID='$array[$i]'";		
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            
            if (in_array($OutPlc, $Bodegas) || $Origin == 1) { //  ORDER BY Bal LIMIT '$Qty'
                $consulta = "UPDATE Bales SET DO = '$DOrd' WHERE (DO = '' OR DO = 0 OR DO is null) and Lot='$Lot' ORDER BY Bal Limit $Qty";
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
            }
            
        }
        break;
    case 14:
        $consulta = "SELECT IDReg FROM Region WHERE RegNam='$OutReg'";
	    $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataOutPlc = $resultado->fetch();
        $OutPlc = $dataOutPlc['IDReg'];
        
        $consulta = "SELECT LotID, Lot, Qty,
        if(TrkID=0,'',CONCAT(' - (TrkID: ',TrkID,')')) as truckid,        
        if (SchDate != '' AND SchDate is not null, CONCAT(' - (A. Date: ',DATE_FORMAT(STR_TO_DATE(SchDate, '%Y-%m-%d'), '%d/%m/%Y'),')'), '') as fecha
                    FROM Lots
                    WHERE Location = '$OutPlc' and DOrd = '$DOrd'
                    order by Lot;"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 15:
        $array = explode(",", $Lotes);
        $longitud = count($array);
        $data="";
        //consulta preparada para validar que el lote no tenga trkid
        $consulta_preparada="SELECT TrkID,Lot from Lots where LotID = :valor_loteid";
        $stmt = $conexion->prepare($consulta_preparada);
        for($i=0; $i<$longitud; $i++){

            $stmt->bindParam(':valor_loteid', $array[$i], PDO::PARAM_STR);
            $stmt->execute();
            $infotrk = $stmt->fetch();
            $TrkiLot = $infotrk['TrkID'];
            $lote = $infotrk['Lot'];

            if( $TrkiLot != 0){
                $data.="-Lot:".$lote." is on Truck:".$TrkiLot." it is not possible to disassociate"."\n";
            }

            else{


                $consulta = "UPDATE Lots SET DOrd = 0 WHERE LotID=$array[$i];";		
               // print($consulta);
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
                
                $consulta = "UPDATE Bales SET DO = 0 WHERE DO='$DOrd' ";		
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();

                $data.="-Lot: ".$lote." has been removed"."\n";
            }
        }
        break;
    case 16:
        //$txt = $selectText."|".$DoCmt;
        $consulta = "UPDATE DOrds SET DoCmt='$DoCmt' WHERE DOrd='$DOrd'";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        
        //Actualiza contadores en DO
        $consulta = "UPDATE DOrds SET Assoc = (SELECT SUM(Qty) FROM Lots WHERE DOrd = '$DOrd') 
                     WHERE DOrd = '$DOrd'";       
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        //-------------------------        
        
        $consulta = "SELECT Year, DOrd, Qty, Rqs, Date, Typ, OutPlc, Gin, InReg, InPlc, Ctc, DoCmt FROM DOrds WHERE DOrd='$DOrd' ";       
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
        
    case 17:         

 
  
      $consulta = "SELECT Certificate,
      (Select IsOrigin FROM Region WHERE RegNam ='$OutPlc') as IsOrigin
      from Clients WHERE Cli='$InPlc'";       
      
      
      $resultado = $conexion->prepare($consulta);
      $resultado->execute();
      $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
      break;

      case 18:
        $consulta = "SELECT count(Lot) as LotesDO from Lots WHERE DOrd='$DOrd'";  
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
}

print json_encode($data, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX
$conexion=null;
