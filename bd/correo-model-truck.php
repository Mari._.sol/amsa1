<?php

include_once ('conexion.php');
$objeto = new Conexion();
$conexion = $objeto->Conectar();

function getAuth($user){
    global $conexion;
    $query = 'SELECT email, passApp FROM Users WHERE User = "'.$user.'"';
    $result =$conexion->prepare($query);
    $result->execute();
    return $result->fetch(PDO::FETCH_ASSOC);
    $conexion=null;
}

function getLots($trkid,$delibery){
    global $conexion;
    $query = 'SELECT Lot, Qty  FROM Lots WHERE TrkID= "'.$trkid.'" AND DOrd = "'.$delibery.'"';
    $result = $conexion->prepare($query);
    $result->execute();
    return $result->fetchAll(PDO::FETCH_ASSOC);
}

function getcantidad($trkid,$delibery){
    global $conexion;
    $query = 'SELECT  SUM(Qty) as cantidad  FROM Lots WHERE TrkID= "'.$trkid.'" AND DOrd = "'.$delibery.'"';
    $result = $conexion->prepare($query);
    $result->execute();
    return $result->fetchAll(PDO::FETCH_ASSOC);
}

function cartaporte($trkid){
    global $conexion;
    $query = 'SELECT  RemisionPDF  FROM Truks WHERE TrkID= "'.$trkid.'"';
    $result = $conexion->prepare($query);
    $result->execute();
    return $result->fetchAll(PDO::FETCH_ASSOC);
}

//VALIDAR SI EL TRUCK SALE DE ORIGEN O DE BODEGA
function getregcarga($regsalida,$delibery){
    global $conexion;
    $query = 'SELECT IsOrigin  FROM Region WHERE RegNam= "'.$regsalida.'"';
    $result = $conexion->prepare($query);
    $result->execute();
    $isgin = $result->fetch();
	$isgin = $isgin['IsOrigin'];

    if($isgin == 1){
        $query2 = "SELECT Gin FROM DOrds WHERE Dord = '$delibery'";
        $gin = $conexion->prepare($query2);
        $gin->execute();
        $gin = $gin->fetch();
        $gin = $gin['Gin'];

        $query = "SELECT Name,BnName,Drctn,Ref,GinName as nombre
        FROM Gines WHERE IDGin = '$gin'";
        $result = $conexion->prepare($query);
        $result->execute();
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }
    else{
        $query3 = 'SELECT AMSAS  FROM Region WHERE RegNam= "'.$regsalida.'"';
        $result3 = $conexion->prepare($query3);
        $result3->execute();
        $isamsa = $result3->fetch();
	    $isamsa = $isamsa['AMSAS'];
       
        if($isamsa == ""){
            $query = 'SELECT  BnName , Drctn,Ref,RegNam as nombre FROM Region WHERE RegNam= "'.$regsalida.'"';
            $result = $conexion->prepare($query);
            $result->execute();
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }
        else{
            $query = 'SELECT AMSAS as BnName , Drctn,Ref,RegNam as nombre FROM Region WHERE RegNam= "'.$regsalida.'"';
            $result = $conexion->prepare($query);
            $result->execute();
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }
    }
}

function getmail_gin($regsalida,$delibery){
    $hijo = []; // iniciar array
    $arreglo1 = [];

    global $conexion;
    $query = 'SELECT IsOrigin,IsWHOrigin  FROM Region WHERE RegNam= "'.$regsalida.'"';
    $result = $conexion->prepare($query);
    $result->execute();
    $isgin = $result->fetch();

	$isgin2 = $isgin['IsOrigin'];
    $bod = $isgin['IsWHOrigin'];

    if ($isgin2 == 1){
        $query2 = "SELECT Gin FROM DOrds WHERE Dord = '$delibery'";
        $gin = $conexion->prepare($query2);
        $gin->execute();
        $gin = $gin->fetch();
        $gin = $gin['Gin'];

        $query = "SELECT mails FROM Gines WHERE IDGin = '$gin'";
        $result = $conexion->prepare($query);
        $result->execute();
    

        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    else if($bod == 1){
        $query = 'SELECT MailWHOrigin as mails  FROM Region WHERE RegNam= "'.$regsalida.'";';          
        $result = $conexion->prepare($query);
        $result->execute();
       // print_r($result);
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }   

    else{
        $hijo = [
            'mails' => "0"
        ];

        array_push($arreglo1, $hijo);
        return $arreglo1;   
        //print_r("ENtro al else");       
         
    }
}


function getmail_amsa($regsalida){
    global $conexion;

    $query = 'SELECT MailsAMSA as mails FROM Region WHERE RegNam= "'.$regsalida.'"';
    $result = $conexion->prepare($query);
    $result->execute();
    return $result->fetchAll(PDO::FETCH_ASSOC);
}


//Obtener correos con bandera HVI 
function getmail_HVI(){
    global $conexion;
    $email = "";
    $query = "SELECT email  FROM Users WHERE BanHVI='1' ";
    $result = $conexion->prepare($query);
    $result->execute();
    foreach($result as $row){
        $email .= ",";
        $email .= $row['email'];
        
    }
    return $email;
}


//OBTENER BANDERA DE CERTIFICADO EN TABLA DORS 
function getcertificate($TrkDO){
    global $conexion;
    $query = "SELECT Certificate FROM  DOrds WHERE 	DOrd = '$TrkDO'";
    $result = $conexion->prepare($query);
    $result->execute();
    return $result->fetchAll(PDO::FETCH_ASSOC);
}


function getregdescarga($regllegada){
    /*
    global $conexion;
    $query = 'SELECT AMSAS as BnName, Drctn,Maps,Ref  FROM Region WHERE RegNam= "'.$regllegada.'"';
    $result = $conexion->prepare($query);
    $result->execute();
    return $result->fetchAll(PDO::FETCH_ASSOC);*/
    global $conexion;
    $query3 = 'SELECT AMSAS  FROM Region WHERE RegNam= "'.$regllegada.'"';
    $result3 = $conexion->prepare($query3);
    $result3->execute();
    $isamsa = $result3->fetch();
    $isamsa = $isamsa['AMSAS'];
   

    if($isamsa == ""){
        $query = 'SELECT  BnName , Drctn,Ref,Maps,RegNam as nombre FROM Region WHERE RegNam= "'.$regllegada.'"';
        $result = $conexion->prepare($query);
        $result->execute();
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }
    else{
        $query = 'SELECT AMSAS as BnName , Drctn,Ref,Maps,RegNam as nombre FROM Region WHERE RegNam= "'.$regllegada.'"';
        $result = $conexion->prepare($query);
        $result->execute();
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

}

function getdircliente($cliente){
    global $conexion;
    $query = 'SELECT Drctn, Ref,Maps,BnName  FROM Clients WHERE Cli= "'.$cliente.'"';
    $result = $conexion->prepare($query);
    $result->execute();
    return $result->fetchAll(PDO::FETCH_ASSOC);
}


function insertdate($date,$trkid){
    global $conexion;
    $query = 'SELECT Datemailsent  FROM Truks WHERE TrkID= "'.$trkid.'"';
    $result = $conexion->prepare($query);
    $result->execute();
    $fecha = $result->fetch();
	$fecha = $fecha['Datemailsent'];

    if ($fecha ==""){
        $consulta = "UPDATE Truks SET Datemailsent='$date' WHERE TrkID ='$trkid' ";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
    }
}


function getmails_transport($trkid){
    global $conexion;

    $hijo2 = []; // iniciar array
    $arreglo2 = [];

    $query = 'SELECT TNam  FROM Truks WHERE TrkID= "'.$trkid.'"';
    $result = $conexion->prepare($query);
    $result->execute();
    $name = $result->fetch();
    $name = $name['TNam'];

    //print_r($name);

    if($name =="0"){
        $hijo2 = [
            'mails' => ""
        ];
        array_push($arreglo2, $hijo2);
        return $arreglo2;  
    }
    else{
        $query = 'SELECT mails   FROM Transports WHERE TptID= "'.$name.'"';
        $result = $conexion->prepare($query);
        $result->execute();
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }
}

function getinfo_transport($trkid){
    global $conexion;
    $query = 'SELECT TrkLPlt,TraLPlt,TNam,WBill,DrvNam,DrvTel,XML  FROM Truks WHERE TrkID= "'.$trkid.'"';
    $result = $conexion->prepare($query);
    $result->execute();
    return $result->fetchAll(PDO::FETCH_ASSOC);
}

function getname_transport($idtransport){
    global $conexion;
    $query = 'SELECT BnName FROM  Transports WHERE 	TptID= "'.$idtransport.'"';
    $result = $conexion->prepare($query);
    $result->execute();
    return $result->fetchAll(PDO::FETCH_ASSOC);
}


///CREAR PDF DE REMISION Y GUARDARLO EN CARPETA
function create_rem($TrkID,$TrkDO,$TrkTyp,$Reg,$Cli){
    global $conexion; 

    $consulta = "SELECT Cert FROM DOrds WHERE DOrd = '$TrkDO'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataDO=$resultado->fetch();
    $Cert = $dataDO['Cert'];

    if ($TrkTyp == "CON"){
        $consulta = "SELECT * FROM DOrds WHERE DOrd = '$TrkDO'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataDO=$resultado->fetch();
        $RegIn = $dataDO['InReg'];
        $RegOut = $dataDO['OutPlc'];
        
        $consulta = "SELECT * FROM Region WHERE IDReg = '$RegIn'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataRIn=$resultado->fetch();
        $CdeIn = $dataRIn['Cde'];
        
        $consulta = "SELECT * FROM Region WHERE IDReg = '$RegOut'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataROut=$resultado->fetch();
        $CdeOut = $dataROut['Cde'];
    }else{
        $consulta = "SELECT * FROM DOrds WHERE DOrd = '$TrkDO'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataDO=$resultado->fetch();
        $InPlc = $dataDO['InPlc'];
        $RegOut = $dataDO['OutPlc'];
        
        $consulta = "SELECT * FROM Clients WHERE CliID = '$InPlc'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataRIn=$resultado->fetch();
        $CdeIn = $dataRIn['Cde'];
        
        $consulta = "SELECT * FROM Region WHERE IDReg = '$RegOut'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataROut=$resultado->fetch();
        $CdeOut = $dataROut['Cde'];
    }
    
//SE AGREGA CONTRATO
$consulta = "SELECT Ctc,OutPlc FROM DOrds WHERE DOrd = '$TrkDO'"; 
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$dataDO=$resultado->fetch();
$contrato = $dataDO['Ctc'];
$salida = $dataDO['OutPlc'];

$consulta = "SELECT 	RegNam FROM Region WHERE IDReg = '$salida'"; 
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$dataDO=$resultado->fetch();
$regsal = $dataDO['RegNam'];
    
    $consulta = "SELECT * FROM Truks WHERE TrkID = '$TrkID'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $data=$resultado->fetch();
    
    $TName = $data['TNam'];
    
    $consulta = "SELECT  (CASE 
    when Lots.LoteLigado= '0'    then Lots.Lot            
    else CONCAT(Lots.Lot,' / ',Lots.LoteLigado)
END) as Lot,

  (CASE 
    when Lots.LoteLigado= '0'    then '13'            
    else '27'
END) as ancho,

Qty FROM Lots WHERE TrkID = '$TrkID'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataLots=$resultado->fetchAll(PDO::FETCH_ASSOC);
    
    $consulta = "SELECT BnName FROM Transports WHERE TptID = '$TName'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataTpt=$resultado->fetch();
    
    if ($Reg != "CLIENTE"){
        $consulta = "SELECT * FROM Region WHERE RegNam = '$Reg'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataCli=$resultado->fetch();
        $RemTo = $dataCli['BnName'];
        $Drc = $dataCli['Drctn'];
        $CP = $dataCli['CP'];
        $Town = $dataCli['Town'];
        $State = $dataCli['State'];
        $Ct1 = $dataCli['Ct1'];
        $Tel1 = $dataCli['Tel1'];
        $Ct2 = $dataCli['Ct2'];
        $Tel2 = $dataCli['Tel2'];
        $ref = $dataCli['Ref'];
        $maps = $dataCli['Maps'];
        //   $codigomasps = $maps;
    
        if($maps!=""){
            $pos = strpos($maps, ',');
            
            if ($pos !== false) {
                $maps = str_replace(',','%2C',$maps);    
            } 

            $link = "https://www.google.com/maps/search/?api=1%26query=".$maps."%26zoom=20";
            $qr="https://api.qrserver.com/v1/create-qr-code/?size=300x300&data=".$link;
        }
        
        $result1 = preg_replace('/\d{3}/', '$0-', str_replace('.', null, trim($Tel1)), 2); //Dar formato tel xxx-xxx-xxxx
        $result2 = preg_replace('/\d{3}/', '$0-', str_replace('.', null, trim($Tel2)), 2); //Dar formato tel xxx-xxx-xxxx
        
    }
else{

    if($TrkTyp =="EXP"){
        $consultaport = "SELECT Ports.Port,Ports.City,Ports.State,Ports.Direction,Ports.PostalCode,Ports.Reference,Ports.Maps 
        FROM amsadb1.Ports, amsadb1.Export  
        WHERE Export.TrkID = '$TrkID' AND Export.Port = Ports.IdPort;"; 
        $resultadoport = $conexion->prepare($consultaport);
        $resultadoport->execute();
        $portinfo=$resultadoport->fetch();

        //$RemTo = $portinfo['Port'];
        $Drc = $portinfo['Direction'];
        $CP = $portinfo['PostalCode'];
        $Town = $portinfo['City'];
        $State = $portinfo['State'];
        $ref = $portinfo['Reference'];
        $maps = $portinfo['Maps'];

        $consulta = "SELECT BnName FROM Clients WHERE Cli = '$Cli'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataCli=$resultado->fetch();
        $RemTo = $dataCli['BnName'];
    }
    else{
        $consulta = "SELECT * FROM Clients WHERE Cli = '$Cli'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataCli=$resultado->fetch();
        $RemTo = $dataCli['BnName'];
        $Drc = $dataCli['Drctn'];
        $CP = $dataCli['CP'];
        $Town = $dataCli['Town'];
        $State = $dataCli['State'];
        $ref = $dataCli['Ref'];

        
        $maps = $dataCli['Maps'];

    }
if($maps!=""){
    $pos = strpos($maps, ',');    
    if ($pos !== false) {
        $maps = str_replace(',','%2C',$maps);    
    } 
    $link = "https://www.google.com/maps/search/?api=1%26query=".$maps."%26zoom=20";

    $qr="https://api.qrserver.com/v1/create-qr-code/?size=300x300&data=".$link;
}



}

    
    include_once '../fpdf/fpdf.php';
    
    $pdf = new FPDF();
    $pdf->AddPage('portrait');
    $pdf->SetTitle($TrkID);
    $pdf->SetFont('Arial','B',10);
    $pdf->Image('../img/logo1.png', 12, 7, 15, 15, 'PNG');
    $pdf->Cell(0,5, utf8_decode('AGROINDUSTRIAS UNIDAS DE MEXICO S.A. DE C.V.'),0,0,'C');
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(0,5,'DIVISION DE ALGODON', 0, 0,'C');
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(198,5,'C. Bosque de Alisos 45A Piso 2, Bosques de las Lomas, Cuajimalpa de Morelos, CDMX, 05120. Tel. 55 52 57 65 00', 0, 0,'C');
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(0,5,'AUM980109Q78', 0, 0,'C');
    
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(0,10,'CARTA REMISION', 0, 0,'C');
    
    //CUERPO REMISION
    $pdf->Ln(6);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(170,8,'Numero: ', 0, 0,'R');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(0, 8, $TrkID, 0, 0, 'L');
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(170,8,'Fecha de carga: ', 0, 0,'R');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(0, 8, strtoupper(strftime('%d/%b/%Y', strtotime($data['OutDat']))), 0, 0, 'L'); //fecha español dd/mm/yyyy
    //$pdf->Cell(0, 8, strtoupper(date('d/M/Y', strtotime($data['OutDat']))), 0, 0, 'L'); //fecha ingles dd/mm/yyyy
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(170,8,'Fecha de entrega: ', 0, 0,'R');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(0, 8, strtoupper(strftime('%d/%b/%Y', strtotime($data['InDat']))), 0, 0, 'L'); //fecha español dd/mm/yyyy
    //$pdf->Cell(0, 8, strtoupper(date('d/M/Y', strtotime($data['InDat']))), 0, 0, 'L');
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(170,8,'Hora de entrega: ', 0, 0,'R');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(0, 8, substr($data['InTime'], -8, 5), 0, 0, 'L');
    
    $pdf->Ln(9);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(25,8,'Remitido a: ', 0, 0,'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(0, 8, utf8_decode($RemTo), 0, 0, 'L'); //buscar en tabla Reg o Client dependiendo de CON, DOM, Exp
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(25,8,'Direccion: ', 0, 0,'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(0, 8, strtoupper(utf8_decode($Drc)).", C.P. ".$CP, 0, 0, 'L'); //buscar en tabla Reg o Client dependiendo de CON, DOM, Exp
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(25,8,'Destino: ', 0, 0,'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(0, 8, strtoupper($Town).", ".strtoupper($State), 0, 0, 'L'); //buscar en tabla Reg o Client dependiendo de CON, DOM, Exp
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(25,8,'Referencia: ', 0, 0,'L');
    $pdf->SetFont('Arial','',9);
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(0, 8, utf8_decode($ref), 0, 0, 'L'); //buscar en tabla Reg o Client dependiendo de CON, DOM, Exp
    
    
    $pdf->Ln(9);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(25,8,'Descripcion: ', 0, 0,'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(37, 8, utf8_decode("PACAS DE ALGODÓN"), 0, 0, 'L');
    $pdf->Ln(7);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(25,5,'Marcas: ', 0, 0,'L');
    foreach($dataLots as $row){
        $pdf->SetFont('Arial','',9);
        $pdf->Cell($row['ancho'], 5, $row['Lot'], 'R', 0, 'C');
    }
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(25,5,'Cantidad: ', 0, 0,'L');
    foreach($dataLots as $row){
        $pdf->SetFont('Arial','',9);
        $pdf->Cell($row['ancho'], 5, $row['Qty'], 'R', 0, 'C');
    }
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(25,8,'Carta porte: ', 0, 0,'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(37, 8, $data['WBill'], 0, 0, 'L');
    $pdf->Ln(0);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(92,8,'Tracto:', 0, 0,'R');
    //$pdf->Ln(0);
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(20, 8, $data['TrkLPlt'], 0, 0, 'L');
    $pdf->Ln(0);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(160,8,'Placas:', 0, 0,'R');
    $pdf->Ln(0);
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(175, 8, $data['TraLPlt'], 0, 0, 'R');
    
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(25,8,'Observaciones: ', 0, 0,'L');
    $pdf->SetFont('Arial','',9);
    // agregar si trae pacas certificadas
    if($Cert ==1){
        $pdf->Cell(36, 8,("PACAS CERTIFICADAS"), 0, 0, 'L');
        if (!empty($data['Comments'])) {
            $pdf->Cell(3, 8,("|"), 0, 0, 'C');
        }
        $pdf->Cell(0, 8, ($data['Comments']), 0, 0, 'L');
    }else {
        $pdf->Cell(0, 8, $data['Comments'], 0, 0, 'L');
    }
    
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(35,8,'Linea de Transporte: ', 0, 0,'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(0, 8, utf8_decode($dataTpt['BnName']), 0, 0, 'L');
    
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(35,8,'Nombre de chofer: ', 0, 0,'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(80, 8, utf8_decode($data['DrvNam']), 0, 0, 'L');
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(30,8,'Orden de Embarque: ', 0, 0,'L');
    $pdf->Ln(0);
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(165, 8, $TrkDO, 0, 0, 'R');
    
    
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(35,8,utf8_decode('Región de salida: '), 0, 0,'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(80, 8, utf8_decode($regsal), 0, 0, 'L');
    if($TrkTyp !="CON"){
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(30,8,'Contrato: ', 0, 0,'L');
    $pdf->Ln(0);
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(165, 8, utf8_decode($contrato), 0, 0, 'R');
    }
    
    
    if($maps!=""){
    //$pdf->Image('../img/remision.png', 15, 128, 35, 55, 'PNG');
    
    $pdf->Image('../img/leyenda_qr.PNG', 9, 160, 46, 66, 'PNG');  
    $pdf->Image($qr, 14, 181, 35, 38, "png");
    }       
    
    $pdf->Ln(8);
if ($Reg != "CLIENTE"){

if($Reg=="PUEBLA"){
        $pdf->Cell(50, 8,'', 0, 0,'R');
        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(30, 8,'', 'TL', 0,'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(1, 8, "CONTACTOS", 'T', 0, 'R');
        $pdf->Cell(7, 8, "", 'T', 0, 'R');
        $pdf->Cell(47, 8, "Christian Becerra: 222-650-0994", 'T', 0, 'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(10, 8, "", 'TR', 0, 'C');
        $pdf->Ln(5);
        $pdf->Cell(50, 8,'', 0, 0,'R');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(30, 8,'', 'LB', 0,'R');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(1, 8, "", 'B', 0, 'R');
        $pdf->Cell(54, 8, "Juan Carlos: 222-650-0982", 'B', 0, 'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(10, 8, "", 'RB', 0, 'C');            
    
    
}

else if($Reg =="GOMEZ"){

        $pdf->Cell(50, 8,'', 0, 0,'R');
        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(30, 8,'', 'TL', 0,'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(1, 8, "CONTACTOS", 'T', 0, 'R');
        $pdf->Cell(5, 8, "", 'T', 0, 'R');
        $pdf->Cell(49, 8, "Bruno Barrientos: 618-134-0710", 'T', 0, 'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(10, 8, "", 'TR', 0, 'C');
        $pdf->Ln(5);
        $pdf->Cell(50, 8,'', 0, 0,'R');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(30, 8,'', 'LB', 0,'R');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(7, 8, "", 'B', 0, 'R');
        $pdf->Cell(48, 8, "Antonio Gutierrez: 871-213-0084", 'B', 0, 'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(10, 8, "", 'RB', 0, 'C');       

}

else if($Reg =="BOD. MOCT" || $Reg =="BOD. NH" || $Reg =="BOD. OASIS" || $Reg =="BOD. AGATE" ){

    $pdf->Cell(45, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',7);
    $pdf->Cell(45, 8,'', 'TL', 0,'C');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(10, 8, "CONTACTO : Bruno Barrientos", 'T', 0, 'C');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(45, 8, "", 'TR', 0, 'C');
    $pdf->Ln(5);
    $pdf->Cell(45, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(45, 8,'', 'LB', 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(10, 8, "TELEFONO: 618-134-0710", 'B', 0, 'C');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(45, 8, "", 'RB', 0, 'C');

}
else if($Reg == "BOD. MEXICALI"){

    $pdf->Cell(50, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',7);
    $pdf->Cell(30, 8,'', 'TL', 0,'C');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(1, 8, "CONTACTOS", 'T', 0, 'R');
    $pdf->Cell(54, 8, "     Bruno Barrientos: 618-134-0710", 'T', 0, 'C');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(10, 8, "", 'TR', 0, 'C');
    $pdf->Ln(5);
    $pdf->Cell(50, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(30, 8,'', 'L', 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(2, 8, "", '', 0, 'R');
    $pdf->Cell(53, 8, "Marcos Torres: 686-272-0121 ", '', 0, 'C');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(10, 8, "", 'R', 0, 'C');      
    $pdf->Ln(5);
    $pdf->Cell(50, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(30, 8,'', 'LB', 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(5.2, 8, "", 'B', 0, 'R');
    $pdf->Cell(49.8, 8, "Miguel  Diosdado: 686-228-2269", 'B', 0, 'C');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(10, 8, "", 'RB', 0, 'C');  
        


}



else if($Reg == "COM. DISTEX" || $Reg == "COM. TOLUCA" || $Reg == "COM. TURBO"){

    $pdf->Cell(50, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',7);
    $pdf->Cell(30, 8,'', 'TL', 0,'C');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(5, 8, "CONTACTOS", 'T', 0, 'R');
    $pdf->Cell(50, 8, "Christian Becerra: 222-650-0994", 'T', 0, 'C');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(10, 8, "", 'TR', 0, 'C');
    $pdf->Ln(5);
    $pdf->Cell(50, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(30, 8,'', 'LB', 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(5.5, 8, "", 'B', 0, 'R');
    $pdf->Cell(49.5, 8, "Bruno Barrientos: 618-134-0710", 'B', 0, 'C');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(10, 8, "", 'RB', 0, 'C');       
}

else{

    $pdf->Cell(45, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',7);
    $pdf->Cell(45, 8,'', 'TL', 0,'C');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(10, 8, "CONTACTO : Bruno Barrientos", 'T', 0, 'C');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(45, 8, "", 'TR', 0, 'C');
    $pdf->Ln(5);
    $pdf->Cell(45, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(45, 8,'', 'LB', 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(10, 8, "TELEFONO: 618-134-0710", 'B', 0, 'C');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(45, 8, "", 'RB', 0, 'C');

}

}
else{
    if ($regsal =='MEXICALI' && $TrkTyp =="EXP"){

        $pdf->Cell(50, 8,'', 0, 0,'R');
        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(30, 8,'', 'TL', 0,'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(1, 8, "CONTACTOS", 'T', 0, 'R');
        $pdf->Cell(54, 8, "     Bruno Barrientos: 618-134-0710", 'T', 0, 'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(10, 8, "", 'TR', 0, 'C');
        $pdf->Ln(5);
        $pdf->Cell(50, 8,'', 0, 0,'R');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(30, 8,'', 'L', 0,'R');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(2, 8, "", '', 0, 'R');
        $pdf->Cell(53, 8, "Marcos Torres: 686-272-0121 ", '', 0, 'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(10, 8, "", 'R', 0, 'C');      
        $pdf->Ln(5);
        $pdf->Cell(50, 8,'', 0, 0,'R');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(30, 8,'', 'LB', 0,'R');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(5.2, 8, "", 'B', 0, 'R');
        $pdf->Cell(49.8, 8, "Miguel  Diosdado: 686-228-2269", 'B', 0, 'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(10, 8, "", 'RB', 0, 'C');      

    }

    else if ($regsal =='GOMEZ' && $TrkTyp =="EXP"){

        $pdf->Cell(50, 8,'', 0, 0,'R');
        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(30, 8,'', 'TL', 0,'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(1, 8, "CONTACTOS", 'T', 0, 'R');
        $pdf->Cell(5, 8, "", 'T', 0, 'R');
        $pdf->Cell(49, 8, "Bruno Barrientos: 618-134-0710", 'T', 0, 'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(10, 8, "", 'TR', 0, 'C');
        $pdf->Ln(5);
        $pdf->Cell(50, 8,'', 0, 0,'R');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(30, 8,'', 'LB', 0,'R');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(7, 8, "", 'B', 0, 'R');
        $pdf->Cell(48, 8, "Antonio Gutierrez: 871-213-0084", 'B', 0, 'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(10, 8, "", 'RB', 0, 'C');       
    }
    else{
        $pdf->Cell(50, 8,'', 0, 0,'R');
        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(30, 8,'', 'TL', 0,'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(1, 8, "CONTACTOS", 'T', 0, 'R');
        $pdf->Cell(5, 8, "", 'T', 0, 'R');
        $pdf->Cell(49, 8, "Christian Becerra: 222-650-0994", 'T', 0, 'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(10, 8, "", 'TR', 0, 'C');
        $pdf->Ln(5);
        $pdf->Cell(50, 8,'', 0, 0,'R');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(30, 8,'', 'LB', 0,'R');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(6, 8, "", 'B', 0, 'R');
        $pdf->Cell(49, 8, "Bruno Barrientos: 618-134-0710", 'B', 0, 'C');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(10, 8, "", 'RB', 0, 'C');         

}



}  

$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0, 8,'___________________________________________________________________________________________________________', 0, 0,'C');
$pdf->Ln(6);
if($maps !=""){
$pdf->SetFont('Arial','B',9);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0, 8, "ESTIMADO OPERADOR:", '0', 0, 'L');
$pdf->Ln(7);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->SetFont('Arial','',8);
$pdf->MultiCell(0, 4, utf8_decode("-Revisar que el origen le entregue mínimo dos juegos de copias de la documentación (carta remisión, listados de pacas, carta porte). Un juego le será requerido como evidencia de entrega para la liberación del pago. La carta porte debe de estar sellada y/o firmada."), 0, 'J', false);
$pdf->Ln(2);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->SetFont('Arial','',8);
$pdf->MultiCell(0, 4, utf8_decode("-Es su obligación estar al pendiente de la carga. Se realizará el cargo correspondiente por cualquier faltante al momento de la descarga."), 0, 'L', false);
$pdf->Ln(2);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0, 8, "PROTOCOLO DE VIAJE*:", 0, 0, 'L');
$pdf->Ln(7);
if($State =="PUEBLA" || $State =="DURANGO"){
$pdf->SetFont('Arial','',8);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->MultiCell(0, 4, utf8_decode("-Por ningún motivo el personal de AMSA se encuentra laborando fuera de las instalaciones. Tampoco está facultado para solicitar la descarga en un lugar diferente a la dirección de entrega indicada en este documento."), 0, 'J', false);
$pdf->Ln(2);
}
if($regsal =="ASCENSION"){
$pdf->SetFont('Arial','',8);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->MultiCell(0, 4, utf8_decode("-Por seguridad, no transitar el tramo libre de Flores Magón-El Sueco, Buenaventura, Chih."), 0, 'J', false);
$pdf->Ln(2);
}
$pdf->SetFont('Arial','',8);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->MultiCell(0, 4, utf8_decode("-En viajes al centro del país (Puebla, México, Hidalgo, etc.), el único paradero autorizado para pasar las noches es el paradero de San Pedro o aquellos autorizados por su propia línea de transporte (en caso de tener)."), 0, 'J', false);
$pdf->Ln(2);
$pdf->SetFont('Arial','',8);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->MultiCell(0, 4, utf8_decode("-En caso de falla de la unidad, enfermedad del operador o cualquier otro imponderable, comunicarse inmediatamente al contacto proporcionado en esta hoja."), 0, 'L', false);
$pdf->Ln(2);
$pdf->SetFont('Arial','',8);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->MultiCell(0, 4, utf8_decode("-Por seguridad, se prohíbe la entrada al Arco Norte después de las 19:00 hrs."), 0, 'L', false);
$pdf->Ln(2);
$pdf->SetFont('Arial','',8);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->MultiCell(0, 4, utf8_decode("-Continuar su ruta a partir de las 6:00 hrs hacia su destino."), 0, 'L', false);
$pdf->Ln(2);
$pdf->SetFont('Arial','',8);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->MultiCell(0, 4, utf8_decode("-Si no encuentra la dirección de entrega, escanear el código QR. En caso de requerir mayor asistencia, comunicarse al contacto proporcionado en esta hoja. No buscar guías."), 0, 'L', false);
$pdf->Ln(5);
$pdf->SetFont('Arial','B',8);
$pdf->MultiCell(0, 4, utf8_decode("*En caso de no seguir con el protocolo al pie de la letra, la línea de transporte se dará de baja como proveedor de servicios de manera inmediata, y en caso de robo se detendrán los pagos."), 0, 'L', false);
}
else{
$pdf->Ln(4);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(2, 8,'', 0, 0,'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0, 8, "ESTIMADO OPERADOR:", '0', 0, 'L');
$pdf->Ln(7);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(2, 8,'', 0, 0,'L');
$pdf->SetFont('Arial','',8);
$pdf->MultiCell(0, 4, utf8_decode("-Revisar que el origen le entregue mínimo dos juegos de copias de la documentación (carta remisión, listados de pacas, carta porte). Un juego le será requerido como evidencia de entrega para la liberación del pago. La carta porte debe de estar sellada y/o firmada."), 0, 'J', false);
$pdf->Ln(2);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(2, 8,'', 0, 0,'L');
$pdf->SetFont('Arial','',8);
$pdf->MultiCell(0, 4, utf8_decode("-Es su obligación estar al pendiente de la carga. Se realizará el cargo correspondiente por cualquier faltante al momento de la descarga."), 0, 'L', false);
$pdf->Ln(2);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(2, 8,'', 0, 0,'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0, 8, "PROTOCOLO DE VIAJE*:", 0, 0, 'L');
$pdf->Ln(7);
if($State =="PUEBLA" || $State =="DURANGO"){
$pdf->SetFont('Arial','',8);
$pdf->Cell(2, 8,'', 0, 0,'L');
$pdf->MultiCell(0, 4, utf8_decode("-Por ningún motivo el personal de AMSA se encuentra laborando fuera de las instalaciones. Tampoco está facultado para solicitar la descarga en un lugar diferente a la dirección de entrega indicada en este documento."), 0, 'J', false);
$pdf->Ln(2);
}
if($regsal =="ASCENSION"){
$pdf->SetFont('Arial','',8);
$pdf->Cell(2, 8,'', 0, 0,'L');
$pdf->MultiCell(0, 4, utf8_decode("-Por seguridad, no transitar el tramo libre de Flores Magón-El Sueco, Buenaventura, Chih."), 0, 'J', false);
$pdf->Ln(2);
}
$pdf->SetFont('Arial','',8);
$pdf->Cell(2, 8,'', 0, 0,'L');
$pdf->MultiCell(0, 4, utf8_decode("-En viajes al centro del país (Puebla, México, Hidalgo, etc.), el único paradero autorizado para pasar las noches es el paradero de San Pedro o aquellos autorizados por su propia línea de transporte (en caso de tener)."), 0, 'J', false);
$pdf->Ln(2);
$pdf->SetFont('Arial','',8);
$pdf->Cell(2, 8,'', 0, 0,'L');
$pdf->MultiCell(0, 4, utf8_decode("-En caso de falla de la unidad, enfermedad del operador o cualquier otro imponderable, comunicarse inmediatamente al contacto proporcionado en esta hoja."), 0, 'L', false);
$pdf->Ln(2);
$pdf->SetFont('Arial','',8);
$pdf->Cell(2, 8,'', 0, 0,'L');
$pdf->MultiCell(0, 4, utf8_decode("-Por seguridad, se prohíbe la entrada al Arco Norte después de las 19:00 hrs."), 0, 'L', false);
$pdf->Ln(2);
$pdf->SetFont('Arial','',8);
$pdf->Cell(2, 8,'', 0, 0,'L');
$pdf->MultiCell(0, 4, utf8_decode("-Continuar su ruta a partir de las 6:00 hrs hacia su destino."), 0, 'L', false);
$pdf->Ln(2);
$pdf->SetFont('Arial','',8);
$pdf->Cell(2, 8,'', 0, 0,'L');
$pdf->MultiCell(0, 4, utf8_decode("-Si no encuentra la dirección de entrega, comunicarse al contacto proporcionado en esta hoja. No buscar guías."), 0, 'L', false);
$pdf->Ln(5);
$pdf->SetFont('Arial','B',8);
$pdf->MultiCell(0, 4, utf8_decode("*En caso de no seguir con el protocolo al pie de la letra, la línea de transporte se dará de baja como proveedor de servicios de manera inmediata, y en caso de robo se detendrán los pagos."), 0, 'L', false);
$pdf->Ln(5);

}
$pdf->Ln(3);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(0, 8,'REMITE', 0, 0,'C');
$pdf->Ln(12);
$pdf->SetFont('Arial','',8);
$pdf->Cell(65, 8, "", 0, 0, 'C');
$pdf->Cell(60, 8, "AGROINDUSTRIAS UNIDAS DE MEXICO SA DE CV", 'T', 0, 'C');
$pdf->Cell(65, 8, "", 0, 0, 'C');
    
    
    
    
    $filename = "Remision-".$TrkID.".pdf";
    
    //$pdf->Output('I', $TrkDO.".pdf");
    
    $pdf->Output('F', './temp/'.$filename, true);
    
    //return "/temp/'.$filename";
    //$conexion=null;
        }

    // Recibo el método, funcionaría para cualquier método que se genere sólo hay que pasarlo en la url como m o en la data de un ajax
    if(!empty($_REQUEST['m'])){ //verifico que exista algún método m sino no hace nada aunque llames al archivo
        $invocaMetodo = $_REQUEST['m'];
        if($invocaMetodo == "getTransport") //verifico que lo que recibí es este método para así llamarlo y me regrese la data necesaria
            $data = getTransport($_REQUEST['BnName']); //Paso el BnName para verificarlo en la base

        if($invocaMetodo == "getLastPrice")
            $data = getLastPrice();

        if($invocaMetodo == "getPrivReg")
            $data = getPrivReg($_REQUEST['Priv']);

        if($invocaMetodo == "getInfTrkID")
            $data = getInfTrkID($_REQUEST['RTrkID']);

        if($invocaMetodo == "getTrkID")
            $data = getTrkID($_REQUEST['LotID']);

        print json_encode($data, JSON_UNESCAPED_UNICODE);
    }


    //INSERTAR FECHA PLANEADA CUANDO MANDE CORREO
    function insert_date_plan($trkid){
        global $conexion;
        $query = 'SELECT SchOutDate,SchInDate,OutDat,OutTime,InDat,InTime  FROM Truks WHERE TrkID= "'.$trkid.'"';
        $result = $conexion->prepare($query);
        $result->execute();
        $fecha = $result->fetch();
        $fechaplansal = $fecha['SchOutDate'];
        $fechaplanent = $fecha['SchInDate'];
        $fecha_sal=$fecha['OutDat'];
        $hora_sal=$fecha['OutTime'];
        $fecha_lleg=$fecha['InDat'];
        $hora_lleg=$fecha['InTime'];
       // print_r($fechaplansal);
        if (($fechaplansal == "" || $fechaplansal == NULL )  && ($fechaplanent == "" || $fechaplanent == NULL )){        
        $consulta = "UPDATE Truks SET SchOutDate='$fecha_sal', SchOutTime='$hora_sal',SchInDate='$fecha_lleg',SchInTime='$hora_lleg'  WHERE TrkID ='$trkid' ";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
    
        }
    }

?>
