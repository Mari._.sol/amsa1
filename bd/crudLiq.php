<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

$data = array();

$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$idLiq = (isset($_POST['idLiq'])) ? $_POST['idLiq'] : '';
$liquidacion = (isset($_POST['liquidacion'])) ? $_POST['liquidacion'] : '';
$reg = (isset($_POST['reg'])) ? $_POST['reg'] : '';
$porc = (isset($_POST['porc'])) ? $_POST['porc'] : '';
$pay = (isset($_POST['pay'])) ? $_POST['pay'] : '';
$coment = (isset($_POST['coment'])) ? $_POST['coment'] : '';
$datProv = (isset($_POST['datProv'])) ? $_POST['datProv'] : '';
$usuario = (isset($_POST['usuario'])) ? $_POST['usuario'] : '';
//$datInv = (isset($_POST['datInv'])) ? $_POST['datInv'] : '';
$Supp = substr($liquidacion, 3, 3);

switch($opcion) {
    case 1:
        $consulta = "SELECT * FROM amsadb1.Liquidation;";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data = $resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
    case 2:
        $consulta = "SELECT MailLiq FROM amsadb1.Region where RegNam='$reg';";     
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_COLUMN, 0);
    break;
    case 3:
        $consulta = "UPDATE amsadb1.Liquidation SET PagoPorc ='$porc', Pago ='$pay', Comentarios='$coment', datProv='$datProv' WHERE IdLiq='$idLiq';";     //, datInv='$datInv'           
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data = $resultado->fetch(PDO::FETCH_ASSOC);
    break;
    case 4:
        $consulta = "SELECT fileFactura, fileXML From amsadb1.Liquidation WHERE IdLiq='$idLiq';";  
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
    case 5:
        $consulta = "SELECT filePago From amsadb1.Liquidation WHERE IdLiq='$idLiq';";  
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
    case 6:
        $consulta = "SELECT mailLiq From amsadb1.Users WHERE User='$usuario';";  
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
    case 7:
        $consulta = "SELECT DatCSF FROM amsadb1.ConfGen where View = 'ViewLiq' and IdConfig=1;";  
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
    case 8:
        $consulta = "SELECT MailLiq FROM amsadb1.proveed where Supplier='$Supp'AND MailLiq IS NOT NULL LIMIT 1;"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_COLUMN, 0);
    break;
}

print json_encode($data, JSON_UNESCAPED_UNICODE); // Envío del array final en formato JSON a AJAX
$conexion = null;
