<?php
include_once('correo-model.php');
$usuario = (!empty($_REQUEST['usuario'])) ? $_REQUEST['usuario'] : '';
  $typeDO = (!empty($_REQUEST['typeDO'])) ? $_REQUEST['typeDO'] : '';
  $numDO = (!empty($_REQUEST['numDO'])) ? $_REQUEST['numDO'] : '';
  $salSucu = (!empty($_REQUEST['salSucu'])) ? $_REQUEST['salSucu'] : '';
  $regOri = (!empty($_REQUEST['regOri'])) ? $_REQUEST['regOri'] : '';
  $arrReg = (!empty($_REQUEST['arrReg'])) ? $_REQUEST['arrReg'] : '';
  $client = (!empty($_REQUEST['client'])) ? $_REQUEST['client'] : '';
  $observ = (!empty($_REQUEST['observ'])) ? $_REQUEST['observ'] : '';
  $contra = (!empty($_REQUEST['contra'])) ? $_REQUEST['contra'] : '';
  $totalQty = (!empty($_REQUEST['totalQty'])) ? $_REQUEST['totalQty'] : '';

//Tabla de lotes asociados a la DO
$lotsAssociated = getLots($numDO);
if(!empty($lotsAssociated)){
$tabla = '<table style="margin-left: 1em; border-collapse: collapse; text-align= center;">';
foreach ($lotsAssociated as $lot) {
    $tabla .= '<tr><td>' . $lot['Lot'] . '</td><td>-</td><td>' . $lot['Qty'] . ' bc </td>';
    if (!empty($lot['SchDate']))
        $tabla .= '<td> (' . date("d-M-y", strtotime($lot['SchDate'])) . ') </td></tr>';
    else
        $tabla .= '</tr>';
} //date("d-M-y", strtotime($lot['SchDate']))
$tabla .= '</table>';
} else 
    $tabla = '';
//Emails predeterminados para la región de salida y llegada

$emailsDestino = getDestinoR($salSucu);
$emailsDestino1 = getDestinoR($arrReg);
$mailsEcom ="";
//OBTENER LOS CORREOS DE EQUIPO DE ECOM USA SOLO SI LA REGION DE SALIDA ES UNA Y GIN USA TEXAS
if($regOri == "USA TEXAS" && $salSucu =="USA"){
  $mailsEcom =  getMailsUSA($regOri);
}

//validar si se adjuntan los correos de ECOM USA
if($mailsEcom!=""){
  $emailsD = $emailsDestino['mails'] .",". $emailsDestino1['mails'].",".$mailsEcom['mails'];
}

else{
  $emailsD = $emailsDestino['mails'] .",". $emailsDestino1['mails'];
}
$emailDestino= array_unique($emailDestino = explode(",", $emailsD) , SORT_STRING);
$emailDestino = implode(",",$emailDestino);
//Asunto del correo
if ($typeDO != "CON"){
    $asunto = "DO " . $numDO . ", " . $salSucu . " - " . $client." (".$totalQty." bc)";
  }else{
    $asunto = "DO " . $numDO . ", " . $salSucu . " - " . $arrReg." (".$totalQty." bc)";
  }




$data['asunto']=$asunto;  
$data['emails']=$emailDestino;
$data['tabla'] = $tabla;

print json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);//envio el array final el formato json a AJAX
$conexion=null;

?>