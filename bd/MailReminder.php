<?php
require '../vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

require '../bd/vendor/phpmailer/phpmailer/src/PHPMailer.php';
require '../bd/vendor/phpmailer/phpmailer/src/SMTP.php';
require 'vendor/autoload.php';

$asunto = (isset($_POST['asunto'])) ? $_POST['asunto'] : '';
$prov = (isset($_POST['prov'])) ? $_POST['prov'] : '';
$acceso = "http://www.ecom-tracking.com/AMSASupplier/";

$email = "marisol.hernandez@ecomtrading.com";
$pass = "bgxbbgiyobochcsc";
$usuario = "Marisol Hernandez";

// Consultar los correos de acuerdo a los nombres de los proveedores
$Np = implode(',', array_fill(0, count($prov), '?'));
$updateAL = "UPDATE amsadb1.proveed SET Attempts = 0, Locked = 0 WHERE ProveedID IN ($Np)";
$updateBD = $conexion->prepare($updateAL);
$updateBD->execute($prov);

$nameProv = implode(',', array_fill(0, count($prov), '?'));
$consulta = "SELECT ProveedID, GROUP_CONCAT(email) AS emails, Password FROM amsadb1.proveed WHERE ProveedID IN ($nameProv) GROUP BY ProveedID";
$resultado = $conexion->prepare($consulta);
$resultado->execute($prov);
$proveedores = $resultado->fetchAll(PDO::FETCH_ASSOC);
// Instancia de PHPMailer
$mail = new PHPMailer();
$mail->isSMTP();
$mail->SMTPDebug = 0; //SMTP::DEBUG_SERVER;
$mail->Host = 'smtp.gmail.com';
$mail->Port = 465; // 465 o 587
$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS; // tls o ssl para gmail obligado
$mail->SMTPAuth = true;
// Credenciales de la cuenta
$mail->Username = $email;
$mail->Password = $pass;
// Quien envía este mensaje
$mail->setFrom($email, $usuario);

// Asunto del correo
$mail->Subject = $asunto;

// Contenido del correo
$mail->IsHTML(true);
$mail->CharSet = 'UTF-8';

foreach ($proveedores as $proveedor) {
  $emails = explode(',', $proveedor['emails']);
  
  foreach ($emails as $email) {
      // Construir el cuerpo del correo para cada proveedor
      $bodyy = 'Hola, le comparto su usuario y su contraseña para que pueda acceder al portal de proveedores.<br><br>';
      $bodyy .= '<div style="text-align: left;">
        <b style="color: #17562c; font-size: 16px;">Usuario:</b> ' . $proveedor['ProveedID'] . '<br>
      </div>';
      $bodyy .= '<div style="text-align: left;">
        <b style="color: #17562c; font-size: 16px;">Contraseña:</b> ' . $proveedor['Password'] . '<br><br>
      </div>';
      $bodyy .= 'Comparto el Link al cual debe ingresar para poder acceder al portal <a href="' . $acceso . '">Clic aqui.</a> <br>
      Cualquier duda estamos al pendiente.<br><br>';
      $bodyy .= '<b style="color: #6AA84F; font-size: 13px;">---------<br> Saludos</b>';

      // Destinatario (se envia correo a cada prov en cada iteracion)
      $mail->clearAddresses();
      $mail->addAddress($email);

      $mail->Body = $bodyy;

      if ($mail->send()) {
        $mailSent = true;
      } else {
          echo 'Error al enviar el correo a ' . $proveedor['ProveedID'] . ' ('.$email.'): ' . $mail->ErrorInfo . ',';
      }
  }
}
if ($mailSent) {
  echo 'Correo enviado a todos los destinatarios.';
}
?>
