<?php 
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set("America/Mexico_City");


// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

//Query
$query= "SELECT DISTINCT Truks.TrkID, TrkLPlt, TraLPlt, WBill, DrvLcs, DO, DrvNam, DrvTel, CrgQty, OutDat, OutTime, InDat, InTime, LotsAssc, Comments, Truks.Status,NumEcon, 
(SELECT Typ FROM DOrds WHERE DO=Dord) as Typ,
(SELECT Year FROM DOrds WHERE DO=Dord) as Crp,
(SELECT Date FROM DOrds WHERE DO=Dord) as DODate,
(Select BnName From Transports Where Truks.TNam = Transports.TptID) as TNam,
(Select RegNam From DOrds, Region Where Truks.DO = DOrds.DOrd and OutPlc = IDReg) as RegNameOut,
(Select RegNam From DOrds, Region Where Truks.DO = DOrds.DOrd and InReg = IDReg) as RegNameIn,
(Select Cli From DOrds, Clients Where Truks.DO = DOrds.DOrd and InPlc = CliID) as PlcNameIn,
(Select IFNULL(SUM(Lots.Qty),0) From Lots Where Truks.TrkID = Lots.TrkID) as LotQty,
ifnull(GinName, (SELECT GinName FROM DOrds, Gines WHERE Truks.DO = DOrds.DOrd and DOrds.Gin = Gines.IDGin)) as GinName 
FROM Truks 
left join Lots on Truks.DO = Lots.DOrd and Truks.LotsAssc = Lots.Lot
left join Gines on Lots.GinID = Gines.IDGin
WHERE Truks.Status = 'Transit' OR Truks.Status = 'Programmed'  order by Truks.TrkID DESC;";

$result = $conexion->prepare($query);
$result->execute();
$siexiste=0; //Para verificar que hayan datos

//Define the filename with current date
$fileName = "TrucksStatus-".date('d-m-Y').".xlsx";

$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Trucks in Transit");
//$hojaActiva->freezePane("A2");

//negritas en encabezado

$hojaActiva->getStyle('A1:X1')->getFont()->setBold( true );

//relleno de celda encabezado
$hojaActiva->getStyle('A1:X1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFE1E1E1');

//fijar primera fila
$hojaActiva->freezePane('A2');


$hojaActiva->getColumnDimension('A')->setWidth(13);
$hojaActiva->setCellValue('A1','Crop');
$hojaActiva->getColumnDimension('B')->setWidth(13);
$hojaActiva->setCellValue('B1','Type');
$hojaActiva->getColumnDimension('C')->setWidth(13);
$hojaActiva->setCellValue('C1','DO');
$hojaActiva->getColumnDimension('D')->setWidth(13);
$hojaActiva->setCellValue('D1','Lot');
$hojaActiva->getColumnDimension('E')->setWidth(13);
$hojaActiva->setCellValue('E1','BC');
$hojaActiva->getColumnDimension('F')->setWidth(13);
$hojaActiva->setCellValue('F1','Gin');
$hojaActiva->getColumnDimension('G')->setWidth(13);
$hojaActiva->setCellValue('G1','Departure Region');
$hojaActiva->getColumnDimension('H')->setWidth(13);
$hojaActiva->setCellValue('H1','Arrival Region');
$hojaActiva->getColumnDimension('I')->setWidth(13);
$hojaActiva->setCellValue('I1','Client');
$hojaActiva->getColumnDimension('J')->setWidth(13);
$hojaActiva->setCellValue('J1','Departure Date');
$hojaActiva->getColumnDimension('K')->setWidth(13);
$hojaActiva->setCellValue('K1','Departure Time');
$hojaActiva->getColumnDimension('L')->setWidth(13);
$hojaActiva->setCellValue('L1','Arrival Date');
$hojaActiva->getColumnDimension('M')->setWidth(13);
$hojaActiva->setCellValue('M1','Arrival Time');
$hojaActiva->getColumnDimension('N')->setWidth(13);
$hojaActiva->setCellValue('N1','Days Late');
$hojaActiva->getColumnDimension('O')->setWidth(13);
$hojaActiva->setCellValue('O1','Truck ID');
$hojaActiva->getColumnDimension('P')->setWidth(13);
$hojaActiva->setCellValue('P1','Bussiness Name Transport');
$hojaActiva->getColumnDimension('Q')->setWidth(13);
$hojaActiva->setCellValue('Q1','License Plate');
$hojaActiva->getColumnDimension('R')->setWidth(13);
$hojaActiva->setCellValue('R1','Driver');
$hojaActiva->getColumnDimension('S')->setWidth(13);
$hojaActiva->setCellValue('S1','Phone Number');
$hojaActiva->getColumnDimension('T')->setWidth(13);
$hojaActiva->setCellValue('T1','WayBill');
$hojaActiva->getColumnDimension('U')->setWidth(13);
$hojaActiva->setCellValue('U1','Status');
$hojaActiva->getColumnDimension('V')->setWidth(13);
$hojaActiva->setCellValue('V1','DO Date');
$hojaActiva->getColumnDimension('W')->setWidth(13);
$hojaActiva->setCellValue('W1','Comments');
$hojaActiva->getColumnDimension('X')->setWidth(13);
$hojaActiva->setCellValue('X1','N° Econ.');


$fila = 2;

$today = new DateTime();//DateTime("2022-04-06"); //echo $today->format('Y-m-d');

$fecha_salida="";
$fecha_llegada="";
$fecha_creacion="";

while($row = $result->fetch(PDO::FETCH_ASSOC)){
    if($siexiste==0){
        if(!empty($row['TrkID']))
            $siexiste=1;
    }
    $dlate = 0;
    $textlate = "";
    $textlateDep = "";
    if($row['Status'] == "Transit"){
        $arrivalDate = new DateTime($row['InDat']); //echo $second->format('Y-m-d');
        $intvl = $arrivalDate->diff($today);
        
        if(($today > $arrivalDate)&&($intvl->days > 0)){
            $dlate = $intvl->days;
            $textlate = "Delayed ";
    }}

    if($row['Status'] == "Programmed"){
        $DepartureDate = new DateTime($row['OutDat']); //echo $second->format('Y-m-d');
        $intvl = $DepartureDate->diff($today);
        
        if(($today > $DepartureDate)&&($intvl->days > 0)){
            $dlate = $intvl->days;
            $textlateDep = "Delayed ";
    }}
    
        
    $fecha_salida = strtotime($row['OutDat']);
    $fecha_salida =25569 + ($fecha_salida / 86400);
    $fecha_llegada =strtotime($row['InDat']);
    $fecha_llegada =25569 + ($fecha_llegada / 86400);
    $fecha_creacion =strtotime($row['DODate']);
    $fecha_creacion =25569 + ($fecha_creacion / 86400);     
     
      $hojaActiva->getStyle('J' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    $hojaActiva->getStyle('L' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    $hojaActiva->getStyle('V' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);

    $hojaActiva->setCellValue('A' . $fila,$row['Crp']);
    $hojaActiva->setCellValue('B' . $fila,$row['Typ']);
    $hojaActiva->setCellValue('C' . $fila,$row['DO']);
    //$hojaActiva->setCellValue('D' . $fila,$row['LotsAssc']);
    $hojaActiva->setCellValue('E' . $fila,$row['LotQty']);
    $hojaActiva->setCellValue('F' . $fila,$row['GinName']);
    $hojaActiva->setCellValue('G' . $fila,$row['RegNameOut']);
    $hojaActiva->setCellValue('H' . $fila,$row['RegNameIn']);
    $hojaActiva->setCellValue('I'. $fila,$row['PlcNameIn']);
    $hojaActiva->setCellValue('J'. $fila,$fecha_salida);
    $hojaActiva->setCellValue('K'. $fila,$row['OutTime']);
    $hojaActiva->setCellValue('L'. $fila,$fecha_llegada);
    $hojaActiva->setCellValue('M'. $fila,$row['InTime']);
    $hojaActiva->setCellValue('N'. $fila,$dlate);
    $hojaActiva->setCellValue('O'. $fila,$row['TrkID']);
    $hojaActiva->setCellValue('P'. $fila,$row['TNam']);
    $hojaActiva->setCellValue('Q'. $fila,$row['DrvLcs']);
    $hojaActiva->setCellValue('R'. $fila,$row['DrvNam']);
    $hojaActiva->setCellValue('S'. $fila,$row['DrvTel']);
    $hojaActiva->setCellValue('T'. $fila,$row['WBill']);
    if ($row['Status'] == "Programmed"){
      $hojaActiva->setCellValue('U'. $fila,$textlateDep."Scheduled");
    }else{
      $hojaActiva->setCellValue('U'. $fila,$textlate.$row['Status']);
    }
    $hojaActiva->setCellValue('V' . $fila,$fecha_creacion);
    $hojaActiva->setCellValue('W' . $fila,$row['Comments']);
    $hojaActiva->setCellValue('X' . $fila,$row['NumEcon']);
    
    
    //quitar funcion exponencial en excel para lotes americanos 23E02 
    
    $style = $hojaActiva->getStyle('D'. $fila);

    $style->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
    
    $hojaActiva->getCell('D'. $fila)->setValueExplicit($row['LotsAssc'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);

    $fila++;
}

//Query Lots sin truck
$query= "SELECT DISTINCT DOrds.Year as Crp, DOrds.Typ, Lots.DOrd as DO, Lots.Lot, Lots.Qty, Gines.GinName,
(Select RegNam From DOrds, Region Where Lots.DOrd = DOrds.DOrd and OutPlc = IDReg) as RegNameOut,
(Select RegNam From DOrds, Region Where Lots.DOrd = DOrds.DOrd and InReg = IDReg) as RegNameIn,
(Select Cli From DOrds, Clients Where Lots.DOrd = DOrds.DOrd and InPlc = CliID) as PlcNameIn, SchDate as InDat, DOrds.Date as DODate
FROM Lots
JOIN DOrds on DOrds.DOrd = Lots.DOrd
JOIN Gines on DOrds.Gin = Gines.IDGin
WHERE Lots.TrkID = 0 and Lots.Crop >= 2023 group by Lot, DO order by Lot;";

$result = $conexion->prepare($query);
$result->execute();

while($row = $result->fetch(PDO::FETCH_ASSOC)){
    if($siexiste==0){
        if(!empty($row['TrkID']))
            $siexiste=1;
    }
    $dlate = 0;
    $textlate = "";
    if($row['Status'] == "Transit"){
        $arrivalDate = new DateTime($row['InDat']); //echo $second->format('Y-m-d');
        $intvl = $arrivalDate->diff($today);
        
        if(($today > $arrivalDate)&&($intvl->days > 0)){
            $dlate = $intvl->days;
            $textlate = "Delayed ";
    }}
    
    $fecha_salida = strtotime($row['OutDat']);
    $fecha_salida =25569 + ($fecha_salida / 86400);
    if ($row['InDat'] != ""){
    $fecha_llegada =strtotime($row['InDat']);
    $fecha_llegada =25569 + ($fecha_llegada / 86400);
    }else{ $fecha_llegada = ""; }
    $fecha_creacion =strtotime($row['DODate']);
    $fecha_creacion =25569 + ($fecha_creacion / 86400);     
     
      $hojaActiva->getStyle('J' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    $hojaActiva->getStyle('L' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    $hojaActiva->getStyle('V' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);

    $hojaActiva->setCellValue('A' . $fila,$row['Crp']);
    $hojaActiva->setCellValue('B' . $fila,$row['Typ']);
    $hojaActiva->setCellValue('C' . $fila,$row['DO']);
    //$hojaActiva->setCellValue('D' . $fila,$row['Lot']);
    $hojaActiva->setCellValue('E' . $fila,$row['Qty']);
    $hojaActiva->setCellValue('F' . $fila,$row['GinName']);
    $hojaActiva->setCellValue('G' . $fila,$row['RegNameOut']);
    $hojaActiva->setCellValue('H' . $fila,$row['RegNameIn']);
    $hojaActiva->setCellValue('I'. $fila,$row['PlcNameIn']);
    $hojaActiva->setCellValue('J'. $fila,"");
    $hojaActiva->setCellValue('K'. $fila,"");
    $hojaActiva->setCellValue('L'. $fila,$fecha_llegada);
    $hojaActiva->setCellValue('M'. $fila,"");
    $hojaActiva->setCellValue('N'. $fila,"");
    $hojaActiva->setCellValue('O'. $fila,"");
    $hojaActiva->setCellValue('P'. $fila,"");
    $hojaActiva->setCellValue('Q'. $fila,"");
    $hojaActiva->setCellValue('R'. $fila,"");
    $hojaActiva->setCellValue('S'. $fila,"");
    $hojaActiva->setCellValue('T'. $fila,"");
    if ($row['GinName'] == "AGATE" && $row['RegNameIn'] == "BOD. AGATE"){
      $hojaActiva->setCellValue('U'. $fila,"To Store");
    }else if ($row['GinName'] == "OASIS 1" && $row['RegNameIn'] == "BOD. OASIS"){
      $hojaActiva->setCellValue('U'. $fila,"To Store");
    }else if ($row['GinName'] == "MOCT. 1" && $row['RegNameIn'] == "BOD. MOCT"){
      $hojaActiva->setCellValue('U'. $fila,"To Store");
    }else if ($row['GinName'] == "MOCT. 2" && $row['RegNameIn'] == "BOD. MOCT"){
      $hojaActiva->setCellValue('U'. $fila,"To Store");
    }else if ($row['GinName'] == "NH 1" && $row['RegNameIn'] == "BOD. NH"){
      $hojaActiva->setCellValue('U'. $fila,"To Store");
    }else if ($row['GinName'] == "NH 2" && $row['RegNameIn'] == "BOD. NH"){
      $hojaActiva->setCellValue('U'. $fila,"To Store");
    }else{
      $hojaActiva->setCellValue('U'. $fila,"Unscheduled");
    } 
    $hojaActiva->setCellValue('V' . $fila,$fecha_creacion);
    $hojaActiva->setCellValue('W' . $fila,"");
    
    
     //quitar funcion exponencial en excel para lotes americanos 23E02 
    
    $style = $hojaActiva->getStyle('D'. $fila);

    $style->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
    
    $hojaActiva->getCell('D'. $fila)->setValueExplicit($row['Lot'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);

    $fila++;
}

//Query Lotes por asociar a DO
$query= "SELECT DISTINCT DOrds.Year as Crp, DOrds.Typ, DOrds.DOrd as DO, Qty - Assoc as TTQty, Gines.GinName,
(Select RegNam From Region Where OutPlc = IDReg) as RegNameOut,
(Select RegNam From Region Where InReg = IDReg) as RegNameIn,
(Select Cli From Clients Where InPlc = CliID) as PlcNameIn, DOrds.Date as DODate
FROM DOrds
JOIN Gines on DOrds.Gin = Gines.IDGin
WHERE Assoc != Qty order by DO;";

$result = $conexion->prepare($query);
$result->execute();

while($row = $result->fetch(PDO::FETCH_ASSOC)){
    if($siexiste==0){
        if(!empty($row['TrkID']))
            $siexiste=1;
    }
    $dlate = 0;
    $textlate = "";
    if($row['Status'] == "Transit"){
        $arrivalDate = new DateTime($row['InDat']); //echo $second->format('Y-m-d');
        $intvl = $arrivalDate->diff($today);
        
        if(($today > $arrivalDate)&&($intvl->days > 0)){
            $dlate = $intvl->days;
            $textlate = "Delayed ";
    }}
    
    $fecha_salida = strtotime($row['OutDat']);
    $fecha_salida =25569 + ($fecha_salida / 86400);
    if ($row['InDat'] != ""){
    $fecha_llegada =strtotime($row['InDat']);
    $fecha_llegada =25569 + ($fecha_llegada / 86400);
    }else{ $fecha_llegada = ""; }
    $fecha_creacion =strtotime($row['DODate']);
    $fecha_creacion =25569 + ($fecha_creacion / 86400);     
     
      $hojaActiva->getStyle('J' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    $hojaActiva->getStyle('L' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    $hojaActiva->getStyle('V' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);

    $hojaActiva->setCellValue('A' . $fila,$row['Crp']);
    $hojaActiva->setCellValue('B' . $fila,$row['Typ']);
    $hojaActiva->setCellValue('C' . $fila,$row['DO']);
    $hojaActiva->setCellValue('D' . $fila,"S/L");
    $hojaActiva->setCellValue('E' . $fila,$row['TTQty']);
    $hojaActiva->setCellValue('F' . $fila,$row['GinName']);
    $hojaActiva->setCellValue('G' . $fila,$row['RegNameOut']);
    $hojaActiva->setCellValue('H' . $fila,$row['RegNameIn']);
    $hojaActiva->setCellValue('I'. $fila,$row['PlcNameIn']);
    $hojaActiva->setCellValue('J'. $fila,"");
    $hojaActiva->setCellValue('K'. $fila,"");
    $hojaActiva->setCellValue('L'. $fila,"");
    $hojaActiva->setCellValue('M'. $fila,"");
    $hojaActiva->setCellValue('N'. $fila,"");
    $hojaActiva->setCellValue('O'. $fila,"");
    $hojaActiva->setCellValue('P'. $fila,"");
    $hojaActiva->setCellValue('Q'. $fila,"");
    $hojaActiva->setCellValue('R'. $fila,"");
    $hojaActiva->setCellValue('S'. $fila,"");
    $hojaActiva->setCellValue('T'. $fila,"");
    $hojaActiva->setCellValue('U'. $fila,"No Lot");
    $hojaActiva->setCellValue('V' . $fila,$fecha_creacion);
    $hojaActiva->setCellValue('W' . $fila,"");

    $fila++;
}


$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => Border::BORDER_THIN,
            'color' => ['rgb' => 'FF000000'],
        ],
    ],
];
$fila--;
//$hojaActiva->getStyle('A1:W1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

$hojaActiva->getStyle('A1:X'.$fila)->applyFromArray($styleArray);


// redirect output to client browser
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');

$writer = IOFactory::createWriter($excel, 'Xlsx');
$writer->save('php://output');
exit;

?>