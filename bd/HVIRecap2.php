<?php

setlocale(LC_TIME, "spanish");

function lowercase($element)
{
    return "'".$element."'";
}

$DO = $_GET['DO'];
$Lots = $_GET['Lots'];
$Crp = $_GET['Crp'];
$TypHVI = $_GET['TypHVI'];
$Cli = $_GET['Cli'];

include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

//Si la busqueda es por DO
if ($DO != ""){
    $Lots = array();
    $i = 0;
    $consulta = 'SELECT distinct Bales.Lot FROM Bales, DOrds, Lots WHERE DOrds.DOrd = '.$DO.' and DOrds.DOrd = Lots.DOrd and Bales.Lot = Lots.Lot;'; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataDO=$resultado->fetchALL(PDO::FETCH_ASSOC);
    foreach($dataDO as $dat){
        $Lots[$i] = $dat['Lot'];
        $i++;
    }
    $array = array_map('lowercase',$Lots);
    $array = implode(",",$array);
}else{
    $array = array_map('lowercase', explode(',', $Lots));
    $array = implode(",",$array);
}


include_once '../fpdf/fpdf.php';

class PDF extends FPDF
{
   //Cabecera de página
   function Header()
   {
    $this->SetFont('Arial','B',10);
    $this->Image('../img/logo1.png', 15, 8, 13, 13, 'PNG');
    $this->SetTextColor(1,100,46);
    $this->Cell(0,6, utf8_decode('AGROINDUSTRIAS UNIDAS DE MEXICO S.A. DE C.V.'),0,0,'C');
    $this->Ln(4);
    $this->SetTextColor(0,0,0);
    $this->SetFont('Arial','B',9);
    $this->Cell(0,7,'COTTON DIVISION', 0, 0,'C');
    $this->Ln(4);
    $this->SetFont('Arial','B',9);
    $this->SetLineWidth(0.6);
    $this->Cell(0,5,date('d/m/Y'), 'B', 0,'R');
    $this->Ln();
    $this->Ln(3);
    //$this->Ln();
    //$this->Cell(0,6, utf8_decode(''),0,0,'C');
    //$this->Ln();
   }

   //Pie de página
   function Footer()
   {
       // Position at 1.5 cm from bottom
       $this->SetY(-15);
       // Arial italic 8
       $this->SetFont('Arial','I',8);
       // Page number
       $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
   }
}

//consulta promedio
//colsulta
if ($TypHVI == 'O'){
    if ($Crp != ""){
        $consulta = 'SELECT Round(AVG(Mic),1) as PromMic, Round(AVG(Len),2) as PromLen, Round(AVG(Str),1) as PromStr, Round(AVG(Tgrd),1) as PromTgrd, Round(AVG(Unf),1) as PromUnf, Round(AVG(Rd),1) as PromRd, Round(AVG(b),1) as Promb, Round(AVG(Sfi),1) as PromSfi 
        FROM Bales WHERE Lot in ('.$array.') and Crp = '.$Crp.';'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataProm=$resultado->fetch();
    }else{
        $consulta = 'SELECT Round(AVG(Mic),1) as PromMic, Round(AVG(Len),2) as PromLen, Round(AVG(Str),1) as PromStr, Round(AVG(Tgrd),1) as PromTgrd, Round(AVG(Unf),1) as PromUnf, Round(AVG(Rd),1) as PromRd, Round(AVG(b),1) as Promb, Round(AVG(Sfi),1) as PromSfi 
        FROM Bales WHERE Lot in ('.$array.');'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataProm=$resultado->fetch();
    }
}else{
    if ($Crp != ""){
        $consulta = 'SELECT Round(AVG(Mic_Mod),1) as PromMic, Round(AVG(Len_Mod),2) as PromLen, Round(AVG(Str_Mod),1) as PromStr, Round(AVG(Tgrd_Mod),1) as PromTgrd, Round(AVG(Unf_Mod),1) as PromUnf, Round(AVG(Rd_Mod),1) as PromRd, Round(AVG(b_Mod),1) as Promb, Round(AVG(Sfi_Mod),1) as PromSfi 
        FROM Bales WHERE Lot in ('.$array.') and Crp = '.$Crp.';'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataProm=$resultado->fetch();
    }else{
        $consulta = 'SELECT Round(AVG(Mic_Mod),1) as PromMic, Round(AVG(Len_Mod),2) as PromLen, Round(AVG(Str_Mod),1) as PromStr, Round(AVG(Tgrd_Mod),1) as PromTgrd, Round(AVG(Unf_Mod),1) as PromUnf, Round(AVG(Rd_Mod),1) as PromRd, Round(AVG(b_Mod),1) as Promb, Round(AVG(Sfi_Mod),1) as PromSfi 
        FROM Bales WHERE Lot in ('.$array.');'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataProm=$resultado->fetch();
    }
}

//Cuerpo del PDF
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('Landscape','Letter');
$pdf->SetTitle("Recap ".$Cli);

if ($DO == ""){
    $pdf->Cell(0,5, utf8_decode('Lots: '.$Lots),0,0,'L');
}else{
    $Lots = implode( ',', $Lots);
    $pdf->Cell(0,5, utf8_decode('Lots: '.$Lots),0,0,'L');
}$pdf->Ln();
$pdf->Cell(0,2, utf8_decode(''),0,0,'C');
$pdf->Ln();

$pdf->SetXY(10,35);
$pdf->SetFillColor(218,245,207);
$pdf->Rect(10, 55, 260, 10, 'F');
$pdf->Rect(10, 35, 96, 10, 'F');

$pdf->SetFont('Arial','B',9);

$pdf->Cell(96,5, utf8_decode('Averages'),1,0,'C');
$pdf->Ln();
$pdf->Cell(12,5, utf8_decode('Mic'),1,0,'C');
$pdf->Cell(12,5, utf8_decode('Len'),1,0,'C');
$pdf->Cell(12,5, utf8_decode('Str'),1,0,'C');
$pdf->Cell(12,5, utf8_decode('Trs'),1,0,'C');
$pdf->Cell(12,5, utf8_decode('Unf'),1,0,'C');
$pdf->Cell(12,5, utf8_decode('Rd'),1,0,'C');
$pdf->Cell(12,5, utf8_decode('b'),1,0,'C');
$pdf->Cell(12,5, utf8_decode('Sfi'),1,0,'C');
$pdf->Ln();
$pdf->SetFont('Arial','',9);
$pdf->Cell(12,5, utf8_decode($dataProm['PromMic']),1,0,'C');
$pdf->Cell(12,5, utf8_decode($dataProm['PromLen']),1,0,'C');
$pdf->Cell(12,5, utf8_decode($dataProm['PromStr']),1,0,'C');
$pdf->Cell(12,5, utf8_decode($dataProm['PromTgrd']),1,0,'C');
$pdf->Cell(12,5, utf8_decode($dataProm['PromUnf']),1,0,'C');
$pdf->Cell(12,5, utf8_decode($dataProm['PromRd']),1,0,'C');
$pdf->Cell(12,5, utf8_decode($dataProm['Promb']),1,0,'C');
$pdf->Cell(12,5, utf8_decode($dataProm['PromSfi']),1,0,'C');

$pdf->Ln(10);
//$pdf->Line(10, 105, 270, 105);

//$pdf->SetXY(10,35);
//$pdf->SetFillColor(218,245,207);
//$pdf->Rect(10, 35, 260, 10, 'F');

$pdf->SetFont('Arial','B',9);

$coordenadaX=$pdf->GetX();
$coordenadaY=$pdf->GetY();

//PINTAR SOLO CUADRICULA SIN ENCABEZADO
$pdf->Cell(20,5, utf8_decode(''),1,0,'C');
$pdf->Cell(120,5, utf8_decode(''),1,0,'C');
$pdf->Cell(108,5, utf8_decode(''),1,0,'C');
$pdf->Cell(12,5, utf8_decode('  '),1,0,'C');

$pdf->SetXY($coordenadaX, $coordenadaY);
$pdf->SetLineWidth(0.7);
//COLOCAR DATOS DE ENCABEZADO CON GROSOR DERECHO
$pdf->Cell(20,5, utf8_decode(''),'R',0,'C');
$pdf->Cell(120,5, utf8_decode('Len'),'R',0,'C');
$pdf->Cell(108,5, utf8_decode('Mic'),'R',0,'C');

$pdf->Ln();
$pdf->SetLineWidth(0);

$coordenadaX=$pdf->GetX();
$coordenadaY=$pdf->GetY();

//PINTAR SOLO CUADRICULA SIN ENCABEZADO
$pdf->Cell(10,5, '',1,0,'C');
$pdf->Cell(10,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');
$pdf->Cell(12,5, '',1,0,'C');

$pdf->SetXY($coordenadaX, $coordenadaY);
$pdf->SetLineWidth(0.7);

//AGREGAR TEXTO AL ENCABEZADO CON GROSOR DE LINEAS DIFERENTES
$pdf->Cell(10,5, utf8_decode('Col'),'B',0,'C');
$pdf->Cell(10,5, utf8_decode('Trs'),'BR',0,'C');
$pdf->Cell(12,5, utf8_decode('33-'),'B',0,'C');
$pdf->Cell(12,5, utf8_decode('34'),'B',0,'C');
$pdf->Cell(12,5, utf8_decode('35'),'B',0,'C');
$pdf->Cell(12,5, utf8_decode('36'),'B',0,'C');
$pdf->Cell(12,5, utf8_decode('37'),'B',0,'C');
$pdf->Cell(12,5, utf8_decode('38'),'B',0,'C');
$pdf->Cell(12,5, utf8_decode('39'),'B',0,'C');
$pdf->Cell(12,5, utf8_decode('40'),'B',0,'C');
$pdf->Cell(12,5, utf8_decode('41'),'B',0,'C');
$pdf->Cell(12,5, utf8_decode('42+'),'BR',0,'C');
$pdf->Cell(12,5, utf8_decode('2.6-'),'B',0,'C');
$pdf->Cell(12,5, utf8_decode('2.7-2.9'),'B',0,'C');
$pdf->Cell(12,5, utf8_decode('3.0-3.2'),'B',0,'C');
$pdf->Cell(12,5, utf8_decode('3.3-3.4'),'B',0,'C');
$pdf->Cell(12,5, utf8_decode('3.5-3.6'),'B',0,'C');
$pdf->Cell(12,5, utf8_decode('3.7-4.7'),'B',0,'C');
$pdf->Cell(12,5, utf8_decode('4.8-4.9'),'B',0,'C');
$pdf->Cell(12,5, utf8_decode('5.0-5.2'),'B',0,'C');
$pdf->Cell(12,5, utf8_decode('5.3+'),'BR',0,'C');
$pdf->Cell(12,5, utf8_decode('Total'),'B',0,'C');



//colocar grosor de celda a la normalidad
$pdf->SetLineWidth(0);


//colsulta
if ($TypHVI == 'O'){
    if ($Crp != ""){
        $consulta = 'SELECT Col1 as Col1s, Tgrd, Round(32*Round(Len,2)+0.05,0) as Lens, Round(Mic,1) as Mics, count(Bal) as Bal 
        FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' 
        Group By Col1s, Tgrd Order by Col1s;'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);

        $consulta = 'SELECT Col1 as Col1s, Tgrd, Round(Mic,1) as Mics, count(Bal) as Bal 
        FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' 
        Group By Col1s, Tgrd, Mics;'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataMic=$resultado->fetchALL(PDO::FETCH_ASSOC);

        $consulta = 'SELECT Col1 as Col1s, Tgrd, Round(32*Round(Len,2)+0.05,0) as Lens, count(Bal) as Bal 
        FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' 
        Group By Col1s, Tgrd, Lens;'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataLen = $resultado->fetchALL(PDO::FETCH_ASSOC);
        
    }else{
        $consulta = 'SELECT Col1 as Col1s, Tgrd, Round(32*Round(Len,2)+0.05,0) as Lens, Round(Mic,1) as Mics, count(Bal) as Bal 
        FROM Bales WHERE Lot IN ('.$array.') 
        Group By Col1s, Tgrd Order by Col1s;'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);

        $consulta = 'SELECT Col1 as Col1s, Tgrd, Round(Mic,1) as Mics, count(Bal) as Bal 
        FROM Bales WHERE Lot IN ('.$array.') 
        Group By Col1s, Tgrd, Mics;'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataMic=$resultado->fetchALL(PDO::FETCH_ASSOC);

        $consulta = 'SELECT Col1 as Col1s, Tgrd, Round(32*Round(Len,2)+0.05,0) as Lens, count(Bal) as Bal 
        FROM Bales WHERE Lot IN ('.$array.') 
        Group By Col1s, Tgrd, Lens;'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataLen = $resultado->fetchALL(PDO::FETCH_ASSOC);

    }
}else{
    if ($Crp != ""){
        $consulta = 'SELECT Col1_Mod as Col1s, Tgrd_Mod as Tgrd, Round(32*Round(Len_Mod,2)+0.05,0) as Lens, Round(Mic_Mod,1) as Mics, count(Bal) as Bal 
        FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' 
        Group By Col1s, Tgrd Order by Col1s;'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);

        $consulta = 'SELECT Col1_Mod as Col1s, Tgrd_Mod as Tgrd, Round(Mic_Mod,1) as Mics, count(Bal) as Bal 
        FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' 
        Group By Col1s, Tgrd, Mics;'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataMic=$resultado->fetchALL(PDO::FETCH_ASSOC);

        $consulta = 'SELECT Col1_Mod as Col1s, Tgrd_Mod as Tgrd, Round(32*Round(Len_Mod,2)+0.05,0) as Lens, count(Bal) as Bal 
        FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' 
        Group By Col1s, Tgrd, Lens;'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataLen = $resultado->fetchALL(PDO::FETCH_ASSOC);

    }else{
        $consulta = 'SELECT Col1_Mod as Col1s, Tgrd_Mod as Tgrd, Round(32*Round(Len_Mod,2)+0.05,0) as Lens, Round(Mic_Mod,1) as Mics, count(Bal) as Bal 
        FROM Bales WHERE Lot IN ('.$array.') 
        Group By Col1s, Tgrd Order by Col1s;';
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);

        $consulta = 'SELECT Col1_Mod as Col1s, Tgrd_Mod as Tgrd, Round(Mic_Mod,1) as Mics, count(Bal) as Bal 
        FROM Bales WHERE Lot IN ('.$array.') 
        Group By Col1s, Tgrd, Mics;'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataMic=$resultado->fetchALL(PDO::FETCH_ASSOC);

        $consulta = 'SELECT Col1_Mod as Col1s, Tgrd_Mod as Tgrd, Round(32*Round(Len_Mod,2)+0.05,0) as Lens, count(Bal) as Bal 
        FROM Bales WHERE Lot IN ('.$array.') 
        Group By Col1s, Tgrd, Lens;'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataLen = $resultado->fetchALL(PDO::FETCH_ASSOC);

    }
}

$valorMin = 0;
$valorMax = 0;
$valorMinMic = 0;
$valorMaxMic = 0;
$uf = 65;
foreach ($data as $row) {
    $valoresMic = array(0,0,0,0,0,0,0,0,0);
    $valoresLen = array(0,0,0,0,0,0,0,0,0,0);
    $valorMinMic = 0;
    $valorMaxMic = 0;
    $valorMinLen = 0;
    $valorMaxLen = 0;
    $pdf->Ln();
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(10,5, utf8_decode($row['Col1s']),1,0,'C');
    $pdf->SetFont('Arial','B',9);
    $coordenadaX=$pdf->GetX();
    $coordenadaY=$pdf->GetY();
    //pintar solo cuadro vacio
    $pdf->Cell(10,5,'',1,0,'C');   
    $pdf->SetXY($coordenadaX, $coordenadaY);
    //valor con borde mas grueso
    $pdf->SetLineWidth(0.7);
    $pdf->Cell(10,5, utf8_decode($row['Tgrd']),'R',0,'C');
    $pdf->SetLineWidth(0);
    
    //Obtener la coordenada para agregar color a la ultima fola de totales
    $uf = $uf + 5;
    
    //Llenar vector de Len
    foreach($dataLen as $dat){
        if ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Lens'] <= 33){
            $valorMinLen = $valorMinLen + $dat['Bal'];
            $valoresLen[0] = $valorMinLen;
        }elseif ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Lens'] == 34){
            $valoresLen[1] = $valoresLen[1] + $dat['Bal'];
        }elseif ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Lens'] == 35){
            $valoresLen[2] = $valoresLen[2] + $dat['Bal'];
        }elseif ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Lens'] == 36){
            $valoresLen[3] = $valoresLen[3] + $dat['Bal'];
        }elseif ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Lens'] == 37){
            $valoresLen[4] = $valoresLen[4] + $dat['Bal'];
        }elseif ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Lens'] == 38){
            $valoresLen[5] = $valoresLen[5] + $dat['Bal'];
        }elseif ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Lens'] == 39){
            $valoresLen[6] = $valoresLen[6] + $dat['Bal'];
        }elseif ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Lens'] == 40){
            $valoresLen[7] = $valoresLen[7] + $dat['Bal'];
        }elseif ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Lens'] == 41){
            $valoresLen[8] = $valoresLen[8] + $dat['Bal'];
        }elseif ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Lens'] >= 42){
            $valorMaxLen = $valorMaxLen + $dat['Bal'];
            $valoresLen[9] = $valorMaxLen;
        }
    }

    //llenar Vecto de micro
    foreach($dataMic as $dat){
        if ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Mics'] <= 2.6){
            $valorMinMic = $valorMinMic + $dat['Bal'];
            $valoresMic[0] = $valorMinMic;
        }elseif ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Mics'] > 2.6 && $dat['Mics'] <= 2.9){
            $valoresMic[1] = $valoresMic[1] + $dat['Bal'];
        }elseif ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Mics'] > 2.9 && $dat['Mics'] <= 3.2){
            $valoresMic[2] = $valoresMic[2] + $dat['Bal'];
        }elseif ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Mics'] > 3.2 && $dat['Mics'] <= 3.4){
            $valoresMic[3] = $valoresMic[3] + $dat['Bal'];
        }elseif ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Mics'] > 3.4 && $dat['Mics'] <= 3.6){
            $valoresMic[4] = $valoresMic[4] + $dat['Bal'];
        }elseif ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Mics'] > 3.6 && $dat['Mics'] <= 4.7){
            $valoresMic[5] = $valoresMic[5] + $dat['Bal'];
        }elseif ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Mics'] > 4.7 && $dat['Mics'] <= 4.9){
            $valoresMic[6] = $valoresMic[6] + $dat['Bal'];
        }elseif ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Mics'] > 4.9 && $dat['Mics'] <= 5.2){
            $valoresMic[7] = $valoresMic[7] + $dat['Bal'];
        }elseif ($row['Col1s'] == $dat['Col1s'] && $row['Tgrd'] == $dat['Tgrd'] && $dat['Mics'] > 5.2){
            $valorMaxMic = $valorMaxMic + $dat['Bal'];
            $valoresMic[8] = $valorMaxMic;
        }
    }

    //Pegar valores Len en la tabla
    $pdf->SetFont('Arial','',9);
    for ($i=0; $i < Count($valoresLen); $i++){
        if ($valoresLen[$i] != 0){
            if($i==9){
                $coordenadaX=$pdf->GetX();
                $coordenadaY=$pdf->GetY();
                $pdf->Cell(12,5,'',1,0,'C');
                $pdf->SetXY($coordenadaX, $coordenadaY);
                $pdf->SetLineWidth(0.7);
                 $pdf->Cell(12,5, utf8_decode($valoresLen[$i]),'R',0,'C');
                 $pdf->SetLineWidth(0);
            }
            else{
                $pdf->Cell(12,5, utf8_decode($valoresLen[$i]),1,0,'C');
            }
        }else{
            if($i==9){

                $coordenadaX=$pdf->GetX();
                $coordenadaY=$pdf->GetY();
                $pdf->Cell(12,5,'',1,0,'C');
                $pdf->SetXY($coordenadaX, $coordenadaY);
                $pdf->SetLineWidth(0.7);
                $pdf->Cell(12,5, utf8_decode(''),'R',0,'C');
                $pdf->SetLineWidth(0);
            }
            else{
                $pdf->Cell(12,5, utf8_decode(''),1,0,'C');
            }
        }  
    }
    
    //pegar valores Mic en tabla
    for ($i=0; $i < Count($valoresMic); $i++){
        
        if ($valoresMic[$i] != 0){
            if($i==8){
                $coordenadaX=$pdf->GetX();
                $coordenadaY=$pdf->GetY();
                $pdf->Cell(12,5,'',1,0,'C');
                $pdf->SetXY($coordenadaX, $coordenadaY);
                $pdf->SetLineWidth(0.7);
                $pdf->Cell(12,5, utf8_decode($valoresMic[$i]),'R',0,'C');
                $pdf->SetLineWidth(0);
            }
            else{
                $pdf->Cell(12,5, utf8_decode($valoresMic[$i]),1,0,'C');
            }


        }else{

            if($i==8){
                $coordenadaX=$pdf->GetX();
                $coordenadaY=$pdf->GetY();
                $pdf->Cell(12,5,'',1,0,'C');
                $pdf->SetXY($coordenadaX, $coordenadaY);
                $pdf->SetLineWidth(0.7);
                $pdf->Cell(12,5, utf8_decode(''),'R',0,'C');
                $pdf->SetLineWidth(0);
            }
            else{
                $pdf->Cell(12,5, utf8_decode(''),1,0,'C');
            }
        }  
    }

    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(12,5, utf8_decode($row['Bal']),1,0,'C');

}


//Llenar vector de suma Len y Mic
$valorMinMic = 0;
$valorMaxMic = 0;
$valorMinLen = 0;
$valorMaxLen = 0;
$sumTotales = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
foreach($dataLen as $dat){
    if ($dat['Lens'] <= 33){
        $valorMinLen = $valorMinLen + $dat['Bal'];
        $sumTotales[0] = $valorMinLen;
    }elseif ($dat['Lens'] == 34){
        $sumTotales[1] = $sumTotales[1] + $dat['Bal'];
    }elseif ($dat['Lens'] == 35){
        $sumTotales[2] = $sumTotales[2] + $dat['Bal'];
    }elseif ($dat['Lens'] == 36){
        $sumTotales[3] = $sumTotales[3] + $dat['Bal'];
    }elseif ($dat['Lens'] == 37){
        $sumTotales[4] = $sumTotales[4] + $dat['Bal'];
    }elseif ($dat['Lens'] == 38){
        $sumTotales[5] = $sumTotales[5] + $dat['Bal'];
    }elseif ($dat['Lens'] == 39){
        $sumTotales[6] = $sumTotales[6] + $dat['Bal'];
    }elseif ($dat['Lens'] == 40){
        $sumTotales[7] = $sumTotales[7] + $dat['Bal'];
    }elseif ($dat['Lens'] == 41){
        $sumTotales[8] = $sumTotales[8] + $dat['Bal'];
    }elseif ($dat['Lens'] >= 42){
        $valorMaxLen = $valorMaxLen + $dat['Bal'];
        $sumTotales[9] = $valorMaxLen;
    }
}

foreach($dataMic as $dat){
    if ($dat['Mics'] <= 2.6){
        $valorMinMic = $valorMinMic + $dat['Bal'];
        $sumTotales[10] = $valorMinMic;
    }elseif ($dat['Mics'] > 2.6 && $dat['Mics'] <= 2.9){
        $sumTotales[11] = $sumTotales[11] + $dat['Bal'];
    }elseif ($dat['Mics'] > 2.9 && $dat['Mics'] <= 3.2){
        $sumTotales[12] = $sumTotales[12] + $dat['Bal'];
    }elseif ($dat['Mics'] > 3.2 && $dat['Mics'] <= 3.4){
        $sumTotales[13] = $sumTotales[13] + $dat['Bal'];
    }elseif ($dat['Mics'] > 3.4 && $dat['Mics'] <= 3.6){
        $sumTotales[14] = $sumTotales[14] + $dat['Bal'];
    }elseif ($dat['Mics'] > 3.6 && $dat['Mics'] <= 4.7){
        $sumTotales[15] = $sumTotales[15] + $dat['Bal'];
    }elseif ($dat['Mics'] > 4.7 && $dat['Mics'] <= 4.9){
        $sumTotales[16] = $sumTotales[16] + $dat['Bal'];
    }elseif ($dat['Mics'] > 4.9 && $dat['Mics'] <= 5.2){
        $sumTotales[17] = $sumTotales[17] + $dat['Bal'];
    }elseif ($dat['Mics'] > 5.2){
        $valorMaxMic = $valorMaxMic + $dat['Bal'];
        $sumTotales[18] = $valorMaxMic;
    }
}

//print_r ($sumTotales);
//echo nl2br("\n");

//Ultima fila de Totales ---

$pdf->SetFillColor(241,253,242);
$pdf->Rect(10, $uf, 260, 5, 'F');

$totalGeneral = 0;
$pdf->Ln();
$pdf->SetFont('Arial','B',9);
$coordenadaX=$pdf->GetX();
$coordenadaY=$pdf->GetY();
$pdf->Cell(20,5,'',1,0,'C');
$pdf->SetXY($coordenadaX, $coordenadaY);
$pdf->SetLineWidth(0.7);
$pdf->Cell(20,5, utf8_decode('Total'),'TR',0,'C');
$pdf->SetLineWidth(0);



for ($i=0; $i < Count($sumTotales); $i++){
    if ($sumTotales[$i] != 0){
        $coordenadaX=$pdf->GetX();
        $coordenadaY=$pdf->GetY();
        $pdf->Cell(12,5,'',1,0,'C');
        $pdf->SetXY($coordenadaX, $coordenadaY);
        $pdf->SetLineWidth(0.7);
        if($i==9){
            $pdf->Cell(12,5, utf8_decode($sumTotales[$i]),'TR',0,'C');
        }
        else{
            $pdf->Cell(12,5, utf8_decode($sumTotales[$i]),'T',0,'C');
        }    
    
        $pdf->SetLineWidth(0);   
        $totalGeneral = $totalGeneral + $sumTotales[$i];
    }else{
        $coordenadaX=$pdf->GetX();
        $coordenadaY=$pdf->GetY();
        $pdf->Cell(12,5,'',1,0,'C');
        $pdf->SetXY($coordenadaX, $coordenadaY);
        $pdf->SetLineWidth(0.7);    
        if($i==9){
            $pdf->Cell(12,5, utf8_decode(''),'TR',0,'C');
        }
        else{
            $pdf->Cell(12,5, utf8_decode(''),'T',0,'C');
        }
        $pdf->SetLineWidth(0);   
    }
}
$coordenadaX=$pdf->GetX();
$coordenadaY=$pdf->GetY();
$pdf->Cell(12,5,'',1,0,'C');
$pdf->SetXY($coordenadaX, $coordenadaY);
$pdf->SetLineWidth(0.7);    
$pdf->Cell(12,5, utf8_decode($totalGeneral/2),'LT',0,'C');
$pdf->SetLineWidth(0);   


$pdf->SetLineWidth(0.7);
/*$pdf->Line(30,55,30,$uf+5);
$pdf->Line(150,55,150,$uf+5);
$pdf->Line(258,55,258,$uf+5);

$pdf->Line(10,65,270,65);
$pdf->Line(10,$uf,270,$uf);
*/

//HOJA 2
$pdf->SetLineWidth(0.2);
$pdf->AddPage('Landscape','Letter');

//Arreglo con los valores definidos en Recap
$TCol = array('11','12','13','21','22','23','24','25','31','32','33','34','35','41','42','43','44','51','52','53','54','61','62','63','71');
$TTrs = array('1','2','3','4','5','6','7','','','','','','','','','','','','','','','','','','');
$TMic = array('2.8','2.9','3.0','3.1','3.2','3.3','3.4','3.5','3.6','3.7','3.8','3.9','4.0','4.1','4.2','4.3','4.4','4.5','4.6','4.7','4.8','4.9','5.0','5.1','5.2');
$TStr = array('16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40');
$TLen = array('0.98','0.99','1.00','1.01','1.02','1.03','1.04','1.05','1.06','1.07','1.08','1.09','1.10','1.11','1.12','1.13','1.14','1.15','1.16','1.17','1.18','1.19','1.20','1.21','1.22');
$TUnf = array('68','69','70','71','72','73','74','75','76','77','78','79','80','81','82','83','84','85','86','87','88','89','90','91','92');
$TRd = array('66','67','68','69','70','71','72','73','74','75','76','77','78','79','80','81','82','83','84','85','86','87','88','89','90');
$Tb = array('6.8','6.9','7.0','7.1','7.2','7.3','7.4','7.5','7.6','7.7','7.8','7.9','8.0','8.1','8.2','8.3','8.4','8.5','8.6','8.7','8.8','8.9','9.0','9.1','9.2');


//------- Col
$sum = 0;
$pdf->Ln();
$pdf->SetXY(10,35);
$pdf->SetFillColor(218,245,207);
$pdf->Rect(10, 35, 28, 5, 'F');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(14,5, utf8_decode('Col'),1,0,'C');
$pdf->Cell(14,5, utf8_decode('Bal'),1,0,'C');

if ($TypHVI == 'O'){
    if ($Crp != ""){
        $consulta = 'SELECT Round(Col1,0) as Col1s, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Col1s'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Col1,0) as Col1s, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Col1s'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}else{
    if ($Crp != ""){
        $consulta = 'SELECT Round(Col1_Mod,0) as Col1s, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Col1s'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Col1_Mod,0) as Col1s, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Col1s'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}

foreach ($TCol as $row) {
    $pdf->Ln();
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(14,5, utf8_decode($row),1,0,'C');

    foreach ($data as $dat){
        if ($dat['Col1s'] == $row){
            $valor = $dat['Bal'];
            $sum = $sum + $dat['Bal'];
            break;
        }else {
            $valor = '';
        }
    }
    
    $pdf->Cell(14,5, utf8_decode($valor),1,0,'C'); 

}

$pdf->Ln();
$pdf->Cell(14,5, utf8_decode('Total'),1,0,'C');
$pdf->Cell(14,5, utf8_decode($sum),1,0,'C');

// ------ Trs
$pdf->Ln();
$pdf->SetXY(43,35);
$pdf->SetFillColor(218,245,207);
$pdf->Rect(43, 35, 28, 5, 'F');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(14,5, utf8_decode('Trs'),1,0,'C');
$pdf->Cell(14,5, utf8_decode('Bal'),1,0,'C');

if ($TypHVI == 'O'){
    if ($Crp != ""){
        $consulta = 'SELECT Round(Tgrd,0) as Tgrds, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Tgrds'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Tgrd,0) as Tgrds, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Tgrds'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}else{
    if ($Crp != ""){
        $consulta = 'SELECT Round(Tgrd_Mod,0) as Tgrds, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Tgrds'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Tgrd_Mod,0) as Tgrds, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Tgrds'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}

$y=40;
foreach ($TTrs as $row) {
    $pdf->Ln();
    $pdf->SetXY(43,$y);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(14,5, utf8_decode($row),1,0,'C');

    foreach ($data as $dat){
        if ($dat['Tgrds'] == $row){
            $valor = $dat['Bal'];
            break;
        }else { 
            $valor = '';
        }
    }

    $pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    $y = $y + 5;
}

$pdf->Ln();
$pdf->SetXY(43,$y);
$pdf->Cell(14,5, utf8_decode('Total'),1,0,'C');
$pdf->Cell(14,5, utf8_decode($sum),1,0,'C');

// ------ Mic
$pdf->Ln();
$pdf->SetXY(76,35);
$pdf->SetFillColor(218,245,207);
$pdf->Rect(76, 35, 28, 5, 'F');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(14,5, utf8_decode('Mic'),1,0,'C');
$pdf->Cell(14,5, utf8_decode('Bal'),1,0,'C');

if ($TypHVI == 'O'){
    if ($Crp != ""){
        $consulta = 'SELECT Round(Mic,1) as Mics, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Mics'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Mic,1) as Mics, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Mics'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}else{
    if ($Crp != ""){
        $consulta = 'SELECT Round(Mic_Mod,1) as Mics, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Mics'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Mic_Mod,1) as Mics, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Mics'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}

$y=40;
$valorMin = 0;
$valorMax = 0;
//$valor = 0;
foreach ($TMic as $row) {
    $valorMin = 0;
    $valorMax = 0;
    $pdf->Ln();
    $pdf->SetXY(76,$y);
    $pdf->SetFont('Arial','B',9);
    if ($row == 2.8){
        $pdf->Cell(14,5, utf8_decode($row."-"),1,0,'C');     
    }elseif($row == 5.2){
        $pdf->Cell(14,5, utf8_decode($row."+"),1,0,'C');
    }else{
        $pdf->Cell(14,5, utf8_decode($row),1,0,'C');
    }

    foreach ($data as $dat){
        if ($dat['Mics'] <= 2.8){
            //$dat['Mics'] = 2.8;
            $valorMin = $valorMin + $dat['Bal'];
        }elseif ($dat['Mics'] >= 5.2){
            //$dat['Mics'] = 5.2;
            $valorMax = $valorMax + $dat['Bal'];
        }elseif ($dat['Mics'] == $row && $row != 2.8 && $row != 5.2){
            $valor = $dat['Bal'];
            break;
        }else{ 
            $valor = '';
        }
    }

    if ($row == 2.8 && $valorMin != 0){
        $pdf->Cell(14,5, utf8_decode($valorMin),1,0,'C');       
    }elseif ($row == 2.8 && $valorMin == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }elseif ($row == 5.2 && $valorMax != 0){
        $pdf->Cell(14,5, utf8_decode($valorMax),1,0,'C');       
    }elseif ($row == 5.2 && $valorMax == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }else{
        $pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    }
    $y = $y + 5;
}

$pdf->Ln();
$pdf->SetXY(76,$y);
$pdf->Cell(14,5, utf8_decode('Total'),1,0,'C');
$pdf->Cell(14,5, utf8_decode($sum),1,0,'C');

// ------ Len
$pdf->Ln();
$pdf->SetXY(109,35);
$pdf->SetFillColor(218,245,207);
$pdf->Rect(109, 35, 28, 5, 'F');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(14,5, utf8_decode('Len'),1,0,'C');
$pdf->Cell(14,5, utf8_decode('Bal'),1,0,'C');

if ($TypHVI == 'O'){
    if($Crp != ""){
        $consulta = 'SELECT Round(Len,2) as Lens, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Lens'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Len,2) as Lens, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Lens'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}else{
    if($Crp != ""){
        $consulta = 'SELECT Round(Len_Mod,2) as Lens, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Lens'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Len_Mod,2) as Lens, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Lens'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}

$y=40;
$valorMin = 0;
$valorMax = 0;
foreach ($TLen as $row) {
    $valorMin = 0;
    $valorMax = 0;
    $pdf->Ln();
    $pdf->SetXY(109,$y);
    $pdf->SetFont('Arial','B',9);
    if ($row == 0.98){
        $pdf->Cell(14,5, utf8_decode($row."-"),1,0,'C');
    }elseif ($row == 1.22){
        $pdf->Cell(14,5, utf8_decode($row."+"),1,0,'C');
    }else{
        $pdf->Cell(14,5, utf8_decode($row),1,0,'C');
    }

    foreach ($data as $dat){
        if ($dat['Lens'] <= 0.98){
            $valorMin = $valorMin + $dat['Bal']; //$dat['Lens'] = 0.98;
        }elseif ($dat['Lens'] >= 1.22){
            $valorMax = $valorMax + $dat['Bal']; //$dat['Lens'] = 1.22;
        }elseif ($dat['Lens'] == $row){
            $valor = $dat['Bal'];
            break;
        }else{ 
            $valor = '';
        }
    }

    if ($row == 0.98 && $valorMin != 0){
        $pdf->Cell(14,5, utf8_decode($valorMin),1,0,'C');       
    }elseif ($row == 0.98 && $valorMin == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }elseif ($row == 1.22 && $valorMax != 0){
        $pdf->Cell(14,5, utf8_decode($valorMax),1,0,'C');       
    }elseif ($row == 1.22 && $valorMax == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }else{
        $pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    }
    $y = $y + 5;
}

$pdf->Ln();
$pdf->SetXY(109,$y);
$pdf->Cell(14,5, utf8_decode('Total'),1,0,'C');
$pdf->Cell(14,5, utf8_decode($sum),1,0,'C');

// ------ Str
$pdf->Ln();
$pdf->SetXY(142,35);
$pdf->SetFillColor(218,245,207);
$pdf->Rect(142, 35, 28, 5, 'F');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(14,5, utf8_decode('Str'),1,0,'C');
$pdf->Cell(14,5, utf8_decode('Bal'),1,0,'C');

if ($TypHVI == 'O'){
    if ($Crp != ""){
        $consulta = 'SELECT Round(Str,0) as Strs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Strs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Str,0) as Strs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Strs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}else{
    if ($Crp != ""){
        $consulta = 'SELECT Round(Str_Mod,0) as Strs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Strs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Str_Mod,0) as Strs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Strs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}

$y=40;
$valorMin = 0;
$valorMax = 0;
foreach ($TStr as $row) {
    $valorMin = 0;
    $valorMax = 0;
    $pdf->Ln();
    $pdf->SetXY(142,$y);
    $pdf->SetFont('Arial','B',9);
    if ($row == 16){
        $pdf->Cell(14,5, utf8_decode($row."-"),1,0,'C');
    }elseif ($row == 40){
        $pdf->Cell(14,5, utf8_decode($row."+"),1,0,'C');
    }else{
        $pdf->Cell(14,5, utf8_decode($row),1,0,'C');
    }

    foreach ($data as $dat){
        if ($dat['Strs'] <= 16){
            $valorMin = $valorMin + $dat['Bal']; //$dat['Strs'] = 16;
        }elseif ($dat['Strs'] >= 40){
            $valorMax = $valorMax + $dat['Bal']; //$dat['Strs'] = 40;
        }elseif ($dat['Strs'] == $row){
            $valor = $dat['Bal'];
            break;
        }else{ 
            $valor = '';
        }
    }

    if ($row == 16 && $valorMin != 0){
        $pdf->Cell(14,5, utf8_decode($valorMin),1,0,'C');       
    }elseif ($row == 16 && $valorMin == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }elseif ($row == 40 && $valorMax != 0){
        $pdf->Cell(14,5, utf8_decode($valorMax),1,0,'C');       
    }elseif ($row == 40 && $valorMax == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }else{
        $pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    }
    //$pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    $y = $y + 5;
}

$pdf->Ln();
$pdf->SetXY(142,$y);
$pdf->Cell(14,5, utf8_decode('Total'),1,0,'C');
$pdf->Cell(14,5, utf8_decode($sum),1,0,'C');

// ------ Unf
$pdf->Ln();
$pdf->SetXY(175,35);
$pdf->SetFillColor(218,245,207);
$pdf->Rect(175, 35, 28, 5, 'F');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(14,5, utf8_decode('Unf'),1,0,'C');
$pdf->Cell(14,5, utf8_decode('Bal'),1,0,'C');

if ($TypHVI == 'O'){
    if ($Crp != ""){
        $consulta = 'SELECT Round(Unf,0) as Unfs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Unfs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Unf,0) as Unfs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Unfs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}else{
    if ($Crp != ""){
        $consulta = 'SELECT Round(Unf_Mod,0) as Unfs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Unfs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Unf_Mod,0) as Unfs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Unfs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}

$y=40;
$valorMin = 0;
$valorMax = 0;
foreach ($TUnf as $row) {
    $valorMin = 0;
    $valorMax = 0;
    $pdf->Ln();
    $pdf->SetXY(175,$y);
    $pdf->SetFont('Arial','B',9);
    if($row == 68){
        $pdf->Cell(14,5, utf8_decode($row."-"),1,0,'C');
    }elseif($row == 92){
        $pdf->Cell(14,5, utf8_decode($row."+"),1,0,'C');
    }else{
        $pdf->Cell(14,5, utf8_decode($row),1,0,'C');
    }

    foreach ($data as $dat){
        if ($dat['Unfs'] <= 68){
            $valorMin = $valorMin + $dat['Bal']; //$dat['Unfs'] = 68;
        }elseif ($dat['Unfs'] >= 92){
            $valorMax = $valorMax + $dat['Bal']; //$dat['Unfs'] = 92;
        }elseif ($dat['Unfs'] == $row){
            $valor = $dat['Bal'];
            break;
        }else{ 
            $valor = '';
        }
    }

    if ($row == 68 && $valorMin != 0){
        $pdf->Cell(14,5, utf8_decode($valorMin),1,0,'C');       
    }elseif ($row == 68 && $valorMin == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }elseif ($row == 92 && $valorMax != 0){
        $pdf->Cell(14,5, utf8_decode($valorMax),1,0,'C');       
    }elseif ($row == 92 && $valorMax == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }else{
        $pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    }
    //$pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    $y = $y + 5;
}

$pdf->Ln();
$pdf->SetXY(175,$y);
$pdf->Cell(14,5, utf8_decode('Total'),1,0,'C');
$pdf->Cell(14,5, utf8_decode($sum),1,0,'C');

// ------ Rd
$pdf->Ln();
$pdf->SetXY(208,35);
$pdf->SetFillColor(218,245,207);
$pdf->Rect(208, 35, 28, 5, 'F');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(14,5, utf8_decode('Rd'),1,0,'C');
$pdf->Cell(14,5, utf8_decode('Bal'),1,0,'C');

if ($TypHVI == 'O'){
    if ($Crp != ""){
        $consulta = 'SELECT Round(Rd,0) as Rds, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Rds'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Rd,0) as Rds, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Rds'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}else{
    if ($Crp != ""){
        $consulta = 'SELECT Round(Rd_Mod,0) as Rds, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By Rds'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(Rd_Mod,0) as Rds, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By Rds'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}

$y=40;
$valorMin = 0;
$valorMax = 0;
foreach ($TRd as $row) {
    $valorMin = 0;
    $valorMax = 0;
    $pdf->Ln();
    $pdf->SetXY(208,$y);
    $pdf->SetFont('Arial','B',9);
    if ($row == 66){
        $pdf->Cell(14,5, utf8_decode($row."-"),1,0,'C');
    }elseif ($row == 90){
        $pdf->Cell(14,5, utf8_decode($row."+"),1,0,'C');
    }else{
        $pdf->Cell(14,5, utf8_decode($row),1,0,'C');
    }

    foreach ($data as $dat){
        if ($dat['Rds'] <= 66){
            $valorMin = $valorMin + $dat['Bal']; //$dat['Rds'] = 66;
        }elseif ($dat['Rds'] >= 90){
            $valorMax = $valorMax + $dat['Bal']; //$dat['Rds'] = 90;
        }elseif ($dat['Rds'] == $row){
            $valor = $dat['Bal'];
            break;
        }else{ 
            $valor = '';
        }
    }

    if ($row == 66 && $valorMin != 0){
        $pdf->Cell(14,5, utf8_decode($valorMin),1,0,'C');       
    }elseif ($row == 66 && $valorMin == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }elseif ($row == 90 && $valorMax != 0){
        $pdf->Cell(14,5, utf8_decode($valorMax),1,0,'C');       
    }elseif ($row == 90 && $valorMax == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }else{
        $pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    }
    //$pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    $y = $y + 5;
}

$pdf->Ln();
$pdf->SetXY(208,$y);
$pdf->Cell(14,5, utf8_decode('Total'),1,0,'C');
$pdf->Cell(14,5, utf8_decode($sum),1,0,'C');

// ------ b
$pdf->Ln();
$pdf->SetXY(241,35);
$pdf->SetFillColor(218,245,207);
$pdf->Rect(241, 35, 28, 5, 'F');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(14,5, utf8_decode('b'),1,0,'C');
$pdf->Cell(14,5, utf8_decode('Bal'),1,0,'C');

if ($TypHVI == 'O'){
    if ($Crp != ""){
        $consulta = 'SELECT Round(b,1) as bs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By bs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(b,1) as bs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By bs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}else{
    if ($Crp != ""){
        $consulta = 'SELECT Round(b_Mod,1) as bs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' Group By bs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }else{
        $consulta = 'SELECT Round(b_Mod,1) as bs, count(Bal) as Bal FROM Bales WHERE Lot IN ('.$array.') Group By bs'; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchALL(PDO::FETCH_ASSOC);
    }
}

$y=40;
$valorMin = 0;
$valorMax = 0;
foreach ($Tb as $row) {
    $valorMin = 0;
    $valorMax = 0;
    $pdf->Ln();
    $pdf->SetXY(241,$y);
    $pdf->SetFont('Arial','B',9);
    if ($row == 6.8){
        $pdf->Cell(14,5, utf8_decode($row."-"),1,0,'C');
    }elseif ($row == 9.2){
        $pdf->Cell(14,5, utf8_decode($row."+"),1,0,'C');
    }else{
        $pdf->Cell(14,5, utf8_decode($row),1,0,'C');
    }

    foreach ($data as $dat){
        if ($dat['bs'] <= 6.8){
            $valorMin = $valorMin + $dat['Bal']; //$dat['bs'] = 6.8;
        }elseif ($dat['bs'] >= 9.2){
            $valorMax = $valorMax + $dat['Bal']; //$dat['bs'] = 9.2;
        }elseif ($dat['bs'] == $row){
            $valor = $dat['Bal'];
            break;
        }else{ 
            $valor = '';
        }
    }

    if ($row == 6.8 && $valorMin != 0){
        $pdf->Cell(14,5, utf8_decode($valorMin),1,0,'C');       
    }elseif ($row == 6.8 && $valorMin == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }elseif ($row == 9.2 && $valorMax != 0){
        $pdf->Cell(14,5, utf8_decode($valorMax),1,0,'C');       
    }elseif ($row == 9.2 && $valorMax == 0){
        $pdf->Cell(14,5, utf8_decode(""),1,0,'C');       
    }else{
        $pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    }
    //$pdf->Cell(14,5, utf8_decode($valor),1,0,'C');
    $y = $y + 5;
}

$pdf->Ln();
$pdf->SetXY(241,$y);
$pdf->Cell(14,5, utf8_decode('Total'),1,0,'C');
$pdf->Cell(14,5, utf8_decode($sum),1,0,'C');

$pdf->Output('I', "RECAP ".$Cli." ".date('d-M-Y').".pdf");

$conexion=null;

?>