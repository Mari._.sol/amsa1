<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

$Dat = (isset($_POST['Dat'])) ? $_POST['Dat'] : '';
$DO = (isset($_POST['DO'])) ? $_POST['DO'] : '';
$Rqstr = (isset($_POST['Rqstr'])) ? $_POST['Rqstr'] : '';
$Lot = (isset($_POST['Lot'])) ? $_POST['Lot'] : '';
$Qty = (isset($_POST['Qty'])) ? $_POST['Qty'] : '';
$Typ = (isset($_POST['Typ'])) ? $_POST['Typ'] : '';
$Rsn = (isset($_POST['Rsn'])) ? $_POST['Rsn'] : '';
$Cmt = (isset($_POST['Cmt'])) ? $_POST['Cmt'] : '';
$FullLot = (isset($_POST['FullLot'])) ? $_POST['FullLot'] : '';
$DODev = (isset($_POST['DODev'])) ? $_POST['DODev'] : '';

$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$ID = (isset($_POST['ID'])) ? $_POST['ID'] : '';


switch($opcion){
    case 1:
        $consulta = "INSERT INTO Stg (Date, DO, Requester, Bales, Lot, TypeStg, Rsn, Comments, FullLot, DODev, DevID) VALUES('$Dat', '$DO', '$Rqstr', '$Qty', '$Lot', '$Typ', '$Rsn', '$Cmt', '$FullLot', '$DODev', '')";			
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        
        if ($Typ == 'REJECTED' and $FullLot == 'YES'){
            $consulta = "SELECT InReg FROM DOrds WHERE DOrds.DOrd='$DO'"; 
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $dataReg=$resultado->fetch();
            $Loc = $dataReg['InReg'];
            
            $consulta = "SELECT * FROM Lots WHERE Lot='$Lot' and Lots.DOrd='$DO'"; 
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
                    foreach($data as $row){
                        $Crop = $row['Crop'];
                        $Lot = $row['Lot'];
                        $Reg = $row['Reg'];
                        $GinID = $row['GinID'];
                        $Loc = $dataReg['InReg'];
                        $Qty = $row['Qty'];
                        $LiqWgh = $row['LiqWgh'];
                        $Qlty = $row['Qlty'];
                        $Cmt = $row['Cmt'];
                        $AssgTo = $row['AssgTo'];
                        $AsgmDate = $row['AsgmDate'];
                        $Status = $row['Status'];
                        $TotalQty = $row['TotalQty'];
                        $consulta = "INSERT INTO Lots (Crop, Lot, Reg, GinID, Location, Qty, Unt, LiqWgh, Qlty, Cmt, Recap, CliID, AsgmDate, DOrd, SchDate, TrkID, InvID, Status,TotalQty) VALUES('$Crop', '$Lot', '$Reg', '$GinID', '$Loc', '$Qty', '$Qty', '$LiqWgh', '$Qlty', '$Cmt', '', '', '$AsgmDate', '$DODev', '', '', '', '$Status', '$TotalQty')";			
                        $resultado = $conexion->prepare($consulta);
                        $resultado->execute();
                    }
        }else if ($Typ == 'REJECTED' and $FullLot == 'NO'){
            $consulta = "SELECT InReg FROM DOrds WHERE DOrds.DOrd='$DO'"; 
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $dataReg=$resultado->fetch();
            $Loc = $dataReg['InReg'];
            
            $consulta = "SELECT * FROM Lots WHERE Lot='$Lot' and DOrd='$DO'"; //and IDGin=GinID and Region.IDReg=Reg and IDGin=Location"; 
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
                    foreach($data as $row){
                        $Crop = $row['Crop'];
                        $Lot = $row['Lot'];
                        $Reg = $row['Reg'];
                        $GinID = $row['GinID'];
                        $Loc = $dataReg['InReg'];
                        $QtyLots = $row['Qty'];
                        $LiqWgh = $row['LiqWgh'];
                        $Qlty = $row['Qlty'];
                        $Cmt = $row['Cmt'];
                        $AssgTo = $row['AssgTo'];
                        $AsgmDate = $row['AsgmDate'];
                        $Status = $row['Status'];
                        $TotalQty = $row['TotalQty'];
                        $NewWgh = $Qty*$LiqWgh/$QtyLots;
                        $consulta = "INSERT INTO Lots (Crop, Lot, Reg, GinID, Location, Qty, Unt, LiqWgh, Qlty, Cmt, Recap, CliID, AsgmDate, DOrd, SchDate, TrkID, InvID, Status, TotalQty) VALUES('$Crop', '$Lot', '$Reg', '$GinID', '$Loc', '$Qty', '$Qty', '$NewWgh', '$Qlty', '$Cmt', '', '', '$AsgmDate', '$DODev', '', '', '', '$Status', '$TotalQty')";			
                        $resultado = $conexion->prepare($consulta);
                        $resultado->execute();
                    }
        }
        
        $consulta = "SELECT ID, Date,
        (SELECT Year FROM DOrds WHERE DO=Dord) as Year,
        (SELECT Typ FROM DOrds WHERE DO=Dord) as Typ,
        (SELECT Gin FROM DOrds WHERE DO=Dord) as Gin,
        (SELECT OutPlc FROM DOrds WHERE DO=Dord) as OutPlc,
        (SELECT InReg FROM DOrds WHERE DO=Dord) as InReg,
        (SELECT Ctc FROM DOrds WHERE DO=Dord) as Ctc,
        (SELECT InPlc FROM DOrds WHERE DO=Dord) as InPlc, DO, Requester, Bales, Lot, TypeStg, Comments, FullLot, DODev, DevID FROM Stg";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);       
        break;    
    case 2:        
        $consulta = "UPDATE Stg SET Date='$Dat', DO='$DO', Requester='$Rqstr', Bales='$Qty', Lot='$Lot', TypeStg='$Typ', Rsn='$Rsn', Comments='$Cmt', FullLot='$FullLot', DODev='$DODev' WHERE ID='$ID' ";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        
        $consulta = "SELECT ID, Date,
        (SELECT Year FROM DOrds WHERE DO=Dord) as Year,
        (SELECT Typ FROM DOrds WHERE DO=Dord) as Typ,
        (SELECT Gin FROM DOrds WHERE DO=Dord) as Gin,
        (SELECT OutPlc FROM DOrds WHERE DO=Dord) as OutPlc,
        (SELECT InReg FROM DOrds WHERE DO=Dord) as InReg,
        (SELECT Ctc FROM DOrds WHERE DO=Dord) as Ctc,
        (SELECT InPlc FROM DOrds WHERE DO=Dord) as InPlc, DO, Requester, Bales, Lot, TypeStg, Comments, FullLot, DODev, DevID FROM Stg";       
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 3:        
        $consulta = "UPDATE DOrds Set Qty='0', Unt='0' WHERE DOrd='$DOrd'"; //"DELETE FROM Lots WHERE LotID='$LotID' ";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();                           
        break;
    case 4:    
        $consulta = "SELECT ID, Date,
        (SELECT Year FROM DOrds WHERE DO=Dord) as Year,
        (SELECT Typ FROM DOrds WHERE DO=Dord) as Typ,
        (SELECT RegNam FROM DOrds, Region WHERE DO=Dord and OutPlc = IDReg) as OutPlc,
        (SELECT RegNam FROM DOrds, Region WHERE DO=Dord and InReg = IDReg) as InReg,
        (SELECT GinName FROM DOrds, Gines WHERE DO=Dord and Gin = IDGin) as Gin,
        (SELECT Ctc FROM DOrds WHERE DO=Dord) as Ctc,
        (SELECT RegNam FROM DOrds, Region WHERE DO=Dord and InPlc = IDReg) as InPlc, DO, Requester, Bales, Lot, TypeStg, Rsn, Comments, FullLot, DODev, DevID FROM Stg"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 5:    
        $consulta = "SELECT User FROM Users WHERE Authorize=1 ORDER BY User ASC"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 6:    
        $consulta = "SELECT Typ,
        Qty + (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE Stg.DO=DOrds.Dord) as Qty,
        (SELECT RegNam FROM Region WHERE IDReg = OutPlc) as OutPlc,
        (SELECT GinName FROM Gines WHERE IDGin = Gin) as Gin,
        (SELECT RegNam FROM Region WHERE IDReg = InReg) as InReg,
        (SELECT Cli FROM Clients WHERE Clients.CliID = InPlc) as CliID
        FROM DOrds 
        WHERE DOrd='$DO'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 7:    
        $consulta = "SELECT Lot FROM Lots WHERE DOrd='$DO'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
}

print json_encode($data, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX
$conexion=null;