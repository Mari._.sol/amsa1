<?php

ini_set('memory_limit', '512M');
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

function lowercase($element)
{
    return "'".$element."'";
}

$LotU = (isset($_POST['LotU'])) ? $_POST['LotU'] : ''; 
$CrpU = (isset($_POST['CrpU'])) ? $_POST['CrpU'] : '';
$Region = (isset($_POST['Reg'])) ? $_POST['Reg'] : '';

//Valores para definir arreglo de la consulta
$GrdHVI = (isset($_POST['GrdHVI'])) ? $_POST['GrdHVI'] : '';
$TaHVI = (isset($_POST['TaHVI'])) ? $_POST['TaHVI'] : '';
$TcHVI = (isset($_POST['TcHVI'])) ? $_POST['TcHVI'] : '';
$PcHVI = (isset($_POST['PcHVI'])) ? $_POST['PcHVI'] : '';
$LenHVI = (isset($_POST['LenHVI'])) ? $_POST['LenHVI'] : '';
$UiHVI = (isset($_POST['UiHVI'])) ? $_POST['UiHVI'] : '';
$SfiHVI = (isset($_POST['SfiHVI'])) ? $_POST['SfiHVI'] : '';
$StrHVI = (isset($_POST['StrHVI'])) ? $_POST['StrHVI'] : '';
$ElgHVI = (isset($_POST['ElgHVI'])) ? $_POST['ElgHVI'] : '';
$MicHVI = (isset($_POST['MicHVI'])) ? $_POST['MicHVI'] : '';
$RDHVI = (isset($_POST['RDHVI'])) ? $_POST['RDHVI'] : '';
$MatHVI = (isset($_POST['MatHVI'])) ? $_POST['MatHVI'] : '';
$bHVI = (isset($_POST['bHVI'])) ? $_POST['bHVI'] : '';
$ColHVI = (isset($_POST['ColHVI'])) ? $_POST['ColHVI'] : '';
$MstHVI = (isset($_POST['MstHVI'])) ? $_POST['MstHVI'] : '';

//Valores recibidos de los inputs
$GrdMinO = (isset($_POST['GrdMinO'])) ? $_POST['GrdMinO'] : '';
$GrdMaxO = (isset($_POST['GrdMaxO'])) ? $_POST['GrdMaxO'] : '';
$GrdMinM = (isset($_POST['GrdMinM'])) ? $_POST['GrdMinM'] : '';
$GrdMaxM = (isset($_POST['GrdMaxM'])) ? $_POST['GrdMaxM'] : '';
$GrdRange = (isset($_POST['GrdRange'])) ? $_POST['GrdRange'] : '';

$Col2 = (isset($_POST['Col2'])) ? $_POST['Col2'] : '';
$Col21 = (isset($_POST['Col21'])) ? $_POST['Col21'] : '';

$TaMinO = (isset($_POST['TaMinO'])) ? $_POST['TaMinO'] : '';
$TaMaxO = (isset($_POST['TaMaxO'])) ? $_POST['TaMaxO'] : '';
$TaMinM = (isset($_POST['TaMinM'])) ? $_POST['TaMinM'] : '';
$TaMaxM = (isset($_POST['TaMaxM'])) ? $_POST['TaMaxM'] : '';
$TaRange = (isset($_POST['TaRange'])) ? $_POST['TaRange'] : '';

$TcMinO = (isset($_POST['TcMinO'])) ? $_POST['TcMinO'] : '';
$TcMaxO = (isset($_POST['TcMaxO'])) ? $_POST['TcMaxO'] : '';
$TcMinM = (isset($_POST['TcMinM'])) ? $_POST['TcMinM'] : '';
$TcMaxM = (isset($_POST['TcMaxM'])) ? $_POST['TcMaxM'] : '';
$TcRange = (isset($_POST['TcRange'])) ? $_POST['TcRange'] : '';

$PcMinO = (isset($_POST['PcMinO'])) ? $_POST['PcMinO'] : '';
$PcMaxO = (isset($_POST['PcMaxO'])) ? $_POST['PcMaxO'] : '';
$PcMinM = (isset($_POST['PcMinM'])) ? $_POST['PcMinM'] : '';
$PcMaxM = (isset($_POST['PcMaxM'])) ? $_POST['PcMaxM'] : '';
$PcRange = (isset($_POST['PcRange'])) ? $_POST['PcRange'] : '';

$LenMinO = (isset($_POST['LenMinO'])) ? $_POST['LenMinO'] : '';
$LenMaxO = (isset($_POST['LenMaxO'])) ? $_POST['LenMaxO'] : '';
$LenMinM = (isset($_POST['LenMinM'])) ? $_POST['LenMinM'] : '';
$LenMaxM = (isset($_POST['LenMaxM'])) ? $_POST['LenMaxM'] : '';
$LenRange = (isset($_POST['LenRange'])) ? $_POST['LenRange'] : '';

$UiMinO = (isset($_POST['UiMinO'])) ? $_POST['UiMinO'] : '';
$UiMaxO = (isset($_POST['UiMaxO'])) ? $_POST['UiMaxO'] : '';
$UiMinM = (isset($_POST['UiMinM'])) ? $_POST['UiMinM'] : '';
$UiMaxM = (isset($_POST['UiMaxM'])) ? $_POST['UiMaxM'] : '';
$UiRange = (isset($_POST['UiRange'])) ? $_POST['UiRange'] : '';

$SfiMinO = (isset($_POST['SfiMinO'])) ? $_POST['SfiMinO'] : '';
$SfiMaxO = (isset($_POST['SfiMaxO'])) ? $_POST['SfiMaxO'] : '';
$SfiMinM = (isset($_POST['SfiMinM'])) ? $_POST['SfiMinM'] : '';
$SfiMaxM = (isset($_POST['SfiMaxM'])) ? $_POST['SfiMaxM'] : '';
$SfiRange = (isset($_POST['SfiRange'])) ? $_POST['SfiRange'] : '';

$StrMinO = (isset($_POST['StrMinO'])) ? $_POST['StrMinO'] : '';
$StrMaxO = (isset($_POST['StrMaxO'])) ? $_POST['StrMaxO'] : '';
$StrMinM = (isset($_POST['StrMinM'])) ? $_POST['StrMinM'] : '';
$StrMaxM = (isset($_POST['StrMaxM'])) ? $_POST['StrMaxM'] : '';
$StrRange = (isset($_POST['StrRange'])) ? $_POST['StrRange'] : '';

$ElgMinO = (isset($_POST['ElgMinO'])) ? $_POST['ElgMinO'] : '';
$ElgMaxO = (isset($_POST['ElgMaxO'])) ? $_POST['ElgMaxO'] : '';
$ElgMinM = (isset($_POST['ElgMinM'])) ? $_POST['ElgMinM'] : '';
$ElgMaxM = (isset($_POST['ElgMaxM'])) ? $_POST['ElgMaxM'] : '';
$ElgRange = (isset($_POST['ElgRange'])) ? $_POST['ElgRange'] : '';

$MicMinO = (isset($_POST['MicMinO'])) ? $_POST['MicMinO'] : '';
$MicMaxO = (isset($_POST['MicMaxO'])) ? $_POST['MicMaxO'] : '';
$MicMinM = (isset($_POST['MicMinM'])) ? $_POST['MicMinM'] : '';
$MicMaxM = (isset($_POST['MicMaxM'])) ? $_POST['MicMaxM'] : '';
$MicRange = (isset($_POST['MicRange'])) ? $_POST['MicRange'] : '';

$RdMinO = (isset($_POST['RdMinO'])) ? $_POST['RdMinO'] : '';
$RdMaxO = (isset($_POST['RdMaxO'])) ? $_POST['RdMaxO'] : '';
$RdMinM = (isset($_POST['RdMinM'])) ? $_POST['RdMinM'] : '';
$RdMaxM = (isset($_POST['RdMaxM'])) ? $_POST['RdMaxM'] : '';
$RdRange = (isset($_POST['RdRange'])) ? $_POST['RdRange'] : '';

$MatMinO = (isset($_POST['MatMinO'])) ? $_POST['MatMinO'] : '';
$MatMaxO = (isset($_POST['MatMaxO'])) ? $_POST['MatMaxO'] : '';
$MatMinM = (isset($_POST['MatMinM'])) ? $_POST['MatMinM'] : '';
$MatMaxM = (isset($_POST['MatMaxM'])) ? $_POST['MatMaxM'] : '';
$MatRange = (isset($_POST['MatRange'])) ? $_POST['MatRange'] : '';

$bMinO = (isset($_POST['bMinO'])) ? $_POST['bMinO'] : '';
$bMaxO = (isset($_POST['BMaxO'])) ? $_POST['bMaxO'] : '';
$bMinM = (isset($_POST['bMinM'])) ? $_POST['bMinM'] : '';
$bMaxM = (isset($_POST['bMaxM'])) ? $_POST['bMaxM'] : '';
$bRange = (isset($_POST['bRange'])) ? $_POST['bRange'] : '';

$MstMinO = (isset($_POST['MstMinO'])) ? $_POST['MstMinO'] : '';
$MstMaxO = (isset($_POST['MstMaxO'])) ? $_POST['MstMaxO'] : '';
$MstMinM = (isset($_POST['MstMinM'])) ? $_POST['MstMinM'] : '';
$MstMaxM = (isset($_POST['MstMaxM'])) ? $_POST['MstMaxM'] : '';
$MstRange = (isset($_POST['MstRange'])) ? $_POST['MstRange'] : '';

/*$Lot = (isset($_POST['Lot'])) ? $_POST['Lot'] : '';
$Qty = (isset($_POST['Qty'])) ? $_POST['Qty'] : '';
$Reg = (isset($_POST['Reg'])) ? $_POST['Reg'] : '';
$GinID = (isset($_POST['GinID'])) ? $_POST['GinID'] : '';
$Loc = (isset($_POST['Loc'])) ? $_POST['Loc'] : '';
$LiqWgh = (isset($_POST['LiqWgh'])) ? $_POST['LiqWgh'] : '';
$Qlty = (isset($_POST['Qlty'])) ? $_POST['Qlty'] : '';
$Cmt = (isset($_POST['Cmt'])) ? $_POST['Cmt'] : '';
$Recap = (isset($_POST['Recap'])) ? $_POST['Recap'] : '';
$Cli = (isset($_POST['Cli'])) ? $_POST['Cli'] : '';
$AssgTo = (isset($_POST['AssgTo'])) ? $_POST['AssgTo'] : '';
$AsgmDate = (isset($_POST['AsgmDate'])) ? $_POST['AsgmDate'] : '';
$DOrd = (isset($_POST['DOrd'])) ? $_POST['DOrd'] : '';
$TrkID = (isset($_POST['TrkID'])) ? $_POST['TrkID'] : '';
$Status = (isset($_POST['Status'])) ? $_POST['Status'] : '';
$SchDate = (isset($_POST['SchDate'])) ? $_POST['SchDate'] : '';*/

$idReg = (isset($_POST['idReg'])) ? $_POST['idReg'] : '';
$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$LotID = (isset($_POST['LotID'])) ? $_POST['LotID'] : '';

//Variables filtros
session_start();
$filterSes = $_SESSION['Filters'];

$DO = $filterSes['DO'];
$Reg = $filterSes['Reg'];
$Gin = $filterSes['Gin'];
$Lot = $filterSes['Lot'];
$Bale = $filterSes['Bale'];
$Crp = $filterSes['Crp'];
$Status = $filterSes['Status'];


switch($opcion){
    case 1:
        $consulta = "SELECT IDReg FROM Region WHERE RegNam='$Reg'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataR=$resultado->fetch();
        $Reg = $dataR['IDReg'];
        
        $consulta = "SELECT IDGin FROM Gines WHERE GinName='$GinID'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataGin=$resultado->fetch();
        $GinID = $dataGin['IDGin'];
        
        $consulta = "SELECT IDReg FROM Region WHERE RegNam='$Loc'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataLoc=$resultado->fetch();
        $Loc = $dataLoc['IDReg'];
        
        $consulta = "SELECT CliID FROM Clients WHERE Cli='$Cli'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataCli=$resultado->fetch();
        $Cli = $dataCli['CliID'];
        
        $consulta = "INSERT INTO Lots (Lot, Reg, GinID, Location, Qty, Unt, LiqWgh, Qlty, Cmt, Recap, CliID, AsgmDate, DOrd, SchDate, TrkID, InvID, Status) VALUES ('$Lot', '$Reg', '$GinID', '$Loc', '$Qty', '$Qty', '$LiqWgh', '$Qlty', '$Cmt', '$Recap', '$Cli', '$AsgmDate', '$DOrd', '$SchDate', '', '', '$Status');";			
        $resultado = $conexion->prepare($consulta);
        $resultado->execute(); 
        
        $consulta = "SELECT LotID, Lot, Qty, Reg, GinID, Location, LiqWgh, DOrd, TrkID, Status FROM Lots ORDER BY LotID DESC LIMIT 1";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);       
        break;    
    case 2:
        $consulta = "SELECT IDReg FROM Region WHERE RegNam='$Reg'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataR=$resultado->fetch();
        $Reg = $dataR['IDReg'];
        
        $consulta = "SELECT IDGin FROM Gines WHERE GinName='$GinID'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataGin=$resultado->fetch();
        $GinID = $dataGin['IDGin'];
        
        $consulta = "SELECT IDReg FROM Region WHERE RegNam='$Loc'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataLoc=$resultado->fetch();
        $Loc = $dataLoc['IDReg'];
        
        $consulta = "SELECT CliID FROM Clients WHERE Cli='$Cli'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataCli=$resultado->fetch();
        $Cli = $dataCli['CliID'];
        
        $consulta = "UPDATE Lots SET Lot='$Lot', Reg='$Reg', GinID='$GinID', Location='$Loc', Qty='$Qty', Unt='$Qty', LiqWgh='$LiqWgh', Qlty='$Qlty', Cmt='$Cmt', Recap='$Recap', CliID='$Cli', AsgmDate='$AsgmDate', DOrd='$DOrd', SchDate='$SchDate', Status='$Status' WHERE LotID='$LotID';";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        
        $consulta = "SELECT LotID, Lot, Qty, Reg, GinID, Location, LiqWgh, DOrd, TrkID, Status FROM Lots WHERE LotID='$LotID' ";       
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 3:        
        /*$consulta = "SELECT * FROM Lots WHERE LotID='$LotID'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
                foreach($data as $row){
                    $Lot = $row['Lot'];
                    $Reg = $row['Reg'];
                    $GinID = $row['GinID'];
                    $Loc = $dataDO['InPlc'];
                    $Qty = $row['Qty'];
                    $LiqWgh = $row['LiqWgh'];
                    $Status = $row['Status'];
                    $consulta = "INSERT INTO Lots (Lot, Qty, Reg, GinID, Location, Unt, LiqWgh, DOrd, TrkID, InvID, Status) VALUES('$Lot', '$Qty', '$Reg', '$GinID', '$Loc',  '$Qty', '$LiqWgh', '', '', '', '$Status') ";			
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                }*/
        
        $consulta = "DELETE FROM Lots WHERE LotID='$LotID'";	
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();                           
        break;
    case 4:    
        //Busqueda global dependiendo los filtros
        if($DO == "" && $Reg == "" && $Gin == "" && $Lot == "" && $Bale == "" && $Crp == ""){
            $consulta = 'SELECT * FROM Bales ORDER BY Bal DESC LIMIT 0;';
            $data = 0;
        }
        if ($Status == "All"){
        //solo DO
        if($DO == "" && $Reg == "" && $Gin == "" && $Lot == "" && $Bale == "" && $Crp == ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales ORDER BY Bal DESC LIMIT 0;';
            $data = 0;
        }elseif ($DO != "" && $Reg == "" && $Gin == "" && $Lot == "" && $Bale == "" && $Crp == ""){
            $consulta = 'SELECT Crp, Bales.GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Bales.Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales, Lots WHERE Lots.DOrd = '.$DO.' and Lots.Lot = Bales.Lot;';

        //Combinacion por region    
        }elseif($Reg != "" && $Gin == "" && $Lot == "" && $Bale == "" && $Crp == ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID LIKE "%'.$Reg.'%"';

        }elseif($Reg != "" && $Gin != "" && $Lot == "" && $Bale == "" && $Crp == ""){
            if ($Reg == 99002){
              $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID = '.$Gin.'';
            }else{
              $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.'';
              }

        }elseif($Reg != "" && $Gin != "" && $Lot != "" && $Bale == "" && $Crp == ""){
            $array=array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.' and Lot IN ('.$array.')';
        
        }elseif($Reg != "" && $Gin != "" && $Lot != "" && $Bale == "" && $Crp != ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.' and Lot IN ('.$array.') and Crp = '.$Crp.'';

        }elseif($Reg != "" && $Gin == "" && $Lot == "" && $Bale == "" && $Crp != ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID LIKE "%'.$Reg.'%" and Crp = '.$Crp.'';
        
        }elseif($Reg != "" && $Gin == "" && $Lot != "" && $Bale == "" && $Crp == ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID LIKE "%'.$Reg.'%" and Lot IN ('.$array.')';

        }elseif($Reg != "" && $Gin != "" && $Lot == "" && $Bale == "" && $Crp != ""){
          if ($Reg == 99002){
                $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID = '.$Gin.' and Crp = '.$Crp.'';
              }else{
              $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.' and Crp = '.$Crp.'';
            }

        }elseif($Reg != "" && $Gin == "" && $Lot != "" && $Bale == "" && $Crp != ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID LIKE "%'.$Reg.'%" and Lot IN ('.$array.') and Crp ='.$Crp.'';
        
        }elseif($Reg != "" && $Gin == "" && $Lot == "" && $Bale != "" && $Crp == ""){
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID LIKE "%'.$Reg.'%" and Bal IN ('.$arrayB.');';

        }elseif($Reg != "" && $Gin == "" && $Lot == "" && $Bale != "" && $Crp != ""){
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID LIKE "%'.$Reg.'%" and Bal IN ('.$arrayB.') and Crp = '.$Crp.';';

        }elseif($Reg != "" && $Gin != "" && $Lot == "" && $Bale != "" && $Crp == ""){
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.' and Bal IN ('.$arrayB.');';
        
        }elseif($Reg != "" && $Gin == "" && $Lot != "" && $Bale != "" && $Crp == ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID LIKE "%'.$Reg.'%" and Lot IN ('.$array.') and Bal IN ('.$arrayB.');';

        }elseif($Reg != "" && $Gin != "" && $Lot != "" && $Bale != "" && $Crp == ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.' and Lot IN ('.$array.') and Bal IN ('.$arrayB.');';
        
        }elseif($Reg != "" && $Gin != "" && $Lot != "" && $Bale != "" && $Crp != ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.' and Lot IN ('.$array.') and Bal IN ('.$arrayB.') and Crp = '.$Crp.';';
        //Combinacion por Gin        
        }elseif($Reg == "" && $Gin != "" && $Lot == "" && $Bale == "" && $Crp == ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID = '.$Gin.';';
        
        }elseif($Reg == "" && $Gin != "" && $Lot != "" && $Bale == "" && $Crp == ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID = '.$Gin.' and Lot IN ('.$array.');';

        }elseif($Reg == "" && $Gin != "" && $Lot == "" && $Bale != "" && $Crp == ""){
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID = '.$Gin.' and Bal IN ('.$arrayB.');';
        
        }elseif($Reg == "" && $Gin != "" && $Lot == "" && $Bale == "" && $Crp != ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID = '.$Gin.' and Crp = '.$Crp.';';

        }elseif($Reg == "" && $Gin != "" && $Lot != "" && $Bale != "" && $Crp == ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID = '.$Gin.' and Lot IN ('.$array.') and Bal IN ('.$arrayB.');';

        }elseif($Reg == "" && $Gin != "" && $Lot != "" && $Bale == "" && $Crp != ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID = '.$Gin.' and Lot IN ('.$array.') and Crp = '.$Crp.';';
        
        }elseif($Reg == "" && $Gin != "" && $Lot == "" && $Bale != "" && $Crp != ""){
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID = '.$Gin.' and Bal IN ('.$arrayB.') and Crp = '.$Crp.';';

        }elseif($Reg == "" && $Gin != "" && $Lot != "" && $Bale != "" && $Crp != ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE GinID = '.$Gin.' and Lot IN ('.$array.') and Bal IN ('.$arrayB.') and Crp = '.$Crp.';';
        //Combinacion por Lot
        }elseif($Reg == "" && $Gin == "" && $Lot != "" && $Bale == "" && $Crp == ""){
            $array=array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot IN ('.$array.') ORDER BY Bal;';

        }elseif($Reg == "" && $Gin == "" && $Lot != "" && $Bale != "" && $Crp == ""){
            $array=array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB=array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot IN ('.$array.') and Bal IN ('.$arrayB.') ORDER BY Bal;';

        }elseif($Reg == "" && $Gin == "" && $Lot != "" && $Bale == "" && $Crp != ""){
            $array=array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot IN ('.$array.') and Crp = '.$Crp.' ORDER BY Bal;';

        }elseif($Reg == "" && $Gin == "" && $Lot != "" && $Bale != "" && $Crp != ""){
            $array=array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB=array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot IN ('.$array.') and Bal IN ('.$arrayB.') and Crp = '.$Crp.' ORDER BY Bal;';
        //Combinacion por Bale
        }elseif($Reg == "" && $Gin == "" && $Lot == "" && $Bale != "" && $Crp == ""){
            $arrayB=array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Bal IN ('.$arrayB.') ORDER BY Bal;';

        }elseif($Reg == "" && $Gin == "" && $Lot == "" && $Bale != "" && $Crp != ""){
            $arrayB=array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Bal IN ('.$arrayB.') and Crp = '.$Crp.' ORDER BY Bal;';

        }elseif($Reg == "" && $Gin == "" && $Lot == "" && $Bale == "" && $Crp != ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Crp = '.$Crp.';';

        }
        
        //buscar bales sin lote
        }elseif($Status == "SL"){
          if($DO == "" && $Reg == "" && $Gin == "" && $Lot == "" && $Bale == "" && $Crp == ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales ORDER BY Bal DESC LIMIT 0;';
            $data = 0;
        }elseif ($DO != "" && $Reg == "" && $Gin == "" && $Lot == "" && $Bale == "" && $Crp == ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales, DOrds, Lots WHERE Lot = "" and DOrds.DOrd = '.$DO.' and DOrds.DOrd = Lots.DOrd and Lots.Lot = Bales.Lot;';

        //Combinacion por region    
        }elseif($Reg != "" && $Gin == "" && $Lot == "" && $Bale == "" && $Crp == ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID LIKE "%'.$Reg.'%"';

        }elseif($Reg != "" && $Gin != "" && $Lot == "" && $Bale == "" && $Crp == ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.'';

        }elseif($Reg != "" && $Gin != "" && $Lot != "" && $Bale == "" && $Crp == ""){
            $array=array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.' ';
        
        }elseif($Reg != "" && $Gin != "" && $Lot != "" && $Bale == "" && $Crp != ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.'  and Crp = '.$Crp.'';

        }elseif($Reg != "" && $Gin == "" && $Lot == "" && $Bale == "" && $Crp != ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID LIKE "%'.$Reg.'%" and Crp = '.$Crp.'';
        
        }elseif($Reg != "" && $Gin == "" && $Lot != "" && $Bale == "" && $Crp == ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID LIKE "%'.$Reg.'%" ';

        }elseif($Reg != "" && $Gin != "" && $Lot == "" && $Bale == "" && $Crp != ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.' and Crp = '.$Crp.'';

        }elseif($Reg != "" && $Gin == "" && $Lot != "" && $Bale == "" && $Crp != ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID LIKE "%'.$Reg.'%"  and Crp ='.$Crp.'';
        
        }elseif($Reg != "" && $Gin == "" && $Lot == "" && $Bale != "" && $Crp == ""){
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID LIKE "%'.$Reg.'%" and Bal IN ('.$arrayB.');';

        }elseif($Reg != "" && $Gin == "" && $Lot == "" && $Bale != "" && $Crp != ""){
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID LIKE "%'.$Reg.'%" and Bal IN ('.$arrayB.') and Crp = '.$Crp.';';

        }elseif($Reg != "" && $Gin != "" && $Lot == "" && $Bale != "" && $Crp == ""){
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.' and Bal IN ('.$arrayB.');';
        
        }elseif($Reg != "" && $Gin == "" && $Lot != "" && $Bale != "" && $Crp == ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID LIKE "%'.$Reg.'%"  and Bal IN ('.$arrayB.');';

        }elseif($Reg != "" && $Gin != "" && $Lot != "" && $Bale != "" && $Crp == ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.'  and Bal IN ('.$arrayB.');';
        
        }elseif($Reg != "" && $Gin != "" && $Lot != "" && $Bale != "" && $Crp != ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.'  and Bal IN ('.$arrayB.') and Crp = '.$Crp.';';
        //Combinacion por Gin        
        }elseif($Reg == "" && $Gin != "" && $Lot == "" && $Bale == "" && $Crp == ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID = '.$Gin.';';
        
        }elseif($Reg == "" && $Gin != "" && $Lot != "" && $Bale == "" && $Crp == ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID = '.$Gin.' ;';

        }elseif($Reg == "" && $Gin != "" && $Lot == "" && $Bale != "" && $Crp == ""){
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID = '.$Gin.' and Bal IN ('.$arrayB.');';
        
        }elseif($Reg == "" && $Gin != "" && $Lot == "" && $Bale == "" && $Crp != ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID = '.$Gin.' and Crp = '.$Crp.';';

        }elseif($Reg == "" && $Gin != "" && $Lot != "" && $Bale != "" && $Crp == ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID = '.$Gin.'  and Bal IN ('.$arrayB.');';

        }elseif($Reg == "" && $Gin != "" && $Lot != "" && $Bale == "" && $Crp != ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID = '.$Gin.'  and Crp = '.$Crp.';';
        
        }elseif($Reg == "" && $Gin != "" && $Lot == "" && $Bale != "" && $Crp != ""){
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID = '.$Gin.' and Bal IN ('.$arrayB.') and Crp = '.$Crp.';';

        }elseif($Reg == "" && $Gin != "" && $Lot != "" && $Bale != "" && $Crp != ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and GinID = '.$Gin.'  and Bal IN ('.$arrayB.') and Crp = '.$Crp.';';
        //Combinacion por Lot
        }elseif($Reg == "" && $Gin == "" && $Lot != "" && $Bale == "" && $Crp == ""){
            $array=array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = ""  ORDER BY Bal;';

        }elseif($Reg == "" && $Gin == "" && $Lot != "" && $Bale != "" && $Crp == ""){
            $array=array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB=array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = ""  and Bal IN ('.$arrayB.') ORDER BY Bal;';

        }elseif($Reg == "" && $Gin == "" && $Lot != "" && $Bale == "" && $Crp != ""){
            $array=array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = ""  and Crp = '.$Crp.' ORDER BY Bal;';

        }elseif($Reg == "" && $Gin == "" && $Lot != "" && $Bale != "" && $Crp != ""){
            $array=array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB=array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = ""  and Bal IN ('.$arrayB.') and Crp = '.$Crp.' ORDER BY Bal;';
        //Combinacion por Bale
        }elseif($Reg == "" && $Gin == "" && $Lot == "" && $Bale != "" && $Crp == ""){
            $arrayB=array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and Bal IN ('.$arrayB.') ORDER BY Bal;';

        }elseif($Reg == "" && $Gin == "" && $Lot == "" && $Bale != "" && $Crp != ""){
            $arrayB=array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and Bal IN ('.$arrayB.') and Crp = '.$Crp.' ORDER BY Bal;';

        }elseif($Reg == "" && $Gin == "" && $Lot == "" && $Bale == "" && $Crp != ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot = "" and Crp = '.$Crp.';';
        }
        
        //Buscar solo pacas loteadas
        }elseif($Status == "CL"){
          //solo DO
        if($DO == "" && $Reg == "" && $Gin == "" && $Lot == "" && $Bale == "" && $Crp == ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales ORDER BY Bal DESC LIMIT 0;';
            $data = 0;
        }elseif ($DO != "" && $Reg == "" && $Gin == "" && $Lot == "" && $Bale == "" && $Crp == ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales, DOrds, Lots WHERE Lot != "" and DOrds.DOrd = '.$DO.' and DOrds.DOrd = Lots.DOrd and Lots.Lot = Bales.Lot;';

        //Combinacion por region    
        }elseif($Reg != "" && $Gin == "" && $Lot == "" && $Bale == "" && $Crp == ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID LIKE "%'.$Reg.'%"';

        }elseif($Reg != "" && $Gin != "" && $Lot == "" && $Bale == "" && $Crp == ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.'';

        }elseif($Reg != "" && $Gin != "" && $Lot != "" && $Bale == "" && $Crp == ""){
            $array=array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.' and Lot IN ('.$array.')';
        
        }elseif($Reg != "" && $Gin != "" && $Lot != "" && $Bale == "" && $Crp != ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.' and Lot IN ('.$array.') and Crp = '.$Crp.'';

        }elseif($Reg != "" && $Gin == "" && $Lot == "" && $Bale == "" && $Crp != ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID LIKE "%'.$Reg.'%" and Crp = '.$Crp.'';
        
        }elseif($Reg != "" && $Gin == "" && $Lot != "" && $Bale == "" && $Crp == ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID LIKE "%'.$Reg.'%" and Lot IN ('.$array.')';

        }elseif($Reg != "" && $Gin != "" && $Lot == "" && $Bale == "" && $Crp != ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.' and Crp = '.$Crp.'';

        }elseif($Reg != "" && $Gin == "" && $Lot != "" && $Bale == "" && $Crp != ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID LIKE "%'.$Reg.'%" and Lot IN ('.$array.') and Crp ='.$Crp.'';
        
        }elseif($Reg != "" && $Gin == "" && $Lot == "" && $Bale != "" && $Crp == ""){
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID LIKE "%'.$Reg.'%" and Bal IN ('.$arrayB.');';

        }elseif($Reg != "" && $Gin == "" && $Lot == "" && $Bale != "" && $Crp != ""){
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID LIKE "%'.$Reg.'%" and Bal IN ('.$arrayB.') and Crp = '.$Crp.';';

        }elseif($Reg != "" && $Gin != "" && $Lot == "" && $Bale != "" && $Crp == ""){
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.' and Bal IN ('.$arrayB.');';
        
        }elseif($Reg != "" && $Gin == "" && $Lot != "" && $Bale != "" && $Crp == ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID LIKE "%'.$Reg.'%" and Lot IN ('.$array.') and Bal IN ('.$arrayB.');';

        }elseif($Reg != "" && $Gin != "" && $Lot != "" && $Bale != "" && $Crp == ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.' and Lot IN ('.$array.') and Bal IN ('.$arrayB.');';
        
        }elseif($Reg != "" && $Gin != "" && $Lot != "" && $Bale != "" && $Crp != ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID LIKE "%'.$Reg.'%" and GinID = '.$Gin.' and Lot IN ('.$array.') and Bal IN ('.$arrayB.') and Crp = '.$Crp.';';
        //Combinacion por Gin        
        }elseif($Reg == "" && $Gin != "" && $Lot == "" && $Bale == "" && $Crp == ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID = '.$Gin.';';
        
        }elseif($Reg == "" && $Gin != "" && $Lot != "" && $Bale == "" && $Crp == ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID = '.$Gin.' and Lot IN ('.$array.');';

        }elseif($Reg == "" && $Gin != "" && $Lot == "" && $Bale != "" && $Crp == ""){
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID = '.$Gin.' and Bal IN ('.$arrayB.');';
        
        }elseif($Reg == "" && $Gin != "" && $Lot == "" && $Bale == "" && $Crp != ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID = '.$Gin.' and Crp = '.$Crp.';';

        }elseif($Reg == "" && $Gin != "" && $Lot != "" && $Bale != "" && $Crp == ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID = '.$Gin.' and Lot IN ('.$array.') and Bal IN ('.$arrayB.');';

        }elseif($Reg == "" && $Gin != "" && $Lot != "" && $Bale == "" && $Crp != ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID = '.$Gin.' and Lot IN ('.$array.') and Crp = '.$Crp.';';
        
        }elseif($Reg == "" && $Gin != "" && $Lot == "" && $Bale != "" && $Crp != ""){
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID = '.$Gin.' and Bal IN ('.$arrayB.') and Crp = '.$Crp.';';

        }elseif($Reg == "" && $Gin != "" && $Lot != "" && $Bale != "" && $Crp != ""){
            $array = array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB = array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and GinID = '.$Gin.' and Lot IN ('.$array.') and Bal IN ('.$arrayB.') and Crp = '.$Crp.';';
        //Combinacion por Lot
        }elseif($Reg == "" && $Gin == "" && $Lot != "" && $Bale == "" && $Crp == ""){
            $array=array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and Lot IN ('.$array.') ORDER BY Bal;';

        }elseif($Reg == "" && $Gin == "" && $Lot != "" && $Bale != "" && $Crp == ""){
            $array=array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB=array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and Lot IN ('.$array.') and Bal IN ('.$arrayB.') ORDER BY Bal;';

        }elseif($Reg == "" && $Gin == "" && $Lot != "" && $Bale == "" && $Crp != ""){
            $array=array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and Lot IN ('.$array.') and Crp = '.$Crp.' ORDER BY Bal;';

        }elseif($Reg == "" && $Gin == "" && $Lot != "" && $Bale != "" && $Crp != ""){
            $array=array_map('lowercase', explode(',', $Lot));
            $array = implode(",",$array);
            $arrayB=array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and Lot IN ('.$array.') and Bal IN ('.$arrayB.') and Crp = '.$Crp.' ORDER BY Bal;';
        //Combinacion por Bale
        }elseif($Reg == "" && $Gin == "" && $Lot == "" && $Bale != "" && $Crp == ""){
            $arrayB=array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and Bal IN ('.$arrayB.') ORDER BY Bal;';

        }elseif($Reg == "" && $Gin == "" && $Lot == "" && $Bale != "" && $Crp != ""){
            $arrayB=array_map('lowercase', explode(',', $Bale));
            $arrayB = implode(",",$arrayB);
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and Bal IN ('.$arrayB.') and Crp = '.$Crp.' ORDER BY Bal;';

        }elseif($Reg == "" && $Gin == "" && $Lot == "" && $Bale == "" && $Crp != ""){
            $consulta = 'SELECT Crp, GinID, Bal, Rcv, BuyIt, Grp, Round(Mic_Mod,1) as Mic_Mod, Round(Len_Mod,2)  as Len_Mod, Round(Str_Mod,1)  as Str_Mod, Round(Rd_Mod,1) as Rd_Mod, Round(b_Mod,1) as b_Mod, Tcnt_Mod, Round(Tar_Mod,2) as Tar_Mod, Col1_Mod, Col2_Mod, Tgrd_Mod, Round(Mst_Mod,2) as Mst_Mod, Round(Sfi_Mod,1) as Sfi_Mod, Round(Sci_Mod,1) as Sci_Mod, Round(Mat_Mod,2) as Mat_Mod, Round(Elg_Mod,1) as Elg_Mod, Round(Unf_Mod,1) as Unf_Mod, Cnt_Mod, Wgh, Lot, Round(Mic,1)  as Mic, Round(Len,2) as Len, Round(Str,1) as Str, Round(Rd,1) as Rd, Round(b,1) as b, Tcnt, Round(Tar,2) as Tar, Col1, Col2, Tgrd, Round(Mst,2) as Mst, Round(Sfi,1) as Sfi, Round(Sci,1) as Sci, Round(Mat,2) as Mat, Round(Elg,1) as Elg, Round(Unf,1) as Unf, Cnt FROM Bales WHERE Lot != "" and Crp = '.$Crp.';';
        }
        
        }
  
        if($consulta !=""){
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();        
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }
        break;
    case 5:
        $selectParametros = array($GrdHVI, $TaHVI, $TcHVI, $PcHVI, $LenHVI, $UiHVI, $SfiHVI, $StrHVI, $ElgHVI, $MicHVI, $MatHVI, $RDHVI, $bHVI, $ColHVI, $MstHVI); //arreglo de checkbox 1/0 dependiento si estan seleccionados
        $HVIParametros = array('Col1', 'Tar', 'Tgrd', 'Tcnt', 'Len', 'Unf', 'Sfi', 'Str', 'Elg', 'Mic', 'Mat', 'Rd', 'b', 'Col2', 'Mst'); // Parametros HVI ('Sci', 'Cnt' )
        $HVI = array(); //declarar arreglo vacio para almacenar los valores activos
        $array=array_map('lowercase', explode(',', $LotU));
        $array = implode(",",$array);

        $cont = 0;
        foreach($selectParametros as $row){
            if ($row == 1){
                $HVI[] = $HVIParametros[$cont];
            } 
            $cont++;
        }

        if (empty($HVI)){
            $data = 0;
        }
        
        foreach($HVI as $row){
        
            if ($row =="Mat" || $row =="Len" || $row =="Mst" || $row =="Tar"){
            
                $consulta = 'SELECT ROUND('.$row.',2) as '.$row.'s, Count(Bal) as '.$row.'Bal
                FROM Bales 
                WHERE Lot IN ('.$array.') 
                Group by '.$row.'s Order by '.$row.'s;';
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $dataO=$resultado->fetchAll(PDO::FETCH_ASSOC);

                $consulta = 'SELECT ROUND('.$row.'_Mod,2) as '.$row.'s_Mod, Count(Bal) as '.$row.'Bal_Mod
                FROM Bales 
                WHERE Lot IN ('.$array.') 
                Group by '.$row.'s_Mod Order by '.$row.'s_Mod;';
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $dataM=$resultado->fetchAll(PDO::FETCH_ASSOC);
                
            }else if($row =="Col2"){
                $consulta = 'SELECT CONCAT(Col1,"-",Col2) as Color, COUNT(Bal) as BalCol
                FROM Bales 
                WHERE Lot IN ('.$array.') 
                Group by Color Order by Color;';
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $dataO=$resultado->fetchAll(PDO::FETCH_ASSOC);

                $consulta = 'SELECT CONCAT(Col1_Mod,"-",Col2_Mod) as Color_Mod, COUNT(Bal) as BalCol_Mod
                FROM Bales 
                WHERE Lot IN ('.$array.') 
                Group by Color_Mod;';
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $dataM=$resultado->fetchAll(PDO::FETCH_ASSOC);
            }else{
                $consulta = 'SELECT ROUND('.$row.',1) as '.$row.'s, Count(Bal) as '.$row.'Bal
                FROM Bales 
                WHERE Lot IN ('.$array.') 
                Group by '.$row.'s Order by '.$row.'s;';
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $dataO=$resultado->fetchAll(PDO::FETCH_ASSOC);

                $consulta = 'SELECT ROUND('.$row.'_Mod,1) as '.$row.'s_Mod, Count(Bal) as '.$row.'Bal_Mod
                FROM Bales 
                WHERE Lot IN ('.$array.') 
                Group by '.$row.'s_Mod Order by '.$row.'s_Mod;';
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $dataM=$resultado->fetchAll(PDO::FETCH_ASSOC);
            }

            $data[] = array_merge($dataO, $dataM); //$data[] = array_merge($dataO, $dataM);
        }
        
        //-------
       /* $stmt1 = $conexion->prepare('SELECT ROUND(:row,:precision) as rowS, COUNT(Bal) as rowBal FROM Bales WHERE Lot IN ('.$array.') GROUP BY rowS ORDER BY rowS;');
        $stmt2 = $conexion->prepare('SELECT ROUND(:row_Mod,:precision) as rowS_Mod, COUNT(Bal) as rowBal_Mod FROM Bales WHERE Lot IN ('.$array.') GROUP BY rowS_Mod ORDER BY rowS_Mod;');
        $stmt3 = $conexion->prepare('SELECT CONCAT(Col1, "-", Col2) as Color, COUNT(Bal) as BalCol FROM Bales WHERE Lot IN ('.$array.') GROUP BY Color ORDER BY Color;');
        $stmt4 = $conexion->prepare('SELECT CONCAT(Col1_Mod, "-", Col2_Mod) as Color_Mod, COUNT(Bal) as BalCol_Mod FROM Bales WHERE Lot IN ('.$array.') GROUP BY Color_Mod;');
        
        foreach($HVI as $row){ 
            if ($row =="Mat" || $row =="Len" || $row =="Mst" || $row =="Tar"){
                $precision = 2;
                $stmt1->bindParam(':precision', $precision, PDO::PARAM_INT);
                $stmt1->bindParam(':row', $row);
                $stmt2->bindParam(':precision', $precision, PDO::PARAM_INT);
                $stmt2->bindParam(':row', $row);
        
                $stmt1->execute();
                $dataO=$stmt1->fetchAll(PDO::FETCH_ASSOC);
        
                $stmt2->execute();
                $dataM=$stmt2->fetchAll(PDO::FETCH_ASSOC);
            } else if($row =="Col2"){
                $stmt3->execute();
                $dataO=$stmt3->fetchAll(PDO::FETCH_ASSOC);
        
                $stmt4->execute();
                $dataM=$stmt4->fetchAll(PDO::FETCH_ASSOC);
            } else {
                $precision = 1;
                $stmt1->bindParam(':precision', $precision, PDO::PARAM_INT);
                $stmt2->bindParam(':precision', $precision, PDO::PARAM_INT);
        
                $stmt1->execute([$row,$row,$row,$row,$row]);
                $dataO=$stmt1->fetchAll(PDO::FETCH_ASSOC);
        
                $stmt2->execute([$row,$row,$row,$row,$row]);
                $dataM=$stmt2->fetchAll(PDO::FETCH_ASSOC);
            }
            $data[] = array_merge($dataO, $dataM);
        }

        //-------
        
        // Preparar la consulta
        /*$consulta = 'SELECT ';
        foreach ($HVI as $row) {
          if ($row =="Mat" || $row =="Len" || $row =="Mst" || $row =="Tar"){ 
            $consulta .= 'ROUND('.$row.',2) as '.$row.'s, Count(Bal) as '.$row.'Bal, ';
            $consulta .= 'ROUND('.$row.'_Mod,2) as '.$row.'s_Mod, Count(Bal) as '.$row.'Bal_Mod, ';      
          }else if ($row == 'Col2') {
            $consulta .= 'CONCAT(Col1,"-",Col2) as Color, COUNT(Bal) as BalCol, ';
            $consulta .= 'CONCAT(Col1_Mod,"-",Col2_Mod) as Color_Mod, COUNT(Bal) as BalCol_Mod, ';
          } else {
            $consulta .= 'ROUND('.$row.',1) as '.$row.'s, Count(Bal) as '.$row.'Bal, ';
            $consulta .= 'ROUND('.$row.'_Mod,1) as '.$row.'s_Mod, Count(Bal) as '.$row.'Bal_Mod, ';
          }
        }
        // Eliminar la coma final
        $consulta = rtrim($consulta, ', ');
        
        $consulta .= ' FROM Bales
        WHERE Lot IN ('.$array.')
        GROUP BY ';
        foreach ($HVI as $row) {
        if ($row == 'Col2') {
        $consulta .= 'Color, Color_Mod, ';
        } else {
        $consulta .= $row.'s, '.$row.'s_Mod, ';
        }
        }
        // Eliminar la coma final
        $consulta = rtrim($consulta, ', ');
        $consulta .= ' ORDER BY ';
        foreach ($HVI as $row) {
        if ($row == 'Col2') {
        $consulta .= 'Color, Color_Mod, ';
        } else {
        $consulta .= $row.'s, '.$row.'s_Mod, ';
        }
        }
        // Eliminar la coma final
        $consulta = rtrim($consulta, ', ');
        
        // Ejecutar la consulta preparada
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data = $resultado->fetchAll(PDO::FETCH_ASSOC);
        
        // Combinar los datos recuperados con $dataHVI
        $data = array_merge($dataHVI, $data);*/
        
        /* Crear una función que ejecute una consulta y devuelva los resultados
        function ejecutarConsulta($row, $array, $conexion) {
            $consulta = "SELECT ROUND($row, 2) as ${row}s, Count(Bal) as ${row}Bal, 
                ROUND(${row}_Mod, 2) as ${row}s_Mod, Count(Bal) as ${row}Bal_Mod
                FROM Bales 
                WHERE Lot IN ($array) 
                GROUP BY ${row}s, ${row}s_Mod 
                ORDER BY ${row}s, ${row}s_Mod";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();        
            return $resultado->fetchAll(PDO::FETCH_ASSOC);
        }
        
        // Crear una variable para almacenar los resultados
        $data = array();
        
        // Crear una sola consulta que agrupe los resultados por elemento de $HVI
        $hvi = implode("', '", array('Col1', 'Tar', 'Tgrd', 'Tcnt', 'Len', 'Unf', 'Sfi', 'Str', 'Elg', 'Mic', 'Mat', 'Rd', 'b', 'Col2', 'Mst'));
        
        $consulta = "SELECT HVI, GROUP_CONCAT(DISTINCT ROUND(HVI, 2) SEPARATOR ',') AS ${hvi}_s, 
            GROUP_CONCAT(DISTINCT Count(Bal) SEPARATOR ',') AS ${hvi}_Bal,
            GROUP_CONCAT(DISTINCT ROUND(HVI_Mod, 2) SEPARATOR ',') AS ${hvi}_s_Mod,
            GROUP_CONCAT(DISTINCT Count(Bal) SEPARATOR ',') AS ${hvi}_Bal_Mod
            FROM Bales
            WHERE Lot IN ($array) AND HVI IN ('$hvi')
            GROUP BY HVI
            ORDER BY HVI";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataHVI = $resultado->fetchAll(PDO::FETCH_ASSOC);*/


        /*foreach($HVI as $row){
            
            if (in_array($row, array("Mat", "Len", "Mst", "Tar"))) {
                $data[] = ejecutarConsulta($row, $array, $conexion);
            }else if(in_array($row, array("Col2"))){
                $consulta = 'SELECT CONCAT(Col1,"-",Col2) as Color, COUNT(Bal) as BalCol
                FROM Bales 
                WHERE Lot IN ('.$array.') 
                Group by Color Order by Color;';
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $dataO=$resultado->fetchAll(PDO::FETCH_ASSOC);

                $consulta = 'SELECT CONCAT(Col1_Mod,"-",Col2_Mod) as Color_Mod, COUNT(Bal) as BalCol_Mod
                FROM Bales 
                WHERE Lot IN ('.$array.') 
                Group by Color_Mod;';
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $dataM=$resultado->fetchAll(PDO::FETCH_ASSOC);
            }else{
                $consulta = 'SELECT ROUND('.$row.',1) as '.$row.'s, Count(Bal) as '.$row.'Bal
                FROM Bales 
                WHERE Lot IN ('.$array.') 
                Group by '.$row.'s Order by '.$row.'s;';
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $dataO=$resultado->fetchAll(PDO::FETCH_ASSOC);

                $consulta = 'SELECT ROUND('.$row.'_Mod,1) as '.$row.'s_Mod, Count(Bal) as '.$row.'Bal_Mod
                FROM Bales 
                WHERE Lot IN ('.$array.') 
                Group by '.$row.'s_Mod Order by '.$row.'s_Mod;';
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $dataM=$resultado->fetchAll(PDO::FETCH_ASSOC);
            }

            $data = array_merge($dataHVI, $data);
        }*/

        break;
    case 6:
    
    //-------------------------------------- Vectores para modificar Rd y +b ---------------------------------------------------------

        $ColorMod = array('11-1', '11-2', '11-3', '11-4', '21-1', '21-2', '21-3', '21-4', '31-1', '31-2', '31-3', '31-4', '41-1', '41-2', '41-3', '41-4', '51-1', '51-2', '51-3', '51-4', '61-1', '61-2', '61-3', '61-4', '71-1', '71-2', '71-3', '71-4', '12-1', '12-2', '22-1', '22-2', '32-1', '32-2', '42-1', '42-2', '52-1', '52-2', '62-1', '62-2', '82-1', '82-2', '13-1', '13-2', '23-1', '23-2', '33-1', '33-2', '43-1', '43-2', '53-1', '53-2', '63-1', '63-2', '13-3', '13-4', '23-3', '23-4', '33-3', '33-4', '43-3', '43-4', '53-3', '53-4', '63-3', '63-4', '24-1', '24-2', '34-1', '34-2', '44-1', '44-2', '54-1', '54-2', '24-3', '24-4', '34-3', '34-4', '44-3', '44-4', '54-3', '54-4', '25-1', '25-2', '35-1', '35-2');
        $Rd_RotMin = array('80.9', '80.1','81','80.5','78.8','78.1','79.1','78.2','76.7','75.2','76.7','75.2','72.7','70.5','72.9','70.8','67.5','65.4','67.9','65.6','62.1','59.8','62.5','60.3','56.5','54.8','57.1','55.5','81.1','80.4','79.1','78.1','76.5','75','72.8','70.8','68','65.9','62.7','60.6','57.8','56','81.1','80.5','79.2','78.2','76.5','75','72.8','70.9','68.2','66.3','63.4','61.3','81.2','80.5','79.3','78.4','76.4','75','72.9','71.1','68.5','66.5','63.7','61.8','80.6','78.3','76.5','74.7','72.8','71.1','68.7','66.7','80.6','78.4','76.5','74.7','72.8','71.3','68.8','67.1','80.6','78.6','76.5','74.7');
        $Rd_RotMax = array('87', '80.9', '87.1', '81', '80.2', '78.8', '80.1', '78.8', '78', '76.5', '78', '76.5', '75.2', '72.6', '74.9', '72.9', '70.5', '67.5', '70.7', '67.7', '65.2', '62.1', '65.6', '62.5', '59.6', '56.2', '60.3', '57.1', '87.1', '81.1', '80.2', '79', '78', '76.5', '75', '72.8', '70.8', '68', '65.9', '62.7', '60.6', '57.6', '87.1', '81.1', '80.3', '79.2', '78.1', '76.4', '74.9', '72.7', '70.8', '68.2', '66.1', '63.2', '87.1', '81.2', '80.4', '79.2', '78.2', '76.4', '74.9', '72.7', '71', '68.4', '66.4', '63.6', '87.1', '80.5', '78.2', '76.4', '74.6', '72.7', '71', '68.7', '87.1', '80.5', '78.3', '76.4', '74.6', '72.8', '71.2', '68.8', '87.1', '80.6', '78.5', '76.4');
        $b_RotMin = array('-34', '-34', '-25.5', '-25.5', '-35', '-35', '-25.6', '-25.6', '-35', '-35', '-25.6', '-25.4', '-35.4', '-35.8', '-25.1', '-24.7', '-35.8', '-35.1', '-23.9', '-23', '-32.5', '-30.1', '-21.9', '-20.8', '-26.7', '-23.1', '-19', '-17.9', '-23.4', '-23.4', '-23.3', '-23.2', '-23', '-22.9', '-22.4', '-22.1', '-21.6', '-20.9', '-19.7', '-18.8', '-15.5', '-14.8', '-19.8', '-19.6', '-19.4', '-19.2', '-19.1', '-18.9', '-18.6', '-18.1', '-17.6', '-16.5', '-15.4', '-14.2', '-15.9', '-15.6', '-15.4', '-15.2', '-15.2', '-15', '-14.8', '-14.2', '-13.5', '-12.5', '-11.8', '-10.8', '-12.3', '-11.7', '-11.2', '-10.8', '-10.6', '-10.2', '-9.8', '-8.8', '-8.7', '-8.1', '-7.8', '-7.4', '-7', '-6.3', '-5.8', '-4.9', '-3.5', '-3.1', '-2.8', '-2.2');
        $b_RotMax = array('-25.6', '-25.6', '-23.5', '-23.5', '-25.6', '-25.6', '-23.5', '-23.4', '-25.6', '-25.6', '-23.3', '-22.9', '-25.1', '-24.7', '-22.8', '-22.2', '-23.9', '-23.1', '-21.4', '-20.9', '-22.1', '-20.8', '-19.9', '-18.9', '-19.7', '-17.9', '-16.2', '-14.7', '-19.8', '-19.8', '-19.8', '-19.6', '-19.4', '-19.1', '-18.6', '-18.3', '-17.7', '-16.8', '-15.7', '-14.4', '-13.1', '-11.6', '-15.9', '-15.9', '-15.9', '-15.7', '-15.4', '-15.2', '-14.9', '-14.2', '-13.7', '-12.9', '-11.8', '-10.8', '-12.3', '-12.1', '-12.1', '-11.9', '-11.6', '-11.3', '-10.8', '-10.3', '-9.7', '-9', '-8', '-6.8', '-8.7', '-8.3', '-7.9', '-7.5', '-7.1', '-6.6', '-6', '-5.1', '-4.1', '-3.5', '-3.1', '-2.6', '-2.1', '-1.5', '-1.1', '-0.4', '1.2', '1.9', '4.1', '5.1');

        //--------------------------------------------------------------------------------------------------------------------------------
    
    
        $selectParametros = array($GrdHVI, $TaHVI, $TcHVI, $PcHVI, $LenHVI, $UiHVI, $SfiHVI, $StrHVI, $ElgHVI, $MicHVI, $MatHVI, $RDHVI, $bHVI, $ColHVI, $MstHVI); //arreglo de checkbox 1/0 dependiento si estan seleccionados
        $HVIParametros = array('Col1', 'Tar', 'Tgrd', 'Tcnt', 'Len', 'Unf', 'Sfi', 'Str', 'Elg', 'Mic', 'Mat', 'Rd', 'b', 'Col2', 'Mst'); // Parametros HVI ('Sci', 'Cnt' )
        $HVI = array(); //declarar arreglo vacio para almacenar los valores activos
        $array=array_map('lowercase', explode(',', $LotU));
        $array = implode(",",$array);

        $cont = 0;
        foreach($selectParametros as $row){
            if ($row == 1){
                $HVI[] = $HVIParametros[$cont];
            } 
            $cont++;
        }

        if (empty($HVI)){
            $data = 0;
        }else{
            //Micro
            if (in_array("Mic", $HVI)) {
                //Mandar valores a cero
                if ($MicMinO == 0 && $MicMaxO == "" && $MicMinM == 0 && $MicMaxM == "" && $MicRange == ""){

                    $consulta = 'UPDATE Bales SET Mic_Mod = 0 WHERE Lot IN ('.$array.');';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();        
                //Regresar valores a original    
                }else if($MicMinO != "" && $MicMaxO != "" && $MicMinM == "" && $MicMaxM == "" && $MicRange == 0){
    
                    $consulta = 'UPDATE Bales SET Mic_Mod = Mic WHERE Lot IN ('.$array.');'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Sumar valor especifico a un rango            
                }else if($MicMinO != "" && $MicMaxO != "" && $MicMinM == "" && $MicMaxM == "" && $MicRange != "" && $MicRange != 0){
    
                    $consulta = 'UPDATE Bales SET Mic_Mod = ROUND(Mic,1) + '.$MicRange.' WHERE Lot IN ('.$array.') and Round(Mic,1) BETWEEN '.$MicMinO.' and '.$MicMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Mover de rango a rango
                }else if($MicMinO != "" && $MicMaxO != "" && $MicMinM != "" && $MicMaxM != "" && $MicRange == ""){
    
                    $consulta = 'UPDATE Bales SET Mic_Mod = Round((select (RAND() * ('.$MicMaxM.'-'.$MicMinM.') + '.$MicMinM.')),1) WHERE Lot IN ('.$array.') and Round(Mic,1) BETWEEN '.$MicMinO.' and '.$MicMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                }
                
                /*$consulta = 'SELECT Bal, Mic FROM Bales WHERE Lot IN ('.$array.');'; 
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);*/
            }
            //Grado
            if (in_array("Col1", $HVI)) {
                //Mandar valores a cero
                if ($GrdMinO == 0 && $GrdMaxO == "" && $GrdMinM == 0 && $GrdMaxM == "" && $GrdRange == ""){

                    $consulta = 'UPDATE Bales SET Col1_Mod = 0 WHERE Lot IN ('.$array.');';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();        
                //Regresar valores a original    
                }else if($GrdMinO != "" && $GrdMaxO != "" && $GrdMinM == "" && $GrdMaxM == "" && $GrdRange == 0){
    
                    $consulta = 'UPDATE Bales SET Col1_Mod = Col1 WHERE Lot IN ('.$array.');'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                    
                    //Col2 a original
                    $consulta = 'UPDATE Bales SET Col2_Mod = Col2, Rd_Mod = Rd, b_Mod = b WHERE Lot IN ('.$array.');'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Sumar valor especifico a un rango            
                }else if($GrdMinO != "" && $GrdMaxO != "" && $GrdMinM == "" && $GrdMaxM == "" && $GrdRange != "" && $GrdRange != 0){
    
                    $consulta = 'UPDATE Bales SET Col1_Mod = ROUND(Col1,0) + '.$Col1Range.' WHERE Lot IN ('.$array.') and Col1 BETWEEN '.$GrdMinO.' and '.$GrdMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Mover de rango a rango  (esperar confirmacion de como realizar dicha operacion)
                }else if($GrdMinO != "" && $GrdMaxO != "" && $GrdMinM != "" && $GrdMaxM != "" && $GrdRange == ""){
                    
                    $consulta = 'UPDATE Bales SET Col1_Mod = Round((select (RAND() * ('.$GrdMaxM.'-'.$GrdMinM.') + '.$GrdMinM.')),0) WHERE Lot IN ('.$array.') and Round(Col1,0) BETWEEN '.$GrdMinO.' and '.$GrdMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();

                    //Actualizar color
                    if ($GrdMinO != $GrdMaxO){
                        $consulta = 'UPDATE Bales SET Col2_Mod = '.$Col2.' WHERE Lot IN ('.$array.') and Col1 = '.$GrdMinO.';'; 
                        $resultado = $conexion->prepare($consulta);
                        $resultado->execute();

                        $consulta = 'UPDATE Bales SET Col2_Mod = '.$Col2.' WHERE Lot IN ('.$array.') and Col1 = '.$GrdMaxO.';'; 
                        $resultado = $conexion->prepare($consulta);
                        $resultado->execute();
                        
                        //obtener color modificado para modificar Rd y +b
                        $consulta = 'SELECT Bal, Col1, Col2, CONCAT(Col1_Mod,"-",Col2_Mod) as ColMod, COUNT(Bal) as BalCol
                        FROM Bales
                        WHERE Lot IN ('.$array.') and Col1 BETWEEN '.$GrdMinO.' AND '.$GrdMaxO.' 
                        Group by Bal;';
                        $resultado = $conexion->prepare($consulta);
                        $resultado->execute();        
                        $dataColMod=$resultado->fetchAll();
                        
                        //Consulta Preparada
                        // Definimos la consulta base
                        $consulta = 'UPDATE Bales SET Rd_Mod = CASE';
                        
                        // Creamos una variable para almacenar los valores de la columna 'Bal' que cumplen la condición
                        $bals = [];
                        
                        foreach($dataColMod as $dataColor) {
                            $posCol = array_search($dataColor['ColMod'], $ColorMod);
                            $Rd_Min = $Rd_RotMin[$posCol]; 
                            $Rd_Max = $Rd_RotMax[$posCol];
                            $b_Min = $b_RotMin[$posCol];
                            $b_Max = $b_RotMax[$posCol];
                        
                            $Rd_Rot = rand($Rd_Min, $Rd_Max);
                            $b_Rot = rand($b_Min, $b_Max);
                        
                            $Rd_Final = (-$b_Rot * 0.60843) + ($Rd_Rot * 0.7936);
                            $b_Final = (($b_Rot * 0.7936) + ($Rd_Rot * 0.60843))/3;
                        
                            // Agregamos la condición a la consulta
                            $consulta .= ' WHEN Bal = '.$dataColor['Bal'].' and Lot IN ('.$array.') and Col1 = '.$dataColor['Col1'].' and Col2 = '.$dataColor['Col2'].' THEN '.$Rd_Final;
                            $bals[] = $dataColor['Bal'];
                        }
                        
                        // Agregamos la cláusula ELSE para mantener el valor actual de la columna
                        $consulta .= ' ELSE Rd_Mod END, b_Mod = CASE';
                        
                        foreach($dataColMod as $dataColor) {
                            $posCol = array_search($dataColor['ColMod'], $ColorMod);
                            $Rd_Min = $Rd_RotMin[$posCol]; 
                            $Rd_Max = $Rd_RotMax[$posCol];
                            $b_Min = $b_RotMin[$posCol];
                            $b_Max = $b_RotMax[$posCol];
                        
                            $Rd_Rot = rand($Rd_Min, $Rd_Max);
                            $b_Rot = rand($b_Min, $b_Max);
                        
                            $Rd_Final = (-$b_Rot * 0.60843) + ($Rd_Rot * 0.7936);
                            $b_Final = (($b_Rot * 0.7936) + ($Rd_Rot * 0.60843))/3;
                        
                            // Agregamos la condición a la consulta
                            $consulta .= ' WHEN Bal = '.$dataColor['Bal'].' and Lot IN ('.$array.') and Col1 = '.$dataColor['Col1'].' and Col2 = '.$dataColor['Col2'].' THEN '.$b_Final;
                        }
                        
                        // Agregamos la cláusula WHERE para especificar los valores de la columna 'Bal' que cumplen la condición
                        $consulta .= ' END WHERE Bal IN ('.implode(',', $bals).') and Lot IN ('.$array.')';
                        
                        // Ejecutamos la consulta
                        $resultado = $conexion->prepare($consulta);
                        $resultado->execute();
                            
                    }else{
                        $consulta = 'UPDATE Bales SET Col2_Mod = '.$Col2.' WHERE Lot IN ('.$array.') and Col1 = '.$GrdMinO.';'; 
                        $resultado = $conexion->prepare($consulta);
                        $resultado->execute();
                        
                        //obtener color modificado
                        $consulta = 'SELECT Bal, Col1, Col2, CONCAT(Col1_Mod,"-",Col2_Mod) as ColMod, COUNT(Bal) as BalCol
                        FROM Bales
                        WHERE Lot IN ('.$array.') and Col1 = '.$GrdMinO.'
                        Group by Bal;';
                        $resultado = $conexion->prepare($consulta);
                        $resultado->execute();        
                        $dataColMod=$resultado->fetchAll();
                        
                        //consulta preparada
                        // Definimos la consulta base
                        $consulta = 'UPDATE Bales SET Rd_Mod = CASE';
                        
                        // Creamos una variable para almacenar los valores de la columna 'Bal' que cumplen la condición
                        $bals = [];
                        
                        foreach($dataColMod as $dataColor) {
                            $posCol = array_search($dataColor['ColMod'], $ColorMod);
                            $Rd_Min = $Rd_RotMin[$posCol]; 
                            $Rd_Max = $Rd_RotMax[$posCol];
                            $b_Min = $b_RotMin[$posCol];
                            $b_Max = $b_RotMax[$posCol];
                        
                            $Rd_Rot = rand($Rd_Min, $Rd_Max);
                            $b_Rot = rand($b_Min, $b_Max);
                        
                            $Rd_Final = (-$b_Rot * 0.60843) + ($Rd_Rot * 0.7936);
                            $b_Final = (($b_Rot * 0.7936) + ($Rd_Rot * 0.60843))/3;
                        
                            // Agregamos la condición a la consulta
                            $consulta .= ' WHEN Bal = '.$dataColor['Bal'].' and Lot IN ('.$array.') and Col1 = '.$dataColor['Col1'].' and Col2 = '.$dataColor['Col2'].' THEN '.$Rd_Final;
                            $bals[] = $dataColor['Bal'];
                        }
                        
                        // Agregamos la cláusula ELSE para mantener el valor actual de la columna
                        $consulta .= ' ELSE Rd_Mod END, b_Mod = CASE';
                        
                        foreach($dataColMod as $dataColor) {
                            $posCol = array_search($dataColor['ColMod'], $ColorMod);
                            $Rd_Min = $Rd_RotMin[$posCol]; 
                            $Rd_Max = $Rd_RotMax[$posCol];
                            $b_Min = $b_RotMin[$posCol];
                            $b_Max = $b_RotMax[$posCol];
                        
                            $Rd_Rot = rand($Rd_Min, $Rd_Max);
                            $b_Rot = rand($b_Min, $b_Max);
                        
                            $Rd_Final = (-$b_Rot * 0.60843) + ($Rd_Rot * 0.7936);
                            $b_Final = (($b_Rot * 0.7936) + ($Rd_Rot * 0.60843))/3;
                        
                            // Agregamos la condición a la consulta
                            $consulta .= ' WHEN Bal = '.$dataColor['Bal'].' and Lot IN ('.$array.') and Col1 = '.$dataColor['Col1'].' and Col2 = '.$dataColor['Col2'].' THEN '.$b_Final;
                        }
                        
                        // Agregamos la cláusula WHERE para especificar los valores de la columna 'Bal' que cumplen la condición
                        $consulta .= ' END WHERE Bal IN ('.implode(',', $bals).') and Lot IN ('.$array.')';
                        
                        // Ejecutamos la consulta
                        $resultado = $conexion->prepare($consulta);
                        $resultado->execute();                         
                    }
                    
                    /*$conexion->beginTransaction();

                    try {
                    
                        $consulta = 'UPDATE Bales SET Col1_Mod = Round((select (RAND() * ('.$GrdMaxM.'-'.$GrdMinM.') + '.$GrdMinM.')),0) WHERE Lot IN ('.$array.') and Round(Col1,0) BETWEEN '.$GrdMinO.' and '.$GrdMaxO.';'; 
                        $resultado = $conexion->prepare($consulta);
                        $resultado->execute();
                        $conjunto =[];
    
                        //Actualizar color
                        if ($GrdMinO != $GrdMaxO){
                            $consulta = 'UPDATE Bales SET Col2_Mod = '.$Col2.' WHERE Lot IN ('.$array.') and Col1 = '.$GrdMinO.';'; 
                            $resultado = $conexion->prepare($consulta);
                            $resultado->execute();
    
                            $consulta = 'UPDATE Bales SET Col2_Mod = '.$Col2.' WHERE Lot IN ('.$array.') and Col1 = '.$GrdMaxO.';'; 
                            $resultado = $conexion->prepare($consulta);
                            $resultado->execute();
                            
                            //obtener color modificado para modificar Rd y +b
                            $consulta = 'SELECT Bal, Col1, Col2, CONCAT(Col1_Mod,"-",Col2_Mod) as ColMod, COUNT(Bal) as BalCol
                            FROM Bales
                            WHERE Lot IN ('.$array.') and Col1 BETWEEN '.$GrdMinO.' AND '.$GrdMaxO.' 
                            Group by Bal;';
                            $resultado = $conexion->prepare($consulta);
                            $resultado->execute();        
                            $dataColMod=$resultado->fetchAll();
                            
                            foreach($dataColMod as $dataColor){
                                $valor=[];
                                $posCol = array_search($dataColor['ColMod'], $ColorMod);
                                $Rd_Min = $Rd_RotMin[$posCol]; 
                                $Rd_Max = $Rd_RotMax[$posCol];
                                $b_Min = $b_RotMin[$posCol];
                                $b_Max = $b_RotMax[$posCol];
    
    
                                $Rd_Rot = rand($Rd_Min, $Rd_Max);
                                $b_Rot = rand($b_Min, $b_Max);
    
    
                                $Rd_Final = (-$b_Rot * 0.60843) + ($Rd_Rot * 0.7936);
                                $b_Final = (($b_Rot * 0.7936) + ($Rd_Rot * 0.60843))/3;
    
    
                                $valor=[
                                    'Rd_Final'=>$Rd_Final,
                                    'b_Final'=>$b_Final,
                                    'dataColorBal'=>$dataColor['Bal'],
                                    'dataColorCol1'=>$dataColor['Col1'],
                                    'dataColorCol2'=>$dataColor['Col2']                                
                                ];
    
                               
                            }
    
                            $consulta = 'UPDATE Bales SET Rd_Mod = :valor1, b_Mod = :valor2 WHERE Bal = :valor3 and Lot IN ('.$array.') and Col1 =:valor4 and Col2 = :valor5 ;'; 
                            $resultado = $conexion->prepare($consulta);
    
                            for($i=0; $i<count($conjunto);$i++){
    
                                $params = array(
                                    ':valor1' => $conjunto[$i]['Rd_Final'],
                                    ':valor2' => $conjunto[$i]['b_Final'],
                                    ':valor3' => $conjunto[$i]['dataColorBal'],
                                    ':valor4' => $conjunto[$i]['dataColorCol1'],
                                    ':valor5' => $conjunto[$i]['dataColorCol2'],
                                );
                                $resultado->execute($params);
                                
                            }   
                        }*/
                        /*$consulta = 'UPDATE Bales SET Col1_Mod = Round((select (RAND() * ('.$GrdMaxM.'-'.$GrdMinM.') + '.$GrdMinM.')),0) WHERE Lot IN ('.$array.') and Round(Col1,0) BETWEEN '.$GrdMinO.' and '.$GrdMaxO.';'; 
                        $resultado = $conexion->prepare($consulta);
                        $resultado->execute();
                    
                        if ($GrdMinO != $GrdMaxO){

                            $consulta = 'UPDATE Bales SET Col2_Mod = '.$Col2.' WHERE Lot IN ('.$array.') and Col1 = '.$GrdMinO.';'; 
                            $resultado = $conexion->prepare($consulta);
                            $resultado->execute();
                    
                            $consulta = 'UPDATE Bales SET Col2_Mod = '.$Col2.' WHERE Lot IN ('.$array.') and Col1 = '.$GrdMaxO.';'; 
                            $resultado = $conexion->prepare($consulta);
                            $resultado->execute();
                    
                            $consulta = 'SELECT Bal, Col1, Col2, CONCAT(Col1_Mod,"-",Col2_Mod) as ColMod, COUNT(Bal) as BalCol
                            FROM Bales
                            WHERE Lot IN ('.$array.') and Col1 = '.$GrdMinO.'
                            Group by Bal;';
                            $resultado = $conexion->prepare($consulta);
                            $resultado->execute();        
                            $dataColMod=$resultado->fetchAll();
                            $consulta2="";
                            
                            foreach($dataColMod as $dataColor){
                                $posCol = array_search($dataColor['ColMod'], $ColorMod);
                                $Rd_Min = $Rd_RotMin[$posCol]; 
                                $Rd_Max = $Rd_RotMax[$posCol];
                                $b_Min = $b_RotMin[$posCol];
                                $b_Max = $b_RotMax[$posCol];
    
    
                                $Rd_Rot = rand($Rd_Min, $Rd_Max);
                                $b_Rot = rand($b_Min, $b_Max);
    
    
                                $Rd_Final = (-$b_Rot * 0.60843) + ($Rd_Rot * 0.7936);
                                $b_Final = (($b_Rot * 0.7936) + ($Rd_Rot * 0.60843))/3;
    
                                $consulta2 .= 'UPDATE Bales SET Rd_Mod = '.$Rd_Final.', b_Mod = '.$b_Final.' WHERE Bal = '.$dataColor['Bal'].' and Lot IN ('.$array.') and Col1 = '.$dataColor['Col1'].' and Col2 = '.$dataColor['Col2'].';';
                                
    
                            }
    
    
                            $resultado=$conexion->beginTransaction();
                            $resultado = $conexion->prepare($consulta2);
                            $resultado->execute($consulta2); 
                            $resultado=$conexion->commit();
                        } /*else{
                        
                            $consulta = 'UPDATE Bales SET Col2_Mod = :col2_mod WHERE Lot IN ('.$array.') and Col1 = :grd_min_o;
                                SELECT Bal, Col1, Col2, CONCAT(Col1_Mod,"-",Col2_Mod) as ColMod, COUNT(Bal) as BalCol
                                FROM Bales
                                WHERE Lot IN ('.$array.') and Col1 = :grd_min_o
                                GROUP BY Bal';
                    
                            $resultado = $conexion->prepare($consulta);
                            $resultado->bindParam(':col2_mod', $Col2, PDO::PARAM_INT);
                            $resultado->bindParam(':grd_min_o', $GrdMinO, PDO::PARAM_INT);
                            $resultado->execute();
                            
                            $dataColMod = $resultado->fetchAll();
                            
                            foreach($dataColMod as $dataColor) {
                                $posCol = array_search($dataColor['ColMod'], $ColorMod);
                                $Rd_Min = $Rd_RotMin[$posCol]; 
                                $Rd_Max = $Rd_RotMax[$posCol];
                                $b_Min = $b_RotMin[$posCol];
                                $b_Max = $b_RotMax[$posCol];
                            
                                $Rd_Rot = rand($Rd_Min, $Rd_Max);
                                $b_Rot = rand($b_Min, $b_Max);
                            
                                $Rd_Final = (-$b_Rot * 0.60843) + ($Rd_Rot * 0.7936);
                                $b_Final = (($b_Rot * 0.7936) + ($Rd_Rot * 0.60843))/3;
                            
                                $consulta = 'UPDATE Bales SET Rd_Mod = :rd_mod, b_Mod = :b_mod WHERE Bal = :bal AND Lot IN ('.$array.') AND Col1 = :col1 AND Col2 = :col2';
                                $resultado = $conexion->prepare($consulta);
                                $resultado->bindParam(':rd_mod', $Rd_Final, PDO::PARAM_STR);
                                $resultado->bindParam(':b_mod', $b_Final, PDO::PARAM_STR);
                                $resultado->bindParam(':bal', $dataColor['Bal'], PDO::PARAM_INT);
                                $resultado->bindParam(':col1', $dataColor['Col1'], PDO::PARAM_INT);
                                $resultado->bindParam(':col2', $dataColor['Col2'], PDO::PARAM_INT);
                                $resultado->execute();
                            }        
                        }*/
                       /* $conexion->commit();
                    } catch (Exception $e) {
                        $conexion->rollBack();
                        echo "Error en la transacción: " . $e->getMessage();
                    }*/
                    
                    
                    

                } //fin else if
                
                /*$consulta = 'SELECT Bal, Col1 FROM Bales WHERE Lot IN ('.$array.') Group by Col1;'; 
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);*/
            } // fin blique Col1
            //T. Code
            if (in_array("Tgrd", $HVI)) {
                //Mandar valores a cero
                if ($TcMinO == 0 && $TcMaxO == "" && $TcMinM == 0 && $TcMaxM == "" && $TcRange == ""){

                    $consulta = 'UPDATE Bales SET Tgrd_Mod = 0 WHERE Lot IN ('.$array.');';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();        
                //Regresar valores a original    
                }else if($TcMinO != "" && $TcMaxO != "" && $TcMinM == "" && $TcMaxM == "" && $TcRange == 0){
    
                    $consulta = 'UPDATE Bales SET Tgrd_Mod = Tgrd WHERE Lot IN ('.$array.');'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Sumar valor especifico a un rango            
                }else if($TcMinO != "" && $TcMaxO != "" && $TcMinM == "" && $TcMaxM == "" && $TcRange != "" && $TcRange != 0){
    
                    $consulta = 'UPDATE Bales SET Tgrd_Mod = ROUND(Tgrd,0) + '.$TcRange.' WHERE Lot IN ('.$array.') and Tgrd BETWEEN '.$TcMinO.' and '.$TcMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Mover de rango a rango  (esperar confirmacion de como realizar dicha operacion)
                }else if($TcMinO != "" && $TcMaxO != "" && $TcMinM != "" && $TcMaxM != "" && $TcRange == ""){
    
                    $consulta = 'UPDATE Bales SET Tgrd_Mod = Round((select (RAND() * ('.$TcMaxM.'-'.$TcMinM.') + '.$TcMinM.')),0) WHERE Lot IN ('.$array.') and Round(Tgrd,0) BETWEEN '.$TcMinO.' and '.$TcMaxO.';';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                }
                
                /*$consulta = 'SELECT Bal, Tgrd FROM Bales WHERE Lot IN ('.$array.') Group by Tgrd;'; 
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);*/
            }
            //T. Area
            if (in_array("Tar", $HVI)) {
                //Mandar valores a cero
                if ($TaMinO == 0 && $TaMaxO == "" && $TaMinM == 0 && $TaMaxM == "" && $TaRange == ""){

                    $consulta = 'UPDATE Bales SET Tar_Mod = 0 WHERE Lot IN ('.$array.');';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();        
                //Regresar valores a original    
                }else if($TaMinO != "" && $TaMaxO != "" && $TaMinM == "" && $TaMaxM == "" && $TaRange == 0){
    
                    $consulta = 'UPDATE Bales SET Tar_Mod = Tar WHERE Lot IN ('.$array.');'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Sumar valor especifico a un rango            
                }else if($TaMinO != "" && $TaMaxO != "" && $TaMinM == "" && $TaMaxM == "" && $TaRange != "" && $TaRange != 0){
    
                    $consulta = 'UPDATE Bales SET Tar_Mod = ROUND(Tgrd,2) + '.$TaRange.' WHERE Lot IN ('.$array.') and Tar BETWEEN '.$TaMinO.' and '.$TaMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Mover de rango a rango  (esperar confirmacion de como realizar dicha operacion)
                }else if($TaMinO != "" && $TaMaxO != "" && $TaMinM != "" && $TaMaxM != "" && $TaRange == ""){
    
                    $consulta = 'UPDATE Bales SET Tar_Mod = Round((select (RAND() * ('.$TaMaxM.'-'.$TaMinM.') + '.$TaMinM.')),2) WHERE Lot IN ('.$array.') and Round(Tar,2) BETWEEN '.$TaMinO.' and '.$TaMaxO.';';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                }
                
                /*$consulta = 'SELECT Bal, Tgrd FROM Bales WHERE Lot IN ('.$array.') Group by Tgrd;'; 
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);*/
            }
            //P. Count
            if (in_array("Tcnt", $HVI)) {
                //Mandar valores a cero
                if ($PcMinO == 0 && $PcMaxO == "" && $PcMinM == 0 && $PcMaxM == "" && $PcRange == ""){

                    $consulta = 'UPDATE Bales SET Tcnt_Mod = 0 WHERE Lot IN ('.$array.');';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();        
                //Regresar valores a original    
                }else if($PcMinO != "" && $PcMaxO != "" && $PcMinM == "" && $PcMaxM == "" && $PcRange == 0){
    
                    $consulta = 'UPDATE Bales SET Tcnt_Mod = Tcnt WHERE Lot IN ('.$array.');'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Sumar valor especifico a un rango            
                }else if($PcMinO != "" && $PcMaxO != "" && $PcMinM == "" && $PcMaxM == "" && $PcRange != "" && $PcRange != 0){
    
                    $consulta = 'UPDATE Bales SET Tcnt_Mod = ROUND(Tcnt,0) + '.$PcRange.' WHERE Lot IN ('.$array.') and Tcnt BETWEEN '.$PcMinO.' and '.$PcMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Mover de rango a rango  (esperar confirmacion de como realizar dicha operacion)
                }else if($PcMinO != "" && $PcMaxO != "" && $PcMinM != "" && $PcMaxM != "" && $PcRange == ""){
    
                    $consulta = 'UPDATE Bales SET Tcnt_Mod = Round((select (RAND() * ('.$PcMaxM.'-'.$PcMinM.') + '.$PcMinM.')),0) WHERE Lot IN ('.$array.') and Round(Tcnt,0) BETWEEN '.$PcMinO.' and '.$PcMaxO.';';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                }
                
                /*$consulta = 'SELECT Bal, Tcnt FROM Bales WHERE Lot IN ('.$array.') Group by Tcnt;'; 
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);*/
            }
            //Len
            if (in_array("Len", $HVI)) {
                //Mandar valores a cero
                if ($LenMinO == 0 && $LenMaxO == "" && $LenMinM == 0 && $LenMaxM == "" && $LenRange == ""){

                    $consulta = 'UPDATE Bales SET Len_Mod = 0 WHERE Lot IN ('.$array.');';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();        
                //Regresar valores a original    
                }else if($LenMinO != "" && $LenMaxO != "" && $LenMinM == "" && $LenMaxM == "" && $LenRange == 0){
    
                    $consulta = 'UPDATE Bales SET Len_Mod = Len WHERE Lot IN ('.$array.');'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Sumar valor especifico a un rango            
                }else if($LenMinO != "" && $LenMaxO != "" && $LenMinM == "" && $LenMaxM == "" && $LenRange != "" && $LenRange != 0){
    
                    $consulta = 'UPDATE Bales SET Len_Mod = ROUND(Len,2) + '.$LenRange.' WHERE Lot IN ('.$array.') and Len BETWEEN '.$LenMinO.' and '.$LenMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Mover de rango a rango  (esperar confirmacion de como realizar dicha operacion)
                }else if($LenMinO != "" && $LenMaxO != "" && $LenMinM != "" && $LenMaxM != "" && $LenRange == ""){
    
                    $consulta = 'UPDATE Bales SET Len_Mod = Round((select (RAND() * ('.$LenMaxM.'-'.$LenMinM.') + '.$LenMinM.')),2) WHERE Lot IN ('.$array.') and Round(Len,2) BETWEEN '.$LenMinO.' and '.$LenMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                }
                
                /*$consulta = 'SELECT Bal, Len FROM Bales WHERE Lot IN ('.$array.') Group by Len;'; 
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);*/
            }
            //Uniformidad
            if (in_array("Unf", $HVI)) {
                //Mandar valores a cero
                if ($UiMinO == 0 && $UiMaxO == "" && $UiMinM == 0 && $UiMaxM == "" && $UiRange == ""){

                    $consulta = 'UPDATE Bales SET Unf_Mod = 0 WHERE Lot IN ('.$array.');';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();        
                //Regresar valores a original    
                }else if($UiMinO != "" && $UiMaxO != "" && $UiMinM == "" && $UiMaxM == "" && $UiRange == 0){
    
                    $consulta = 'UPDATE Bales SET Unf_Mod = Unf WHERE Lot IN ('.$array.');'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Sumar valor especifico a un rango            
                }else if($UiMinO != "" && $UiMaxO != "" && $UiMinM == "" && $UiMaxM == "" && $UiRange != "" && $UiRange != 0){
    
                    $consulta = 'UPDATE Bales SET Unf_Mod = ROUND(Unf,1) + '.$UiRange.' WHERE Lot IN ('.$array.') and Unf BETWEEN '.$UiMinO.' and '.$UiMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Mover de rango a rango  (esperar confirmacion de como realizar dicha operacion)
                }else if($UiMinO != "" && $UiMaxO != "" && $UiMinM != "" && $UiMaxM != "" && $UiRange == ""){
    
                    $consulta = 'UPDATE Bales SET Unf_Mod = Round((select (RAND() * ('.$UiMaxM.'-'.$UiMinM.') + '.$UiMinM.')),1) WHERE Lot IN ('.$array.') and Round(Unf,1) BETWEEN '.$UiMinO.' and '.$UiMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                }
                
                /*$consulta = 'SELECT Bal, Unf FROM Bales WHERE Lot IN ('.$array.') Group by Unf;'; 
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);*/
            }
            //Sfi
            if (in_array("Sfi", $HVI)) {
                //Mandar valores a cero
                if ($SfiMinO == 0 && $SfiMaxO == "" && $SfiMinM == 0 && $SfiMaxM == "" && $SfiRange == ""){

                    $consulta = 'UPDATE Bales SET Sfi_Mod = 0 WHERE Lot IN ('.$array.');';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();        
                //Regresar valores a original    
                }else if($SfiMinO != "" && $SfiMaxO != "" && $SfiMinM == "" && $SfiMaxM == "" && $SfiRange == 0){
    
                    $consulta = 'UPDATE Bales SET Sfi_Mod = Sfi WHERE Lot IN ('.$array.');'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Sumar valor especifico a un rango            
                }else if($SfiMinO != "" && $SfiMaxO != "" && $SfiMinM == "" && $SfiMaxM == "" && $SfiRange != "" && $SfiRange != 0){
    
                    $consulta = 'UPDATE Bales SET Sfi_Mod = ROUND(Sfi,1) + '.$UiRange.' WHERE Lot IN ('.$array.') and Sfi BETWEEN '.$SfiMinO.' and '.$SfiMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Mover de rango a rango  (esperar confirmacion de como realizar dicha operacion)
                }else if($SfiMinO != "" && $SfiMaxO != "" && $SfiMinM != "" && $SfiMaxM != "" && $SfiRange == ""){
    
                    $consulta = 'UPDATE Bales SET Sfi_Mod = Round((select (RAND() * ('.$SfiMaxM.'-'.$SfiMinM.') + '.$SfiMinM.')),1) WHERE Lot IN ('.$array.') and Round(Sfi,1) BETWEEN '.$SfiMinO.' and '.$SfiMaxO.';';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                }
                
                /*$consulta = 'SELECT Bal, Sfi FROM Bales WHERE Lot IN ('.$array.') Group by Sfi;'; 
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);*/
            }
            //Str
            if (in_array("Str", $HVI)) {
                //Mandar valores a cero
                if ($StrMinO == 0 && $StrMaxO == "" && $StrMinM == 0 && $StrMaxM == "" && $StrRange == ""){

                    $consulta = 'UPDATE Bales SET Str_Mod = 0 WHERE Lot IN ('.$array.');';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();        
                //Regresar valores a original    
                }else if($StrMinO != "" && $StrMaxO != "" && $StrMinM == "" && $StrMaxM == "" && $StrRange == 0){
    
                    $consulta = 'UPDATE Bales SET Str_Mod = Str WHERE Lot IN ('.$array.');'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Sumar valor especifico a un rango            
                }else if($StrMinO != "" && $StrMaxO != "" && $StrMinM == "" && $StrMaxM == "" && $StrRange != "" && $StrRange != 0){
    
                    $consulta = 'UPDATE Bales SET Str_Mod = ROUND(Str,1) + '.$StrRange.' WHERE Lot IN ('.$array.') and Str BETWEEN '.$StrMinO.' and '.$StrMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Mover de rango a rango  (esperar confirmacion de como realizar dicha operacion)
                }else if($StrMinO != "" && $StrMaxO != "" && $StrMinM != "" && $StrMaxM != "" && $StrRange == ""){
    
                    $consulta = 'UPDATE Bales SET Str_Mod = Round((select (RAND() * ('.$StrMaxM.'-'.$StrMinM.') + '.$StrMinM.')),1) WHERE Lot IN ('.$array.') and Round(Str,1) BETWEEN '.$StrMinO.' and '.$StrMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                }
                
                /*$consulta = 'SELECT Bal, Str FROM Bales WHERE Lot IN ('.$array.') Group by Str;'; 
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);*/
            }
            //Elg
            if (in_array("Elg", $HVI)) {
                //Mandar valores a cero
                if ($ElgMinO == 0 && $ElgMaxO == "" && $ElgMinM == 0 && $ElgMaxM == "" && $ElgRange == ""){

                    $consulta = 'UPDATE Bales SET Elg_Mod = 0 WHERE Lot IN ('.$array.');';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();        
                //Regresar valores a original    
                }else if($ElgMinO != "" && $ElgMaxO != "" && $ElgMinM == "" && $ElgMaxM == "" && $ElgRange == 0){
    
                    $consulta = 'UPDATE Bales SET Elg_Mod = Elg WHERE Lot IN ('.$array.');'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Sumar valor especifico a un rango            
                }else if($ElgMinO != "" && $ElgMaxO != "" && $ElgMinM == "" && $ElgMaxM == "" && $ElgRange != "" && $ElgRange != 0){
    
                    $consulta = 'UPDATE Bales SET Elg_Mod = ROUND(Elg,1) + '.$ElgRange.' WHERE Lot IN ('.$array.') and Elg BETWEEN '.$ElgMinO.' and '.$ElgMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Mover de rango a rango  (esperar confirmacion de como realizar dicha operacion)
                }else if($ElgMinO != "" && $ElgMaxO != "" && $ElgMinM != "" && $ElgMaxM != "" && $ElgRange == ""){
    
                    $consulta = 'UPDATE Bales SET Elg_Mod = Round((select (RAND() * ('.$ElgMaxM.'-'.$ElgMinM.') + '.$ElgMinM.')),1) WHERE Lot IN ('.$array.') and Round(Elg,1) BETWEEN '.$ElgMinO.' and '.$ElgMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                }
                
                /*$consulta = 'SELECT Bal, Elg FROM Bales WHERE Lot IN ('.$array.') Group by Elg;'; 
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);*/
            }
            //Rd
            if (in_array("Rd", $HVI)) {
                //Mandar valores a cero
                if ($RdMinO == 0 && $RdMaxO == "" && $RdMinM == 0 && $RdMaxM == "" && $RdRange == ""){

                    $consulta = 'UPDATE Bales SET Rd_Mod = 0 WHERE Lot IN ('.$array.');';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();        
                //Regresar valores a original    
                }else if($RdMinO != "" && $RdMaxO != "" && $RdMinM == "" && $RdMaxM == "" && $RdRange == 0){
    
                    $consulta = 'UPDATE Bales SET Rd_Mod = Rd WHERE Lot IN ('.$array.');'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Sumar valor especifico a un rango            
                }else if($RdMinO != "" && $RdMaxO != "" && $RdMinM == "" && $RdMaxM == "" && $RdRange != "" && $RdRange != 0){
    
                    $consulta = 'UPDATE Bales SET Rd_Mod = ROUND(Rd,1) + '.$RdRange.' WHERE Lot IN ('.$array.') and Rd BETWEEN '.$RdMinO.' and '.$RdMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Mover de rango a rango  (esperar confirmacion de como realizar dicha operacion)
                }else if($RdMinO != "" && $RdMaxO != "" && $RdMinM != "" && $RdMaxM != "" && $RdRange == ""){
    
                    $consulta = 'UPDATE Bales SET Rd_Mod = Round((select (RAND() * ('.$RdMaxM.'-'.$RdMinM.') + '.$RdMinM.')),1) WHERE Lot IN ('.$array.') and Round(Rd,1) BETWEEN '.$RdMinO.' and '.$RdMaxO.';';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                }
                
                $consulta = 'SELECT Bal, Rd FROM Bales WHERE Lot IN ('.$array.') Group by Rd;'; 
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
            }
            //b
            if (in_array("b", $HVI)) {
                //Mandar valores a cero
                if ($bMinO == 0 && $bMaxO == "" && $bMinM == 0 && $bMaxM == "" && $bRange == ""){

                    $consulta = 'UPDATE Bales SET b_Mod = 0 WHERE Lot IN ('.$array.');';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();        
                //Regresar valores a original    
                }else if($bMinO != "" && $bMaxO != "" && $bMinM == "" && $bMaxM == "" && $bRange == 0){
    
                    $consulta = 'UPDATE Bales SET b_Mod = b WHERE Lot IN ('.$array.');'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Sumar valor especifico a un rango            
                }else if($bMinO != "" && $bMaxO != "" && $bMinM == "" && $bMaxM == "" && $bRange != "" && $bRange != 0){
    
                    $consulta = 'UPDATE Bales SET b_Mod = ROUND(b,1) + '.$bRange.' WHERE Lot IN ('.$array.') and b BETWEEN '.$bMinO.' and '.$bMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Mover de rango a rango  (esperar confirmacion de como realizar dicha operacion)
                }else if($bMinO != "" && $bMaxO != "" && $bMinM != "" && $bMaxM != "" && $bRange == ""){
    
                    $consulta = 'UPDATE Bales SET b_Mod = Round((select (RAND() * ('.$bMaxM.'-'.$bMinM.') + '.$bMinM.')),1) WHERE Lot IN ('.$array.') and Round(b,1) BETWEEN '.$bMinO.' and '.$bMaxO.';';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                }
                
                $consulta = 'SELECT Bal, b FROM Bales WHERE Lot IN ('.$array.') Group by b;'; 
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
            }
            //Mst
            if (in_array("Mst", $HVI)) {
                //Mandar valores a cero
                if ($MstMinO == 0 && $MstMaxO == "" && $MstMinM == 0 && $MstMaxM == "" && $MstRange == ""){

                    $consulta = 'UPDATE Bales SET Mst_Mod = 0 WHERE Lot IN ('.$array.');';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();        
                //Regresar valores a original    
                }else if($MstMinO != "" && $MstMaxO != "" && $MstMinM == "" && $MstMaxM == "" && $MstRange == 0){
    
                    $consulta = 'UPDATE Bales SET Mst_Mod = Mst WHERE Lot IN ('.$array.');'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Sumar valor especifico a un rango            
                }else if($MstMinO != "" && $MstMaxO != "" && $MstMinM == "" && $MstMaxM == "" && $MstRange != "" && $MstRange != 0){
    
                    $consulta = 'UPDATE Bales SET Mst_Mod = ROUND(Mst,1) + '.$MstRange.' WHERE Lot IN ('.$array.') and Mst BETWEEN '.$MstMinO.' and '.$MstMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Mover de rango a rango  (esperar confirmacion de como realizar dicha operacion)
                }else if($MstMinO != "" && $MstMaxO != "" && $MstMinM != "" && $MstMaxM != "" && $MstRange == ""){
    
                    $consulta = 'UPDATE Bales SET Mst_Mod = Round((select (RAND() * ('.$MstMaxM.'-'.$MstMinM.') + '.$MstMinM.')),2) WHERE Lot IN ('.$array.') and Round(Mst,2) BETWEEN '.$MstMinO.' and '.$MstMaxO.';';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                }
                
                /*$consulta = 'SELECT Bal, Mst FROM Bales WHERE Lot IN ('.$array.') Group by Mst;'; 
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);*/
            }
            //Mat
            if (in_array("Mat", $HVI)) {
                //Mandar valores a cero
                if ($MatMinO == 0 && $MatMaxO == "" && $MatMinM == 0 && $MatMaxM == "" && $MatRange == ""){

                    $consulta = 'UPDATE Bales SET Mat_Mod = 0 WHERE Lot IN ('.$array.');';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();        
                //Regresar valores a original    
                }else if($MatMinO != "" && $MatMaxO != "" && $MatMinM == "" && $MatMaxM == "" && $MatRange == 0){
    
                    $consulta = 'UPDATE Bales SET Mat_Mod = Mat WHERE Lot IN ('.$array.');'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Sumar valor especifico a un rango            
                }else if($MatMinO != "" && $MatMaxO != "" && $MatMinM == "" && $MatMaxM == "" && $MatRange != "" && $MatRange != 0){
    
                    $consulta = 'UPDATE Bales SET Mat_Mod = ROUND(Mat,2) + '.$MatRange.' WHERE Lot IN ('.$array.') and Mat BETWEEN '.$MatMinO.' and '.$MatMaxO.';'; 
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                //Mover de rango a rango  (esperar confirmacion de como realizar dicha operacion)
                }else if($MatMinO != "" && $MatMaxO != "" && $MatMinM != "" && $MatMaxM != "" && $MatRange == ""){
    
                    $consulta = 'UPDATE Bales SET Mat_Mod = Round((select (RAND() * ('.$MatMaxM.'-'.$MatMinM.') + '.$MatMinM.')),2) WHERE Lot IN ('.$array.') and Round(Mat,2) BETWEEN '.$MatMinO.' and '.$MatMaxO.';';
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                }
                
                /*$consulta = 'SELECT Bal, Mat FROM Bales WHERE Lot IN ('.$array.') Group by Mat;'; 
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();        
                $data=$resultado->fetchAll(PDO::FETCH_ASSOC);*/
            }
        }
        break;
        case 7:
          $consulta = "SELECT IDGin, GinName FROM Gines WHERE IDReg = '$Region' ORDER BY GinName;"; 
          $resultado = $conexion->prepare($consulta);
          $resultado->execute();
          $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
  
        break;
}

print json_encode($data, JSON_UNESCAPED_UNICODE);
$conexion=null;