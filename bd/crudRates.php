
<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set('America/Los_Angeles');
setlocale(LC_TIME, "spanish");

$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$IdRate = (isset($_POST['IdRate'])) ? $_POST['IdRate'] : '';
$zone = (isset($_POST['zone'])) ? $_POST['zone'] : '';
$estado = (isset($_POST['estado'])) ? $_POST['estado'] : '';
$promedio = (isset($_POST['promedio'])) ? $_POST['promedio'] : '';
$transport = (isset($_POST['transport'])) ? $_POST['transport'] : '';
$startdate = (isset($_POST['startdate'])) ? $_POST['startdate'] : '';
$fechafin = (isset($_POST['fechafin'])) ? $_POST['fechafin'] : '';
$city= (isset($_POST['city'])) ? $_POST['city'] : '';

$fecha = date("d/m/Y");

switch($opcion){
    case 1:

        //CONSULTAR LAS RUTAS ASOCIADAS A UNA ZONA
        $consulta = "SELECT IFNULL(( SELECT DISTINCT IDReg FROM Gines WHERE Zone= '$zone' LIMIT 1) , 'N/A') as idreg";        
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $resultado= $resultado->fetch();
        $idreg=$resultado['idreg'];
   

        if($idreg == 'N/A'){
            $consulta2 = "SELECT DISTINCT IDReg           
            FROM Region where Zone='$zone' ";
            $resultado2 = $conexion->prepare($consulta2);
            $resultado2->execute();        
            $resultado2= $resultado2->fetch();
            $idreg=$resultado2['IDReg'];
        }


        $consulta3 = "SELECT  TptID as TransportID,
        (SELECT BnName FROM Transports where TptID = TransportID AND Transports.Status ='1') as Linea,
        (SELECT State FROM Cities where City = '$city') as Estado
        FROM Routes WHERE OutReg= '$idreg' Order BY Linea ASC";        
        $resultado3 = $conexion->prepare($consulta3);
        $resultado3->execute();    
        $data=$resultado3->fetchAll(PDO::FETCH_ASSOC);
        break;
      
    case 2:        
        //Consultar las zonas registradas en tablas Gines y Region
        $data = [];
        $consulta = "SELECT DISTINCT Zone FROM Region where Zone != ''";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();    
        foreach($resultado as $row){
            $zona = $row['Zone'];
            array_push($data, $zona);            
        }

        $consulta2 = "SELECT DISTINCT Zone FROM Gines where Zone != ''";
        $resultado2 = $conexion->prepare($consulta2);
        $resultado2->execute();            

        foreach($resultado2 as $row){
            array_push($data,$row['Zone']);            
        }
        sort($data);
        break;
    case 3:

       //CONSULTAR LAS CIUDADE ASICIADAS A UNA ESTADO
       $consulta = "SELECT  City FROM Cities where 	State = '$estado'";
       $resultado = $conexion->prepare($consulta);
       $resultado->execute();  
         
       $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
       sort($data);
  

        
        break;
    case 4:    
        $consulta = "SELECT IDRate,TransportID,Zone,City,TRUNCATE(AverageCost,2)as AverageCost,StartDate,EndDate,TRUNCATE((AverageCost*120),2) as Total, IF((STR_TO_DATE(EndDate, '%d/%m/%Y'))>(STR_TO_DATE('$fecha', '%d/%m/%Y')), 'Active', 'Inactive')as Status,
        (SELECT BnName FROM Transports where TptID = TransportID) as Linea        
         FROM Rates  ORDER BY IDRate DESC";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 5:    
       // $fechafin = date_format($fechafin,'Y-m-d' );

        $consulta = "SELECT TptID  FROM Transports WHERE BnName='$transport'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute(); 
        $resultado= $resultado->fetch();
        $idtransport=$resultado['TptID'];
        

        $consulta2 = "SELECT IDRate,TransportID,Zone,City FROM Rates WHERE TransportID='$idtransport' AND City='$city' AND Zone = '$zone' AND  (STR_TO_DATE('$fecha', '%d/%m/%Y') <= STR_TO_DATE(EndDate, '%d/%m/%Y'))";
        $resultado2 = $conexion->prepare($consulta2);
        $resultado2->execute();  
        $resultado2= $resultado2->fetch();
       
        
        if(empty($resultado2)){
            
            $consulta3 = "INSERT INTO Rates (TransportID,Zone,City,AverageCost,StartDate,EndDate) VALUES ('$idtransport','$zone','$city','$promedio','$startdate','$fechafin')";
            $resultado3 = $conexion->prepare($consulta3);
            $resultado3->execute();  

            $data ="DATOS REGISTRADOS";

        }

        else{
            $data ="REGISTRO YA EXISTENTE CON EL ID RATE ".$resultado2['IDRate'];
        }

    break;

    case 6:

        $consulta = "SELECT  State FROM Cities where City = '$city' ";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();    
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);



    break;


    case 7:

        $consulta = "SELECT TptID  FROM Transports WHERE BnName='$transport'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute(); 
        $resultado= $resultado->fetch();
        $idtransport=$resultado['TptID'];


        $consulta2 = "SELECT IDRate,TransportID,Zone,City FROM Rates WHERE TransportID='$idtransport' AND City='$city' AND Zone = '$zone' AND IDRate !='$IdRate' AND  (STR_TO_DATE('$fecha', '%d/%m/%Y') <= STR_TO_DATE(EndDate, '%d/%m/%Y')) ";
        $resultado2 = $conexion->prepare($consulta2);
        $resultado2->execute();  
        $resultado2= $resultado2->fetch();
       
        
        if(empty($resultado2)){
            
            $consulta3 = "UPDATE Rates SET AverageCost='$promedio',TransportID='$idtransport',StartDate='$startdate',EndDate='$fechafin' WHERE IDRate='$IdRate'";		
            $resultado3 = $conexion->prepare($consulta3);
            $resultado3->execute();
            $data ="DATOS REGISTRADOS";

        }

        else{
            $data ="REGISTRO YA EXISTENTE CON EL ID RATE ".$resultado2['IDRate'];
        }




    break;

    case 8:
        $fecha_actual = date("d-m-Y");
        $dia_anterior=date("d-m-Y",strtotime($fecha_actual."-1 days"));
        $dia_anterior=str_replace('-','/',$dia_anterior);
        
        $consulta = "UPDATE Rates SET EndDate='$dia_anterior' WHERE IDRate='$IdRate'";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data ="RATE MODIFICADO ";

    
    break;
}

print json_encode($data, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX
$conexion=null;
