<?php
include_once('correo-model.php');

  $numDO = (!empty($_REQUEST['numDO'])) ? $_REQUEST['numDO'] : '';
  $QtyDO = (!empty($_REQUEST['QtyDO'])) ? $_REQUEST['QtyDO'] : '';
  $GinDO = (!empty($_REQUEST['GinDO'])) ? $_REQUEST['GinDO'] : '';
  $RegDO = (!empty($_REQUEST['RegDO'])) ? $_REQUEST['RegDO'] : '';








//Tabla de lotes asociadas a DO
$consulta = 'SELECT DOrds.DOrd, DOrds.Qty as QtyDO, Lots.Lot, DOrds.OutPlc as Loc, Lots.Qty as QtyLot, Region.RegNam as Region, Gines.GinName as Gin
             FROM DOrds, Lots, Region, Gines
             WHERE DOrds.DOrd = '.$numDO.' and Lots.DOrd = DOrds.DOrd and Region.IDReg = DOrds.OutPlc and Gines.IDGin = Lots.GinID;';
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchALL();

//$lotsAssociated = getLots($numDO);
if(!empty($data)){
    $tabla = '<table style="margin-left: 1em; border-collapse: collapse; text-align= center;">';
    foreach ($data as $lot) {
        $tabla .= '<tr><td>' . $lot['Lot'] . '</td><td>-</td><td>' . $lot['QtyLot'] . ' bc </td>';
        $gin = $lot['Gin'];
        $QtyOE = $lot['QtyDO'];
        $regO = $lot['Region'];
        $location = $lot['Loc'];
    }

    $tabla .= '</table>';
} else {
    $tabla = '';
    $gin = $GinDO;
    $regO = $RegDO;
}

//Emails predeterminados para la región de salida y llegada

//Emails predeterminados para la región de salida y llegada
$bodegaorigen  = getalmacenorigen($regO);

if ($bodegaorigen['IsWHOrigin'] == 1){
    $emailsDestino = getmailsbodegas($regO);
}
else{
    $emailsDestino = getDestinoGines($gin);
} 

$emailsD = $emailsDestino['mails'];

$emailDestino= array_unique($emailDestino = explode(",", $emailsD) , SORT_STRING);
$emailDestino = implode(",",$emailDestino);

//Correos AMSA en CC
$emailsCC = getOrigenReg($regO);

$emailsAMSA = $emailsCC['MailsAMSA'];

$emailCC= array_unique($emailCC = explode(",", $emailsAMSA) , SORT_STRING);
$emailCC = implode(",",$emailCC);

//VALIDAR SI SALE DE UN GIN O DE UNA BODEGA QUE ESTA EN EL GIN
$tiporigen=get_tiporigen($regO);
//print_r($tiporigen);


//Asunto del correo
$asunto = "Listado DO: " . $numDO . " (" . $QtyDO . " bc) - " . $gin. ""; //"Listado la siguiente DO: " . $numDO . ", cantidad total: " . $QtyDO . " bc"; //

if($tiporigen[0]['IsOrigin']==1 && $tiporigen[0]['IsWHOrigin']==0){//VALIDAMOS SI SALE DE UN ORIGEN

    //Tabla para validar que QtyDO, QtyLots y QtyBales sean iguales
$consulta = 'SELECT DOrds.DOrd, DOrds.Qty as QtyDO,
(Select sum(Lots.Qty) FROM Lots where Lots.DOrd = DOrds.DOrd) as QtyLot,
(Select count(Bales.Bal) FROM Bales Where Bales.DO = DOrds.DOrd) as QtyBales
FROM DOrds
WHERE DOrds.DOrd = '.$numDO.';';
$resultado = $conexion->prepare($consulta);
$resultado->execute();
$dataQty=$resultado->fetchALL();

    if(!empty($dataQty)){
        foreach ($dataQty as $BalQty) {
            $cantDO = $BalQty['QtyDO'];
            $cantLot = $BalQty['QtyLot'];
            $cantBal = $BalQty['QtyBales'];
        }
        if($cantDO == $cantLot && $cantDO == $cantBal){
            $data['validar'] = "completo";
        }else{
            $data['validar'] = "incompleto";
        }
    }else{
        $data['validar'] = "incompleto";
    }

}

else if($tiporigen[0]['IsOrigin']==0 && $tiporigen[0]['IsWHOrigin']==1){//VALIDAMOS SI SALE DE UNA BODEGA ORIGEN

    //Asunto del correo si sale de bodega
    $asunto = "DO: " . $numDO . " (" . $QtyDO . " bc) - " . $regO. " "; //"Listado la siguiente DO: " . $numDO . ", cantidad total: " . $QtyDO . " bc"; //

    $consulta = 'SELECT DOrds.DOrd, DOrds.Qty as QtyDO,
            (Select sum(Lots.Qty) FROM Lots where Lots.DOrd = DOrds.DOrd) as QtyLot            
            FROM DOrds
            WHERE DOrds.DOrd = '.$numDO.';';
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataQty2=$resultado->fetchALL();


    if(!empty($dataQty2)){
        foreach ($dataQty2 as $BalQty) {
            $cantDO = $BalQty['QtyDO'];
            $cantLot = $BalQty['QtyLot'];
           // $cantBal = $BalQty['QtyBales'];
        }
        if($cantDO == $cantLot &&  $tiporigen[0]['IsWHOrigin']==1){
            $data['validar'] = "completo";
        }else{
            $data['validar'] = "incompleto";
        }
    }else{
        $data['validar'] = "incompleto";
    }
}


else {//CASO CUANDO YA NO SON ORIGEN NI BODEGA DE ORIGEN

    //Tabla para validar que QtyDO, QtyLots y QtyBales sean iguales
$consulta = 'SELECT DOrds.DOrd, DOrds.Qty as QtyDO,
(Select sum(Lots.Qty) FROM Lots where Lots.DOrd = DOrds.DOrd) as QtyLot,
(Select count(Bales.Bal) FROM Bales Where Bales.DO = DOrds.DOrd) as QtyBales
FROM DOrds
WHERE DOrds.DOrd = '.$numDO.';';
$resultado = $conexion->prepare($consulta);
$resultado->execute();
$dataQty=$resultado->fetchALL();

    if(!empty($dataQty)){
        foreach ($dataQty as $BalQty) {
            $cantDO = $BalQty['QtyDO'];
            $cantLot = $BalQty['QtyLot'];
            $cantBal = $BalQty['QtyBales'];
        }
        if($cantDO == $cantLot && $cantDO == $cantBal){
            $data['validar'] = "completo";
        }else{
            $data['validar'] = "incompleto";
        }
    }else{
        $data['validar'] = "incompleto";
    }

}


$data['location']=$location;
$data['asunto']=$asunto;
$data['emails']=$emailDestino;
$data['emailsCC']=$emailCC;
$data['tabla'] = $tabla;
$data['RegionOri'] = $regO;
//agregamos la bandera para ver si es bodega de un origen
$data['IsWHOrigin'] = $tiporigen[0]['IsWHOrigin'];

print json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);//envio el array final el formato json a AJAX
$conexion=null;

?>