<?php

$Date = $_GET['Date'];
$Type = $_GET['Type'];
$DO = $_GET['DO'];
$Ctc = $_GET['Ctc'];
$Out = $_GET['Out'];
$In = $_GET['In'];
$Gin = $_GET['Gin'];
$Cli = $_GET['Cli'];
$Crss = $_GET['Crss'];
$PurNo = $_GET['PurNo'];

include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

include_once '../fpdf/fpdf.php';


$pdf = new FPDF();
$pdf->AddPage('portrait');
$pdf->SetTitle($DO);
$pdf->SetFont('Arial','B',10);
$pdf->Image('../img/logo1.png', 35, 10, 20, 20, 'PNG');
$pdf->Cell(0,10, utf8_decode('AGROINDUSTRIAS UNIDAS DE MEXICO S.A. DE C.V.'),0,0,'C');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0,10,'COTTON DIVISION', 0, 0,'C');
$pdf->Ln(18);
$pdf->Cell(150, 8, 'Delivery Order: ', 0, 0, 'R');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, $DO, 0, 0, 'L');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(150, 8, 'Type: ', 0, 0, 'R');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, $Type, 0, 0, 'L');
$pdf->Ln(11);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0, 8, 'GENERAL INFORMATION', 0, 0, 'L');
$pdf->Ln(8);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(32, 8, 'Date: ', 0, 0, 'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(61, 8, strtoupper(date('d/M/Y', strtotime($Date))), 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
if ($Type != "CON"){
$pdf->Cell(22, 8, 'Contract: ', 0, 0, 'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(75, 8, $Ctc, 0, 0, 'L');}
$pdf->Ln(6);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(32, 8, 'Departure Region: ', 0, 0, 'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(61, 8, $Out, 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
if ($Type != "CON"){
$pdf->Cell(22, 8, 'Client: ', 0, 0, 'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(75, 8, $Cli, 0, 0, 'L');}
$pdf->Ln(6);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(32, 8, 'Arrival Region: ', 0, 0, 'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(61, 8, $In, 0, 0, 'L');
$pdf->Ln(6);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(32, 8, 'Gin: ', 0, 0, 'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(61, 8, $Gin, 0, 0, 'L');
if ($Out == "USA"){
$pdf->Ln(6);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(32, 8, 'Purchase No: ', 0, 0, 'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(61, 8, $PurNo, 0, 0, 'L');
$pdf->Ln(6);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(32, 8, 'Crossing Date: ', 0, 0, 'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(61, 8, strtoupper(date('d/M/Y', strtotime($Crss))), 0, 0, 'L');}


$pdf->Ln(10);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0, 8, 'DELIVERY DETAILS', 0, 0, 'L');
$pdf->Ln(8);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(40, 6, 'Lot ', 'B', 0, 'C');
$pdf->Cell(20, 6, 'B/C ', 'B', 0, 'C');
$pdf->Cell(45, 6, 'SCHEDULED ARRIVAL DATE ', 'B', 0, 'C');
$pdf->Ln(6);
$pdf->SetFont('Arial','',9);
//consulta Lotes
$consulta = "SELECT Lot, Qty, SchDate FROM Lots WHERE DOrd = '$DO' order by Lot;"; 
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$data=$resultado->fetchAll(PDO::FETCH_ASSOC);
//$pdf->Cell(40, 8, $data, 1, 0, 'C');
foreach($data as $row){
    $pdf->Cell(40, 8, $row['Lot'], 0, 0, 'C');
    $pdf->Cell(20, 8, $row['Qty'], 0, 0, 'C');
    if ($row['SchDate'] != ""){
    $pdf->Cell(45, 8,  strtoupper(date('d-M-y', strtotime($row['SchDate']))), 0, 0, 'C');
    }else{
    $pdf->Cell(45, 8, "", 0, 0, 'C');    
    }
    $pdf->Ln(4);    
}
$pdf->Ln(2);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(40, 8, 'Total: ', 'T', 0, 'C');
//suma pacas
$consulta = "SELECT SUM(Qty) as Total FROM Lots WHERE DOrd = '$DO'"; 
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$data=$resultado->fetch();
$pdf->Cell(20, 8, $data['Total'], 'T', 0, 'C');
$pdf->Cell(45, 8, '', 'T', 0, 'C');

//COMENTARIOS DELIVERY AMERICANA
$pdf->Ln(10);

if ($Type != "CON"){
    $consulta = "SELECT * FROM Clients WHERE Cli = '$Cli'";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataCliIn=$resultado->fetch();
    $Drc = $dataCliIn['Drctn'];
    $State = $dataCliIn['State'];
    $Town = $dataCliIn['Town'];
    $CP = $dataCliIn['CP'];
    $Cty = $dataCliIn['Cty'];
}else{
    $consulta = "SELECT * FROM Region WHERE RegNam = '$In'";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();       
    $dataRegIn=$resultado->fetch();
    $Drc = $dataRegIn['Drctn'];
    $State = $dataRegIn['State'];
    $Town = $dataRegIn['Town'];
    $CP = $dataRegIn['CP'];
    $Cty = $dataRegIn['Cty'];
}

if($Type != "EXP"){
  $pdf->SetFont('Arial','B',9);
  $pdf->Cell(36, 6, 'DELIVERY LOCATION', 0, 0, 'L');
  $pdf->Ln(6);
  $pdf->SetFont('Arial','',9);
  $pdf->MultiCell(0, 4, strtoupper(utf8_decode($Drc)),  0, 'L', false);
  //$pdf->Ln(3);
  $pdf->SetFont('Arial','',9);
  $pdf->MultiCell(0, 4, strtoupper(utf8_decode("CP: ".$CP.", ".$Town)),  0, 'L', false);
  //$pdf->Ln(3);
  $pdf->SetFont('Arial','',9);
  $pdf->MultiCell(0, 4, strtoupper(utf8_decode($State.", ".$Cty)),  0, 'L', false);
}
//$pdf->MultiCell(0, 6, utf8_decode('CARRETERA A LA RESURRECCION NO. 1002, ZONA INDUSTRIAL CAMINO A MANZANILLO, PUEBLA, MEXICO.'),  0, 'L', false);

//Agregar Notes
$consulta = "SELECT PDF FROM DOrds WHERE DOrd = '$DO'"; 
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$data=$resultado->fetch();

if ($data['PDF'] != null){
$pdf->Ln(8);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0, 8, 'NOTES', 0, 0, 'L');
$pdf->Ln(6);
$pdf->SetFont('Arial','',9);
$pdf->MultiCell(0, 5, $data['PDF'], 0, 'L', false);}

$pdf->Ln(6);


$pdf->Output('I', $DO.".pdf");

$conexion=null;

?>

