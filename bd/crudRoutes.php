<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

$TptCo = (isset($_POST['TptCo'])) ? $_POST['TptCo'] : '';
$BnName = (isset($_POST['BnName'])) ? $_POST['BnName'] : '';
$OutReg = (isset($_POST['OutReg'])) ? $_POST['OutReg'] : '';

$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$TptID = (isset($_POST['TptID'])) ? $_POST['TptID'] : '';


switch($opcion){
    case 1:
        $consulta = "INSERT INTO Transports (TptCo, BnName, CAAT) VALUES('$TptCo', '$BnName', '') ";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute(); 
        
        $consulta = "SELECT * FROM Transports ORDER BY TptID DESC LIMIT 1";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);       
        break;    
    case 2:        
        $consulta = "UPDATE Invs SET InvID='$InvID', DO='$DO', Contract='$CtcInv', Lot='$Lot', Qty='$Qty', Unt='$Qty', InvDat='$InvDat', InvWgh='$WghGss', InvWghNet='$WghNet', InvAmt='$Amt', Price='$Price', PosDat='$PosDat', InvSta='$InvSts', InvCmt='$InvCmt' WHERE InvID='$InvID' ";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        
        $consulta = "SELECT InvID, DO, Contract, Lot, Qty, InvDat, InvWgh, InvWghNet, InvAmt, Price, PosDat, InvSta, InvCmt FROM Invs WHERE InvID='$InvID' ";       
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 3:        
        $consulta = "SELECT * FROM Transports Order By TptCo"; //"DELETE FROM Lots WHERE LotID='$LotID' ";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 4:    
        $consulta = "SELECT RteID, OutReg, InReg, TptID, 
        (SELECT RegNam FROM Region WHERE Region.IDReg = Routes.OutReg) as RegName,
        (SELECT TptCo FROM Transports WHERE Transports.TptID = Routes.TptID) as TptCo,
        (SELECT BnName FROM Transports WHERE Transports.TptID = Routes.TptID) as BnName
        FROM Routes;";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 5:    
        $consulta = "SELECT RteID, OutReg, RegNam FROM Routes, Region WHERE TptID='$TptCo' and IDReg=OutReg Order by RegNam;";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 6:    
        $consulta = "SELECT IDReg, RegNam FROM Region Order By RegNam;";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 7:    
        $consulta = "INSERT INTO Routes (OutReg, InReg, TptID, Dtime) VALUES('$OutReg', '0', '$TptCo', '0');";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
}

print json_encode($data, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX
$conexion=null;
