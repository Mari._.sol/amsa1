<?php

include_once('conexion.php');
$objeto = new Conexion();
$conexion = $objeto->Conectar();

function getAuth($user){
    global $conexion;
    $query = "SELECT email, passApp FROM Users WHERE User = :usuario";
    $result =$conexion->prepare($query);
    $result->execute(['usuario'=>$user]);
    return $result->fetch(PDO::FETCH_ASSOC);
}

function getLots($numDO){
    global $conexion;
    $query = 'SELECT Lot, Qty, SchDate  FROM Lots WHERE DOrd = :numDO';
    $result = $conexion->prepare($query);
    $result->execute(['numDO'=>$numDO]);
    return $result->fetchAll(PDO::FETCH_ASSOC);
}

function getDestinoC($client){
    global $conexion;
    $query = 'SELECT mails FROM Clients WHERE Cli =:client';
    $result = $conexion->prepare($query);
    $result->execute(['client'=>$client]);
    return $result->fetch(PDO::FETCH_ASSOC);
}
function getDestinoR($RegNam){
    global $conexion;
    $query = 'SELECT mails FROM Region WHERE RegNam =:RegNam';
    $result = $conexion->prepare($query);
    $result->execute(['RegNam'=>$RegNam]);
    return $result->fetch(PDO::FETCH_ASSOC);
}


function muestrasDO($dord){
    global $conexion;
    $query = "SELECT Samp FROM DOrds WHERE Dord = :orden";
    $result =$conexion->prepare($query);
    $result->execute(['orden'=>$dord]);
    return $result->fetch(PDO::FETCH_ASSOC);
}


function getDestinoGines($GinName){
    global $conexion;
    $query = 'SELECT mails FROM Gines WHERE GinName =:GinName';
    $result = $conexion->prepare($query);
    $result->execute(['GinName'=>$GinName]);
    return $result->fetch(PDO::FETCH_ASSOC);
}

function getOrigenReg($regO){
    global $conexion;
    $query = 'SELECT MailsAMSA FROM Region WHERE RegNam =:regO';
    $result = $conexion->prepare($query);
    $result->execute(['regO'=>$regO]);
    return $result->fetch(PDO::FETCH_ASSOC);
}

//obtener correo para bodegas de origen 

function getmailsbodegas($regO){
    global $conexion;
    $query = 'SELECT MailWHOrigin as mails FROM Region WHERE RegNam =:regO';
    $result = $conexion->prepare($query);
    $result->execute(['regO'=>$regO]);
    return $result->fetch(PDO::FETCH_ASSOC);
}

function getalmacenorigen($regO){
    global $conexion;
    $query = 'SELECT IsWHOrigin FROM Region WHERE RegNam =:regO';
    $result = $conexion->prepare($query);
    $result->execute(['regO'=>$regO]);
    return $result->fetch(PDO::FETCH_ASSOC);
}

// Modelos para trucks


function getRegiones(){
    global $conexion;

    $query = 'SELECT IDReg, RegNam, IsOrigin FROM Region ORDER BY RegNam ASC';
    $result = $conexion->prepare($query);
    $result->execute();
    return $result->fetchall();
}

function getRegionesO(){
    global $conexion;

    $query = 'SELECT IDReg, RegNam, IsOrigin FROM Region Where IsOrigin = 1 ORDER BY RegNam ASC';
    $result = $conexion->prepare($query);
    $result->execute();
    return $result->fetchall();
}

function getGines(){
    global $conexion;

    $query = 'SELECT IDGin, IDReg, GinName, BnName FROM Gines ORDER BY GinName ASC';
    $result = $conexion->prepare($query);
    $result->execute();
    return $result->fetchall();
}

function getSupplier(){
        global $conexion;

        $query = 'SELECT SupID, SupName FROM Supplier ORDER BY SupName ASC';
        $result = $conexion->prepare($query);
        $result->execute();
        return $result->fetchall();
    }

// Modelos para la vista de DO

    function getClients(){
        global $conexion;

        $query = 'SELECT CliID, Cli FROM Clients ORDER BY Cli ASC';
        $result = $conexion->prepare($query);
        $result->execute();
        return $result->fetchall();
    }

    //Modelo para verificar si existe el Transport en la BD
    function getTransport($BnName){
        global $conexion;

        $query = 'SELECT BnName FROM Transports where BnName = "'.$BnName.'"'; 
        $result = $conexion->prepare($query);
        $result->execute();
        return $result->fetch(PDO::FETCH_ASSOC);
    }

    // Modelo para traer el last price of Invoice
    function getLastPrice(){
        global $conexion; 
        $query = 'SELECT Price FROM Invs order by InvID DESC LIMIT 1';
        $result = $conexion->prepare($query);
        $result->execute();
        return $result->fetch(PDO::FETCH_ASSOC);
    }

     // Modelo para traer info de TrkID
     function getInfTrkID($RTrkID){
        global $conexion; 
        $query = 'SELECT DISTINCT DO, RqstID,CrgQty, PO, LotsAssc, Transports.BnName as TNam, DOrds.Typ as Typ, Region.RegNam as RegSalida,OutDat, Region1.RegNam as RegLlegada
        FROM Truks
        join DOrds on Truks.DO = DOrds.Dord
        left join Transports on Truks.TNam = Transports.TptID
        left join Lots on Truks.DO = Lots.DOrd and Truks.LotsAssc = Lots.Lot
        left join Region on DOrds.OutPlc = Region.IDReg
        left join Region as Region1 on DOrds.InReg = Region1.IDReg
        WHERE Truks.TrkID = "'.$RTrkID.'"';
        $result = $conexion->prepare($query);
        $result->execute();
        return $result->fetch(PDO::FETCH_ASSOC);
    }


    // Obtener el RegNam para validar los permisos de edicion en Trucks
    function getPrivReg($Priv){
        global $conexion;
        $array=array_map('intval', explode(',', $Priv));
        $array = implode(",",$array);
        $query = 'SELECT RegNam FROM Region WHERE IDReg IN ('.$array.')';
        $result = $conexion->prepare($query);
        $result->execute();
        return $result->fetchALL(PDO::FETCH_ASSOC);
    }

    // Obtener TrkID de lote seleccionado antes de asociar o desasociar
    function getTrkID($LotID){
        global $conexion; 
        $query = 'SELECT TrkID
        FROM Lots
        WHERE Lots.LotID = "'.$LotID.'"';
        $result = $conexion->prepare($query);
        $result->execute();
        return $result->fetch(PDO::FETCH_ASSOC);
    }
    
    //obtener todos los origenes
    
     function get_allOrigin(){
        global $conexion; 
        $query = 'SELECT Gines.GinName FROM Gines ORDER BY Gines.GinName ASC';
        $result = $conexion->prepare($query);
        $result->execute();
        return  $result->fetchall();
    }

    //FUNCION PARA SABER SI ES ORIGEN O SI ES BODEGA QUE ESTA EN UN ORIGEN 
    function get_tiporigen($regO){
        global $conexion; 
        $query = "SELECT Region.IsOrigin,Region.IsWHOrigin FROM Region WHERE RegNam = '$regO';";
        $result = $conexion->prepare($query);
        $result->execute();
        return $result->fetchALL(PDO::FETCH_ASSOC);
    }

    




    // Recibo el método, funcionaría para cualquier método que se genere sólo hay que pasarlo en la url como m o en la data de un ajax
    if(!empty($_REQUEST['m'])){ //verifico que exista algún método m sino no hace nada aunque llames al archivo
        $invocaMetodo = $_REQUEST['m'];
        if($invocaMetodo == "getTransport") //verifico que lo que recibí es este método para así llamarlo y me regrese la data necesaria
            $data = getTransport($_REQUEST['BnName']); //Paso el BnName para verificarlo en la base

        if($invocaMetodo == "getLastPrice")
            $data = getLastPrice();

        if($invocaMetodo == "getPrivReg")
            $data = getPrivReg($_REQUEST['Priv']);

        if($invocaMetodo == "getInfTrkID")
            $data = getInfTrkID($_REQUEST['RTrkID']);

        if($invocaMetodo == "getTrkID")
            $data = getTrkID($_REQUEST['LotID']);

        print json_encode($data, JSON_UNESCAPED_UNICODE);
    }


    //OBTENER LOS CORREOS DE EQUIPO DE ECOM USA SOLO SI LA REGION DE SALIDA ES UNA Y GIN USA TEXAS

function getMailsUSA($GINUSA){
    global $conexion;
    $query = 'SELECT mails FROM Gines WHERE GinName =:ginusa';
    $result = $conexion->prepare($query);
    $result->execute(['ginusa'=>$GINUSA]);
    return $result->fetch(PDO::FETCH_ASSOC);
}

//OBTENER LAS FECHAS DE LA DO

function getfechaDO($DO){
    global $conexion;
    $query = 'SELECT Date,Crss FROM amsadb1.DOrds where DOrd =:delivery';
    $result = $conexion->prepare($query);
    $result->execute(['delivery'=>$DO]);
    return $result->fetch(PDO::FETCH_ASSOC);
}


function getnamecliente(){
    global $conexion;

    $consulta = "Select Cli,CliID from amsadb1.Clients where UserApp = 1;" ;      
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    return $resultado->fetchAll(PDO::FETCH_ASSOC);
}

    //Generar el PDF de la DO y adjuntarla en correo solo si sale de USA

    function crearPDFDO($Date,$Type,$DO,$Ctc,$Out,$In,$Gin,$Cli,$Crss,$PurNo){
        include_once '../fpdf/fpdf.php';
        global $conexion; 
        $pdf = new FPDF();
        $pdf->AddPage('portrait');
        $pdf->SetTitle($DO);
        $pdf->SetFont('Arial','B',10);
        $pdf->Image('../img/logo1.png', 35, 10, 20, 20, 'PNG');
        $pdf->Cell(0,10, utf8_decode('AGROINDUSTRIAS UNIDAS DE MEXICO S.A. DE C.V.'),0,0,'C');
        $pdf->Ln(5);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(0,10,'COTTON DIVISION', 0, 0,'C');
        $pdf->Ln(18);
        $pdf->Cell(150, 8, 'Delivery Order: ', 0, 0, 'R');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0, 8, $DO, 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(150, 8, 'Type: ', 0, 0, 'R');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(0, 8, $Type, 0, 0, 'L');
        $pdf->Ln(11);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(0, 8, 'GENERAL INFORMATION', 0, 0, 'L');
        $pdf->Ln(8);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(32, 8, 'Date: ', 0, 0, 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(61, 8, strtoupper(date('d/M/Y', strtotime($Date))), 0, 0, 'L');
        $pdf->SetFont('Arial','B',9);
        if ($Type != "CON"){
        $pdf->Cell(22, 8, 'Contract: ', 0, 0, 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(75, 8, $Ctc, 0, 0, 'L');}
        $pdf->Ln(6);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(32, 8, 'Departure Region: ', 0, 0, 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(61, 8, $Out, 0, 0, 'L');
        $pdf->SetFont('Arial','B',9);
        if ($Type != "CON"){
        $pdf->Cell(22, 8, 'Client: ', 0, 0, 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(75, 8, $Cli, 0, 0, 'L');}
        $pdf->Ln(6);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(32, 8, 'Arrival Region: ', 0, 0, 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(61, 8, $In, 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(32, 8, 'Gin: ', 0, 0, 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(61, 8, $Gin, 0, 0, 'L');
        if ($Out == "USA"){
        $pdf->Ln(6);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(32, 8, 'Purchase No: ', 0, 0, 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(61, 8, $PurNo, 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(32, 8, 'Crossing Date: ', 0, 0, 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(61, 8, strtoupper(date('d/M/Y', strtotime($Crss))), 0, 0, 'L');}
        
        
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(0, 8, 'DELIVERY DETAILS', 0, 0, 'L');
        $pdf->Ln(8);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(40, 6, 'Lot ', 'B', 0, 'C');
        $pdf->Cell(20, 6, 'B/C ', 'B', 0, 'C');
        $pdf->Cell(45, 6, 'SCHEDULED ARRIVAL DATE ', 'B', 0, 'C');
        $pdf->Ln(6);
        $pdf->SetFont('Arial','',9);
        //consulta Lotes
        $consulta = "SELECT Lot, Qty, SchDate FROM Lots WHERE DOrd = '$DO' order by Lot;"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        //$pdf->Cell(40, 8, $data, 1, 0, 'C');
        foreach($data as $row){
            $pdf->Cell(40, 8, $row['Lot'], 0, 0, 'C');
            $pdf->Cell(20, 8, $row['Qty'], 0, 0, 'C');
            if ($row['SchDate'] != ""){
            $pdf->Cell(45, 8,  strtoupper(date('d-M-y', strtotime($row['SchDate']))), 0, 0, 'C');
            }else{
            $pdf->Cell(45, 8, "", 0, 0, 'C');    
            }
            $pdf->Ln(4);    
        }
        $pdf->Ln(2);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(40, 8, 'Total: ', 'T', 0, 'C');
        //suma pacas
        $consulta = "SELECT SUM(Qty) as Total FROM Lots WHERE DOrd = '$DO'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetch();
        $pdf->Cell(20, 8, $data['Total'], 'T', 0, 'C');
        $pdf->Cell(45, 8, '', 'T', 0, 'C');
        
        //COMENTARIOS DELIVERY AMERICANA
        $pdf->Ln(10);
        
        if ($Type != "CON"){
            $consulta = "SELECT * FROM Clients WHERE Cli = '$Cli'";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();        
            $dataCliIn=$resultado->fetch();
            $Drc = $dataCliIn['Drctn'];
            $State = $dataCliIn['State'];
            $Town = $dataCliIn['Town'];
            $CP = $dataCliIn['CP'];
            $Cty = $dataCliIn['Cty'];
        }else{
            $consulta = "SELECT * FROM Region WHERE RegNam = '$In'";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();       
            $dataRegIn=$resultado->fetch();
            $Drc = $dataRegIn['Drctn'];
            $State = $dataRegIn['State'];
            $Town = $dataRegIn['Town'];
            $CP = $dataRegIn['CP'];
            $Cty = $dataRegIn['Cty'];
        }
        
        if($Type != "EXP"){
          $pdf->SetFont('Arial','B',9);
          $pdf->Cell(36, 6, 'DELIVERY LOCATION', 0, 0, 'L');
          $pdf->Ln(6);
          $pdf->SetFont('Arial','',9);
          $pdf->MultiCell(0, 4, strtoupper(utf8_decode($Drc)),  0, 'L', false);
          //$pdf->Ln(3);
          $pdf->SetFont('Arial','',9);
          $pdf->MultiCell(0, 4, strtoupper(utf8_decode("CP: ".$CP.", ".$Town)),  0, 'L', false);
          //$pdf->Ln(3);
          $pdf->SetFont('Arial','',9);
          $pdf->MultiCell(0, 4, strtoupper(utf8_decode($State.", ".$Cty)),  0, 'L', false);
        }
        //$pdf->MultiCell(0, 6, utf8_decode('CARRETERA A LA RESURRECCION NO. 1002, ZONA INDUSTRIAL CAMINO A MANZANILLO, PUEBLA, MEXICO.'),  0, 'L', false);
        
        //Agregar Notes
        $consulta = "SELECT PDF FROM DOrds WHERE DOrd = '$DO'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetch();
        
        if ($data['PDF'] != null){
        $pdf->Ln(8);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(0, 8, 'NOTES', 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->SetFont('Arial','',9);
        $pdf->MultiCell(0, 5, $data['PDF'], 0, 'L', false);}
        
        $pdf->Ln(6);
        
        
        $filename = $DO.".pdf";
        
        $pdf->Output('F', './temp/'.$filename, true);
        
        }

?>
