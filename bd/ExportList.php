<?php

include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set("America/Mexico_City");

function lowercase($element)
{
    return "'".$element."'";
}

$DO = $_GET['DOCMS'];
$LotsCMS = $_GET['LotsCMS'];

//$array = array_map('lowercase', explode(',', $LotsCMS));
//$array = implode(",",$array);

//Si la búsqueda es por DO
if ($DO != ""){
    $Lots = array();
    $i = 0;
    $consulta = 'SELECT distinct Bales.Lot 
                 FROM Bales, DOrds, Lots 
                 
                 WHERE DOrds.DOrd = '.$DO.' and DOrds.DOrd = Lots.DOrd and Bales.Lot = Lots.Lot;'; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataDO=$resultado->fetchALL(PDO::FETCH_ASSOC);
    foreach($dataDO as $dat){
        $Lots[$i] = $dat['Lot'];
        $i++;
    }
    $array = array_map('lowercase',$Lots);
    $array = implode(",",$array);
}else{
    $array = array_map('lowercase', explode(',', $LotsCMS));
    $array = implode(",",$array);
}


// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


//Set variable to false for heading
$heading = false;

//Define the filename with current date
//$fileName = "Listado-".date('d-m-Y').".xlsx";

$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Hoja 1");
//$hojaActiva->freezePane("A2");

$hojaActiva->getColumnDimension('A')->setWidth(20);
$hojaActiva->setCellValue('A1','Gin');
$hojaActiva->getColumnDimension('B')->setWidth(15);
$hojaActiva->setCellValue('B1','Bal');
$hojaActiva->getColumnDimension('C')->setWidth(15);
$hojaActiva->setCellValue('C1','Lot');
$hojaActiva->getColumnDimension('D')->setWidth(15);
$hojaActiva->setCellValue('D1','LiqID');
$hojaActiva->getColumnDimension('E')->setWidth(15);
$hojaActiva->setCellValue('E1','Certificadas');

//Consulta para seleccionar lotes completos
$query= 'SELECT Crp, Gines.IDReg, Gines.CMSGin, Lot, Count(Bal) as Qty, CEILING(Sum(Wgh)) as Wgh, (Sum(if(LiqID != "" ,1,0))) as LiqBal
         FROM Bales
         join Gines on Gines.IDGin = Bales.GinID
         WHERE Bales.Lot IN ('.$array.')
         Group by Lot;';
$result = $conexion->prepare($query);
$result->execute();

$i = 0;
$LotsAll = array(); // Inicializa $LotsAll como un array vacío
while($row = $result->fetch(PDO::FETCH_ASSOC)){
    if ($row['LiqBal'] == $row['Qty']){
        $LotsAll[$i] = $row['Lot'];
        $i++;
    }
}

$array2 = array_map('lowercase',$LotsAll);
$array2 = implode(",",$array2);

$query= 'SELECT DISTINCT Crp, Gines.GinName, Gines.IDReg, Gines.CMSGin, Bales.Lot, Bal, LiqID, Lots.Cert 
         FROM Bales
         join Gines on Gines.IDGin = Bales.GinID
         JOIN Lots ON Lots.Lot = Bales.Lot
         WHERE Bales.Lot IN ('.$array.') 
         Order By Lot, Bal;';
$result = $conexion->prepare($query);
$result->execute();
$siexiste=0; //Para verificar que hayan datos  join Lots on Bales.Lot = Lots.Lot

$fila = 2;

while($row = $result->fetch(PDO::FETCH_ASSOC)){
    $Cert = ($row['Cert'] == 1) ? "SI" : "NO";

    $hojaActiva->setCellValue('A' . $fila, $row['GinName']);
    $hojaActiva->setCellValue('B' . $fila, $row['Bal']);
    $hojaActiva->setCellValue('C' . $fila, $row['Lot']);
    $hojaActiva->setCellValue('D' . $fila, ($row['LiqID'] != "") ? $row['LiqID'] : "PENDIENTE");
    $hojaActiva->setCellValue('E' . $fila, $Cert);

    $fila++;
    $GinName = $row['GinName'];
}

//Define the filename with current date
$fileName = "Listado_".$GinName."_".date('d-m-Y').".xlsx";


// redirect output to client browser
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');

$writer = IOFactory::createWriter($excel, 'Xlsx');
$writer->save('php://output');
exit;

?>
