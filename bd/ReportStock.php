<?php

set_time_limit(1200);

include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set("America/Mexico_City");

function lowercase($element)
{
    return "'".$element."'";
}


$Region = $_GET['Reg'];

$array = array_map('lowercase', explode(',', $Region));
$Reg = implode(",",$array);

// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;



//Set variable to false for heading
$heading = false;

//Define the filename with current date
$fileName = "ReportStock-".date('d-m-Y').".xlsx";

$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Hoja 1");
//$hojaActiva->freezePane("A2");

$hojaActiva->getColumnDimension('A')->setWidth(20);
$hojaActiva->setCellValue('A1','Location');
$hojaActiva->getColumnDimension('B')->setWidth(15);
$hojaActiva->setCellValue('B1','Crop');
$hojaActiva->getColumnDimension('C')->setWidth(15);
$hojaActiva->setCellValue('C1','Gin');
$hojaActiva->getColumnDimension('D')->setWidth(15);
$hojaActiva->setCellValue('D1','Lot');
$hojaActiva->getColumnDimension('E')->setWidth(15);
$hojaActiva->setCellValue('E1','Qty');
$hojaActiva->getColumnDimension('F')->setWidth(15);
$hojaActiva->setCellValue('F1','DO');
$hojaActiva->getColumnDimension('G')->setWidth(15);
$hojaActiva->setCellValue('G1','Truck ID');
$hojaActiva->getColumnDimension('H')->setWidth(25);
$hojaActiva->setCellValue('H1','Scheduled Departure Date');
$hojaActiva->getColumnDimension('I')->setWidth(25);
$hojaActiva->setCellValue('I1','Scheduled Arrival Date');
$hojaActiva->getColumnDimension('J')->setWidth(25);
$hojaActiva->setCellValue('J1','Assigned To');

//Consulta para seleccionar lotes completos
/*$query= 'SELECT R.RegNam as Location, Crop, G.GinName as Gin, L.Lot, L.Qty, L.DOrd as DO, SchDate, C.Cli as Cli
        FROM Lots L
        JOIN Region R on L.Location = R.IDReg
        JOIN Gines G on L.GinID = G.IDGin
        LEFT JOIN Clients C on C.CliID = L.CliID
        WHERE L.Location LIKE "'.$Reg.'%"  and L.TrkID = 0 Order by Gin, Lot;'; //in ('.$Reg.')
$result = $conexion->prepare($query);
$result->execute();*/

/*$i = 0;
//$LotsALL = array();
while($row = $result->fetch(PDO::FETCH_ASSOC)){
        $LotsAll[$i] = $row['Lot'];
        $i++;
}

$array2 = array_map('lowercase',$LotsAll);
$array2 = implode(",",$array2);

$query= 'SELECT G.GinName as Gin, B.Lot, count(B.Bal) as Qty, B.DO, S.SupName 
         FROM Bales B
         JOIN Gines G on G.IDGin = B.GinID
         JOIN Supplier S on S.SupID = B.SupID
         WHERE B.Lot IN ('.$array2.') Group by Lot
         Order By Gin, Lot;';
         
/*$query= 'SELECT DISTINCT G.GinName, B.Lot, count(B.Bal) as Qty, B.DO, S.SupName
        FROM Bales B
        JOIN Gines G on G.IDGin = B.GinID
        JOIN Supplier S on S.SupID = B.SupID
        JOIN Lots L on B.Lot = L.Lot and L.Location in ('.$Reg.') AND L.TrkID = 0
        Group by B.Lot
        ORDER BY GinName, Bal, Lot;';*/
         
/*Comentario 10 de marzo 2023
$result = $conexion->prepare($query);
$result->execute();
$siexiste=0; //Para verificar que hayan datos  join Lots on Bales.Lot = Lots.Lot


$fila = 2;

while($row = $result->fetch(PDO::FETCH_ASSOC)){
    $hojaActiva->setCellValue('A' . $fila,$row['Location']);
    $hojaActiva->setCellValue('B' . $fila,$row['Crop']);
    $hojaActiva->setCellValue('C' . $fila,$row['Gin']);
    $hojaActiva->setCellValue('D' . $fila,$row['Lot']);
    $hojaActiva->setCellValue('E' . $fila,$row['Qty']);
    $hojaActiva->setCellValue('F' . $fila,$row['DO']);
    $hojaActiva->setCellValue('G' . $fila,"");
    $hojaActiva->setCellValue('H' . $fila,$row['SchDate']); //$arrivalDate = new DateTime($row['InDat'])
    $hojaActiva->setCellValue('I' . $fila,"");
    $hojaActiva->setCellValue('J' . $fila,$row['Cli']);
    $fila++;
}
*/

//Lotes con truck estatus programado----------------------------------------------------------------
/*$query= 'SELECT R.RegNam as Location, L.Crop, G.GinName as Gin, L.Lot, L.Qty, L.DOrd as DO, T.TrkID, T.SchOutDate, T.SchInDate, C.Cli
        FROM Lots L
        JOIN Region R on L.Location = R.IDReg
        JOIN Gines G on L.GinID = G.IDGin
        JOIN DOrds D on L.DOrd = D.DOrd
        LEFT JOIN Clients C on C.CliID = D.InPlc
        JOIN Truks T on L.TrkID = T.TrkID and T.Status = "Programmed"
        WHERE L.Location LIKE "'.$Reg.'%" Order by Gin, Lot;'; //in ('.$Reg.')
$result = $conexion->prepare($query);
$result->execute();*/

/*$i = 0;
//$LotsALL = array();
while($row = $result->fetch(PDO::FETCH_ASSOC)){
        $LotsAll[$i] = $row['Lot'];
        $i++;
}

$array2 = array_map('lowercase',$LotsAll);
$array2 = implode(",",$array2);

$query= 'SELECT G.GinName as Gin, B.Lot, count(B.Bal) as Qty, B.DO, S.SupName 
         FROM Bales B
         JOIN Gines G on G.IDGin = B.GinID
         JOIN Supplier S on S.SupID = B.SupID
         WHERE B.Lot IN ('.$array2.') Group by Lot
         Order By Gin, Lot;';*/
         
/*$result = $conexion->prepare($query);
$result->execute();
$siexiste=0; //Para verificar que hayan datos  join Lots on Bales.Lot = Lots.Lot

while($row = $result->fetch(PDO::FETCH_ASSOC)){
    $hojaActiva->setCellValue('A' . $fila,$row['Location']);
    $hojaActiva->setCellValue('B' . $fila,$row['Crop']);
    $hojaActiva->setCellValue('C' . $fila,$row['Gin']);
    $hojaActiva->setCellValue('D' . $fila,$row['Lot']);
    $hojaActiva->setCellValue('E' . $fila,$row['Qty']);
    $hojaActiva->setCellValue('F' . $fila,$row['DO']);
    $hojaActiva->setCellValue('G' . $fila,$row['TrkID']);
    $hojaActiva->setCellValue('H' . $fila,$row['SchOutDate']);
    $hojaActiva->setCellValue('I' . $fila,$row['SchInDate']);
    $hojaActiva->setCellValue('J' . $fila,$row['Cli']);
    $fila++;
}*/


//Bales sin Lotes ----------------------------------------------------------------------------------------
$fila = 2;
$array = explode(",",$Region);

foreach ($array as $dato){
    //Sin Trk
    $query= 'SELECT R.RegNam as Location, Crop, G.GinName as Gin, L.Lot, L.Qty, L.DOrd as DO, SchDate, C.Cli as Cli
        FROM Lots L
        JOIN Region R on L.Location = R.IDReg
        JOIN Gines G on L.GinID = G.IDGin
        LEFT JOIN Clients C on C.CliID = L.CliID
        WHERE L.Location LIKE "'.$dato.'%"  and L.TrkID = 0 Order by Gin, Lot;'; //in ('.$Reg.')
    $result = $conexion->prepare($query);
    $result->execute();
    
    $result = $conexion->prepare($query);
    $result->execute();
    $siexiste=0; //Para verificar que hayan datos  join Lots on Bales.Lot = Lots.Lot

    while($row = $result->fetch(PDO::FETCH_ASSOC)){
      if ($row['SchDate'] != ""){
        $fecha_salida = strtotime($row['SchDate']);
        $fecha_salida =25569 + ($fecha_salida / 86400);
      }else{ $fecha_salida = ""; }
      $hojaActiva->setCellValue('A' . $fila,$row['Location']);
      $hojaActiva->setCellValue('B' . $fila,$row['Crop']);
      $hojaActiva->setCellValue('C' . $fila,$row['Gin']);
      $hojaActiva->setCellValue('D' . $fila,$row['Lot']);
      $hojaActiva->setCellValue('E' . $fila,$row['Qty']);
      $hojaActiva->setCellValue('F' . $fila,$row['DO']);
      $hojaActiva->setCellValue('G' . $fila,"");
      $hojaActiva->setCellValue('H' . $fila, $fecha_salida); //$fila,$row['SchDate']); //$arrivalDate = new DateTime($row['InDat'])
      $hojaActiva->setCellValue('I' . $fila,"");
      $hojaActiva->setCellValue('J' . $fila,$row['Cli']);
      $fila++;
    }
    
    
    //Trk Programmed
    $query= 'SELECT R.RegNam as Location, L.Crop, G.GinName as Gin, L.Lot, L.Qty, L.DOrd as DO, T.TrkID, T.SchOutDate, T.SchInDate, C.Cli
        FROM Lots L
        JOIN Region R on L.Location = R.IDReg
        JOIN Gines G on L.GinID = G.IDGin
        JOIN DOrds D on L.DOrd = D.DOrd
        LEFT JOIN Clients C on C.CliID = D.InPlc
        JOIN Truks T on L.TrkID = T.TrkID and T.Status = "Programmed"
        WHERE L.Location LIKE "'.$Reg.'%" Order by Gin, Lot;'; //in ('.$Reg.')
    $result = $conexion->prepare($query);
    $result->execute();

         
    $result = $conexion->prepare($query);
    $result->execute();
    $siexiste=0; //Para verificar que hayan datos  join Lots on Bales.Lot = Lots.Lot
    
    while($row = $result->fetch(PDO::FETCH_ASSOC)){
        if ($row['SchOutDate'] != ""){
            $fecha_salida = strtotime($row['SchOutDate']);
            $fecha_salida =25569 + ($fecha_salida / 86400);
        }else{ $fecha_salida = ""; }
        if ($row['SchInDate'] != ""){
            $fecha_llegada = strtotime($row['SchInDate']);
            $fecha_llegada =25569 + ($fecha_salida / 86400);
        }else{ $fecha_llegada = ""; }
        $hojaActiva->setCellValue('A' . $fila,$row['Location']);
        $hojaActiva->setCellValue('B' . $fila,$row['Crop']);
        $hojaActiva->setCellValue('C' . $fila,$row['Gin']);
        $hojaActiva->setCellValue('D' . $fila,$row['Lot']);
        $hojaActiva->setCellValue('E' . $fila,$row['Qty']);
        $hojaActiva->setCellValue('F' . $fila,$row['DO']);
        $hojaActiva->setCellValue('G' . $fila,$row['TrkID']);
        $hojaActiva->setCellValue('H' . $fila,$fecha_salida); //$fila,$row['SchOutDate']);
        $hojaActiva->setCellValue('I' . $fila,$fecha_llegada); //$row['SchInDate']);
        $hojaActiva->setCellValue('J' . $fila,$row['Cli']);
        $fila++;
    }

    //Sin Lote
    $query= 'SELECT R.RegNam as Location, B.Crp as Crop, G.GinName as Gin, B.Lot, count(B.Bal) as Qty, B.DO, S.SupName
            FROM Bales B
            JOIN Gines G on G.IDGin = B.GinID
            JOIN Region R on R.IDReg = '.$dato.'
            JOIN Supplier S on S.SupID = B.SupID
            WHERE B.GinID LIKE "'.$dato.'%" and B.Lot = "" 
            group by GinName Order by Gin;';

    $result = $conexion->prepare($query);
    $result->execute();
    $siexiste=0; //Para verificar que hayan datos  join Lots on Bales.Lot = Lots.Lot //$row['Lot']

    while($row = $result->fetch(PDO::FETCH_ASSOC)){
        $hojaActiva->setCellValue('A' . $fila,$row['Location']);
        $hojaActiva->setCellValue('B' . $fila,$row['Crop']);
        $hojaActiva->setCellValue('C' . $fila,$row['Gin']);
        $hojaActiva->setCellValue('D' . $fila,"S/L");
        $hojaActiva->setCellValue('E' . $fila,$row['Qty']);
        $hojaActiva->setCellValue('F' . $fila,$row['DO']);
        $hojaActiva->setCellValue('G' . $fila,"");
        $hojaActiva->setCellValue('H' . $fila,"");
        $hojaActiva->setCellValue('I' . $fila,"");
        $hojaActiva->setCellValue('J' . $fila,"");
        $fila++;
    }
}

$hojaActiva->setAutoFilter('A1:J1');

$hojaActiva->getStyle('H2:I'.$fila)
    ->getNumberFormat()
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY)
;

// redirect output to client browser
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');


$writer = IOFactory::createWriter($excel, 'Xlsx');
$writer->save('php://output');
exit;

?>