<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set("America/Mexico_City");
// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill, Alignment};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//use PhpOffice\PhpSpreadsheet\Style\Alignment as alignment;
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;



$heading = false;
$regsalida = (!empty($_GET['regsalida'])) ? $_GET['regsalida'] : '';
$regllegada = (!empty($_GET['regllegada'])) ? $_GET['regllegada'] : '';
$fromdate = (!empty($_GET['fromdate'])) ? $_GET['fromdate'] : '';
$todate = (!empty($_GET['todate'])) ? $_GET['todate'] : '';
$linea = (!empty($_GET['linea'])) ? $_GET['linea'] : '';
$all = (!empty($_GET['all'])) ? $_GET['all'] : '';
$po = (!empty($_GET['po'])) ? $_GET['po'] : '';
$ro = (!empty($_GET['ro'])) ? $_GET['ro'] : '';
$pr = (!empty($_GET['pr'])) ? $_GET['pr'] : '';

$consulta = "";
$complemento ="(Truks.PO = 0  OR Truks.RO = 0 OR Truks.RqstID = 0)";

//$complemento="";
if($all == 1){
    $complemento="(Truks.PO = 0  OR Truks.RO = 0 OR Truks.RqstID = 0)";
}

//buscar solo por PR
if($pr==1 && $po =="" && $ro == "" && $all == ""){
    $complemento="Truks.RqstID = 0";
}

//buscar solo por PO
if($pr=="" && $po ==1 && $ro =="" && $all == ""){
    $complemento="Truks.PO = 0 ";
}

//buscar solo por RO
if($pr=="" && $po =="" && $ro == 1 && $all == ""){
    $complemento="Truks.RO = 0 ";
}
//buscar por PR y PO
if($pr==1 && $po ==1 && $ro == "" && $all == ""){
    $complemento="(Truks.PO = 0 AND Truks.RqstID = 0)";
}

//buscar por PR y RO
if($pr==1 && $po =="" && $ro == 1 && $all == ""){
    $complemento="(Truks.RO = 0 AND Truks.RqstID = 0)";
}

//buscar por PO y RO
if($pr=="" && $po ==1 && $ro == 1 && $all == ""){
    $complemento="(Truks.RO = 0 AND Truks.PO = 0)";
}

//print_r($complemento);

//1.- buscar por region de salida, region de llegada y linea de transporte 
if($regsalida !="" && $regllegada !="" && $linea!="" && $todate =="" && $todate ==""){
    $consulta="SELECT  Truks.TrkID,Truks.DO,Truks.OutDat,Truks.InDat,Truks.CrgQty as Bal,Truks.LotsAssc, 
    Region.RegNam as RegSal,R1.RegNam as Reglleg,Gines.GinName,Clients.Cli,Truks.RqstID,Truks.PO,DOrds.Typ,
    Truks.RO,Truks.Status,Transports.BnName as Linea,Truks.TNam as IdLinea,DOrds.Year
    FROM amsadb1.Truks
    left join amsadb1.DOrds
    ON  Truks.DO= DOrds.DOrd 
    left join amsadb1.Region
    ON DOrds.OutPlc = Region.IDReg 
    left join amsadb1.Region as R1
    ON DOrds.InReg = R1.IDReg
    left join amsadb1.Gines
    ON DOrds.Gin = Gines.IDGin
    left join amsadb1.Clients
    ON DOrds.InPlc = Clients.CliID
    left join amsadb1.Transports
    ON  Truks.TNam = Transports.TptID
    WHERE".$complemento." AND Truks.TNam =  '$linea'AND Truks.TNam !=137 AND Truks.CrgQty > 0 AND Truks.Status != 'Cancelled'
    HAVING
    RegSal = '$regsalida'  AND Reglleg = '$regllegada' AND  Typ !='EXP' AND  RegSal != 'USA'
    ORDER BY Truks.OutDat ASC;";

}

//2.- buscar por region de salida y linea de transporte 
if($regsalida !="" && $regllegada =="" && $linea!="" && $todate =="" && $todate ==""){

    $consulta="SELECT  Truks.TrkID,Truks.DO,Truks.OutDat,Truks.InDat,Truks.CrgQty as Bal,Truks.LotsAssc, 
    Region.RegNam as RegSal,R1.RegNam as Reglleg,Gines.GinName,Clients.Cli,Truks.RqstID,Truks.PO,DOrds.Typ,
    Truks.RO,Truks.Status,Transports.BnName as Linea,Truks.TNam as IdLinea,DOrds.Year
    FROM amsadb1.Truks
    left join amsadb1.DOrds
    ON  Truks.DO= DOrds.DOrd 
    left join amsadb1.Region
    ON DOrds.OutPlc = Region.IDReg 
    left join amsadb1.Region as R1
    ON DOrds.InReg = R1.IDReg
    left join amsadb1.Gines
    ON DOrds.Gin = Gines.IDGin
    left join amsadb1.Clients
    ON DOrds.InPlc = Clients.CliID
    left join amsadb1.Transportss
    ON  Truks.TNam = Transports.TptID
    WHERE ".$complemento." AND Truks.TNam =  '$linea'  AND Truks.TNam !=137 AND Truks.CrgQty > 0 AND Truks.Status != 'Cancelled'
    HAVING
    RegSal = '$regsalida'  AND  Typ !='EXP' AND  RegSal != 'USA'
    ORDER BY Truks.OutDat ASC;";

}

//3.- buscar solo por region de salida 
if($regsalida !="" && $regllegada =="" && $linea=="" && $todate =="" && $todate ==""){
    $consulta="SELECT  Truks.TrkID,Truks.DO,Truks.OutDat,Truks.InDat,Truks.CrgQty as Bal,Truks.LotsAssc, 
    Region.RegNam as RegSal,R1.RegNam as Reglleg,Gines.GinName,Clients.Cli,Truks.RqstID,Truks.PO,DOrds.Typ,
    Truks.RO,Truks.Status,Transports.BnName as Linea,Truks.TNam as IdLinea,DOrds.Year
    FROM amsadb1.Truks
    left join amsadb1.DOrds
    ON  Truks.DO= DOrds.DOrd 
    left join amsadb1.Region
    ON DOrds.OutPlc = Region.IDReg 
    left join amsadb1.Region as R1
    ON DOrds.InReg = R1.IDReg
    left join amsadb1.Gines
    ON DOrds.Gin = Gines.IDGin
    left join amsadb1.Clients
    ON DOrds.InPlc = Clients.CliID
    left join amsadb1.Transports
    ON  Truks.TNam = Transports.TptID
    WHERE ".$complemento." AND Truks.TNam !=137 AND Truks.CrgQty > 0 AND Truks.Status != 'Cancelled'
    HAVING
    RegSal = '$regsalida'  AND  Typ !='EXP' AND  RegSal != 'USA'
    ORDER BY Truks.OutDat  ASC;";

}

//4.- buscar solo por region de llegada 
if($regsalida =="" && $regllegada !="" && $linea=="" && $todate =="" && $todate ==""){
    $consulta="SELECT  Truks.TrkID,Truks.DO,Truks.OutDat,Truks.InDat,Truks.CrgQty as Bal,Truks.LotsAssc, 
    Region.RegNam as RegSal,R1.RegNam as Reglleg,Gines.GinName,Clients.Cli,Truks.RqstID,Truks.PO,DOrds.Typ,
    Truks.RO,Truks.Status,Transports.BnName as Linea,Truks.TNam as IdLinea,DOrds.Year
    FROM amsadb1.Truks
    left join amsadb1.DOrds
    ON  Truks.DO= DOrds.DOrd
    left join amsadb1.Region
    ON DOrds.OutPlc = Region.IDReg 
    left join amsadb1.Region as R1
    ON DOrds.InReg = R1.IDReg
    left join amsadb1.Gines
    ON DOrds.Gin = Gines.IDGin
    left join amsadb1.Clients
    ON DOrds.InPlc = Clients.CliID
    left join amsadb1.Transports
    ON  Truks.TNam = Transports.TptID
    WHERE ".$complemento." AND Truks.TNam !=137 AND Truks.CrgQty > 0 AND Truks.Status != 'Cancelled'
    HAVING
    Reglleg = '$regllegada'  AND  Typ !='EXP' AND  RegSal != 'USA'
    ORDER BY Truks.OutDat ASC;";


}


//5.- buscar por region de salida y fecha de salida 
if($regsalida !="" && $regllegada =="" && $linea=="" && $todate !="" && $todate !=""){

    $consulta="SELECT  Truks.TrkID,Truks.DO,Truks.OutDat,Truks.InDat,Truks.CrgQty as Bal,Truks.LotsAssc, 
    Region.RegNam as RegSal,R1.RegNam as Reglleg,Gines.GinName,Clients.Cli,Truks.RqstID,Truks.PO,DOrds.Typ,
    Truks.RO,Truks.Status,Transports.BnName as Linea,Truks.TNam as IdLinea,DOrds.Year
    FROM amsadb1.Truks
    left join amsadb1.DOrds
    ON  Truks.DO= DOrds.DOrd
    left join amsadb1.Region
    ON DOrds.OutPlc = Region.IDReg 
    left join amsadb1.Region as R1
    ON DOrds.InReg = R1.IDReg
    left join amsadb1.Gines
    ON DOrds.Gin = Gines.IDGin
    left join amsadb1.Clients
    ON DOrds.InPlc = Clients.CliID
    left join amsadb1.Transports
    ON  Truks.TNam = Transports.TptID
    WHERE ".$complemento." AND  Truks.TNam !=137 AND  Truks.OutDat BETWEEN '$fromdate' AND '$todate' AND Truks.CrgQty > 0 AND Truks.Status != 'Cancelled'
    HAVING
    RegSal = '$regsalida'  AND  Typ !='EXP' AND  RegSal != 'USA'
    ORDER BY Truks.OutDat ASC;";

}

//6.- buscar solo por region de llegada y linea

if($regsalida =="" && $regllegada !="" && $linea!="" && $todate =="" && $todate ==""){

 
    $consulta="SELECT  Truks.TrkID,Truks.DO,Truks.OutDat,Truks.InDat,Truks.CrgQty as Bal,Truks.LotsAssc, 
    Region.RegNam as RegSal,R1.RegNam as Reglleg,Gines.GinName,Clients.Cli,Truks.RqstID,Truks.PO,DOrds.Typ,
    Truks.RO,Truks.Status,Transports.BnName as Linea,Truks.TNam as IdLinea,DOrds.Year
    FROM amsadb1.Truks
    left join amsadb1.DOrds
    ON  Truks.DO= DOrds.DOrd
    left join amsadb1.Region
    ON DOrds.OutPlc = Region.IDReg 
    left join amsadb1.Region as R1
    ON DOrds.InReg = R1.IDReg
    left join amsadb1.Gines
    ON DOrds.Gin = Gines.IDGin
    left join amsadb1.Clients
    ON DOrds.InPlc = Clients.CliID
    left join amsadb1.Transports
    ON  Truks.TNam = Transports.TptID
    WHERE ".$complemento." AND Truks.TNam = '$linea' AND  Truks.TNam !=137 AND Truks.CrgQty > 0 AND Truks.Status != 'Cancelled'
    HAVING
    Reglleg = '$regllegada'  AND  Typ !='EXP' AND  RegSal != 'USA'
    ORDER BY Truks.OutDat  ASC;";

}

//7.- buscar solo por linea de transporte 
if($regsalida =="" && $regllegada =="" && $linea!="" && $todate =="" && $todate ==""){
    $consulta="SELECT  Truks.TrkID,Truks.DO,Truks.OutDat,Truks.InDat,Truks.CrgQty as Bal,Truks.LotsAssc, 
    Region.RegNam as RegSal,R1.RegNam as Reglleg,Gines.GinName,Clients.Cli,Truks.RqstID,Truks.PO,DOrds.Typ,
    Truks.RO,Truks.Status,Transports.BnName as Linea,Truks.TNam as IdLinea,DOrds.Year
    FROM amsadb1.Truks
    left join amsadb1.DOrds
    ON  Truks.DO= DOrds.DOrd
    left join amsadb1.Region
    ON DOrds.OutPlc = Region.IDReg 
    left join amsadb1.Region as R1
    ON DOrds.InReg = R1.IDReg
    left join amsadb1.Gines
    ON DOrds.Gin = Gines.IDGin
    left join amsadb1.Clients
    ON DOrds.InPlc = Clients.CliID
    left join amsadb1.Transports
    ON  Truks.TNam = Transports.TptID
    WHERE ".$complemento." AND  Truks.TNam !=137 AND  Truks.TNam = '$linea'  AND Truks.CrgQty > 0 AND Truks.Status != 'Cancelled'
    HAVING
    Typ !='EXP' AND  RegSal != 'USA'
    ORDER BY Truks.OutDat  ASC;";
}


//8.- buscar por linea de transporte y fecha
if($regsalida =="" && $regllegada =="" && $linea!="" && $todate!="" && $todate !=""){
    $consulta="SELECT  Truks.TrkID,Truks.DO,Truks.OutDat,Truks.InDat,Truks.CrgQty as Bal,Truks.LotsAssc, 
    Region.RegNam as RegSal,R1.RegNam as Reglleg,Gines.GinName,Clients.Cli,Truks.RqstID,Truks.PO,DOrds.Typ,
    Truks.RO,Truks.Status,Transports.BnName as Linea,Truks.TNam as IdLinea,DOrds.Year
    FROM amsadb1.Truks
    left join amsadb1.DOrds
    ON  Truks.DO= DOrds.DOrd
    left join amsadb1.Region
    ON DOrds.OutPlc = Region.IDReg 
    left join amsadb1.Region as R1
    ON DOrds.InReg = R1.IDReg
    left join amsadb1.Gines
    ON DOrds.Gin = Gines.IDGin
    left join amsadb1.Clients
    ON DOrds.InPlc = Clients.CliID
    left join amsadb1.Transports
    ON  Truks.TNam = Transports.TptID
    WHERE ".$complemento." AND  Truks.TNam !=137 AND  Truks.TNam = '$linea'  AND  (Truks.OutDat BETWEEN '$fromdate' AND '$todate') AND Truks.CrgQty > 0 AND Truks.Status != 'Cancelled'
    HAVING
    Typ !='EXP' AND  RegSal != 'USA'
    ORDER BY Truks.OutDat  ASC;";
}

//9.- buscar por region de salida, transporte y fecha de salida 

if($regsalida !="" && $regllegada =="" && $linea!="" && $todate!="" && $todate !=""){
    $consulta="SELECT  Truks.TrkID,Truks.DO,Truks.OutDat,Truks.InDat,Truks.CrgQty as Bal,Truks.LotsAssc, 
    Region.RegNam as RegSal,R1.RegNam as Reglleg,Gines.GinName,Clients.Cli,Truks.RqstID,Truks.PO,DOrds.Typ,
    Truks.RO,Truks.Status,Transports.BnName as Linea,Truks.TNam as IdLinea,DOrds.Year
    FROM amsadb1.Truks
    left join amsadb1.DOrds
    ON  Truks.DO= DOrds.DOrd
    left join amsadb1.Region
    ON DOrds.OutPlc = Region.IDReg 
    left join amsadb1.Region as R1
    ON DOrds.InReg = R1.IDReg
    left join amsadb1.Gines
    ON DOrds.Gin = Gines.IDGin
    left join amsadb1.Clients
    ON DOrds.InPlc = Clients.CliID
    left join amsadb1.Transports
    ON  Truks.TNam = Transports.TptID
    WHERE ".$complemento." AND  Truks.TNam !=137 AND  Truks.TNam = '$linea'  AND  (Truks.OutDat BETWEEN '$fromdate' AND '$todate') AND Truks.CrgQty > 0 AND Truks.Status != 'Cancelled'
    HAVING
    RegSal = '$regsalida'  AND  Typ !='EXP' AND  RegSal != 'USA' 
    ORDER BY Truks.OutDat  ASC;";
    
}


//10.- buscar por region de llegada, transporte y fecha de salida 

if($regsalida =="" && $regllegada !="" && $linea!="" && $todate!="" && $todate !=""){
    $consulta="SELECT  Truks.TrkID,Truks.DO,Truks.OutDat,Truks.InDat,Truks.CrgQty as Bal,Truks.LotsAssc, 
    Region.RegNam as RegSal,R1.RegNam as Reglleg,Gines.GinName,Clients.Cli,Truks.RqstID,Truks.PO,DOrds.Typ,
    Truks.RO,Truks.Status,Transports.BnName as Linea,Truks.TNam as IdLinea,DOrds.Year
    FROM amsadb1.Truks
    left join amsadb1.DOrds
    ON  Truks.DO= DOrds.DOrd
    left join amsadb1.Region
    ON DOrds.OutPlc = Region.IDReg 
    left join amsadb1.Region as R1
    ON DOrds.InReg = R1.IDReg
    left join amsadb1.Gines
    ON DOrds.Gin = Gines.IDGin
    left join amsadb1.Clients
    ON DOrds.InPlc = Clients.CliID
    left join amsadb1.Transports
    ON  Truks.TNam = Transports.TptID
    WHERE ".$complemento." AND  Truks.TNam !=137 AND  Truks.TNam = '$linea'  AND  (Truks.OutDat BETWEEN '$fromdate' AND '$todate') AND Truks.CrgQty > 0 AND Truks.Status != 'Cancelled'
    HAVING
    Reglleg = '$regllegada'  AND  Typ !='EXP' AND  RegSal != 'USA'
    ORDER BY Truks.OutDat  ASC;";
    
}

//11.- buscar solo por fecha de salida 

if($regsalida =="" && $regllegada =="" && $linea=="" && $todate!="" && $todate !=""){
    $consulta="SELECT  Truks.TrkID,Truks.DO,Truks.OutDat,Truks.InDat,Truks.CrgQty as Bal,Truks.LotsAssc, 
    Region.RegNam as RegSal,R1.RegNam as Reglleg,Gines.GinName,Clients.Cli,Truks.RqstID,Truks.PO,DOrds.Typ,
    Truks.RO,Truks.Status,Transports.BnName as Linea,Truks.TNam as IdLinea,DOrds.Year
    FROM amsadb1.Truks
    left join amsadb1.DOrds
    ON  Truks.DO= DOrds.DOrd 
    left join amsadb1.Region
    ON DOrds.OutPlc = Region.IDReg 
    left join amsadb1.Region as R1
    ON DOrds.InReg = R1.IDReg
    left join amsadb1.Gines
    ON DOrds.Gin = Gines.IDGin
    left join amsadb1.Clients
    ON DOrds.InPlc = Clients.CliID
    left join amsadb1.Transports
    ON  Truks.TNam = Transports.TptID
    WHERE ".$complemento." AND  Truks.TNam !=137  AND  (Truks.OutDat BETWEEN '$fromdate' AND '$todate') AND Truks.CrgQty > 0 AND Truks.Status != 'Cancelled'
    HAVING Typ !='EXP' AND  RegSal != 'USA'
    ORDER BY Truks.OutDat  ASC;";
    
}

// 12.-Default todos los campos vacios 

if($regsalida =="" && $regllegada =="" && $linea=="" && $todate =="" && $todate ==""){

    $consulta="SELECT  Truks.TrkID,Truks.DO,Truks.OutDat,Truks.InDat,Truks.CrgQty as Bal,Truks.LotsAssc, 
    Region.RegNam as RegSal,R1.RegNam as Reglleg,Gines.GinName,Clients.Cli,Truks.RqstID,Truks.PO,DOrds.Typ,
    Truks.RO,Truks.Status,Transports.BnName as Linea,Truks.TNam as IdLinea,DOrds.Year
    FROM amsadb1.Truks
    left join amsadb1.DOrds
    ON  Truks.DO= DOrds.DOrd 
    left join amsadb1.Region
    ON DOrds.OutPlc = Region.IDReg 
    left join amsadb1.Region as R1
    ON DOrds.InReg = R1.IDReg
    left join amsadb1.Gines
    ON DOrds.Gin = Gines.IDGin
    left join amsadb1.Clients
    ON DOrds.InPlc = Clients.CliID
    left join amsadb1.Transports
    ON  Truks.TNam = Transports.TptID
    WHERE".$complemento." AND  Truks.TNam !=137 AND Truks.CrgQty > 0 AND Truks.Status != 'Cancelled'
    HAVING Typ !='EXP' AND  RegSal != 'USA'
    ORDER BY Truks.OutDat  ASC;";

}

// 13.-todos campos llenos 

if($regsalida !="" && $regllegada !="" && $linea!="" && $todate !="" && $todate !=""){

    $consulta="SELECT  Truks.TrkID,Truks.DO,Truks.OutDat,Truks.InDat,Truks.CrgQty as Bal,Truks.LotsAssc, 
    Region.RegNam as RegSal,R1.RegNam as Reglleg,Gines.GinName,Clients.Cli,Truks.RqstID,Truks.PO,DOrds.Typ,
    Truks.RO,Truks.Status,Transports.BnName as Linea,Truks.TNam as IdLinea,DOrds.Year
    FROM amsadb1.Truks
    left join amsadb1.DOrds
    ON  Truks.DO= DOrds.DOrd 
    left join amsadb1.Region
    ON DOrds.OutPlc = Region.IDReg 
    left join amsadb1.Region as R1
    ON DOrds.InReg = R1.IDReg
    left join amsadb1.Gines
    ON DOrds.Gin = Gines.IDGin
    left join amsadb1.Clients
    ON DOrds.InPlc = Clients.CliID
    left join amsadb1.Transports
    ON  Truks.TNam = Transports.TptID
    WHERE ".$complemento." AND Truks.TNam =  '$linea' AND Truks.TNam !=137 AND  (Truks.OutDat BETWEEN '$fromdate' AND '$todate') AND Truks.CrgQty > 0 AND Truks.Status != 'Cancelled'
    HAVING
    RegSal = '$regsalida'  AND Reglleg = '$regllegada' AND  Typ !='EXP' AND  RegSal != 'USA'
    ORDER BY Truks.OutDat  ASC;";

}

// 14.-buscar por region de llegada y fecha de salida

if($regsalida =="" && $regllegada !="" && $linea=="" && $todate !="" && $todate !=""){

 
    $consulta="SELECT  Truks.TrkID,Truks.DO,Truks.OutDat,Truks.InDat,Truks.CrgQty as Bal,Truks.LotsAssc, 
    Region.RegNam as RegSal,R1.RegNam as Reglleg,Gines.GinName,Clients.Cli,Truks.RqstID,Truks.PO,DOrds.Typ,
    Truks.RO,Truks.Status,Transports.BnName as Linea,Truks.TNam as IdLinea,DOrds.Year
    FROM amsadb1.Truks
    left join amsadb1.DOrds
    ON  Truks.DO= DOrds.DOrd
    left join amsadb1.Region
    ON DOrds.OutPlc = Region.IDReg 
    left join amsadb1.Region as R1
    ON DOrds.InReg = R1.IDReg
    left join amsadb1.Gines
    ON DOrds.Gin = Gines.IDGin
    left join amsadb1.Clients
    ON DOrds.InPlc = Clients.CliID
    left join amsadb1.Transports
    ON  Truks.TNam = Transports.TptID
    WHERE ".$complemento."  AND  Truks.TNam !=137 AND  (Truks.OutDat BETWEEN '$fromdate' AND '$todate') AND Truks.CrgQty > 0 AND Truks.Status != 'Cancelled'
    HAVING
    Reglleg = '$regllegada'  AND  Typ !='EXP' AND  RegSal != 'USA'
    ORDER BY Truks.OutDat  ASC;";

}


// 15.- buscar por region de salida, region de llegada y fecha de salida 

if($regsalida !="" && $regllegada !="" && $linea=="" && $todate !="" && $todate !=""){

 
    $consulta="SELECT  Truks.TrkID,Truks.DO,Truks.OutDat,Truks.InDat,Truks.CrgQty as Bal,Truks.LotsAssc, 
    Region.RegNam as RegSal,R1.RegNam as Reglleg,Gines.GinName,Clients.Cli,Truks.RqstID,Truks.PO,DOrds.Typ,
    Truks.RO,Truks.Status,Transports.BnName as Linea,Truks.TNam as IdLinea,DOrds.Year
    FROM amsadb1.Truks
    left join amsadb1.DOrds
    ON  Truks.DO= DOrds.DOrd 
    left join amsadb1.Region
    ON DOrds.OutPlc = Region.IDReg 
    left join amsadb1.Region as R1
    ON DOrds.InReg = R1.IDReg
    left join amsadb1.Gines
    ON DOrds.Gin = Gines.IDGin
    left join amsadb1.Clients
    ON DOrds.InPlc = Clients.CliID
    left join amsadb1.Transports
    ON  Truks.TNam = Transports.TptID
    WHERE ".$complemento." AND  Truks.TNam !=137 AND  (Truks.OutDat BETWEEN '$fromdate' AND '$todate') AND Truks.CrgQty > 0 AND Truks.Status != 'Cancelled'
    HAVING
    RegSal = '$regsalida'AND  Reglleg = '$regllegada' AND Typ !='EXP' AND  RegSal != 'USA'
    ORDER BY Truks.OutDat  ASC;";

}
/*
print_r("PO");
print_r($po);
print_r("PR");
print_r($pr);
print_r("RO");
print_r($ro);
print_r("ALL");
print_r($all);
print_r("COMPLEMENTO");
print_r($complemento);
print_r($consulta);
*/


$resultado = $conexion->prepare($consulta);
$resultado->execute();        
//print_r($resultado);


$siexiste=0; //$data=$resultado->fetchAll(PDO::FETCH_ASSOC);
$fileName = "MissingReport-PO-PR-RO-".date('d-m-Y').".xlsx";
$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("MissingReport");
//$hojaActiva->freezePane("A2");



$hojaActiva->getColumnDimension('A')->setWidth(12);
$hojaActiva->setCellValue('A1','TruckID');
$hojaActiva->getColumnDimension('B')->setWidth(12);
$hojaActiva->setCellValue('B1','Crop');
$hojaActiva->getColumnDimension('C')->setWidth(12);
$hojaActiva->setCellValue('C1','DO');
$hojaActiva->getColumnDimension('D')->setWidth(12);
$hojaActiva->setCellValue('D1','Departure date');
$hojaActiva->getColumnDimension('E')->setWidth(12);
$hojaActiva->setCellValue('E1','Arrival date');
$hojaActiva->getColumnDimension('F')->setWidth(12);
$hojaActiva->setCellValue('F1','Bales');
$hojaActiva->getColumnDimension('G')->setWidth(12);
$hojaActiva->setCellValue('G1','Associated Lots');
$hojaActiva->getColumnDimension('H')->setWidth(12);
$hojaActiva->setCellValue('H1','Departure Region');
$hojaActiva->getColumnDimension('I')->setWidth(12);
$hojaActiva->setCellValue('I1','Arrival region');
$hojaActiva->getColumnDimension('J')->setWidth(12);
$hojaActiva->setCellValue('J1','Gin');
$hojaActiva->getColumnDimension('K')->setWidth(12);
$hojaActiva->setCellValue('K1','Client');
$hojaActiva->getColumnDimension('L')->setWidth(12);
$hojaActiva->setCellValue('L1','Transport');
$hojaActiva->getColumnDimension('M')->setWidth(12);
$hojaActiva->setCellValue('M1','PR');
$hojaActiva->getColumnDimension('N')->setWidth(12);
$hojaActiva->setCellValue('N1','PO');
$hojaActiva->getColumnDimension('O')->setWidth(12);
$hojaActiva->setCellValue('O1','RO');
$hojaActiva->getColumnDimension('P')->setWidth(12);
$hojaActiva->setCellValue('P1','Status');
$hojaActiva->getColumnDimension('Q')->setWidth(12);
$hojaActiva->setCellValue('Q1','Type');



//ESTILO DE CELDA

//negritas en encabezado

//$from = "A1"; // or any value
//$to = "O1"; // or any value
$hojaActiva->getStyle('A1:Q1')->getFont()->setBold( true );

//relleno de celda encabezado
$hojaActiva->getStyle('A1:Q1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFE1E1E1');

$hojaActiva->freezePane('A2');

//$hojaActiva->freezePane('A1');

$fila = 2;
$fecha_salida="";
$fecha_llegada="";
while($row = $resultado->fetch(PDO::FETCH_ASSOC)){
    if($siexiste==0){
        if(!empty($row['TrkID']))
            $siexiste=1;
    }

    if($row['OutDat']!=""){
       // $fecha_salida = date("d/m/Y", ($row['OutDat']));


        $fecha_salida = strtotime($row['OutDat']);

        $fecha_salida =25569 + ($fecha_salida / 86400);
        
    }
    else{
        $fecha_salida = "";

    }

    if($row['InDat']!=""){
       // $fecha_llegada = date("d/m/Y", ($row['InDat']));
        $fecha_llegada =strtotime($row['InDat']);
        $fecha_llegada =25569 + ($fecha_llegada / 86400);
        
    }
    else{
        $fecha_llegada = "";

    }    

    $hojaActiva->getStyle('D' . $fila)->getNumberFormat()//formato de fecha separado por /
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    $hojaActiva->getStyle('E' . $fila)->getNumberFormat()//formato de fecha separado por /
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    //date("d-m-Y", strtotime('2018-02-02'))
    $hojaActiva->setCellValue('A' . $fila,$row['TrkID']);
    $hojaActiva->setCellValue('B' . $fila,$row['Year']);
    $hojaActiva->setCellValue('C' . $fila,$row['DO']);
    $hojaActiva->setCellValue('D' . $fila,$fecha_salida );
    $hojaActiva->setCellValue('E' . $fila,$fecha_llegada );
    $hojaActiva->setCellValue('F' . $fila,$row['Bal']);
    //$hojaActiva->setCellValue('G' . $fila,$row['LotsAssc']);
    $hojaActiva->setCellValue('H' . $fila,$row['RegSal']);
    $hojaActiva->setCellValue('I'. $fila,$row['Reglleg']);
    $hojaActiva->setCellValue('J'. $fila,$row['GinName']);
    $hojaActiva->setCellValue('K'. $fila,$row['Cli']);
    $hojaActiva->setCellValue('L'. $fila,$row['Linea']);
    $hojaActiva->setCellValue('M'. $fila,$row['RqstID']);
    $hojaActiva->setCellValue('N'. $fila,$row['PO']);
    $hojaActiva->setCellValue('O'. $fila,$row['RO']); 
    $hojaActiva->setCellValue('P'. $fila,$row['Status']);  
    $hojaActiva->setCellValue('Q'. $fila,$row['Typ']);
    
    //PONER FORMATO DE REXTO PARA LOTES AMERICANOS  
    
    $style = $hojaActiva->getStyle('G'. $fila);
    $style->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
    $hojaActiva->getCell('G'. $fila)->setValueExplicit($row['LotsAssc'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);

    $fila++;
}


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');
ob_end_clean();
$writer = IOFactory::createWriter($excel, 'Xlsx');
$writer->save('php://output');
exit;


?>