<?php 
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set("America/Mexico_City");


// Filter the excel data 
function filterData(&$str){ 
    $str = preg_replace("/\t/", "\\t", $str); 
    $str = preg_replace("/\r?\n/", "\\n", $str); 
    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"'; 
} 


//Query
$query= "SELECT DISTINCT Truks.TrkID, TrkLPlt, TraLPlt, WBill, DrvLcs, DO, DrvNam, DrvTel, CrgQty, OutDat, OutTime, InDat, InTime, LotsAssc, Comments, Truks.Status, 
(SELECT Typ FROM DOrds WHERE DO=Dord) as Typ,
(Select BnName From Transports Where Truks.TNam = Transports.TptID) as TNam,
(Select RegNam From DOrds, Region Where Truks.DO = DOrds.DOrd and OutPlc = IDReg) as RegNameOut,
(Select RegNam From DOrds, Region Where Truks.DO = DOrds.DOrd and InReg = IDReg) as RegNameIn,
(Select Cli From DOrds, Clients Where Truks.DO = DOrds.DOrd and InPlc = CliID) as PlcNameIn,
(Select IFNULL(SUM(Lots.Qty),0) From Lots Where Truks.TrkID = Lots.TrkID) as LotQty,
ifnull(GinName, (SELECT GinName FROM DOrds, Gines WHERE Truks.DO = DOrds.DOrd and DOrds.Gin = Gines.IDGin)) as GinName 
FROM Truks 
left join Lots on Truks.DO = Lots.DOrd and Truks.LotsAssc = Lots.Lot
left join Gines on Lots.GinID = Gines.IDGin
WHERE Truks.Status = 'Transit' OR Truks.Status = 'Programmed'  order by Truks.OutDat ASC, Truks.OutTime ASC;";

$result = $conexion->prepare($query);
$result->execute();
$siexiste=0; //Para verificar que hayan datos

//Define the filename with current date
$fileName = "TrucksStatus-".date('d-m-Y').".xls";

$fields = array('DO','Lot','BC Transit','Gin','Departure region','Arrival region','Client','Departure date','Arrival Date',
'Days late', 'Truck ID','Bussiness name transport','License plate','Driver','Phone Number','Waybill','Status','Comments');

// Display column names as first row 
$excelData = implode("\t", array_values($fields)) . "\n"; 

$today = new DateTime();//DateTime("2022-04-06"); //echo $today->format('Y-m-d');


while($row = $result->fetch(PDO::FETCH_ASSOC)){
    if($siexiste==0){
        if(!empty($row['TrkID']))
            $siexiste=1;
    }
	$dlate="";
	$textlate="";
	if($row['Status'] == "Transit" || $row['Status'] == "Programmed"){
    $arrivalDate = new DateTime($row['InDat']); //echo $second->format('Y-m-d');
    $intvl = $arrivalDate->diff($today);
    $dlate = 0;
    $textlate = "";
    if(($today > $arrivalDate)&&($intvl->days > 0)){
        $dlate = $intvl->days;
        $textlate = "Delayed ";
    }}
    $linedata = array($row['DO'],$row['LotsAssc'],$row['LotQty'],$row['GinName'],$row['RegNameOut'],$row['RegNameIn'],$row['PlcNameIn'],$row['OutDat'].' '.$row['OutTime'],$row['InDat'].' '.$row['InTime'],$dlate,$row['TrkID'],$row['TNam'],$row['DrvLcs'],$row['DrvNam'],$row['DrvTel'],$row['WBill'],$textlate.$row['Status'],$row['Comments'] );
    array_walk($linedata, 'filterData'); 
    $excelData .= implode("\t", array_values($linedata)) . "\n"; 
} 


if($siexiste == 0){
    $excelData .= 'No records found...'. "\n"; 
}

// Headers for download 
header("Content-Type: application/vnd.ms-excel"); 
header("Content-Disposition: attachment; filename=\"$fileName\"");
 
// Render excel data 
echo $excelData; 
 
exit;




/* POR SI DESPUÉS VEMOS QUE ES MEJOR CONSULTA
$queryIds = 'SELECT T.TrkID FROM Truks T where T.Status = "Transit";';
$result = $conexion->prepare($queryIds);
$result->execute();
$data = $result->fetchAll(PDO::FETCH_ASSOC);


$ids = "";
foreach($data as $dat){
    $ids.= $dat['TrkID'].",";
}
$ids = substr($ids, 0, -1);

$array=array_map('intval', explode(',', $ids));
$array = implode("','",$array); */




/*$query= "SELECT DISTINCT Truks.TrkID, TrkLPlt, TraLPlt, WBill, DrvLcs, DO, DrvNam, DrvTel, CrgQty, OutDat, OutTime, InDat, InTime, LotsAssc, Comments, Truks.Status, 
(SELECT Typ FROM DOrds WHERE DO=Dord) as Typ,
(Select BnName From Transports Where Truks.TNam = Transports.TptID) as TNam,
(Select RegNam From DOrds, Region Where Truks.DO = DOrds.DOrd and OutPlc = IDReg) as RegNameOut,
(Select RegNam From DOrds, Region Where Truks.DO = DOrds.DOrd and InReg = IDReg) as RegNameIn,
(Select Cli From DOrds, Clients Where Truks.DO = DOrds.DOrd and InPlc = CliID) as PlcNameIn,
(Select IFNULL(SUM(Lots.Qty),0) From Lots Where Truks.TrkID = Lots.TrkID) as LotQty,
ifnull(GinName, (SELECT GinName FROM DOrds, Gines WHERE Truks.DO = DOrds.DOrd and DOrds.Gin = Gines.IDGin)) as GinName 
FROM Truks 
left join Lots on Truks.DO = Lots.DOrd and Truks.LotsAssc = Lots.Lot
left join Gines on Lots.GinID = Gines.IDGin
WHERE Truks.TrkID IN ('".$array."');"; 9.10*/
?>
