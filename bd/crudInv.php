<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

$Inv = (isset($_POST['Inv'])) ? $_POST['Inv'] : '';
$DO = (isset($_POST['DO'])) ? $_POST['DO'] : '';
$CtcInv = (isset($_POST['CtcInv'])) ? $_POST['CtcInv'] : '';
$Lot = (isset($_POST['Lot'])) ? $_POST['Lot'] : '';
$Qty = (isset($_POST['Qty'])) ? $_POST['Qty'] : '';
$InvDat = (isset($_POST['InvDat'])) ? $_POST['InvDat'] : '';
$WghGss = (isset($_POST['WghGss'])) ? $_POST['WghGss'] : '';
$WghNet = (isset($_POST['WghNet'])) ? $_POST['WghNet'] : '';
$Amt = (isset($_POST['Amt'])) ? $_POST['Amt'] : '';
$Price = (isset($_POST['Price'])) ? $_POST['Price'] : '';
$PriceUnit = (isset($_POST['PriceUnit'])) ? $_POST['PriceUnit'] : '';
$PosDat = (isset($_POST['PosDat'])) ? $_POST['PosDat'] : '';
$InvSts = (isset($_POST['InvSts'])) ? $_POST['InvSts'] : '';
$InvCmt = (isset($_POST['InvCmt'])) ? $_POST['InvCmt'] : '';

$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$InvID = (isset($_POST['InvID'])) ? $_POST['InvID'] : '';


switch($opcion){
    case 1:
        $consulta = "INSERT INTO Invs (Inv, DO, Contract, Lot, Qty, Unt, InvDat, InvWgh, InvWghNet, InvAmt, Price, PriceUnit, PosDat, InvSta, InvCmt) VALUES('$Inv', '$DO', '$CtcInv', '$Lot', '$Qty', '$Qty', '$InvDat', '$WghGss', '$WghNet', '$Amt', '$Price', '$PriceUnit', '$PosDat', '$InvSts', '$InvCmt') ";			
        $resultado = $conexion->prepare($consulta);
        $resultado->execute(); 
        
        $consulta = "SELECT InvID, DO, Contract, Lot, Qty, InvDat, InvWgh, InvWghNet, InvAmt, Price, PriceUnit, PosDat, InvSta, InvCmt FROM Invs ORDER BY InvID DESC LIMIT 1";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);       
        break;    
    case 2:        
        $consulta = "UPDATE Invs SET Inv='$Inv', DO='$DO', Contract='$CtcInv', Lot='$Lot', Qty='$Qty', Unt='$Qty', InvDat='$InvDat', InvWgh='$WghGss', InvWghNet='$WghNet', InvAmt='$Amt', Price='$Price', PriceUnit='$PriceUnit', PosDat='$PosDat', InvSta='$InvSts', InvCmt='$InvCmt' WHERE InvID='$InvID' ";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        
        $consulta = "SELECT InvID, DO, Contract, Lot, Qty, InvDat, InvWgh, InvWghNet, InvAmt, Price, PriceUnit, PosDat, InvSta, InvCmt FROM Invs WHERE InvID='$InvID' ";       
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 3:        
        $consulta = "UPDATE Invs Set InvSta='Cancelled' WHERE InvID='$InvID'"; //"DELETE FROM Lots WHERE LotID='$LotID' ";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();                           
        break;
    case 4:    
        $consulta = "SELECT InvID, Inv, DO,
        (SELECT RegNam FROM DOrds, Region WHERE DOrd = DO and OutPlc = IDReg) as OutReg,
        (SELECT Cli FROM DOrds, Clients WHERE DOrd = DO and InPlc = CliID) as Cli,
        (SELECT Ctc FROM DOrds WHERE DOrd = DO) as DOCtc, 
        (SELECT Rqs FROM DOrds WHERE DOrd = DO) as DOClt, Contract, Lot, Qty, InvDat, InvWgh, InvWghNet, (InvWgh - InvWghNet) as Tare, InvAmt, Price, PriceUnit, PosDat, InvSta, InvCmt FROM Invs";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 5:    
        $consulta = "SELECT Ctc, Typ,
        Qty + (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE Stg.DO='$DO') as Qty,
        (SELECT RegNam FROM DOrds, Region WHERE DOrd = '$DO' and OutPlc = IDReg) as OutReg,
        (SELECT RegNam FROM DOrds, Region WHERE DOrd = '$DO' and InReg = IDReg) as InReg,
        (SELECT Cli FROM DOrds, Clients WHERE DOrd = '$DO' and InPlc = CliID) as Cli
        FROM DOrds WHERE DOrd='$DO'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 6:    
        $consulta = "SELECT DOrd FROM Lots WHERE Lot = '$Lot'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
}

print json_encode($data, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX
$conexion=null;