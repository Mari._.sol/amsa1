<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

function lowercase($element)
{
    return "'".$element."'";
}

$Lot = (isset($_POST['Lot'])) ? $_POST['Lot'] : '';
$Qty = (isset($_POST['Qty'])) ? $_POST['Qty'] : '';
$Reg = (isset($_POST['Reg'])) ? $_POST['Reg'] : '';
$GinID = (isset($_POST['GinID'])) ? $_POST['GinID'] : '';
$Loc = (isset($_POST['Loc'])) ? $_POST['Loc'] : '';
$LiqWgh = (isset($_POST['LiqWgh'])) ? $_POST['LiqWgh'] : '';
$Qlty = (isset($_POST['Qlty'])) ? $_POST['Qlty'] : '';
$Cmt = (isset($_POST['Cmt'])) ? $_POST['Cmt'] : '';
$Recap = (isset($_POST['Recap'])) ? $_POST['Recap'] : '';
$Cli = (isset($_POST['Cli'])) ? $_POST['Cli'] : '';
$AssgTo = (isset($_POST['AssgTo'])) ? $_POST['AssgTo'] : '';
$AsgmDate = (isset($_POST['AsgmDate'])) ? $_POST['AsgmDate'] : '';
$DOrd = (isset($_POST['DOrd'])) ? $_POST['DOrd'] : '';
$TrkID = (isset($_POST['TrkID'])) ? $_POST['TrkID'] : '';
$Status = (isset($_POST['Status'])) ? $_POST['Status'] : '';
$SchDate = (isset($_POST['SchDate'])) ? $_POST['SchDate'] : '';
$Lotes = (isset($_POST['Lotes'])) ? $_POST['Lotes'] : '';

$idReg = (isset($_POST['idReg'])) ? $_POST['idReg'] : '';
$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$LotID = (isset($_POST['LotID'])) ? $_POST['LotID'] : '';
$Crop = (isset($_POST['Crop'])) ? $_POST['Crop'] : '';
$pacascert = (isset($_POST['pacascert'])) ? $_POST['pacascert'] : '';
$TotalQty = (isset($_POST['TotalQty'])) ? $_POST['TotalQty'] : '';

$Region = (isset($_POST['Region'])) ? $_POST['Region'] : '';
//Variables filtros
session_start();
$filterSes = $_SESSION['Filters'];

$RegL = $filterSes['RegL'];
$GinL = $filterSes['GinL'];
$LocL = $filterSes['LocL'];
$DOL = $filterSes['DOL'];
$LotL = $filterSes['LotL'];
$CliL = $filterSes['CliL'];
$CrpL = $filterSes['CrpL'];


switch($opcion){
    case 1:
        $consulta = "SELECT IDReg FROM Region WHERE RegNam='$Reg'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataR=$resultado->fetch();
        $Reg = $dataR['IDReg'];
        
        $consulta = "SELECT IDGin FROM Gines WHERE GinName='$GinID'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataGin=$resultado->fetch();
        $GinID = $dataGin['IDGin'];
        
        $consulta = "SELECT IDReg FROM Region WHERE RegNam='$Loc'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataLoc=$resultado->fetch();
        $Loc = $dataLoc['IDReg'];
        
        $consulta = "SELECT CliID FROM Clients WHERE Cli='$Cli'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataCli=$resultado->fetch();
        $Cli = $dataCli['CliID'];
        
        $consulta = "INSERT INTO Lots (Crop, Lot, Reg, GinID, Location, Qty, Unt, LiqWgh, Qlty, Cmt, Recap, CliID, AsgmDate, DOrd, SchDate, TrkID, InvID, Status, Cert, TotalQty) VALUES ('$Crop', '$Lot', '$Reg', '$GinID', '$Loc', '$Qty', '$Qty', '$LiqWgh', '$Qlty', '$Cmt', '$Recap', '$Cli', '$AsgmDate', '$DOrd', '$SchDate', '', '', '$Status','$pacascert', '$TotalQty');";			
        $resultado = $conexion->prepare($consulta);
        $resultado->execute(); 
        
        $consulta = "SELECT LotID, Lot, Qty, Reg, GinID, Location, LiqWgh, DOrd, TrkID,Cert Status FROM Lots ORDER BY LotID DESC LIMIT 1";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);       
        break;    
    case 2:
        $consulta = "SELECT IDReg FROM Region WHERE RegNam='$Reg'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataR=$resultado->fetch();
        $Reg = $dataR['IDReg'];
        
        $consulta = "SELECT IDGin FROM Gines WHERE GinName='$GinID'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataGin=$resultado->fetch();
        $GinID = $dataGin['IDGin'];
        
        $consulta = "SELECT IDReg FROM Region WHERE RegNam='$Loc'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataLoc=$resultado->fetch();
        $Loc = $dataLoc['IDReg'];
        
        $consulta = "SELECT CliID FROM Clients WHERE Cli='$Cli'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataCli=$resultado->fetch();
        $Cli = $dataCli['CliID'];
        
        $consulta = "UPDATE Lots SET Lot='$Lot', Reg='$Reg', GinID='$GinID', Location='$Loc', Qty='$Qty', Unt='$Qty', LiqWgh='$LiqWgh', Qlty='$Qlty', Cmt='$Cmt', Recap='$Recap', CliID='$Cli', AsgmDate='$AsgmDate', DOrd='$DOrd', SchDate='$SchDate', Status='$Status', Crop='$Crop', Cert = '$pacascert', TotalQty = '$TotalQty' WHERE LotID='$LotID';";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        
        $consulta = "SELECT LotID, Lot, Qty, Reg, GinID, Location, LiqWgh, DOrd, TrkID, Status, Cert FROM Lots WHERE LotID='$LotID' ";       
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 3:        
        /*$consulta = "SELECT * FROM Lots WHERE LotID='$LotID'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
                foreach($data as $row){
                    $Lot = $row['Lot'];
                    $Reg = $row['Reg'];
                    $GinID = $row['GinID'];
                    $Loc = $dataDO['InPlc'];
                    $Qty = $row['Qty'];
                    $LiqWgh = $row['LiqWgh'];
                    $Status = $row['Status'];
                    $consulta = "INSERT INTO Lots (Lot, Qty, Reg, GinID, Location, Unt, LiqWgh, DOrd, TrkID, InvID, Status) VALUES('$Lot', '$Qty', '$Reg', '$GinID', '$Loc',  '$Qty', '$LiqWgh', '', '', '', '$Status') ";			
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                }*/
        
        $consulta = "DELETE FROM Lots WHERE LotID='$LotID'";	
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();                           
        break;
    case 4:
        if ($RegL == "" && $GinL == "" & $LocL == "" && $DOL == "" && $LotL == "" & $CliL == "" && $CrpL == ""){
            $consulta = "SELECT LotID, Crop, Lot, Qty, Reg, SchDate,Cert,TotalQty,
            (SELECT RegNam FROM Region WHERE IDReg = Reg) as RegName, GinID,
            (SELECT GinName FROM Gines WHERE IDGin = GinID) as GinName, Location, Lots.CliID,
            (SELECT Cli FROM Clients WHERE Lots.CliID = Clients.CliID) as CliID,
            (SELECT RegNam FROM DOrds, Region WHERE Lots.DOrd = DOrds.DOrd and DOrds.InReg = Region.IDReg) as InReg,
            (SELECT Cli FROM DOrds, Clients WHERE Lots.DOrd = DOrds.DOrd and DOrds.InPlc = Clients.CliID) as CliDO,
            (SELECT RegNam FROM Region WHERE IDReg = Location) as LocName, LiqWgh, DOrd, TrkID, Qlty, AsgmDate, Cmt, Recap, Status 
            FROM Lots 
            Where Crop IN (2024) ORDER BY LotID";
            //(SELECT GinName FROM Gines WHERE IDGin = Location) as LocName
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }elseif ($RegL != "" && $GinL == "" & $LocL == "" && $DOL == "" && $LotL == "" & $CliL == "" && $CrpL == "") {
            $reg = implode("','", $RegL);
            $consulta = "SELECT LotID, Crop, Lot, Qty, Reg, SchDate,Cert,TotalQty,
            (SELECT RegNam FROM Region WHERE IDReg = Lots.Reg) as RegName, GinID,
            (SELECT GinName FROM Gines WHERE IDGin = Lots.GinID) as GinName, Location, Lots.CliID,
            (SELECT Cli FROM Clients WHERE Lots.CliID = Clients.CliID) as CliID,
            (SELECT RegNam FROM DOrds, Region WHERE Lots.DOrd = DOrds.DOrd and DOrds.InReg = Region.IDReg) as InReg,
            (SELECT Cli FROM DOrds, Clients WHERE Lots.DOrd = DOrds.DOrd and DOrds.InPlc = Clients.CliID) as CliDO,
            (SELECT RegNam FROM Region WHERE IDReg = Lots.Location) as LocName, LiqWgh, DOrd, TrkID, Qlty, AsgmDate, Cmt, Recap, Status 
            FROM Lots 
            WHERE Reg IN ('$reg') ORDER BY LotID";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data = $resultado->fetchAll(PDO::FETCH_ASSOC);        
        }elseif ($RegL != "" && $GinL != "" & $LocL == "" && $DOL == "" && $LotL == "" & $CliL == "" && $CrpL == "") {
            $reg1 = implode("','", $RegL);
            $consulta = "SELECT LotID, Crop, Lot, Qty, Reg, SchDate,Cert,TotalQty,
            (SELECT RegNam FROM Region WHERE IDReg = Lots.Reg) as RegName, GinID,
            (SELECT GinName FROM Gines WHERE IDGin = Lots.GinID) as GinName, Location, Lots.CliID,
            (SELECT Cli FROM Clients WHERE Lots.CliID = Clients.CliID) as CliID,
            (SELECT RegNam FROM DOrds, Region WHERE Lots.DOrd = DOrds.DOrd and DOrds.InReg = Region.IDReg) as InReg,
            (SELECT Cli FROM DOrds, Clients WHERE Lots.DOrd = DOrds.DOrd and DOrds.InPlc = Clients.CliID) as CliDO,
            (SELECT RegNam FROM Region WHERE IDReg = Lots.Location) as LocName, LiqWgh, DOrd, TrkID, Qlty, AsgmDate, Cmt, Recap, Status 
            FROM Lots 
            WHERE GinID = '$GinL' AND Reg IN ('$reg1') ORDER BY LotID";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data = $resultado->fetchAll(PDO::FETCH_ASSOC);
        }elseif ($RegL != "" && $GinL == "" & $LocL == "" && $DOL == "" && $LotL == "" & $CliL == "" && $CrpL != ""){
            $reg2 = implode("','", $RegL);
            $consulta = "SELECT LotID, Crop, Lot, Qty, Reg, SchDate,Cert,TotalQty,
            (SELECT RegNam FROM Region WHERE IDReg = Lots.Reg) as RegName, GinID,
            (SELECT GinName FROM Gines WHERE IDGin = Lots.GinID) as GinName, Location, Lots.CliID,
            (SELECT Cli FROM Clients WHERE Lots.CliID = Clients.CliID) as CliID,
            (SELECT RegNam FROM DOrds, Region WHERE Lots.DOrd = DOrds.DOrd and DOrds.InReg = Region.IDReg) as InReg,
            (SELECT Cli FROM DOrds, Clients WHERE Lots.DOrd = DOrds.DOrd and DOrds.InPlc = Clients.CliID) as CliDO,
            (SELECT RegNam FROM Region WHERE IDReg = Lots.Location) as LocName, LiqWgh, DOrd, TrkID, Qlty, AsgmDate, Cmt, Recap, Status 
            FROM Lots 
            WHERE Crop = '$CrpL' AND Reg IN ('$reg2') ORDER BY LotID";
            //(SELECT GinName FROM Gines WHERE IDGin = Location) as LocName
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);/* 
            print_r($consulta); */
        }elseif ($RegL != "" && $GinL != "" & $LocL == "" && $DOL == "" && $LotL == "" & $CliL == "" && $CrpL != ""){
            $reg2 = implode("','", $RegL);
            $consulta = "SELECT LotID, Crop, Lot, Qty, Reg, SchDate,Cert,TotalQty,
            (SELECT RegNam FROM Region WHERE IDReg = Lots.Reg) as RegName, GinID,
            (SELECT GinName FROM Gines WHERE IDGin = Lots.GinID) as GinName, Location, Lots.CliID,
            (SELECT Cli FROM Clients WHERE Lots.CliID = Clients.CliID) as CliID,
            (SELECT RegNam FROM DOrds, Region WHERE Lots.DOrd = DOrds.DOrd and DOrds.InReg = Region.IDReg) as InReg,
            (SELECT Cli FROM DOrds, Clients WHERE Lots.DOrd = DOrds.DOrd and DOrds.InPlc = Clients.CliID) as CliDO,
            (SELECT RegNam FROM Region WHERE IDReg = Lots.Location) as LocName, LiqWgh, DOrd, TrkID, Qlty, AsgmDate, Cmt, Recap, Status 
            FROM Lots 
            WHERE Crop = '$CrpL' AND GinID = '$GinL' AND Reg IN ('$reg2') ORDER BY LotID";
            //(SELECT GinName FROM Gines WHERE IDGin = Location) as LocName
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }elseif ($RegL == "" && $GinL == "" & $LocL != "" && $DOL == "" && $LotL == "" & $CliL == "" && $CrpL == "") {
            $loc = implode("','", $LocL);
            $consulta = "SELECT LotID, Crop, Lot, Qty, Reg, SchDate,Cert,TotalQty,
            (SELECT RegNam FROM Region WHERE IDReg = Lots.Reg) as RegName, GinID,
            (SELECT GinName FROM Gines WHERE IDGin = Lots.GinID) as GinName, Location, Lots.CliID,
            (SELECT Cli FROM Clients WHERE Lots.CliID = Clients.CliID) as CliID,
            (SELECT RegNam FROM DOrds, Region WHERE Lots.DOrd = DOrds.DOrd and DOrds.InReg = Region.IDReg) as InReg,
            (SELECT Cli FROM DOrds, Clients WHERE Lots.DOrd = DOrds.DOrd and DOrds.InPlc = Clients.CliID) as CliDO,
            (SELECT RegNam FROM Region WHERE IDReg = Lots.Location) as LocName, LiqWgh, DOrd, TrkID, Qlty, AsgmDate, Cmt, Recap, Status 
            FROM Lots 
            WHERE Location IN ('$loc') ORDER BY LotID";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data = $resultado->fetchAll(PDO::FETCH_ASSOC);
        }elseif ($RegL == "" && $GinL == "" & $LocL != "" && $DOL == "" && $LotL == "" & $CliL == "" && $CrpL != ""){
            $loc = implode("','", $LocL);
            $consulta = "SELECT LotID, Crop, Lot, Qty, Reg, SchDate,Cert,TotalQty,
            (SELECT RegNam FROM Region WHERE IDReg = Lots.Reg) as RegName, GinID,
            (SELECT GinName FROM Gines WHERE IDGin = Lots.GinID) as GinName, Location, Lots.CliID,
            (SELECT Cli FROM Clients WHERE Lots.CliID = Clients.CliID) as CliID,
            (SELECT RegNam FROM DOrds, Region WHERE Lots.DOrd = DOrds.DOrd and DOrds.InReg = Region.IDReg) as InReg,
            (SELECT Cli FROM DOrds, Clients WHERE Lots.DOrd = DOrds.DOrd and DOrds.InPlc = Clients.CliID) as CliDO,
            (SELECT RegNam FROM Region WHERE IDReg = Lots.Location) as LocName, LiqWgh, DOrd, TrkID, Qlty, AsgmDate, Cmt, Recap, Status 
            FROM Lots
            Where Location IN ('$loc') and Crop = '$CrpL' ORDER BY LotID";
            //(SELECT GinName FROM Gines WHERE IDGin = Location) as LocName
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }elseif ($RegL == "" && $GinL == "" & $LocL == "" && $DOL != "" && $LotL == "" & $CliL == "" && $CrpL == ""){
            $consulta = 'SELECT LotID, Crop, Lot, Qty, Reg, SchDate,Cert,TotalQty,
            (SELECT RegNam FROM Region WHERE IDReg = Reg) as RegName, GinID,
            (SELECT GinName FROM Gines WHERE IDGin = GinID) as GinName, Location, Lots.CliID,
            (SELECT Cli FROM Clients WHERE Lots.CliID = Clients.CliID) as CliID,
            (SELECT RegNam FROM DOrds, Region WHERE Lots.DOrd = DOrds.DOrd and DOrds.InReg = Region.IDReg) as InReg,
            (SELECT Cli FROM DOrds, Clients WHERE Lots.DOrd = DOrds.DOrd and DOrds.InPlc = Clients.CliID) as CliDO,
            (SELECT RegNam FROM Region WHERE IDReg = Location) as LocName, LiqWgh, DOrd, TrkID, Qlty, AsgmDate, Cmt, Recap, Status 
            FROM Lots 
            Where DOrd = '.$DOL.' ORDER BY LotID';
            //(SELECT GinName FROM Gines WHERE IDGin = Location) as LocName
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }elseif ($RegL == "" && $GinL == "" & $LocL == "" && $DOL == "" && $LotL != "" & $CliL == "" && $CrpL == ""){
            $array = array_map('lowercase', explode(',', $LotL));
            //$array=array_map('lowercase', $LotL);
            $array = implode(",",$array);

            $consulta = 'SELECT LotID, Crop, Lot, Qty, Reg, SchDate,Cert,TotalQty,
            (SELECT RegNam FROM Region WHERE IDReg = Reg) as RegName, GinID,
            (SELECT GinName FROM Gines WHERE IDGin = GinID) as GinName, Location, Lots.CliID,
            (SELECT Cli FROM Clients WHERE Lots.CliID = Clients.CliID) as CliID,
            (SELECT RegNam FROM DOrds, Region WHERE Lots.DOrd = DOrds.DOrd and DOrds.InReg = Region.IDReg) as InReg,
            (SELECT Cli FROM DOrds, Clients WHERE Lots.DOrd = DOrds.DOrd and DOrds.InPlc = Clients.CliID) as CliDO,
            (SELECT RegNam FROM Region WHERE IDReg = Location) as LocName, LiqWgh, DOrd, TrkID, Qlty, AsgmDate, Cmt, Recap, Status 
            FROM Lots 
            Where Lot IN ('.$array.') ORDER BY LotID';
            //(SELECT GinName FROM Gines WHERE IDGin = Location) as LocName
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }elseif ($RegL == "" && $GinL == "" & $LocL != "" && $DOL == "" && $LotL != "" & $CliL == "" && $CrpL == ""){
            $loc2 = implode("','", $LocL);
            $array = array_map('lowercase', explode(',', $LotL));
            //$array=array_map('lowercase', $LotL);
            $array1 = implode("','",$array);

            $consulta = "SELECT LotID, Crop, Lot, Qty, Reg, SchDate,Cert,TotalQty,
            (SELECT RegNam FROM Region WHERE IDReg = Lots.Reg) as RegName, GinID,
            (SELECT GinName FROM Gines WHERE IDGin = Lots.GinID) as GinName, Location, Lots.CliID,
            (SELECT Cli FROM Clients WHERE Lots.CliID = Clients.CliID) as CliID,
            (SELECT RegNam FROM DOrds, Region WHERE Lots.DOrd = DOrds.DOrd and DOrds.InReg = Region.IDReg) as InReg,
            (SELECT Cli FROM DOrds, Clients WHERE Lots.DOrd = DOrds.DOrd and DOrds.InPlc = Clients.CliID) as CliDO,
            (SELECT RegNam FROM Region WHERE IDReg = Lots.Location) as LocName, LiqWgh, DOrd, TrkID, Qlty, AsgmDate, Cmt, Recap, Status 
            FROM Lots 
            Where Location IN ('$loc2') and Lot IN ($array1)  ORDER BY LotID";
            //print_r($consulta);
            //(SELECT GinName FROM Gines WHERE IDGin = Location) as LocName
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }elseif ($RegL == "" && $GinL == "" & $LocL == "" && $DOL == "" && $LotL == "" & $CliL != "" && $CrpL == ""){
            $consulta = 'SELECT LotID, Crop, Lot, Qty, Reg, SchDate,Cert,TotalQty,
            (SELECT RegNam FROM Region WHERE IDReg = Reg) as RegName, GinID,
            (SELECT GinName FROM Gines WHERE IDGin = GinID) as GinName, Location, Lots.CliID,
            (SELECT Cli FROM Clients WHERE Lots.CliID = Clients.CliID) as CliID,
            (SELECT RegNam FROM DOrds, Region WHERE Lots.DOrd = DOrds.DOrd and DOrds.InReg = Region.IDReg) as InReg,
            (SELECT Cli FROM DOrds, Clients WHERE Lots.DOrd = DOrds.DOrd and DOrds.InPlc = Clients.CliID) as CliDO,
            (SELECT RegNam FROM Region WHERE IDReg = Location) as LocName, LiqWgh, DOrd, TrkID, Qlty, AsgmDate, Cmt, Recap, Status 
            FROM Lots 
            Where CliID = '.$CliL.' ORDER BY LotID';
            //(SELECT GinName FROM Gines WHERE IDGin = Location) as LocName
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }elseif ($RegL == "" && $GinL == "" & $LocL == "" && $DOL == "" && $LotL == "" & $CliL == "" && $CrpL != ""){
            $consulta = 'SELECT LotID, Crop, Lot, Qty, Reg, SchDate,Cert,TotalQty,
            (SELECT RegNam FROM Region WHERE IDReg = Reg) as RegName, GinID,
            (SELECT GinName FROM Gines WHERE IDGin = GinID) as GinName, Location, Lots.CliID,
            (SELECT Cli FROM Clients WHERE Lots.CliID = Clients.CliID) as CliID,
            (SELECT RegNam FROM DOrds, Region WHERE Lots.DOrd = DOrds.DOrd and DOrds.InReg = Region.IDReg) as InReg,
            (SELECT Cli FROM DOrds, Clients WHERE Lots.DOrd = DOrds.DOrd and DOrds.InPlc = Clients.CliID) as CliDO,
            (SELECT RegNam FROM Region WHERE IDReg = Location) as LocName, LiqWgh, DOrd, TrkID, Qlty, AsgmDate, Cmt, Recap, Status 
            FROM Lots 
            Where Crop = '.$CrpL.' ORDER BY LotID';
            //(SELECT GinName FROM Gines WHERE IDGin = Location) as LocName
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }    
        /*$consulta = "SELECT LotID, Crop, Lot, Qty, Reg, SchDate,
        (SELECT RegNam FROM Region WHERE IDReg = Reg) as RegName, GinID,
        (SELECT GinName FROM Gines WHERE IDGin = GinID) as GinName, Location, Lots.CliID,
        (SELECT Cli FROM Clients WHERE Lots.CliID = Clients.CliID) as CliID,
        (SELECT RegNam FROM DOrds, Region WHERE Lots.DOrd = DOrds.DOrd and DOrds.InReg = Region.IDReg) as InReg,
        (SELECT Cli FROM DOrds, Clients WHERE Lots.DOrd = DOrds.DOrd and DOrds.InPlc = Clients.CliID) as CliDO,
        (SELECT RegNam FROM Region WHERE IDReg = Location) as LocName, LiqWgh, DOrd, TrkID, Qlty, AsgmDate, Cmt, Recap, Status FROM Lots ORDER BY LotID";
        //(SELECT GinName FROM Gines WHERE IDGin = Location) as LocName
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);*/
        break;
    case 5:    
        $consulta = "SELECT DrvNam, DrvTel, OutDat, InDat,Status,
        (SELECT TptCo FROM Transports WHERE TptID = TNam) as TNam
        FROM Truks 
        WHERE TrkID='$TrkID'"; 
        //Lots WHERE Status='Enable'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 6:    
        $consulta = "SELECT * FROM Region WHERE IsOrigin=1 ORDER BY RegNam ASC"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 7:
        $consulta = "SELECT IDReg FROM Region WHERE RegNam='$Reg'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataR=$resultado->fetch();
        $RegID = $dataR['IDReg'];
        
        $consulta = "SELECT IDGin, GinName FROM Gines WHERE IDReg='$RegID' ORDER BY GinName ASC"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 8:    
        $consulta = "SELECT * FROM Region ORDER BY RegNam ASC"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 9:    
        $consulta = "SELECT * FROM Clients ORDER BY Cli ASC"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 10:
        $consulta = "SELECT IDReg FROM Region WHERE RegNam='$Reg'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataR=$resultado->fetch();
        $RegID = $dataR['IDReg'];
        
        $consulta = "SELECT * FROM Gines WHERE IDReg='$RegID' ORDER BY GinName ASC"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 11:    
        $consulta = "SELECT Qty, Typ,
        (SELECT IFNULL(SUM(Bales),0) FROM Stg WHERE DO=DOrd and Stg.TypeStg='Adjustment') + Qty as TTQty,
        (SELECT RegNam FROM Region WHERE IDReg = OutPlc) as OutPlc,
        (SELECT GinName FROM Gines WHERE IDGin = Gin) as Gin,
        (SELECT RegNam FROM Region WHERE IDReg = InReg) as InReg,
        (SELECT Cli FROM Clients WHERE Clients.CliID = InPlc) as CliID
        FROM DOrds WHERE DOrd='$DOrd'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 12:
        $consulta = "SELECT IDGin, GinName FROM Gines WHERE IDReg = '$Region' ORDER BY GinName;"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 13:
        if ($GinID == ""){
            $consulta = "SELECT LotID, Lot, Qty, Cli
            FROM Lots
            left join Clients on Lots.CliID = Clients.CliID
            WHERE Reg = '$Reg' and  Location = '$Loc' and DOrd = '0'
            order by Lot;";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();        
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }else{
            $consulta = "SELECT LotID, Lot, Qty, Cli
            FROM Lots
            left join Clients on Lots.CliID = Clients.CliID
            WHERE Reg = '$Reg' and GinID = '$GinID' and  Location = '$Loc' and DOrd = '0'
            order by Lot;";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();        
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        }
        break;
    case 14:
    
        /*$array = array_map('lowercase', explode(',', $Lotes));
        $array = implode(",",$array);

        $consulta = 'UPDATE Lots SET Cmt= "'.$Cmt.'", CliID='.$Cli.', SchDate='.$SchDate.' WHERE LotID in ('.$array.');';
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll();*/
        
        $array = array_map('intval', explode(',', $Lotes));
        
        foreach ($array as $val) {
            $consulta = "UPDATE Lots SET Cmt='$Cmt', CliID='$Cli', SchDate='$SchDate' WHERE LotID = '$val';";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
            $data=$resultado->fetchAll();
        }
        break;
}

print json_encode($data, JSON_UNESCAPED_UNICODE); //envio el array final el formato json a AJAX
$conexion=null;
