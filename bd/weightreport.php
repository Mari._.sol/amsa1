<?php

include_once ('conexion.php');
$objeto = new Conexion();
$conexion = $objeto->Conectar();

$regsalida = (!empty($_GET['salidareg'])) ? $_GET['salidareg'] : '';
$regllegada = (!empty($_GET['llegadareg'])) ? $_GET['llegadareg'] : '';
$fromdate = (!empty($_GET['fromdate_pesos'])) ? $_GET['fromdate_pesos'] : '';
$todate = (!empty($_GET['todate_pesos'])) ? $_GET['todate_pesos'] : '';
$cliente = (!empty($_GET['cliente'])) ? $_GET['cliente'] : '';
$gin = (!empty($_GET['origen'])) ? $_GET['origen'] : '';
//$pendiente = (!empty($_GET['pendiente'])) ? $_GET['pendiente'] : '';

date_default_timezone_set("America/Mexico_City");

$fecha_hoy=date("Y-m-d");

$myarray = [];


$complemento="ORDER BY Truks.OutDat,Truks.TrkID ASC;";
$compgin ="";

if($gin!=""){
    $compgin=" AND Gines.GinName ='$gin' ";
}

//buscar por region de salida y region de llegada
if($regsalida!="" && $regllegada !="" && $cliente==""){
    $complemento= " HAVING  RegionSalida = '$regsalida'  AND RegionLlegada = '$regllegada' ORDER BY Truks.OutDat,Truks.TrkID ASC;";
}

//buscar solo por region de salida
if($regsalida!="" && $regllegada =="" && $cliente==""){
    $complemento= " HAVING  RegionSalida = '$regsalida'   ORDER BY Truks.OutDat,Truks.TrkID ASC;";
}

//buscar solo por region de llegada
if($regsalida=="" && $regllegada !="" && $cliente==""){
    $complemento= " HAVING  RegionLlegada = '$regllegada'  ORDER BY Truks.OutDat,Truks.TrkID ASC;";
}

//buscar solo por cliente en especifico
if($regsalida=="" && $regllegada =="CLIENTE" && $cliente!=""){
    $complemento= "AND Clients.Cli='$cliente' HAVING  RegionLlegada = '$regllegada'   ORDER BY Truks.OutDat,Truks.TrkID ASC;";
}


//buscar por region de salida, region de llegada y cliente en especifico
if($regsalida!="" && $regllegada =="CLIENTE" && $cliente!=""){
    $complemento= "AND Clients.Cli='$cliente' HAVING  RegionSalida = '$regsalida' AND RegionLlegada = '$regllegada'  ORDER BY Truks.OutDat,Truks.TrkID ASC;";
}


// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill, Alignment};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//use PhpOffice\PhpSpreadsheet\Style\Alignment as alignment;
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
$heading = false;


$consulta="SELECT Lots.TrkID,Lots.DOrd,Lots.Lot,Lots.Crop,Lots.Qty,Region.RegNam as RegionSalida,Gines.GinName,Truks.OutDat AS FechaSalida,Truks.InDat AS Fechallegada,Truks.Status,Clients.BnName as Cliente,
Transports.BnName as Linea,Truks.DrvNam AS CHOFER,ROUND(((Truks.OutWgh/Truks.CrgQty)*Lots.Qty),2) as pesosalida,ROUND(((Truks.InWgh/Truks.CrgQty)*Lots.Qty),2) as pesollegada,Clients.Cli,
DOrds.Ctc AS Contrato,Truks.InvoiceComment,DOrds.Typ,ROUND(((Truks.PWgh/Truks.CrgQty)*Lots.Qty),2) as pesocompra,
IF(((Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd))=99001,
(Select Town From amsadb1.Clients Where  Clients.CliID = (Select InPlc From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd)),
((Select Town From amsadb1.Region Where Region.IDReg =(Select InReg From amsadb1.DOrds Where Lots.DOrd = DOrds.DOrd) ))) as CiudadLlegada,R1.RegNam as RegionLlegada
From amsadb1.Lots
LEFT JOIN amsadb1.Truks
ON Lots.TrkID = Truks.TrkID
LEFT JOIN amsadb1.DOrds
ON  Lots.DOrd = DOrds.DOrd
LEFT JOIN amsadb1.Clients
ON Clients.CliID = DOrds.InPlc
LEFT JOIN amsadb1.Gines
ON  Lots.GinID=Gines.IDGin 
LEFT JOIN amsadb1.Region
ON  DOrds.OutPlc=Region.IDReg 
LEFT JOIN amsadb1.Transports
ON Truks.TNam = Transports.TptID
LEFT JOIN amsadb1.Region R1
ON  DOrds.InReg=R1.IDReg 
WHERE Truks.TNam !=137  AND  Truks.Status !='Cancelled' AND  Truks.CrgQty > 0  AND Truks.OutDat BETWEEN '$fromdate' AND '$todate' ".$compgin.$complemento;

/*$consulta = "SELECT   Truks.TrkID,Truks.DO,Truks.OutDat,Truks.InDat,Truks.CrgQty as Bal,Truks.LotsAssc,Region.RegNam as RegSal,R1.RegNam as Reglleg,Gines.GinName,Clients.Cli,Truks.OutWgh,Truks.InWgh,Truks.PWgh,Truks.Status,Truks.TNam
FROM amsadb1.Truks
left join amsadb1.DOrds
ON  Truks.DO= DOrds.DOrd
left join amsadb1.Region
ON DOrds.OutPlc = Region.IDReg
left join amsadb1.Region as R1
ON DOrds.InReg = R1.IDReg
left join amsadb1.Gines
ON DOrds.Gin = Gines.IDGin
left join amsadb1.Clients
ON DOrds.InPlc = Clients.CliID
where Truks.OutDat > '2023-01-01'  AND  Truks.CrgQty > 0 AND ((Truks.OutWgh = 0 AND (SELECT TIMESTAMPDIFF(DAY,  Truks.OutDat,'$fecha_hoy')) > 2 ) OR (Truks.InWgh = 0 AND  (SELECT TIMESTAMPDIFF(DAY,  Truks.InDat, '$fecha_hoy')) > 2  ) OR Truks.PWgh = 0)  AND Truks.Status != 'Cancelled' 
AND Truks.TNam != 137 ORDER BY Truks.OutDat,Truks.TrkIDDESC;";*/

$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$siexiste=0; //$data=$resultado->fetchAll(PDO::FETCH_ASSOC);




$fileName = "Weight Report-".date('d-m-Y').".xlsx";

$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("WeightReport");
//$hojaActiva->freezePane("A2");



//negritas en encabezado

$hojaActiva->getStyle('A1:W1')->getFont()->setBold( true );

//relleno de celda encabezado
$hojaActiva->getStyle('A1:W1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFE1E1E1');

//fijar primera fila
$hojaActiva->freezePane('A2');


$hojaActiva->getColumnDimension('A')->setWidth(12);
$hojaActiva->setCellValue('A1','TruckID');
$hojaActiva->getColumnDimension('B')->setWidth(12);
$hojaActiva->setCellValue('B1','Crop');
$hojaActiva->getColumnDimension('C')->setWidth(12);
$hojaActiva->setCellValue('C1','Type');
$hojaActiva->getColumnDimension('D')->setWidth(12);
$hojaActiva->setCellValue('D1','DO');
$hojaActiva->getColumnDimension('E')->setWidth(12);
$hojaActiva->setCellValue('E1','Lot');
$hojaActiva->getColumnDimension('F')->setWidth(12);
$hojaActiva->setCellValue('F1','Bales');
$hojaActiva->getColumnDimension('G')->setWidth(12);
$hojaActiva->setCellValue('G1','Departure Date');
$hojaActiva->getColumnDimension('H')->setWidth(12);
$hojaActiva->setCellValue('H1','Arrival Date');
$hojaActiva->getColumnDimension('I')->setWidth(12);
$hojaActiva->setCellValue('I1','Gin');
$hojaActiva->getColumnDimension('J')->setWidth(12);
$hojaActiva->setCellValue('J1','Departure Region');
$hojaActiva->getColumnDimension('K')->setWidth(12);
$hojaActiva->setCellValue('K1','Arrival Region');
$hojaActiva->getColumnDimension('L')->setWidth(12);
$hojaActiva->setCellValue('L1','Client');
/*$hojaActiva->getColumnDimension('M')->setWidth(12);
$hojaActiva->setCellValue('M1','Bussiness Name');*/
$hojaActiva->getColumnDimension('M')->setWidth(12);
$hojaActiva->setCellValue('M1','Purchase Weight');
$hojaActiva->getColumnDimension('N')->setWidth(12);
$hojaActiva->setCellValue('N1','Departure Weight');
$hojaActiva->getColumnDimension('O')->setWidth(12);
$hojaActiva->setCellValue('O1','Arrival Weight');
$hojaActiva->getColumnDimension('P')->setWidth(12);
$hojaActiva->setCellValue('P1','Purchase Weight Avg');
$hojaActiva->getColumnDimension('Q')->setWidth(12);
$hojaActiva->setCellValue('Q1','Departure Weight Avg');
$hojaActiva->getColumnDimension('R')->setWidth(12);
$hojaActiva->setCellValue('R1','Arrival Weight Avg');
$hojaActiva->getColumnDimension('S')->setWidth(12);
$hojaActiva->setCellValue('S1','Dif Arrival vs Departure');
$hojaActiva->getColumnDimension('T')->setWidth(12);
$hojaActiva->setCellValue('T1','Dif Arrival vs Purchase');
$hojaActiva->getColumnDimension('U')->setWidth(12);
$hojaActiva->setCellValue('U1','Status');
$hojaActiva->getColumnDimension('V')->setWidth(12);
$hojaActiva->setCellValue('V1','Missing Weight');
$hojaActiva->getColumnDimension('W')->setWidth(12);
$hojaActiva->setCellValue('W1','Transport');





$fila = 2;
$fecha_salida="";
$fecha_llegada="";
$trkanterior=0;
while($row = $resultado->fetch(PDO::FETCH_ASSOC)){
    if($siexiste==0){
        if(!empty($row['TrkID']))
            $siexiste=1;
    }
    
    
     $fecha_salida = strtotime($row['FechaSalida']);
     $fecha_salida =25569 + ($fecha_salida / 86400);
     $fecha_llegada =strtotime($row['Fechallegada']);
     $fecha_llegada =25569 + ($fecha_llegada / 86400);

     $prompesocompra=round(($row['pesocompra'])/($row['Qty']),2);
     $prompesosalida=round(($row['pesosalida'])/($row['Qty']),2);
     $prompesollegada=round(($row['pesollegada'])/($row['Qty']),2);

     if($row['pesocompra']>0 && $row['pesosalida'] >0 && $row['pesollegada']>0 ){
        $faltapeso ="No";
     }
     else{
        $faltapeso ="Yes";
     }

     //Dif Arrival vs Departure
     if($prompesollegada>0 && $prompesosalida>0){
     $dif1=round($prompesollegada-$prompesosalida,2);
     }
     else{
        $dif1=0;
     }
     //Dif Arrival vs Purchase
     if($prompesollegada>0 && $prompesocompra>0){
        $dif2=round($prompesollegada-$prompesocompra,2);
     }
     else{
        $dif2=0;
     }

     if($trkanterior == $row['TrkID']){

        array_push($myarray,($fila-1));
        array_push($myarray,$fila);
     }
     

    
     
     
      $hojaActiva->getStyle('G' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    $hojaActiva->getStyle('H' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    
    $hojaActiva->setCellValue('A' . $fila,$row['TrkID']);
    $hojaActiva->setCellValue('B' . $fila,$row['Crop']);
    $hojaActiva->setCellValue('C' . $fila,$row['Typ']);
    $hojaActiva->setCellValue('D' . $fila, $row['DOrd']);
    //$hojaActiva->setCellValue('E' . $fila,$row['Lot']);
    $hojaActiva->setCellValue('F' . $fila,$row['Qty']);
    $hojaActiva->setCellValue('G' . $fila,$fecha_salida);
    $hojaActiva->setCellValue('H'. $fila,$fecha_llegada);
    $hojaActiva->setCellValue('I'. $fila,$row['GinName']);
    $hojaActiva->setCellValue('J'. $fila,$row['RegionSalida']);
    $hojaActiva->setCellValue('K'. $fila,$row['RegionLlegada']);
    $hojaActiva->setCellValue('L'. $fila,$row['Cli']);
    //$hojaActiva->setCellValue('M'. $fila,$row['Cliente']); 
    $hojaActiva->setCellValue('M'. $fila,$row['pesocompra']); 
    $hojaActiva->setCellValue('N'. $fila,$row['pesosalida']);  
    $hojaActiva->setCellValue('O' . $fila,$row['pesollegada']);
    $hojaActiva->setCellValue('P' . $fila,$prompesocompra);
    $hojaActiva->setCellValue('Q'. $fila,$prompesosalida);
    $hojaActiva->setCellValue('R'. $fila,$prompesollegada);
    $hojaActiva->setCellValue('S'. $fila,$dif1);
    $hojaActiva->setCellValue('T'. $fila,$dif2);
    $hojaActiva->setCellValue('U'. $fila,$row['Status']);
    $hojaActiva->setCellValue('V'. $fila,$faltapeso);
    $hojaActiva->setCellValue('W'. $fila,$row['Linea']);
    
    
    
     //CAMBIAR COLUMNA LOTE A TEXTO PARA QUE NO LO TOME COMO FORMULA
    $style = $hojaActiva->getStyle('E'. $fila);
    $style->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
    $hojaActiva->getCell('E'. $fila)->setValueExplicit($row['Lot'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
 
    $trkanterior = $row['TrkID'];
    $fila++;
}
$hojaActiva->getStyle('M2:M'.$fila)->getNumberFormat()->setFormatCode('###0.00');
$hojaActiva->getStyle('N2:N'.$fila)->getNumberFormat()->setFormatCode('###0.00');
$hojaActiva->getStyle('O2:O'.$fila)->getNumberFormat()->setFormatCode('###0.00');
$hojaActiva->getStyle('P2:P'.$fila)->getNumberFormat()->setFormatCode('###0.00');
$hojaActiva->getStyle('Q2:Q'.$fila)->getNumberFormat()->setFormatCode('###0.00');
$hojaActiva->getStyle('R2:R'.$fila)->getNumberFormat()->setFormatCode('###0.00');
$hojaActiva->getStyle('S2:S'.$fila)->getNumberFormat()->setFormatCode('###0.00');
$hojaActiva->getStyle('T2:T'.$fila)->getNumberFormat()->setFormatCode('###0.00');

$longitud = count($myarray);
for($i=0; $i<$longitud; $i++){

    $hojaActiva->getStyle('A' . $myarray[$i])->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFFFFF00');

}





header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');
ob_end_clean();
$writer = IOFactory::createWriter($excel, 'Xlsx');
$writer->save('php://output');
exit;


?>