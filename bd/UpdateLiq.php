<?php
include_once '../bd/conexion.php';
require 'vendor/autoload.php'; // Cargar PhpSpreadsheet
date_default_timezone_set("America/Mexico_City");

use PhpOffice\PhpSpreadsheet\IOFactory;

$objeto = new Conexion();
$conexion = $objeto->Conectar();

$liqIDs = []; 
$TipoLiqs = [];

// Verifica si se ha cargado el archivo
if (isset($_FILES['fileExcel']['name']) && $_FILES['fileExcel']['size'] > 0) {
    $fileName = $_FILES['fileExcel']['tmp_name'];
    $fileExtension = $_POST['fileExtension'];

    // Cargar el archivo Excel
    $spreadsheet = IOFactory::load($fileName);
    $worksheet = $spreadsheet->getSheetByName('InfoLiq');
    $totalRows = $worksheet->getHighestRow(); 


    if ($totalRows >= 2) {
        $row = 2;
        
        // Obtener todas las celdas de la fila 2
        $cellIterator = $worksheet->getRowIterator($row, $row)->current()->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(false); 
        
        $data = [];
        foreach ($cellIterator as $cell) {
            $data[] = $cell->getValue(); // Almacenar los valores de las celdas
        }
        
        $region = $data[0];
        $proveedor = $data[1];
        $contrato = $data[2];
        $liqID = $data[3];
        $pacas = $data[4];
        $precio = $data[5];
        $pesoProm = $data[6];
        $TipoPago = $data[7];
        $TipoLiq = $data[8];
        $TipoDesc = $data[9];
        $No = $data[10];
        $SubNo = $data[11];
        $FechaEnv = $data[12];
        $SinDescuento = $data[13];
        $ConDescuento = $data[14];
        $Prom = $data[15];
        $Pago = $data[16];
        $PagoPorc = $data[17];
        $Grado = $data[18];
        $Micro = $data[19];
        $Fibra = $data[20];
        $Resis = $data[21];
        $Otro = $data[22];
        $Total = $data[23];
        $SM = $data[24];
        $MP = $data[25];
        $M = $data[26];
        $SLMP = $data[27];
        $SLM = $data[28];
        $LMP = $data[29];
        $LM = $data[30];
        $SGOP = $data[31];
        $SGO = $data[32];
        $GO = $data[33];
        $O = $data[34];
        $mic1 = $data[35];
        $mic2 = $data[36];
        $mic3 = $data[37];
        $mic4 = $data[38];
        $mic5 = $data[39];
        $mic6 = $data[40];
        $fib1 = $data[41];
        $fib2 = $data[42];
        $fib3 = $data[43];
        $fib4 = $data[44];
        $fib5 = $data[45];
        $res1 = $data[46];
        $res2 = $data[47];
        $res3 = $data[48];
        $res4 = $data[49];
        $res5 = $data[50];
        $res6 = $data[51];
        $Grd = $data[52];
        $Mic = $data[53];
        $Len = $data[54];
        $Str = $data[55];
        $Comentarios = !empty($data[56]) ? $data[56] : 'N/A';
        
        if (is_numeric($FechaEnv)) {
            $FechaEnv = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($FechaEnv)->format('Y-m-d');
        } else {
            $FechaEnv = (new DateTime($FechaEnv))->format('Y-m-d');
        }

       
        $consulta = "UPDATE amsadb1.Liquidation SET
        Region = :Region,
        Proveedor = :Proveedor,
        Contrato = :Contrato,
        LiqID = :LiqID,
        Pacas = :Pacas,
        Precio = :Precio,
        PesoProm = :PesoProm,
        TipoPago = :TipoPago,
        TipoLiq = :TipoLiq,
        TipoDesc = :TipoDesc,
        No = :No,
        SubNo = :SubNo,
        FechaEnv = :FechaEnv,
        SinDescuento = :SinDescuento,
        ConDescuento = :ConDescuento,
        Prom = :Prom,
        Pago = :Pago,
        PagoPorc = :PagoPorc,
        Grado = :Grado,
        Micro = :Micro,
        Fibra = :Fibra,
        Resis = :Resis,
        Otro = :Otro,
        Total = :Total,
        SM = :SM,
        MP = :MP,
        M = :M,
        SLMP = :SLMP,
        SLM = :SLM,
        LMP = :LMP,
        LM = :LM,
        SGOP = :SGOP,
        SGO = :SGO,
        GO = :GO,
        O = :O,
        mic1 = :mic1,
        mic2 = :mic2,
        mic3 = :mic3,
        mic4 = :mic4,
        mic5 = :mic5,
        mic6 = :mic6,
        fib1 = :fib1,
        fib2 = :fib2,
        fib3 = :fib3,
        fib4 = :fib4,
        fib5 = :fib5,
        res1 = :res1,
        res2 = :res2,
        res3 = :res3,
        res4 = :res4,
        res5 = :res5,
        res6 = :res6,
        Grd = :Grd,
        Mic = :Mic,
        Len = :Len,
        Str = :Str,
        Comentarios = :Comentarios
        WHERE LiqID = :LiqID and TipoLiq = :TipoLiq"; 

        // Asignar parámetros correctamente
        $resultado = $conexion->prepare($consulta);
        $resultado->bindParam(':Region', $region);
        $resultado->bindParam(':Proveedor', $proveedor);
        $resultado->bindParam(':Contrato', $contrato);
        $resultado->bindParam(':LiqID', $liqID);
        $resultado->bindParam(':Pacas', $pacas);
        $resultado->bindParam(':Precio', $precio);
        $resultado->bindParam(':PesoProm', $pesoProm);
        $resultado->bindParam(':TipoPago', $TipoPago);
        $resultado->bindParam(':TipoLiq', $TipoLiq);
        $resultado->bindParam(':TipoDesc', $TipoDesc);
        $resultado->bindParam(':No', $No);
        $resultado->bindParam(':SubNo', $SubNo);
        $resultado->bindParam(':FechaEnv', $FechaEnv);
        $resultado->bindParam(':SinDescuento', $SinDescuento);
        $resultado->bindParam(':ConDescuento', $ConDescuento);
        $resultado->bindParam(':Prom', $Prom);
        $resultado->bindParam(':Pago', $Pago);
        $resultado->bindParam(':PagoPorc', $PagoPorc);
        $resultado->bindParam(':Grado', $Grado);
        $resultado->bindParam(':Micro', $Micro);
        $resultado->bindParam(':Fibra', $Fibra);
        $resultado->bindParam(':Resis', $Resis);
        $resultado->bindParam(':Otro', $Otro);
        $resultado->bindParam(':Total', $Total);
        $resultado->bindParam(':SM', $SM);
        $resultado->bindParam(':MP', $MP);
        $resultado->bindParam(':M', $M);
        $resultado->bindParam(':SLMP', $SLMP);
        $resultado->bindParam(':SLM', $SLM);
        $resultado->bindParam(':LMP', $LMP);
        $resultado->bindParam(':LM', $LM);
        $resultado->bindParam(':SGOP', $SGOP);
        $resultado->bindParam(':SGO', $SGO);
        $resultado->bindParam(':GO', $GO);
        $resultado->bindParam(':O', $O);
        $resultado->bindParam(':mic1', $mic1);
        $resultado->bindParam(':mic2', $mic2);
        $resultado->bindParam(':mic3', $mic3);
        $resultado->bindParam(':mic4', $mic4);
        $resultado->bindParam(':mic5', $mic5);
        $resultado->bindParam(':mic6', $mic6);
        $resultado->bindParam(':fib1', $fib1);
        $resultado->bindParam(':fib2', $fib2);
        $resultado->bindParam(':fib3', $fib3);
        $resultado->bindParam(':fib4', $fib4);
        $resultado->bindParam(':fib5', $fib5);
        $resultado->bindParam(':res1', $res1);
        $resultado->bindParam(':res2', $res2);
        $resultado->bindParam(':res3', $res3);
        $resultado->bindParam(':res4', $res4);
        $resultado->bindParam(':res5', $res5);
        $resultado->bindParam(':res6', $res6);
        $resultado->bindParam(':Grd', $Grd);
        $resultado->bindParam(':Mic', $Mic);
        $resultado->bindParam(':Len', $Len);
        $resultado->bindParam(':Str', $Str);
        $resultado->bindParam(':Comentarios', $Comentarios);
                    
        if ($resultado->execute()) {
            $liqIDs[] = $liqID; 
            $TipoLiqs[] = $TipoLiq;
        }     
    }
    echo json_encode([
        "message" => "Datos insertados correctamente", 
        "liqID" => $liqIDs,
        "TipoLiq" => $TipoLiqs
    ]);    
} else {
    echo json_encode("No se ha cargado ningún archivo");
}

$conexion = null;

?>
