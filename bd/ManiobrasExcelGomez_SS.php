<?php

include_once ('conexion.php');
$objeto = new Conexion();
$conexion = $objeto->Conectar();

//$fromdate = (!empty($_GET['fechainicio'])) ? $_GET['fechainicio'] : '';
//$todate = (!empty($_GET['fechafin'])) ? $_GET['fechafin'] : '';
$region = (!empty($_GET['region'])) ? $_GET['region'] : '';

$clave=651;
$clave2=650;

$fecha=date('Y-m-d');
// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill, Alignment};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//use PhpOffice\PhpSpreadsheet\Style\Alignment as alignment;
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
$heading = false;

$fileName = "Maniobras-Gomez-".date('Y-m-d').".xlsx";

//SE CREA Y SE EMPIEZA A LLENAR LA HOJA
$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Maniobras");

//negritas en encabezado

$hojaActiva->getStyle('A1:R1')->getFont()->setBold( true );

//relleno de celda encabezado
$hojaActiva->getStyle('A1:R1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFE1E1E1');
//fijar primera fila
$hojaActiva->freezePane('A2');

$hojaActiva->getColumnDimension('A')->setWidth(12);
$hojaActiva->setCellValue('A1','TruckID');
$hojaActiva->getColumnDimension('B')->setWidth(12);
$hojaActiva->setCellValue('B1','Movimiento');
$hojaActiva->getColumnDimension('C')->setWidth(12);
$hojaActiva->setCellValue('C1','Destino');
$hojaActiva->getColumnDimension('D')->setWidth(34);
$hojaActiva->setCellValue('D1','DO');
$hojaActiva->getColumnDimension('E')->setWidth(12);
$hojaActiva->setCellValue('E1','Lot');
$hojaActiva->getColumnDimension('F')->setWidth(12);
$hojaActiva->setCellValue('F1','Bales');
$hojaActiva->getColumnDimension('G')->setWidth(12);
$hojaActiva->setCellValue('G1','Date');
$hojaActiva->getColumnDimension('H')->setWidth(12);
$hojaActiva->setCellValue('H1','Maniobra');
$hojaActiva->getColumnDimension('I')->setWidth(12);
$hojaActiva->setCellValue('I1','Costo x Lote');
$hojaActiva->getColumnDimension('J')->setWidth(12);
$hojaActiva->setCellValue('J1','Factura');
$hojaActiva->getColumnDimension('K')->setWidth(12);
$hojaActiva->setCellValue('K1','PQ');

//centrar celdas unidas horizontalmente 
$hojaActiva->getStyle('A1:R1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


//LLEGADAS GOMEZ
$query="SELECT Truks.TrkID,Transports.BnName,Truks.WBill,Lots.Lot,Lots.Qty,Gines.GinName,Region.RegNam,Truks.Status,
        Truks.InDat as fecha,Truks.TipoManArr as TipoMan,Truks.RespManArr as RespMan,Truks.NotaSalArr as NotaSal,Maniobras.CostoMan as costoxpaca,Truks.ComentarioManArr as ComentarioMan,
        (Maniobras.CostoMan * Lots.Qty )as costoxlote,Truks.FacManArr AS Factura,
        concat(concat((IF((Select IsOrigin From Region Where DOrds.OutPlc = IDReg)=1,(Select Cde from Gines where Truks.TrkID = Lots.TrkID and Lots.GinID = Gines.IDGin),(Select Cde from Region where Truks.TrkID = Lots.TrkID and DOrds.OutPlc = Region.IDReg))),'-',
        (IF((Select DOrds.Typ From DOrds Where Truks.DO = DOrds.DOrd)='CON',(Select Cde From Region Where DOrds.InReg = Region.IDReg),(Select Cde From Clients Where DOrds.InPlc = Clients.CliID)))),'/',Truks.DO) as DO,



        (CASE 
            when DOrds.InReg= '$clave'    then 'ENTRADA GOMEZ'             
            else ''
        END) as TipoMov,
        (CASE 
            when DOrds.InReg= '$clave'    then 'AMSA GOMEZ'
            else ''
        END) as Lugar

        FROM amsadb1.Truks
        LEFT JOIN amsadb1.DOrds 
        ON Truks.DO = DOrds.DOrd
        LEFT JOIN amsadb1.Clients 
        ON DOrds.InPlc = Clients.CliID
        LEFT JOIN amsadb1.Transports
        ON Truks.TNam = Transports.TptID 

        left JOIN amsadb1.Lots 
        ON Lots.TrkID = Truks.TrkID

        LEFT JOIN amsadb1.Gines 
        ON Lots.GinID = Gines.IDGin

        LEFT JOIN amsadb1.Region 
        ON Lots.Reg = Region.IDReg

        LEFT JOIN amsadb1.Maniobras 
        ON Truks.TipoManArr = Maniobras.TipoMan


        where (DOrds.InReg= '$clave'  or ( DOrds.InReg = 99001  AND Clients.State ='$region' and Clients.Directo = 1 )) and DOrds.OutPlc != '$clave' AND Truks.Status !='Cancelled' AND  Truks.FacManArr !='' AND Truks.PQArr = ''
        order By Truks.TrkId Desc;"
;

//and (Truks.InDat between '$fromdate' and '$todate')
$resultado = $conexion->prepare($query);
$resultado->execute();        
$trkanterior=0;
$myarray = [];
$fila = 2;
while($row = $resultado->fetch(PDO::FETCH_ASSOC)){

    //formato de fecha sin hora 

    $fechaMan = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel( $row['fecha'] );
    //strtotime($row['fecha']);
    //$fechaMan =25569 + ($fechaMan / 86400);


    if($trkanterior == $row['TrkID']){

        array_push($myarray,($fila-1));
        array_push($myarray,$fila);
    }

    //FORMATO DE FECHA EN LA COLUMNA
    $hojaActiva->getStyle('G' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);


    $hojaActiva->setCellValue('A' . $fila,$row['TrkID']);
    $hojaActiva->setCellValue('B' . $fila,$row['TipoMov']);
    $hojaActiva->setCellValue('C' . $fila,$row['Lugar']);
    $hojaActiva->setCellValue('D' . $fila, $row['DO']);
    $hojaActiva->setCellValue('E' . $fila,$row['Lot']);
    $hojaActiva->setCellValue('F' . $fila,$row['Qty']);   
    $hojaActiva->setCellValue('G'. $fila,$fechaMan);
    $hojaActiva->setCellValue('H'. $fila,$row['TipoMan']);
    $hojaActiva->setCellValue('I'. $fila,$row['costoxlote']);
    $hojaActiva->setCellValue('J'. $fila,$row['Factura']);
    /*$hojaActiva->setCellValue('M'. $fila,$row['BnName']);
    $hojaActiva->setCellValue('N'. $fila,$row['Cli']);*/

    $trkanterior = $row['TrkID'];
    $fila++;
   



}



//SALIDAS GOMEZ Y LAGUNA 
$query2="SELECT Truks.TrkID,Transports.BnName,Truks.WBill,Lots.Lot,Lots.Qty,Gines.GinName,Region.RegNam,Truks.Status,
        Truks.OutDat as fecha,Truks.TipoManDep as TipoMan,Truks.RespManDep as RespMan,Truks.NotaSalDep as NotaSal,Maniobras.CostoMan as costoxpaca,Truks.ComentarioManDep as ComentarioMan,
        (Maniobras.CostoMan * Lots.Qty)as costoxlote,Truks.FacManDep as Factura,
        concat(concat((IF((Select IsOrigin From Region Where DOrds.OutPlc = IDReg)=1,(Select Cde from Gines where Truks.TrkID = Lots.TrkID and Lots.GinID = Gines.IDGin),(Select Cde from Region where Truks.TrkID = Lots.TrkID and DOrds.OutPlc = Region.IDReg))),'-',
        (IF((Select DOrds.Typ From DOrds Where Truks.DO = DOrds.DOrd)='CON',(Select Cde From Region Where DOrds.InReg = Region.IDReg),(Select Cde From Clients Where DOrds.InPlc = Clients.CliID)))),'/',Truks.DO) as DO,



        (CASE 
            when DOrds.OutPlc= '$clave'    then 'SALIDA GOMEZ'
            when DOrds.OutPlc= '$clave2'    then 'SALIDA LAGUNA'
            else ''
        END) as tipomov ,

        IF(DOrds.InReg = 99001,(select Clients.Cli where DOrds.InPlc = Clients.CliID),(select R1.RegNam from Region as R1  where DOrds.InReg = R1.IDReg)) as Lugar

        FROM amsadb1.Truks
        LEFT JOIN amsadb1.DOrds 
        ON Truks.DO = DOrds.DOrd
        LEFT JOIN amsadb1.Clients 
        ON DOrds.InPlc = Clients.CliID
        LEFT JOIN amsadb1.Transports
        ON Truks.TNam = Transports.TptID 

        left JOIN amsadb1.Lots 
        ON Lots.TrkID = Truks.TrkID

        LEFT JOIN amsadb1.Gines 
        ON Lots.GinID = Gines.IDGin

        LEFT JOIN amsadb1.Region 
        ON Lots.Reg = Region.IDReg

        LEFT JOIN amsadb1.Maniobras 
        ON Truks.TipoManDep = Maniobras.TipoMan

        where (DOrds.OutPlc= '$clave' or DOrds.OutPlc=$clave2) AND Truks.Status !='Cancelled' AND  Truks.FacManDep !='' AND Truks.PQDep = ''
        order By Truks.TrkId Desc;"
;
//and (Truks.OutDat between '$fromdate' and '$todate')

$resultado2 = $conexion->prepare($query2);
$resultado2->execute();     


while($row = $resultado2->fetch(PDO::FETCH_ASSOC)){

    //formato de fecha sin hora 

    $fechaMan = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel( $row['fecha'] );
    //strtotime($row['fecha']);
    //$fechaMan =25569 + ($fechaMan / 86400);


    if($trkanterior == $row['TrkID']){

        array_push($myarray,($fila-1));
        array_push($myarray,$fila);
    }

    //FORMATO DE FECHA EN LA COLUMNA
    $hojaActiva->getStyle('G' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);


    $hojaActiva->setCellValue('A' . $fila,$row['TrkID']);
    $hojaActiva->setCellValue('B' . $fila,$row['tipomov']);
    $hojaActiva->setCellValue('C' . $fila,$row['Lugar']);
    $hojaActiva->setCellValue('D' . $fila, $row['DO']);
    $hojaActiva->setCellValue('E' . $fila,$row['Lot']);
    $hojaActiva->setCellValue('F' . $fila,$row['Qty']);
    $hojaActiva->setCellValue('G'. $fila,$fechaMan);
    $hojaActiva->setCellValue('H'. $fila,$row['TipoMan']);
    $hojaActiva->setCellValue('I'. $fila,$row['costoxlote']);
    $hojaActiva->setCellValue('J'. $fila,$row['Factura']);
    $trkanterior = $row['TrkID'];
    $fila++;
   



}
//$hojaActiva->getStyle('I2:I'.$fila)->getNumberFormat()->setFormatCode('###0.00');

$longitud = count($myarray);
for($i=0; $i<$longitud; $i++){

    $hojaActiva->getStyle('A' . $myarray[$i])->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFFFFF00');

}

 //FORMATO DOS DIGITOS DESPUES DEL PUNTO
 $hojaActiva->getStyle('I2:I'.$fila)->getNumberFormat()->setFormatCode('###0.00');
 //$hojaActiva->getStyle('M2:M'.$fila)->getNumberFormat()->setFormatCode('###0.00');

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');
ob_end_clean();
$writer = IOFactory::createWriter($excel, 'Xlsx');
$writer->save('php://output');
exit;


?>