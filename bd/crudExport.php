<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

$Port = (isset($_POST['Port'])) ? $_POST['Port'] : '';
$ShpCo = (isset($_POST['ShpCo'])) ? $_POST['ShpCo'] : '';
$Bkg = (isset($_POST['Bkg'])) ? $_POST['Bkg'] : '';
$Ctr = (isset($_POST['Ctr'])) ? $_POST['Ctr'] : '';
$Seal = (isset($_POST['Seal'])) ? $_POST['Seal'] : '';
$DO = (isset($_POST['DO'])) ? $_POST['DO'] : '';
$Lots = (isset($_POST['Lots'])) ? $_POST['Lots'] : '';
$SchDate = (isset($_POST['SchDate'])) ? $_POST['SchDate'] : '';
$InDate = (isset($_POST['InDate'])) ? $_POST['InDate'] : '';
$InTime = (isset($_POST['InTime'])) ? $_POST['InTime'] : '';
$Status = (isset($_POST['Status'])) ? $_POST['Status'] : '';
$InWgh = (isset($_POST['InWgh'])) ? $_POST['InWgh'] : '';
$InvNo = (isset($_POST['InvNo'])) ? $_POST['InvNo'] : '';
$Cmt = (isset($_POST['Cmt'])) ? $_POST['Cmt'] : '';

$ExpID = (isset($_POST['ExpID'])) ? $_POST['ExpID'] : '';
$LotID = (isset($_POST['LotID'])) ? $_POST['LotID'] : '';
$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$TrkID = (isset($_POST['TrkID'])) ? $_POST['TrkID'] : '';

$destino = (isset($_POST['destino'])) ? $_POST['destino'] : '';
$Booking = (isset($_POST['Booking'])) ? $_POST['Booking'] : '';
$Francbal= (isset($_POST['Francbal'])) ? $_POST['Francbal'] : '';
$TareBale= (isset($_POST['TareBale'])) ? $_POST['TareBale'] : '';

switch($opcion){
    case 1:
        $consulta = "SELECT Lots.TrkID
        FROM Lots
        WHERE Lots.DOrd='$DO' and Lots.LotID='$LotID';";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataL=$resultado->fetch();
        $TrkID = $dataL['TrkID'];
        
        $consulta = "INSERT INTO Export (Port, ShpCo, Bkg, Ctr, Seal, DO, Lot, TrkID, SchDate, InDate, InTime, Status,Cmt) VALUES('$Port', '$ShpCo', '$Bkg', '$Ctr', '$Seal', '$DO', '$Lots', '$TrkID', '$SchDate', '$InDate', '$InTime', '$Status', '$Cmt');";			
        $resultado = $conexion->prepare($consulta);
        $resultado->execute(); 
        
        /*$consulta = "SELECT InvID, DO, Contract, Lot, Qty, InvDat, InvWgh, InvWghNet, InvAmt, Price, PosDat, InvSta, InvCmt FROM Invs ORDER BY InvID DESC LIMIT 1";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);*/       
        break;    
    case 2:       
        $TrkID=0; 
        if($Lots!=0){
            $consulta = "SELECT Lots.TrkID
            FROM Lots
            WHERE Lots.DOrd='$DO' and Lots.LotID='$LotID';";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();        
            $dataL=$resultado->fetch();
            $TrkID = $dataL['TrkID'];

        }


        $consulta = "UPDATE Export SET Port='$Port', ShpCo='$ShpCo', Bkg='$Bkg', Ctr='$Ctr', Seal='$Seal', DO='$DO', Lot='$Lots',TrkID='$TrkID', SchDate='$SchDate', InDate='$InDate', InTime='$InTime', Status='$Status', InWgh='$InWgh', InvNo='$InvNo', Cmt='$Cmt' WHERE ExpID='$ExpID'";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        
        /*$consulta = "SELECT InvID, DO, Contract, Lot, Qty, InvDat, InvWgh, InvWghNet, InvAmt, Price, PosDat, InvSta, InvCmt FROM Invs WHERE InvID='$InvID' ";       
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);*/
        break;
    case 3:        
        /*$consulta = "UPDATE Invs Set InvSta='Cancelled' WHERE InvID='$InvID'"; //"DELETE FROM Lots WHERE LotID='$LotID' ";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();*/                           
        break;
    case 4:    
        $consulta = "SELECT ExpID,  Bkg, Ctr, Seal, DO,  Lots.Lot, Export.TrkID, Export.SchDate, InDate, InTime, Export.Status, Export.Cmt,
        (SELECT Qty FROM Lots WHERE Export.DO = Lots.DOrd and Export.TrkID = Lots.TrkID AND Export.Lot = Lots.LotID) as Qty,
        (SELECT BnName FROM Truks, Transports WHERE Transports.TptID = Truks.TNam and Export.TrkID = Truks.TrkID ) as TName,
        (SELECT WBill FROM Truks WHERE Export.TrkID = Truks.TrkID and Export.DO = Truks.DO) as WBill,
        (SELECT TrkLPlt FROM Truks WHERE Export.TrkID = Truks.TrkID and Export.DO = Truks.DO) as TrkPlt,
        (SELECT TraLPlt FROM Truks WHERE Export.TrkID = Truks.TrkID and Export.DO = Truks.DO) as TraPlt,
        (SELECT DrvNam FROM Truks WHERE Export.TrkID = Truks.TrkID and Export.DO = Truks.DO) as DrvName,
        (SELECT OutDat FROM Truks WHERE Export.TrkID = Truks.TrkID and Export.DO = Truks.DO) as DepDate,
        (SELECT OutTime FROM Truks WHERE Export.TrkID = Truks.TrkID and Export.DO = Truks.DO) as OutTime,
        (SELECT Invoice FROM Truks WHERE Truks.TrkID  = Export.TrkID ) as InvNo,
        (SELECT Port FROM Ports WHERE Export.Port = Ports.IDPort) as Port,
	    (SELECT LiqWgh FROM Lots WHERE Export.DO = Lots.DOrd and Export.Lot =  Lots.LotID and Export.TrkID = Lots.TrkID) as LiqWgh,
        (SELECT (OutWgh/Truks.CrgQty)*Qty FROM Truks WHERE Export.TrkID = Truks.TrkID ) as DepWgh,
        (SELECT (InWgh/Truks.CrgQty)*Qty FROM Truks WHERE Export.TrkID = Truks.TrkID ) as InWgh,    
        (SELECT Naviera FROM Navieras WHERE Export.ShpCo = Navieras.IDNaviera ) as ShpCo,   
        (SELECT Status FROM Truks WHERE Export.TrkID = Truks.TrkID) as Statustrk,
        (SELECT CAAT FROM Truks, Transports WHERE Export.TrkID = Truks.TrkID and Transports.TptID = Truks.TNam) as CAAT,
        (SELECT Ctc FROM DOrds WHERE Export.DO = DOrds.DOrd) as DOCtc,
        (SELECT RegNam FROM DOrds, Region WHERE Export.DO = DOrds.DOrd and DOrds.OutPlc = IDReg) as DepReg,
        (SELECT BnName FROM DOrds, Clients WHERE Export.DO = DOrds.DOrd and DOrds.InPlc = CliID) as Ctl,
        ifnull(GinName, (SELECT GinName FROM DOrds, Gines WHERE Export.DO = DOrds.DOrd and DOrds.Gin = Gines.IDGin)) as GinName
        FROM Export 
        left join Lots on Export.DO = Lots.DOrd and Export.Lot = Lots.LotID and Export.TrkID=Lots.TrkID
        left join Gines on Lots.GinID = Gines.IDGin;";
        //FROM Export;"; *(SELECT TNam FROM Truks WHERE Export.DO = Truks.DO and Export.Lot = Truks.LotsAssc) as TName,
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 5:    
        $consulta = "SELECT distinct DOrds.OutPlc, DOrds.Ctc, DOrds.Gin, DOrds.InPlc,
        (SELECT RegNam FROM Region WHERE IDReg = DOrds.OutPlc) as OutReg,
        (SELECT GinName FROM Gines WHERE IDGin = DOrds.Gin) as Gin,
        (SELECT RegNam FROM Region WHERE IDReg = DOrds.InPlc) as Cli,
        Lots.LotID, Lots.Lot,Lots.Qty
        FROM DOrds 
        left join Lots on Lots.Dord = '$DO'
        left join  Export on Lots.LotID = Export.Lot 
        WHERE DOrds.DOrd='$DO' and Lots.Trkid != 0  and Export.Lot IS NULL ORDER BY Lots.Lot ASC";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 6:    
        $consulta = "SELECT Lots.TrkID
        FROM Lots
        WHERE Lots.DOrd='$DO' and Lots.LotID='$LotID';";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $dataL=$resultado->fetch();
        $TrkID = $dataL['TrkID'];
        
        $consulta = "SELECT Truks.OutDat,Truks.Status,TRUNCATE((InWgh/Truks.CrgQty)*Lots.Qty,2) as InWgh,Invoice,Truks.OutTime, WBill, TrkLPlt, TraLPlt, DrvNam, TRUNCATE((OutWgh/Truks.CrgQty)*Lots.Qty,2) as OutWgh,Truks.TrkID,
        (SELECT TptCo FROM Transports WHERE TptID = TNam) as TNam,
        (SELECT CAAT FROM Transports WHERE TptID = TNam) as CAAT,
        Lots.Lot, Lots.Qty, Lots.LiqWgh
        FROM Truks
        left join Lots on Lots.TrkID = '$TrkID'  AND Lots.DOrd='$DO' AND Lots.LotID = '$LotID'
        WHERE Truks.TrkID = '$TrkID' AND Truks.DO = '$DO'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;

    case 7:

        $consulta = "SELECT IDPort,Port,City FROM Ports where City=(Select City From  Ports where Port='$Port') ORDER BY Port ASC ";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);


         break;

    case 8:

            $consulta = "SELECT ExpID,Ctr FROM Export WHERE DO = '$DO' AND TrkID = 0 AND Lot = 0 AND Status !='Disabled' ORDER BY Port ASC ";
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();        
            $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    
    break;


    case 9:

        $consulta = "UPDATE Export SET Lot = (Select Lots.LotID from Lots where  Lots.Lot='$Lots' AND Lots.TrkID = '$TrkID' AND Lots.DOrd='$DO'), TrkID = '$TrkID' WHERE ExpID = '$ExpID' ";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data="Ok";

    break;

    case 10:

        $consulta = "SELECT ExpID from Export where TrkID = '$TrkID'";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dat=$resultado->fetch(); 
        if(!empty($dat)){ 
        $data="1";
        }

        else{
        $data="0";
        }

    break;

    case 11:

        $consulta = "SELECT IDNaviera,Naviera from Navieras";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
       

    break;

    case 12:

        $consulta = "SELECT Ports.Port,Ports.Direction,Ports.PostalCode,Ports.IDPort,Navieras.".$destino." as selectdef from Ports,Navieras where Navieras.IDNaviera='$ShpCo' AND Ports.City='$destino'";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
       

    break;

    case 13:

        $consulta = "SELECT Ports.Direction,Ports.PostalCode from Ports WHERE Ports.IDPort ='$Port' OR Ports.Port ='$Port'";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
       

    break;
    case 14:

        $consulta = "SELECT SUM(Lots.Qty) as CantidadBales, SUM(Truks.OutWgh) as RealGross,
        (SELECT IF(Truks.LotsAssc Like '%|%',1,0))	as Lotpartido,
        (SELECT COUNT(*) FROM amsadb1.Export,amsadb1.Lots,amsadb1.Truks where Truks.OutWgh = 0.00 and  Export.Bkg ='$Booking' AND Export.Lot = Lots.LotID AND Export.TrkID = Truks.TrkID  AND Export.DO=Lots.DOrd )	as pesoscap	
        FROM amsadb1.Export,amsadb1.Lots,amsadb1.Truks
        where Export.Bkg ='$Booking' AND Export.Lot = Lots.LotID AND Export.TrkID = Truks.TrkID  AND Export.DO=Lots.DOrd";
        
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetch(PDO::FETCH_ASSOC);    
        
        
    break;

    case 15:

        $consulta="SELECT Lots.Lot,Lots.Qty as cantidad,Export.TrkID,Truks.OutWgh/Lots.Qty as promedio,Truks.OutWgh
        FROM amsadb1.Export,amsadb1.Lots,amsadb1.DOrds,amsadb1.Truks
        where Export.Bkg ='$Bkg' AND Export.Lot=Lots.LotID AND Export.DO=DOrds.DOrd AND Export.TrkID=Truks.TrkID
        GROUP BY TrkID;";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
        
        
    break;
}

print json_encode($data, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX
$conexion=null;
