<?php

include_once ('conexion.php');
$objeto = new Conexion();
$conexion = $objeto->Conectar();

//$vendor_puebla="CATA550310CW8MXN";
//$vendor_gomez="FORR690607R4AMXN";

$fecha=date('Y-m-d');
// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill, Alignment};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//use PhpOffice\PhpSpreadsheet\Style\Alignment as alignment;
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
$heading = false;

$fileName = "Maniobras-".date('Y-m-d').".xlsx";

//SE CREA Y SE EMPIEZA A LLENAR LA HOJA
$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Maniobras");

//negritas en encabezado


// definir fechas feriadas

$fechas_festivas = array("2024-05-01");


$hojaActiva->getStyle('A1:M1')->getFont()->setBold( true );

//relleno de celda encabezado
$hojaActiva->getStyle('A1:M1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFE1E1E1');
//fijar primera fila
$hojaActiva->freezePane('A2');

$hojaActiva->getColumnDimension('A')->setWidth(12);
$hojaActiva->setCellValue('A1','TruckID');
$hojaActiva->getColumnDimension('B')->setWidth(12);
$hojaActiva->setCellValue('B1','Movimiento');
$hojaActiva->getColumnDimension('C')->setWidth(12);
$hojaActiva->setCellValue('C1','Destino');
$hojaActiva->getColumnDimension('D')->setWidth(12);
$hojaActiva->setCellValue('D1','Vendor');
$hojaActiva->getColumnDimension('E')->setWidth(34);
$hojaActiva->setCellValue('E1','DO');
$hojaActiva->getColumnDimension('F')->setWidth(12);
$hojaActiva->setCellValue('F1','Lot');
$hojaActiva->getColumnDimension('G')->setWidth(12);
$hojaActiva->setCellValue('G1','Bales');
$hojaActiva->getColumnDimension('H')->setWidth(12);
$hojaActiva->setCellValue('H1','Date');
$hojaActiva->getColumnDimension('I')->setWidth(12);
$hojaActiva->setCellValue('I1','Maniobra');
$hojaActiva->getColumnDimension('J')->setWidth(12);
$hojaActiva->setCellValue('J1','Costo x Paca');
$hojaActiva->getColumnDimension('K')->setWidth(12);
$hojaActiva->setCellValue('K1','Costo x Lote');
$hojaActiva->getColumnDimension('L')->setWidth(12);
$hojaActiva->setCellValue('L1','Factura');
$hojaActiva->getColumnDimension('M')->setWidth(12);
$hojaActiva->setCellValue('M1','PQ');



//centrar celdas unidas horizontalmente 
$hojaActiva->getStyle('A1:L1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

//clave puebla
$clave=660;
$region="PUEBLA";
//LLEGADAS Y DIRECTOS PUEBLA
$query="SELECT Truks.TrkID,Transports.BnName,Truks.WBill,Lots.Lot,Lots.Qty,Gines.GinName,Region.RegNam,Truks.Status,Maniobras.Vendor,
        Truks.InDat as fecha,Truks.TipoManArr as TipoMan,Truks.RespManArr as RespMan,Truks.NotaSalArr as NotaSal,Truks.ComentarioManArr as ComentarioMan,
        Truks.FacManArr AS Factura,
        concat(concat((IF((Select IsOrigin From Region Where DOrds.OutPlc = IDReg)=1,(Select Cde from Gines where Truks.TrkID = Lots.TrkID and Lots.GinID = Gines.IDGin),(Select Cde from Region where Truks.TrkID = Lots.TrkID and DOrds.OutPlc = Region.IDReg))),'-',
(IF((Select DOrds.Typ From DOrds Where Truks.DO = DOrds.DOrd)='CON',(Select Cde From Region Where DOrds.InReg = Region.IDReg),(Select Cde From Clients Where DOrds.InPlc = Clients.CliID)))),'/',Truks.DO) as DO,


        (CASE 
            when DOrds.InReg= '$clave'    then 'ENTRADA'             
            when  DOrds.InReg = 99001  and  DOrds.OutPlc != '$clave' AND Clients.Directo = 1 AND Clients.State ='$region'  then 'DIRECTO' 
            else ''
        END) as TipoMov,
        (CASE 
            when DOrds.InReg= '$clave'    then 'AMSA PUEBLA'             
            when DOrds.InReg = 99001  and  DOrds.OutPlc != '$clave' AND Clients.Directo = 1 AND Clients.State ='$region' then (select Clients.Cli where DOrds.InPlc = Clients.CliID )
            else ''
        END) as Lugar,

        (CASE 
            WHEN Maniobras.TipoMan = Truks.TipoManArr AND (Truks.InDat between Maniobras.FechaInicio and FechaFin) THEN (select  Maniobras.CostoMan WHERE Maniobras.TipoMan = Truks.TipoManArr)
            else ''

        END)as costoxpaca

        FROM amsadb1.Truks
        LEFT JOIN amsadb1.DOrds 
        ON Truks.DO = DOrds.DOrd
        LEFT JOIN amsadb1.Clients 
        ON DOrds.InPlc = Clients.CliID
        LEFT JOIN amsadb1.Transports
        ON Truks.TNam = Transports.TptID 

        left JOIN amsadb1.Lots 
        ON Lots.TrkID = Truks.TrkID

        LEFT JOIN amsadb1.Gines 
        ON Lots.GinID = Gines.IDGin

        LEFT JOIN amsadb1.Region 
        ON Lots.Reg = Region.IDReg

        LEFT JOIN amsadb1.Maniobras 
        ON Truks.TipoManArr = Maniobras.TipoMan AND Truks.InDat between Maniobras.FechaInicio and FechaFin

        where (DOrds.InReg= '$clave'  or ( DOrds.InReg = 99001  AND Clients.State ='$region' and Clients.Directo = 1 )) and DOrds.OutPlc != '$clave'   AND Truks.Status !='Cancelled' AND  Truks.FacManArr !='' AND Truks.PQArr = ''
        order By Truks.TrkId Desc;"
;
//and (Truks.InDat between '$fromdate' and '$todate')
$costoxpaca=0;
$resultado = $conexion->prepare($query);
$resultado->execute();        
$trkanterior=0;
$myarray = [];
$fila = 2;
while($row = $resultado->fetch(PDO::FETCH_ASSOC)){

    //formato de fecha sin hora 

    $fechaMan = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel( $row['fecha'] );
    //strtotime($row['fecha']);
    //$fechaMan =25569 + ($fechaMan / 86400);


    if($trkanterior == $row['TrkID']){

        array_push($myarray,($fila-1));
        array_push($myarray,$fila);
    }

    //definir el costo por lote 
    $costoxpaca=$row['costoxpaca'];
    if($costoxpaca!=""){
       $costoxlote = $costoxpaca * $row['Qty'];
    }
    else{
        $costoxlote = 0;
        $costoxpaca= 0; 
     }


    //FORMATO DE FECHA EN LA COLUMNA
    $hojaActiva->getStyle('H' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);


    $hojaActiva->setCellValue('A' . $fila,$row['TrkID']);
    $hojaActiva->setCellValue('B' . $fila,$row['TipoMov']);
    $hojaActiva->setCellValue('C' . $fila,$row['Lugar']);
    $hojaActiva->setCellValue('D' . $fila, $row['Vendor']);
    $hojaActiva->setCellValue('E' . $fila, $row['DO']);
   // $hojaActiva->setCellValue('F' . $fila,$row['Lot']);
    $hojaActiva->setCellValue('G' . $fila,$row['Qty']);   
    $hojaActiva->setCellValue('H'. $fila,$fechaMan);
    $hojaActiva->setCellValue('I'. $fila,$row['TipoMan']);
    $hojaActiva->setCellValue('J'. $fila,$costoxpaca);
    $hojaActiva->setCellValue('K'. $fila, $costoxlote);
    $hojaActiva->setCellValue('L'. $fila,$row['Factura']);
    $trkanterior = $row['TrkID'];
    
    
      //quitar funcion exponencial en excel para lotes americanos 23E02 
    
    $style = $hojaActiva->getStyle('F'. $fila);

    $style->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
    
    $hojaActiva->getCell('F'. $fila)->setValueExplicit($row['Lot'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
    
    
    $fila++;
}



//SALIDAS PUEBLA
$query2="SELECT Truks.TrkID,Transports.BnName,Truks.WBill,Lots.Lot,Lots.Qty,Gines.GinName,Region.RegNam,Truks.Status,Maniobras.Vendor,
        Truks.OutDat as fecha,Truks.TipoManDep as TipoMan,Truks.RespManDep as RespMan,Truks.NotaSalDep as NotaSal,Truks.ComentarioManDep as ComentarioMan,
       Truks.FacManDep AS Factura,
        concat(concat((IF((Select IsOrigin From Region Where DOrds.OutPlc = IDReg)=1,(Select Cde from Gines where Truks.TrkID = Lots.TrkID and Lots.GinID = Gines.IDGin),(Select Cde from Region where Truks.TrkID = Lots.TrkID and DOrds.OutPlc = Region.IDReg))),'-',
(IF((Select DOrds.Typ From DOrds Where Truks.DO = DOrds.DOrd)='CON',(Select Cde From Region Where DOrds.InReg = Region.IDReg),(Select Cde From Clients Where DOrds.InPlc = Clients.CliID)))),'/',Truks.DO) as DO,

        IF(DOrds.InReg = 99001,(select Clients.Cli where DOrds.InPlc = Clients.CliID),(select R1.RegNam from Region as R1  where DOrds.InReg = R1.IDReg)) as Lugar,
        (CASE 
            WHEN Maniobras.TipoMan = Truks.TipoManDep AND (Truks.OutDat between Maniobras.FechaInicio and FechaFin) THEN (select  Maniobras.CostoMan WHERE Maniobras.TipoMan = Truks.TipoManDep)
            else ''

        END)as costoxpaca

        FROM amsadb1.Truks
        LEFT JOIN amsadb1.DOrds 
        ON Truks.DO = DOrds.DOrd
        LEFT JOIN amsadb1.Clients 
        ON DOrds.InPlc = Clients.CliID
        LEFT JOIN amsadb1.Transports
        ON Truks.TNam = Transports.TptID 

        left JOIN amsadb1.Lots 
        ON Lots.TrkID = Truks.TrkID

        LEFT JOIN amsadb1.Gines 
        ON Lots.GinID = Gines.IDGin

        LEFT JOIN amsadb1.Region 
        ON Lots.Reg = Region.IDReg

        LEFT JOIN amsadb1.Maniobras 
        ON Truks.TipoManDep = Maniobras.TipoMan AND Truks.OutDat between Maniobras.FechaInicio and Maniobras.FechaFin

        where DOrds.OutPlc= '$clave'  AND Truks.Status !='Cancelled' AND  Truks.FacManDep !='' AND Truks.PQDep = ''
        order By Truks.TrkId Desc;"
;

$resultado2 = $conexion->prepare($query2);
$resultado2->execute();     


while($row = $resultado2->fetch(PDO::FETCH_ASSOC)){

    //formato de fecha sin hora 

    $fechaMan = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel( $row['fecha'] );
    //strtotime($row['fecha']);
    //$fechaMan =25569 + ($fechaMan / 86400);


    if($trkanterior == $row['TrkID']){

        array_push($myarray,($fila-1));
        array_push($myarray,$fila);
    }

    //definir el costo por lote 
    $costoxpaca=$row['costoxpaca'];
    if($costoxpaca!=""){
       $costoxlote = $costoxpaca * $row['Qty'];
    }
    else{
        $costoxlote = 0;
        $costoxpaca= 0; 
     }


    //FORMATO DE FECHA EN LA COLUMNA
    $hojaActiva->getStyle('H' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
  
    
    $hojaActiva->setCellValue('A' . $fila,$row['TrkID']);
    $hojaActiva->setCellValue('B' . $fila,'SALIDA');
    $hojaActiva->setCellValue('C' . $fila,$row['Lugar']);
    $hojaActiva->setCellValue('D' . $fila, $row['Vendor']);
    $hojaActiva->setCellValue('E' . $fila, $row['DO']);
   // $hojaActiva->setCellValue('F' . $fila,$row['Lot']);
    $hojaActiva->setCellValue('G' . $fila,$row['Qty']);   
    $hojaActiva->setCellValue('H'. $fila,$fechaMan);
    $hojaActiva->setCellValue('I'. $fila,$row['TipoMan']);
    $hojaActiva->setCellValue('J'. $fila, $costoxpaca);
    $hojaActiva->setCellValue('K'. $fila,$costoxlote);
    $hojaActiva->setCellValue('L'. $fila,$row['Factura']);
    $trkanterior = $row['TrkID'];
      //quitar funcion exponencial en excel para lotes americanos 23E02 
    
    $style = $hojaActiva->getStyle('F'. $fila);

    $style->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
    
    $hojaActiva->getCell('F'. $fila)->setValueExplicit($row['Lot'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
    
    $fila++;
}

//SE AGREGAN LAS MANIOBRAS DE GOMEZ/LAGUNA
//CLAVE GOMEZ
$clave=651;
//CLAVE LAGUNA
$clave2=650;


//LLEGADAS GOMEZ
$query="SELECT Truks.TrkID,Transports.BnName,Truks.WBill,Lots.Lot,Lots.Qty,Gines.GinName,Region.RegNam,Truks.Status,Maniobras.Vendor,
        Truks.InDat as fecha,Truks.TipoManArr as TipoMan,Truks.RespManArr as RespMan,Truks.NotaSalArr as NotaSal,Truks.ComentarioManArr as ComentarioMan,Truks.FacManArr AS Factura,
        concat(concat((IF((Select IsOrigin From Region Where DOrds.OutPlc = IDReg)=1,(Select Cde from Gines where Truks.TrkID = Lots.TrkID and Lots.GinID = Gines.IDGin),(Select Cde from Region where Truks.TrkID = Lots.TrkID and DOrds.OutPlc = Region.IDReg))),'-',
        (IF((Select DOrds.Typ From DOrds Where Truks.DO = DOrds.DOrd)='CON',(Select Cde From Region Where DOrds.InReg = Region.IDReg),(Select Cde From Clients Where DOrds.InPlc = Clients.CliID)))),'/',Truks.DO) as DO,



        (CASE 
            when DOrds.InReg= '$clave'    then 'ENTRADA GOMEZ'             
            else ''
        END) as TipoMov,
        (CASE 
            when DOrds.InReg= '$clave'    then 'AMSA GOMEZ'
            else ''
        END) as Lugar,
        
         (CASE 
            when DOrds.InReg= '$clave'    then  (if((DAYOFWEEK(Truks.InDat))=1,(Maniobras.CostoMan * Lots.Qty)*2,Maniobras.CostoMan * Lots.Qty)) 
          
            else '' 
        END) as costoxlote,
        

        (CASE 
            when DOrds.InReg= '$clave'    then (if((DAYOFWEEK(Truks.InDat))=1,(Maniobras.CostoMan * 2),Maniobras.CostoMan))
           
            else ''
        END) as costoxpaca

        FROM amsadb1.Truks
        LEFT JOIN amsadb1.DOrds 
        ON Truks.DO = DOrds.DOrd
        LEFT JOIN amsadb1.Clients 
        ON DOrds.InPlc = Clients.CliID
        LEFT JOIN amsadb1.Transports
        ON Truks.TNam = Transports.TptID 

        left JOIN amsadb1.Lots 
        ON Lots.TrkID = Truks.TrkID

        LEFT JOIN amsadb1.Gines 
        ON Lots.GinID = Gines.IDGin

        LEFT JOIN amsadb1.Region 
        ON Lots.Reg = Region.IDReg

        LEFT JOIN amsadb1.Maniobras 
        ON Truks.TipoManArr = Maniobras.TipoMan


        where DOrds.InReg= '$clave'  and DOrds.OutPlc != '$clave' AND Truks.Status !='Cancelled' AND  Truks.FacManArr !='' AND Truks.PQArr = 0
        order By Truks.TrkId Desc;"
;


$resultado = $conexion->prepare($query);
$resultado->execute();    

while($row = $resultado->fetch(PDO::FETCH_ASSOC)){
    $fechaMan = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel( $row['fecha'] );
    if($trkanterior == $row['TrkID']){

        array_push($myarray,($fila-1));
        array_push($myarray,$fila);
    }
    
    
        //determinar el pago de maniobras en dia festivo 
    if (in_array($row['fecha'], $fechas_festivas)) {
        $costo_paca=$row['costoxpaca'] * 2;
        $costo_lote=$row['costoxlote'] * 2;
        
    } else {
        $costo_paca=$row['costoxpaca'] ;
        $costo_lote=$row['costoxlote'] ;
    }
    

    //FORMATO DE FECHA EN LA COLUMNA
    $hojaActiva->getStyle('H' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);


    $hojaActiva->setCellValue('A' . $fila,$row['TrkID']);
    $hojaActiva->setCellValue('B' . $fila,$row['TipoMov']);
    $hojaActiva->setCellValue('C' . $fila,$row['Lugar']);
    $hojaActiva->setCellValue('D' . $fila, $row['Vendor']);
    $hojaActiva->setCellValue('E' . $fila, $row['DO']);
    //$hojaActiva->setCellValue('F' . $fila,$row['Lot']);
    $hojaActiva->setCellValue('G' . $fila,$row['Qty']);   
    $hojaActiva->setCellValue('H'. $fila,$fechaMan);
    $hojaActiva->setCellValue('I'. $fila,$row['TipoMan']);
    $hojaActiva->setCellValue('J'. $fila,$costo_paca);
    $hojaActiva->setCellValue('K'. $fila,$costo_lote);
    $hojaActiva->setCellValue('L'. $fila,$row['Factura']);


    $style = $hojaActiva->getStyle('F'. $fila);

    $style->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
    
    $hojaActiva->getCell('F'. $fila)->setValueExplicit($row['Lot'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
    
    $fila++;

    $trkanterior = $row['TrkID'];
    $fila++;
}


//SALIDAS GOMEZ Y LAGUNA 
$query2="SELECT Truks.TrkID,Transports.BnName,Truks.WBill,Lots.Lot,Lots.Qty,Gines.GinName,Region.RegNam,Truks.Status,Maniobras.Vendor,
        Truks.OutDat as fecha,Truks.TipoManDep as TipoMan,Truks.RespManDep as RespMan,Truks.NotaSalDep as NotaSal,Truks.ComentarioManDep as ComentarioMan,
        Truks.FacManDep as Factura,Truks.PQDep as PQ,
         concat(concat((IF((Select IsOrigin From Region Where DOrds.OutPlc = IDReg)=1,(Select Cde from Gines where Truks.TrkID = Lots.TrkID and Lots.GinID = Gines.IDGin),(Select Cde from Region where Truks.TrkID = Lots.TrkID and DOrds.OutPlc = Region.IDReg))),'-',
        (IF((Select DOrds.Typ From DOrds Where Truks.DO = DOrds.DOrd)='CON',(Select Cde From Region Where DOrds.InReg = Region.IDReg),(Select Cde From Clients Where DOrds.InPlc = Clients.CliID)))),'/',Truks.DO) as DO,


        (CASE 
            when DOrds.OutPlc= '$clave'    then 'SALIDA GOMEZ'
            when DOrds.OutPlc= '$clave2'    then 'SALIDA LAGUNA'
            else ''
        END) as tipomov,

        (CASE 
            when DOrds.OutPlc= '$clave'    then  (if((DAYOFWEEK(Truks.OutDat))=1,(Maniobras.CostoMan * Lots.Qty)*2,Maniobras.CostoMan * Lots.Qty)) 
            when DOrds.OutPlc= '$clave2'    then (Maniobras.CostoMan * Lots.Qty) 
            else '' 
        END) as costoxlote,
        

        (CASE 
            when DOrds.OutPlc= '$clave'    then (if((DAYOFWEEK(Truks.OutDat))=1,(Maniobras.CostoMan * 2),Maniobras.CostoMan))
            when DOrds.OutPlc= '$clave2'    then (Maniobras.CostoMan) 
            else ''
        END) as costoxpaca,

        

        IF(DOrds.InReg = 99001,(select Clients.Cli where DOrds.InPlc = Clients.CliID),(select R1.RegNam from Region as R1  where DOrds.InReg = R1.IDReg)) as Lugar

        FROM amsadb1.Truks
        LEFT JOIN amsadb1.DOrds 
        ON Truks.DO = DOrds.DOrd
        LEFT JOIN amsadb1.Clients 
        ON DOrds.InPlc = Clients.CliID
        LEFT JOIN amsadb1.Transports
        ON Truks.TNam = Transports.TptID 

        left JOIN amsadb1.Lots 
        ON Lots.TrkID = Truks.TrkID

        LEFT JOIN amsadb1.Gines 
        ON Lots.GinID = Gines.IDGin

        LEFT JOIN amsadb1.Region 
        ON Lots.Reg = Region.IDReg

        LEFT JOIN amsadb1.Maniobras 
        ON Truks.TipoManDep = Maniobras.TipoMan

        where (DOrds.OutPlc= '$clave' or DOrds.OutPlc=$clave2)  AND  Truks.FacManDep !='' AND Truks.PQDep = 0 AND Truks.Status !='Cancelled'
        order By Truks.TrkId Desc;"
;

//print_r($query2);

$resultado2 = $conexion->prepare($query2);
$resultado2->execute();     


while($row = $resultado2->fetch(PDO::FETCH_ASSOC)){


    //determinar el pago de maniobras en dia festivo 
    if (in_array($row['fecha'], $fechas_festivas)) {
        $costo_paca=$row['costoxpaca'] * 2;
        $costo_lote=$row['costoxlote'] * 2;
        
    } else {
        $costo_paca=$row['costoxpaca'] ;
        $costo_lote=$row['costoxlote'] ;
    }
    

    //formato de fecha sin hora 

    $fechaMan = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel( $row['fecha'] );
    //strtotime($row['fecha']);
    //$fechaMan =25569 + ($fechaMan / 86400);


    if($trkanterior == $row['TrkID']){

        array_push($myarray,($fila-1));
        array_push($myarray,$fila);
    }

    //FORMATO DE FECHA EN LA COLUMNA
    $hojaActiva->getStyle('H' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);


    $hojaActiva->setCellValue('A' . $fila,$row['TrkID']);
    $hojaActiva->setCellValue('B' . $fila,$row['tipomov']);
    $hojaActiva->setCellValue('C' . $fila,$row['Lugar']);
    $hojaActiva->setCellValue('D' . $fila, $row['Vendor']);
    $hojaActiva->setCellValue('E' . $fila, $row['DO']);
    //$hojaActiva->setCellValue('F' . $fila,$row['Lot']);
    $hojaActiva->setCellValue('G' . $fila,$row['Qty']);
    $hojaActiva->setCellValue('H'. $fila,$fechaMan);
    $hojaActiva->setCellValue('I'. $fila,$row['TipoMan']);
    $hojaActiva->setCellValue('J'. $fila,$costo_paca);
    $hojaActiva->setCellValue('K'. $fila,$costo_lote);
    $hojaActiva->setCellValue('L'. $fila,$row['Factura']);

    $style = $hojaActiva->getStyle('F'. $fila);

    $style->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
    
    $hojaActiva->getCell('F'. $fila)->setValueExplicit($row['Lot'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
    
    $fila++;
    $trkanterior = $row['TrkID'];
    $fila++;
   
}

//$hojaActiva->getStyle('I2:I'.$fila)->getNumberFormat()->setFormatCode('###0.00');

$longitud = count($myarray);
for($i=0; $i<$longitud; $i++){

    $hojaActiva->getStyle('A' . $myarray[$i])->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFFFFF00');

}

  //FORMATO DOS DIGITOS DESPUES DEL PUNTO
  $hojaActiva->getStyle('J2:J'.$fila)->getNumberFormat()->setFormatCode('###0.00');
  $hojaActiva->getStyle('K2:K'.$fila)->getNumberFormat()->setFormatCode('###0.00');


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');
ob_end_clean();
$writer = IOFactory::createWriter($excel, 'Xlsx');
$writer->save('php://output');
exit;


?>