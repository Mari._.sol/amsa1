<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="#" />  
    <title>Users</title>
      
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <!-- CSS personalizado --> 
    <link rel="stylesheet" href="main.css">  
      
      
    <!--datables CSS básico-->
    <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css"/>
    <!--datables estilo bootstrap 4 CSS-->  
    <link rel="stylesheet"  type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">  
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  </head>
    
  <body>

     <div class="container">
      <div class="mx-auto col-lg-12 main-section" id="myTab" role="tablist">
        <div class="w3-bar w3-light-gray">
         <a class="w3-bar-item">Usuarios Registrados</a>
         <!--<a href="#" class="w3-bar-item w3-button">Home</a>
         <a href="trucks.php" class="w3-bar-item w3-button">Trucks</a>
         <a href="lots.php" class="w3-bar-item w3-button">Lots</a>
         <a href="do.php" class="w3-bar-item w3-button">Delivery Order</a>
         <a href="#" class="w3-bar-item w3-button">Invoice</a>-->
         <div class="w3-dropdown-hover w3-right">
          <button class="w3-button">NameSession</button>
          <div class="w3-dropdown-content w3-bar-block w3-card-4">
           <a href="#" class="w3-bar-item w3-button w3-right">Logout</a>
          </div>
         </div> 
        </div> 
      </div>
     </div> 
 
     <header>
     <h3 class='text-center'></h3>
     </header>    
    
  
    <div class="container">
        <div class="row">
            <div class="col-lg-12">            
            <button id="btnNuevo" type="button" class="btn btn-info" data-toggle="modal"><i class="material-icons">library_add</i></button>    
            </div>    
        </div>    
    </div>    
    <br>  

    <div class="container caja">
        <div class="row">
            <div class="col-lg-12">
            <div class="table-responsive">        
                <table id="tablaUsers" class="table table-striped table-bordered table-condensed" style="width:100%" >
                    <thead class="text-center thead-light">
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>UserID</th>                                
                            <th>Password</th>  
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>                           
                    </tbody>        
                </table>               
            </div>
            </div>
        </div>  
    </div>   

<!--Modal para CRUD-->
<div class="modal fade" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
        <form id="formUsers">    
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Name:</label>
                    <input type="text" class="form-control" id="Name">
                    </div>
                    </div>
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Email</label>
                    <input type="text" class="form-control" id="Email">
                    </div> 
                    </div>    
                </div>
                <div class="row"> 
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">UserID</label>
                    <input type="text" class="form-control" id="UserID">
                    </div>               
                    </div>
                    <div class="col-lg-6">
                    <div class="form-group">
                    <label for="" class="col-form-label">Password</label>
                    <input type="text" class="form-control" id="Password">
                    </div>
                    </div>
                </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                <button type="submit" id="btnGuardar" class="btn btn-dark">Save</button>
            </div>
        </form>    
        </div>
    </div>
</div>  
      
    <!-- jQuery, Popper.js, Bootstrap JS -->
    <script src="assets/jquery/jquery-3.3.1.min.js"></script>
    <script src="assets/popper/popper.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
      
    <!-- datatables JS -->
    <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>    
     
    <script type="text/javascript" src="main.js"></script>  
    
    
  </body>
</html>
