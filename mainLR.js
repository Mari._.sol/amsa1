$(document).ready(function() {

//funcion para sumar
jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
    return this.flatten().reduce( function ( a, b ) {
      if ( typeof a === 'string' ) {
        a = a.replace(/[^\d.-]/g, '') * 1;
      }
      if ( typeof b === 'string' ) {
        b = b.replace(/[^\d.-]/g, '') * 1;
      }
      return a + b;
    }, 0);
});
    
$('#tablaUsuarios tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
    
var LotID, opcion;
opcion = 4;
    
tablaUsuarios = $('#tablaUsuarios').DataTable({

        drawCallback: function () {
        var api = this.api();

        var total = api.column( 3, {"filter":"applied"}).data().sum();
        var numlotes = api.rows({"filter":"applied"}).count();
        var pesototal = api.column( 11, {"filter":"applied"}).data().sum();
        $('#monto').html(total);
        $('#totallotes').html(numlotes);
       // $("#totallotes").val(numlotes);
        $('#totalpeso').html(pesototal);

      },
    
    //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Export to Excel',
				className: 'btn btn-success',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			{
				extend:    'pdfHtml5',
				text:      '<i class="fas fa-file-pdf"></i> ',
				titleAttr: 'Export to PDF',
                className: 'btn btn-danger',
                /*customize: function (doc) {
                    doc.content[1].table.widths = 
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                },*/
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
      {
                text: '<i class="bi bi-funnel-fill"></i>',
                titleAttr: 'Filtrar por',
                orientation: 'landscape',
                className: 'btn btn-info btn-filtrar',
                attr:{
                        id:'filtraboton',
                        "data-toggle": 'modal tooltip',
                        "disabled": false
                }
            },
			/*{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},*/
		],
    
    initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
    },	        
    "order": [ 0, 'desc' ],
    "scrollX": true,
    "scrollY": "50vh",
    "scrollCollapse": true,
    "lengthMenu": [100,200,300,500],
    "ajax":{            
        "url": "bd/crud.php", 
        "method": 'POST', //usamos el metodo POST
        "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
        "dataSrc":""
    },
    "columns":[
        {"data": "LotID"},
        {"data": "Crop"},
        {"data": "Lot"},
        {"data": "Qty"},
        {"data": "RegName"},
        {"data": "GinName"},
        {"data": "LocName"},
        {"data": "DOrd"},
        {"data": "SchDate"},
        {"data": "InReg"},
        {"data": "CliDO"},
        {"data": "LiqWgh"},
        {"data": "Qlty"},
        {"data": "Cmt"},
        {"data": "Recap"},
        {"data": "CliID"},
        {"data": "AsgmDate"},
	    {"data": "TrkID"},
        {"data": "Status"},
        {"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-info btn-sm btnTrk'><i class='material-icons'>local_shipping</i></button></div></div>"}
    ]

});
    
    
$('.dataTables_length').addClass('bs-select');

    $(document).on("click", ".btnTrk", function(){
    $("#Inf").html("");
    opcion = 5;
    var opts = "";
    fila = $(this).closest("tr");	        
    TrkID = parseInt(fila.find('td:eq(17)').text()); //capturo el ID del camion asociado
    $.ajax({
          url: "bd/crud.php",
          type: "POST",
          datatype:"json",    
          data:  {TrkID:TrkID, opcion:opcion},    
          success: function(data) {
            opts = JSON.parse(data);
            for (var i = 0; i< opts.length; i++){
                htmlData = '<ul><li>Transport Name: '+opts[i].TNam+'</li><li>Driver Name: '+opts[i].DrvNam+'</li><li>Driver Telephone: '+opts[i].DrvTel+'</li><li>Departure Date: '+opts[i].OutDat+'</li><li>Arrival Date: '+opts[i].InDat+'</li></ul>';
                $('#Inf').html(htmlData);
            } 
          }
    });
    $(".modal-header").css("background-color", "#17a2b8");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Trucks");		
    $('#modalInf').modal('show');
 });
 
 //---------------- Inicia funciones botón filtros .-------------------------------------------------------------------------------------------------------

    
$("#filtraboton").click(function(){
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    DOL = $('#DOL').val();
    RegL = $('#RegL').val();
    GinL = $('#GinL').val();
    LotL = $('#LotL').val();
    LocL = $('#LocL').val();
    CrpL = $('#CrpL').val();
    CliL = $('#CliL').val();
    //$('#GinL').attr('disabled', true);

    $('#filtrarmodal').modal('show');
    $('#buscafiltro').prop('disabled', true);
});

var botonglobal = 0;
$("#buscafiltro, #borrarFiltro, #cer").click(function(){
    boton = $(this).val();
    if(boton == 1 || boton == 0)
        botonglobal = boton;
    if(boton != 3){
        idGlobal = 0;
        DOL = $('#DOL').val();
        RegL = $('#RegL').val();
        GinL = $('#GinL').val();
        LotL = $('#LotL').val();
        LocL = $('#LocL').val();
        CrpL = $('#CrpL').val();
        CliL = $('#CliL').val();
        //filters = {"muestras":muestras,"regionfil":regionfil};
        opcion = 4;
        var applyFilter =  $.ajax({
            type: 'POST',
            url: 'bd/assignFilters.php',
            data: {
                boton: boton,
                idGlobal:idGlobal,
                DOL: DOL,
                RegL: RegL,
                GinL: GinL,
                LotL: LotL,
                LocL: LocL,
                CrpL: CrpL,
                CliL: CliL
            }
        }) 
        applyFilter.done(function(data){
            console.log(data);
            tablaUsuarios.ajax.reload(function(){
                if(boton == 1){
                    $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Filtros asignados con éxito</div>')
                    setTimeout(function() {
                        $('#alert1').fadeOut(1000);
                        $('#filtrarmodal').modal('hide');
                    },1000);
                }
                else{
                    choicesRegL.enable()
                    $('#GinL').attr('disabled', true);
                    choicesLocL.enable();
                    $('#DOL').attr('disabled', false);
                    $('#LotL').attr('disabled', false);
                    $('#CliL').attr('disabled', false);
                    $('#CrpL').attr('disabled', false);
                    
                    choicesRegL.removeActiveItems();
                    $('#GinL').prop('selectedIndex',0);
                    choicesLocL.removeActiveItems();
                    $('#CliL').prop('selectedIndex',0);
                    $('#CrpL').prop('selectedIndex',0);
                    $('#DOL').val("");
                    $('#LotL').val("");
                    $('#buscafiltro').prop('disabled', true);

                    /*$('#DOL').val("");
                    $('#RegL').val("");
                    $('#GinL').val("");
                    $('#LotL').val("");
                    $('#LocL').val("");
                    $('#CrpL').val("");
                    $('#CliL').val("");*/
                    $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"> <i class="bi bi-trash-fill"></i> Filtros borrados con éxito</div>')
                    setTimeout(function() {
                        $('#alert1').fadeOut(1500);
                    },1500);
                }

            });

            $('#aviso').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');      
        });  
    }else{
        if(botonglobal == 0){
            choicesRegL.enable()
            $('#GinL').attr('disabled', true);
            choicesLocL.enable();
            $('#DOL').attr('disabled', false);
            $('#LotL').attr('disabled', false);
            $('#CliL').attr('disabled', false);
            $('#CrpL').attr('disabled', false);
            
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            choicesLocL.removeActiveItems();
            $('#CliL').prop('selectedIndex',0);
            $('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            $('#LotL').val("");
            $('#buscafiltro').prop('disabled', true);
        }
    }
});

// ---------------------- Fin funcion de filtros --------------------------------------------------------------------------------------------

});

//---------------------------------- Comienza funciones para bloquear y desbloquear inputs ---------------------------------------------------------------------------------------
$(document).ready(function(){
    $('#RegL,#GinL,#LocL,#DOL,#LotL,#CliL,#CrpL').on('change',function(){
        $('#buscafiltro').prop('disabled', false);
    });

    $('#RegL').on('change',function(){
        Region = $.trim($('#RegL').val());
        if (Region != ""){
            $('#GinL').attr('disabled', false);
            choicesLocL.disable();
            $('#DOL').attr('disabled', true);
            $('#LotL').attr('disabled', true);
            $('#CliL').attr('disabled', true);
            
            choicesLocL.removeActiveItems();
            $('#CliL').prop('selectedIndex',0);
            $('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            $('#LotL').val("");
            $('#buscafiltro').prop('disabled', false);
            $("#GinL").empty();
            Region = $.trim($('#RegL').val());    
            var opts = "";
            opcion = 12;

            //alert (Reg);
            if (Region.split(',').length === 1) {
                $('#GinL').attr('disabled', false);
                $.ajax({
                    url: "bd/crud.php",
                    type: "POST",
                    datatype: "json",
                    data: { Region: Region, opcion: opcion },
                    success: function (data) {
                        opts = JSON.parse(data);
                        $('#GinL').append('<option value="" selected >All...</option>');
                        for (var i = 0; i < opts.length; i++) {
                            $('#GinL').append('<option value="' + opts[i].IDGin + '">' + opts[i].GinName + '</option>');
                        }
                    },
                    error: function (data) {
                        alert('error');
                    }
                });
            } else {
                $('#GinL').attr('disabled', true);
            }
        }else{
            choicesRegL.enable()
            $('#GinL').attr('disabled', true);
            choicesLocL.enable();
            $('#DOL').attr('disabled', false);
            $('#LotL').attr('disabled', false);
            $('#CliL').attr('disabled', false);
            $('#CrpL').attr('disabled', false);
            
            //$('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            choicesLocL.removeActiveItems();
            $('#CliL').prop('selectedIndex',0);
            //$('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            $('#LotL').val("");
            $('#buscafiltro').prop('disabled', true);
        }
    });


    $('#LocL').on('change',function(){
        LocL = $.trim($('#LocL').val());
        if (LocL != ""){
            choicesRegL.disable()
            $('#GinL').attr('disabled', true);
            $('#DOL').attr('disabled', true);
            //$('#LotL').attr('disabled', true);
            $('#CliL').attr('disabled', true);
            
            //$('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            $('#CliL').prop('selectedIndex',0);
            //$('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            //$('#LotL').val("");
            
            $('#buscafiltro').prop('disabled', false);
        }else{
            choicesRegL.enable()
            $('#GinL').attr('disabled', true);
            choicesLocL.enable();
            $('#DOL').attr('disabled', false);
            $('#LotL').attr('disabled', false);
            $('#CliL').attr('disabled', false);
            $('#CrpL').attr('disabled', false);
            
            //$('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            choicesLocL.removeActiveItems();
            $('#CliL').prop('selectedIndex',0);
            //$('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            $('#LotL').val("");
            $('#buscafiltro').prop('disabled', true);
        }
        
    });

    $('#DOL').on('change',function(){
        DOL = $.trim($('#DOL').val());
        if (DOL != ""){
            choicesRegL.disable()
            $('#GinL').attr('disabled', true);
            choicesLocL.disable();
            $('#LotL').attr('disabled', true);
            $('#CliL').attr('disabled', true);
            $('#CrpL').attr('disabled', true);
            //$("input.group1").attr("disabled", true);
            
            //$('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            choicesLocL.removeActiveItems();
            $('#CliL').prop('selectedIndex',0);
            $('#CrpL').prop('selectedIndex',0);
            //$('#DOL').val("");
            $('#LotL').val("");
            
            $('#buscafiltro').prop('disabled', false);
        }else{
            choicesRegL.enable()
            $('#GinL').attr('disabled', true);
            choicesLocL.enable();
            $('#DOL').attr('disabled', false);
            $('#LotL').attr('disabled', false);
            $('#CliL').attr('disabled', false);
            $('#CrpL').attr('disabled', false);
            //$("input.group1").attr("disabled", false);
            
            //$('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            choicesLocL.removeActiveItems();
            $('#CliL').prop('selectedIndex',0);
            $('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            $('#LotL').val("");
            $('#buscafiltro').prop('disabled', true);
        }
        
    });

    $('#LotL').on('change',function(){
        LotL = $.trim($('#LotL').val());
        if (LotL != ""){
            choicesRegL.disable()
            $('#GinL').attr('disabled', true);
            //choicesLocL.disable();
            $('#DOL').attr('disabled', true);
            $('#CliL').attr('disabled', true);
            $('#CrpL').attr('disabled', true);
            
            //$('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            $('#CliL').prop('selectedIndex',0);
            $('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            //$('#LotL').val("");
            
            $('#buscafiltro').prop('disabled', false);
        }else{
            choicesRegL.enable()
            $('#GinL').attr('disabled', true);
            choicesLocL.enable();
            $('#DOL').attr('disabled', false);
            $('#LotL').attr('disabled', false);
            $('#CliL').attr('disabled', false);
            $('#CrpL').attr('disabled', false);
            
            //$('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            choicesLocL.removeActiveItems();
            $('#CliL').prop('selectedIndex',0);
            $('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            $('#LotL').val("");
            $('#buscafiltro').prop('disabled', true);
        }
        
    });

    $('#CliL').on('change',function(){
        CliL = $.trim($('#CliL').val());
        if (CliL != ""){
            choicesRegL.disable()
            $('#GinL').attr('disabled', true);
            choicesLocL.disable();
            $('#DOL').attr('disabled', true);
            $('#LotL').attr('disabled', true);
            $('#CrpL').attr('disabled', true);
            
            //$('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            choicesLocL.removeActiveItems();
            $('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            $('#LotL').val("");
            
            $('#buscafiltro').prop('disabled', false);
        }else{
            choicesRegL.enable()
            $('#GinL').attr('disabled', true);
            choicesLocL.enable();
            $('#DOL').attr('disabled', false);
            $('#LotL').attr('disabled', false);
            $('#CliL').attr('disabled', false);
            $('#CrpL').attr('disabled', false);
            
            //('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            choicesLocL.removeActiveItems();
            $('#CliL').prop('selectedIndex',0);
            $('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            $('#LotL').val("");
            $('#buscafiltro').prop('disabled', true);
        }
        
    });

    $('#CrpL').on('change',function(){
        CrpL = $.trim($('#CrpL').val());
        if (CrpL != ""){
            $('#buscafiltro').prop('disabled', false);
        }
    });
    
});

//---------------------------------- Termina funciones para bloquear y desbloquear inputs ---------------------------------------------------------------------------------------


$(document).ready( function () {

    //Condiciones check para DO
    $('#SinDO').on('change',function(){
        if (this.checked) {
            $('.groupCDO').prop('checked', false);
            $('.groupAllDO').prop('checked', false);
        }  
    });

    $('#ConDO').on('change',function(){
        if (this.checked) {
            $('.groupSDO').prop('checked', false);
            $('.groupAllDO').prop('checked', false);
        }  
    });

    $('#AllDO').on('change',function(){
        if (this.checked) {
            $('.groupSDO').prop('checked', false);
            $('.groupCDO').prop('checked', false);
        }  
    });

    $('#SinDO,#ConDO,#AllDO').on('change',function(){
        if (this.checked) {
        }else{
            $('.groupAllDO').prop('checked', true);
        }  
    });

    //Condiciones check para assigned
    $('#SinCli').on('change',function(){
        if (this.checked) {
            $('.groupCC').prop('checked', false);
            $('.groupAllC').prop('checked', false);
        }  
    });

    $('#ConCli').on('change',function(){
        if (this.checked) {
            $('.groupSC').prop('checked', false);
            $('.groupAllC').prop('checked', false);
        }  
    });

    $('#AllCli').on('change',function(){
        if (this.checked) {
            $('.groupSC').prop('checked', false);
            $('.groupCC').prop('checked', false);
        }  
    });

    $('#SinCli,#ConCli,#AllCli').on('change',function(){
        if (this.checked) {
        }else{
            $('.groupAllC').prop('checked', true);
        }  
    });

    // filtrar tabla Sin DO
    $.fn.dataTable.ext.search.push(
      function( settings, searchData, index, rowData, counter ) {
        var SDOs = $('input:checkbox[name="SinDO"]:checked').map(function() {
          return this.value;
        }).get();
     
        if (SDOs.length === 0) {
          return true;
        }
        
        if (SDOs.indexOf(searchData[7]) !== -1) {
          return true;
        }
        
        return false;

      }
    );
    
    // Filtrar tabla Con DO
    $.fn.dataTable.ext.search.push(
        function( settings, searchData, index, rowData, counter ) {

          var CDOs = $('input:checkbox[name="ConDO"]:checked').map(function() {
            return this.value;
          }).get();
       
          if (CDOs.length === 0) {
            return true;
          }
          
          if (CDOs.indexOf(searchData[7]) === -1){
            return true;
          }
          
          return false;
          
        }
      );
  
    // Filtrar Sin Client
    $.fn.dataTable.ext.search.push(
      function( settings, searchData, index, rowData, counter ) {

        var SClis = $('input:checkbox[name="SinCli"]:checked').map(function() {
          return this.value;
        }).get();
  
        if (SClis.length === 0) {
          return true;
        }
        
        if (SClis.indexOf(searchData[15]) !== -1) {
          return true;
        }
        
        return false;
      }
    );

    // Filtrar con Client
    $.fn.dataTable.ext.search.push(
        function( settings, searchData, index, rowData, counter ) {
  
          var CClis = $('input:checkbox[name="ConCli"]:checked').map(function() {
            return this.value;
          }).get();
    
          if (CClis.length === 0) {
            return true;
          }
          
          if (CClis.indexOf(searchData[15]) === -1) {
            return true;
          }
          
          return false;
        }
      );
    
  
    //var tablaUsuarios = $('#example').DataTable();
    
   $('input:checkbox').on('change', function () {
    tablaUsuarios.draw();
   });
  
});
  
 var choicesRegL;
$(document).ready(function(){
    choicesRegL = new Choices('#RegL', {
        removeItemButton: true,
        maxItemCount:7
    });     
});

var choicesLocL;
$(document).ready(function(){
    choicesLocL = new Choices('#LocL', {
        removeItemButton: true,
        maxItemCount:7
    });     
});

