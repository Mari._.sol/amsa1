$(document).ready(function() {

$('#tablaClients tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );


var opcion;
opcion = 4;
    
tablaCli = $('#tablaClients').DataTable({
    
    //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Export to Excel',
				className: 'btn btn-success',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			{
				extend:    'pdfHtml5',
				text:      '<i class="fas fa-file-pdf"></i> ',
				titleAttr: 'Export to PDF',
                className: 'btn btn-danger',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			/*{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},*/
		],
    
    initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
    },
    "order": [ 1, 'asc' ],
    "scrollX": true,
    "scrollY": "50vh",
    "autoWidth": true,
    "scrollCollapse": true,
    "lengthMenu": [100,200,300,500],
    //"aoColumnDefs": [{ "bVisible": false, "aTargets": [ 19 ]}],
    fixedColumns:   {
            left: 0,
            right: 1
        },
    "ajax":{            
        "url": "bd/crudClients.php", 
        "method": 'POST', //usamos el metodo POST
        "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
        "dataSrc":""
    },
    "columns":[
        {"data": "CliID"},
        {"data": "Cli"},
        {"data": "BnName"},
	{"data": "RFC"},
        {"data": "Cty"},
        {"data": "State"},
        {"data": "Town"},
        {"data": "CP"},
        {"data": "Drctn"},
        {"data": "Ref"},
        {"data": "Ct1"},
        {"data": "Tel1"},
        {"data": "Ext1"},
        {"data": "Ct2"},
        {"data": "Tel2"},
        {"data": "Ext2"},
        {"data": "Ct3"},
        {"data": "Tel3"},
        {"data": "Ext3"},
        {"data" : "Cmt"},
        {"data" : "Cde"},
        {"data" : "ClientGrp"},   //NUEVA COLUMNA DE CLIENTE 
        {"data" : "Maps"},
        {"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button>"}
    ]
});     

$('.dataTables_length').addClass('bs-select');

var fila; //captura la fila, para editar o eliminar
//submit para el Alta y Actualización
$('#formCli').submit(function(e){
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
    Cli = $.trim($('#Cli').val().toUpperCase());    
    BnName = $.trim($('#BnName').val().toUpperCase());
    RFC = $.trim($('#RFC').val().toUpperCase());
    Cty = $.trim($('#Cty').val().toUpperCase());
    State = $.trim($('#State').val());
    Town = $.trim($('#Town').val());
    CP = $.trim($('#CP').val());    
    Drctn = $.trim($('#Drctn').val().toUpperCase());
    Ref = $.trim($('#Ref').val().toUpperCase());
    Ct1 = $.trim($('#Ct1').val().toUpperCase());
    Tel1 = $.trim($('#Tel1').val());
    Ext1 = $.trim($('#Ext1').val());
    Ct2 = $.trim($('#Ct2').val().toUpperCase());
    Tel2 = $.trim($('#Tel2').val());
    Ext2 = $.trim($('#Ext2').val());
    Ct3 = $.trim($('#Ct3').val().toUpperCase());
    Tel3 = $.trim($('#Tel3').val());
    Ext3 = $.trim($('#Ext3').val());
    Cmt = $.trim($('#Cmt').val().toUpperCase());
    Cde = $.trim($('#Cde').val().toUpperCase());    
    ClientGrp = $.trim($('#ClientGrp').val().toUpperCase());
    latitud = $.trim($('#latitud').val());
    longitud = $.trim($('#longitud').val());   
    
    coordenadas="";
    if( (latitud == "" || latitud.length == 0) && ( longitud != "" || longitud.length != 0) ) {
        return alert('Ingresa Latitud');
    }else if ( (longitud == "" || longitud.length == 0) && (latitud != "" || latitud.length != 0) ) {
        return alert('Ingresa Longitud');
    }
    latitude = parseFloat(document.getElementById('latitud').value);
    longitude = parseFloat(document.getElementById('longitud').value);
    if(latitud != "" && longitud!=""){
      if (latitude >= 14.5 && latitude <= 32.7 && longitude >= -118.4 && longitude <= -86.7) {
          document.getElementById('resultado').textContent = " ";
          console.log("Dentro de MX");
      } else {
          document.getElementById('resultado').textContent = "¡Coordenadas invalidas!";
          alert("¡Coordenadas fuera de México...!");
          return;
      }
      coordenadas = latitud+","+longitud;
    }

    
    
    
        $.ajax({
          url: "bd/crudClients.php",
          type: "POST",
          datatype:"json",    
          data:  { CliID:CliID, Cli:Cli, BnName:BnName, RFC:RFC, Cty:Cty, State:State, Town:Town, CP:CP, Drctn:Drctn, Ref:Ref, Ct1:Ct1, Tel1:Tel1, Ext1:Ext1, Ct2:Ct2, Tel2:Tel2, Ext2:Ext2, Ct3:Ct3, Tel3:Tel3, Ext3:Ext3, Cmt:Cmt, Cde:Cde,ClientGrp:ClientGrp, opcion:opcion, maps:coordenadas},    
          success: function(data) {
            tablaCli.ajax.reload(null, false);
           }
        });
    $('#modalCRUD').modal('hide');									     			
});

//para limpiar los campos antes de dar de Alta una Persona
$("#btnNuevo").click(function(){
    opcion = 1; //alta           
    CliID=null;
    $("#Town").empty();
    $('#Town').append('<option class="form-control" selected disabled>- Select -</option>');
    $("#formCli").trigger("reset");
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    $(".modal-title").text("New Client");
    $('#modalCRUD').modal('show');	    
     document.getElementById('resultado').textContent = " ";


    //AGREGAR MISMO NOMBRE A CAMPO CLIENTGRP SOLO EN MODAL NEW
    $("#Cli").on("change", function(){
        cligrp()
    });
});

//Editar        
$(document).on("click", ".btnEditar", function(){
    opcion = 2;//editar
    fila = $(this).closest("tr");	   
    $("#Town").empty();     
    CliID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID
    Cli = fila.find('td:eq(1)').text(); 		            
    BnName = fila.find('td:eq(2)').text();
    RFC = fila.find('td:eq(3)').text();
    Cty = fila.find('td:eq(4)').text();
    State = fila.find('td:eq(5)').text();
    Town = fila.find('td:eq(6)').text();
    CP = fila.find('td:eq(7)').text();
    Drctn = fila.find('td:eq(8)').text();
    Ref = fila.find('td:eq(9)').text();
    Ct1 = fila.find('td:eq(10)').text();
    Tel1 = parseFloat(fila.find('td:eq(11)').text());
    Ext1 = fila.find('td:eq(12)').text();
    Ct2 = fila.find('td:eq(13)').text();
    Tel2 = parseFloat(fila.find('td:eq(14)').text());
    Ext2 = fila.find('td:eq(15)').text();
    Ct3 = fila.find('td:eq(16)').text();
    Tel3 = parseFloat(fila.find('td:eq(17)').text());
    Ext3 = fila.find('td:eq(18)').text();
    Cmt = fila.find('td:eq(19)').text();
    ClientGrp = fila.find('td:eq(21)').text();
    Maps = fila.find('td:eq(22)').text();
    coord = Maps.split(',');
    latitud = coord[0];
    longitud = coord[1];
    var data = $('#tablaClients').DataTable().row(fila).data(); //fila.find('td:eq(24)').text();
    Cde = data['Cde'];
    $("#Cli").val(Cli);
    $("#BnName").val(BnName);
    $("#RFC").val(RFC);
    $("#Cty").val(Cty);
    $("#State").val(State); //append('<option class="form-control selected">' + fila.find('td:eq(4)').text() + '</option>');
    //$("#Town").val(Town);
    $('#Town').append($('<option>').val(Town).text(Town));
    $("#CP").val(CP);
    $("#Drctn").val(Drctn);
    $("#Ref").val(Ref);
    $("#Ct1").val(Ct1);
    $("#Tel1").val(Tel1);
    $("#Ext1").val(Ext1);
    $("#Ct2").val(Ct2);
    $("#Tel2").val(Tel2);
    $("#Ext2").val(Ext2);
    $("#Ct3").val(Ct3);
    $("#Tel3").val(Tel3);
    $("#Ext3").val(Ext3);
    $("#Cmt").val(Cmt);
    $("#Cde").val(Cde);
    $("#ClientGrp").val(ClientGrp);
    $("#latitud").val(latitud);
    $("#longitud").val(longitud);
    $(".modal-header").css("background-color", "#17562c");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Edit Client");		
    $('#modalCRUD').modal('show');		 
    document.getElementById('resultado').textContent = " ";  
});


$("#State").change(function () {   
    var estado = $(this).val();
    cuidades(estado);         

     });    

//Borrar
$(document).on("click", ".btnBorrar", function(){
    fila = $(this);           
    InvID = $(this).closest('tr').find('td:eq(1)').text();		
    opcion = 3; //eliminar        
    var respuesta = confirm("¿Está seguro que deseas deshabilitar el registro "+InvID+"?");                
    if (respuesta) {            
        $.ajax({
          url: "bd/crudExport.php",
          type: "POST",
          datatype:"json",    
          data:  {opcion:opcion, InvID:InvID},    
          success: function() {
              tablaExp.ajax.reload(null, false); //tablaInv.row(fila.parents('tr')).remove().draw();                  
           }
        });	
    }
 });
     
});

function cuidades(estado){
opcion=6;
    $.ajax({
        url: "bd/crudClients.php",
        cache: false,
        type: "POST",
        datatype:"json",
        data:  {opcion:opcion,estado:estado},    
        success: function(data){                  
            $("#Town").empty();
            $('#Town').append('<option class="form-control" selected disabled>- Select -</option>');
            opts = JSON.parse(data);
            for (var i = 0; i< opts.length; i++){    

                $('#Town').append($('<option>').val(opts[i].City).text(opts[i].City));         
            
            }
 
        },
        error: function(data) {
           
            alert('error');
        }
    });
}

function cligrp(){
    Cli = $("#Cli").val();
    $("#ClientGrp").val(Cli);

}
/*function validarCoordenadas() {
    const latitud = parseFloat(document.getElementById('latitud').value);
    const longitud = parseFloat(document.getElementById('longitud').value);

    if (latitud >= 14.5 && latitud <= 32.7 && longitud >= -118.4 && longitud <= -86.7) {
        document.getElementById('resultado').textContent = "Las coordenadas están dentro de México.";
        //console.log("Dentro de MX");
    } else {
        document.getElementById('resultado').textContent = "Las coordenadas no están dentro de México"
        //return alert('Coordenadas fuera de México');
    }
}*/
