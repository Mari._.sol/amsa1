<?php
session_start();
if (!isset($_SESSION['username'])) :
    include_once('index.php');
else :
    $Typ_Cli = $_SESSION['Typ_Cli'];
    $permiso = $_SESSION['Admin'];
?>
    <!doctype html>
    <html lang="es en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="img/ecom.png" />
        <title>Clients</title>

        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="../portalLogistica/assets/bootstrap/css/bootstrap.min.css">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="../portalLogistica/main.css">
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>

    <body>

        <!-- Scripts -->

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>


        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <!-- librerias necesarias para finalizar sesion por inactividad -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
        <!-- Scrip para finalizar sesion por inactividad -->
        <script type="text/javascript" src="timer.js"></script>        

        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>

        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
        
        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>

        <?php if ($Typ_Cli == 1) { ?>
            <script type="text/javascript" src="mainClients.js"></script>
        <?php } else { ?>
            <script type="text/javascript" src="mainClientsR.js"></script>
        <?php } ?>
        

        <!-- Terminan Scripts -->

        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>

                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <li><a class="dropdown-item" href="trucks.php">Trucks</a></li>
                        <li><a class="dropdown-item" href="lots.php">Lots</a></li>
                        <?php if ($_SESSION['username'] == 'ISELA LARA' || $_SESSION['username'] == 'DARIO ROMERO' || $_SESSION['username'] == 'CESAR ARMAS' || $_SESSION['username'] == 'LUIS ANGEL RENDON TARIN' || $_SESSION['username'] == 'JOSE RENDON'){?>
                        <li><a class="dropdown-item" href="bales.php">Bales</a></li>
                        <?php } ?>
                        <li><a class="dropdown-item" href="do.php">Delivery Order</a></li>
                        <li><a class="dropdown-item" href="adjustments.php">Adjustments</a></li>
                        <li><a class="dropdown-item" href="inv.php">Invoice</a></li>
                        <li><a class="dropdown-item" href="export.php">Containers</a></li>
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="#">Clients</a></li>
                        <li><a class="dropdown-item" href="routes.php">Routes</a></li>
                        <li><a class="dropdown-item" href="requisition.php">Requisition</a></li>
                        <li><a class="dropdown-item" href="CMS.php">CMS</a></li>
                        <?php if ($permiso == 1){?>
                        <li><a class="dropdown-item" href="adminusers.php">User Management</a></li>
                        <?php } ?>
                    </ul>
                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-2" href="main.php">
                    <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <p>/ Clients</p>
                </div>
                <div class="container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                    <!-- Botones -->
                    <?php if ($Typ_Cli == 1) : ?>
                        <button id="btnNuevo" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Add"><i class="bi bi-plus-square"></i></button>
                    <?php endif ?>
                </div>
                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                    <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"><?php echo $_SESSION['username']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->

        <!-- Aquí inicia todo código de tablas etc -->
        <!-- Inicio de la tabla -->
        <div class="card card-body" style="opacity:100%;">
            <div class="table-responsive" style="opacity:100%;">
                <table id="tablaClients" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;">
                    <thead style="background-color: #65ac7c;" style="opacity:100%;">
                        <tr>
                            <th class="th-sm">Client ID</th>
                            <th class="th-sm">Client</th>
                            <th class="th-sm">Bussiness Name</th>
			    <th class="th-sm">RFC</th>
                            <th class="th-sm">Country</th>
                            <th class="th-sm">State</th>
                            <th class="th-sm">City/Town</th>
                            <th class="th-sm">Postal Code</th>
                            <th class="th-sm">Direction</th>
                            <th class="th-sm">Reference</th>
                            <th class="th-sm">Contact 1</th>
                            <th class="th-sm">Phone Number 1</th>
                            <th class="th-sm">Extension 1</th>
                            <th class="th-sm">Contact 2</th>
                            <th class="th-sm">Phone Number 2</th>
                            <th class="th-sm">Extension 2</th>
                            <th class="th-sm">Contact 3</th>
                            <th class="th-sm">Phone Number 3</th>
                            <th class="th-sm">Extension 3</th>
                            <th class="th-sm">Comments</th>
                            <th class="th-sm">Code</th>
                            <th class="th-sm">Client Group</th>
                            <th class="th-sm">Coordenadas Maps</th>
                            <?php if ($Typ_Cli == 1) { ?>
                                <th class="no-exportar"></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="th-sm">Client ID</th>
                            <th class="th-sm">Client</th>
                            <th class="th-sm">Bussiness Name</th>
			    <th class="th-sm">RFC</th>
                            <th class="th-sm">Country</th>
                            <th class="th-sm">State</th>
                            <th class="th-sm">City/Town</th>
                            <th class="th-sm">Postal Code</th>
                            <th class="th-sm">Direction</th>
                            <th class="th-sm">Reference</th>
                            <th class="th-sm">Contact 1</th>
                            <th class="th-sm">Phone Number 1</th>
                            <th class="th-sm">Extension 1</th>
                            <th class="th-sm">Contact 2</th>
                            <th class="th-sm">Phone Number 2</th>
                            <th class="th-sm">Extension 2</th>
                            <th class="th-sm">Contact 3</th>
                            <th class="th-sm">Phone Number 3</th>
                            <th class="th-sm">Extension 3</th>
                            <th class="th-sm">Comments</th>
                            <th class="th-sm">Code</th>
                            <th class="th-sm">Client Group</th>
                            <th class="th-sm">Coordenadas Maps</th>
                            <?php if ($Typ_Cli == 1) { ?>
                                <th class="no-exportar"></th>
                            <?php } ?>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!--Modal para CRUD-->
        <div class="modal fade in" data-bs-backdrop="static" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formCli">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Client<font size=2>*</font></label>
                                        <input type="text" class="form-control" style="text-transform:uppercase;" id="Cli" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Bussiness Name <font size=2>*</font></label>
                                        <input type="text" class="form-control" style="text-transform:uppercase;" id="BnName" required>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">RFC:</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase;" id="RFC">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
				<div class="col-lg-2">
				    <div class="form-group">
				        <label for="" class="col-form-label">Code:</label>
					<input type="text" class="form-control" style="text-transform:uppercase;" id="Cde"> 
   				    </div>
				</div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Country:</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase;" id="Cty" value="MEXICO">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">State:</label>
                                        <select class="form-control form-control-sm" id="State" name="State">
                                            <option value="" selected disabled>-Select-</option>
                                            <option value="AGUASCALIENTES">AGUASCALIENTES</option>
                                            <option value="BAJA CALIFORNIA">BAJA CALIFORNIA</option>
                                            <option value="BAJA CALIFORNIA SUR">BAJA CALIFORNIA SUR</option>
                                            <option value="CAMPECHE">CAMPECHE</option>
                                            <option value="CIUDAD DE MEXICO">CIUDAD DE MEXICO</option>
                                            <option value="COAHUILA">COAHUILA</option>
                                            <option value="COLIMA">COLIMA</option>
                                            <option value="CHIAPAS">CHIAPAS</option>
                                            <option value="CHIHUAHUA">CHIHUAHUA</option>
                                            <option value="DURANGO">DURANGO</option>
                                            <option value="ESTADO DE MEXICO">ESTADO DE MEXICO</option>                                            
                                            <option value="GUANAJUATO">GUANAJUATO</option>
                                            <option value="GUERRERO">GUERRERO</option>
                                            <option value="HIDALGO">HIDALGO</option>
                                            <option value="JALISCO">JALISCO</option>                                            
                                            <option value="MICHOACAN">MICHOACAN</option>
                                            <option value="MORELOS">MORELOS</option>
                                            <option value="NAYARIT">NAYARIT</option>
                                            <option value="NUEVO LEON">NUEVO LEON</option>
                                            <option value="OAXACA">OAXACA</option>
                                            <option value="PUEBLA">PUEBLA</option>
                                            <option value="QUERETARO">QUERETARO</option>
                                            <option value="QUINTANA ROO">QUINTANA ROO</option>
                                            <option value="SAN LUIS POTOSI">SAN LUIS POTOSI</option>
                                            <option value="SINALOA">SINALOA</option>
                                            <option value="SONORA">SONORA</option>
                                            <option value="TABASCO">TABASCO</option>
                                            <option value="TAMAULIPAS">TAMAULIPAS</option>
                                            <option value="TLAXCALA">TLAXCALA</option>
                                            <option value="VERACRUZ">VERACRUZ</option>
                                            <option value="YUCATAN">YUCATAN</option>
                                            <option value="ZACATECAS">ZACATECAS</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                         <label for="" class="col-form-label">City</label>
                                         <select class="form-control form-control-sm" style="text-transform:uppercase;" id="Town" >
                                         </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Direction:</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase;" id="Drctn">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Postal Code:</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase;" id="CP">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Reference:</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase;" id="Ref">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Contact 1:</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase;" id="Ct1">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Phone Number:</label>
                                        <input type="text" class="form-control form-control-sm" id="Tel1" value="">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Ext.:</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase;" id="Ext1">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Contact 2:</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase;" id="Ct2">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Phone Number:</label>
                                        <input type="text" class="form-control form-control-sm" id="Tel2" value="">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Ext.:</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase;" id="Ext2">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Contact 3:</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase;" id="Ct3">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Phone Number:</label>
                                        <input type="text" class="form-control form-control-sm" id="Tel3" value="">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Ext.:</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase;" id="Ext3">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Comments:</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase;" id="Cmt">
                                    </div>
                                </div>
                            </div>
                            <!-- Nuevo campo clientGrp -->
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Client Group*</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase;" id="ClientGrp" required>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Latitud</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase;" id="latitud" onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Longitud</label>
                                        <input type="text" class="form-control" style="text-transform:uppercase;" id="longitud" onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
                                    </div>
                                </div>
                                <p id="resultado" style="color:#FF0000" align="left"></p>
                                
                            </div>
                            <font size=2>*Required Fields</font>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" id="btnGuardar" class="btn btn-dark" >Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </body>

    </html>
<?php
endif;
?>
