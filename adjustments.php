<?php
session_start();

if (!isset($_SESSION['username'])) :
    include_once('index.php');
else :
    $Typ_Adj = $_SESSION['Typ_Adj'];
    $permiso = $_SESSION['Admin'];
    $rates=$_SESSION['ViewRates'];


?>

    <!doctype html>
    <html lang="es en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="img/ecom.png" />
        <title>Adjusments</title>

        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="../portalLogistica/assets/bootstrap/css/bootstrap.min.css">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="../portalLogistica/main.css">
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

    </head>

    <body>
        <!-- Scripts -->

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>


        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <!-- librerias necesarias para finalizar sesion por inactividad -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
        <!-- Scrip para finalizar sesion por inactividad -->
        <script type="text/javascript" src="timer.js"></script>


        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>

        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
        
        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#DOrd").keyup(function() {})

                function update() {
                    $("#DOType").val("");
                    $('#DOQty').val("");
                    $('#DOGin').val("");
                    $('#DOOutReg').val("");
                    $('#DOInReg').val("");
                    $('#DOCli').val("");
                    var opts = "";
                    opcion = 6;
                    DO = $.trim($('#DOrd').val());
                    $.ajax({
                        url: "bd/crudStg.php",
                        type: "POST",
                        datatype: "json",
                        data: {
                            DO: DO,
                            opcion: opcion
                        },
                        success: function(data) {
                            opts = JSON.parse(data);
                            tam = opts.length;
                            if (tam < 1) {
                                alert("Delivery Order doesn't exist.");
                            }
                            $('#DOQty').val(opts[0].Qty);
                            $('#DOType').val(opts[0].Typ);
                            $('#DOGin').val(opts[0].Gin);
                            $('#DOOutReg').val(opts[0].OutPlc);
                            $('#DOInReg').val(opts[0].InReg);
                            $('#DOCli').val(opts[0].InPlc);
                        },
                        error: function(data) {
                            alert('error');
                        }
                    });
                }

                $("#DOrd").on("change", function() {
                    $("#InfDO").css("display", "block");
                    update();
                })
            });
        </script>

        <?php if ($Typ_Adj == 1) { ?>
            <script type="text/javascript" src="mainStg.js"></script>
        <?php } else { ?>
            <script type="text/javascript" src="mainAR.js"></script>
        <?php } ?>
       

        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>

                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <li><a class="dropdown-item" href="trucks.php">Trucks</a></li>
                        <li><a class="dropdown-item" href="lots.php">Lots</a></li>
                        <?php if ($_SESSION['username'] == 'ISELA LARA' || $_SESSION['username'] == 'DARIO ROMERO' || $_SESSION['username'] == 'CESAR ARMAS' || $_SESSION['username'] == 'LUIS ANGEL RENDON TARIN' || $_SESSION['username'] == 'JOSE RENDON'){?>
                        <li><a class="dropdown-item" href="bales.php">Bales</a></li>
                        <?php } ?>
                        <li><a class="dropdown-item" href="do.php">Delivery Order</a></li>
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="#">Adjustments</a></li>
                        <li><a class="dropdown-item" href="inv.php">Invoice</a></li>
                        <li><a class="dropdown-item" href="export.php">Containers</a></li>
                        <li><a class="dropdown-item" href="clients.php">Clients</a></li>
                        <li><a class="dropdown-item" href="routes.php">Routes</a></li>
                        <li><a class="dropdown-item" href="requisition.php">Requisition</a></li>
                        <li><a class="dropdown-item" href="CMS.php">CMS</a></li>
                        <?php if ($permiso == 1){?>
                        <li><a class="dropdown-item" href="adminusers.php">User Management</a></li>
                        <?php } ?>
                        <?php if ($rates == 1){?>
                         <li><a class="dropdown-item" href="Rates.php">Rates</a></li>
                        <?php } ?>
                        
                    </ul>
                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-2" href="main.php">
                    <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <p>/ Adjustments</p>
                </div>
                <div class="container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                    <!-- Botones -->
                    <?php if ($Typ_Adj == 1) : ?>
                        <button id="btnNuevo" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Add"><i class="bi bi-plus-square"></i></button>
                    <?php endif ?>
                </div>
                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                    <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"><?php echo $_SESSION['username']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->

        <!-- Inicio de la tabla -->
        <div class="card card-body" style="opacity:100%;">
            <div class="table-responsive" style="opacity:100%;">
                <table id="tablaSettings" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;">
                    <thead style="background-color: #65ac7c;" style="opacity:100%;">
                        <tr>
                            <th class="th-sm">Adjustment ID</th>
                            <th class="th-sm">Date</th>
                            <th class="th-sm">Year</th>
                            <th class="th-sm">Type</th>
                            <th class="th-sm">Delivery Order</th>
                            <th class="th-sm">Requester</th>
                            <th class="th-sm">Gin</th>
                            <th class="th-sm">Departure Region</th>
                            <th class="th-sm">Arrival Region</th>
                            <th class="th-sm">Contract</th>
                            <th class="th-sm">Client</th>
                            <th class="th-sm">Quantity</th>
                            <th class="th-sm">Lot</th>
                            <th class="th-sm">Adjustment Type</th>
                            <th class="th-sm">Reason</th>
                            <th class="th-sm">Comments</th>
                            <th class="th-sm">Full Lot</th>
                            <th class="th-sm">Return DO</th>
                            <th class="th-sm">Return ID</th>
                            <?php if ($Typ_Adj == 1) { ?>
                                <th class="no-exportar"></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="th-sm">Adjustment ID</th>
                            <th class="th-sm">Date</th>
                            <th class="th-sm">Year</th>
                            <th class="th-sm">Type</th>
                            <th class="th-sm">Delivery Order</th>
                            <th class="th-sm">Requester</th>
                            <th class="th-sm">Gin</th>
                            <th class="th-sm">Departure Region</th>
                            <th class="th-sm">Arrival Region</th>
                            <th class="th-sm">Contract</th>
                            <th class="th-sm">Client</th>
                            <th class="th-sm">Quantity</th>
                            <th class="th-sm">Lot</th>
                            <th class="th-sm">Adjustment Type</th>
                            <th class="th-sm">Reason</th>
                            <th class="th-sm">Comments</th>
                            <th class="th-sm">Full Lot</th>
                            <th class="th-sm">Return DO</th>
                            <th class="th-sm">Return ID</th>
                            <?php if ($Typ_Adj == 1) { ?>
                                <th class="th-sm"></th>
                            <?php } ?>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <!--Modal para CRUD-->
        <div class="modal fade" id="modalCRUD" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formStg">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Date<font size=2>*</font></label>
                                        <input type="date" class="form-control form-control-sm" id="Dat" value="<?php echo date("Y-m-d"); ?>" required>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Delivery Order<font size=2>*</font></label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="DOrd" required>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Lot</label>
                                        <select class="form-control form-control-sm" id="Lot"></select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Quantity<font size=2>*</font></label>
                                        <input type="number" class="form-control form-control-sm" id="Qty" pattern="[0-9]+" required>
                                    </div>
                                </div>
                            </div>
                            <div id="InfDO" style="display: none">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Type</label>
                                            <input type="text" class="form-control form-control-sm" id="DOType" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">DO Quantity</label>
                                            <input type="text" class="form-control form-control-sm" id="DOQty" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Gin</label>
                                            <input type="text" class="form-control form-control-sm" id="DOGin" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Departure Region</label>
                                            <input type="text" class="form-control form-control-sm" id="DOOutReg" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Arrival Region</label>
                                            <input type="text" class="form-control form-control-sm" id="DOInReg" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Client</label>
                                            <input type="text" class="form-control form-control-sm" id="DOCli" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Full Lot</label>
                                        <select class="form-control form-control-sm" id="FullTyp" name="FullTyp">
                                            <option value="YES">YES</option>
                                            <option value="NO">NO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Type<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="Typ" name="Typ" required>
                                            <option value="" disabled selected>- Select -</option>
                                            <option value="ADJUSTMENT">ADJUSTMENT</option>
                                            <option value="REJECTED">REJECTED</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Requester<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="Requester" name="Requester" required></select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Return DO</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="DODev">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Reason<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="Rsn" name="Rsn" required>
                                            <option value="" disabled selected>- Select -</option>
                                            <option value="PACAS CON DAÑO">PACAS CON DAÑO</option>
                                            <option value="ERROR EMBARQUE">ERROR EMBARQUE</option>
                                            <option value="CANCELACION DEL CLIENTE">CANCELACION DEL CLIENTE</option>
                                            <option value="CAMBIO DESTINO">CAMBIO DESTINO</option>
                                            <option value="CAMBIO DE MARCAS">CAMBIO DE MARCAS</option>
                                            <option value="SOLICITUD DEL CLIENTE">SOLICITUD DEL CLIENTE</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-7">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Comments</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="Cmt">
                                    </div>
                                </div>
                            </div>
                            <font size=2>*Required Fields</font>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" id="btnGuardar" class="btn btn-dark">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </body>

    </html>
<?php
endif;
?>
