$(document).ready(function() {

$('#tablaSettings tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );


var ID, opcion;
opcion = 4;
    
tablaSettings = $('#tablaSettings').DataTable({
    //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Export to Excel',
				className: 'btn btn-success',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			{
				extend:    'pdfHtml5',
				text:      '<i class="fas fa-file-pdf"></i> ',
				titleAttr: 'Export to PDF',
                className: 'btn btn-danger',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			/*{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},*/
		],
    
    initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
    },
    "order": [ 0, 'desc' ],
    "scrollX": true,
    "scrollY": "50vh",
    "scrollCollapse": true,
    "lengthMenu": [100,200,300,500], 
    "ajax":{            
        "url": "bd/crudStg.php", 
        "method": 'POST', //usamos el metodo POST
        "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
        "dataSrc":""
    },
    "columns":[
        {"data": "ID"},
        {"data": "Date"},
        {"data": "Year"},
        {"data": "Typ"},
        {"data": "DO"},
        {"data": "Requester"},
        {"data": "Gin"},
        {"data": "OutPlc"},
        {"data": "InReg"},
        {"data": "Ctc"},
        {"data": "InPlc"}, //InPlc = cliente en DO
        {"data": "Bales"},
        {"data": "Lot"},
        {"data": "TypeStg"},
        {"data": "Rsn"},
        {"data": "Comments"},
        {"data": "FullLot"},
        {"data": "DODev"},
        {"data": "DevID"}, //<button class='btn btn-danger btn-sm btnBorrar'><i class='material-icons'>delete</i></button>
        //{"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button><button class='btn btn-danger btn-sm btnReturn'><i class='material-icons'>assignment_return</i></button></div></div>"}
    ]
});     

$('.dataTables_length').addClass('bs-select');
     
});
