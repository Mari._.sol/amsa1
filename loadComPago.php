<?php

include_once 'bd/conexion.php';
require __DIR__.'/vendor/autoload.php';
date_default_timezone_set("America/Mexico_City");

use Aws\S3\S3Client; 
use Aws\Exception\AwsException; 

$objeto = new Conexion();
$conexion = $objeto->Conectar();
$LiqID = $_POST['LiqID'];
$fechaload= date('Y-m-d h:i:s a', time());  
$bucket = 'pruebasportal'; //bucket de pruebas
//$bucket = 'portal-liq ';
$ticket = $_FILES['file']['name'];
//Obtener la extencion del archivo
$ext=pathinfo($ticket, PATHINFO_EXTENSION);
$data="Comprobante cargado";
$newfilename = $LiqID.'.'.$ext;


//Consultar si ya hay un certifiicad cargado 
$consulta = "SELECT filePago From amsadb1.Liquidation WHERE LiqID='$LiqID'";  
$resultado = $conexion->prepare($consulta);
$resultado->execute();
$datos=$resultado->fetch();
$extetik = $datos['filePago'];


$s3 = new S3Client([
    'version'     => 'latest',
    'region'      => 'us-east-2', //bucket de pruebas
    //'region'      => 'us-east-1',
    'credentials' => [
        'key'    => 'AKIAT442VUCJQXPEFVZW',
        'secret' => '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',
    ],
]);


// 1. Borrar el archivo si ya exise 
if($extetik != "0" ){
    try {
        $result = $s3->deleteObject([
            'Bucket' => $bucket,
            'Key'    => "ComPago-".$LiqID.".".$extetik
        ]);

        if ($result['DeleteMarker'])
        {
            $data= "actualizado";
        } else {
            $data="Error al actualizar";
        }
    }
    catch (S3Exception $e) {
        exit('Error: ' . $e->getAwsErrorMessage() . PHP_EOL);
    }
}

try {
    // Upload data.
    $result = $s3->putObject([
        'Bucket' => $bucket,
        'Key'    => "CompPago-".$LiqID.".".$ext,
        'SourceFile' => $_FILES['file']['tmp_name']
    ]);

    $consulta = "UPDATE amsadb1.Liquidation SET filePago='$ext' WHERE LiqID='$LiqID'";		
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();    

    /* $Hoy = date('Y-m-d');
    $consulta = "UPDATE amsadb1.Liquidation SET datInv ='$Hoy' WHERE LiqID='$LiqID'";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();  
 */
} catch (S3Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}

$data = "CompPago-".$LiqID.".".$ext;

print json_encode($data, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX
?>