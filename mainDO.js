var choices;
var choices2;
$(document).ready(function() {

$('#tablaOrdenes tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
});


var opcion;
opcion = 4;


  choices = new Choices('#DepRegFil', {
       removeItemButton: true,
       maxItemCount:10
     });     

 choices2  = new Choices('#ArrRegFil', {
       removeItemButton: true,
       maxItemCount:10
     });  
  
    
tablaDO = $('#tablaOrdenes').DataTable({
    
    //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Export to Excel',
				className: 'btn btn-success',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			{
				extend:    'pdfHtml5',
				text:      '<i class="fas fa-file-pdf"></i> ',
				titleAttr: 'Export to PDF',
                className: 'btn btn-danger',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
            {
                text: '<i class="bi bi-funnel-fill"></i>',
                titleAttr: 'Filtrar por',
                orientation: 'landscape',
                className: 'btn btn-info btn-filtrar',
                attr:{
                        id:'filtraboton',
                        "data-toggle": 'modal tooltip',
                        "disabled": false
                }
            },
			/*{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},*/
		],
    
    initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
    },
    "order": [ 20, 'desc' ],
    "order": [ 0, 'desc' ],
    //"order": [ 0, 'desc' ],
    "scrollX": true,
    "scrollY": "50vh",
    "scrollCollapse": true,
    /*"fixedColumns":   true,*/
    "lengthMenu": [100,200,300,500],
    //"aoColumnDefs": [{ "bVisible": false, "aTargets": [ 25, 26]}], //ocultar columnas
    fixedColumns:   {
        left: 0,
        right: 1
    },
    //"aoColumnDefs": [{ "bVisible": false, "aTargets": [24]}],
    "ajax":{            
        "url": "bd/crudDO.php", 
        "method": 'POST', //usamos el metodo POST
        "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
        "dataSrc":""
    },
    "columns":[
        {"data": "Date"},
        {"data": "Year"},
        {"data": "Typ"},
        {"data": "DOrd"},
        {"data": "Rqs"},
        {"data": "GinName"},
        {"data": "RegNameOut"},
        {"data": "RegNameIn"},
        {"data" : "Ctc"},
        {"data": "PlcNameIn"},
        {"data": "Crss"},
        {"data": "PurNo"},
        {"data": "TTQty"},
        {"data": "ToDO"},
        {"data": "Transit"},
        {"data": "Received"},
        {"data": "Rejected"},
        {"data": "Associated"},
        {"data": "ToAssociate"},
        {"data" : "DoCmt"},
        {"data" : "Cons"},
        {"data" : "PDF"},
        {"data" : "Certificate"},
        {"data" : "Cert"},
        {"data" : "Samp"},       
        
        {"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button><button class='btn btn-primary btn-sm previewEmail'><i class='bi bi-envelope-check-fill'></i></button><button class='btn btn-primary btn-sm btnMail' title='Send list'><i class='bi bi-card-list'></i></button><button class='btn btn-info btn-sm btnFnd-Lot'><i class='material-icons'>add_box</i></button><button class='btn btn-info btn-sm btnPDF-DO'><i class='material-icons'>picture_as_pdf</i></button><button class='btn btn-danger btn-sm btnBorrar'><i class='material-icons'>delete</i></button></div></div>"}
    ],
    columnDefs : [
        { targets : [22],
            render : function (data, type, row) {
              return data == '1' ? 'YES' : 'NO'
            }
        },
        { targets : [23],
            render : function (data, type, row) {
              return data == '1' ? 'YES' : 'NO'
            }
        },
        { targets : [24],
            render : function (data, type, row) {
              return data == '1' ? 'YES' : 'NO'
            }
        }

    ]
});     
    
$('.dataTables_length').addClass('bs-select');

var fila; //captura la fila, para editar o eliminar
//submit para el Alta y Actualización
$('#formDO').submit(function(e){
    /*var comboOutPlc = document.getElementById("OutPlc");
    var selectedOutPlc = comboOutPlc.options[comboOutPlc.selectedIndex].text;
    var comboGin = document.getElementById("Gin");
    var selectedGin = comboGin.options[comboGin.selectedIndex].text;
    var comboInReg = document.getElementById("InReg");
    var selectedInReg = comboInReg.options[comboInReg.selectedIndex].text;*/
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
    DOrd = $.trim($('#DOrd').val());    
    Qty = $.trim($('#Qty').val());
    Rqs = $.trim($('#Rqs').val().toUpperCase());
    Dat = $.trim($('#Dat').val());
    Crss = $.trim($('#Crss').val());
    Year = $.trim($('#Year').val());
    Typ = $.trim($('#Typ').val());    
    OutPlc = $.trim($('#OutPlc').val());
    Gin = $.trim($('#Gin').val());
    InReg = $.trim($('#InReg').val());
    InPlc = $.trim($('#InPlc').val());
    Ctc = $.trim($('#Ctc').val().toUpperCase());
    PurNo = $.trim($('#PurNo').val().toUpperCase());
    PDF = $.trim($('#PDF').val().toUpperCase());
    DoCmt = $.trim($('#DoCmt').val().toUpperCase());
    Cons = DOrd.substr(-4);
    CrpDO = DOrd.substr(0,2);
    pacascert = 0;
    muestrasdo = 0;
    //VALIDAR SI ESTA ACTIVA EL CHEACK DE CERTIFICADO
    if( $('#certificado').prop('checked') ) {
        cert = 1;
    }  else {
        cert = 0;
    }

    if( $('#pacascertificadas').prop('checked') ) {
        pacascert = 1;
    }  else {
        pacascert = 0;
    }

    if( $('#muestrasdo').prop('checked') ) {
        muestrasdo = 1;
    }  else {
        muestrasdo = 0;
    }

    if (DOrd == ""){
        alert ("No se ha generado DO number.")
    }else{
        $.ajax({
          url: "bd/crudDO.php",
          type: "POST",
          datatype:"json",    
          data:  {DOrd:DOrd, Qty:Qty, Rqs:Rqs, Dat:Dat, Year:Year, CrpDO:CrpDO, Typ:Typ, OutPlc:OutPlc, Gin:Gin, InReg:InReg, InPlc:InPlc, Ctc:Ctc, DoCmt:DoCmt, Cons:Cons, Crss:Crss, PurNo:PurNo, PDF:PDF, cert:cert, pacascert:pacascert, muestrasdo:muestrasdo, opcion:opcion},    
          success: function(data) {
            tablaDO.ajax.reload(null, false);
           }
        });			        
    $('#modalCRUD').modal('hide');
    }											     			
});
        

// ************************** codigo asignar lotes a DO ****************************************

// submit para asignar DO a los Lotes seleccionados
$('#formDO-Lots').submit(function(e){
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
    DOrd = $.trim($('#DO').val());
    DoCmt = $.trim($('#DoCmtLot').val().toUpperCase());
    
    var selectText = $("#LotesDO").map(function (){
                return $(this).text();
            }).get().join(", ");
    //alert (selectText);
    opcion = 16;
    $.ajax({
          url: "bd/crudDO.php",
          type: "POST",
          datatype:"json",    
          data:  {DOrd:DOrd, DoCmt:DoCmt, opcion:opcion},    
          success: function(data) {
            tablaDO.ajax.reload(null, false);
           }
        });			        
    $('#modalLots').modal('hide');
    //obtener el texto de los lotes seleccionados
    /*var selectText = $("#Lotes option:selected").map(function (){
        return $(this).text();
    }).get().join(", ");*/
    //obtene el LotID de los lotes seleccionados
    /*Lotes = $.trim($('#Lotes').val());
    
    var respuesta = confirm("Are you sure you want to associate these lots "+selectText+"?");
    if (respuesta) {
        $.ajax({
            url: "bd/crudDO.php",
            type: "POST",
            datatype:"json",    
            data:  {DOrd:DOrd, Lotes:Lotes, opcion:opcion},    
            success: function(data){
            }
        });
        $('#modalLots').modal('hide');
    }*/											     			
});
    
    
    

// Formulario Splits ***************************************************************************
    $('#Split-Lots').submit(function(e){                         
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
        var ban = 0;
        LocSplit = $.trim($('#LocSplit').val());    
        DOSplit = $.trim($('#DOSplit').val());
        // Obtener texto del selector y la cantidad de pacas totales del lote ******
        var SelectLot = document.getElementById("LotsSplit");
        var LotTxt = SelectLot.options[SelectLot.selectedIndex].text;
        LotIDSplit = $.trim($('#LotsSplit').val());
        BC = LotTxt.split(" - ");
        BC = BC[1];
        Bal = LotTxt.split(" - ");
        Bal = Bal[0];
        // ***************************************************************************
        Split = $.trim($('#Split').val());
        Split = Split.split(",");
        tam = Split.length;
        //validar que las particiones sean positivas
        for (i=0; i<=tam; i++){
            if (Split[i] < 0){
                ban = 1;
            }
        }
        if (ban == 1){
            alert ("Partitions must be positive");
        }else{
        
            sum = 0;
            $.each(Split,function(){sum+=parseFloat(this) || 0; }); // suma de las particiones
            if (sum>BC || sum<BC){
                alert ("The sum total of the partitions must match the number of bales in the Lot");    
            }else{
                opcion = 14;
                var respuesta = confirm("Are you sure you want to partition the Lot "+Bal+" in "+Split+"?");                
                if (respuesta) {            
                    $.ajax({
                        url: "bd/crudTrk.php",
                        type: "POST",
                        datatype:"json",    
                        data:  {LotIDSplit:LotIDSplit, Split:Split, opcion:opcion},    
                        success: function(data) {
                            tablaTrk.ajax.reload(null, false);
                        }
                    });
	            $('#SplitLots').modal('hide');
                }
            }
        }
        
        /*$.ajax({
            url: "bd/crudTrk.php",
            type: "POST",
            datatype:"json",    
            data:  {TrkID:TrkID, TrkLPlt:TrkLPlt,opcion:opcion},    
            success: function(data) {
                tablaTrk.ajax.reload(null, false);
            }
        });*/			        
        //$('#SplitLots').modal('hide');											     			
    });

 
//Buscar lotes sin DO asignada
$(document).on("click", ".btnFnd-Lot", function(){
    
    //var select = document.getElementById('Lotes');
    //select.size = select.length;
    $("#Lotes").empty();
    $("#LotesDO").empty();
    $("#DoCmtLot").empty();
    //opcion = 12;//editar
    fila = $(this).closest("tr");	        
    DOrd = fila.find('td:eq(3)').text();
    OutReg = fila.find('td:eq(6)').text();
    OutGin = fila.find('td:eq(5)').text();
    DoCmt = fila.find('td:eq(19)').text();
    CrpDO = fila.find('td:eq(1)').text();
    pacascertificadas = fila.find('td:eq(23)').text();
    $("#DO").val(DOrd);
    $("#OutReg").val(OutReg);
    $("#OutGin").val(OutGin);
    $("#DoCmtLot").val(DoCmt);
    $("#CrpDO").val(CrpDO);
    $("#pacascertaddlots").val(pacascertificadas);    
    LoadLotsDisp();
    LoadLotsinDO();
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    $(".modal-title").text("Lots Available");
    $("#modalLots").modal("show");	    
});

 

//para limpiar los campos antes de dar de Alta una Persona
$("#btnNuevo").click(function(){
    $("#USA").css("display", "none");
    $("#NewDO").css("display", "block");
    $("#InReg").empty();
    LoadRegion();
    LoadGin();
    LoadCli();
    LoadRqs();
    $('#OutPlc').append('<option value="" disabled selected >- Select -</option>');
    //$('#InReg').append('<option value="" disabled selected >- Select -</option>');
    $('#Gin').append('<option value="" disabled selected >- Select -</option>');
    $('#InPlc').append('<option value="" disabled selected >- Select -</option>');
    $('#Rqs').append('<option value="" disabled selected >- Select -</option>');
    opcion = 1; //alta           
    //LotID=null;
    $("#formDO").trigger("reset");
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    $(".modal-title").text("New DO");
    $("#modalCRUD").modal("show");
    DOrd = 0;	    

});

//Editar        
$(document).on("click", ".btnEditar", function(){
    DOrd = 0;
    //$("#USA").css("display", "none");
    $("#InReg").empty();
    $("#NewDO").css("display", "none"); 
    $("#certificado").prop("checked", false);
    LoadRegion();
    LoadGin();
    LoadCli();
    LoadRqs();
    opcion = 2;//editar
    fila = $(this).closest("tr");	        
    Dat = fila.find('td:eq(0)').text();
    Year = fila.find('td:eq(1)').text();
    Typ = fila.find('td:eq(2)').text();
    DOrd = parseFloat(fila.find('td:eq(3)').text());
    Qty = parseFloat(fila.find('td:eq(12)').text());
    Crss = fila.find('td:eq(10)').text();
    PurNo = fila.find('td:eq(11)').text();
    Ctc = fila.find('td:eq(8)').text();
    DoCmt = fila.find('td:eq(19)').text();
    RegUSA = fila.find('td:eq(6)').text();
    var data = $('#tablaOrdenes').DataTable().row(fila).data(); //fila.find('td:eq(24)').text();
    certi = fila.find('td:eq(22)').text();//certificado
    pacascertificadas = fila.find('td:eq(23)').text();//certificado
    muestras = fila.find('td:eq(24)').text();//certificado
    PDF = data['PDF'];
    $("#DOrd").val(DOrd);
    $("#Qty").val(Qty);
    $('#Rqs').append('<option class="form-control selected">' + fila.find('td:eq(4)').text() + '</option>');
    $("#Dat").val(Dat);
    $("#Year").val(Year);
    $("#Typ").val(Typ);
    $('#OutPlc').append('<option class="form-control selected">' + fila.find('td:eq(6)').text() + '</option>');
    $('#Gin').append('<option class="form-control selected">' + fila.find('td:eq(5)').text() + '</option>');
    $('#InReg').append('<option class="form-control selected">' + fila.find('td:eq(7)').text() + '</option>');
    $('#InPlc').append('<option class="form-control selected">' + fila.find('td:eq(9)').text() + '</option>');
    $("#Ctc").val(Ctc);
    $("#Crss").val(Crss);
    $("#PurNo").val(PurNo);
    $("#DoCmt").val(DoCmt);
    $("#PDF").val(PDF);
    LoadInReg();
    if (RegUSA == "USA"){
        $("#USA").css("display", "block");
    }else{
        $("#USA").css("display", "none");
    }
    if (Typ == "CON"){
        $("#InPlc").prop("disabled", true);
        $("#Ctc").prop("disabled", true);
    } else {
        $("#InPlc").prop("disabled", false);
        $("#Ctc").prop("disabled", false);
    }

     //MARCAR LOS CHECKS DE PACAS CERTIFICADAS, MUESTRAS Y CERTIFICADO DE ORIGEN
    if (certi=="YES"){
        $("#certificado").prop("checked", true);
    }
    else{
        $("#certificado").prop("checked", false);
    }

    if (pacascertificadas=="YES"){
        $("#pacascertificadas").prop("checked", true);
    }
    else{
        $("#pacascertificadas").prop("checked", false);
    }

    if (muestras=="YES"){
        $("#muestrasdo").prop("checked", true);
    }
    else{
        $("#muestrasdo").prop("checked", false);
    }

    $(".modal-header").css("background-color", "#17562c");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Edit DO");		
    $('#modalCRUD').modal('show');		   
});

//VALIDAR SI SE ACTIVA LA CASILLA DE PACAS CERTIFICADAS
$('#pacascertificadas').change(function(){
    cadenapdf=$("#PDF").val();
    resultadoval = validadocert(DOrd)
    //console.log(resultadoval)
    if(resultadoval==0){
        if($(this).is(':checked')) {
            cadenapdf = 'PACAS CERTIFICADAS | ' + cadenapdf
            $("#PDF").val(cadenapdf);      
    
        } else {       
            cadenapdf=$("#PDF").val();      
            $("#PDF").val('');
            textoReemplazado = cadenapdf.replace('PACAS CERTIFICADAS | ', '');
            $("#PDF").val(textoReemplazado);        
        }
        
    }
    else{
        alert ('DO contains ' +resultadoval +' lots. Disassociate Lots to continue')
        if (pacascertificadas=="YES"){
            $("#pacascertificadas").prop("checked", true);
        }
        else{
            $("#pacascertificadas").prop("checked", false);
        }
    }
});


    

//Boton para split de lotes
    $("#btnSplit").click(function(){
        $("#LocSplit").empty();
        $("#DOrdSplit").empty();
        $("#LotsSplit").empty();
        LoadRegionSplit();
        $('#LocSplit').append('<option value="" disabled selected >- Select -</option>');
        //ValidateWgh();
        //opcion = 1; //alta
        //TrkID=null;
        $("#Split-Lots").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Split a Lot");
        $('#SplitLots').modal('show');	    
    });
    

//Borrar
$(document).on("click", ".btnBorrar", function(){
    fila = $(this);           
    DOrd = parseFloat($(this).closest('tr').find('td:eq(3)').text()) ;		
    opcion = 3; //eliminar        
    var respuesta = confirm("¿Está seguro que deseas deshabilitar el registro "+DOrd+"?");                
    if (respuesta) {            
        $.ajax({
          url: "bd/crudDO.php",
          type: "POST",
          datatype:"json",    
          data:  {opcion:opcion, DOrd:DOrd},    
          success: function() {
              tablaDO.row(fila.parents('tr')).remove().draw();                  
           }
        });	
    }
 });
    
//Generar PDF

/*$(document).on("click", ".btnPDF-DO", function(){
    fila = $(this);           
    Date = $(this).closest('tr').find('td:eq(0)').text();
    Typ = $(this).closest('tr').find('td:eq(2)').text();
    DO = parseFloat($(this).closest('tr').find('td:eq(3)').text());
    Dep = $(this).closest('tr').find('td:eq(6)').text();
    Arr = $(this).closest('tr').find('td:eq(7)').text();
    Gin = $(this).closest('tr').find('td:eq(5)').text();
    Ctc = $(this).closest('tr').find('td:eq(8)').text();
    Cli = $(this).closest('tr').find('td:eq(9)').text();
    window.open("/_Internal-User2021/bd/PDFDO.php?Date="+Date+"&Type="+Typ+"&DO="+DO+"&Out="+Dep+"&In="+Arr+"&Gin="+Gin+"&Ctc="+Ctc+"&Cli="+Cli+"");
});*/
    
$("#Typ").on("change", function(){
        $("#InReg").empty();
        LoadInReg();
});



//*******VALIDAR SI SE REQUIERE CERTIFICADO*****************************/
$("#InPlc").change(function () {   
    certificado()

});  

$("#Typ").change(function () {   
    certificado()

});  

$("#InReg").change(function () {   
    certificado()

});  

$("#OutPlc").change(function () {   
    certificado()

}); 






     
});


// *********************************************************************************

// Funcion asociar lotes de DO con boton dentro del modal
$(document).ready(function(){
    
        $("#btnAddLot").click(function(){
            
            OutReg = $.trim($('#OutReg').val());
            DOrd = $.trim($('#DO').val());
            SchDate = $.trim($('#ArrDte').val()); //extraer fecha
            var info = SchDate.split('-').reverse().join('/').slice(0,5); //camviar formato dd/mm/aaaa
            opcion = 13;
            DOrdCmt = $.trim($('#DoCmtLot').val());
            //var myString = DOrdCmt.split("|").pop(); //DOrdCmt.substr(DOrdCmt.indexOf("|") + 1)
            //obtener el texto de los lotes seleccionados
            var selectText = $("#Lotes option:selected").map(function (){
                return $(this).text();
            }).get().join(", ");
            //obtene el LotID de los lotes seleccionados
            Lotes = $.trim($('#Lotes').val());

                $.ajax({
                    url: "bd/crudDO.php",
                    type: "POST",
                    datatype:"json",    
                    data:  {OutReg:OutReg, DOrd:DOrd, Lotes:Lotes, SchDate:SchDate, opcion:opcion},    
                    success: function(data){
                    }
                });
            
            LoadLotsDisp();
            LoadLotsinDO();
    
            //var selectTextDO = $("#LotesDO").map(function (){
            //    return $(this).text();
            //}).get().join(", ");
            
            //var str2 = selectTextDO.replace(/\n|\r/g, " ");
            
            var str2 = selectText.replace(/\n|\r/g, " ");
            if (SchDate != ""){
                var coment = str2 + "("+info+")| " + DOrdCmt;
            }else{
                var coment = str2 + "| " + DOrdCmt;
            }
            
            $("#DoCmtLot").val(coment);
            
            /*LoadLotsDisp();
            LoadLotsinDO();*/
            
        });


      
        
       
});


//cambiar fechas de lotes
$(document).ready(function(){
    $(document).on("click", ".btnfechas", function(){    

    fila = $(this).closest("tr");	        
    DOrd = fila.find('td:eq(3)').text();
    OutReg = fila.find('td:eq(6)').text();
    OutGin = fila.find('td:eq(5)').text();
    DoCmt = fila.find('td:eq(19)').text();
    CrpDO = fila.find('td:eq(1)').text();
    $('#modalfechaslote').modal('show');


    });
});
// Funcion Eliminar lotes de DO con boton dentro del modal
$(document).ready(function(){

        $("#btnDelLot").click(function(){
        
            OutReg = $.trim($('#OutReg').val());
            DOrd = $.trim($('#DO').val());
            opcion = 15;
            //obtener el texto de los lotes seleccionados
            var selectText = $("#LotesDO option:selected").map(function (){
                return $(this).text();
            }).get().join(", ");
            //obtene el LotID de los lotes seleccionados
            Lotes = $.trim($('#LotesDO').val());

            //var respuesta = confirm("Are you sure you want to associate these lots "+selectText+"?");
            //if (respuesta) {
                $.ajax({
                    url: "bd/crudDO.php",
                    type: "POST",
                    datatype:"json",    
                    data:  {OutReg:OutReg, DOrd:DOrd, Lotes:Lotes, opcion:opcion},    
                    success: function(data){
                    opts = JSON.parse(data);
                    alert(opts);
                    }
                });
            //}
            LoadLotsDisp();
            LoadLotsinDO();
        });
});


// ******************************************************************************************************

// Cargar Lotes Disponibles
function LoadLotsDisp(){
    $("#Lotes").empty();
    $("#LotesDO").empty();
    
    OutReg = $.trim($('#OutReg').val());
    DOrd = $.trim($('#DO').val());
    OutGin = $.trim($('#OutGin').val());
    CrpDO = $.trim($('#CrpDO').val());
    lotscert = $("#pacascertaddlots").val();
    var opts = "";
    opcion = 12;
    $.ajax({
        url: "bd/crudDO.php",
        type: "POST",
        datatype:"json",    
        data:  {DOrd:DOrd, OutReg:OutReg, OutGin:OutGin, CrpDO:CrpDO, lotscert:lotscert, opcion:opcion},    
        success: function(data) {
            opts = JSON.parse(data);
            for (var i = 0; i< opts.length; i++){
                $('#Lotes').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '-' + opts[i].Qty + opts[i].fecha + '</option>  ');
            }
        }
    });	
}

//Cargar Lotes in DO
function LoadLotsinDO(){
    $("#Lotes").empty();
    $("#LotesDO").empty();
    
    OutReg = $.trim($('#OutReg').val());
    DOrd = $.trim($('#DO').val());
    var opts = "";
    opcion = 14;
    $.ajax({
        url: "bd/crudDO.php",
        type: "POST",
        datatype:"json",    
        data:  {DOrd:DOrd, OutReg:OutReg, opcion:opcion},    
        success: function(data) {
            opts = JSON.parse(data);
            for (var i = 0; i< opts.length; i++){
                $('#LotesDO').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '-' + opts[i].Qty + opts[i].fecha + opts[i].truckid+'</option>  ');
            }
        }
    });
}

// *********************************************************************************************************

//Cargar Region
function LoadRegionSplit(){
    $("#LocSplit").empty();   
    var opts = "";
    opcion = 5;
    $.ajax({
          url: "bd/crudDO.php",
          type: "POST",
          datatype:"json",
          data:  {opcion:opcion},    
          success: function(data){
              opts = JSON.parse(data);
              //$('#OutPlc').append('<option class="form-control" >- Select -</option>');
              //$('#InReg').append('<option class="form-control" >- Select -</option>');
              for (var i = 0; i< opts.length; i++){
                  $('#LocSplit').append('<option value="' + opts[i].RegNam + '">' + opts[i].RegNam + '</option>');
                  //$('#InReg').append('<option value="' + opts[i].RegNam + '">' + opts[i].RegNam + '</option>');
              }
           },
        error: function(data) {
            alert('error');
        }
    });
}


//Funcion para buscar lotes para realizar el split  
$(document).ready(function(){
    
    //$("#btnSplit").click(function(){
        $("#btnFndLot").click(function(){
            $("#LotsSplit").empty();
            $("#Split").val("");
            opcion = 13;
            LocSplit = $.trim($('#LocSplit').val());
            DOSplit = $.trim($('#DOSplit').val());
            $.ajax({
                    url: "bd/crudTrk.php",
                    type: "POST",
                    datatype:"json",
                    data:  {LocSplit:LocSplit, DOSplit:DOSplit, opcion:opcion},    
                    success: function(data){
                        opts = JSON.parse(data);
                        $('#LotsSplit').append('<option class="form-control" value="" disabled selected>- Select -</option>');
                        for (var i = 0; i< opts.length; i++){
                            $('#LotsSplit').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + ' - ' + opts[i].Qty + '</option>');
                        }
                    },
                    error: function(data) {
                        alert('error');
                    }
                });
        });
    //});
});


//Cargar Region
function LoadRegion(){
    $("#OutPlc").empty();    
    var opts = "";
    opcion = 5;
    $.ajax({
          url: "bd/crudDO.php",
          type: "POST",
          datatype:"json",
          data:  {opcion:opcion},    
          success: function(data){
              opts = JSON.parse(data);
              //$('#OutPlc').append('<option class="form-control" >- Select -</option>');
              //$('#InReg').append('<option class="form-control" >- Select -</option>');
              for (var i = 0; i< opts.length; i++){
                  $('#OutPlc').append('<option value="' + opts[i].RegNam + '">' + opts[i].RegNam + '</option>');
                  //$('#InReg').append('<option value="' + opts[i].RegNam + '">' + opts[i].RegNam + '</option>');
              }
           },
        error: function(data) {
            alert('error');
        }
    });
}


function LoadInReg(){  
            Typ = $.trim($('#Typ').val());
            if (Typ == "CON"){
                var opts = "";
                opcion = 9;
                $.ajax({
                      url: "bd/crudDO.php",
                      type: "POST",
                      datatype:"json",
                      data:  {opcion:opcion},    
                      success: function(data){
                          opts = JSON.parse(data);
                          $('#InReg').append('<option class="form-control" value="">- Select -</option>');
                          for (var i = 0; i< opts.length; i++){
                              $('#InReg').append('<option value="'+opts[i].RegNam + '">' + opts[i].RegNam + '</option>');
                          }
                       },
                    error: function(data) {
                        alert('error');
                    }
                });
            }else{
                var opts = "";
                opcion = 10;
                $.ajax({
                      url: "bd/crudDO.php",
                      type: "POST",
                      datatype:"json",
                      data:  {opcion:opcion},    
                      success: function(data){
                          opts = JSON.parse(data);
                          $('#InReg').append('<option class="form-control" option value="">- Select -</option>');
                          for (var i = 0; i< opts.length; i++){
                              $('#InReg').append('<option value="' + opts[i].RegNam + '">' + opts[i].RegNam + '</option>');
                          }
                       },
                    error: function(data) {
                        alert('error');
                    }
                });
            }
        }



//Cargar Region
function LoadGin(){
    $("#Gin").empty();
    //$("#InReg").empty();
    var opts = "";
    opcion = 6;
    $.ajax({
          url: "bd/crudDO.php",
          type: "POST",
          datatype:"json",
          data:  {opcion:opcion},    
          success: function(data){
              opts = JSON.parse(data);
              //$('#Gin').append('<option class="form-control">- Select -</option>');
              for (var i = 0; i< opts.length; i++){
                  $('#Gin').append('<option value="' + opts[i].GinName + '">' + opts[i].GinName + '</option>');
              }
           },
        error: function(data) {
            alert('error');
        }
    });
}

//Cargar clientes
function LoadCli(){
    $("#InPlc").empty();
    //$("#InReg").empty();
    var opts = "";
    opcion = 7;
    $.ajax({
          url: "bd/crudDO.php",
          type: "POST",
          datatype:"json",
          data:  {opcion:opcion},    
          success: function(data){
              opts = JSON.parse(data);
              //$('#InPlc').append('<option class="form-control">- Select -</option>');
              for (var i = 0; i< opts.length; i++){
                  $('#InPlc').append('<option value="' + opts[i].Cli + '">' + opts[i].Cli + '</option>');
              }
           },
        error: function(data) {
            alert('error');
        }
    });
}

//Cargar Usuarios requester
function LoadRqs(){
    $("#Rqs").empty();
    //$("#InReg").empty();
    var opts = "";
    opcion = 8;
    $.ajax({
          url: "bd/crudDO.php",
          type: "POST",
          datatype:"json",
          data:  {opcion:opcion},    
          success: function(data){
              opts = JSON.parse(data);
              //$('#InPlc').append('<option class="form-control">- Select -</option>');
              for (var i = 0; i< opts.length; i++){
                  $('#Rqs').append('<option value="' + opts[i].User + '">' + opts[i].User + '</option>');
              }
           },
        error: function(data) {
            alert('error');
        }
    });
}

$( function() {
    $("#Typ").change( function() {
        if ($(this).val() === "CON") {
            $("#InPlc").val("");
            $("#Ctc").val("");
            $("#InPlc").prop("disabled", true);
            $("#Ctc").prop("disabled", true);
        } else {
            $("#InPlc").prop("disabled", false);
            $("#Ctc").prop("disabled", false);
        }
    });
});

//DO de USA
$( function() {
    $("#OutPlc").change( function() {
        if ($(this).val() === "USA") {
            $("#USA").css("display", "block");
        }else{
            $("#USA").css("display", "none");
        }
    });
});

//Funcion para generar DO number
$(document).ready(function(){
    
    $("#btnNuevo").click(function(){
        $("#btnDONumber").click(function(){
            Year = $.trim($('#Year').val());
            OutPlc = $.trim($('#OutPlc').val());
            if (Year == "" || OutPlc == ""){
                alert ("Insert Year and Departure Region");
            }
            opcion = 11;
            //LotID = $.trim($('#LotsAssc').val());
            $.ajax({
                    url: "bd/crudDO.php",
                    type: "POST",
                    datatype:"json",
                    data:  {Year:Year, OutPlc:OutPlc , opcion:opcion},    
                    success: function(data){
                        opts = JSON.parse(data);
                        Yr = parseInt(Year.substr(-2));
                        //Cons = parseInt(opts[0].Cons)
                        //Reg = parseInt(opts[0].DOGen);
                        //alert (Cons);
                        //alert (Reg);
                        Cons = parseInt(opts[0].Cons)
                        Reg = parseInt(opts[0].DOGen);
                        NewDO = Yr * 1000000 + Reg * 10000 + Cons;
                        $('#DOrd').val(NewDO);
                    },
                    error: function(data) {
                        alert('error');
                    }
            });
        });
    });
});


$(document).ready(function(){
$(document).on("click", ".btnPDF-DO", function(){
    fila = $(this);           
    date = $(this).closest('tr').find('td:eq(0)').text();
    Typ = $(this).closest('tr').find('td:eq(2)').text();
    DO = parseFloat($(this).closest('tr').find('td:eq(3)').text());
    Dep = $(this).closest('tr').find('td:eq(6)').text();
    Arr = $(this).closest('tr').find('td:eq(7)').text();
    Gin = $(this).closest('tr').find('td:eq(5)').text();
    Ctc = $(this).closest('tr').find('td:eq(8)').text();
    Cli = $(this).closest('tr').find('td:eq(9)').text();
    Crss = $(this).closest('tr').find('td:eq(10)').text();
    PurNo = $(this).closest('tr').find('td:eq(11)').text();
    window.open("./bd/PDFDO.php?Date="+date+"&Type="+Typ+"&DO="+DO+"&Out="+Dep+"&In="+Arr+"&Gin="+Gin+"&Ctc="+Ctc+"&Cli="+Cli+"&Crss="+Crss+"&PurNo="+PurNo+"");
    tablaDO.ajax.reload(null, false); //location.reload();
});
});

$(document).ready(function(){
 $(document).on("click", ".previewEmail", function () {
    var fila = $(this);
    var usuario = $('#dropdownMenuLink2').text();
    var typeDO = $(this).closest('tr').find('td:eq(2)').text();
    var numDO = $(this).closest('tr').find('td:eq(3)').text();
    var salSucu = $(this).closest('tr').find('td:eq(6)').text(); //Salida de sucursal -> Departure Region
    var regOri = $(this).closest('tr').find('td:eq(5)').text(); //Región de origen -> GIN
    var arrReg = $(this).closest('tr').find('td:eq(7)').text(); // Cliente / Bodega -> Arrival region si es DOM se agrega Client
    var totalQty = $(this).closest('tr').find('td:eq(12)').text(); // Cliente / Bodega -> Arrival region si es DOM se agrega Client
    var client = '';
    if(typeDO != "CON" ){
         client = $(this).closest('tr').find('td:eq(9)').text();
    }
    var observ = $(this).closest('tr').find('td:eq(19)').text();//Observaciones
    var contra = $(this).closest('tr').find('td:eq(8)').text(); //Contrato
    var certif = $(this).closest('tr').find('td:eq(22)').text(); //Certificado
    var balescert = $(this).closest('tr').find('td:eq(23)').text(); //pacas certificadas
    var samples = $(this).closest('tr').find('td:eq(24)').text(); //muestras
    var comp_asunt="";
    var asuntocertori='';
    var asuntocerti='';
    var asuntomuestra ='';
    //AGREGAR AL ASUNTO SI LLEVA CERTIFICADO
    if (certif == 'YES'){
        asuntocertori = ' | CERTIFICADOS DE ORIGEN'
    }
    else{
        asuntocertori=""
    }

    if(balescert === 'YES'){
        asuntocerti = ' | PACAS CERTIFICADAS'
    }
    else{
        asuntocerti ='';
    }

    if(samples === 'YES'){
        asuntomuestra = ' | MUESTRAS'
    }
    else{
        asuntomuestra='';
    }

    comp_asunt =asuntocerti+ asuntomuestra+ asuntocertori 


    $.ajax({
        type: 'POST',
        url: 'bd/getCorreo.php',
        datatype: 'json',
        data: {usuario: usuario,
            typeDO: typeDO, 
            numDO: numDO, 
            salSucu: salSucu, 
            regOri: regOri, 
            arrReg: arrReg, 
            client: client, 
            observ: observ, 
            contra: contra, 
            totalQty:totalQty}
        }) .done(function(data){
                datos = JSON.parse(data);
                $(".modal-header").css( "background-color", "#17562c");
                $(".modal-header").css( "color", "white" );

                $('#tablahtml').val(datos['tabla']);
                $('#tablamue').html(datos['tabla']);
                $('#extraEmails').val(datos["emails"]);
                $('#subject').val(datos["asunto"] + comp_asunt);

                $('#usuario').val(usuario);
                $('#typeDO').val(typeDO);
                $('#numDO').val(numDO);
                $('#salSucu').val(salSucu);
                $('#regOri').val(regOri);
                $('#arrReg').val(arrReg);
                $('#client').val(client);
                $('#observ').val(observ);
                $('#contra').val(contra);
                $('#totalQty').val(totalQty);
                $('#Correo').modal('show');

                if(datos['tabla']===""){
                    $('#lotess').text("Advertencia: No hay lotes asociados");
                }else
                    $('#lotess').text("");
                console.log(datos);


            });

    /*$.ajax({
        type:"POST",
        url: 'bd/correo.php',
        data: {usuario: usuario,typeDO: typeDO, numDO: numDO, salSucu: salSucu, regOri: regOri, arrReg: arrReg, client: client, observ: observ, contra: contra, totalQty:totalQty}
    }) .done(function(ma){
        alert(ma);
    }) .fail(function(){
        alert("error");
    })  */

 });

 $(document).on("click",".sendEmail",function(){
    tablahtml = $('#tablahtml').val();
    emails    = $('#extraEmails').val();
    numDO     = $('#numDO').val();
    subject   = $('#subject').val();
    usuario   = $('#usuario').val();
    typeDO    = $('#typeDO').val();
    salSucu   = $('#salSucu').val();
    regOri    = $('#regOri').val();
    arrReg    = $('#arrReg').val();
    client    = $('#client').val();
    observ    = $('#observ').val();
    contra    = $('#contra').val();
    totalQty  = $('#totalQty').val();

    if(tablahtml===""){
        if(confirm('There is no associated lot. Do you want to submit the form?')){
            $.ajax({
                type:"POST",
                url: 'bd/correo.php',
                data: {usuario: usuario,
                    typeDO: typeDO, 
                    numDO: numDO, 
                    salSucu: salSucu, 
                    regOri: regOri, 
                    arrReg: arrReg, 
                    client: client, 
                    observ: observ, 
                    contra: contra, 
                    totalQty:totalQty,            
                    subject:subject,              
                    emails:emails,
                    tablahtml,tablahtml}
            }) .done(function(ma){
                alert(ma);
                $('#Correo').modal('hide');
            }) .fail(function(){
                alert("error");
            })
        }
    }else{
        $.ajax({
            type:"POST",
            url: 'bd/correo.php',
            data: {usuario: usuario,
                typeDO: typeDO, 
                numDO: numDO, 
                salSucu: salSucu, 
                regOri: regOri, 
                arrReg: arrReg, 
                client: client, 
                observ: observ, 
                contra: contra, 
                totalQty:totalQty,            
                subject:subject,
                emails:emails,
                tablahtml,tablahtml}
        }) .done(function(ma){
            alert(ma);
            $('#Correo').modal('hide');
        }) .fail(function(){
            alert("error");
        })
    }
    
 });

  //Filtrar botón
 function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}


 $("#filtraboton").click(function(){
 
 //inicializar multi select



     
     
     
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    TDO = $('#TDO').val();
    if(TDO != "CON" && TDO != ""){
        $('#clientsDO').attr('disabled',false);
    }else{
        $('#clientsDO').attr('disabled',true);
    }
    $('#filtrarmodal').modal('show');
    
});


//filtro por DO
$('#FilterDO').on('change keyup', function(){
    FDO = $('#FilterDO').val();
    
    if(FDO){
        $('#muestras').attr('disabled', true);
        $('#DepRegFil').attr('disabled', true);
        $('#ArrRegFil').attr('disabled', true);
        $('#TDO').attr('disabled', true);
        $('#clientDO').attr('disabled', true);
        $('#timeSelect').attr('disabled', true);
        $('#fromdate').attr('disabled', true);
        $('#todate').attr('disabled', true);
        //limpiar multiselect
        choices.removeActiveItems();
        choices2.removeActiveItems();


        $('#todate').val("");
        $('#fromdate').val("");
        $('#muestras').prop('selectedIndex',0);
        $('#DepRegFil').prop('selectedIndex',0);
        $('#ArrRegFil').prop('selectedIndex',0);
        $('#timeSelect').prop('selectedIndex',0);
        $('#TDO').prop('selectedIndex',0);
        $('#clientsDO').prop('selectedIndex',0);

        $('#buscafiltro').attr('disabled', false);
    }else{
        $('#muestras').attr('disabled', false);
        $('#DepRegFil').attr('disabled', false);
        $('#ArrRegFil').attr('disabled', false);
        $('#TDO').attr('disabled', false);
        $('#clientDO').attr('disabled', false);
        $('#timeSelect').attr('disabled', false);
        $('#fromdate').attr('disabled', false);
        $('#todate').attr('disabled', false);   
    }
});

$('#TDO').on('change keyup', function(){
    TDO = $('#TDO').val();
    timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
    DepRegfil = $('#DepRegFil').val();
    ArrRegfil = $('#ArrRegFil').val();
    clientsDO = $('#clientsDO').val();
    fromdate = $('#fromdate').val();
    todate = $('#todate').val();

    if(TDO){
        $('#muestras').attr('disabled',true);
        $('#muestras').prop('selectedIndex',0);
        if(TDO != "CON")
            $('#clientsDO').attr('disabled',false);
        else
            $('#clientsDO').attr('disabled',true);

    }else{
        $('#clientsDO').attr('disabled',true);
        $('#clientsDO').prop('selectedIndex',0);
        
        if(timeSelect || DepRegfil || ArrRegfil || fromdate || todate)
            $('#muestras').attr('disabled',true);
        else 
            $('#muestras').attr('disabled',false);
    }
});

$('#timeSelect').on('change', function () {
    timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
    DepRegfil = $('#DepRegFil').val();
    ArrRegfil = $('#ArrRegFil').val();
    clientsDO = $('#clientsDO').val();
    TDO = $('#TDO').val();
    if(timeSelect){
    timeSelect = parseInt(timeSelect);
    let today = new Date();
    //today = today.toLocaleTimeString('es-MX');
    let dateEnMilisegundos = 1000 * 60* 60* 24* timeSelect;
    let suma = today.getTime() - dateEnMilisegundos;
    let fechainicial = new Date(suma);
    //console.log(today);
    datetoday =  formatDate(today);
    datetosearch = formatDate(fechainicial);
    $('#fromdate').val(datetosearch);
    $('#todate').val(datetoday);
    $('#muestras').attr('disabled', true);
    $('#muestras').prop('selectedIndex',0);

    }else{
        $('#fromdate').val("");
        $('#todate').val("");
        if(DepRegfil || ArrRegfil || clientsDO || TDO )
             $('#muestras').attr('disabled', true);
        else
            $('#muestras').attr('disabled', false);


    }

    //console.log(datetoday); //today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()
});

$('#fromdate, #todate').on('change keyup', function () {
    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    muestras = $('select[name=muestras] option').filter(':selected').val();
    muestratext = $( "#muestras option:selected" ).text();
    DepRegFil = $('select[name=DepRegFil] option').filter(':selected').val();
    ArrRegFil = $('select[name=ArrRegFil] option').filter(':selected').val();
    timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
    clientsDO = $('#clientsDO').val();
    TDO = $('#TDO').val();

    if(fromdate == "" && todate == ""){
        $('#timeSelect').prop('selectedIndex',0);
        if(DepRegFil || ArrRegFil || TDO || clientsDO){
            $('#muestras').attr('disabled',true);
        }else{
            $('#muestras').attr('disabled',false);
        }
    }
    if(muestratext != "200 (default)" || DepRegFil || ArrRegFil){
        $('#buscafiltro').attr('disabled', false);
    }else $('#buscafiltro').attr('disabled', true);
});

//On change Region se bloquea last trucks
$(('#DepRegFil, #ArrRegFil')).on('change', function () {
    DepRegfil = $('#DepRegFil').val();
    ArrRegfil = $('#ArrRegFil').val();
    timeSelect = $('#timeSelect').val();
    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    clientsDO = $('#clientsDO').val();
    TDO = $('#TDO').val();
    if(DepRegfil || ArrRegfil){
        $('#muestras').attr('disabled',true);
        if(timeSelect == "" && fromdate == "" && todate == ""){
            $('#timeSelect').prop('selectedIndex',3);
            timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
            timeSelect = parseInt(timeSelect);
            let today = new Date();
            //today = today.toLocaleTimeString('es-MX');
            let dateEnMilisegundos = 1000 * 60* 60* 24* timeSelect;
            let suma = today.getTime() - dateEnMilisegundos;
            let fechainicial = new Date(suma);
            //console.log(today);
            datetoday =  formatDate(today);
            datetosearch = formatDate(fechainicial);
            $('#fromdate').val(datetosearch);
            $('#fromdate').attr('disabled', false);
            $('#todate').attr('disabled',false);
            $('#todate').val(datetoday);
            $('#buscafiltro').attr('disabled',false); 
        }
    }else
        if(TDO || clientsDO || fromdate || todate)
             $('#muestras').attr('disabled', true);
        else 
            $('#muestras').attr('disabled', false);
});
$('#muestras').on('change', function () {
    muestrastext = $( "#muestras option:selected" ).text();
    if(muestrastext != "200 (default)"){
        $('#DepRegFil').attr('disabled', true);
        $('#ArrRegFil').attr('disabled', true);
        $('#TDO').attr('disabled', true);
        $('#clientDO').attr('disabled', true);
        $('#timeSelect').attr('disabled', true);
        $('#fromdate').attr('disabled', true);
        $('#todate').attr('disabled', true);


        $('#todate').val("");
        $('#fromdate').val("");
        $('#DepRegFil').prop('selectedIndex',0);
        $('#ArrRegFil').prop('selectedIndex',0);
        $('#timeSelect').prop('selectedIndex',0);
        $('#TDO').prop('selectedIndex',0);
        $('#clientsDO').prop('selectedIndex',0);
    }else{
         $('#DepRegFil').attr('disabled', false);
                $('#ArrRegFil').attr('disabled', false);
                $('#TDO').attr('disabled', false);
                $('#clientDO').attr('disabled', false);
                $('#timeSelect').attr('disabled', false);
                $('#fromdate').attr('disabled', false);
                $('#todate').attr('disabled', false);
    }

});

$('#fromdate,#todate,#muestras,#DepRegFil,#ArrRegFil,#clientsDO,#TDO, #timeSelect').on('change keyup', function(){
    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    muestras = $('#muestras').val();
    timeSelect = $('#timeSelect').val();
    muestratext = $( "#muestras option:selected" ).text();
    DepRegfil = $('#DepRegFil').val();
    ArrRegfil = $('#ArrRegFil').val();
    clientsDO = $('#clientsDO').val();
    TDO = $('#TDO').val();

    if(fromdate || todate || muestratext != "200 (default)" || DepRegfil || ArrRegfil || clientsDO || TDO|| timeSelect){
        $('#buscafiltro').attr('disabled', false);
    }else{
        $('#buscafiltro').attr('disabled', true);
    }

});

var botonglobal = 0;
$('#buscafiltro, #borrarFiltro, #cer').on('click', function () {
    boton = $(this).val();
    if(boton == 1 || boton == 0)
    botonglobal = boton;
    if(boton != 3){
        FDO = $('#FilterDO').val(); //FilterDO
        fromdate = $('#fromdate').val();
        todate = $('#todate').val();
        muestras = $('#muestras').val();
        DepRegfil = $('#DepRegFil').val();
        ArrRegfil = $('#ArrRegFil').val();
        clientsDO = $('#clientsDO').val();
        TDO = $('#TDO').val();

        var applyFilter = $.ajax({
            type: "POST",
            url: "bd/assignFilters.php",
            data: {
                boton       : boton,
                fromdate    : fromdate,
                todate      : todate,
                muestras    : muestras,
                DepRegFil   : DepRegfil,
                ArrRegFil   : ArrRegfil,
                clientsDO   : clientsDO,
                typeDO         : TDO,
                FDO : FDO
            }
        })
        applyFilter.done(function(data){
            console.log(data);
            tablaDO.ajax.reload(function(){

            if(boton == 1){
                $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Filtros asignados con éxito</div>')
                setTimeout(function() {
                    $('#alert1').fadeOut(1000);
                    $('#filtrarmodal').modal('hide');
                },1000);
            }
            else{
                // Habilitar botones que pueden estar deshabilitados
                $('#DepRegFil').attr('disabled', false);
                $('#ArrRegFil').attr('disabled', false);
                $('#muestras').attr('disabled', false);
                $('#TDO').attr('disabled', false);
                $('#clientDO').attr('disabled', true);
                $('#timeSelect').attr('disabled', false);
                $('#fromdate').attr('disabled', false);
                $('#todate').attr('disabled', false);
                
                choices.removeActiveItems();
                choices2.removeActiveItems();

                $('#FilterDO').val(""); //FilterDO
                $('#todate').val("");
                $('#fromdate').val("");
                $('#muestras').prop('selectedIndex',0);
                $('#DepRegFil').prop('selectedIndex',0);
                $('#ArrRegFil').prop('selectedIndex',0);
                $('#timeSelect').prop('selectedIndex',0);
                $('#TDO').prop('selectedIndex',0);
                $('#clientsDO').prop('selectedIndex',0);
               
                $('#buscafiltro').prop('disabled', true);
                $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"> <i class="bi bi-trash-fill"></i> Filtros borrados con éxito</div>')
                setTimeout(function() {
                    $('#alert1').fadeOut(1500);
                },1500);
            }

        });
        $('#aviso').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');      
    });

    }else{
        if(botonglobal == 0){
            $('#DepRegFil').attr('disabled', false);
            $('#ArrRegFil').attr('disabled', false);
            $('#muestras').attr('disabled', false);
            $('#TDO').attr('disabled', false);
            $('#clientsDO').attr('disabled', true);
            $('#timeSelect').attr('disabled', false);
            $('#fromdate').attr('disabled', false);
            $('#todate').attr('disabled', false);
            
             choices.removeActiveItems();
             choices2.removeActiveItems();
             
            $('#muestras').prop('selectedIndex',0);
            $('#DepRegFil').prop('selectedIndex',0);
            $('#ArrRegFil').prop('selectedIndex',0);
            $('#clientsDO').prop('selectedIndex',0);

            $('#TDO').prop('selectedIndex',0);
            $('#timeSelect').prop('selectedIndex',0);
           
            $('#FilterDO').val(""); //FilterDO
            $('#fromdate').val("");
            $('#todate').val("");
            
            $('#buscafiltro').prop('disabled', true);
        }
    }

});

});


//Aciones de correo para enviar listados
$(document).ready(function(){

    $(document).on("click", ".btnMail", function () {
        
        var fila = $(this);
        var numDO = $(this).closest('tr').find('td:eq(3)').text();
        var GinDO = $(this).closest('tr').find('td:eq(5)').text();
        var RegDO = $(this).closest('tr').find('td:eq(6)').text();
        var QtyDO = $(this).closest('tr').find('td:eq(12)').text();
        var CliDo = $(this).closest('tr').find('td:eq(7)').text();
        var Cli = $(this).closest('tr').find('td:eq(9)').text();
        var usuario = $('#dropdownMenuLink2').text();
        var observ = $(this).closest('tr').find('td:eq(19)').text();//Observaciones
        
        $("#correoList").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $('#CorreoList').modal('show');
        $('#sendEmailList').prop('disabled', false);
        $.ajax({
            type: 'POST',
            url: 'bd/getCorreoCMS.php',
            datatype: 'json',
            data: {
                numDO: numDO,
                QtyDO: QtyDO,
                GinDO: GinDO,
                RegDO: RegDO
            }
            }) .done(function(data){
                    datos = JSON.parse(data);

                    if (CliDo == 'CLIENTE'){
                        nuevo =  ' - '+ CliDo+' - '+ Cli;
                    }else{
                        nuevo = ' - '+ CliDo;
                    }
                    
                    loc = datos['location'];
                    origenes = [653, 656, 650, 670, 654, 613, 652, 630, 99002];
                    //BANDERA PARA VALIDAR SI ESTA EN UNA BODEGA DE UN ORIGEN
                    IsWHOrigin = datos['IsWHOrigin'];
                    if(jQuery.inArray(loc, origenes) == -1 && IsWHOrigin==0){
                        mails = "";
                        alert ("This lot is no longer in origin");

                        $(".modal-header").css( "background-color", "#17562c");
                        $(".modal-header").css( "color", "white" );
        
                        $('#tablahtmlList').val(datos['tabla']);
                        $('#tablamueList').html(datos['tabla']);
                        $('#extraEmailsList').val(mails);
                        $('#CCList').val(datos["emailsCC"]);
                        $('#subjectList').val(datos["asunto"]+ nuevo);
        
                        $('#usuarioList').val(usuario);
                        $('#typeDOList').val(typeDO);
                        $('#numDOList').val(numDO);
                        $('#salSucuList').val(salSucu);
                        $('#regOriList').val(datos["RegionOri"]);
                        $('#arrRegList').val(datos["validar"]);
                        $('#clientList').val(client);
                        $('#observList').val(observ);
                        $('#contraList').val(contra);
                        $('#totalQtyList').val(totalQty);
                        $('#CorreoList').modal('show');
        
                        if(datos['tabla']===""){
                            $('#lotess').text("Advertencia: No hay lotes asociados");
                        }else
                            $('#lotess').text("");
                        console.log(datos);

                    }else{
                    
                          $(".modal-header").css( "background-color", "#17562c");
                          $(".modal-header").css( "color", "white" );
          
                          $('#tablahtmlList').val(datos['tabla']);
                          $('#tablamueList').html(datos['tabla']);
                          $('#extraEmailsList').val(datos["emails"]);
                          $('#CCList').val(datos["emailsCC"]);
                          $('#subjectList').val(datos["asunto"]+ nuevo);
          
                          $('#usuarioList').val(usuario);
                          $('#typeDOList').val(typeDO);
                          $('#numDOList').val(numDO);
                          $('#salSucuList').val(salSucu);
                          $('#regOriList').val(datos["RegionOri"]);
                          $('#arrRegList').val(datos["validar"]);
                          $('#clientList').val(client);
                          $('#observList').val(observ);
                          $('#contraList').val(contra);
                          $('#totalQtyList').val(totalQty);
                          $('#CorreoList').modal('show');
          
                          if(datos['tabla']===""){
                              $('#lotess').text("Advertencia: No hay lotes asociados");
                          }else
                              $('#lotess').text("");
                          console.log(datos);
                    }
            });
     });


     $(document).on("click",".sendEmailList",function(){
        tablahtml = $('#tablahtmlList').val();
        emails    = $('#extraEmailsList').val();
        emailsCC  = $('#CCList').val();
        numDO     = $('#numDOList').val();
        subject   = $('#subjectList').val();
        usuario   = $('#usuarioList').val();
        typeDO    = $('#typeDOList').val();
        salSucu   = $('#salSucuList').val();
        regOri    = $('#regOriList').val();
        arrReg    = $('#arrRegList').val();
        client    = $('#clientList').val();
        observ    = $('#observList').val();
        contra    = $('#contraList').val();
        totalQty  = $('#totalQtyList').val();
        esMuestra = 0;

        if (arrReg === "incompleto"){
            alert("This DO is incomplete");
            $('#sendEmailList').prop('disabled', true);
        }else{    

        if(tablahtml===""){
            if(confirm('There is no associated lot. Do you want to submit the form?')){
                $.ajax({
                    type:"POST",
                    url: 'bd/correoCMS.php',
                    data: {
                        usuario: usuario,
                        typeDO: typeDO, 
                        numDO: numDO, 
                        salSucu: salSucu, 
                        regOri: regOri, 
                        arrReg: arrReg, 
                        client: client, 
                        observ: observ, 
                        contra: contra, 
                        totalQty:totalQty,
                        esMuestra:esMuestra,
                        subject:subject,
                        emails:emails,
                        emailsCC:emailsCC,
                        tablahtml:tablahtml}
                }) .done(function(ma){
                    alert(ma);
                    $('#CorreoList').modal('hide');
                }) .fail(function(){
                    alert("error");
                })
            }
        }else{
            $.ajax({
                type:"POST",
                url: 'bd/correoCMS.php',
                data: {usuario: usuario,
                    //typeDO: typeDO, 
                    numDO: numDO, 
                    //salSucu: salSucu, 
                    regOri: regOri, 
                    arrReg: arrReg, 
                    //client: client, 
                    observ: observ, 
                    //contra: contra, 
                    //totalQty:totalQty,
                    esMuestra:esMuestra,
                    subject:subject,
                    emails:emails,
                    emailsCC:emailsCC,
                    tablahtml:tablahtml}
            }) .done(function(ma){
                alert(ma);
                $('#CorreoList').modal('hide');
            }) .fail(function(){
                alert("Message sent successfully");
                $('#CorreoList').modal('hide');
            })
        }
    }
        
     });
});

//inicializar multiselect

/*
var choices;
var choices2;
$(document).ready(function(){
    var  choices = new Choices('#DepRegFil', {
       removeItemButton: true,
       maxItemCount:10
     });     
});
$(document).ready(function(){
    var choices2  = new Choices('#ArrRegFil', {
       removeItemButton: true,
       maxItemCount:10
     });     
});

*/

//clear multiselect 


//VALIDAR SI REQUIERE CERTIFICADO DE ORIGEN 
function certificado(){
    InReg    = $('#InReg').val();
    InPlc = $('#InPlc').val();
    opcion = 17;
    Typ = $('#Typ').val();
    OutPlc = $('#OutPlc').val();
    bandera = 0;

    if(OutPlc == 'ASCENSION' || OutPlc == 'OASIS' || OutPlc == 'JUAREZ' ){
        bandera = 1;
    }
    else{
        bandera = 0;
    }


    if (InReg == 'CLIENTE' && Typ =='DOM' && InPlc !=null && OutPlc!=null && bandera == 0){
        $.ajax({
            type: 'POST',
            url: 'bd/crudDO.php',
            datatype: 'json',
            data: {
                InPlc: InPlc,
                OutPlc:OutPlc,
                opcion:opcion            
            },
            success: function(data) {       
                opts = JSON.parse(data);
               // console.log(opts[0]['Certificate'])
                if(opts[0]['Certificate'] == 1 && opts[0]['IsOrigin'] == 1){        
                    $("#certificado").prop("checked", true);       
                }
                else{
                    $("#certificado").prop("checked", false);
                } 
            }  


            })

    }

    else{
        $("#certificado").prop("checked", false);
    }

}

function validadocert (DOrd){
    opcion = 18;
    var lostesenDO
    if (DOrd != 0){
        $.ajax({
            type: 'POST',
            url: 'bd/crudDO.php',
            datatype: 'json',
            data: {           
                DOrd:DOrd,
                opcion:opcion            
            },
            success: function(data) {       
                opts = JSON.parse(data);
                lostesenDO = opts[0]['LotesDO']
                
            }, async: false   


        })

        return lostesenDO;
    }
    else{
        lostesenDO = 0
        return lostesenDO
    }
}
