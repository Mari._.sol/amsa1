<?php

include_once 'bd/conexion.php';
require __DIR__.'/vendor/autoload.php';
date_default_timezone_set("America/Mexico_City");

use Aws\S3\S3Client; 
use Aws\Exception\AwsException; 

$objeto = new Conexion();
$conexion = $objeto->Conectar();
$LiqID = $_POST['LiqID'];
$fechaload = date('Y-m-d h:i:s a', time());  
$bucket = 'pruebasportal'; // bucket de pruebas
//$bucket = 'portal-liq ';
// Datos del archivo PDF
$ticketPdf = $_FILES['filePdf']['name']; // Ahora diferencio los archivos por sus nombres en el formulario
$extPdf = $_POST['ExtPdf'];
$newfilenamePdf = $LiqID.'.'.$extPdf;

// Datos del archivo XML
$ticketXml = $_FILES['fileXml']['name']; // Archivo XML
$extXml = $_POST['ExtXml'];
$newfilenameXml = $LiqID.'.'.$extXml;

// Consultar si ya hay archivos cargados 
$consulta = "SELECT fileFactura, fileXml FROM amsadb1.Liquidation WHERE LiqID='$LiqID'";  
$resultado = $conexion->prepare($consulta);
$resultado->execute();
$datos = $resultado->fetch();
$extInvPdf = $datos['fileFactura'];
$extInvXml = $datos['fileXml'];

$s3 = new S3Client([
    'version'     => 'latest',
    'region'      => 'us-east-2', //bucket de pruebas
    //'region'      => 'us-east-1',
    'credentials' => [
        'key'    => 'AKIAT442VUCJ4A2UNRMK',
        'secret' => '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',
    ],
]);


// 1. Borrar el archivo PDF si ya existe
if($extInvPdf != "0" ){
    try {
        $result = $s3->deleteObject([
            'Bucket' => $bucket,
            'Key'    => "Fac-".$LiqID.".".$extInvPdf
        ]);

        if ($result['DeleteMarker']) {
            $data = "actualizado";
        } else {
            $data = "Error al actualizar el PDF";
        }
    } catch (S3Exception $e) {
        exit('Error: ' . $e->getAwsErrorMessage() . PHP_EOL);
    }
}

// 2. Borrar el archivo XML si ya existe
if($extInvXml != "0" ){
    try {
        $result = $s3->deleteObject([
            'Bucket' => $bucket,
            'Key'    => "Fac-".$LiqID.".".$extInvXml
        ]);

        if ($result['DeleteMarker']) {
            $data = "actualizado";
        } else {
            $data = "Error al actualizar el XML";
        }
    } catch (S3Exception $e) {
        exit('Error: ' . $e->getAwsErrorMessage() . PHP_EOL);
    }
}

// 3. Subir el nuevo archivo PDF
try {
    if ($_FILES['filePdf']['tmp_name']) {
        $result = $s3->putObject([
            'Bucket' => $bucket,
            'Key'    => "Fac-".$LiqID.".".$extPdf,
            'SourceFile' => $_FILES['filePdf']['tmp_name']
        ]);

        $consulta = "UPDATE amsadb1.Liquidation SET fileFactura='$extPdf' WHERE LiqID='$LiqID'";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();    
    }

    // 4. Subir el nuevo archivo XML
    if ($_FILES['fileXml']['tmp_name']) {
        $result = $s3->putObject([
            'Bucket' => $bucket,
            'Key'    => "Fac-".$LiqID.".".$extXml,
            'SourceFile' => $_FILES['fileXml']['tmp_name']
        ]);

        $consulta = "UPDATE amsadb1.Liquidation SET fileXml='$extXml' WHERE LiqID='$LiqID'";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();    
    }

    // 5. Actualizar la fecha de carga
    $Hoy = date('Y-m-d');
    $consulta = "UPDATE amsadb1.Liquidation SET datInv='$Hoy' WHERE LiqID='$LiqID'";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();

} catch (S3Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}

// 6. Enviar respuesta al frontend
$data = [
    'pdf' => "Fac-".$LiqID.".".$extPdf,
    'xml' => "Fac-".$LiqID.".".$extXml
];

print json_encode($data, JSON_UNESCAPED_UNICODE); // Enviar respuesta en formato JSON a AJAX
?>