<?php
session_start();
if (!isset($_SESSION['username'])) :
    include_once('index.php');
    
else :
    $Typ_Lots = $_SESSION['Typ_Lots'];
    $permiso = $_SESSION['Admin'];
    $rates=$_SESSION['ViewRates'];
    
    $filtersSes = $_SESSION['Filters'];
    include_once('bd/correo-model.php');
    $regionesAll = getRegiones();
    $regiones = getRegionesO();
    $clientes = getClients();

    $_SESSION['Filters'] = array("muestras" => "0",
                                "CrpL" => "",
                                "RegL" => "",
                                "GinL" => "",
                                "LocL" => "",
								"DOL" => "",
                                "LotL" => "",
                                "CliL" => "",
								);

    foreach($regiones as $reg){
        $regsID[] = $reg['IDReg'];
        $regs[] = $reg['RegNam'];
    }

    foreach($regionesAll as $regAll){
        $regsIDAll[] = $regAll['IDReg'];
        $regsAll[] = $regAll['RegNam'];
    }

    foreach($clientes as $cli){
        $cliID[] = $cli['CliID'];
        $clis[] = $cli['Cli'];
    }
    
?>

    <!doctype html>
    <html lang="es en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv='cache-control' content='no-cache'>
        <meta http-equiv='expires' content='0'>
        <meta http-equiv='pragma' content='no-cache'>

        <link rel="shortcut icon" href="img/ecom.png" />
        <title>Lots</title>

        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="../portalLogistica/assets/bootstrap/css/bootstrap.min.css">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="../portalLogistica/main.css">
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>

    <body>
        <!-- Scripts -->

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>


        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <!-- librerias necesarias para finalizar sesion por inactividad -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
        <!-- Scrip para finalizar sesion por inactividad -->
        <script type="text/javascript" src="timer.js"></script>

        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>

        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>

        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>
        
        <!-- Multiselect en filter-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
        <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                Qty = parseFloat($.trim($('#QtyDv').val()));
                LiqWgh = parseFloat($.trim($('#LiqWghDv').val()));
                $("#QtyDv").keyup(function() {
                    var texto_escrito = parseInt($(this).val());
                    $("#LiqWghDv").val((texto_escrito * LiqWgh) / Qty);
                })
            })
        </script>
        <?php if ($Typ_Lots == 1) { ?>
            <script type="text/javascript" src="main.js"></script>
        <?php } else { ?>
            <script type="text/javascript" src="mainLR.js"></script>
        <?php } ?>
        

        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>

                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <li><a class="dropdown-item" href="trucks.php">Trucks</a></li>
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="#">Lots</a></li>
                        <?php if ($_SESSION['username'] == 'ISELA LARA' || $_SESSION['username'] == 'DARIO ROMERO' || $_SESSION['username'] == 'CESAR ARMAS' || $_SESSION['username'] == 'LUIS ANGEL RENDON TARIN' || $_SESSION['username'] == 'JOSE RENDON'){?>
                        <li><a class="dropdown-item" href="bales.php">Bales</a></li>
                        <?php } ?>
                        <li><a class="dropdown-item" href="do.php">Delivery Order</a></li>
                        <li><a class="dropdown-item" href="adjustments.php">Adjustments</a></li>
                        <li><a class="dropdown-item" href="inv.php">Invoice</a></li>
                        <li><a class="dropdown-item" href="export.php">Containers</a></li>
                        <li><a class="dropdown-item" href="clients.php">Clients</a></li>
                        <li><a class="dropdown-item" href="routes.php">Routes</a></li>
                        <li><a class="dropdown-item" href="requisition.php">Requisition</a></li>
                        <li><a class="dropdown-item" href="CMS.php">CMS</a></li>
                        <?php if ($permiso == 1){?>
                        <li><a class="dropdown-item" href="adminusers.php">User Management</a></li>
                        <li><a class="dropdown-item" href="adminproveed.php">Proveed Management</a></li>
                        <?php } ?>                        
                        <?php if ($rates == 1){?>
                        <li><a class="dropdown-item" href="Rates.php">Rates</a></li>
                        <?php } ?>
                        
                    </ul>
                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-2" href="main.php">
                    <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <p>/ Lots</p>
                </div>
                <div class="container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                    <!-- Botones -->
                    <?php if ($Typ_Lots == 1) : ?>
                        <button id="btnNuevo" type="button" class="btn btn-light " data-toggle="modal tooltip" data-placement="bottom" title="Add Lot"><i class="bi bi-plus-square"></i></button>
                        <button id="btnFile" type="button" class="btn btn-light " data-toggle="modal tooltip" data-placement="bottom" title="Import CSV file"><i class="bi bi-filetype-csv"></i></button>
                        <button id="btnAddInfo" type="button" class="btn btn-info " data-toggle="modal tooltip" data-placement="bottom" title="Add Inf"><i class="bi bi-info-square"></i></button>
                    <?php endif ?>
                </div>
                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                    <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"><?php echo $_SESSION['username']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->

        <!-- Aquí inicia todo código de tablas etc -->



        <!-- Inicio de la tabla -->
        <div class="card card-body">
                  <div class="input-group input-group mb-12">
                        <div id = SDO class="col-1">
                            <input type="checkbox" id="SinDO" value = 0 name="SinDO" class="groupSDO">
                            <label for="horns" style="color:white">No DO</label>
                        </div>
                        <div id = SDO class="col-1">
                            <input type="checkbox" id="ConDO" value = 0 name="ConDO" class="groupCDO">
                            <label for="horns" style="color:white">With DO</label>
                        </div>
                        <div id = SDO class="col-1">
                            <input type="checkbox" id="AllDO" value = 0 name="AllDO" class="groupAllDO" checked>
                            <label for="horns" style="color:white">All</label>
                        </div>
                    </div>
                    <div class="input-group input-group mb-3">
                        <div id = SCli class="col-1">
                            <input type="checkbox" id="SinCli" value = "" name="SinCli" class="groupSC">
                            <label for="horns" style="color:white">Unassigned</label>
                        </div>
                        <div id = SCli class="col-1">
                            <input type="checkbox" id="ConCli" value = "" name="ConCli" class="groupCC">
                            <label for="horns" style="color:white">Assigned</label>
                        </div>
                        <div id = SCli class="col-1">
                            <input type="checkbox" id="AllCli" value = "" name="AllCli" class="groupAllC" checked>
                            <label for="horns" style="color:white">All</label>
                        </div>
                        
                        <div id = SDO class="col-12">
                            <div class="d-flex justify-content-end">
                <div class="col-xs-12">
                <div class="input-group mb-1">
                <label class="input-group-text" >Total Lots</label>
                <label class="input-group-text"  id="totallotes" >0</label>
                <label class="input-group-text" >Total Bales</label>
                <label class="input-group-text"  id="monto" >0</label>
                <label class="input-group-text" >Overall Weight</label>
                <label class="input-group-text"  id="totalpeso" >0</label>
                </div>
                </div>
              </div>
                        </div>
                        
                    </div>
             <!-- Resumen de busqueda
              <div class="d-flex justify-content-end">
                <div class="col-xs-3">
                <div class="input-group mb-1">
                <label class="input-group-text" >Total Lots</label>
                <label class="input-group-text"  id="totallotes" >0</label>
                <label class="input-group-text" >Total Bales</label>
                <label class="input-group-text"  id="monto" >0</label>
                <label class="input-group-text" >Overall Weight</label>
                <label class="input-group-text"  id="totalpeso" >0</label>
                </div>
                </div>
              </div> -->
            <div class="table-responsive">
                <table id="tablaUsuarios" class="table bg-white table-striped row-border order-column table-hover">
                    <thead style="background-color: #65ac7c;">
                        <tr>
                            <th class="th-sm">Lot ID</th>
                            <th class="th-sm">Crop</th> 
                            <th class="th-sm">Lot</th>
                            <th class="th-sm">Quantity</th>
                            <th class="th-sm">Region</th>
                            <th class="th-sm">Gin Name</th>
                            <th class="th-sm">Location</th>
                            <th class="th-sm">Delivery Order</th>
                            <th class="th-sm">Scheduled Arrival Date</th>
                            <th class="th-sm">Arrival Region</th>
                            <th class="th-sm">Client</th>
                            <th class="th-sm">Purchase Weight</th>
                            <th class="th-sm">Quality</th>
                            <th class="th-sm">Comments</th>
                            <th class="th-sm">Recap Code</th>
                            <th class="th-sm">Assigned To</th>
                            <th class="th-sm">Assignment Date</th>
                            <th class="th-sm">TruckID</th>
                            <!--<th class="th-sm">InvID</th>-->
                            <th class="th-sm">Status</th>
                            <th class="th-sm">Pacas Certificadas</th>
                            <th class="th-sm">Cantidad Original</th>
                            <th class="no-exportar"></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="th-sm">Lot ID</th>
                            <th class="th-sm">Crop</th> 
                            <th class="th-sm">Lot</th>
                            <th class="th-sm">Quantity</th>
                            <th class="th-sm">Region</th>
                            <th class="th-sm">Gin Name</th>
                            <th class="th-sm">Location</th>
                            <th class="th-sm">Delivery Order</th>
                            <th class="th-sm">Scheduled Arrival Date</th>
                            <th class="th-sm">Arrival Region</th>
                            <th class="th-sm">Client</th>
                            <th class="th-sm">Purchase Weight</th>
                            <th class="th-sm">Quality</th>
                            <th class="th-sm">Comments</th>
                            <th class="th-sm">Recap Code</th>
                            <th class="th-sm">Assigned To</th>
                            <th class="th-sm">Assignment Date</th>
                            <th class="th-sm">TruckID</th>
                            <!--<th class="th-sm">InvID</th>-->
                            <th class="th-sm">Status</th>
                            <th class="th-sm">Pacas Certificadas</th>
                            <th class="th-sm">Cantidad Original</th>
                            <th class="th-sm"></th>
                        </tr>
                    </tfoot>
                </table>
            </div>

        </div>

        <!--Modal para CRUD-->
        <div class="modal fade" id="modalCRUD" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formUsuarios">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Crop<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="Crop" name="Crop" required>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                        </select>
                                    </div>
                            </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Lote<font size=2>*</font></label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="Lot" required>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Quantity<font size=2>*</font></label>
                                        <input type="text" class="form-control form-control-sm" id="Qty" pattern="[0-9]+" required>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Quality</label>
                                        <input type="text" class="form-control form-control-sm" id="Qlty">
                                    </div>
                                </div>
                             
                            </div>
                            <!--segunda fila -->

                            <div class="row">
                            <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Purchase Weight</label>
                                        <input type="text" class="form-control form-control-sm" id="LiqWgh">
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Region<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="Reg" required></select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Gin Name<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="GinID" required></select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Location<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="Loc" required></select>
                                    </div>
                                </div>
                                
                            </div>
                             <!--tercera fila -->
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Assigned To</label>
                                        <select class="form-control form-control-sm" id="Cli"></select>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label-sm">Scheduled Arrivla Date</label>
                                        <input type="date" class="form-control form-control-sm" id="SchDate" onkeydown="return false">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Assignment Date</label>
                                        <input type="date" class="form-control form-control-sm" id="AsgmDate" onkeydown="return false">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Recap Code</label>
                                        <input type="text" class="form-control form-control-sm" id="Recap">
                                    </div>
                                </div>
                                
                            </div>
                            <div id="InfDO" style="display: none">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label fro="" class="col-form-label">DO Quantity</label>
                                            <input type="text" class="form-control form-control-sm" id="DOQty" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label fro="" class="col-form-label">Type</label>
                                            <input type="text" class="form-control form-control-sm" id="DOType" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label fro="" class="col-form-label">Gin</label>
                                            <input type="text" class="form-control form-control-sm" id="DOGin" readonly>
                                        </div>
                                    </div>
                                </div>
                                 <!--fila no mostrada -->
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label fro="" class="col-form-label">Departure Region</label>
                                            <input type="text" class="form-control form-control-sm" id="DOOutReg" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label fro="" class="col-form-label">Arrival Region</label>
                                            <input type="text" class="form-control form-control-sm" id="DOInReg" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label fro="" class="col-form-label">Client</label>
                                            <input type="text" class="form-control form-control-sm" id="DOCli" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <!--Cuarta Fila -->
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label fro="" class="col-form-label">Delivery Order</label>
                                        <input type="text" class="form-control form-control-sm" id="DOrd">
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Status<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="Status" name="Status" required>
                                            <option value="ENABLE">ENABLE</option>
                                            <option value="DISABLE">DISABLE</option>
                                            <option value="REJECTED">REJECTED</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Comments</label>
                                        <input type="text" class="form-control form-control-sm" id="Cmt">
                                    </div>
                                </div>
                                    <!--Cuarta Fila 
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Crop<font size=2>*</font></label>
                                        <input type="text" class="form-control form-control-sm" id="Crop" required>
                                    </div>
                                </div>-->

                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group" >
                                        <label for="" class="col-form-label">Pacas Certificadas</label>
                                        <br>
                                        <input class="form-check-input" type="checkbox" id="pacascertificadas" >
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Total Quantity<font size=2>*</font></label>
                                        <input type="text" class="form-control form-control-sm" id="cantidadtotal" pattern="[0-9]+" required>
                                    </div>
                                </div>
                            </div>
                        <font size=2>*Required Fields</font>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" id="btnGuardar" class="btn btn-dark">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--Modal devoluciones-->
        <div class="modal fade" id="modalFile" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formFile">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="" class="col-form-label"></label>
                                        <input type="file" class="form-control" id="csv" name="csv">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" for="DepRegFil">Location</label>
                                        <select class="form-select me-2" name="Reg" id="LocLot">
                                            <option value="">Choose...</option>
                                            <?php $i = 0; while($i < count($regsAll)): ?>
                                            <option value="<?php echo $regsIDAll[$i]; ?>"><?php echo $regsAll[$i]; ?></option>
                                            <?php $i++; endwhile; ?>

                                                                        
                                        </select>
                                    </div>
                                </div>
                            </div>
       


                        </div>
                        <div class="modal-footer">
                            <div style="height: auto;" id="aviso"></div>
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" id="btnGuardarFile" class="btn btn-dark">Import</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <!--Modal informacion de camiones-->
        <div class="modal fade" data-bs-backdrop="static" id="modalInf" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form id="Inf">
                        <div class="modal-body">

                        </div>
                    </form>
                    <div class="modal-footer">
                        <!--<button type="submit" id="btnClose" class="btn btn-light">Close</button>-->
                        <!--<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>-->
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal filtros bales -->
        <div class="modal hide fade" id="filtrarmodal" data-bs-backdrop="static" tabindex="-1" role="dialog" data-bs-keyboard="false" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-tittle">Filter table by</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="filtros" class="filtros">
                            <div class="input-group mb-3">
                                <div class="input-group mb-3">
                                    <label class="input-group-text" for="DepRegFil">Region</label>
                                    <div class='col-lg-9'>
                                        <select class="form-select me-2" name="RegL" id="RegL" multiple placeholder="Choose...">
                                            <?php $i = 0; while($i < count($regs)): ?>
                                                <option value="<?php echo $regsID[$i]; ?>"><?php echo $regs[$i]; ?></option>
                                            <?php $i++; endwhile; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <label class="input-group-text" for="Gin">Planta</label>
                                    <div class='col-lg-4'>
                                        <select class="form-select me-2" id="GinL" name="GinL">    
                                        </select>
                                    </div>
                                </div>    
                                <div class="input-group mb-3">
                                    <label class="input-group-text" for="Location">Location</label>
                                    <div class='col-lg-9'>
                                        <select class="form-select me-2" id="LocL" name="LocL" multiple placeholder="Choose...">
                                            <?php $i = 0; while($i < count($regsAll)): ?>
                                                <option value="<?php echo $regsIDAll[$i]; ?>"><?php echo $regsAll[$i]; ?></option>
                                            <?php $i++; endwhile; ?>   
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group mb-3">
                                <label class="input-group-text" for="idDO">DO</label>
                                <input  class="form-control me-2" type="text" name="DO" id="DOL" placeholder="Write Delivery Order" >
                                <label class="input-group-text" for="idDO">Lot</label>
                                <input  class="form-control me-2" type="text" name="Lot" id="LotL" placeholder="Write Lots" >
    
                            </div>

                            <div class="input-group mb-3">
                                <label class="input-group-text" for="timeSelect">Client</label>
                                    <select class="form-select me-2" id="CliL" name="CliL">
                                    <option value="">Choose...</option>
                                    <?php $i = 0; while($i < count($clis)): ?>
                                    <option value="<?php echo $cliID[$i]; ?>"><?php echo $clis[$i]; ?></option>
                                    <?php $i++; endwhile; ?>
                                    </select>
                                
                                <label class="input-group-text" for="timeSelect">Crop</label>
                                <select class="form-select me-2" id="CrpL" name="CrpL">
                                    <option value="" >All...</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                    <option value="2024" selected>2024</option>
                                </select>
                            </div>

                            <!--<div class="input-group input-group mb-3">
                                <div class="col-2">
                                <input type="checkbox" id="SinDO" name="SinDO" class="group1">
                                <label for="horns">No Delivery Order</label>
                                </div>
                                <div class="col-2">
                                <input type="checkbox" id="SinCli" name="SinCli" class="group1">
                                <label for="horns">Unassigned</label>
                                </div>
                            </div>-->

                        </form>
                    </div>
                    <div class="modal-footer">
                        <div style="height: 2.5em;" id="aviso"></div>
                    <button id="cer" type="button" class="btn btn-light" value="3" data-bs-dismiss="modal" >Close</button>
                        <button id="borrarFiltro" type="button" value="0" class="btn btn-danger Filtro" >Clear filters</button>
                        <button id="buscafiltro" type="button" value="1" class="btn btn-primary Filtro" >Apply filters</button>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- --------------- Modal para agregar clientes, comentarios, scheduled arrival date de manera masiva --------------- -->
        <div class="modal hide fade" id="modalAdd" data-bs-backdrop="static" tabindex="-1" role="dialog" data-bs-keyboard="false" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-tittle">Add Information</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="InfAdd">
                            <div class="row">
                                
                                <div class='col-lg-8'>
                                    <br>
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" for="DepRegFil">Region*</label>
                                        <select class="form-select me-2" name="RegInf" id="RegInf">
                                            <option value="">Choose...</option>
                                            <?php $i = 0; while($i < count($regs)): ?>
                                            <option value="<?php echo $regsID[$i]; ?>"><?php echo $regs[$i]; ?></option>
                                            <?php $i++; endwhile; ?>
                                        </select>
                                    </div>
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" for="Gin">Planta</label>
                                        <select class="form-select me-2" id="GinInf" name="GinInf" >    
                                        </select>
                                    </div>
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" for="Location">Location*</label>
                                        <select class="form-select me-2" id="LocInf" name="LocInf">
                                        <option value="">Choose...</option>
                                            <?php $i = 0; while($i < count($regsAll)): ?>
                                            <option value="<?php echo $regsIDAll[$i]; ?>"><?php echo $regsAll[$i]; ?></option>
                                            <?php $i++; endwhile; ?>   
                                        </select>
                                        <button id="btnBuscar" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Add Inf"><i class="bi bi-search"></i></button>
                                    </div>
                                    <div class="strike">
                                        <span>Information to associate</span>
                                        <br><br>
                                    </div>
                                    <div class="input-group mb-3" class="right">
                                            <label class="input-group-text" for="Gin">Cliente</label>
                                            <select class="form-select me-2" id="CliInf" name="CliInf">
                                            <option value="">Choose...</option>
                                            <?php $i = 0; while($i < count($clis)): ?>
                                            <option value="<?php echo $cliID[$i]; ?>"><?php echo $clis[$i]; ?></option>
                                            <?php $i++; endwhile; ?>
                                            </select>
                                    </div>
                                    <div class="input-group mb-3" class="right">
                                            <label for="" class="input-group-text">Scheduled Arrivla Date</label>
                                            <input type="date" class="form-select me-2" id="SchDateInf" onkeydown="return false">
                                    </div>
                                    <div class="input-group mb-3">
                                        <label for="" class="input-group-text">Comments</label>
                                        <input type="text" class="form-select me-2" style="text-transform:uppercase;" id="CmtInf">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label fro="" class="col-form-label">Lots</label>
                                        <select class="form-control form-control-sm" name="Lotes[]" multiple="multiple" id="Lotes" size="19"></select>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <font size=2>*Required Fields</font>
                    </div>
                    <div class="modal-footer">
                        <div style="height: 2.5em;" id="avisoInf"></div>
                        <button id="btnAddLote" type="button" class="btn btn-dark" data-toggle="modal tooltip" data-placement="bottom" title="Add">Save</i></button>
                        <!--<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
            </div>
        </div>

        <!-- Aquí termina todo código de tablas etc -->
    </body>

    </html>
<?php
endif;
?>
