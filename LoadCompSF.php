<?php

include_once 'bd/conexion.php';
require __DIR__.'/vendor/autoload.php';
date_default_timezone_set("America/Mexico_City");

use Aws\S3\S3Client; 
use Aws\Exception\AwsException; 

$objeto = new Conexion();
$conexion = $objeto->Conectar();
$fechaload = date('Y-m-d', time());  

$bucket = 'pruebasportal'; // bucket de pruebas
//$bucket = 'portal-liq';

// Datos del archivo 
$_FILES['CSF']['name'];
$extPdf = $_POST['ExtCSF'];


// Consultar si ya hay archivos cargados 
$consulta = "SELECT DatCSF, ExtCSF FROM amsadb1.ConfGen where View = 'ViewLiq' and IdConfig =1";  
$resultado = $conexion->prepare($consulta);
$resultado->execute();
$datos = $resultado->fetch();
$DatCSF = $datos['DatCSF'];
$ExtCSF = $datos['ExtCSF'];

$s3 = new S3Client([
    'version'     => 'latest',
    'region'      => 'us-east-2', //bucket de pruebas
    //'region'      => 'us-east-1',
    'credentials' => [
        'key'    => 'AKIAT442VUCJQXPEFVZW',
        'secret' => '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',
    ],
]);


// 1. Borrar el archivo PDF si ya existe
if($ExtCSF != "0" ){
    try {
        $result = $s3->deleteObject([
            'Bucket' => $bucket,
            'Key'    => "constancia_Fiscal.".$extPdf
        ]);

        if ($result['DeleteMarker']) {
            $data = "actualizado";
        } else {
            $data = "Error al actualizar el PDF";
        }
    } catch (S3Exception $e) {
        exit('Error: ' . $e->getAwsErrorMessage() . PHP_EOL);
    }
}

// 3. Subir el nuevo archivo PDF
try {
    if ($_FILES['CSF']['tmp_name']) {
        $result = $s3->putObject([
            'Bucket' => $bucket,
            'Key'    => "constancia_Fiscal.".$extPdf,
            'SourceFile' => $_FILES['CSF']['tmp_name']
        ]);

        $consulta = "UPDATE amsadb1.ConfGen SET ExtCSF='$extPdf'  where View = 'ViewLiq' and IdConfig =1";		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();    
    }
 
    // 5. Actualizar la fecha de carga
    $Hoy = date('Y-m-d');
    $consulta = "UPDATE amsadb1.ConfGen SET DatCSF='$Hoy' where View = 'ViewLiq' and IdConfig =1 ";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();

} catch (S3Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}

// 6. Enviar respuesta al frontend
$data = [
    'conF' => "constancia_Fiscal.".$extPdf
];

print json_encode($data, JSON_UNESCAPED_UNICODE); // Enviar respuesta en formato JSON a AJAX
?>