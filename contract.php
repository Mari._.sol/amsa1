<?php
session_start();
if (!isset($_SESSION['username'])) :
    include_once('index.php');
else :
    $Typ_Cli = $_SESSION['Typ_Cli'];
    $permiso = $_SESSION['Admin'];
?>
<!doctype html>
<html lang="es en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="img/ecom.png" />
        <title>Contract Request</title>

        <!-- CSS bootstrap -->
        <link rel="stylesheet" href=".//assets/bootstrap/css/bootstrap.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="./main.css">
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>
    <body>
        <!-- Scripts -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>

        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>

        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>

        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>

        <?php if ($Typ_Cli == 1) { ?>
            <script type="text/javascript" src="contract.js"></script>
        <?php } else { ?>
            <script type="text/javascript" src="contract.js"></script>
        <?php } ?>
        <!-- Terminan Scripts -->

        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>

                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <li><a class="dropdown-item" href="trucks.php">Trucks</a></li>
                        <li><a class="dropdown-item" href="lots.php">Lots</a></li>
                        <?php if ($_SESSION['username'] == 'ISELA LARA' || $_SESSION['username'] == 'DARIO ROMERO' || $_SESSION['username'] == 'CESAR ARMAS' || $_SESSION['username'] == 'LUIS ANGEL RENDON TARIN'){?>
                        <li><a class="dropdown-item" href="bales.php">Bales</a></li>
                        <?php } ?>
                        <li><a class="dropdown-item" href="do.php">Delivery Order</a></li>
                        <li><a class="dropdown-item" href="adjustments.php">Adjustments</a></li>
                        <li><a class="dropdown-item" href="inv.php">Invoice</a></li>
                        <li><a class="dropdown-item" href="export.php">Exports</a></li>
                        <li><a class="dropdown-item" href="clients.php">Clients</a></li>
                        <li><a class="dropdown-item" href="routes.php">Routes</a></li>
                        <li><a class="dropdown-item" href="requisition.php">Requisition</a></li>
                        <li><a class="dropdown-item" href="CMS.php">CMS</a></li>
                        <?php if ($permiso == 1){?>
                        <li><a class="dropdown-item" href="adminusers.php">User Management</a></li>
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="#" >Contract</a></li>
                        <?php } ?>
                    </ul>
                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-2" href="main.php">
                    <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <p>/ Contract</p>
                </div>
                <div class="container-fluid container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                    <!-- Botones -->
                    <?php if ($Typ_Cli == 1) : ?>
                        <button id="btnNueva" type="button" class="btn btn-success" data-toggle="modal tooltip" data-placement="bottom" title="Add Contract"><i class="bi bi-plus-square"></i></button>
                    <?php endif ?>
                </div>
                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                    <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"><?php echo $_SESSION['username']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Aquí inicia todo código de tablas etc -->
        <!-- Inicio de la tabla -->
        <div class="card card-body" style="opacity:100%;">
            <div class="table-responsive" style="opacity:100%;">
                <table id="tablaContract" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;">
                    <thead style="background-color: #65ac7c;" style="opacity:100%;">
                        <tr>
                            <th class="th-sm">Id Contract</th>
                            <th class="th-sm">Solicita</th>
                            <th class="th-sm">Fecha</th>
                            <th class="th-sm">Cliente</th>
                            <th class="th-sm">Sub Cliente</th>
                            <th class="th-sm">Lugar de entrega</th>
			                <th class="th-sm">Periodo De</th>
                            <th class="th-sm">Al</th>
                            <th class="th-sm">Incoterm</th>   
                            <th class="th-sm">Cantidad</th>
                            <th class="th-sm">Cosecha</th>   
                            <th class="th-sm">Mes cobertura</th>
                            <th class="th-sm">Region</th>
                            <th class="th-sm">Destino</th>
                            <th class="th-sm">Grado</th>
                            <th class="th-sm">Fibra</th>
                            <th class="th-sm">Micronara</th>
                            <th class="th-sm">Resistencia</th>
                            <th class="th-sm">Uniformidad</th>
                            <th class="th-sm">Fijo/On call</th>
                            <th class="th-sm">Precio NY</th>
                            <th class="th-sm">Puntos</th>
                            <th class="th-sm">Precio final</th>
                            <th class="th-sm">Unidad de medida</th>
                            <th class="th-sm">Condiciones de Pago</th>
                            <th class="th-sm">Intereses Normal</th>
                            <th class="th-sm">Intereses Moral</th>
                            <th class="th-sm">Status</th>
                            <th class="th-sm">Comentario</th>
                            <th class="th-sm">Nota</th>
                            <th class="th-sm">Aprueba</th>
                            <?php if ($Typ_Cli == 1) { ?>
                                <th class="no-exportar"></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="th-sm">Id Contract</th>
                            <th class="th-sm">Solicita</th>
                            <th class="th-sm">Fecha</th>
                            <th class="th-sm">Cliente</th>
                            <th class="th-sm">Sub Cliente</th>
                            <th class="th-sm">Lugar de entrega</th>
			                <th class="th-sm">Periodo De</th>
                            <th class="th-sm">Al</th>
                            <th class="th-sm">Incoterm</th>   
                            <th class="th-sm">Cantidad</th>
                            <th class="th-sm">Cosecha</th>   
                            <th class="th-sm">Mes cobertura</th>
                            <th class="th-sm">Region</th>
                            <th class="th-sm">Destino</th>
                            <th class="th-sm">Grado</th>
                            <th class="th-sm">Fibra</th>
                            <th class="th-sm">Micronara</th>
                            <th class="th-sm">Resistencia</th>
                            <th class="th-sm">Uniformidad</th>
                            <th class="th-sm">Fijo/On call</th>
                            <th class="th-sm">Precio NY</th>
                            <th class="th-sm">Puntos</th>
                            <th class="th-sm">Precio final</th>
                            <th class="th-sm">Unidad de medida</th>
                            <th class="th-sm">Condiciones de Pago</th>
                            <th class="th-sm">Intereses Normal</th>
                            <th class="th-sm">Intereses Moral</th>
                            <th class="th-sm">Status</th>
                            <th class="th-sm">Comentario</th>
                            <th class="th-sm">Nota</th>
                            <th class="th-sm">Aprueba</th>
                            <?php if ($Typ_Cli == 1) { ?>
                                <th class="no-exportar"></th>
                            <?php } ?>
                        </tr>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!--Modal para CRUD-->
        <div class="modal fade in" data-bs-backdrop="static" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formContract">
                        <div class="modal-body">
                            <div class="">
                                <div class="row">
                                    <div class="col-lg-6"></div> 
                                    <div class="col-lg-6">
                                        <div class="input-group mt-1">
                                            <!-- <label class="input-group-text" for="subject">Asunto</label> -->
                                            <input type="hidden" class="form-control subject" id="subject" value="Solicitud de aprobación de venta pacas de algodón" readonly>
                                        </div>
                                    </div>
                                </div>
                                <!-- <br><br> -->
                                <div class="row ">
                                    <div class="col-lg-5">
                                        <div class="input-group">
                                            <label for="" class="input-group-text">Solicita:</label>
                                            <input type="text" class="form-control form-control-sm" style="" id="solicita">
                                        </div>
                                    </div>
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-3">
                                        <div class="input-group">
                                            <label for="" class="input-group-text">Fecha:</label>
                                            <input type="date" class="form-control form-control-sm noS" id="fecha" disabled>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-group">
                                            <label class="input-group-text" for="asLots">No. Solicitud:</label>
                                            <input  class="form-control form-control-sm noS" type="number" name="noSolicitud" id="noSolicitud" disabled>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row ">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Cliente</label><!-- <font size=2> *</font> -->
                                            <select class="form-control form-control-sm" style="text-transform:uppercase;" id="cliente">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Sub Cliente</label>  
                                            <input type="text" class="form-control form-control form-control-sm"  style="text-transform:uppercase;" id="subCliente">                                   
                                            <!-- <select class="form-control form-control-sm" style="text-transform:uppercase;" id="subCliente">
                                            </select> -->
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Lugar de entrega </label>
                                            <input type="text" class="form-control form-control-sm"  style="text-transform:uppercase;"id="lugarEntrega" >
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Periodo De:</label>
                                            <input type="date" class="form-control form-control-sm"  style="" id="de" onchange="valFechaDeA();">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Al: </label>
                                            <input type="date" class="form-control form-control-sm"  style="" id="al" onchange="valFechaDeA();">
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Incoterm </label>
                                            <select class="form-control form-control-sm" style="text-transform:uppercase;" id="incoterm">
                                                <option value="" selected disabled>-SELECT-</option>
                                                <option value="FOB">FOB</option>
                                                <option value="CFR">CFR</option>
                                                <option value="CIF">CIF</option>
                                                <option value="DDP">DDP</option>
                                                <option value="EXW">EXW</option>
                                                <option value="CPT">CPT</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Cantidad (BC)</label>
                                            <input type="number" min="0" onkeydown="evitarNegativo(event)" onpaste="return false;" onDrop="return false;" autocomplete="off" class="form-control form-control-sm" id="cantidad">
                                        </div>
                                    </div>
                                    <!-- <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Calidad </label>
                                            <input type="text" class="form-control form-control-sm"  style="text-transform:uppercase;" id="calidad">
                                        </div>
                                    </div> -->
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Cosecha </label>
                                            <select type="number" class="form-control form-control-sm"  style="" id="cosecha">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Mes de cobertura </label>
                                            <select class="form-control form-control-sm" id="mesCobertura" name="mesCobertura">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Region</label>
                                            <select class="form-control form-control-sm" id="region" name="region">
                                                <option value="" selected disabled>-SELECT-</option>
                                                <option value="MEXICANO">MEXICANO</option>
                                                <option value="AMERICANO">AMERICANO</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Destino</label>
                                            <select class="form-control form-control-sm" id="destino" name="destino">
                                                <option value="" selected disabled>-SELECT-</option>
                                                <option value="NACIONAL">NACIONAL</option>
                                                <option value="EXPORTACION">EXPORTACION</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Grado</label>
                                            <input type="" class="form-control form-control-sm"  style="" id="grado">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Fibra </label>
                                            <input type="number" class="form-control form-control-sm"  style="" id="fibra">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Micronara</label>
                                            <input type="number" class="form-control form-control-sm"  style="" id="micronara">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Resistencia</label>
                                            <input type="number" class="form-control form-control-sm"  style="" id="resistencia">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Uniformidad</label>
                                            <input type="number" class="form-control form-control-sm"  style="" id="uniformidad">
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Fijo / On call</label>
                                            <select class="form-control form-control-sm" id="fijoCall" name="fijoCall">
                                                <option value="" selected disabled>-SELECT-</option>
                                                <option value="FIJO">FIJO</option>
                                                <option value="ONCALL">ON CALL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Precio base NY</label>
                                            <input type="number" class="form-control form-control-sm prePun"  style="" id="precioNY" onchange="sumar();">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Puntos</label>
                                            <input type="number" class="form-control form-control-sm prePun"  style="" id="puntos" onchange="sumar();">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Precio final</label>
                                            <input type="text" class="form-control form-control-sm"  style="" id="precioFinal" disabled >
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Unidad de medida</label>
                                            <select class="form-control form-control-sm" id="uMedida" name="uMedida">
                                                <option value="" selected disabled>-SELECT-</option>
                                                <option value="CLBN">CLBN</option>
                                                <option value="QUINTALES">QUINTALES</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <!-- <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Bases de precio </label>
                                            <input type="number" class="form-control form-control-sm"  style="" id="basesPrecio">
                                        </div>
                                    </div> -->
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Condiciones de pago</label>
                                            <select class="form-control form-control-sm" id="condPago" name="condPago">
                                                <option value="" selected disabled>-SELECT-</option>
                                                <option value="ANTICIPADO">ANTICIPADO</option>
                                                <option value="CONTADO">CONTADO</option>
                                                <option value="30DIAS">30 DIAS</option>
                                                <option value="90DIAS">90 DIAS</option>
                                                <option value="120DIAS">120 DIAS</option>
                                                <option value="180DIAS">180 DIAS</option>
                                            </select>
                                        </div>                                        
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Intereses normal</label>
                                            <input type="number" class="form-control form-control-sm"  style="" id="intNormal">
                                        </div>
                                    </div>  
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Intereses moral</label>
                                            <input type="number" class="form-control form-control-sm"  style="" id="intMoral">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Status </label>
                                            <select class="form-control form-control-sm" id="contStatus" name="contStatus" disabled>
                                                <option value="" selected disabled>-SELECT-</option>
                                                <option value="POR APROBAR">POR APROBAR</option>
                                                <option value="APROBADO">APROBADO</option>
                                                <option value="RECHAZADO">RECHAZADO</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Comentario</label>
                                            <input type="text" class="form-control form-control-sm"  style="" id="comentario" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Nota</label>
                                            <input type="text" class="form-control form-control-sm"  style="" id="nota">
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Aprueba</label>
                                            <input type="text" class="form-control form-control-sm"  style="" id="aprueba" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <font size=2>*Required Fields</font>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" id="btnSend" class="btn btn-dark">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <style>
    .noS {
        text-align: center;
        font-weight: bold;
    }
    </style>

</html>
<?php
endif;
?>
