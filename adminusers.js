$(document).ready(function() {

    $('#tablaClients tfoot th').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        } );
    
    
    var opcion;
    opcion = 4;
    
    tablaCli = $('#tablaClients').DataTable({
        
            //resumen de total de lotes, total de pacas y suma de pesos 
   
        //para usar los botones   
            responsive: "true",
            dom: 'Brtilp',       
            buttons:[ 
                {
                    extend:    'excelHtml5',
                    text:      '<i class="fas fa-file-excel"></i> ',
                    titleAttr: 'Export to Excel',
                    className: 'btn btn-success',
                    exportOptions: {
                        columns: ":not(.no-exportar)"
                    }
                },
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fas fa-file-pdf"></i> ',
                    titleAttr: 'Export to PDF',
                    className: 'btn btn-danger',
                    exportOptions: {
                        columns: ":not(.no-exportar)"
                    }
                },
                /*{
                    extend:    'print',
                    text:      '<i class="fa fa-print"></i> ',
                    titleAttr: 'Imprimir',
                    className: 'btn btn-info'
                },*/
            ],
        
        initComplete: function () {
                // Apply the search
                this.api().columns().every( function () {
                    var that = this;
     
                    $( 'input', this.footer() ).on( 'keyup change clear', function () {
                        if ( that.search() !== this.value ) {
                            that
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );
        },
        "order": [ 1, 'asc' ],
        "scrollX": true,
        "scrollY": "50vh",
        "autoWidth": true,
        "scrollCollapse": true,
        "lengthMenu": [100,200,300,500],
        //"aoColumnDefs": [{ "bVisible": false, "aTargets": [ 19 ]}],
        fixedColumns:   {
                left: 0,
                right: 1
            },
        "ajax":{            
            "url": "bd/crudusers.php", 
            "method": 'POST', //usamos el metodo POST
            "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
            "dataSrc":""
        },
        "columns":[
            {"data": "UserID"},
            {"data": "User"},
            {"data": "Region"},
            {"data": "email"},
            {"data": "Locked", className: "text-center"},            
            {"data": "Trucks", className: "text-center"},
            {"data": "Lots", className: "text-center"},
            {"data": "DO", className: "text-center"},
            {"data": "Adjustments", className: "text-center"},
            {"data": "Invoices", className: "text-center"},
            {"data": "Exports", className: "text-center"},
            {"data": "Transports", className: "text-center"},
            {"data": "Routes", className: "text-center"},
            {"data": "Clients", className: "text-center"},
            {"data": "Authorize", className: "text-center"},
            {"data": "Requisition", className: "text-center"},
            {"data": "Admin", className: "text-center"},
            {"data": "Priv"},
        

            //

            {"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button><button class='btn btn-success btn-sm btnUnblock'><i class='material-icons'>lock_open</i></button></div></div>"}
        ],
        columnDefs : [
            { targets : [4],
              render : function (data, type, row) {
                return data == '1' ? 'Locked' : 'Active'
              }
            }
       ]

       

       
    });     

    muestra();  

    
    $('.dataTables_length').addClass('bs-select');
    
    var fila; //captura la fila, para editar o eliminar
    //submit para el Alta y Actualización
    $('#formusers').submit(function(e){
       
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
        nameuser = $.trim($('#nameuser').val());    
        UserID = $.trim($('#UserID').val());
        region = $.trim($('#region').val());
        email = $.trim($('#email').val());
        Priv = $.trim($('#Priv').val());
        

        if( $('#trucks').prop('checked') ) {
            trucks = 1;
        }  else {
            trucks = 0;
        }

        if( $('#lots').prop('checked') ) {
            lots = 1;
        }  else {
            lots = 0;
        }


        if( $('#do').prop('checked') ) {
            deo = 1;
        }  else {
            deo = 0;
        }

        if( $('#adjustments').prop('checked') ) {
            adjustments = 1;
        }  else {
            adjustments = 0;
        }

        if( $('#adjustments').prop('checked') ) {
            adjustments = 1;
        }  else {
            adjustments = 0;
        }

        if( $('#invoices').prop('checked') ) {
            invoices = 1;
        }  else {
            invoices = 0;
        }

        if( $('#exports').prop('checked') ) {
            expor = 1;
        }  else {
            expor = 0;
        }


        if( $('#transports').prop('checked') ) {
            transports = 1;
        }  else {
            transports = 0;
        }

        if( $('#routes').prop('checked') ) {
            routes = 1;
        }  else {
            routes = 0;
        }

        if( $('#clients').prop('checked') ) {
            clients = 1;
        }  else {
            clients = 0;
        }

        if( $('#authorize').prop('checked') ) {
            authorize = 1;
        }  else {
            authorize = 0;
        }

        if( $('#requisition').prop('checked') ) {
            requisition = 1;
        }  else {
            requisition = 0;
        }


        if( $('#admin').prop('checked') ) {
            admin = 1;
        }  else {
            admin = 0;
        }



                         
            $.ajax({
              url: "bd/crudusers.php",
              type: "POST",
              datatype:"json",    
              data:  { opcion:opcion, nameuser:nameuser, UserID:UserID, region:region, email:email, 
                Priv:Priv, trucks:trucks, lots:lots, deo:deo, adjustments:adjustments, 
                invoices:invoices, expor:expor, transports:transports, routes:routes, clients:clients,
                authorize:authorize, requisition:requisition, admin:admin},    
              success: function(data) {
                tablaCli.ajax.reload(null, false);
                alert('Successful');
               },error: function(data) {
                alert('error');
            }
            });			        
        $('#modalCRUD').modal('hide');											     			
    });
            
     
    
    //para limpiar los campos antes de dar de Alta una Persona
    $("#btnNuevo").click(function(){
        opcion = 1; //alta           
        CliID=null;
        $("#formusers").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("New User");
        $('#modalCRUD').modal('show');	    
    });
    
    //Editar        
    $(document).on("click", ".btnEditar", function(){


        
        opcion = 2;//editar
        fila = $(this).closest("tr");	        
        UserID = fila.find('td:eq(0)').text(); //capturo el ID
        nameuser = fila.find('td:eq(1)').text(); 		            
        region = fila.find('td:eq(2)').text();
        email = fila.find('td:eq(3)').text();
        //Cty = fila.find('td:eq(4)').text();
        trucks = parseInt(fila.find('td:eq(5)').text());
        lots = parseInt(fila.find('td:eq(6)').text());
        deo = parseInt(fila.find('td:eq(7)').text());
        adjustments = parseInt(fila.find('td:eq(8)').text());
        invoices = parseInt(fila.find('td:eq(9)').text());
        expor = parseInt(fila.find('td:eq(10)').text());
        transports = parseInt(fila.find('td:eq(11)').text());
        routes = parseInt(fila.find('td:eq(12)').text());
        clients = parseInt(fila.find('td:eq(13)').text());
        authorize = parseInt(fila.find('td:eq(14)').text());
        requisition = parseInt(fila.find('td:eq(15)').text());
        admin = parseInt(fila.find('td:eq(16)').text());
        Priv = fila.find('td:eq(17)').text();

        var data = $('#tablaClients').DataTable().row(fila).data(); //fila.find('td:eq(24)').text();
        Cde = data['Cde'];
        $("#UserID").val(UserID);
        $("#nameuser").val(nameuser);
        $("#region").val(region);
        $("#email").val(email);

        if (trucks === 1){
            $("#trucks").prop("checked", true);            
         }
         else
         {
             $("#trucks").prop("checked", false);
         }


         if (lots === 1){
            $("#lots").prop("checked", true);            
         }
         else
         {
             $("#lots").prop("checked", false);
         }

         
         if (deo === 1){
            $("#do").prop("checked", true);            
         }
         else
         {
             $("#do").prop("checked", false);
         }

         if (adjustments === 1){
            $("#adjustments").prop("checked", true);            
         }
         else
         {
             $("#adjustments").prop("checked", false);
         }


         if (adjustments === 1){
            $("#adjustments").prop("checked", true);            
         }
         else
         {
             $("#adjustments").prop("checked", false);
         }

         if (invoices === 1){
            $("#invoices").prop("checked", true);            
         }
         else
         {
             $("#invoices").prop("checked", false);
         }


         if (invoices === 1){
            $("#invoices").prop("checked", true);            
         }
         else
         {
             $("#invoices").prop("checked", false);
         }

         if (expor === 1){
            $("#exports").prop("checked", true);            
         }
         else
         {
             $("#exports").prop("checked", false);
         }

         if (transports === 1){
            $("#transports").prop("checked", true);            
         }
         else
         {
             $("#transports").prop("checked", false);
         }

         if (routes === 1){
            $("#routes").prop("checked", true);            
         }
         else
         {
             $("#routes").prop("checked", false);
         }


         if (clients === 1){
            $("#clients").prop("checked", true);            
         }
         else
         {
             $("#clients").prop("checked", false);
         }

         if (authorize === 1){
            $("#authorize").prop("checked", true);            
         }
         else
         {
             $("#authorize").prop("checked", false);
         }

         if (requisition === 1){
            $("#requisition").prop("checked", true);            
         }
         else
         {
            $("#requisition").prop("checked", false);
         }


         if (admin === 1){
            $("#admin").prop("checked", true);            
         }
         else
         {
            $("#admin").prop("checked", false);
         }
         $("#UserID").prop('disabled', true);

       
        $("#Priv").val(Priv);

        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $(".modal-title").text("Edit Client");		
        $('#modalCRUD').modal('show');		   
    });
    
    //Borrar
    $(document).on("click", ".btnUnblock", function(){
        fila = $(this);           
        UserID = $(this).closest('tr').find('td:eq(0)').text();
        Estatuts = $(this).closest('tr').find('td:eq(4)').text();		
        opcion = 3; //Desbloquear usuario        
             
        if (Estatuts === "Locked") {            
            $.ajax({
              url: "bd/crudusers.php",
              type: "POST",
              datatype:"json",    
              data:  {opcion:opcion, UserID:UserID},    
              success: function() {
                tablaCli.ajax.reload(null, false); //tablaInv.row(fila.parents('tr')).remove().draw();
                  alert('User unlocked');                  
               }
            });	
        }

        else  {            
            alert('this user is not locked ');	
        }
     });
     
     
// ------ seccion subir liquidaciones --------------------------------------------------------------------------------

  //carga masiva nuevas pacas
  $("#Pesos").click(function(){
      $("#Scripts").trigger("reset");
      $('#avisoS').html('');
      $(".modal-header").css( "background-color", "#17562c");
      $(".modal-header").css( "color", "white" );
      $(".modal-title").text("Import CSV");
      $('#Scripts').modal('show');	    
  });
  
  //submit para Carga masiva
  $('#Scripts').submit(function(e){
      e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
      var comprobar = $('#csvS').val().length;
      var extension = $('#csvS').val().split(".").pop().toLowerCase();
      //validar extension
      /*if($.inArray(extension, ['csv']) == -1)
      {
          alert("Invalid File");
          retornarError = true;
          $('#csv').val("");
      }else{*/
      if (comprobar>0){
          var formulario = $("#Scripts");
          var archivos = new FormData();
          var url = "bd/Mysql.php";
              for (var i=0; i<(formulario.find('input[type=file]').length); i++){
                  archivos.append((formulario.find('input[type="file"]:eq('+i+')').attr("name")),((formulario.find('input[type="file"]:eq('+i+')'))[0].files[0]));
              }
      var applyFilter = $.ajax({
          url: "bd/Mysql.php",
          type: "POST",
          contentType: false,
          data:  archivos,
          processData: false,
          success: function(dat) {
              datos = JSON.parse(dat);
              data = datos['Status'];
              cont = datos['cont'];
              upd = datos['upd'];
          //$('#avisoNew').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Pacas cargadas con exito</div>');
  
              //var opts = JSON.parse(data);
              //alert (opts);
              //alert (data);
              /*if (data == 'OK'){
                  alert("Successful Import");
                  $('#modalFile').modal('hide');
                  location.reload();
                  return false;
              } else*/
              if (data == 'Columns'){
                  $('#avisoS').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Check number of columns. </div>');
                  setTimeout(function() {
                      $('#alert1').fadeOut(1000);
                      //$('#filtrarmodal').modal('hide');
                  },1000);
                  return false;
              }else if (data == 'Exist'){
                  alert ("Existen los lotes");
                  return false;
              }else{
                  //alert (cont);
                  $('#avisoS').html('<div id="alert1" class="alert alert-success" style="width: 24rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i>New records: '+cont+'; Updated records: '+upd+'</div>');
                  return false;
          }
          }
          });
          $('#avisoS').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');
          return false;    
      }else{
          $('#avisoLiq').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Select csv file to import </div>');
                  setTimeout(function() {
                      $('#alert1').fadeOut(1000);
                      //$('#filtrarmodal').modal('hide');
                  },1000);
          //alert ("Select csv file to import");
          return false;
      }
      //}   	        
  });

// -------------------------------------------------------------------------------------------------------------------     
     

         
    });
    

    function muestra (){
        tablaCli.column(4).nodes().each(function (node, index, dt) {
            if(table.cell(node).data() == '0'){
                console.log("encontrado");
              table.cell(node).data('100');
            }
          });
    }

    function UserCheck(){
        var userid = document.getElementById("UserID").value;
        
      var valuser= tablaCli.column(0).search(userid);
      var column = tablaCli .row(0).data();
       
       
      console.log(column[0]);
        
       // var lastElement = valuser.reverse()[0].countryid

        //$.each(valuser, function(index, val) {
          //  console.log(valuser);
        //});
        /*if (valuser===lastElement) {
            document.getElementById("userexist").innerHTML = "Este usuario y existe";
            $('#btnGuardar').prop('disabled', true);
        }else{
            document.getElementById("userexist").innerHTML = "";
            $('#btnGuardar').prop('disabled', false);
        }*/

    }


   