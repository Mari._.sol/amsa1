<?php
if (!isset($_SESSION))
    session_start();
if (isset($_SESSION['username'])) :
    $Typ_Trk = $_SESSION['Typ_Trk'];
    $permiso = $_SESSION['Admin'];
    $rates=$_SESSION['ViewRates'];
    $ViewLiq=$_SESSION['ViewLiq'];
?>

    <!doctype html>
    <html lang="es en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv='cache-control' content='no-cache'>
        <meta http-equiv='expires' content='0'>
        <meta http-equiv='pragma' content='no-cache'>
        <link rel="shortcut icon" href="img/ecom.png" />
        <title>Home</title>

        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="../portalLogistica/assets/bootstrap/css/bootstrap.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="../portalLogistica/main.css">
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">


        <script defer type="text/javascript" src="./assets/bootstrap/js/bootstrap.min.js"> </script>
    </head>

    <body>

        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>

                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <li><a class="dropdown-item" href="trucks.php">Trucks</a></li>
                        <li><a class="dropdown-item" href="lots.php">Lots</a></li>
                        <?php if ($_SESSION['username'] == 'ISELA LARA' || $_SESSION['username'] == 'DARIO ROMERO' || $_SESSION['username'] == 'CESAR ARMAS' || $_SESSION['username'] == 'LUIS ANGEL RENDON TARIN' || $_SESSION['username'] == 'JOSE RENDON'){?>
                        <li><a class="dropdown-item" href="bales.php">Bales</a></li>
                        <?php } ?>
                        <li><a class="dropdown-item" href="do.php">Delivery Order</a></li>
                        <li><a class="dropdown-item" href="adjustments.php">Adjustments</a></li>
                        <li><a class="dropdown-item" href="inv.php">Invoice</a></li>
                        <li><a class="dropdown-item" href="export.php">Containers</a></li>
                        <li><a class="dropdown-item" href="clients.php">Clients</a></li>
                        <li><a class="dropdown-item" href="routes.php">Routes</a></li>
                        <li><a class="dropdown-item" href="requisition.php">Requisition</a></li>
                        <li><a class="dropdown-item" href="CMS.php">CMS</a></li>
                        <?php if ($permiso == 1){?>
                            <li><a class="dropdown-item" href="adminusers.php">User Management</a></li>
                            <li><a class="dropdown-item" href="adminproveed.php">Proveed Management</a></li>
                        <?php } ?>
                            <?php if ($rates == 1){?>
                            <li><a class="dropdown-item" href="Rates.php">Rates</a></li>
                        <?php } ?>
                        <?php if ($ViewLiq == 1){?>
                            <li><a class="dropdown-item" href="liquidations.php">Liquidations</a></li>
                        <?php } ?>
                    </ul>
                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-2" href="#">
                    <div class="logos"><img class="log" src="img/logo1.png"> AMSA / Home</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <!--<p>/ Home</p>-->
                </div>
                <div class="container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                </div>
                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                    <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"><?php echo $_SESSION['username']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->

        <!-- Aquí inicia todo código de tablas etc -->

        <div class="container-fluid">
            <div class="col-12">
                <p><br><br><br><br><br><br><br><br><br><br><br> </p>
            </div>
            <div class="emblema col-11">
                <h1>
                    "Portal de Logística - AMSA
                </h1>
                <br>
                <h1>
                    Algodón México"
                </h1>
            </div>
        </div>
    </body>

    </html>
<?php
else :
    include_once('index.php');
endif;
?>
