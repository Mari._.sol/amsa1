<?php
session_start();
if (!isset($_SESSION['username'])) :
    include_once('index.php');
else :
    $Typ_Cli = $_SESSION['Typ_Cli'];
    $permiso = $_SESSION['Admin'];
    $editar=$_SESSION['EditRates'];
?>
    <!doctype html>
    <html lang="es en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="img/ecom.png" />
        <title>Rates</title>

        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="../portalLogistica/assets/bootstrap/css/bootstrap.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="../portalLogistica/main.css">
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>

    <body>

        <!-- Scripts -->

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
        <!--SCRIPT FORMATO DE FECHA -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>

        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
        
        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>

        <!-- importo la libreria moments -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.0/moment.min.js"></script>
        <!-- importo todos los idiomas -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.0/moment-with-locales.min.js"></script>


  
            <script type="text/javascript" src="Rates.js"></script>
      
        

        <!-- Terminan Scripts -->
        
        <!-- Bandera edit -->
        <div id="tarifas" style="display:none;">
            <input id="editartarifas" value="<?php echo $editar; ?>"/>
        </div> 

        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>

                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <li><a class="dropdown-item" href="trucks.php">Trucks</a></li>
                        <li><a class="dropdown-item" href="lots.php">Lots</a></li>
                        <?php if ($_SESSION['username'] == 'ISELA LARA' || $_SESSION['username'] == 'DARIO ROMERO' || $_SESSION['username'] == 'CESAR ARMAS' || $_SESSION['username'] == 'LUIS ANGEL RENDON TARIN'){?>
                        <li><a class="dropdown-item" href="bales.php">Bales</a></li>
                        <?php } ?>
                        <li><a class="dropdown-item" href="do.php">Delivery Order</a></li>
                        <li><a class="dropdown-item" href="adjustments.php">Adjustments</a></li>
                        <li><a class="dropdown-item" href="inv.php">Invoice</a></li>
                        <li><a class="dropdown-item" href="export.php">Containers</a></li>
                        <li><a class="dropdown-item" href="clients.php">Clients</a></li>
                        <li><a class="dropdown-item" href="routes.php">Routes</a></li>
                        <li><a class="dropdown-item" href="requisition.php">Requisition</a></li>
                        <li><a class="dropdown-item" href="CMS.php">CMS</a></li>
                        <?php if ($permiso == 1){?>
                        <li><a class="dropdown-item" href="adminusers.php">Admin Users</a></li>
                        <?php } ?>
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="#">Rates</a></li>
                    </ul>
                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-2" href="main.php">
                    <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <p>/ Rates</p>
                </div>
                <div class="container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                    <!-- Botones -->
                    <?php if ($Typ_Cli == 1) : ?>
                        <button id="btnNuevo" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Add"><i class="bi bi-plus-square"></i></button>
                    <?php endif ?>
                </div>
                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                    <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"><?php echo $_SESSION['username']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->

        <!-- Aquí inicia todo código de tablas etc -->
        <!-- Inicio de la tabla -->
        <div class="card card-body" style="opacity:100%;">           


            <div class="table-responsive" style="opacity:100%;">
                 <!-- Check box para filtrar tarifas activas o inactivas -->
                 <div class="input-group input-group mb-12">
                        <div id = "Status" class="col-1">
                            <input type="checkbox" id="activo"  name="activo" value="Active" class="activo">
                            <label for="horns" style="color:white">Actives</label>
                        </div>
                        <div id = "Status"  class="col-1">
                            <input type="checkbox" id="inactivo" name="inactivo" class="inactivo">
                            <label for="horns" style="color:white">Inactives</label>
                        </div>
                        <div id = "Status"  class="col-1">
                            <input type="checkbox" id="todos" name="todos" class="todos" checked>
                            <label for="horns" style="color:white">All</label>
                        </div>
                </div>
                <br>

                <table id="tablaClients" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;">
                    <thead style="background-color: #65ac7c;" style="opacity:100%;">
                        <tr>
                            <th class="th-sm">ID Rate</th>
                            <th class="th-sm">Transport</th>
                            <th class="th-sm">Zone</th>
			                <th class="th-sm">City</th>
                            <th class="th-sm">Average Cost</th>       
                            <th class="th-sm">Authorized Total Cost </th>                      
                            <th class="th-sm">Start Date</th>
                            <th class="th-sm">End Date</th>                                   
                            <th class="th-sm">Status</th>  
                            <?php if ($Typ_Cli == 1) { ?>
                                <th class="no-exportar"></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="th-sm">ID Rate</th>
                            <th class="th-sm">Transport</th>
                            <th class="th-sm">Zone</th>
			                <th class="th-sm">City</th>
                            <th class="th-sm">Average Cost</th>   
                            <th class="th-sm">Authorized Total Cost </th>                           
                            <th class="th-sm">Start Date</th>
                            <th class="th-sm">End Date</th> 
                            <th class="th-sm">Status</th>    
                            <?php if ($Typ_Cli == 1) { ?>
                                <th class="no-exportar"></th>
                            <?php } ?>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!--Modal para CRUD-->
        <div class="modal fade in" data-bs-backdrop="static" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formrates">
                        <div class="modal-body">
                      
                            <div class="row">
                           
                                
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Zone<font size=2> *</font></label>
                                        <select class="form-control form-control-sm" id="zone" name="zone" required>
                                        <option value="" selected disabled>-Select-</option>
                                        </select>  
                                    </div>
                                </div>

                                <div class="col-lg-7">
                                    <div class="form-group">

                                        <label for="" class="col-form-label">Transport<font size=2> *</font></label>
                                        <select class="form-control form-control-sm" id="transport" name="transport" required>
                                        <option value="" selected disabled>-Select-</option>
                                        </select>                                     
                                        
                                    </div>
                                </div>
                                
                               
                            </div>
                            <div class="row">
                                <div class="col-lg-5">
                                     <div class="form-group">
                                            <label for="" class="col-form-label">State<font size=2> *</font></label>
                                            <select class="form-control form-control-sm" id="state" name="state">
                                            <option value="" selected disabled>-Select-</option>                        
                                            <option value="AGUASCALIENTES">AGUASCALIENTES</option>
                                            <option value="BAJA CALIFORNIA">BAJA CALIFORNIA</option>
                                            <option value="BAJA CALIFORNIA SUR">BAJA CALIFORNIA SUR</option>
                                            <option value="CAMPECHE">CAMPECHE</option>
                                            <option value="COAHUILA">COAHUILA </option>
                                            <option value="COLIMA">COLIMA</option>
                                            <option value="CHIAPAS">CHIAPAS</option>
                                            <option value="CHIHUAHUA">CHIHUAHUA</option>
                                            <option value="CIUDAD DE MÉXICO">CIUDAD DE MÉXICO</option>
                                            <option value="DURANGO">DURANGO</option>
                                            <option value="GUANAJUATO">GUANAJUATO</option>
                                            <option value="GUERRERO">GUERRERO</option>
                                            <option value="HIDALGO">HIDALGO</option>
                                            <option value="JALISCO">JALISCO</option>
                                            <option value="ESTADO DE MÉXICO">ESTADO DE MÉXICO</option>
                                            <option value="MICHOACÁN">MICHOACÁN</option>
                                            <option value="MORELOS">MORELOS</option>
                                            <option value="NAYARIT">NAYARIT</option>
                                            <option value="NUEVO LEÓN">NUEVO LEÓN</option>
                                            <option value="OAXACA">OAXACA</option>
                                            <option value="PUEBLA">PUEBLA</option>
                                            <option value="QUERÉTARO">QUERÉTARO</option>
                                            <option value="QUINTANA ROO">QUINTANA ROO</option>
                                            <option value="SAN LUIS POTOSÍ">SAN LUIS POTOSÍ</option>
                                            <option value="SINALOA">SINALOA</option>
                                            <option value="SONORA">SONORA</option>
                                            <option value="TABASCO">TABASCO</option>
                                            <option value="TAMAULIPAS">TAMAULIPAS</option>
                                            <option value="TLAXCALA">TLAXCALA</option>
                                            <option value="VERACRUZ ">VERACRUZ</option>
                                            <option value="YUCATÁN">YUCATÁN</option>
                                            <option value="ZACATECAS">ZACATECAS</option>
                                            </select>  
                                    </div>
                                </div>
                            
                       
                                <div class="col-lg-7">
                                    <div class="form-group">
                                            <label for="" class="col-form-label">City<font size=2> *</font></label>
                                            <select class="form-control form-control-sm" id="city" name="city" required>
                                            <option value="" selected disabled>-Select-</option>
                                            </select>  
                                    </div>
                                </div>
                                
                            </div>
                          

                            <div class="row">
                                <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Authorized Average Cost* </label>                                        
                                            <input type="text" class="form-control form-control-sm" id="costprom" onkeyup="costo()" required>                                   
                                        </div>
                                </div> 

                                <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Authorized Total  Cost </label>                                        
                                            <input type="text" class="form-control form-control-sm" id="total"  readonly>                                   
                                        </div>
                                </div> 
                                
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Start Date<font size=2>*</font></label>
                                        <input type="date" class="form-control form-control-sm" id="startdate" onkeydown="return false" required>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">End Date<font size=2>*</font></label>
                                        <input type="date" class="form-control form-control-sm" id="enddate" onkeydown="return false" required>
                                    </div>
                                </div>
                                


                            </div>
                            <br>
                            <div class="row" id="alerta" style="display:none">
                                <div class="col-lg-12">
                                    <div class="alert alert-danger" role="alert">
                                        <div>
                                             START DATE IS GREATER THAN END DATE!
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <br>                      


                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" id="btnGuardar" class="btn btn-dark">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </body>

    </html>
<?php
endif;
?>
