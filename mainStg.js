$(document).ready(function() {

$('#tablaSettings tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );


var ID, opcion;
opcion = 4;
    
tablaSettings = $('#tablaSettings').DataTable({
    //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Export to Excel',
				className: 'btn btn-success',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			{
				extend:    'pdfHtml5',
				text:      '<i class="fas fa-file-pdf"></i> ',
				titleAttr: 'Export to PDF',
                className: 'btn btn-danger',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			/*{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},*/
		],
    
    initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
    },
    "order": [ 0, 'desc' ],
    "scrollX": true,
    "scrollY": "50vh",
    "scrollCollapse": true,
    "lengthMenu": [100,200,300,500],
    fixedColumns:   {
            left: 0,
            right: 1
        },
    "ajax":{            
        "url": "bd/crudStg.php", 
        "method": 'POST', //usamos el metodo POST
        "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
        "dataSrc":""
    },
    "columns":[
        {"data": "ID"},
        {"data": "Date"},
        {"data": "Year"},
        {"data": "Typ"},
        {"data": "DO"},
        {"data": "Requester"},
        {"data": "Gin"},
        {"data": "OutPlc"},
        {"data": "InReg"},
        {"data": "Ctc"},
        {"data": "InPlc"}, //InPlc = cliente en DO
        {"data": "Bales"},
        {"data": "Lot"},
        {"data": "TypeStg"},
        {"data": "Rsn"},
        {"data": "Comments"},
        {"data": "FullLot"},
        {"data": "DODev"},
        {"data": "DevID"}, //<button class='btn btn-danger btn-sm btnBorrar'><i class='material-icons'>delete</i></button>
        {"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button><button class='btn btn-danger btn-sm btnReturn'><i class='material-icons'>assignment_return</i></button></div></div>"}
    ]
});     

$('.dataTables_length').addClass('bs-select');

var fila; //captura la fila, para editar o eliminar
//submit para el Alta y Actualización
$('#formStg').submit(function(e){
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
    Dat = $.trim($('#Dat').val());
    DO = $.trim($('#DOrd').val());
    Rqstr = $.trim($('#Requester').val().toUpperCase());
    Qty = $.trim($('#Qty').val());
    Lot = $.trim($('#Lot').val());
    Typ = $.trim($('#Typ').val());
    Rsn = $.trim($('#Rsn').val());
    Cmt = $.trim($('#Cmt').val().toUpperCase());
    FullLot = $.trim($('#FullTyp').val());
    DODev = $.trim($('#DODev').val());                            
        $.ajax({
          url: "bd/crudStg.php",
          type: "POST",
          datatype:"json",    
          data:  {ID:ID, Dat:Dat, DO:DO, Rqstr:Rqstr, Qty:Qty, Lot:Lot, Typ:Typ, Rsn:Rsn, Cmt:Cmt, FullLot:FullLot, DODev:DODev, opcion:opcion},    
          success: function(data) {
            tablaSettings.ajax.reload(null, false);
           }
        });			        
    $('#modalCRUD').modal('hide');											     			
});

//para limpiar los campos antes de dar de Alta una Persona
$("#btnNuevo").click(function(){
    $("#InfDO").css("display", "none");
    LoadRqs();
    $('#Requester').append('<option value="" disabled selected >- Select -</option>');
    opcion = 1; //alta           
    ID = null;
    $("#formStg").trigger("reset");
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    $(".modal-title").text("New Adjustment");
    $('#modalCRUD').modal('show');	    
});

//Editar        
$(document).on("click", ".btnEditar", function(){
    $("#InfDO").css("display", "none");
    LoadRqs();
    opcion = 2;//editar
    fila = $(this).closest("tr");	        
    ID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID		            
    Dat = fila.find('td:eq(1)').text();
    DO = parseFloat(fila.find('td:eq(4)').text());
    //Rqstr = fila.find('td:eq(5)').text();
    Qty = fila.find('td:eq(11)').text();
    //Lot = fila.find('td:eq(12)').text();
    Typ = fila.find('td:eq(13)').text();
    Rsn = fila.find('td:eq(14)').text();
    AdjCmt = fila.find('td:eq(15)').text();
    FullLot = fila.find('td:eq(16)').text();
    DODev = fila.find('td:eq(17)').text();
    DevID = fila.find('td:eq(18)').text();
    $("#Dat").val(Dat);
    $("#DOrd").val(DO);
    $('#Lot').append('<option class="form-control selected">' + fila.find('td:eq(12)').text() + '</option>');
    //$("#Lot").val(Lot);
    //$("#Requester").val(Rqstr);
    $("#Qty").val(Qty);
    $("#Typ").val(Typ);
    $('#Requester').append('<option class="form-control selected">' + fila.find('td:eq(5)').text() + '</option>');
    $("#Cmt").val(AdjCmt);
    $("#Rsn").val(Rsn);
    $("#FullTyp").val(FullLot);
    $("#DODev").val(DODev);
    $(".modal-header").css("background-color", "#17562c");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Edit Adjustment");		
    $('#modalCRUD').modal('show');		   
});

//Borrar
$(document).on("click", ".btnReturn", function(){
    fila = $(this);           
    ID = parseInt($(this).closest('tr').find('td:eq(0)').text());
    Status = $(this).closest('tr').find('td:eq(13)').text();
    if (Status == "ADJUSTMENT"){
        alert ("Status doesn't correspond to an REJECTION");
    }else{
        alert(Status);
    }
 });
     
});

//Cargar Usuarios requester
function LoadRqs(){
    $("#Requester").empty();
    //$("#InReg").empty();
    var opts = "";
    opcion = 5;
    $.ajax({
          url: "bd/crudStg.php",
          type: "POST",
          datatype:"json",
          data:  {opcion:opcion},    
          success: function(data){
              opts = JSON.parse(data);
              //$('#InPlc').append('<option class="form-control">- Select -</option>');
              for (var i = 0; i< opts.length; i++){
                  $('#Requester').append('<option value="' + opts[i].User + '">' + opts[i].User + '</option>');
              }
           },
        error: function(data) {
            alert('error');
        }
    });
}

$(document).ready(function(){
    $("#Qty").change(function(){
        Qty = parseInt($.trim($('#Qty').val()));
        DOQty = parseInt($.trim($('#DOQty').val()));
        result = DOQty + Qty;
        if (result < 0 ){
            alert ("Number of bales remaining is less than zero. Check Quantity");
        }
    })
});

$(document).ready(function(){
    //Cargar lotes 
    function LoadLots(){
            $("#Lot").empty();
            var opts = "";
            opcion = 7;
            DO = $.trim($('#DOrd').val());
            $.ajax({
                url: "bd/crudStg.php",
                type: "POST",
                datatype:"json",
                data:  {DO:DO ,opcion:opcion},    
                success: function(data){
                    opts = JSON.parse(data);
                    for (var i = 0; i< opts.length; i++){
                        $('#Lot').append('<option value="' + opts[i].Lot + '">' + opts[i].Lot + ' </option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });
    }
    $("#btnNuevo").click(function(){
        $("#DOrd").change(function(){
            LoadLots();
            $('#Lot').append('<option value="" disabled selected >- Select -</option>');
        });
    });
    
    $(document).on("click", ".btnEditar", function(){
        LoadLots();
    });
});
