var Dev = 0;

$(document).ready(function() {

    //funcion requerida para sumar 
    jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
        return this.flatten().reduce( function ( a, b ) {
          if ( typeof a === 'string' ) {
            a = a.replace(/[^\d.-]/g, '') * 1;
          }
          if ( typeof b === 'string' ) {
            b = b.replace(/[^\d.-]/g, '') * 1;
          }
          return a + b;
        }, 0);
    });

    //promedio de columnas
    jQuery.fn.dataTable.Api.register( 'average()', function () {
        var data = this.flatten();
        var sum = data.reduce( function ( a, b ) {
            return (a*1) + (b*1); // cast values in-case they are strings
        }, 0 );
      
        return sum / data.length;
    });
    
    $('#tablaBales tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    } );

    $('#tablaBalesMod tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    });

    var ID, opcion;
    opcion = 4;
    //var GrdHVI, TaHVI, TcHVI, PcHVI, LenHVI, UiHVI, SfiHVI, StrHVI, ElgHVI, MicHVI, RDHVI, MatHVI, bHVI, ColHVI, MstHVI;
    
    var tablaBales = $('#tablaBales').DataTable({

        //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[
			/*{
			    extend:    'excelHtml5',
			    text:      '<i class="fas fa-file-excel"></i> ',
			    titleAttr: 'Export to Excel',
			    className: 'btn btn-success',
                download: 'open',
			    exportOptions: {
			        columns: ":not(.no-exportar)"
			    }
			},
			/*{
			    extend:    'pdfHtml5',
			    text:      '<i class="fas fa-file-pdf"></i> ',
			    titleAttr: 'Export to PDF',
			    orientation: 'landscape',
			    className: 'btn btn-danger',
                download: 'open',
			    exportOptions: {
			        columns: ":not(.no-exportar)"
			    }
			},
            {
                text: '<i class="bi bi-funnel-fill"></i>',
                titleAttr: 'Filtrar por',
                orientation: 'landscape',
                className: 'btn btn-info btn-filtrar',
                attr:{
                        id:'filtraboton',
                        "data-toggle": 'modal tooltip',
                        "disabled": false
                }
            },
			/*{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},*/
        ],
    
        initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                        .search( this.value, true, false ).draw();
                            
                    }
                } );
            } );
        },
        "order": [ 2, 'asc' ],
        "scrollX": true, 
        "scrollY": '50vh', //"340px",
        "scrollCollapse": true,
        "lengthMenu": [500,700,900,1000],
        /*fixedColumns:   {
            left: 0,
            right: 1
        },*/
        "ajax":{            
            "url": "bd/crudBales.php", 
            "method": 'POST', //usamos el metodo POST
            "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
            "dataSrc":""
        },
        "columns":[
            {"data": "Crp"},
            {"data": "GinID"},
            {"data": "Bal"}, //LotsAssc
            {"data": "Rcv"}, //CrgQty
            {"data": "BuyIt"},
            {"data": "Grp"},
            {"data": "Mic", render: $.fn.dataTable.render.number( ',', '.', 1)},
            {"data": "Len", render: $.fn.dataTable.render.number( ',', '.', 2)},
            {"data": "Str", render: $.fn.dataTable.render.number( ',', '.', 1)},
            {"data": "Rd", render: $.fn.dataTable.render.number( ',', '.', 1)},
            {"data": "b", render: $.fn.dataTable.render.number( ',', '.', 1)},
            {"data": "Tcnt"},
            {"data": "Tar", render: $.fn.dataTable.render.number( ',', '.', 2)}, //Consulta con DO 
            {"data": "Col1"},
            {"data": "Col2"},
            {"data": "Tgrd"},
            {"data": "Mst", render: $.fn.dataTable.render.number( ',', '.', 2)},
            {"data": "Sfi", render: $.fn.dataTable.render.number( ',', '.', 1)},
            {"data": "Sci", render: $.fn.dataTable.render.number( ',', '.', 1)},
            {"data": "Mat", render: $.fn.dataTable.render.number( ',', '.', 2)},
            {"data": "Elg", render: $.fn.dataTable.render.number( ',', '.', 1)}, //PWgh  
            {"data": "Unf", render: $.fn.dataTable.render.number( ',', '.', 1)},
            {"data": "Cnt"},
            {"data": "Wgh"},
            {"data": "Lot"}
        ]
    });
    
    var tablaBalesMod = $('#tablaBalesMod').DataTable({
        //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			/*{
			    extend:    'excelHtml5',
			    text:      '<i class="fas fa-file-excel"></i> ',
			    titleAttr: 'Export to Excel',
			    className: 'btn btn-success',
                download: 'open',
			    exportOptions: {
			        columns: ":not(.no-exportar)"
			    }
			},
			{
			    extend:    'pdfHtml5',
			    text:      '<i class="fas fa-file-pdf"></i> ',
			    titleAttr: 'Export to PDF',
			    orientation: 'landscape',
			    className: 'btn btn-danger',
                download: 'open',
			    exportOptions: {
			        columns: ":not(.no-exportar)"
			    }
			},
            {
                text: '<i class="bi bi-funnel-fill"></i>',
                titleAttr: 'Filtrar por',
                orientation: 'landscape',
                className: 'btn btn-info btn-filtrar',
                attr:{
                        id:'filtraboton',
                        "data-toggle": 'modal tooltip',
                        "disabled": false
                }
            },
			/*{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},*/
        ],
    
        initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                        .search( this.value, true, false ).draw();
                            
                    }
                } );
            } );
        },
        "order": [ 2, 'asc' ],
        "scrollX": true, 
        "scrollY": '50vh', //"340px",
        "scrollCollapse": true,
        "lengthMenu": [500,700,900,1000],
        /*fixedColumns:   {
            left: 0,
            right: 1
        },*/
        "ajax":{            
            "url": "bd/crudBales.php", 
            "method": 'POST', //usamos el metodo POST
            "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
            "dataSrc":""
        },
        "columns":[
            {"data": "Crp"},
            {"data": "GinID"},
            {"data": "Bal"}, //LotsAssc
            {"data": "Rcv"}, //CrgQty
            {"data": "BuyIt"},
            {"data": "Grp"},
            {"data": "Mic_Mod", render: $.fn.dataTable.render.number( ',', '.', 1)},
            {"data": "Len_Mod", render: $.fn.dataTable.render.number( ',', '.', 2)},
            {"data": "Str_Mod", render: $.fn.dataTable.render.number( ',', '.', 1)},
            {"data": "Rd_Mod", render: $.fn.dataTable.render.number( ',', '.', 1)},
            {"data": "b_Mod", render: $.fn.dataTable.render.number( ',', '.', 1)},
            {"data": "Tcnt_Mod"},
            {"data": "Tar_Mod", render: $.fn.dataTable.render.number( ',', '.', 2)}, //Consulta con DO 
            {"data": "Col1_Mod"},
            {"data": "Col2_Mod"},
            {"data": "Tgrd_Mod"},
            {"data": "Mst_Mod", render: $.fn.dataTable.render.number( ',', '.', 2)},
            {"data": "Sfi_Mod", render: $.fn.dataTable.render.number( ',', '.', 1)},
            {"data": "Sci_Mod", render: $.fn.dataTable.render.number( ',', '.', 1)},
            {"data": "Mat_Mod", render: $.fn.dataTable.render.number( ',', '.', 2)},
            {"data": "Elg_Mod", render: $.fn.dataTable.render.number( ',', '.', 1)}, //PWgh  
            {"data": "Unf_Mod", render: $.fn.dataTable.render.number( ',', '.', 1)},
            {"data": "Cnt_Mod"},
            {"data": "Wgh"},
            {"data": "Lot"}
        ]
    });

    $('.dataTables_length').addClass('bs-select');
    
    var fila; //captura la fila, para editar o eliminar -- submit para el Alta y Actualización


    //carga masiva nuevas pacas
    $("#NewBales").click(function(){
        $("#formFile").trigger("reset");
        $('#avisoNew').html('');
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Upload HVI");
        $('#modalNewBales').modal('show');	    
        $("#AlertaSupNew").css("display", "none");
    });
    
    
     //buscar el supplier teclenadolo 

    $("#SupIDNew").on('change keyup',function(){
        //  ValidateOutWgh();
          buscarValorSupNew($(this).val());
      });
      
      
      //SELECCIONAR EL GIN Y EL ID SUPPLIER DEL ARCHIVO

    $('#csv').on('change',function(e){
        
        Gin = $.trim($('#csv').val());   

        validacsv=validar_file_csv(Gin);
        if(validacsv == 1){            
                var $el = $('#csv');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap(); 
                $('#SupIDNew').val(""); 
                $('#Sup').val("");
        }
        else{
            if (Gin != ""){
                var res = Gin.split('-')[2];
                var res2 = res.replace(".csv","");
                console.log(res2);
                $('#SupIDNew').val(res2);
               // $('#SupID').val(res2);
                $('#Sup').val(res2);
                buscarValorSup(res2);

                //OBTENER EL PRIMER DATO DE LA COLUMNA GINID DEL ARCHIVO
                const file = e.target.files[0];
                const reader = new FileReader();

                reader.onload = function(event) {
                    const content = event.target.result;
                    const rows = content.split('\n'); // Separar por saltos de línea

                    // Obtener el segundo dato de la segunda columna
                    let secondDataSecondColumn = null;
                    for (let i = 0; i < rows.length; i++) {
                        const columns = rows[i].split(','); 
                        if (columns.length >= 2) {
                            if (i === 1) { // Verificar si es la segunda fila (0-indexed)
                                secondDataSecondColumn = columns[1]; // Segundo dato de la segunda columna
                                break; // Detenerse después de encontrar el segundo dato
                            }
                        }
                    }

                    //SELECCIONAR EL GINID EN EL SELECTOR
                    $('#GinCB').val(secondDataSecondColumn); 
                }

                reader.readAsText(file);        

            }
        }
        
    });
      
      

    //submit para Carga masiva
    $('#formFile').submit(function(e){
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
        var comprobar = $('#csv').val().length;
        var extension = $('#csv').val().split(".").pop().toLowerCase();
        Sup = $('#SupIDNew').val();
        //validar extension
        /*if($.inArray(extension, ['csv']) == -1)
        {
            alert("Invalid File");
            retornarError = true;
            $('#csv').val("");
        }else{*/
        if (comprobar>0){
            var formulario = $("#formFile");
            var archivos = new FormData();
            archivos.append('Sup',Sup);
            archivos.append('Dev',Dev);
            var url = "bd/NewBales.php";
                for (var i=0; i<(formulario.find('input[type=file]').length); i++){
                    archivos.append((formulario.find('input[type="file"]:eq('+i+')').attr("name")),((formulario.find('input[type="file"]:eq('+i+')'))[0].files[0]));
                }
        var applyFilter = $.ajax({
            url: "bd/NewBales.php",
            type: "POST",
            contentType: false,
            data:  archivos,
            processData: false,
            success: function(dat) {
                
                datos = JSON.parse(dat);
                data = datos['Status'];
                cont = datos['cont'];
                upd = datos['upd'];
                
                /*if (data == 'OK'){
                    alert("Successful Import");
                    $('#modalFile').modal('hide');
                    location.reload();
                    return false;
                } else */
                if (data == 'Columns'){
                    $('#avisoNew').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Check number of columns. </div>');
                    setTimeout(function() {
                        $('#alert1').fadeOut(1000);
                        //$('#filtrarmodal').modal('hide');
                    },1000);
                    return false;
                }else if (data == 'Exist'){
                    alert ("Existen los lotes");
                    return false;
                }else{
                
                    $('#avisoNew').html('<div id="alert1" class="alert alert-success" style="width: 24rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i>New records: '+cont+'; Updated records: '+upd+'</div>');
                    return false;
            }
            }
            });
            $('#avisoNew').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');
            return false;    
        }else{
            $('#avisoNew').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Select csv file to import </div>');
                    setTimeout(function() {
                        $('#alert1').fadeOut(1000);
                        //$('#filtrarmodal').modal('hide');
                    },1000);
            //alert ("Select csv file to import");
            return false;
        }
        //}   	        
    });


// ------ seccion subir liquidaciones --------------------------------------------------------------------------------

  //carga masiva nuevas pacas
  $("#UpdateLiq").click(function(){
      $("#formLiq").trigger("reset");
      $('#avisoLiq').html('');
      $(".modal-header").css( "background-color", "#17562c");
      $(".modal-header").css( "color", "white" );
      $(".modal-title").text("Upload Liqs");
      $('#modalUpdateLiq').modal('show');	 
      $("#exportlist").css("display", "none");   
  });
  
  //submit para Carga masiva
  $('#formLiq').submit(function(e){
      e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
      var comprobar = $('#csvLiq').val().length;
      var extension = $('#csvLiq').val().split(".").pop().toLowerCase();
      //validar extension
      /*if($.inArray(extension, ['csv']) == -1)
      {
          alert("Invalid File");
          retornarError = true;
          $('#csv').val("");
      }else{*/
      if (comprobar>0){
          var formulario = $("#formLiq");
          var archivos = new FormData();
          var url = "bd/LiqBales.php";
              for (var i=0; i<(formulario.find('input[type=file]').length); i++){
                  archivos.append((formulario.find('input[type="file"]:eq('+i+')').attr("name")),((formulario.find('input[type="file"]:eq('+i+')'))[0].files[0]));
              }
      var applyFilter = $.ajax({
          url: "bd/LiqBales.php",
          type: "POST",
          contentType: false,
          data:  archivos,
          processData: false,
          success: function(dat) {
              datos = JSON.parse(dat);
              data = datos['Status'];
              cont = datos['cont'];
              upd = datos['upd'];
              lista = datos['pend'];
              crop = datos['crop'];
              gin = datos['gin'];

              if(cont>0 && upd > 0){
                $("#exportlist").css("display", "block");
                //exportar lista de bales que no encontro
                    $("#exportlist").click(function(e){
                         e.preventDefault();
                         window.open("./bd/balespendientes.php?lista="+lista+"&crop="+crop+"&gin="+gin);
                         //e.preventDefault();
                    });                           
            }
          //$('#avisoNew').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Pacas cargadas con exito</div>');
  
              //var opts = JSON.parse(data);
              //alert (opts);
              //alert (data);
              /*if (data == 'OK'){
                  alert("Successful Import");
                  $('#modalFile').modal('hide');
                  location.reload();
                  return false;
              } else*/
              if (data == 'Columns'){
                  $('#avisoLiq').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Check number of columns. </div>');
                  setTimeout(function() {
                      $('#alert1').fadeOut(1000);
                      //$('#filtrarmodal').modal('hide');
                  },1000);
                  return false;
              }else if (data == 'Exist'){
                  alert ("Existen los lotes");
                  return false;

            
              }
              

              
              else{
                  //alert (cont);
                  $('#avisoLiq').html('<div id="alert1" class="alert alert-success" style="width: 24rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i>Bales not found: '+cont+'; Updated records: '+upd+'</div>');
                  /*setTimeout(function() {
                      $('#alert1').fadeOut(1000);
                      $('#modalFile').modal('hide');
                  },1000);*/
                  //alert("Successful Import"); //alert("Import Error");
                  //$('#modalFile').modal('hide');
                  //location.reload();
                  return false;
          }
          }
          });
          $('#avisoLiq').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');
          return false;    
      }else{
          $('#avisoLiq').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Select csv file to import </div>');
                  setTimeout(function() {
                      $('#alert1').fadeOut(1000);
                      //$('#filtrarmodal').modal('hide');
                  },1000);
          //alert ("Select csv file to import");
          return false;
      }
      //}   	        
  });

// -------------------------------------------------------------------------------------------------------------------



    //submit para actualizar pacas
       //submit para actualizar pacas
       $('#formUBales').submit(function(e){
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
        var comprobar = $('#csvU').val().length;
        var extension = $('#csvU').val().split(".").pop().toLowerCase();
        LocLot = $('#LocLot').val();

        if($('#pacasID').prop('checked') || $('#pacasNum').prop('checked')){      
        
        
            if (comprobar>0){
                var formulario = $("#formUBales");
                var archivos = new FormData();

                //validar que archivo php se va ejecutar dependiendo el check marcado
                if($('#pacasID').prop('checked')){
                    ruta = "bd/UpdateIDBales.php";
                }
                else{
                    ruta = "bd/UpdateBales.php";
                }


                archivos.append('LocLot',LocLot);
                var url = "bd/UpdateBales.php";
                    for (var i=0; i<(formulario.find('input[type=file]').length); i++){
                        archivos.append((formulario.find('input[type="file"]:eq('+i+')').attr("name")),((formulario.find('input[type="file"]:eq('+i+')'))[0].files[0]));
                    }
                    $.ajax({
                        url: ruta,
                        type: "POST",
                        contentType: false,
                        data:  archivos,
                        processData: false,
                        success: function(dat) {
                            datos = JSON.parse(dat);
                            data = datos['Status'];
                            cont = datos['cont'];
                            upd = datos['upd'];
                            newLots = datos['newLot'];
                            
                            if (data == 'Columns'){
                                $('#avisoUpdate').html('<div id="alert1" class="alert alert-success" style="width: 22rem; height: 4rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Check number of columns in Lots File. </div>');
                                setTimeout(function() {
                                    $('#alert1').fadeOut(1000);
                                    //$('#filtrarmodal').modal('hide');
                                },1000);
                                return false;
                            }if (data == 'ColumnsLots'){
                                $('#avisoUpdate').html('<div id="alert1" class="alert alert-success" style="width: 22rem; height: 4rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Check number of columns in the RcpLots File. </div>');
                                setTimeout(function() {
                                    $('#alert1').fadeOut(1000);
                                    //$('#filtrarmodal').modal('hide');
                                },1000);
                                return false;
                            }else if (data == 'Exist'){
                                alert ("Existen los lotes");
                                return false;
                            }else{
                                $('#avisoUpdate').html('<div id="alert1" class="alert alert-success" style="width: 28rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i>Lotted bales: '+cont+'; New Lots: '+newLots+'; Updated Lots: '+upd+'</div>');
                                $('#btnGuardarFileU').attr('disabled', true);
                                return false;
                        }
                        }
                    });
                    $('#avisoUpdate').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');
                    return false;     
            }else{
                $('#avisoUpdate').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Select csv file to import </div>');
                setTimeout(function() {
                    $('#alert1').fadeOut(1000);
                    //$('#filtrarmodal').modal('hide');
                },1000);
                //alert ("Select csv file to import");
                return false;
            }
        }
        else{
            alert("Debe marcar una opcion");
        }
        //}   	        
    });


    // boton update HVI-------------------------------------------------------------------------------------------------------------------------------------
    $("#UpdateHVI").click(function(){
        $('#avisoUpdHVI').html('');
        GrdHVI=0, TaHVI=0, TcHVI=0, PcHVI=0, LenHVI=0, UiHVI=0, SfiHVI=0, StrHVI=0, ElgHVI=0, MicHVI=0, RDHVI=0, MatHVI=0, bHVI=0, ColHVI=0, MstHVI=0;
        //$("#parametros").css("display","none");
        $("#divGrd,#divTA,#divTC,#divPC,#divLen,#divUI,#divSFI,#divStr,#divElg,#divMic,#divMat,#divFnn,#divSgr,#divRD,#divB,#divCol,#divTemp,#divMst").hide();
        LotCli = $('#LotsCli').val();
        $("#HVIOri").show();
        $("#HVIMod").show();
        $("#HVI").trigger("reset");
        $("#LotU").val(LotCli);
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $('#modal-UpdateHVI').modal('show');
    });


    //carga masiva actualizar pacas
    $("#UpdateBales").click(function(){
        $("#formUBales").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Upload Lots");
        $('#modalUpdateBales').modal('show');
        $("#pacasID").prop("checked", true);
        $("#pacasNum").prop("checked", false);
        $('#avisoUpdate').html('');
        $('#FailFiles').html("");
        //validar el checkbox que solo uno este marcado
        $('#pacasID').on('change',function(){
            if (this.checked) {
                $("#pacasNum").prop("checked", false);
            }           
        });

        $('#pacasNum').on('change',function(){
            if (this.checked) {
                $("#pacasID").prop("checked", false);
            }
        });
    });

    // botón filtros

    $("#filtraboton").click(function(){
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        DO = $('#DO').val();
        Reg = $('#Reg').val();
        Gin = $('#Gin').val();
        Lot = $('#Lot').val();
        Bale = $('#Bale').val();
        Crp = $('#Crp').val();

        $('#filtrarmodal').modal('show');
    });

    var botonglobal = 0;
    $("#buscafiltro, #borrarFiltro, #borrarFiltroU, #cer").click(function(){
        boton = $(this).val();
        if(boton == 1 || boton == 0)
            botonglobal = boton;
        if(boton != 3){
            idGlobal = 0;
            DO = $('#DO').val();
            Reg = $('#Reg').val();
            Gin = $('#Gin').val();
            Lot = $('#Lot').val();
            Bale = $('#Bale').val();
            Crp = $('#Crp').val();
            Status = $('#Status').val();
            //filters = {"muestras":muestras,"regionfil":regionfil};
            opcion = 4;
            var applyFilter =  $.ajax({
                type: 'POST',
                url: 'bd/assignFilters.php',
                data: {
                    boton: boton,
                    idGlobal:idGlobal,
                    Reg: Reg,
                    Gin:Gin,
                    DO: DO,
                    Lot: Lot,
                    Bale: Bale,
                    Crp: Crp,
                    Status: Status
                }
                
            }) 
            applyFilter.done(function(data){

                console.log(data);
                tablaBales.ajax.reload(function(){

                    if(boton == 1){
                        $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Filtros asignados con éxito</div>')
                        setTimeout(function() {
                            $('#alert1').fadeOut(1000);
                            $('#filtrarmodal').modal('hide');
                        },1000);
                        $('#DOCli').val(DO);
                        $('#LotsCli').val(Lot);
                        $('#CrpCli').val(Crp);
                    }
                    else{
                        $('#Reg').val("");
                        $('#Gin').val("");
                        $('#Lot').val("");
                        $('#Bale').val("");
                        $('#Crp').val("");
                        $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"> <i class="bi bi-trash-fill"></i> Filtros borrados con éxito</div>')
                        setTimeout(function() {
                            $('#alert1').fadeOut(1500);
                        },1500);
                    }

                });

                tablaBalesMod.ajax.reload(function(){

                    if(boton == 1){
                        $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Filtros asignados con éxito</div>')
                        setTimeout(function() {
                            $('#alert1').fadeOut(1000);
                            $('#filtrarmodal').modal('hide');
                        },1000);
                        $('#DOCli').val(DO);
                        $('#LotsCli').val(Lot);
                        $('#CrpCli').val(Crp);
                    }
                    else{
                        $('#Reg').val("");
                        $('#Gin').val("");
                        $('#Lot').val("");
                        $('#Bale').val("");
                        $('#Crp').val("");
                        $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"> <i class="bi bi-trash-fill"></i> Filtros borrados con éxito</div>')
                        setTimeout(function() {
                            $('#alert1').fadeOut(1500);
                        },1500);
                    }
                    if(boton = 4){
                        $('#LotU').val("");
                        $('#CrpU').val("");
                        $('#CliU').val("");
                        $('#Bale').val("");
                        $('#TextAreaHVIO').val("");
                        $('#TextAreaHVIM').val("");
                        $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"> <i class="bi bi-trash-fill"></i> Filtros borrados con éxito</div>')
                        setTimeout(function() {
                            $('#alert1').fadeOut(1500);
                        },1500);
                    }

                });
                $('#aviso').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');      
            });  
        }else{
            if(botonglobal == 0){
                $('#DepRegFil').attr('disabled', false);
                $('#ArrRegFil').attr('disabled', false);
                $('#muestras').attr('disabled', false);
                $('#asLots').attr('disabled', false);
                $('#idDO').attr('disabled', false);
                $('#DepArrDate').attr('disabled', false);
                $('#timeSelect').attr('disabled', true);
                $('#fromdate').attr('disabled', true);
                $('#todate').attr('disabled', true);

                $('#muestras').prop('selectedIndex',0);
                $('#DepRegFil').prop('selectedIndex',0);
                $('#ArrRegFil').prop('selectedIndex',0);
                $('#DepArrDate').prop('selectedIndex',0);
                $('#timeSelect').prop('selectedIndex',0);
                $('#idDO').val("");
                $('#fromdate').val("");
                $('#todate').val("");
                $('#asLots').val("");
                //$('#buscafiltro').prop('disabled', true);
            }
        }
    });

});

// Mostrar HVI Original/Modificado
$(document).ready(function() {
    $("input[type=radio]").click(function(event){
        var valor = $(event.target).val();
        if(valor =="HVIO"){
            $("#HVIOri").show();
            $("#HVIMod").show();
        } else if (valor == "HVIM") {
            $("#HVIOri").show();
            $("#HVIMod").show();
        }
    });
});

// Mostrar inputs segun los checkbox seleccionados
var GrdHVI, TaHVI, TcHVI, PcHVI, LenHVI, UiHVI, SfiHVI, StrHVI, ElgHVI, MicHVI, RDHVI, MatHVI, bHVI, ColHVI, MstHVI;
$(document).ready(function(){
    $('#Grd').on('change',function(){
        if (this.checked) {
            GrdHVI = 1;
            $("#divGrd").show();
        } else {
            GrdHVI = 0;
            $("#divGrd").hide();
        }  
    });
});

$(document).ready(function(){
    $('#TA').on('change',function(){
        if (this.checked) {
            TaHVI = 1;
            $("#divTA").show();
        } else {
            TaHVI = 0;
            $("#divTA").hide();
        }  
      });
});

$(document).ready(function(){
    $('#TC').on('change',function(){
        if (this.checked) {
            TcHVI = 1;
            $("#divTC").show();
        } else {
            TcHVI = 0;
            $("#divTC").hide();
        }  
      });
});

$(document).ready(function(){
    $('#PC').on('change',function(){
        if (this.checked) {
            PcHVI = 1;
            $("#divPC").show();
        } else {
            PcHVI =0;
            $("#divPC").hide();
        }  
      });
});

$(document).ready(function(){
    $('#Len').on('change',function(){
        if (this.checked) {
            LenHVI = 1;
            $("#divLen").show();
        } else {
            LenHVI = 0;
            $("#divLen").hide();
        }  
      });
});

$(document).ready(function(){
    $('#UI').on('change',function(){
        if (this.checked) {
            UiHVI = 1;
            $("#divUI").show();
        } else {
            UiHVI = 0;
            $("#divUI").hide();
        }  
      });
});

$(document).ready(function(){
    $('#SFI').on('change',function(){
        if (this.checked) {
            SfiHVI = 1;
            $("#divSFI").show();
        } else {
            SfiHVI = 0;
            $("#divSFI").hide();
        }  
      });
});

$(document).ready(function(){
    $('#Str').on('change',function(){
        if (this.checked) {
            StrHVI = 1;
            $("#divStr").show();
        } else {
            StrHVI = 0;
            $("#divStr").hide();
        }  
      });
});

$(document).ready(function(){
    $('#Elg').on('change',function(){
        if (this.checked) {
            ElgHVI = 1;
            $("#divElg").show();
        } else {
            ElgHVI = 0;
            $("#divElg").hide();
        }  
      });
});

$(document).ready(function(){
    $('#Mic').on('change',function(){
        if (this.checked) {
            MicHVI = 1;
            $("#divMic").show();
        } else {
            MicHVI = 0;
            $("#divMic").hide();
        }  
      });
});

$(document).ready(function(){
    $('#Mat').on('change',function(){
        if (this.checked) {
            MatHVI = 1;
            $("#divMat").show();
        } else {
            MatHVI = 0;
            $("#divMat").hide();
        }  
      });
});

$(document).ready(function(){
    $('#Fnn').on('change',function(){
        if (this.checked) {
            $("#divFnn").show();
        } else {
            $("#divFnn").hide();
        }  
      });
});

$(document).ready(function(){
    $('#Sgr').on('change',function(){
        if (this.checked) {
            $("#divSgr").show();
        } else {
            $("#divSgr").hide();
        }  
      });
});

$(document).ready(function(){
    $('#RD').on('change',function(){
        if (this.checked) {
            RDHVI = 1;
            $("#divRD").show();
        } else {
            RDHVI = 0;
            $("#divRD").hide();
        }  
      });
});

$(document).ready(function(){
    $('#B').on('change',function(){
        if (this.checked) {
            bHVI = 1;
            $("#divB").show();
        } else {
            bHVI = 0;
            $("#divB").hide();
        }  
      });
});

$(document).ready(function(){
    $('#Col').on('change',function(){
        if (this.checked) {
            ColHVI = 1;
            //$("#divCol").show();
        } else {
            ColHVI = 0;
            //$("#divCol").hide();
        }  
      });
});

$(document).ready(function(){
    $('#Temp').on('change',function(){
        if (this.checked) {
            $("#divTemp").show();
        } else {
            $("#divTemp").hide();
        }  
      });
});

$(document).ready(function(){
    $('#Mst').on('change',function(){
        if (this.checked) {
            MstHVI = 1;
            $("#divMst").show();
        } else {
            MstHVI = 0;
            $("#divMst").hide();
        }  
      });
});

//Descamrcar todos los checkbox
function uncheckAll() {
    document.querySelectorAll('#HVI input[type=checkbox]').forEach(function(checkElement) {
        checkElement.checked = false;
    });
}

document.addEventListener("DOMContentLoaded", function() {
    document.getElementById('desmarcarTodo').addEventListener('click', function(e) {
        e.preventDefault();
        uncheckAll();
        $("#divGrd,#divTA,#divTC,#divPC,#divLen,#divUI,#divSFI,#divStr,#divElg,#divMic,#divMat,#divFnn,#divSgr,#divRD,#divB,#divCol,#divTemp,#divMst").hide();
        GrdHVI =0; TaHVI=0; TcHVI=0; PcHVI=0; LenHVI=0, UiHVI=0, SfiHVI=0, StrHVI=0, ElgHVI=0, MicHVI=0, RDHVI=0, MatHVI=0, bHVI=0, ColHVI=0, MstHVI = 0;
    });
});

/*$(document).ready(function(){
    function myFunction() {
        var elmnt = document.getElementById("TextAreaHVIO");
        var elmnt2 = document.getElementById("TextAreaHVIM");
        var x = elmnt.scrollLeft;
        var y = elmnt.scrollTop;
        elmnt2.scrollLeft = x;
        elmnt2.scrollTop = y;
    }
});*/

$(document).ready(function(){
    $("#Search").click(function(){
        $('#TextAreaHVIO').val("");
        opcion = 5;
        LotU = $('#LotU').val();
        var applyFilter =  $.ajax({
            url: "bd/crudBales.php",
            type: "POST",
            datatype:"json",
            data:  { GrdHVI:GrdHVI, TaHVI:TaHVI, TcHVI:TcHVI, PcHVI:PcHVI, LenHVI:LenHVI, UiHVI:UiHVI, SfiHVI:SfiHVI, StrHVI:StrHVI, ElgHVI:ElgHVI, MicHVI:MicHVI, RDHVI:RDHVI, MatHVI:MatHVI, bHVI:bHVI, ColHVI:ColHVI, MstHVI:MstHVI, LotU:LotU, opcion:opcion },    
            success: function(data){
            
                console.log(data); //var vectorHVI = ['Grd', 'Micro', 'Leng'];
                opts = JSON.parse(data);

                // HVI Original
                var tablaGO = '<table id="TablaRecap" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;">';
                tablaGO += '<thead>';
                tablaGO += '<tr>';
                tablaGO += '<th>Property</th><th>Value</th><th>Counting</th>';
                tablaGO += '</tr>';
                tablaGO += '</thead>';
                tablaGO += '<tbody>';
                tr = '';
                i=0;
                
                var sumMic = 0, contMic = 0, sumBalMic = 0;
                for (var i = 0; i< opts.length; i++){
                    for (var j = 0; j< opts[i].length; j++){
                        if(typeof(opts[i][j].Col1s) !== "undefined" && GrdHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Grade</td><td>'+opts[i][j].Col1s+'</td><td>'+opts[i][j].Col1Bal+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Tars) !== "undefined" && TaHVI == 1){
                            tr += '<tr>';
                            tr += '<td>T. Area</td><td>'+opts[i][j].Tars+'</td><td>'+opts[i][j].TarBal+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Tgrds) !== "undefined" && TcHVI == 1){
                            tr += '<tr>';
                            tr += '<td>T. Code</td><td>'+opts[i][j].Tgrds+'</td><td>'+opts[i][j].TgrdBal+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Tcnts) !== "undefined" && PcHVI == 1){
                            tr += '<tr>';
                            tr += '<td>P. Count</td><td>'+opts[i][j].Tcnts+'</td><td>'+opts[i][j].TcntBal+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Unfs) !== "undefined" && UiHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Uniformity</td><td>'+opts[i][j].Unfs+'</td><td>'+opts[i][j].UnfBal+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Sfis) !== "undefined" && SfiHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Short Fiber Index</td><td>'+opts[i][j].Sfis+'</td><td>'+opts[i][j].SfiBal+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Strs) !== "undefined" && StrHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Strength</td><td>'+opts[i][j].Strs+'</td><td>'+opts[i][j].StrBal+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Elgs) !== "undefined" && ElgHVI == 1){
                        tr += '<tr>';
                        tr += '<td>Elongation</td><td>'+opts[i][j].Elgs+'</td><td>'+opts[i][j].ElgBal+'</td>';
                        tr += '</tr>';
                        }if(typeof(opts[i][j].Mics) !== "undefined" && MicHVI == 1){
                            sumMic = sumMic + opts[i][j].Mics;
                            sumBalMic = sumBalMic + opts[i][j].MicBal;
                            contMic = contMic + 1;
                            tr += '<tr>';
                            tr += '<td>Micronaire</td><td>'+opts[i][j].Mics+'</td><td>'+opts[i][j].MicBal+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Mats) !== "undefined" && MatHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Maturity</td><td>'+opts[i][j].Mats+'</td><td>'+opts[i][j].MatBal+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Rds) !== "undefined" && RDHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Rd</td><td>'+opts[i][j].Rds+'</td><td>'+opts[i][j].RdBal+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].bs) !== "undefined" && bHVI == 1){
                            tr += '<tr>';
                            tr += '<td>b</td><td>'+opts[i][j].bs+'</td><td>'+opts[i][j].bBal+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Color) !== "undefined" && ColHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Color</td><td>'+opts[i][j].Color+'</td><td>'+opts[i][j].BalCol+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Msts) !== "undefined" && MstHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Moist</td><td>'+opts[i][j].Msts+'</td><td>'+opts[i][j].MstBal+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Lens) !== "undefined" && LenHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Len</td><td>'+opts[i][j].Lens+'</td><td>'+opts[i][j].LenBal+'</td>';
                            tr += '</tr>';
                        }
                    }
                }
                tablaGO += tr;
                tablaGO += '</tbody></table>';


                // HVI Modificado FUNCIONAL -----------------------------------------------------------------------------------------------------------
                var tablaGM = '<table id="TablaRecap" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;">';
                tablaGM += '<thead>';
                tablaGM += '<tr>';
                tablaGM += '<th>Mod. Property</th><th>Mod. Value</th><th>Mod. Counting</th>';
                tablaGM += '</tr>';
                tablaGM += '</thead>';
                tablaGM += '<tbody>';
                tr = '';
                i=0;
                
                for (var i = 0; i< opts.length; i++){
                    for (var j = 0; j< opts[i].length; j++){
                        if(typeof(opts[i][j].Col1s_Mod) !== "undefined" && GrdHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Grade</td><td>'+opts[i][j].Col1s_Mod+'</td><td>'+opts[i][j].Col1Bal_Mod+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Tars_Mod) !== "undefined" && TaHVI == 1){
                            tr += '<tr>';
                            tr += '<td>T. Area</td><td>'+opts[i][j].Tars_Mod+'</td><td>'+opts[i][j].TarBal_Mod+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Tgrds_Mod) !== "undefined" && TcHVI == 1){
                            tr += '<tr>';
                            tr += '<td>T. Code</td><td>'+opts[i][j].Tgrds_Mod+'</td><td>'+opts[i][j].TgrdBal_Mod+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Tcnts_Mod) !== "undefined" && PcHVI == 1){
                            tr += '<tr>';
                            tr += '<td>P. Count</td><td>'+opts[i][j].Tcnts_Mod+'</td><td>'+opts[i][j].TcntBal_Mod+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Unfs_Mod) !== "undefined" && UiHVI ==1){
                            tr += '<tr>';
                            tr += '<td>Uniformity</td><td>'+opts[i][j].Unfs_Mod+'</td><td>'+opts[i][j].UnfBal_Mod+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Sfis_Mod) !== "undefined" && SfiHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Short Fiber Index</td><td>'+opts[i][j].Sfis_Mod+'</td><td>'+opts[i][j].SfiBal_Mod+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Strs_Mod) !== "undefined" && StrHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Strength</td><td>'+opts[i][j].Strs_Mod+'</td><td>'+opts[i][j].StrBal_Mod+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Elgs_Mod) !== "undefined" && ElgHVI == 1){
                        tr += '<tr>';
                        tr += '<td>Elongation</td><td>'+opts[i][j].Elgs_Mod+'</td><td>'+opts[i][j].ElgBal_Mod+'</td>';
                        tr += '</tr>';
                        }if(typeof(opts[i][j].Mics_Mod) !== "undefined" && MicHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Micronaire</td><td>'+opts[i][j].Mics_Mod+'</td><td>'+opts[i][j].MicBal_Mod+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Mats_Mod) !== "undefined" && MatHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Maturity</td><td>'+opts[i][j].Mats_Mod+'</td><td>'+opts[i][j].MatBal_Mod+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Rds_Mod) !== "undefined" && RDHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Rd</td><td>'+opts[i][j].Rds_Mod+'</td><td>'+opts[i][j].RdBal_Mod+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].bs_Mod) !== "undefined" && bHVI == 1){
                            tr += '<tr>';
                            tr += '<td>b</td><td>'+opts[i][j].bs_Mod+'</td><td>'+opts[i][j].bBal_Mod+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Color_Mod) !== "undefined" && ColHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Color</td><td>'+opts[i][j].Color_Mod+'</td><td>'+opts[i][j].BalCol_Mod+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Msts_Mod) !== "undefined" && MstHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Moist</td><td>'+opts[i][j].Msts_Mod+'</td><td>'+opts[i][j].MstBal_Mod+'</td>';
                            tr += '</tr>';
                        }if(typeof(opts[i][j].Lens_Mod) !== "undefined" && LenHVI == 1){
                            tr += '<tr>';
                            tr += '<td>Len</td><td>'+opts[i][j].Lens_Mod+'</td><td>'+opts[i][j].LenBal_Mod+'</td>';
                            tr += '</tr>';
                        }
                    }
                }
                tablaGM += tr;
                tablaGM += '</tbody></table>';
            
                $('#AreaOriginal').html(tablaGO);
                $('#AreaModificada').html(tablaGM);
            },
            error: function(data) {
                alert('error');
            }
        });
        applyFilter.done(function(data){
            $('#avisoUpdHVI').html('<div id="alert1" class="alert alert-success" style="width: 20rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i>Filters assigned successfully</div>')
                        setTimeout(function() {
                            $('#alert1').fadeOut(1000);
                            $('#avisoUpdHVI').html('');
                        },1500);
                        $("#Search").prop("disabled", false);
                        $("#Apply").prop("disabled", false);
        });
        $('#avisoUpdHVI').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');
        $("#Search").prop("disabled", true);
        $("#Apply").prop("disabled", true);
        return false;
    });
});

//Vector para validar que se seleccionen colores existentes y obtener posicion para extraer col2 de la matriz
var Colores = ['11', '12', '13', '21', '22', '23', '24', '25', '31', '32', '33', '34', '35', '41', '42', '43', '44', '51', '52', '53', '54', '61', '62', '63', '71', '81', '82', '83', '84', '85'];
//MAtriz para modificar Col2
var MCol2 = [
            ['','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'],
            ['4','','2','3','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'],
            ['4','2','','3','1','3','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'],
            ['4','2','2','','2','2','2','2','3','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'],
            ['4','2','2','4','','2','2','2','3','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'],
            ['4','2','4','4','2','','2','2','3','1','3','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'],
            ['4','2','4','4','2','4','','2','3','1','3','3','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'],
            ['4','2','4','4','2','4','4','','3','1','3','3','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'],
            ['4','2','2','4','2','2','2','2','','1','2','2','2','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'],
            ['4','2','2','4','2','2','2','2','4','','1','1','1','3','1','1','1','3','1','1','1','1','1','1','1','1','1','1','1','1'],
            ['4','2','4','4','2','4','2','2','4','2','','1','1','3','1','3','3','3','1','3','1','1','1','1','1','1','1','1','1','1'],
            ['4','2','4','4','2','4','4','2','4','2','4','','1','3','1','3','1','3','1','3','1','1','1','1','1','1','1','1','1','1'],
            ['4','2','4','4','2','4','4','2','4','2','3','4','','3','1','3','3','3','1','3','3','1','1','1','1','1','1','1','1','1'],
            ['4','2','4','4','2','2','2','2','4','2','2','2','2','','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'],
            ['4','2','4','4','2','2','2','2','4','2','2','2','2','4','','1','1','3','1','1','1','1','1','1','1','1','1','1','1','1'],
            ['4','2','4','4','2','4','2','2','4','2','4','2','2','4','2','','1','3','1','1','1','3','1','1','1','1','1','1','1','1'],
            ['4','2','4','4','2','4','4','2','4','2','4','4','2','4','2','4','','3','1','3','1','3','1','3','1','1','1','1','1','1'],
            ['4','2','4','4','2','2','2','2','4','2','2','2','2','4','2','2','2','','1','1','1','1','1','1','1','1','1','1','1','3'],
            ['4','2','4','4','2','2','2','2','4','2','2','2','2','4','2','2','2','4','','1','1','3','1','1','3','1','1','1','1','3'],
            ['4','2','4','4','2','4','2','2','4','2','4','2','2','4','2','4','2','4','2','','1','3','1','1','3','1','1','1','1','3'],
            ['4','2','4','4','2','4','4','2','4','2','4','4','2','4','2','4','4','4','2','4','','3','1','1','3','1','1','1','1','3'],
            ['4','2','4','4','2','2','2','2','4','2','2','4','2','4','2','2','2','4','2','2','2','','1','1','1','1','1','1','1','5'],
            ['4','2','4','4','2','2','2','2','4','2','2','4','2','4','2','2','2','4','2','2','2','4','','1','3','1','1','1','1','5'],
            ['4','2','4','4','2','4','2','2','4','2','4','4','2','4','2','4','4','4','2','4','2','4','2','','3','1','1','1','1','5'],
            ['4','2','4','4','2','2','2','2','4','2','4','4','2','4','2','4','4','4','2','4','4','4','2','2','','1','1','1','3','5'],
            ['4','2','4','4','2','2','4','2','4','2','4','4','2','4','2','4','4','4','2','4','4','4','2','4','4','','2','2','4','5'],
            ['4','2','4','4','2','2','4','2','4','2','4','4','2','4','2','4','4','4','2','4','4','4','2','2','3','2','','1','3','5'],
            ['4','2','4','4','2','4','4','2','4','2','4','4','2','4','2','4','4','4','2','4','4','4','2','2','4','2','2','','3','5'],
            ['4','2','4','4','2','4','4','2','4','2','4','4','2','4','2','4','4','4','2','4','4','4','2','4','4','2','2','3','','5'],
            ['4','2','4','4','2','4','4','2','4','2','4','4','2','4','2','4','4','4','2','4','4','4','2','4','4','2','2','3','4','']
            ];


$(document).ready(function(){

    $('#GrdMinO').on('change', function () {
        GrdMinO = $.trim($('#GrdMinO').val());
        if(GrdMinO != "" && jQuery.inArray(GrdMinO, Colores) == -1){
            alert ("Nonexistent Color");
            $("#GrdMinO").val("");
        }
    });

    $('#GrdMaxO').on('change', function () {
        GrdMaxO = $.trim($('#GrdMaxO').val());
        if(GrdMaxO != "" && jQuery.inArray(GrdMaxO, Colores) == -1){
            alert ("Nonexistent Color");
            $("#GrdMaxO").val("");
        }
    });

    $('#GrdMinM').on('change', function () {
        GrdMinM = $.trim($('#GrdMinM').val());
        if(GrdMinM != "" && jQuery.inArray(GrdMinM, Colores) == -1){
            alert ("Nonexistent Color");
            $("#GrdMinM").val("");
        }
    });

    $('#GrdMaxM').on('change', function () {
        GrdMaxM = $.trim($('#GrdMaxM').val());
        if(GrdMaxM != "" && jQuery.inArray(GrdMaxM, Colores) == -1){
            alert ("Nonexistent Color");
            $("#GrdMaxM").val("");
        }
    });

});

$(document).ready(function(){
    $("#Apply").click(function(){
        LotU = $('#LotU').val();

        GrdMinO = $.trim($('#GrdMinO').val());
        GrdMaxO = $.trim($('#GrdMaxO').val());
        GrdMinM = $.trim($('#GrdMinM').val());
        GrdMaxM = $.trim($('#GrdMaxM').val());
        GrdRange = $.trim($('#GrdRange').val());
        
        //Buscar Col2
        if (GrdHVI == 1){
          if (GrdMinO != GrdMaxO){
              var y = jQuery.inArray(GrdMinO, Colores);
              var y1 = jQuery.inArray(GrdMaxO, Colores);
              var x = jQuery.inArray(GrdMinM, Colores);
              var Col2 = MCol2[y][x];
              var Col21 = MCol2[y1][x];
          }else{
              var y = jQuery.inArray(GrdMinO, Colores);
              var x = jQuery.inArray(GrdMinM, Colores);
              Col2 = MCol2[y][x];
              col21 = Col2
          }
        }

        TaMinO = $.trim($('#TAMinO').val());
        TaMaxO = $.trim($('#TAMaxO').val());
        TaMinM = $.trim($('#TAMinM').val());
        TaMaxM = $.trim($('#TAMaxM').val());
        TaRange = $.trim($('#TARange').val());

        TcMinO = $.trim($('#TCMinO').val());
        TcMaxO = $.trim($('#TCMaxO').val());
        TcMinM = $.trim($('#TCMinM').val());
        TcMaxM = $.trim($('#TCMaxM').val());
        TcRange = $.trim($('#TCRange').val());

        PcMinO = $.trim($('#PCMinO').val());
        PcMaxO = $.trim($('#PCMaxO').val());
        PcMinM = $.trim($('#PCMinM').val());
        PcMaxM = $.trim($('#PCMaxM').val());
        PcRange = $.trim($('#PCRange').val());

        LenMinO = $.trim($('#LenMinO').val());
        LenMaxO = $.trim($('#LenMaxO').val());
        LenMinM = $.trim($('#LenMinM').val());
        LenMaxM = $.trim($('#LenMaxM').val());
        LenRange = $.trim($('#LenRange').val());

        UiMinO = $.trim($('#UIMinO').val());
        UiMaxO = $.trim($('#UIMaxO').val());
        UiMinM = $.trim($('#UIMinM').val());
        UiMaxM = $.trim($('#UIMaxM').val());
        UiRange = $.trim($('#UIRange').val());

        SfiMinO = $.trim($('#SFIMinO').val());
        SfiMaxO = $.trim($('#SFIMaxO').val());
        SfiMinM = $.trim($('#SFIMinM').val());
        SfiMaxM = $.trim($('#SFIMaxM').val());
        SfiRange = $.trim($('#SFIRange').val());

        StrMinO = $.trim($('#StrMinO').val());
        StrMaxO = $.trim($('#StrMaxO').val());
        StrMinM = $.trim($('#StrMinM').val());
        StrMaxM = $.trim($('#StrMaxM').val());
        StrRange = $.trim($('#StrRange').val());

        ElgMinO = $.trim($('#ElgMinO').val());
        ElgMaxO = $.trim($('#ElgMaxO').val());
        ElgMinM = $.trim($('#ElgMinM').val());
        ElgMaxM = $.trim($('#ElgMaxM').val());
        ElgRange = $.trim($('#ElgRange').val());

        MicMinO = $.trim($('#MicMinO').val());
        MicMaxO = $.trim($('#MicMaxO').val());
        MicMinM = $.trim($('#MicMinM').val());
        MicMaxM = $.trim($('#MicMaxM').val());
        MicRange = $.trim($('#MicRange').val());

        MatMinO = $.trim($('#MatMinO').val());
        MatMaxO = $.trim($('#MatMaxO').val());
        MatMinM = $.trim($('#MatMinM').val());
        MatMaxM = $.trim($('#MatMaxM').val());
        MatRange = $.trim($('#MatRange').val());

        RdMinO = $.trim($('#RDMinO').val());
        RdMaxO = $.trim($('#RDMaxO').val());
        RdMinM = $.trim($('#RDMinM').val());
        RdMaxM = $.trim($('#RDMaxM').val());
        RdRange = $.trim($('#RDRange').val());

        bMinO = $.trim($('#BMinO').val());
        bMaxO = $.trim($('#BMaxO').val());
        bMinM = $.trim($('#BMinM').val());
        bMaxM = $.trim($('#BMaxM').val());
        bRange = $.trim($('#BRange').val());

        MstMinO = $.trim($('#MstMinO').val());
        MstMaxO = $.trim($('#MstMaxO').val());
        MstMinM = $.trim($('#MstMinM').val());
        MstMaxM = $.trim($('#MstMaxM').val());
        MstRange = $.trim($('#MstRange').val());

        opcion = 6;

        //alert(MicMinO+"|"+MicMaxO+"|"+MicMinM+"|"+MicMaxM+"|"+MicRange+"|"+opcion);
        
        var applyFilter = $.ajax({
            url: "bd/crudBales.php",
            type: "POST",
            datatype:"json",    
            data:  {
                GrdHVI:GrdHVI, 
                TaHVI:TaHVI, 
                TcHVI:TcHVI, 
                PcHVI:PcHVI, 
                LenHVI:LenHVI, 
                UiHVI:UiHVI, 
                SfiHVI:SfiHVI, 
                StrHVI:StrHVI, 
                ElgHVI:ElgHVI, 
                MicHVI:MicHVI, 
                RDHVI:RDHVI, 
                MatHVI:MatHVI, 
                bHVI:bHVI, 
                ColHVI:ColHVI, 
                MstHVI:MstHVI,

                GrdMinO:GrdMinO,
                GrdMaxO:GrdMaxO,
                GrdMinM:GrdMinM,
                GrdMaxM:GrdMaxM,
                GrdRange:GrdRange,

                Col2:Col2,
                Col21:Col21,

                TaMinO:TaMinO,
                TaMaxO:TaMaxO,
                TaMinM:TaMinM,
                TaMaxM:TaMaxM,
                TaRange:TaRange,

                TcMinO:TcMinO,
                TcMaxO:TcMaxO,
                TcMinM:TcMinM,
                TcMaxM:TcMaxM,
                TcRange:TcRange,

                PcMinO:PcMinO,
                PcMaxO:PcMaxO,
                PcMinM:PcMinM,
                PcMaxM:PcMaxM,
                PcRange:PcRange,

                LenMinO:LenMinO,
                LenMaxO:LenMaxO,
                LenMinM:LenMinM,
                LenMaxM:LenMaxM,
                LenRange:LenRange,

                UiMinO:UiMinO,
                UiMaxO:UiMaxO,
                UiMinM:UiMinM,
                UiMaxM:UiMaxM,
                UiRange:UiRange,

                SfiMinO:SfiMinO,
                SfiMaxO:SfiMaxO,
                SfiMinM:SfiMinM,
                SfiMaxM:SfiMaxM,
                SfiRange:SfiRange,

                StrMinO:StrMinO,
                StrMaxO:StrMaxO,
                StrMinM:StrMinM,
                StrMaxM:StrMaxM,
                StrRange:StrRange,

                ElgMinO:ElgMinO,
                ElgMaxO:ElgMaxO,
                ElgMinM:ElgMinM,
                ElgMaxM:ElgMaxM,
                ElgRange:ElgRange,
                
                MicMinO:MicMinO,
                MicMaxO:MicMaxO,
                MicMinM:MicMinM,
                MicMaxM:MicMaxM,
                MicRange:MicRange,

                MatMinO:MatMinO,
                MatMaxO:MatMaxO,
                MatMinM:MatMinM,
                MatMaxM:MatMaxM,
                MatRange:MatRange,

                RdMinO:RdMinO,
                RdMaxO:RdMaxO,
                RdMinM:RdMinM,
                RdMaxM:RdMaxM,
                RdRange:RdRange,

                bMinO:bMinO,
                bMaxO:bMaxO,
                bMinM:bMinM,
                bMaxM:bMaxM,
                bRange:bRange,

                MstMinO:MstMinO,
                MstMaxO:MstMaxO,
                MstMinM:MstMinM,
                MstMaxM:MstMaxM,
                MstRange:MstRange,
                
                LotU: LotU,
                opcion: opcion 
            },    
            success: function(data) {
              //tablaBalesMod.ajax.reload(null, false);
             }
        });
        applyFilter.done(function(data){
            $('#avisoUpdHVI').html('<div id="alert1" class="alert alert-success" style="width: 20rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i>Filtros asignados con éxito</div>')
                        setTimeout(function() {
                            $('#alert1').fadeOut(1000);
                            $('#avisoUpdHVI').html('');
                        },1500);
                        $("#Apply").prop("disabled", false);
                        $("#Search").prop("disabled", false);
        });
        $('#avisoUpdHVI').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');
        $("#Apply").prop("disabled", true);
        $("#Search").prop("disabled", true);
        return false;

        /*var applyChanges =  $.ajax({
            type: 'POST',
            url: 'bd/crudBales.php',
            data: {
                MicMinO: MicMinO,
                MicMaxO:MicMaxO,
                MicMinM: MicMinM,
                MicMaxM:MicMaxM,
                MicRange: MicRange,
                LotU: LotU,
                opcion: opcion 
            }
            
        })*/

    });

});

// Boton enviar valores a cero
$(document).ready(function(){
    $("#ValorCero").click(function(){
        $("#TA").prop("checked",true);
        $("#PC").prop("checked",true);
        $("#Elg").prop("checked",true);
        $("#Mst").prop("checked",true);
        TaHVI = 1;
        PcHVI = 1;
        ElgHVI = 1;
        MstHVI = 1;

        $("#divTA").show();
        $("#divPC").show();
        $("#divElg").show();
        $("#divMst").show();

        $("#TAMinO").val(0);
        $("#TAMinM").val(0);
        $("#PCMinO").val(0);
        $("#PCMinM").val(0);
        $("#ElgMinO").val(0);
        $("#ElgMinM").val(0);
        $("#MstMinO").val(0);
        $("#MstMinM").val(0);

    });

});


$(document).ready(function(){
    $("#HVIExportPDF").click(function(){
        DOCli = $.trim($('#DOCli').val());
        LotsCli = $.trim($('#LotsCli').val());
        CrpCli = $.trim($('#CrpCli').val());
        TypHVI = $.trim($('#TypHVI').val());
        Cli = $.trim($('#CliHVI').val().toUpperCase());
        if (DOCli != "" || LotsCli != ""){
        if (Cli == ""){
            alert ("HVI without assigned client!");
        }
        window.open("./bd/HVIPDF.php?DO="+DOCli+"&Lots="+LotsCli+"&Crp="+CrpCli+"&TypHVI="+TypHVI+"&Cli="+Cli+"");
        }else{
            alert ("No lots filtered...");
        }
    });
});

$(document).ready(function(){
    $("#HVIExportExcel").click(function(){
        DOCli = $.trim($('#DOCli').val());
        LotsCli = $.trim($('#LotsCli').val());
        CrpCli = $.trim($('#CrpCli').val());
        TypHVI = $.trim($('#TypHVI').val());
        Cli = $.trim($('#CliHVI').val().toUpperCase());
        if (DOCli != "" || LotsCli != ""){
        if (Cli == ""){
            alert ("HVI without assigned client!");
        }
        window.open("./bd/HVIExcel.php?DO="+DOCli+"&Lots="+LotsCli+"&Crp="+CrpCli+"&TypHVI="+TypHVI+"&Cli="+Cli+"");
        }else{
            alert ("No lots filtered...");
        }
    });
});



$(document).ready(function(){
    $("#HVIRecap").click(function(){
        DOCli = $.trim($('#DOCli').val());
        LotsCli = $.trim($('#LotsCli').val());
        CrpCli = $.trim($('#CrpCli').val());
        TypHVI = $.trim($('#TypHVI').val());
        Cli = $.trim($('#CliHVI').val().toUpperCase());
        if (DOCli != "" || LotsCli != ""){
        if (Cli == ""){
            alert ("HVI without assigned client!");
        }
        window.open("./bd/HVIRecap2.php?DO="+DOCli+"&Lots="+LotsCli+"&Crp="+CrpCli+"&TypHVI="+TypHVI+"&Cli="+Cli+"");
        }else{
            alert ("No lots filtered...");
        }
    });
});

//bloquear inputs dependiendo de DO y GrdMinM
$(document).ready(function(){
    $('#DO, #GrdMinM').on('change keyup', function(){
        DO = $('#DO').val();
        GrdMinM = $('#GrdMinM').val();

        if(DO != ""){
            $('#Lot, #Bale').val("");
            $('#Reg, #Gin, #Crp').prop('selectedIndex',0);
            $('#Lot').prop('disabled',true);
            $('#Reg').prop('disabled',true);
            $('#Gin').attr('disabled',true);
            $('#Bale').attr('disabled',true);
            $('#Crp').attr('disabled',true);
        }else{
            $('#Lot').attr('disabled',false);
            $('#Reg').attr('disabled',false);
            $('#Gin').attr('disabled',false);
            $('#Bale').attr('disabled',false);
            $('#Crp').attr('disabled',false);
        }

        if(GrdMinM != ""){
            $('#GrdMaxM').val(GrdMinM);
            $('#GrdMaxM').prop('disabled',true);
            $('#GrdRange').val("");
            $('#GrdRange').prop('disabled',true);
        }else{
            $('#GrdMaxM').val("");
            $('#GrdMaxM').prop('disabled',false);
            $('#GrdRange').val("");
            $('#GrdRange').prop('disabled',false);
        }
    });
});

$(document).ready(function(){
    $('#Reg').on('change',function(){
        $("#Gin").empty();
        Reg = $.trim($('#Reg').val());    
        var opts = "";
        opcion = 7;
        //alert (Reg);
        $.ajax({
            url: "bd/crudBales.php",
            type: "POST",
            datatype:"json",
            data:  {Reg:Reg, opcion:opcion},    
            success: function(data){
                opts = JSON.parse(data);
                //$('#GinID').append('<option value="" disabled selected >- Select -</option>');  //$('#GinID').append('<option value="0">- Select Gin -</option disabled>');
                for (var i = 0; i< opts.length; i++){
                    $('#Gin').append('<option value="' + opts[i].IDGin + '">' + opts[i].GinName + '</option>');
                }        
            },
            error: function(data) {
                alert('error');
            }
        });
    });
    
});


// ----- Validar los archivos seleccionados -------
$(document).ready(function(){
    $('#csvU, #csvULots').on('change',function(){
        csvU = $.trim($('#csvU').val());
        csvULots = $.trim($('#csvULots').val());

        if (csvU != "" && csvULots != ""){
            File1 = csvU.substring(12,16);  //la lectuta comienza desde el caracter 12 para evitar el fakepath por default
            File2 = csvULots.substring(12,19);

            // --- obtener el lote del archivo
            LotFile1 = csvU.substring(12).split(".");
            LotFile1 = LotFile1[0];
            LotFile1 = LotFile1.split("s");
            LotFile1 = LotFile1[1];

            LotFile2 = csvULots.substring(12).split("-");
            LotFile2 = LotFile2[0];
            LotFile2 = LotFile2.split("s");
            LotFile2 = LotFile2[1];

            if (File1 != "Lots"){
                $('#FailFiles').html("<small id='VFiles' class='form-text text-muted'>First file isn't valid.</small>");
                $('#btnGuardarFileU').attr('disabled', true);
            }else if (File2 != "RcpLots"){
                $('#FailFiles').html("<small id='VFiles' class='form-text text-muted'>Second file isn't valid.</small>");
                $('#btnGuardarFileU').attr('disabled', true);
            }else if (LotFile1 != LotFile2){
                $('#FailFiles').html("<small id='VFiles' class='form-text text-muted'>Lots don't match.</small>");
                $('#btnGuardarFileU').attr('disabled', true);
            }else{
                $('#FailFiles').html("<small id='VFiles' class='form-text text-muted'>Correct files.</small>");
                $('#btnGuardarFileU').attr('disabled', false);
            }
        }
        
    });
    
});


// ----- recortar nombre del archivo para obtener el location de RecapLots -------
$(document).ready(function(){
    $('#csvULots').on('change',function(){
        
        Loc = $.trim($('#csvULots').val());
        if (Loc != ""){
            var res = Loc.split('-')[1];
            var res2 = res.replace(".csv","");
            $('#LocLot').val(res2);
        }
        
    });
    
});

// ----- recortar nombre del archivo para obtener el supplier de Bales -------
$(document).ready(function(){
    $('#csv').on('change',function(){
        
        Sup = $.trim($('#csv').val());
        if (Sup != ""){
            var res = Sup.split('-')[2];
            var res2 = res.replace(".csv","");
           // $('#Sup').val(res2);
        }
        
    });
    
});

// ----- recortar nombre del archivo para obtener el Gin Carga Bales -------
$(document).ready(function(){
    $('#csv').on('change',function(){
        
        Gin = $.trim($('#csv').val());
        if (Gin != ""){
            var res = Gin.split('-')[2];
            var res2 = res.replace(".csv","");
           // $('#GinCB').val(res2);
        }
        
    });
    
    $('#CheckDev').on('change',function(){
        if (this.checked) {
            Dev = 1;
        }else{
            Dev = 0;
        }
    });
    
});


//FUNCIONES PARA LIQUIDACIONES PROVICIONALES

$(document).ready(function(){    
    $("#Provisional").click(function(){

        $("#formLiqprov").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Upload Provisional Data");
        $('#Liqprov').modal('show');
        $('#avisoTemp').html('');
        $("#AlertaSup").css("display", "none");   
        $('#Supnom').val("");
        
    });

    $("#SupID").on('change keyup',function(){
      //  ValidateOutWgh();
        buscarValorSup($(this).val());
    });


    $('#csvprov').on('change',function(e){
        
        Gin = $.trim($('#csvprov').val());   

        validacsv=validar_file_csv(Gin);
        if(validacsv == 1){            
                var $el = $('#csvprov');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap(); 
                $('#SupID').val(""); 
                $('#Supnom').val("");
        }
        else{
            if (Gin != ""){
                var res = Gin.split('-')[3];
                var res2 = res.replace(".csv","");
                $('#SupID').val(res2);
               // $('#SupID').val(res2);
                $('#Supnom').val(res2);
                buscarValorSup(res2);

                //OBTENER EL PRIMER DATO DE LA COLUMNA GINID DEL ARCHIVO
                const file = e.target.files[0];
                const reader = new FileReader();

                reader.onload = function(event) {
                    const content = event.target.result;
                    const rows = content.split('\n'); // Separar por saltos de línea

                    // Obtener el segundo dato de la segunda columna
                    let secondDataSecondColumn = null;
                    for (let i = 0; i < rows.length; i++) {
                        const columns = rows[i].split(','); 
                        if (columns.length >= 2) {
                            if (i === 1) { // Verificar si es la segunda fila (0-indexed)
                                secondDataSecondColumn = columns[1]; // Segundo dato de la segunda columna
                                break; // Detenerse después de encontrar el segundo dato
                            }
                        }
                    }

                    //SELECCIONAR EL GINID EN EL SELECTOR
                    $('#GinName').val(secondDataSecondColumn); 
                }

                reader.readAsText(file);        

            }
        }
        
    });


    //enviar el csv a php para procesarlo 
    $('#formLiqprov').submit(function(e){
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
        var comprobar = $('#csvprov').val().length;
        SupID= $('#SupID').val();

        if (comprobar>0){
            var formulario = $("#formLiqprov");
            
            var archivos = new FormData();
        
                for (var i=0; i<(formulario.find('input[type=file]').length); i++){
                    archivos.append((formulario.find('input[type="file"]:eq('+i+')').attr("name")),((formulario.find('input[type="file"]:eq('+i+')'))[0].files[0]));
                }
            archivos.append('Sup',SupID);
            var applyFilter = $.ajax({
                url: "bd/LiqTemp.php",
                type: "POST",
                contentType: false,
                data:  archivos,
                processData: false,
                success: function(dat) {
                    datos = JSON.parse(dat);
                    data = datos['Status'];
                    cont = datos['cont'];
                    upd = datos['upd'];           


                    if (data == 'Columns'){
                        $('#avisoTemp').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Check number of columns. </div>');
                        setTimeout(function() {
                            $('#alert1').fadeOut(1000);
                            //$('#filtrarmodal').modal('hide');
                        },1000);
                        return false;
                    }else if (data == 'Exist'){
                        alert ("Existen los lotes");
                        return false;
    
                
                    }    
                    else{

                        $('#avisoTemp').html('<div id="alert1" class="alert alert-success" style="width: 24rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i>Bales Created: '+cont+'; Updated records: '+upd+'</div>');
                
                        return false;
                    }
                }
            });
            $('#avisoTemp').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');
            return false;    
        }else{
            $('#avisoTemp').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Select csv file to import </div>');
                    setTimeout(function() {
                        $('#alert1').fadeOut(1000);
                        //$('#filtrarmodal').modal('hide');
                    },1000);
            //alert ("Select csv file to import");
            return false;
        }
        //}   	        
    });
  
    
});



//FUNCIONES PARA ASIGNAR LOS HVI A CLIENTE/ CARGAR HVI EXCEL Y PDF A BUCKET

$(document).ready(function(){ 

    $("#loadhvi").click(function(){
         
         $('#correoamsa').val('');
        lista_correos='jvizcaya@ecomtrading.com,carmas@ecomtrading.com,luis.rendon@ecomtrading.com'

        $("#formLiqprov").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $('#modalcargahvi').modal('show');
        $('#avisocargahvi').html('');      
        lotesingresados=$('#LotsCli').val();

        $('#loteshvi ').val(lotesingresados);
        $('#correoamsa').val(lista_correos);
        

        
    });

    


    $("#loadhvi").click(function(){

        $("#formLiqprov").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $('#modalcargahvi').modal('show');
        $('#avisocargahvi').html('');      
        lotesingresados=$('#LotsCli').val();

        $('#loteshvi ').val(lotesingresados);
        $("#adjuntarpdf").prop("checked",true);
        $("#adjuntarexcel").prop("checked",true);


        
    });

    $("#planta").change(function () {
        console.log($(this).val());
        if ($(this).val() != ''){
            getmails($(this).val());
        }
        else{
            $("#correocliente").val("");  
        }
    });   



    $("#asignarlote").click(function(e){
        //e.preventDefault(); 
        e.stopPropagation();
        usuario = $('#dropdownMenuLink2').text();
        opcion = 1;
        planta =  $('#planta').val();
        lotes = $('#loteshvi').val();    
        Regionlote  = $('#Regionlote').val();          
        Nombreplanta=$('#planta').children("option:selected").text();
        comentarios = $('#comentarioscorreo').val();    
        correoamsa= $('#correoamsa').val();
        correocliente= $("#correocliente").val(); 
        var adjuntarpdf = 0;
        var adjuntarexcel = 0;

        if($('#adjuntarpdf').prop('checked')){
            adjuntarpdf = 1
        }
        else{
            adjuntarpdf = 0
        }

        if($('#adjuntarexcel').prop('checked')){
            adjuntarexcel = 1
        }
        else{
            adjuntarexcel = 0
        }
         //validar que se marque por lo menos un check 
         if(adjuntarexcel == 0 && adjuntarpdf == 0){
            alert("Debe adjuntar por lo menos un archivo")
        }
        else{
            var continuar = confirm("¿Enviar y asignar lotes cliente "+Nombreplanta+"?");
            if (continuar) {
                $('#avisocargahvi').html('');
                $('#avisocargahvi').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');

                //cargar hvi pdf

                $.ajax({
                    url: "loadHVIPDF.php",
                    type: "POST",
                    datatype:"json",
                    data:  {planta:planta, lotes:lotes},    
                    success: function(data){
                    
                        return false;
            
                    },
                    error: function(data) {
                        alert('error');
                    }
                });

                //cargar hvi excel

                $.ajax({
                    url: "loadHVIExcel.php",
                    type: "POST",
                    datatype:"json",
                    data:  {planta:planta, lotes:lotes},    
                    success: function(data){
                        
                        return false;
            
                    },
                    error: function(data) {
                        alert('error');
                    }
                });

                $.ajax({
                    url: "bd/CrudAsign.php",
                    type: "POST",
                    datatype:"json",
                    data:  {opcion:opcion,planta:planta, lotes:lotes},    
                    success: function(data){
                        opts = JSON.parse(data);

                        console.log(opts);
                        cadena='Lotes asiganados: ' + opts

                        if (opts != 0){
                        
                            $.ajax({
                                
                                url: "SendHVIPDF.php",
                                type: "POST",
                                datatype:"json",
                                data:  {planta:planta, lotes:lotes, comentarios:comentarios, usuario:usuario, correoamsa:correoamsa, correocliente:correocliente, adjuntarexcel:adjuntarexcel,adjuntarpdf:adjuntarpdf},    
                                success: function(data){
                                    
                                    alert ("HVI enviados")
                        
                                },
                                error: function(data) {
                                    alert('error');
                                },async:false
                            });
                            $('#avisocargahvi').html('');
                            $('#avisocargahvi').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i>'+cadena+'</div>');
                            setTimeout(function() {                
                                //$('#filtrarmodal').modal('hide');
                            },1000);
                            return false;

                        }
                        else{

                            $('#avisocargahvi').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i>'+'No se asignaron lotes'+'</div>');
                        }
                        
            
                    },
                    error: function(data) {
                        alert('error');
                    }
                });            
            }
            else {

                alert("No se envio ningun archivo")
            }

        }


 

    }); 
    
    

});


//funcion que valida el archivo que se selecciona 

function validar_file_csv(nombre_file){
      
        // Obtener extensión del archivo
        extension = nombre_file.substring(nombre_file.lastIndexOf('.'),nombre_file.length);
        //Valida la extencion del archivo cargado
      //  console.log(extension);
        if(extension !==".csv") {
          alert('Archivo inválido. No se permite la extensión ' + extension);
          return 1;
        }
      
}

//funcion para buscar el supplier 

function buscarValorSup(Supplier) {
    var select = document.getElementById("Supnom");
    valor=Supplier.toUpperCase()
    for (var i = 0; i < select.options.length; i++) {
      if (select.options[i].value === valor) {
        // Se encontró el valor       
        $("#Supnom").val(valor);
        $("#AlertaSup").css("display", "none");
        // Aquí puedes realizar alguna acción con el valor encontrado
        break; // Rompe el bucle una vez que se encuentra el valor
      }
      else{
        if(Supplier!=""){        
            $("#AlertaSup").css("display", "block");
        }
        else{
            $("#AlertaSup").css("display", "none");
            $("#Supnom").val("");
        }
      }
    }
  }



function buscarValorSupNew(Supplier) {
    var select = document.getElementById("Sup");
    valor=Supplier.toUpperCase()
    for (var i = 0; i < select.options.length; i++) {
      if (select.options[i].value === valor) {
        // Se encontró el valor       
        $("#Sup").val(valor);
        $("#AlertaSupNew").css("display", "none");
        // Aquí puedes realizar alguna acción con el valor encontrado
        break; // Rompe el bucle una vez que se encuentra el valor
      }
      else{
        if(Supplier!=""){        
            $("#AlertaSupNew").css("display", "block");
        }
        else{
            $("#AlertaSupNew").css("display", "none");
            $("#SupNew").val("");
        }
      }
    }
}

function getmails(planta){
    $("#correocliente").val("");  
    opcion = 2;
    $.ajax({
        url: "bd/CrudAsign.php",
        type: "POST",
        datatype:"json",
        data:  {planta:planta, opcion:opcion},    
        success: function(data){
            datos = JSON.parse(data);
            $("#correocliente").val(datos[0]['CorreoHVI']); 
  
        },
        error: function(data) {
            alert('error');
        }
    });

}

