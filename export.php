<?php
session_start();

if (!isset($_SESSION['username'])) :
    include_once('index.php');
else :
    $Typ_Exp = $_SESSION['Typ_Exp'];
    $permiso = $_SESSION['Admin'];

?>

    <!doctype html>
    <html lang="es en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="img/ecom.png" />
        <title>Container</title>

        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="../portalLogistica/assets/bootstrap/css/bootstrap.min.css">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="../portalLogistica/main.css">
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>

    <body>

        <!-- Scripts -->

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>


        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <!-- librerias necesarias para finalizar sesion por inactividad -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
        <!-- Scrip para finalizar sesion por inactividad -->
        <script type="text/javascript" src="timer.js"></script>        

        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>

        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
        
        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>

        <!-- Codigo para realizar funciones relacionadas con la DO dentro del formulario-->
        <script type="text/javascript">
            $(document).ready(function() {
                $("#DO").keyup(function() {})

                function update() {
                    $("#Lots").empty();
                    $('#Gin').val("");
                    $('#OutReg').val("");
                    $('#Ctc').val("");
                    $('#Cli').val("");
                    var opts = "";
                    opcion = 5;
                    DO = $.trim($('#DO').val());
                    $.ajax({
                        url: "bd/crudExport.php",
                        type: "POST",
                        datatype: "json",
                        data: {
                            DO: DO,
                            opcion: opcion
                        },
                        success: function(data) {
                            opts = JSON.parse(data);
                            $('#Lots').append('<option value="0">-Select Lot-</option disabled>');
                            for (var i = 0; i < opts.length; i++) {
                                 $('#Lots').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot +"-" +opts[i].Qty+ " bc"+'</option>');
                            }
                            $('#Gin').val(opts[0].Gin);
                            $('#OutReg').val(opts[0].OutReg);
                            $('#Ctc').val(opts[0].Ctc);
                            $('#Cli').val(opts[0].Cli);
                        },
                        error: function(data) {
                            alert('error');
                        }
                    });
                }

                $("#DO").on("change", function() {
                    update();
                });
            });
        </script>
        <?php if ($Typ_Exp == 1) { ?>
            <script type="text/javascript" src="mainExport.js"></script>
        <?php } else { ?>
            <script type="text/javascript" src="mainER.js"></script>
        <?php } ?>
        

        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>

                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <li><a class="dropdown-item" href="trucks.php">Trucks</a></li>
                        <li><a class="dropdown-item" href="lots.php">Lots</a></li>
                        <?php if ($_SESSION['username'] == 'ISELA LARA' || $_SESSION['username'] == 'DARIO ROMERO' || $_SESSION['username'] == 'CESAR ARMAS' || $_SESSION['username'] == 'LUIS ANGEL RENDON TARIN' || $_SESSION['username'] == 'JOSE RENDON'){?>
                        <li><a class="dropdown-item" href="bales.php">Bales</a></li>
                        <?php } ?>
                        <li><a class="dropdown-item" href="do.php">Delivery Order</a></li>
                        <li><a class="dropdown-item" href="adjustments.php">Adjustments</a></li>
                        <li><a class="dropdown-item" href="inv.php">Invoice</a></li>
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="#">Exports</a></li>
                        <li><a class="dropdown-item" href="clients.php">Clients</a></li>
                        <li><a class="dropdown-item" href="routes.php">Routes</a></li>
                        <li><a class="dropdown-item" href="requisition.php">Requisition</a></li>
                        <li><a class="dropdown-item" href="CMS.php">CMS</a></li>
                        <?php if ($permiso == 1){?>
                        <li><a class="dropdown-item" href="adminusers.php">User Management</a></li>
                        <?php } ?>
                    </ul>
                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-2" href="main.php">
                    <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <p>/ Container</p>
                </div>
                <div class="container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                    <!-- Botones -->
                    <?php if ($Typ_Exp == 1) : ?>
                        <button id="btnNuevo" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Add"><i class="bi bi-plus-square"></i></button>
                        <button id="btnReporte" type="button" class="btn btn-success" data-toggle="modal tooltip" data-placement="bottom" title="Packing Lists"><i class="bi bi-file-earmark-arrow-down-fill"></i></button>
                        <?php endif ?>
                </div>
                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                    <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"><?php echo $_SESSION['username']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <!-- Aquí inicia todo código de tablas etc -->

        <!-- Inicio de la tabla -->
        <div class="card card-body" style="opacity:100%;">
            <div class="table-responsive" style="opacity:100%;">
                <table id="tablaExports" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;">
                    <thead style="background-color: #65ac7c;" style="opacity:100%;">
                        <tr>
                            <th class="th-sm">Container ID</th>
                            <th class="th-sm">Port of Loading</th>
                            <th class="th-sm">Shipping Company</th>
                            <th class="th-sm">Booking</th>
                            <th class="th-sm">Container No.</th>
                            <th class="th-sm">Seal</th>
                            <th class="th-sm">Delivery Order</th>
                            <th class="th-sm">Lot</th>
                            <!--                            <th class="th-sm">Truck ID</th>-->
                            <th class="th-sm">Quantity</th>
                            <th class="th-sm">Scheduled Date</th>
                            <th class="th-sm">Arrival Date</th>
                            <th class="th-sm">Arrival Time</th>
                            <th class="th-sm">Departure Date</th>
                            <th class="th-sm">Departure Time</th>
                            <th class="th-sm">Gin</th>
                            <th class="th-sm">Departure Region</th>
                            <th class="th-sm">Contract</th>
                            <th class="th-sm">Client</th>
                            <th class="th-sm">Status Container</th>
                            <th class="th-sm">Transport</th>
                            <th class="th-sm">CAAT</th>
                            <th class="th-sm">Waybill</th>
                            <th class="th-sm">Trk Plate</th>
                            <th class="th-sm">Trailer Plate</th>
                            <th class="th-sm">Driver Name</th>
                            <th class="th-sm">Purchase Weight</th>
                            <th class="th-sm">Departure Weight</th>
                            <th class="th-sm">Arrival Weight</th>
                            <th class="th-sm">Invoice No.</th>
                            <th class="th-sm">Comments</th>
                            <th class="th-sm">Status Truck</th>
                            <th class="th-sm">TrkID</th>
                            <?php if ($Typ_Exp == 1) { ?>
                                <th class="no-exportar"></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="th-sm">Container ID</th>
                            <th class="th-sm">Port of Loading</th>
                            <th class="th-sm">Shipping Company</th>
                            <th class="th-sm">Booking</th>
                            <th class="th-sm">Container No.</th>
                            <th class="th-sm">Seal</th>
                            <th class="th-sm">Delivery Order</th>
                            <th class="th-sm">Lot</th>
                            <!--                            <th class="th-sm">Truck ID</th>-->
                            <th class="th-sm">Quantity</th>
                            <th class="th-sm">Scheduled Date</th>
                            <th class="th-sm">Arrival Date</th>
                            <th class="th-sm">Arrival Time</th>
                            <th class="th-sm">Departure Date</th>
                            <th class="th-sm">Departure Time</th>
                            <th class="th-sm">Gin</th>
                            <th class="th-sm">Departure Region</th>
                            <th class="th-sm">Contract</th>
                            <th class="th-sm">Client</th>
                            <th class="th-sm">Status Container</th>
                            <th class="th-sm">Transport</th>
                            <th class="th-sm">CAAT</th>
                            <th class="th-sm">Waybill</th>
                            <th class="th-sm">Trk Plate</th>
                            <th class="th-sm">Trailer Plate</th>
                            <th class="th-sm">Driver Name</th>
                            <th class="th-sm">Purchase Weight</th>
                            <th class="th-sm">Departure Weight</th>
                            <th class="th-sm">Arrival Weight</th>
                            <th class="th-sm">Invoice No.</th>
                            <th class="th-sm">Comments</th>
                            <th class="th-sm">Status Truck</th>
                            <th class="th-sm">TrkID</th>
                            <?php if ($Typ_Exp == 1) { ?>
                                <th class="th-sm"></th>
                            <?php } ?>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!--Modal para CRUD-->
        <div class="modal hide fade in" data-bs-backdrop="static" class="modal" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <!--g" role="document">-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formExp">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Destination Port<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="Portdestino">
                                            <option value="">Choose</option>
                                            <option value="ALTAMIRA">ALTAMIRA</option>
                                            <option value="MANZANILLO">MANZANILLO</option>
                                            <option value="ENSENADA">ENSENADA</option>
                                        </select>
                                    </div>
                                </div>                               
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Shipping Company <font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="ShpCo"></select>
                                       
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Port of Loading<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="Port"></select>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Booking <font size=2>*</font></label>
                                        <input type="text" class="form-control form-control-sm" id="Bkg" required>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Container No.</label>
                                        <input type="text" class="form-control form-control-sm" id="Ctr">
                                    </div>
                                </div>
                               
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Delivery Order <font size=2>*</font></label>
                                        <input type="text" class="form-control form-control-sm" id="DO" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                            
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Seal</label>
                                        <input type="text" class="form-control form-control-sm" id="Seal">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Port Address</label>
                                        <input type="text" class="form-control form-control-sm" id="DirPort" readonly >
                                    </div>
                                </div>

                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Status Container</label>
                                        <select class="form-control form-control-sm" id="Status" name="Status">
                                            <option value="Enabled">Enabled </option>
                                            <option value="Disabled">Disabled</option>
                                            
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">TruckID</label>
                                        <input type="text" class="form-control form-control-sm" id="TrkID" readonly >                                     
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Lot</label>
                                        <select class="form-control form-control-sm" id="Lots"></select>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Quantity</label>
                                        <input type="text" class="form-control form-control-sm" id="Qty" required readonly>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Scheduled Date <font size=2>*</font></label>
                                        <input type="date" class="form-control form-control-sm" id="SchDate" required>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Arrival Date</label>
                                        <input type="date" class="form-control form-control-sm" id="InDate">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Arrival Time</label>
                                        <input type="time" class="form-control form-control-sm" id="InTime" value="12:00:00" max="22:30:00" min="8:30:00" step="1">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Departure Date</label>
                                        <input type="text" class="form-control form-control-sm" id="OutDate" required readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Departure Time</label>
                                        <input type="time" class="form-control form-control-sm" id="OutTime" required readonly>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Gin</label>
                                        <input type="text" class="form-control form-control-sm" id="Gin" required readonly>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Departure Region</label>
                                        <input type="text" class="form-control form-control-sm" id="OutReg" required readonly>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Contract</label>
                                        <input type="text" class="form-control form-control-sm" id="Ctc" required readonly>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Client</label>
                                        <input type="text" class="form-control form-control-sm" id="Cli" required readonly>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Transport</label>
                                        <input type="text" class="form-control form-control-sm" id="TNam" required readonly>
                                    </div>
                                </div>
                                <div class="col-lg-1">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">CAAT</label>
                                        <input type="text" class="form-control form-control-sm" id="CAAT" required readonly>
                                    </div>
                                </div>
                                <div class="col-lg-1">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Waybill</label>
                                        <input type="text" class="form-control form-control-sm" id="WBill" required readonly>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Trk Plate</label>
                                        <input type="text" class="form-control form-control-sm" id="TrkPlt" required readonly>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Trailer Plate</label>
                                        <input type="text" class="form-control form-control-sm" id="TraPlt" required readonly>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Driver Name</label>
                                        <input type="text" class="form-control form-control-sm" id="DrvNam" required readonly>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Status Truck</label>
                                        <input type="text" class="form-control form-control-sm" id="statustrk"  readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Purchase Weight</label>
                                        <input type="text" class="form-control form-control-sm" id="PWgh" required readonly>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Departure Weight</label>
                                        <input type="text" class="form-control form-control-sm" id="OutWgh" required readonly>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Arrival Weight</label>
                                        <input type="text" class="form-control form-control-sm" id="InWgh" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Invoice No.</label>
                                        <input type="text" class="form-control form-control-sm" id="Inv" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Comments</label>
                                        <input type="text" class="form-control form-control-sm" id="Cmt">
                                    </div>
                                </div>
                            </div>
                            <font size=2>*Required Fields</font>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" id="btnGuardar" class="btn btn-dark">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


          <!-- MODAL PARA GENERAR REPORTE DE LISTADOS -->
          <<div class="modal hide fade in" data-bs-backdrop="static"  data-bs-keyboard="false" class="modal" id="RepBook" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <!--g" role="document">-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"></h5>
                        <button type="button" class="btn-close" aria-label="Close" id="closelist"></button>
                    </div>
                    <form id="formbook">
                        <div class="modal-body">

                            <div class="row">  
                                <div class="col-lg-6">                         
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" for="regsalida_pesos">Booking</label>
                                        <input type="text" class="form-control form-control-sm" id="Booking">
                                        <button type="button" id="buscar" class="btn btn-sm btn-dark"><i class="bi bi-search"></i></button>
                                       
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="row">                                 
                           
                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >Total Bales</label>
                                        <input type="text" class="form-control form-control-sm" id="TotalBales" readonly>                                        
                                       
                                    </div> 
                                </div>    

                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >Real Gross Weigth</label>
                                        <input type="text" class="form-control form-control-sm" id="PesoBrutoReal" readonly>                                        
                                        
                                    </div> 
                                </div>    

                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >Date Report</label>
                                        <input type="date" class="form-control form-control-sm" id="fecha" onkeydown="return false"  lang="en-US" ></label>                                        
                                       
                                    </div> 
                                </div>    

                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >Average Weigth</label>
                                        <input type="text" class="form-control form-control-sm" id="promedioreal" readonly>                                        
                                       
                                    </div> 
                                </div>                           
                            </div>
                            <p> </p>
                            <div class="row"> 
                                <div class="col-md-3">
                                    <div class="input-group mb-2">
                                        <label class="input-group-text" for="regsalida_pesos">Franchise Per Bal</label>
                                        <input type="text" class="form-control form-control-sm" id="Francbal">                                        
                                        
                                    </div> 
                                </div>   
                                <div class="col-md-3">
                                    <div class="input-group mb-2">
                                        <label class="input-group-text" for="regsalida_pesos">Total Franchise</label>
                                        <input type="text" class="form-control form-control-sm" id="TotalFran" readonly>                                        
                                       
                                    </div> 
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group mb-2">
                                        <label class="input-group-text" for="regsalida_pesos">Gross Weight</label>
                                        <input type="text" class="form-control form-control-sm" id="GrossWght" readonly>                                        
                                       
                                    </div> 
                                </div>     
                                <div class="col-md-3">
                                    <div class="input-group mb-2">
                                        <label class="input-group-text" for="regsalida_pesos">Average Gross Weight</label>
                                        <input type="text" class="form-control form-control-sm" id="promediofranc" readonly>                                        
                                       
                                    </div> 
                                </div>              
                            </div>
                            <p> </p>
                            <div class="row"> 
                                <div class="col-md-3">
                                    <div class="input-group mb-2">
                                        <label class="input-group-text" for="regsalida_pesos">Tare per Bale</label>
                                        <input type="text" class="form-control form-control-sm" id="TareBale">                                        
                                       
                                    </div> 
                                </div> 

                                <div class="col-md-3">
                                    <div class="input-group mb-2">
                                        <label class="input-group-text" for="regsalida_pesos">Total Tare</label>
                                        <input type="text" class="form-control form-control-sm" id="TotalTare" readonly>                                        
                                        </select>
                                    </div> 
                                </div> 

                                <div class="col-md-3">
                                    <div class="input-group mb-2">
                                        <label class="input-group-text" for="regsalida_pesos"> Net Weight</label>
                                        <input type="text" class="form-control form-control-sm" id="NetWght" readonly>                                        
                                        </select>
                                    </div> 
                                </div> 
                                
                                <div class="col-md-3">
                                    <div class="input-group mb-2">
                                        <label class="input-group-text" for="regsalida_pesos">Average Net Weight</label>
                                        <input type="text" class="form-control form-control-sm" id="promediotara" readonly>                                        
                                        </select>
                                    </div> 
                                </div>            

                            </div>
                            <p> </p>
                            <div class="row"> 
                                
                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >Label</label>
                                        <select class="form-control form-control-sm" id="etiqueta" name="etiqueta">
                                            <option value="Original">Original </option>
                                            <option value="Copia">Copia</option>                                            
                                        </select>     
                                    </div> 
                                </div>  
                                <div class="col-md-3">
                                    <button id="infotrucks" class="btn btn-primary">Trucks Info</button>
                                </div>  
                            </div>

                            <!-- ALERTA -->
                            <p> </p>
                            <div id="alerta_lista" class="row" style="display:none"> 

                                <div class="alert alert-warning d-flex align-items-center" role="alert">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
                                    <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                                </svg>
                                <div id="texto_alerta">
                                    An example alert with an icon
                                </div>
                                </div>                            

                            </div>
                            


                        </div>
                    </form>
                    <div class="modal-footer">
                       <button  id="btnCloselist" class="btn btn-light">Close</button>
                        <!--<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>-->
                        <button id="limpiar"   class="btn btn-dark" >Clear</button>
                        <button id="GenerarRep"   class="btn btn-primary" disabled>Generate Report</button>

                    </div>
                </div>
            </div>
        </div>


                <!--Modal informacion de lots en el truck-->
        <div class="modal fade" data-bs-backdrop="static" id="modalInTrucks" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Info Lots</h5>
                        <button type="button" class="btn-close" aria-label="Close" id="closeInfoTrk"></button>
                    </div>
                    <form id="InfTrk">
                        <div class="modal-body">
                            <table id="tablainfo" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>TrkID</th>
                                    <th>Lot</th>
                                    <th>Quantity</th>
                                    <th>Out Weight</th>
                                    <th>AVG Weight</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <!--<button type="submit" id="btnClose" class="btn btn-light">Close</button>-->
                        <button type="button" id="closeinfo"class="btn btn-dark" data-dismiss="modal">Close</button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
            </div>
        </div>


    </body>

    </html>
<?php
endif;
?>
