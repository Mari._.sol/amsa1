$(document).ready(function() {

$('#tablaRoutes tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );


var opcion;
opcion = 4;
    
tablaRte = $('#tablaRoutes').DataTable({
    
    //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Export to Excel',
				className: 'btn btn-success',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			{
				extend:    'pdfHtml5',
				text:      '<i class="fas fa-file-pdf"></i> ',
				titleAttr: 'Export to PDF',
                className: 'btn btn-danger',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			/*{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},*/
		],
    
    initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
    },
    "order": [ 2, 'asc' ],
    "order": [ 1, 'asc' ],
    "scrollX": false,
    "scrollY": "50vh",
    "scrollCollapse": true,
    "lengthMenu": [100,200,300,500], 
    "ajax":{            
        "url": "bd/crudRoutes.php", 
        "method": 'POST', //usamos el metodo POST
        "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
        "dataSrc":""
    },
    "columns":[
        {"data": "RteID"},
        {"data": "RegName"},
        {"data": "TptCo"},
        {"data": "BnName"}
        //{"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button><button class='btn btn-danger btn-sm btnBorrar'><i class='material-icons'>delete</i></button></div></div>"}
    ]
});     

$('.dataTables_length').addClass('bs-select');

var fila; //captura la fila, para editar o eliminar
//submit para el Alta y Actualización
$('#formTransports').submit(function(e){
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
    TptCo = $.trim($('#TptCo').val().toUpperCase());    
    BnName = $.trim($('#BnName').val().toUpperCase());      
    
    var verificaTransport = $.ajax({ //verificamos que exista el transport o no en la bd
        type: "POST",
        url: "./bd/correo-model.php?m=getTransport", //hago referencia al archivo correo model.php y le mando el método getTransport
        data: {
            BnName: BnName // aquí igual se pordría pasar el parámetro m 
            //m   : "getTransport"  /// De esta forma
        }
    });

    $.when(verificaTransport).done(function(d){
        data = JSON.parse(d);
       // console.log(data);
       $('#aviso').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');      

        if(!data){
            $.ajax({
                    url: "bd/crudRoutes.php",
                    type: "POST",
                    datatype:"json",    
                    data:  { TptID:TptID, TptCo:TptCo, BnName:BnName, opcion:opcion},    
                    success: function(data) {
                    tablaRte.ajax.reload(function(){
                        $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 20rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Transport created successfull</div>')
                        setTimeout(function() {
                            $('#alert1').fadeOut(1000);
                            $('#modalTransports').modal('hide');
                        },1000);
                    });
                    
                    }
                });		        
            
        }else{
            $('#aviso').html('<div id="alert1" class="alert alert-danger" style="width: 20rem; height: 3rem;" role="alert"><i class="bi bi-x-circle-fill"></i>    Transport already exist!</div>')
            setTimeout(function() {
                $('#alert1').fadeOut(1000);

            },1000);
        }
    })
       											     			
});

//-----Rutas----
$('#formRoutes').submit(function(e){
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
    TptCo = $.trim($('#TptCoR').val());    
    OutReg = $.trim($('#OutReg').val());                            
        $.ajax({
          url: "bd/crudRoutes.php",
          type: "POST",
          datatype:"json",    
          data:  { RteID:RteID, TptCo:TptCo, OutReg:OutReg, opcion:opcion},    
          success: function(data) {
            tablaRte.ajax.reload(null, false);
           }
        });			        
    $('#modalRoutes').modal('hide');											     			
});

//para limpiar los campos antes de dar de Alta una Persona
$("#btnCar").click(function(){
    opcion = 1; //alta           
    TptID=null;
    $("#formTransports").trigger("reset");
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    $(".modal-title").text("New Transport");
    $('#modalTransports').modal('show');	    
});
    
$("#btnRuta").click(function(){
    LoadTransports();
    LoadRegion();
    //opcion = 2; //alta           
    RteID=null;
    $("#formRoutes").trigger("reset");
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    $(".modal-title").text("New Route");
    $('#modalRoutes').modal('show');	    
});
     
});


// Cargar Lotes Disponibles
function LoadTransports(){
    $("#TptCoR").empty();
    $("#OutReg").empty();
    $("#Salidas").empty();
    
    //OutReg = $.trim($('#OutReg').val());
    //DOrd = $.trim($('#DO').val());
    //var opts = "";
    opcion = 3;
    $.ajax({
        url: "bd/crudRoutes.php",
        type: "POST",
        datatype:"json",    
        data:  {opcion:opcion},    
        success: function(data) {
            opts = JSON.parse(data);
            $('#TptCoR').append('<option class="form-control" selected disabled>- Select -</option>');
            for (var i = 0; i< opts.length; i++){
                $('#TptCoR').append('<option value="' + opts[i].TptID + '">' + opts[i].TptCo + '</option>');
            }
        }
    });	
}

// Cargar Lotes Disponibles
function LoadRegion(){
    $("#TptCoR").empty();
    $("#OutReg").empty();
    $("#Salidas").empty();
    
    //OutReg = $.trim($('#OutReg').val());
    //DOrd = $.trim($('#DO').val());
    //var opts = "";
    opcion = 6;
    $.ajax({
        url: "bd/crudRoutes.php",
        type: "POST",
        datatype:"json",    
        data:  {opcion:opcion},    
        success: function(data) {
            opts = JSON.parse(data);
            $('#OutReg').append('<option class="form-control" selected disabled>- Select -</option>');
            for (var i = 0; i< opts.length; i++){
                $('#OutReg').append('<option value="' + opts[i].IDReg + '">' + opts[i].RegNam + '</option>');
            }
        }
    });	
}

//Cargar rutas registradas
$( function() {
    $("#TptCoR").change( function() {
        $("#Salidas").empty();
        TptCo = $.trim($('#TptCoR').val());
        opcion = 5;
        $.ajax({
            url: "bd/crudRoutes.php",
            type: "POST",
            datatype:"json",    
            data:  { TptCo:TptCo, opcion:opcion},    
            success: function(data) {
                opts = JSON.parse(data);
                for (var i = 0; i< opts.length; i++){
                    $('#Salidas').append('<option value="' + opts[i].RteID + '">' + opts[i].RegNam + '</option>');
                }
            }
        });
        
    });
});

//Funcion agregar ruta
$( function() {
    
        $("#btnAddRte").click(function(){
            
            TptCo = $.trim($('#TptCoR').val());
            OutReg = $.trim($('#OutReg').val());
            //alert(OutReg);
            opcion = 7;
            
            $.ajax({
                    url: "bd/crudRoutes.php",
                    type: "POST",
                    datatype:"json",    
                    data:  {TptCo:TptCo, OutReg:OutReg, opcion:opcion},    
                    success: function(data){
                        tablaRte.ajax.reload(null, false);
                    }
            });
        });
});

