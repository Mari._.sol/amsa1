$(document).ready(function() {
    
$('#tablaUsuarios tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
    
var LotID, opcion;
opcion = 4;
    
tablaUsuarios = $('#tablaUsuarios').DataTable({
    
    initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
    },	        
    //orderCellsTop: true,
    //fixedHeader: true,
    //scrollY: 340,
    //paging: false,
    "scrollY": "340px",
    "scrollCollapse": true,
    "lengthMenu": [100,200,300,500],
    "ajax":{            
        "url": "bd/crud.php", 
        "method": 'POST', //usamos el metodo POST
        "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
        "dataSrc":""
    },
    "columns":[
        {"data": "LotID"},
        {"data": "Lot"},
        {"data": "GinID"},
        {"data": "Qty"},
        {"data": "LiqWgh"},
        {"data": "DOrd"},
	{"data" : "TrkID"},
	{"data" : "InvID"},
        {"data": "Status"},
        {"data": "Cert"}
        //{"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button><button class='btn btn-danger btn-sm btnBorrar'><i class='material-icons'>delete</i></button></div></div>"}
    ],
    columnDefs : [
        { targets : [19],
            render : function (data, type, row) {
              return data == '1' ? 'YES' : 'NO'
            }
        }
    ]
});     

//crear buscadores
/*$('#tablaUsuarios thead tr').clone(true).appendTo('#tablaUsuarios thead');
$('#tablaUsuarios thead tr:eq(1) th').each(function(i) {
    var title = $(this).text();
    $(this).html('<input type="text" placeholder="Search" />');
    
    $('input', this).on('keyup change', function(){
        if (tablaUsuarios.column(i).search() !== this.value){
            tablaUsuarios
                .column(i)
                .search(this.value)
                .draw();
        }
    });
});*/
    
    
$('.dataTables_length').addClass('bs-select');



var fila; //captura la fila, para editar o eliminar
//submit para el Alta y Actualización
$('#formUsuarios').submit(function(e){                         
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
    Lot = $.trim($('#Lot').val());    
    GinID = $.trim($('#GinID').val());
    Qty = $.trim($('#Qty').val());    
    LiqWgh = $.trim($('#LiqWgh').val());    
    DOrd = $.trim($('#DOrd').val());
    TrkID = $.trim($('#TrkID').val());
    InvID = $.trim($('#InvID').val());
    Status = $.trim($('#Status').val());                            
        $.ajax({
          url: "bd/crud.php",
          type: "POST",
          datatype:"json",    
          data:  {LotID:LotID, Lot:Lot, GinID:GinID, Qty:Qty, LiqWgh:LiqWgh, DOrd:DOrd, TrkID:TrkID, InvID:InvID ,Status:Status ,opcion:opcion},    
          success: function(data) {
            tablaUsuarios.ajax.reload(null, false);
           }
        });			        
    $('#modalCRUD').modal('hide');											     			
});
        
 

//para limpiar los campos antes de dar de Alta una Persona
$("#btnNuevo").click(function(){
    opcion = 1; //alta           
    LotID=null;
    $("#formUsuarios").trigger("reset");
    $(".modal-header").css( "background-color", "#17a2b8");
    $(".modal-header").css( "color", "white" );
    $(".modal-title").text("New Lot");
    $('#modalCRUD').modal('show');	    
});

//Editar        
$(document).on("click", ".btnEditar", function(){		        
    opcion = 2;//editar
    fila = $(this).closest("tr");	        
    LotID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID		            
    Lot = fila.find('td:eq(1)').text();
    GinID = parseFloat(fila.find('td:eq(2)').text());
    Qty = parseFloat(fila.find('td:eq(3)').text());
    LiqWgh = parseFloat(fila.find('td:eq(4)').text());
    DOrd = parseFloat(fila.find('td:eq(5)').text());
    TrkID = parseInt(fila.find('td:eq(6)').text());
    InvID = fila.find('td:eq(7)').text();
    Status = fila.find('td:eq(8)').text();
    $("#LotID").val(LotID);
    $("#Lot").val(Lot);
    $("#GinID").val(GinID);
    $("#Qty").val(Qty);
    $("#LiqWgh").val(LiqWgh);
    $("#DOrd").val(DOrd);
    $("#TrkID").val(TrkID);
    $("#InvID").val(InvID);
    $("#Status").val(Status);
    $(".modal-header").css("background-color", "#007bff");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Edit Lot");		
    $('#modalCRUD').modal('show');		   
});

//Borrar
$(document).on("click", ".btnBorrar", function(){
    fila = $(this);           
    LotID = parseInt($(this).closest('tr').find('td:eq(0)').text()) ;		
    opcion = 3; //eliminar        
    var respuesta = confirm("¿Está seguro que deseas deshabilitar el registro "+LotID+"?");                
    if (respuesta) {            
        $.ajax({
          url: "bd/crud.php",
          type: "POST",
          datatype:"json",    
          data:  {opcion:opcion, LotID:LotID},    
          success: function() {
              tablaUsuarios.row(fila.parents('tr')).remove().draw();                  
           }
        });	
    }
 });
});   
