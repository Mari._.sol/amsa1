
$(document).ready(function(){
    var truck_id =0;
    //----------------DESPLEGAR MODAL DE INCIDENTES------------
    $(document).on("click", ".btnincidente", function (e) {
        var fila = $(this);
        truck_id = $(this).closest('tr').find('td:eq(0)').text();
        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );       
        $('#modalincidentes').modal('show'); 	

        
        $("#Typeincident").empty();
        $('#incidentes').empty();
        $('#Typeincident').append('<option class="form-control" selected disabled>- Select -</option>');
        ///DESPLEGAR INCIDENTES CAPTURADOS DEL TRUCK SELECCIONADO
        record_incidents(truck_id);
        ///DESPLEGAR RESPONSABLES EN SELECTOR
        responsables();           
     
        
    });

     ///DESPLEGAR TIPOS DE INCIDENTES EN SELECTOR, DEPENDIENDO DEL RESPONSABLE SELECCIONADO 
         $("#responsable").change(function () {   
         var responsable = $(this).val();
         incidencias(responsable);         

          });         
        ///AGREGAR INCIDENTE A TRUCK SELECCIONADO
        $("#addincidente").click(function(e){
            e.preventDefault();
            agregaincidente(truck_id);

        });	
        ///QUITAR INCIDENTE A TRUCK SELECCIONADO
        $("#remove").click(function(e){
            e.preventDefault();
            borrar_incidente(truck_id);        

        });	

        $('#formincidentes').submit(function(e){
            e.preventDefault();
            $('#modalincidentes').modal('hide');
            $('#formincidentes').trigger("reset");

        });


})


//FUNCION PARA  TRAER LOS TIPOS DE INCIDENCIA DE LA BD
function responsables(){
        
    opcion = 1;  //consultar diccionarios agregados en la BD 
    $.ajax({
        url: "bd/crudincidentes.php",
        type: "POST",
        datatype:"json",
        data:  {opcion:opcion},    
        success: function(data){
            $("#responsable").empty();
            opts = JSON.parse(data);
            $('#responsable').append('<option class="form-control" selected disabled>- Select -</option>');
            for (var i = 0; i< opts.length; i++){
                $('#responsable').append('<option value="' + opts[i].Responsible + '">' + opts[i].Responsible + '</option>');
            }
        },
        error: function(data) {
            alert('error');
        }
    });

}



//FUNCION PARA MOSTRAR LAS INCIDENCIAS ASIGNADAS A UN TRUCK
function record_incidents(trkid){     
    var opts = "";
    opcion = 3;
    $('#incidentes').empty();
    $.ajax({
        url: "bd/crudincidentes.php",
        type: "POST",
        datatype:"json",    
        data:  {trkid:trkid,opcion:opcion},    
        success: function(data) {
            opts = JSON.parse(data);
            $('#incidentes').empty();
            for (var i = 0; i< opts.length; i++){
                record = opts[i].Responsible+"-"+opts[i].Typeincident
                $('#incidentes').append('<option value="' + record + '">' + record + '</option>');
            }
           
        }
    });	
}

//MOSTRAR LAS INCIDENCIAS DEL DICCIONARIO DE LA BD
function incidencias(responsable){
    $.ajax({
        url: "bd/crudincidentes.php",
        cache: false,
        type: "POST",
        datatype:"json",
        data:  {opcion:2,responsable:responsable},    
        success: function(data){                  
            $("#Typeincident").empty();
            opts = JSON.parse(data);
            $('#Typeincident').append('<option class="form-control" selected disabled>- Select -</option>');
            for (var i = 0; i< opts.length; i++){
                $('#Typeincident').append('<option value="' + opts[i].Typeincident + '">' + opts[i].Typeincident + '</option>');
            }
 
        },
        error: function(data) {           
            alert('error');
        }
    });
}

//AGREGAR INCIDENTES A UN TRUCKID
function agregaincidente(idtruck){
    responsable=$('#responsable').val();
    tipoincidente=$('#Typeincident').val();
    opcion=4;

    if(tipoincidente != null && responsable != null  ){
            $.ajax({
                url: "bd/crudincidentes.php",
                type: "POST",
                datatype:"json",    
                data:  {trkid:idtruck,opcion:opcion,responsable:responsable,tipoincidente:tipoincidente}, 
                success: function(data) {                
                    $('#incidentes').empty();
                    record_incidents(idtruck)
                }                                 
            });	

    }

    else{
            alert("Complete los campos");
    }
}


//QUITAR INCIDENTE DE UN TRUCKID
function borrar_incidente(idtruck){

    var selectText = $("#incidentes option:selected").map(function (){
        return $(this).text();
    }).get().join(", ");

    incidentes = $.trim($('#incidentes').val());
    opcion=5;
    if (incidentes !="" && incidentes !=null){ 
        let text = "¿Desea borrar los registros seleccionados?";
        if (confirm(text) == true) {
            $.ajax({
                url: "bd/crudincidentes.php",
                type: "POST",
                datatype:"json",    
                data:  {trkid:idtruck,opcion:opcion,incidentes:incidentes}, 
                success: function(data) {                
                    $('#incidentes').empty();
                    record_incidents(idtruck)            
                }  
                            
        });	
    }
    }
    else{
        alert("Seleccione una incidencia")
    }    

}


