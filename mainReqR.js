$(document).ready(function() {

    $('#tablaReq tfoot th').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        } );
    
    
    var opcion;
    opcion = 4;
        
    tablaReq = $('#tablaReq').DataTable({
        
        //para usar los botones   
            responsive: "true",
            dom: 'Brtilp',       
            buttons:[ 
                {
                    extend:    'excelHtml5',
                    text:      '<i class="fas fa-file-excel"></i> ',
                    titleAttr: 'Export to Excel',
                    className: 'btn btn-success',
                    exportOptions: {
                        columns: ":not(.no-exportar)"
                    }
                },
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fas fa-file-pdf"></i> ',
                    titleAttr: 'Export to PDF',
                    className: 'btn btn-danger',
                    exportOptions: {
                        columns: ":not(.no-exportar)"
                    }
                },
                /*{
                    extend:    'print',
                    text:      '<i class="fa fa-print"></i> ',
                    titleAttr: 'Imprimir',
                    className: 'btn btn-info'
                },*/
            ],
        
        initComplete: function () {
                // Apply the search
                this.api().columns().every( function () {
                    var that = this;
     
                    $( 'input', this.footer() ).on( 'keyup change clear', function () {
                        if ( that.search() !== this.value ) {
                            that
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );
        },
        //"order": [ 0, 'desc' ],
        "scrollX": true,
        "scrollY": '50vh', //"340px",
        "autoWidth": true,
        "scrollCollapse": true,
        "lengthMenu": [100,200,300,500],
        //"aoColumnDefs": [{ "bVisible": false, "aTargets": [ 19 ]}],
        "ajax":{            
            "url": "bd/crudReq.php", 
            "method": 'POST', //usamos el metodo POST
            "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
            "dataSrc":""
        },
        "columns":[
            {"data": "ReqID"},
            {"data": "Services"},
            {"data": "Typ"},
            {"data": "OutReg"},
            {"data": "InReg"},
            {"data": "Cont"},
            {"data": "Total"},
/*
            {"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button>"}
*/
        ]
    });     
    
    $('.dataTables_length').addClass('bs-select');
    
         
    });
