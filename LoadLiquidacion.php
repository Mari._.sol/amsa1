<?php

include_once 'bd/conexion.php';
require __DIR__.'/vendor/autoload.php';
date_default_timezone_set("America/Mexico_City");

use Aws\S3\S3Client; 
use Aws\Exception\AwsException; 

$objeto = new Conexion();
$conexion = $objeto->Conectar();

$LiqID = $_POST['LiqID'];
$TipoLiq = $_POST['TipoLiq'];
$sup = substr($LiqID, 3, 3); 
$crop = substr($LiqID, 6, 2);
$year = "20" . $crop;

$fechaload= date('Y-m-d h:i:s a', time());  
$bucket = 'pruebasportal'; //bucket de pruebas
//$bucket = 'portal-liq';
$ticket = $_FILES['file']['name'];
$ext=pathinfo($ticket, PATHINFO_EXTENSION);
$data="Liquidacion cargada";
//$newfilename = $LiqID.'.'.$ext;



//Consultar si ya hay un certifiicad cargado 
$consulta = "SELECT fileLiq From amsadb1.Liquidation  WHERE LiqID='$LiqID' and TipoLiq='$TipoLiq';";  
$resultado = $conexion->prepare($consulta);
$resultado->execute();
$datos=$resultado->fetch();
$extetik = $datos['fileLiq'];


$s3 = new S3Client([
    'version'     => 'latest',
    'region'      => 'us-east-2', //bucket de pruebas
    //'region'      => 'us-east-1',
    'credentials' => [
        'key'    => 'AKIAT442VUCJQXPEFVZW',
        'secret' => '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',
    ],
]);


// 1. Borrar el archivo si ya exise 
if($extetik != "0" ){
    try {
        $result = $s3->deleteObject([
            'Bucket' => $bucket,
            'Key'    => "Liq".$TipoLiq."_".$LiqID.".".$extetik
        ]);

        if ($result['DeleteMarker'])
        {
            $data= "Liquidacion actualizada";
        } else {
            $data="Error al actualizar Liquidacion";
        }
    }
    catch (S3Exception $e) {
        exit('Error: ' . $e->getAwsErrorMessage() . PHP_EOL);
    }
}

try {
    // Upload data.
    $result = $s3->putObject([
        'Bucket' => $bucket,
        'Key'    => "Liq".$TipoLiq."_".$LiqID.".".$ext,
        'SourceFile' => $_FILES['file']['tmp_name']
    ]);

    $consulta = "UPDATE amsadb1.Liquidation  SET fileLiq ='$ext' WHERE LiqID='$LiqID' and TipoLiq='$TipoLiq'";		
    $resultado = $conexion->prepare($consulta);
    $resultado->execute(); 
    $consulta = "UPDATE amsadb1.Liquidation  SET supplier ='$sup', Crop='$year' WHERE LiqID='$LiqID' and TipoLiq='$TipoLiq'";		
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();    

} catch (S3Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}

$data = "Liq".$TipoLiq."_".$LiqID.".".$ext;

print json_encode($data, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX

?>