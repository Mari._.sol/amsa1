<?php 
require __DIR__.'/bd/vendor/autoload.php';
include_once 'bd/conexion.php';
include_once 'funcionAWS.php';


function lowercase($element)
{
    return "'".$element."'";
}

$cliid = (isset($_POST['planta'])) ? $_POST['planta'] : '';
$Lots = (isset($_POST['lotes'])) ? $_POST['lotes'] : '';


$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set("America/Mexico_City");


$fechaload= date('Y-m-d h:i:s a', time());  

$consulta = "SELECT UserClient FROM amsadb1.Users_Clients WHERE FIND_IN_SET('$cliid', CliID) > 0 Limit 1;";
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$usercliente=$resultado->fetch();
$cliente=$usercliente['UserClient'];

$bucket = 'files-hvi'; //bucket de pruebas
// Preparar la consulta preparada
$consulta_preparada = $conexion->prepare("INSERT INTO FilesHVI (FileName, TypeFile, DateLoadFile) VALUES (:nombrearchivo, :tipo, :fecha)");

//definir parametros del bucket 








$carpeta_destino = 'bd/testfiles/';
$array = explode(",",$Lots);


// Para usar la phpSpreadsheet llamamos a autoload

use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill};
require 'bd/vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require 'bd/vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require 'bd/vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require 'bd/vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require 'bd/vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


$today = new DateTime();//DateTime("2022-04-06"); //echo $today->format('Y-m-d');

$contador = 0;
foreach ($array as $dato){
$fila = 2;
$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Hoja 1");
//$hojaActiva->freezePane("A2");




$hojaActiva->getColumnDimension('A')->setWidth(8);
$hojaActiva->setCellValue('A1','Lot');
$hojaActiva->getColumnDimension('B')->setWidth(9);
$hojaActiva->setCellValue('B1','Bales');
$hojaActiva->getColumnDimension('C')->setWidth(7);
$hojaActiva->setCellValue('C1','Grd');
$hojaActiva->getColumnDimension('D')->setWidth(7);
$hojaActiva->setCellValue('D1','Trs');
$hojaActiva->getColumnDimension('E')->setWidth(7);
$hojaActiva->setCellValue('E1','Mic');
$hojaActiva->getColumnDimension('F')->setWidth(7);
$hojaActiva->setCellValue('F1','Len');
$hojaActiva->getColumnDimension('G')->setWidth(7);
$hojaActiva->setCellValue('G1','Str');
$hojaActiva->getColumnDimension('H')->setWidth(7);
$hojaActiva->setCellValue('H1','Unf');
$hojaActiva->getColumnDimension('I')->setWidth(7);
$hojaActiva->setCellValue('I1','Rd');
$hojaActiva->getColumnDimension('J')->setWidth(7);
$hojaActiva->setCellValue('J1','b');
$hojaActiva->getColumnDimension('K')->setWidth(7);
$hojaActiva->setCellValue('K1','Col');
$hojaActiva->getColumnDimension('L')->setWidth(7);
$hojaActiva->setCellValue('L1','Tar');
$hojaActiva->getColumnDimension('M')->setWidth(7);
$hojaActiva->setCellValue('M1','Tcnt');
$hojaActiva->getColumnDimension('N')->setWidth(7);
$hojaActiva->setCellValue('N1','Sfi');
$hojaActiva->getColumnDimension('O')->setWidth(7);
$hojaActiva->setCellValue('O1','Elg');
$hojaActiva->getColumnDimension('P')->setWidth(7);
$hojaActiva->setCellValue('P1','Mat');
$hojaActiva->getColumnDimension('Q')->setWidth(7);
$hojaActiva->setCellValue('Q1','Mst');



        
                $query= "SELECT Bal, Col1_Mod as Col1, Tgrd_Mod as Tgrd, Mic_Mod as Mic, Len_Mod as Len, Str_Mod as Str, Unf_Mod as Unf, Rd_Mod as Rd, b_Mod as b, Tar_Mod as Tar, Col2_Mod as Col2, Tcnt_Mod as Tcnt, Sfi_Mod as Sfi, Elg_Mod as Elg, Mat_Mod as Mat, Mst_Mod as Mst, Lot FROM Bales WHERE Lot = '".$dato."'  ORDER BY Bal ASC;";

                $result = $conexion->prepare($query);
                $result->execute();
                $siexiste=0; //Para verificar que hayan datos

                //promedios Lotes
                $consulta = "SELECT count(Bal) as SumBal, avg(Mic_Mod) as avgMic, avg(Tgrd_Mod) as avgTgrd, avg(Tar_Mod) as avgTar, avg(Tcnt_Mod) as avgTcnt, avg(Unf_Mod) as avgUnf, avg(Len_Mod) as avgLen, avg(Sfi_Mod) as avgSfi, avg(Str_Mod) as avgStr, avg(Elg_Mod) as avgElg, avg(Mat_Mod) as avgMat, avg(Rd_Mod) as avgRd, avg(b_Mod) as avgb FROM Bales WHERE Lot = '".$dato."';"; 
                $resultado = $conexion->prepare($consulta);
                $resultado->execute();
            


        while($row = $result->fetch(PDO::FETCH_ASSOC)){
        
            $style = $hojaActiva->getStyle('A'. $fila);
            $style->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
            $hojaActiva->getCell('A'. $fila)->setValueExplicit($row['Lot'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);


            //$hojaActiva->setCellValue('A' . $fila,$row['Lot']);
            $hojaActiva->setCellValue('B' . $fila,$row['Bal']);
            $hojaActiva->setCellValue('C' . $fila,$row['Col1']);
            $hojaActiva->setCellValue('D' . $fila,$row['Tgrd']);
            $hojaActiva->setCellValue('E' . $fila,number_format(Round($row['Mic'],1),1));
            $hojaActiva->setCellValue('F' . $fila,number_format(Round($row['Len'],2),2));
            $hojaActiva->setCellValue('G'. $fila,number_format(Round($row['Str'],1),1));
            $hojaActiva->setCellValue('H'. $fila,number_format(Round($row['Unf'],1),1));
            $hojaActiva->setCellValue('I'. $fila,number_format(Round($row['Rd'],1),1));
            $hojaActiva->setCellValue('J'. $fila,number_format(Round($row['b'],1),1));
            $hojaActiva->setCellValue('K'. $fila,$row['Col1']."-".$row['Col2']);
            $hojaActiva->setCellValue('L'. $fila,number_format(Round($row['Tar'],2),2));
            $hojaActiva->setCellValue('M'. $fila,$row['Tcnt']);
            $hojaActiva->setCellValue('N'. $fila,number_format(Round($row['Sfi'],1),1));
            $hojaActiva->setCellValue('O'. $fila,number_format(Round($row['Elg'],1),1));
            $hojaActiva->setCellValue('P'. $fila,number_format(Round($row['Mat'],2),2));
            $hojaActiva->setCellValue('Q' . $fila,number_format(Round($row['Mst'],1),1));

            $fila++;
        }


        while($row = $resultado->fetch(PDO::FETCH_ASSOC)){
            $hojaActiva->setCellValue('B' . $fila,$row['SumBal']);
            $hojaActiva->setCellValue('D' . $fila,Round($row['avgTgrd'],0));
            $hojaActiva->setCellValue('E' . $fila,number_format(Round($row['avgMic'],2),2));
            $hojaActiva->setCellValue('F' . $fila,number_format(Round($row['avgLen'],2),2));
            $hojaActiva->setCellValue('G'. $fila,number_format(Round($row['avgStr'],2),2));
            $hojaActiva->setCellValue('H'. $fila,number_format(Round($row['avgUnf'],2),2));
            $hojaActiva->setCellValue('I'. $fila,number_format(Round($row['avgRd'],2),2));
            $hojaActiva->setCellValue('J'. $fila,number_format(Round($row['avgb'],2),2));
            $hojaActiva->setCellValue('L'. $fila,number_format(Round($row['avgTar'],2),2));
            $hojaActiva->setCellValue('M'. $fila,number_format(Round($row['avgTcnt'],2),2));
            $hojaActiva->setCellValue('N'. $fila,number_format(Round($row['avgSfi'],2),2));
            $hojaActiva->setCellValue('O'. $fila,number_format(Round($row['avgElg'],2),2));
            $hojaActiva->setCellValue('P'. $fila,number_format(Round($row['avgMat'],2),2));
            
            $fila++;
        }


        $hojaActiva->getStyle('G2:J'.$fila)->getNumberFormat()->setFormatCode('#,##0.0');
        $hojaActiva->getStyle('N2:O'.$fila)->getNumberFormat()->setFormatCode('#,##0.0');
        $hojaActiva->getStyle('Q2:Q'.$fila)->getNumberFormat()->setFormatCode('#,##0.0');

        $hojaActiva->getStyle('F2:F'.$fila)->getNumberFormat()->setFormatCode('#,##0.00');
        $hojaActiva->getStyle('P2:P'.$fila)->getNumberFormat()->setFormatCode('#,##0.00');


        // Especificar el nombre del archivo de excel
        $nombre_archivo = $cliente.'-'.$dato.'.xlsx';
        // Vincular parámetros de consulta preparada

        $consulta_preparada->bindValue(':nombrearchivo', $nombre_archivo);
        $consulta_preparada->bindValue(':tipo', 'Excell');
        $consulta_preparada->bindValue(':fecha', $fechaload);
        $consulta_preparada->execute();

        $writer = new Xlsx($excel);
        $writer->save($carpeta_destino . '/' . $nombre_archivo);
        $ruta_archivo = $carpeta_destino . '/' . $nombre_archivo;
        //llamar a la funcion que carga el archivo al bucket de AWS S3
        subearchivo($ruta_archivo,$nombre_archivo);


        $contador ++;

}

//print_r($contador);

print json_encode($contador, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX
$conexion=null;

?>