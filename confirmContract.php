<?php
$contractId = $_GET['noSolicitud'];

session_start();
if (!isset($_SESSION['username'])) :
  include_once('index.php');
else :
  $Typ_Cli = $_SESSION['Typ_Cli'];
  $permiso = $_SESSION['Admin'];
  
  $aprueba = $_SESSION['username'];  

include_once 'bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

if (!empty($contractId)) {
  $consulta = "SELECT contStatus FROM amsadb1.contract WHERE IdContract = :contractId";
  $resultado = $conexion->prepare($consulta);
  $resultado->bindParam(':contractId', $contractId, PDO::PARAM_INT);
  $resultado->execute();
  
  if ($resultado->rowCount() > 0) {
    $data = $resultado->fetch(PDO::FETCH_ASSOC);
    $contStatus = $data['contStatus'];
  }
}
$conexion = null;

?>
<!doctype html>
<html lang="es en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="img/ecom.png" />
    <title>Confirmar contrato</title>

    <!-- CSS bootstrap -->
    <link rel="stylesheet" href=".//assets/bootstrap/css/bootstrap.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- CSS personalizado -->
    <link rel="stylesheet" href="./main.css">
  </head>
  <body>
    <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
    <div>
      <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
        <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">                   
          <a class="navbar-brand nav-item" href="main.php">
            <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
          </a>
        </div>
      </nav>
    </div>
    <br></br>
    <div style="text-align: center;">
      <p id="aprueba" class=""><?php echo $_SESSION['username']; ?></p>
      <?php
        if (empty($contStatus)) {
          // Muestra los botones solo si $contStatus está vacío
          echo '<button id="confirmarC" class="btn btn-success">Confirmar Contrato</button>';
          echo '<br></br>';
          echo '<button id="rechazar" class="btn btn-secondary">Rechazar contrato</button>';
        }else{
          if ($contStatus == 'APROBADO') { // validar el status del contrato
            echo "El contrato con No. de solicitud $contractId <br> ya fue $contStatus";
          } else{
            echo "El contrato con No. de solicitud $contractId <br> ya fue $contStatus";
          }
        }
      ?>
    </div>
  </body>

</html>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
  var aprueba = '<?php echo $aprueba; ?>';

  $(document).ready(function() {
    $("#confirmarC").click(function() {
      $.ajax({
        url: "actualizarContrato.php",
        type: "POST",
        data: { opcion: 0, contractId: <?php echo $contractId; ?>, aprueba: aprueba},
        success: function(response) {
          alert("Contrato confirmado exitosamente.");
          $('#confirmarC').prop('disabled', true);
          $('#rechazar').prop('disabled', true);
          /* setTimeout(function() {
            window.close();
          }, 5000); */
        },
        error: function() {
          alert("Error al confirmar el contrato.");
        }
      });
    });

    $("#rechazar").click(function() {
      $.ajax({
        url: "actualizarContrato.php",
        type: "POST",
        data: { opcion:1, contractId: <?php echo $contractId; ?>, aprueba: aprueba},
        success: function(response) {
          alert("Contrato rechazado exitosamente.");
          $('#confirmarC').prop('disabled', true);
          $('#rechazar').prop('disabled', true);
        },
        error: function() {
          alert("Error al rechazar el contrato.");
        }
      });
    });
  });
</script>
<?php
endif;
?>