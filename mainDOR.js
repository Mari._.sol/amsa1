$(document).ready(function() {

$('#tablaOrdenes tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );


var opcion;
opcion = 4;
    
tablaDO = $('#tablaOrdenes').DataTable({
    
    //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Export to Excel',
				className: 'btn btn-success',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			{
				extend:    'pdfHtml5',
				text:      '<i class="fas fa-file-pdf"></i> ',
				titleAttr: 'Export to PDF',
                className: 'btn btn-danger',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
             {
                text: '<i class="bi bi-funnel-fill"></i>',
                titleAttr: 'Filtrar por',
                orientation: 'landscape',
                className: 'btn btn-info btn-filtrar',
                attr:{
                        id:'filtraboton',
                        "data-toggle": 'modal tooltip',
                        "disabled": false
                }
            },
			/*{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},*/
		],
    
    initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
    },
    "order": [ 0, 'desc' ],
    "order": [ 20, 'desc' ],
    //"order": [ 0, 'desc' ],
    "scrollX": true,
    "scrollY": "50vh",
    "scrollCollapse": true,
    "lengthMenu": [100,200,300,500],
    //fixedColumns:   {
    //        left: 4
    //},
   // "aoColumnDefs": [{ "bVisible": false, "aTargets": [24, 25]}], //ocultar columnas
    //"aoColumnDefs": [{ "bVisible": false, "aTargets": [24]}],
    "ajax":{            
        "url": "bd/crudDO.php", 
        "method": 'POST', //usamos el metodo POST
        "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
        "dataSrc":""
    },
    "columns":[
        {"data": "Date"},
        {"data": "Year"},
        {"data": "Typ"},
        {"data": "DOrd"},
        {"data": "Rqs"},
        {"data": "GinName"},
        {"data": "RegNameOut"},
        {"data": "RegNameIn"},
        {"data" : "Ctc"},
        {"data": "PlcNameIn"},
        {"data": "Crss"},
        {"data": "PurNo"},
        {"data": "TTQty"},
        //{"data": "Adjustments"},
        {"data": "ToDO"},
        //{"data": "Embarked"},
        {"data": "Transit"},
        {"data": "Received"},
        {"data": "Rejected"},
        //{"data": "ToInv"},
        //{"data": "Invoiced"},
	    {"data": "Associated"},
        {"data": "ToAssociate"},
        {"data" : "DoCmt"},
        {"data" : "Cons"},
        {"data" : "PDF"},
        {"data" : "Certificate"},
        {"data" : "Cert"},
        {"data" : "Samp"},    
        
        {"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-info btn-sm btnPDF-DO'><i class='material-icons'>picture_as_pdf</i></button></div></div>"}
    ],
    columnDefs : [
        { targets : [22],
            render : function (data, type, row) {
              return data == '1' ? 'YES' : 'NO'
            }
        },
        { targets : [23],
            render : function (data, type, row) {
              return data == '1' ? 'YES' : 'NO'
            }
        },
        { targets : [24],
            render : function (data, type, row) {
              return data == '1' ? 'YES' : 'NO'
            }
        }
    ]
    
});     

$('.dataTables_length').addClass('bs-select');
     
});

$(document).ready(function(){
$(document).on("click", ".btnPDF-DO", function(){
    fila = $(this);           
    Date = $(this).closest('tr').find('td:eq(0)').text();
    Typ = $(this).closest('tr').find('td:eq(2)').text();
    DO = parseFloat($(this).closest('tr').find('td:eq(3)').text());
    Dep = $(this).closest('tr').find('td:eq(6)').text();
    Arr = $(this).closest('tr').find('td:eq(7)').text();
    Gin = $(this).closest('tr').find('td:eq(5)').text();
    Ctc = $(this).closest('tr').find('td:eq(8)').text();
    Cli = $(this).closest('tr').find('td:eq(9)').text();
    Crss = $(this).closest('tr').find('td:eq(10)').text();
    PurNo = $(this).closest('tr').find('td:eq(11)').text();
    window.open("/_Internal-User2021/bd/PDFDO.php?Date="+Date+"&Type="+Typ+"&DO="+DO+"&Out="+Dep+"&In="+Arr+"&Gin="+Gin+"&Ctc="+Ctc+"&Cli="+Cli+"&Crss="+Crss+"&PurNo="+PurNo+"");
    location.reload();
});

//Filtrar botón
 function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}


 $("#filtraboton").click(function(){
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    TDO = $('#TDO').val();
    if(TDO != "CON" && TDO != ""){
        $('#clientsDO').attr('disabled',false);
    }else{
        $('#clientsDO').attr('disabled',true);
    }
    $('#filtrarmodal').modal('show');
    
});

$('#TDO').on('change keyup', function(){
    TDO = $('#TDO').val();
    timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
    DepRegfil = $('#DepRegFil').val();
    ArrRegfil = $('#ArrRegFil').val();
    clientsDO = $('#clientsDO').val();
    fromdate = $('#fromdate').val();
    todate = $('#todate').val();

    if(TDO){
        $('#muestras').attr('disabled',true);
        $('#muestras').prop('selectedIndex',0);
        if(TDO != "CON")
            $('#clientsDO').attr('disabled',false);
    }else{
        $('#clientsDO').attr('disabled',true);
        $('#clientsDO').prop('selectedIndex',0);
        
        if(timeSelect || DepRegfil || ArrRegfil || fromdate || todate)
            $('#muestras').attr('disabled',true);
        else 
            $('#muestras').attr('disabled',false);
    }
});

$('#timeSelect').on('change', function () {
    timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
    DepRegfil = $('#DepRegFil').val();
    ArrRegfil = $('#ArrRegFil').val();
    clientsDO = $('#clientsDO').val();
    TDO = $('#TDO').val();
    if(timeSelect){
    timeSelect = parseInt(timeSelect);
    let today = new Date();
    //today = today.toLocaleTimeString('es-MX');
    let dateEnMilisegundos = 1000 * 60* 60* 24* timeSelect;
    let suma = today.getTime() - dateEnMilisegundos;
    let fechainicial = new Date(suma);
    //console.log(today);
    datetoday =  formatDate(today);
    datetosearch = formatDate(fechainicial);
    $('#fromdate').val(datetosearch);
    $('#todate').val(datetoday);
    $('#muestras').attr('disabled', true);
    $('#muestras').prop('selectedIndex',0);

    }else{
        $('#fromdate').val("");
        $('#todate').val("");
        if(DepRegfil || ArrRegfil || clientsDO || TDO )
             $('#muestras').attr('disabled', true);
        else
            $('#muestras').attr('disabled', false);


    }

    //console.log(datetoday); //today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()
});

$('#fromdate, #todate').on('change keyup', function () {
    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    muestras = $('select[name=muestras] option').filter(':selected').val();
    muestratext = $( "#muestras option:selected" ).text();
    DepRegFil = $('select[name=DepRegFil] option').filter(':selected').val();
    ArrRegFil = $('select[name=ArrRegFil] option').filter(':selected').val();
    timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
    clientsDO = $('#clientsDO').val();
    TDO = $('#TDO').val();

    if(fromdate == "" && todate == ""){
        $('#timeSelect').prop('selectedIndex',0);
        if(DepRegFil || ArrRegFil || TDO || clientsDO){
            $('#muestras').attr('disabled',true);
        }else{
            $('#muestras').attr('disabled',false);
        }
    }
    if(muestratext != "200 (default)" || DepRegFil || ArrRegFil){
        $('#buscafiltro').attr('disabled', false);
    }else $('#buscafiltro').attr('disabled', true);
});

//On change Region se bloquea last trucks
$(('#DepRegFil, #ArrRegFil')).on('change', function () {
    DepRegfil = $('#DepRegFil').val();
    ArrRegfil = $('#ArrRegFil').val();
    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    clientsDO = $('#clientsDO').val();
    TDO = $('#TDO').val();
    if(DepRegfil || ArrRegfil){
        $('#muestras').attr('disabled',true);
    }else
        if(TDO || clientsDO || fromdate || todate)
             $('#muestras').attr('disabled', true);
        else 
            $('#muestras').attr('disabled', false);
});
$('#muestras').on('change', function () {
    muestrastext = $( "#muestras option:selected" ).text();
    if(muestrastext != "200 (default)"){
        $('#DepRegFil').attr('disabled', true);
        $('#ArrRegFil').attr('disabled', true);
        $('#TDO').attr('disabled', true);
        $('#clientDO').attr('disabled', true);
        $('#timeSelect').attr('disabled', true);
        $('#fromdate').attr('disabled', true);
        $('#todate').attr('disabled', true);


        $('#todate').val("");
        $('#fromdate').val("");
        $('#DepRegFil').prop('selectedIndex',0);
        $('#ArrRegFil').prop('selectedIndex',0);
        $('#timeSelect').prop('selectedIndex',0);
        $('#TDO').prop('selectedIndex',0);
        $('#clientsDO').prop('selectedIndex',0);
    }else{
         $('#DepRegFil').attr('disabled', false);
                $('#ArrRegFil').attr('disabled', false);
                $('#TDO').attr('disabled', false);
                $('#clientDO').attr('disabled', false);
                $('#timeSelect').attr('disabled', false);
                $('#fromdate').attr('disabled', false);
                $('#todate').attr('disabled', false);
    }

});

$('#fromdate,#todate,#muestras,#DepRegFil,#ArrRegFil,#clientsDO,#TDO, #timeSelect').on('change keyup', function(){
    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    muestras = $('#muestras').val();
    timeSelect = $('#timeSelect').val();
    muestratext = $( "#muestras option:selected" ).text();
    DepRegfil = $('#DepRegFil').val();
    ArrRegfil = $('#ArrRegFil').val();
    clientsDO = $('#clientsDO').val();
    TDO = $('#TDO').val();

    if(fromdate || todate || muestratext != "200 (default)" || DepRegfil || ArrRegfil || clientsDO || TDO|| timeSelect){
        $('#buscafiltro').attr('disabled', false);
    }else{
        $('#buscafiltro').attr('disabled', true);
    }

});

$('#buscafiltro, #borrarFiltro, #cer').on('click', function () {
    boton = $(this).val();
    if(boton != 3){
        fromdate = $('#fromdate').val();
        todate = $('#todate').val();
        muestras = $('#muestras').val();
        DepRegfil = $('#DepRegFil').val();
        ArrRegfil = $('#ArrRegFil').val();
        clientsDO = $('#clientsDO').val();
        TDO = $('#TDO').val();

        var applyFilter = $.ajax({
            type: "POST",
            url: "bd/assignFilters.php",
            data: {
                boton       : boton,
                fromdate    : fromdate,
                todate      : todate,
                muestras    : muestras,
                DepRegFil   : DepRegfil,
                ArrRegFil   : ArrRegfil,
                clientsDO   : clientsDO,
                typeDO         : TDO
            }
        })
        applyFilter.done(function(data){
            console.log(data);
            tablaDO.ajax.reload(function(){

            if(boton == 1){
                $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Filtros asignados con éxito</div>')
                setTimeout(function() {
                    $('#alert1').fadeOut(1000);
                    $('#filtrarmodal').modal('hide');
                },1000);
            }
            else{
                // Habilitar botones que pueden estar deshabilitados
                $('#DepRegFil').attr('disabled', false);
                $('#ArrRegFil').attr('disabled', false);
                $('#muestras').attr('disabled', false);
                $('#TDO').attr('disabled', false);
                $('#clientDO').attr('disabled', false);
                $('#timeSelect').attr('disabled', false);
                $('#fromdate').attr('disabled', false);
                $('#todate').attr('disabled', false);

  
                $('#todate').val("");
                $('#fromdate').val("");
                $('#muestras').prop('selectedIndex',0);
                $('#DepRegFil').prop('selectedIndex',0);
                $('#ArrRegFil').prop('selectedIndex',0);
                $('#timeSelect').prop('selectedIndex',0);
                $('#TDO').prop('selectedIndex',0);
                $('#clientsDO').prop('selectedIndex',0);
               
                $('#buscafiltro').prop('disabled', true);
                $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"> <i class="bi bi-trash-fill"></i> Filtros borrados con éxito</div>')
                setTimeout(function() {
                    $('#alert1').fadeOut(1500);
                },1500);
            }

        });
        $('#aviso').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');      
    });

    }
});

});

$(document).ready(function(){
    var multipleCancelButton = new Choices('#DepRegFil', {
       removeItemButton: true,
       maxItemCount:10
     });     
});
$(document).ready(function(){
    var multipleCancelButton = new Choices('#ArrRegFil', {
       removeItemButton: true,
       maxItemCount:10
     });     
});
