<?php
session_start();
if (!isset($_SESSION['username'])) :
    include_once('index.php');
else :
    $Typ_Cli = $_SESSION['Typ_Cli'];
    $permiso = $_SESSION['Admin'];
?>
    <!doctype html>
    <html lang="es en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="img/ecom.png" />
        <title>User Management</title>

        <!-- CSS bootstrap -->
        <link rel="stylesheet" href=".//assets/bootstrap/css/bootstrap.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="./main.css">
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>

    <body>

        <!-- Scripts -->

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>


        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>

        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
        
        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>
        <!-- Multiselect en filtro-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
        <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>

        <?php if ($Typ_Cli == 1) { ?>
            <script type="text/javascript" src="adminproveed.js"></script>
        <?php } else { ?>
            <script type="text/javascript" src="adminproveed.js"></script>
        <?php } ?>
        

        <!-- Terminan Scripts -->

        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>

                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <li><a class="dropdown-item" href="trucks.php">Trucks</a></li>
                        <li><a class="dropdown-item" href="lots.php">Lots</a></li>
                        <?php if ($_SESSION['username'] == 'ISELA LARA' || $_SESSION['username'] == 'DARIO ROMERO' || $_SESSION['username'] == 'CESAR ARMAS' || $_SESSION['username'] == 'LUIS ANGEL RENDON TARIN'){?>
                        <li><a class="dropdown-item" href="bales.php">Bales</a></li>
                        <?php } ?>
                        <li><a class="dropdown-item" href="do.php">Delivery Order</a></li>
                        <li><a class="dropdown-item" href="adjustments.php">Adjustments</a></li>
                        <li><a class="dropdown-item" href="inv.php">Invoice</a></li>
                        <li><a class="dropdown-item" href="export.php">Exports</a></li>
                        <li><a class="dropdown-item" >Clients</a></li>
                        <li><a class="dropdown-item" href="routes.php">Routes</a></li>
                        <li><a class="dropdown-item" href="requisition.php">Requisition</a></li>
                        <li><a class="dropdown-item" href="CMS.php">CMS</a></li>
                        <?php if ($permiso == 1){?>
                        <li><a class="dropdown-item" href="adminusers.php">User Management</a></li>
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="#" >Proveed Management</a></li>
                        <?php } ?>
                    </ul>
                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-2" href="main.php">
                    <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <p>/ User Management</p>
                </div>
                <div class="container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                    <!-- Botones -->
                    <?php if ($Typ_Cli == 1) : ?>
                        <button id="btnNuevo" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Add"><i class="bi bi-plus-square"></i></button>
                    <?php endif ?>
                    <!--Boton exportar excel ultima conexión-->
                    <button id="ExportExcel" type="button" class="btn btn-success" data-toggle="modal tooltip" data-placement="bottom" title="Download last Connection Proveed .Xlsx"><i class="fas fa-file-excel"></i></button>
                    <button id="mailReminder" type="button" class="btn btn-primary" data-toggle="modal tooltip" data-placement="bottom" title="Send reminder"><i class="bi bi-chat-right-dots-fill"></i></button>
                </div>
                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                    <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"><?php echo $_SESSION['username']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->

        <!-- Aquí inicia todo código de tablas etc -->
        <!-- Inicio de la tabla -->
        <div class="card card-body" style="opacity:100%;">
            <div class="table-responsive" style="opacity:100%;">
                <table id="tablaClients" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;">
                    <thead style="background-color: #65ac7c;" style="opacity:100%;">
                        <tr>
                            <th class="th-sm">Proveed ID</th>
                            <th class="th-sm">Proveed Name</th>
                            <th class="th-sm">Region</th>
			                <th class="th-sm">Email</th>
                            <th class="th-sm">Status</th>
                            <th class="th-sm">GIN</th>                             
                            <?php if ($Typ_Cli == 1) { ?>
                                <th class="no-exportar"></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                        <th class="th-sm">Proveed ID</th>
                            <th class="th-sm">Proveed Name</th>
                            <th class="th-sm">Region</th>
			                <th class="th-sm">Email</th>
                            <th class="th-sm">Status</th>
                            <th class="th-sm">GIN</th>    

                            <?php if ($Typ_Cli == 1) { ?>
                                <th class="no-exportar"></th>
                            <?php } ?>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!--Modal para CRUD-->
        <div class="modal fade in" data-bs-backdrop="static" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formproveed">
                        <div class="modal-body">
                            <div class="shadow-sm p-3 mb-5 bg-body rounded">
                                 <div class="row ">
                           
                                            <div class="col-lg-5">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Name Proveed<font size=2> *</font></label>
                                                    <input type="text" class="form-control" style="text-transform:uppercase;" id="nameuser" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">ProveedID <font size=2> *</font></label>
                                                    <input type="text" class="form-control"  style="text-transform:uppercase;" id="UserID" required>
                                                    <span class="badge rounded-pill bg-danger" id ="userexist"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label for="" class="col-form-label">Region<font size=2> *</font></label>
                                                    <select class="form-control form-control-sm" id="region" name="State">
                                                        <option value="" selected disabled>-Select-</option>
                                                        <option value="MEXICALI">MEXICALI</option>
                                                        <option value="OBREGON">OBREGON</option>
                                                        <option value="LAGUNA">LAGUNA</option>
                                                        <option value="GOMEZ">GOMEZ</option>
                                                        <option value="OASIS">OASIS</option>
                                                        <option value="MATRIZ">MATRIZ</option>
                                                        <option value="ASCENSION">ASCENSION</option>
                                                        <option value="MATAMOROS">MATAMOROS</option>
                                                        <option value="ALDAMA">ALDAMA</option>
                                                        <option value="JUAREZ">JUAREZ</option>
                                                        <option value="PUEBLA">PUEBLA</option>
                                                        <option value="MANTE">MANTE</option>
                                                                                                                                    
                                                    </select>
                                                </div>
                                            </div>
                            </div>
                            <div class="row">
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label for="" class="col-form-label">Email:</label>
                                            <input type="text" class="form-control" style="text-transform:uppercase;" id="email"> 
                                            </div>
                                        </div>

                                        <div class="col-lg-5">
                                            <div class="form-group">
                                            <label for="" class="col-form-label">GIN:</label>
                                            <input type="text" class="form-control" style="text-transform:uppercase;" id="gin">                              
                                            </div>
                                        </div>

                                        
                            </div>

                     </div>
                     <font size=2>*Required Fields</font>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" id="btnGuardar" class="btn btn-dark">Save</button>
                        </div>
                           
                       </div>
                      </div>
                  
                  

                    </form>
                </div>
            </div>
        </div>

        <!--Modal para mail-->
        <div class="modal fade in" data-bs-backdrop="static" id="mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" id="close" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formMail">
                        <div class="modal-body">
                            <div class="row">
                                <!-- <div class="col-lg-6"></div>  -->
                                <div class="col-lg-6">
                                    <div class="input-group mt-1">
                                        <label class="input-group-text" for="subject" style="font-weight: bold;">Asunto:</label>
                                        <input type="text" class="form-control subject" id="subject" value="Recordatorio portal de proveedores" readonly>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row ">
                                <div class="col-lg-10">
                                    <div class="form-group">
                                        <label for="" class="col-form-label" style="font-weight: bold;">Proveedores</label>
                                        <select class="form-control form-control-sm" id="prov" name="prov" multiple placeholder="Choose...">
                                        </select>
                                    </div>
                                </div>
                            </div>    
                            <br>
                            <div class="col-auto text-end">
                                <button type="button" id="cancel" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                <button type="submit" id="btnSend" class="btn" style="background-color: #17562c; color: white;" disabled>Send</button>
                            </div>  
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>


    </body>

    </html>
<?php
endif;
?>