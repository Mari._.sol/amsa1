<?php
session_start();
if (!isset($_SESSION['username'])) :
    include_once('index.php');
else :
    //$_SESSION['Reg'];
    $Typ_CMS = $_SESSION['Typ_CMS'];
    $permiso = $_SESSION['Admin'];
    $Priv = $_SESSION['Priv'];
    $filtersSes = $_SESSION['Filters'];
    $rates = $_SESSION['ViewRates'];
    include_once('bd/correo-model.php');
    $regiones = getRegiones();
    $gines = getGines();
    $_SESSION['Filters'] = array("muestras" => "0",
                                "LotCMS" => "",
                                "DOCMS" => "",
								);
    
    foreach($regiones as $reg){
        $regsID[] = $reg['IDReg'];
        $regs[] = $reg['RegNam'];
    }

    foreach($gines as $gin){
        $ginID[] = $gin['IDGin'];
        $gins[] = $gin['GinName'];
    }

?>
    <!doctype html>
    <html lang="es en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>-->

        <link rel="shortcut icon" href="img/ecom.png" />

         <title>CMS</title>
        
        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="./assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="./assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="./main.css">
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>

    <body>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>

        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <!-- librerias necesarias para finalizar sesion por inactividad -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
        <script type="text/javascript" src="timer.js"></script> 

        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>

        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>

        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>

        <?php if ($Typ_CMS == 1) { ?>
            <script type="text/javascript" src="mainCMS.js"></script>
        <?php } else {  ?>
            <script type="text/javascript" src="mainCMS.js"></script>
        <?php } ?>


        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item nav-item col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>

                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <li><a class="dropdown-item" href="trucks.php">Trucks</a></li>
                        <li><a class="dropdown-item" href="lots.php">Lots</a></li>
                        <?php if ($_SESSION['username'] == 'DARIO ROMERO' ||$_SESSION['username'] == 'ISELA LARA' || $_SESSION['username'] == 'CESAR ARMAS' || $_SESSION['username'] == 'LUIS ANGEL RENDON TARIN' || $_SESSION['username'] == 'JOSE RENDON') { ?>
                        <li><a class="dropdown-item" href="bales.php">Bales</a></li>
                        <?php } ?>
                        <li><a class="dropdown-item" href="do.php">Delivery Order</a></li>
                        <li><a class="dropdown-item" href="adjustments.php">Adjustments</a></li>
                        <li><a class="dropdown-item" href="inv.php">Invoice</a></li>
                        <li><a class="dropdown-item" href="export.php">Containers</a></li>
                        <li><a class="dropdown-item" href="clients.php">Clients</a></li>
                        <li><a class="dropdown-item" href="routes.php">Routes</a></li>
                        <li><a class="dropdown-item" href="requisition.php">Requisition</a></li>
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="CMS.php">CMS</a></li>
                        <?php if ($permiso == 1){?>
                        <li><a class="dropdown-item" href="adminusers.php">User Management</a></li>
                        <?php } ?>
                        <?php if ($rates == 1){?>
                        <li><a class="dropdown-item" href="Rates.php">Rates</a></li>
                        <?php } ?>
                    </ul>
                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-2 col-md-2 col-sm-1 col-xs-2" href="main.php">
                    <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <p>/ CMS</p>
                </div>
                <div class="container-fluid col-xl-7 col-lg-4 col-md-4 col-sm-2 col-xs-1">
                    <!-- Botones -->
                        <button id="SubeLots" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Download Sube_Lots"><i class="bi bi-filetype-csv"></i></button>
                        <button id="SubePacasLots" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Download SubePacas_Lots"><i class="bi bi-filetype-csv"></i></button>	
                        <button id="ExportList" type="button" class="btn btn-success" data-toggle="modal tooltip" data-placement="bottom" title="Download List Xlsx"><i class="bi bi-file-earmark-excel-fill"></i></button>
                        <button id="ExportListPDF" type="button" class="btn btn-danger" data-toggle="modal tooltip" data-placement="bottom" title="Download List PDF"><i class="bi bi-file-pdf-fill"></i></button>
                        <button id="SearchBales" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Search by Bale"><i class="bi bi-search"></i></button>
                        <button id="ReportStock" type="button" class="btn btn-success" data-toggle="modal tooltip" data-placement="bottom" title="Report Stock"><i class="bi bi-folder"></i></button>    
                        
                       
                </div>
                </div>
                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-4 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                    <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"><?php echo $_SESSION['username']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Inicio de la tabla -->
        <div class="card card-body" style="opacity:100%;">
            <!-- Identificadores para archivos CMS  -->  
            <div class="d-flex justify-content-end">
                <div class="col-lg-12">
                    <div class="input-group mb-3">
                    <label class="input-group-text" >DO</label>
                        <input type="text" class="form-control" id="DOsCMS" style="text-transform:uppercase;" disabled>
                        <label class="input-group-text" >Lots</label>
                        <input type="text" class="form-control" id="LotsCMS" style="text-transform:uppercase;" disabled>
                        <label class="input-group-text" >Date</label>
                        <input type="date" class="form-control" id="DateCMS" style="text-transform:uppercase;" value="<?php echo date("Y-m-d");?>" onkeydown="return false">
                    </div>
                </div>
            </div>                

            <div class="table-responsive" style="opacity:100%;">
            <div>
                <button id="filtraboton" type="button" class="btn btn-info btn-filtrar" data-toggle="modal tooltip" data-placement="bottom" title="Filter by"><i class="bi bi-funnel-fill"></i></button>
            </div>
                <table id="tablaCMS" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;">
                    <thead style="background-color: #65ac7c;" style="opacity:100%;">
                        <tr>
                            <th class="th-sm">Year</th>
                            <th class="th-sm">Gin</th>
                            <th class="th-sm">Lot</th>
                            <th class="th-sm">Bal</th>
                            <th class="th-sm">Weight</th>
                            <th class="th-sm">Bales in Nav</th>
                            <th class="th-sm">Note</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </div>

        <!-- Modal filtros bales -->
        <div class="modal hide fade" id="filtrarmodal" data-bs-backdrop="static" tabindex="-1" role="dialog" data-bs-keyboard="false" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-tittle">Filter table by</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="filtros" class="filtros">
                            <div class="input-group mb-3">
                                <label class="input-group-text" for="idDO">DO</label>
                                <input  class="form-control me-2" type="text" name="DO" id="DOCMS" placeholder="Write DO">
                            </div>
                            <div class="input-group mb-3">
                                <label class="input-group-text" for="idDO">Lot</label>
                                <input  class="form-control me-2" type="text" name="Lot" id="LotCMS" placeholder="Write Lots">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div style="height: 2.5em;" id="aviso"></div>
                    <button id="cer" type="button" class="btn btn-light" value="3" data-bs-dismiss="modal" >Close</button>
                        <button id="borrarFiltro" type="button" value="0" class="btn btn-danger Filtro" >Clear filters</button>
                        <button id="buscafiltro" type="button" value="1" class="btn btn-primary Filtro" >Apply filters</button>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal busqueda por paca -->
        <div class="modal hide fade" id="modal-SearchBales" data-bs-backdrop="static" tabindex="-1" role="dialog" data-bs-keyboard="false" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-tittle">Search by Bale</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="Bales" class="filtros">
                            <div class="input-group mb-3">
                                <label class="input-group-text" for="DepRegFil">Región</label>
                                <select class="form-select me-2" name="SBReg" id="SBReg">
                                </select>
                                <label class="input-group-text" for="Gin">Gin</label>
                                <select class="form-select me-2" id="SBGin" name="SBGin" >   
                                </select>
                            </div>
                            <div class="input-group mb-3">
                                <label class="input-group-text" for="asLots">Bale*</label>
                                <input  class="form-control me-2" type="text" name="SBBale" id="SBBale" placeholder="Write Bales" required>
                                <label class="input-group-text" for="timeSelect">Crop</label>
                                <select class="form-select me-2" id="SBCrp" name="SBCrp">
                                    <option value="2022">2022</option>
                                    <option value="2023" selected>2023</option>
                                    <option value="2024">2024</option>
                                </select>
                                <button id="Search" type="button" class="btn btn-success" data-toggle="modal tooltip" data-placement="bottom" title="Search"><i class="bi bi-search"></i></button>
                            </div>
                        </form>

                        <div id="InfBales" class="input-group input-group mb-3 ">
                            <!-- Tabla Bales -->
                            <div style="width:1100px; height:300px; overflow:auto;" id="AreaOriginal"></div>
                        </div>
                        <font size=2>*Required Fields</font>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Report Stock -->
        <div class="modal hide fade" id="modal-ReportStock" data-bs-backdrop="static" tabindex="-1" role="dialog" data-bs-keyboard="false" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-tittle">Report Stock</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="Stock" class="filtros">
                            <div class="input-group mb-3">
                                <label class="input-group-text" for="DepRegFil">Region*</label>
                                <select class="form-select me-2" name="RegStock" id="RegStock">
                                    <option value="">Choose...</option>
                                    <?php $i = 0; while($i < count($regs)): ?>
                                    <option value="<?php echo $regsID[$i]; ?>"><?php echo $regs[$i]; ?></option>
                                    <?php $i++; endwhile; ?>
                                    <option value="652,653,656">CHIHUAHUA TODOS</option>
                                </select>
                                <button id="SearchStock" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Search"><i class="bi bi-search"></i></button>
                            </div>
                        </form>
                        <font size=2>*Required Fields</font>
                    </div>
                </div>
            </div>
        </div>
    </body>
    </html>
<?php
endif;
?>
