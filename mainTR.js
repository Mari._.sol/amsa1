$(document).ready(function() {
    
    $('#tablaTrk tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    } );

    var TrkID, opcion;
    opcion = 4;
    
    tablaTrk = $('#tablaTrk').DataTable({
        //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			{
			    extend:    'excelHtml5',
			    text:      '<i class="fas fa-file-excel"></i> ',
			    titleAttr: 'Export to Excel',
			    className: 'btn btn-success',
			    exportOptions: {
			        columns: ":not(.no-exportar)"
			    }
			},
			{
			    extend:    'pdfHtml5',
			    text:      '<i class="fas fa-file-pdf"></i> ',
			    titleAttr: 'Export to PDF',
			    orientation: 'landscape',
			    className: 'btn btn-danger',
			    exportOptions: {
			        columns: ":not(.no-exportar)"
			    }
			},
             {
                text: '<i class="bi bi-funnel-fill"></i>',
                titleAttr: 'Filtrar por',
                orientation: 'landscape',
                className: 'btn btn-info btn-filtrar',
                attr:{
                        id:'filtraboton',
                        "data-toggle": 'modal tooltip',
                        "disabled": false
                }
            },
			/*{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},*/
        ],
    
        initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        },
        "order": [ 0, 'desc' ],
        "scrollX": true, 
        "scrollY": "50vh",
        "scrollCollapse": true,
        "lengthMenu": [100,200,300,500], 
        "ajax":{            
            "url": "bd/crudTrk.php", 
            "method": 'POST', //usamos el metodo POST
            "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
            "dataSrc":""
        },
        "columns":[
            {"data": "TrkID"},
            {"data": "DO"},
            {"data": "Typ"},
            {"data": "LotsAssc"}, //LotsAssc
            {"data": "LotQty"}, //CrgQty
            {"data": "SchOutDate"},
            {"data": "OutDat"},
            {"data": "SchOutTime"},
            {"data": "OutTime"},
            {"data": "SchInDate"},
            {"data": "InDat"},
            {"data": "SchInTime"},
            {"data": "InTime"},
            {"data": "GinName"},
            {"data": "RegNameOut"},
            {"data": "RegNameIn"}, //Sigue contrato
            {"data": "Ctc"},
            {"data": "PlcNameIn"}, //Consulta con DO 
            {"data": "TNam"},
            {"data": "WBill"},
            {"data": "DrvLcs"},
            {"data": "TrkLPlt"},
            {"data": "TraLPlt"},
            {"data": "DrvNam"},
            {"data": "DrvTel", width: 200, render: function ( toFormat ) {
                return toFormat.toString().replace(
                  /(\d\d\d)(\d\d\d)(\d\d\d\d)/g, '$1-$2-$3'
                ); }},
            {"data": "LiqWgh", render: $.fn.dataTable.render.number( ',')}, //PWgh  
            {"data": "OutWgh", render: $.fn.dataTable.render.number( ',')},
            {"data": "InWgh", render: $.fn.dataTable.render.number( ',')},
            {"data": "RqstID"},
            {"data": "PO"},
            {"data": "RO"},
            {"data": "Cost"},
            {"data": "Comments"},
            {"data": "Status"},
            {"data": "IrrDat"},
            {"data": "DateCreated", width: 200},
            {"data": "RemisionPDF"},
            {"data": "XML"},
            {"data": "Samples"},
            {"data":"Incident"},
            {"data":"Invoice"},
            {"data":"TicketPeso"},
            {"data":"NumEcon"},
            {"data": "Cert", // aqui 
                "render": function(data) { 
                    return data == "1" ? "SI" : "NO"; 
                } 
            }, 
            {"data":"FileCertificados",
                "render": function(data) { 
                    return data == '' ||data == null ? 'NO' : 'YES'
                }
            }
            //{"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button></div></div>"}
        ],
        
         columnDefs : [
       
            //COST
            { targets : [31],
                render : function (data, type, row) {
                  return data == '1' ? 'YES' : 'NO'
                }
              },

              //PDF CARTA PORTE
              { targets : [36],
                render : function (data, type, row) {
                  return data == '1' ? 'YES' : 'NO'
                }
              },
              //XML
              { targets : [37],
                render : function (data, type, row) {
                  return data == '1' ? 'YES' : 'NO'
                }
              },

              //Muestras
              { targets : [38],
                render : function (data, type, row) {
                  return data == '1' ? 'True' : 'False'
                }
              },
            //INCIDENT
            { targets : [39],
                render : function (data, type, row) {
                    return data == '1' ? 'YES' : 'NO'
                }
            },
            //INVOICE
            { targets : [40],
                render : function (data, type, row) {
                    return data == '' ||data == null ? '0' : data
                }
            },
             //TICKET PESO

             { targets : [41],
                render : function (data, type, row) {
                    return data == '' ||data == null ? 'NO' : 'YES'
                }
            }

            

            
       ]
        
        
        
        
        
    });     

    $('.dataTables_length').addClass('bs-select');
    
    
    /*function LoadLots(){
            $("#LotsAssc").empty();
            $('#AsscLots').val("");
            $('#QtyDO').val("");
            $('#PWgh').val("");
            var opts = "";
            opcion = 6;
            DO = $.trim($('#DOrd').val());
            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",
                data:  {DO:DO ,opcion:opcion},    
                success: function(data){
                    opts = JSON.parse(data);
                    for (var i = 0; i< opts.length; i++){
                        $('#LotsAssc').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '-' + opts[i].Qty + ' bc </option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });
    };*/
    
    
    var fila; //captura la fila, para editar o eliminar -- submit para el Alta y Actualización
    $('#formTrk').submit(function(e){
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
        DOrd = $.trim($('#DOrd').val());
        TrkLPlt = $.trim($('#TrkLPlt').val().toUpperCase());    
        TraLPlt = $.trim($('#TraLPlt').val().toUpperCase());
        TNam = $.trim($('#TNam').val());    
        WBill = $.trim($('#WBill').val().toUpperCase());
        DrvLcs = $.trim($('#DrvLcs').val().toUpperCase());
        DrvNam = $.trim($('#DrvNam').val().toUpperCase());
        DrvTel = $.trim($('#DrvTel').val());
        OutDat = $.trim($('#OutDat').val());
        OutTime = $.trim($('#OutTime').val());
        OutWgh = $.trim($('#OutWgh').val());
        InDat = $.trim($('#InDat').val());
        InTime = $.trim($('#InTime').val());
        InWgh = $.trim($('#InWgh').val());
        TrkCmt = $.trim($('#TrkCmt').val().toUpperCase());
        RqstID = $.trim($('#RqstID').val());
        RO = $.trim($('#RO').val());
        FreightC = $.trim($('#FreightC').val());
        AsscLots = $.trim($('#AsscLots').val().toUpperCase());
        CrgQty = $.trim($('#QtyDO').val());
        PWgh = $.trim($('#PWgh').val());
        Status = $.trim($('#Status').val());
        //val = $.isNumeric(TNam);
        $.ajax({
            url: "bd/crudTrk.php",
            type: "POST",
            datatype:"json",    
            data:  {TrkID:TrkID, TrkLPlt:TrkLPlt, TraLPlt:TraLPlt, TNam:TNam, WBill:WBill, DrvLcs:DrvLcs, DrvNam:DrvNam, DrvTel:DrvTel, CrgQty:CrgQty, PWgh:PWgh, OutDat:OutDat, OutTime:OutTime, OutWgh:OutWgh, InDat:InDat, InTime:InTime, InWgh:InWgh, TrkCmt:TrkCmt, DOrd:DOrd, RqstID:RqstID, RqstID1:RqstID1, RO:RO, FreightC:FreightC, AsscLots:AsscLots, Status:Status, opcion:opcion}, //AsscLots:AsscLots,
            success: function(data) {
                tablaTrk.ajax.reload(null, false);
            }
        });
        $('#modalCRUD').modal('hide');
    });
    
    
    //submit para Carga masiva PO's
$('#formFile').submit(function(e){
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
    var comprobar = $('#csv').val().length;
    var extension = $('#csv').val().split(".").pop().toLowerCase();
    //validar extension
    /*if($.inArray(extension, ['csv']) == -1)
	{
		alert("Invalid File");
		retornarError = true;
		$('#csv').val("");
	}else{*/
    if (comprobar>0){
        var formulario = $("#formFile");
        var archivos = new FormData();
        var url = "bd/procesarPO.php";
            for (var i=0; i<(formulario.find('input[type=file]').length); i++){
                archivos.append((formulario.find('input[type="file"]:eq('+i+')').attr("name")),((formulario.find('input[type="file"]:eq('+i+')'))[0].files[0]));
            }
    $.ajax({
          url: "bd/procesarPO.php",
          type: "POST",
          contentType: false,
          data:  archivos,
          processData: false,
          success: function(data) {
            //var opts = JSON.parse(data);
            //alert (opts);
            //alert (data);
            if (data == 'OK'){
                alert("Successful Import");
                $('#modalFile').modal('hide');
                tablaTrk.ajax.reload(null, false);
                //location.reload();
                return false;
            } else if (data == 'Columns'){
                alert("Check the number of columns.");
                return false;
            }else if (data == 'Exist'){
                alert ("Existen los lotes");
                return false;
            }else{
                alert("Successful Import"); //alert("Import Error");
                $('#modalFile').modal('hide');
                tablaTrk.ajax.reload(null, false);
                //location.reload();
                return false;
           }
          }
        });
        return false;    
    }else{
        alert ("Select Excel file to import");
        return false;
    }
    //}   	        
    //$('#modalFile').modal('hide');
    //location.reload();
});
    
    
    
    // Formulario Buscar OE y Lots  *comentario 4 junio****
    $('#formOEL').submit(function(e){                         
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
        TrkLPlt = $.trim($('#TrkLPlt').val());    
        TraLPlt = $.trim($('#TraLPlt').val());
        TNam = $.trim($('#TNam').val());                             
        $.ajax({
            url: "bd/crudTrk.php",
            type: "POST",
            datatype:"json",    
            data:  {TrkID:TrkID, TrkLPlt:TrkLPlt,opcion:opcion},    
            success: function(data) {
                tablaTrk.ajax.reload(null, false);
            }
        });			        
        //$('#modalOEL').modal('hide');											     			
    });


    // Formulario Splits ***************************************************************************
    $('#Split-Lots').submit(function(e){                         
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
        var ban = 0;
        LocSplit = $.trim($('#LocSplit').val());    
        DOSplit = $.trim($('#DOSplit').val());
        // Obtener texto del selector y la cantidad de pacas totales del lote ******
        var SelectLot = document.getElementById("LotsSplit");
        var LotTxt = SelectLot.options[SelectLot.selectedIndex].text;
        LotIDSplit = $.trim($('#LotsSplit').val());
        BC = LotTxt.split(" - ");
        BC = BC[1];
        Bal = LotTxt.split(" - ");
        Bal = Bal[0];
        // ***************************************************************************
        Split = $.trim($('#Split').val());
        Split = Split.split(",");
        tam = Split.length;
        //validar que las particiones sean positivas
        for (i=0; i<=tam; i++){
            if (Split[i] < 0){
                ban = 1;
            }
        }
        if (ban == 1){
            alert ("Partitions must be positive");
        }else{
        
            sum = 0;
            $.each(Split,function(){sum+=parseFloat(this) || 0; }); // suma de las particiones
            if (sum>BC || sum<BC){
                alert ("The sum total of the partitions must match the number of bales in the Lot");    
            }else{
                opcion = 14;
                var respuesta = confirm("Are you sure you want to partition the Lot "+Bal+" in "+Split+"?");                
                if (respuesta) {            
                    $.ajax({
                        url: "bd/crudTrk.php",
                        type: "POST",
                        datatype:"json",    
                        data:  {LotIDSplit:LotIDSplit, Split:Split, opcion:opcion},    
                        success: function(data) {
                            tablaTrk.ajax.reload(null, false);
                        }
                    });
	            $('#SplitLots').modal('hide');
                }
            }
        }
        
        /*$.ajax({
            url: "bd/crudTrk.php",
            type: "POST",
            datatype:"json",    
            data:  {TrkID:TrkID, TrkLPlt:TrkLPlt,opcion:opcion},    
            success: function(data) {
                tablaTrk.ajax.reload(null, false);
            }
        });*/			        
        //$('#SplitLots').modal('hide');											     			
    });




        
    //para limpiar los campos antes de dar de Alta una Persona
    $("#btnNuevo").click(function(){
        $("#NoPWgh").hide();
        $("#NoWgh").hide();
        $("#OutWghMin").hide();
        $("#OutWghMax").hide();
        $("#InWghMin").hide();
        $("#InWghMax").hide();
        $("#TNam").empty();
        $("#RqstID").empty();
        $("#LotsAssc").empty();
        $("#DOrd").prop("disabled", false);
        //ValidateWgh();
        opcion = 1; //alta
        TrkID=null;
        RqstID1=null;
        $("#formTrk").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("New Truck");
        $('#modalCRUD').modal('show');	    
    });
    
    
    //carga PO's
    $("#btnFile").click(function(){
        $("#formFile").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Import Data");
        $('#modalFile').modal('show');	    
    });


    //Boton para split de lotes
    $("#btnSplit").click(function(){
        $("#LocSplit").empty();
        $("#DOrdSplit").empty();
        $("#LotsSplit").empty();
        LoadRegion();
        $('#LocSplit').append('<option value="" disabled selected >- Select -</option>');
        //ValidateWgh();
        //opcion = 1; //alta
        //TrkID=null;
        $("#Split-Lots").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Split a Lot");
        $('#SplitLots').modal('show');	    
    });




    //Editar        
    $(document).on("click", ".btnEditar", function(){
        $("#NoPWgh").hide();
        $("#NoWgh").hide();
        $("#OutWghMin").hide();
        $("#OutWghMax").hide();
        $("#InWghMin").hide();
        $("#InWghMax").hide();
        //$("#formTrk").trigger("reset");
        $("#TNam").empty();
        $("#RqstID").empty();
        $("#FreightC").val("");
        //ValidateWgh();  
        opcion = 2;//editar
        fila = $(this).closest("tr");	        
        TrkID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID		            
        DOrd = parseFloat(fila.find('td:eq(1)').text());
        Typ = fila.find('td:eq(2)').text();
        LotsAssc = fila.find('td:eq(3)').text();
        Qty = parseFloat(fila.find('td:eq(4)').text());
        OutDat = fila.find('td:eq(5)').text();
        OutTime = fila.find('td:eq(6)').text();
        InDat = fila.find('td:eq(7)').text();
        InTime = fila.find('td:eq(8)').text();
        DepReg = fila.find('td:eq(10)').text(); 
        ArrReg = fila.find('td:eq(11)').text();
        Cli = fila.find('td:eq(13)').text();
        //TNam = fila.find('td:eq(12)').text(); 
        WBill = fila.find('td:eq(15)').text();
        DrvLcs = fila.find('td:eq(16)').text();
        TrkLPlt = fila.find('td:eq(17)').text();
        TraLPlt = fila.find('td:eq(18)').text();
        DrvNam = fila.find('td:eq(19)').text();
        DrvTel = fila.find('td:eq(20)').text();
        PWgh = parseFloat(fila.find('td:eq(21)').text());
        OutWgh = parseFloat(fila.find('td:eq(22)').text());
        InWgh = parseFloat(fila.find('td:eq(23)').text());
        RO = parseFloat(fila.find('td:eq(26)').text());
        TrkCmt = fila.find('td:eq(27)').text();
        Status = fila.find('td:eq(28)').text();
        //Qty = parseFloat(fila.find('td:eq(3)').text());
        $("#TrkID").val(TrkID);
        $("#TrkLPlt").val(TrkLPlt);
        $("#OutRegDO").val(DepReg);
        $("#InRegDO").val(ArrReg);
        $("#TypDO").val(Typ);
        $("#Client").val(Cli);
        $("#TraLPlt").val(TraLPlt);
        $('#TNam').append('<option class="form-control selected">' + fila.find('td:eq(14)').text() + '</option>'); //$("#TNam").val(TNam);
        $('#RqstID').append('<option class="form-control selected">' + fila.find('td:eq(24)').text() + '</option>'); //$("#TNam").val(TNam);
        $("#WBill").val(WBill);
        $("#DrvLcs").val(DrvLcs);
        $("#DrvNam").val(DrvNam);
        $("#DrvTel").val(DrvTel);
        $("#OutDat").val(OutDat);
        $("#OutTime").val(OutTime);
        $("#OutDat").val(OutDat);
        $("#OutWgh").val(OutWgh);
        $("#InDat").val(InDat);
        $("#InTime").val(InTime);
        $("#RO").val(RO);
        $("#InWgh").val(InWgh);
        $("#TrkCmt").val(TrkCmt);
        $("#DOrd").val(DOrd);
        //$("#RqstID").val(RqstID);
        $("#Status").val(Status);
        $("#LotsAssc").empty();
        $('#AsscLots').val(LotsAssc);
        $('#QtyDO').val(Qty);
        $('#PWgh').val(PWgh);
        RqstID1 = $.trim($('#RqstID').val());
        RqstID = $.trim($('#RqstID').val());
        $("#RqstID").change( function() {
            RqstID1 = $.trim($('#RqstID').val());
            if (RqstID != RqstID1){
                RqstID1 = RqstID;
            }
        });
        
        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $(".modal-title").text("Edit Truck");		
        $('#modalCRUD').modal('show');		   
    });

    //Borrar
    $(document).on("click", ".btnBorrar", function(){
        fila = $(this);           
        TrkID = parseInt($(this).closest('tr').find('td:eq(0)').text()) ;		
        opcion = 3; //eliminar        
        var respuesta = confirm("¿Está seguro que deseas deshabilitar el registro "+TrkID+"?");                
        if (respuesta) {            
            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",    
                data:  {opcion:opcion, TrkID:TrkID},    
                success: function() {
                    tablaTrk.row(fila.parents('tr')).remove().draw();                  
                }
            });	
        }
    });
    
    //Buscar OE y lotes
    $(document).on("click", ".btnBuscar", function(){
        //$("#Lot").empty();
        //opcion = 5; //
        fila = $(this);
        TrkID = parseInt($(this).closest('tr').find('td:eq(0)').text()); //Obtener el ID del camion a asociar
        $("#formOEL").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Search OE y Lots");
        $('#modalOEL').modal('show');
    });
    
    //Actualizar lot
    /*function update(){
        $("#Lot").empty();
        $("#QtyLot").val("");
        var opts = "";
        opcion = 5;
        DO = $.trim($('#DO').val());
        //id = TrkID;
        $.ajax({
            url: "bd/crudTrk.php",
            type: "POST",
            datatype:"json",
            data:  {DO:DO ,opcion:opcion},    
            success: function(data){
                opts = JSON.parse(data);
                for (var i = 0; i< opts.length; i++){
                    $('#Lot').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '</option>');
                    $('#QtyLot').val(opts[0].Qty);
                }
            },
            error: function(data) {
                alert('error');
            }
        });
    }*/
    
    //Vaciar lotes en select html dependientes de la OE ingresada 
    //$(document).on("click", ".btnBuscarLot", function(){
    //    update();
        /*opcion = 5;
        DO = $.trim($('#DO').val());
        //id = TrkID;
        $.ajax({
              url: "bd/crudTrk.php",
              type: "POST",
              datatype:"json",
              data:  {DO:DO ,opcion:opcion},    
              success: function(data){
                  $("#Lot").empty();
                  //$("#QtyLot").empty();
                  var opts = JSON.parse(data);
                  for (var i = 0; i< opts.length; i++){
                      $('#Lot').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '</option>');
                      $('#QtyLot').val(opts[0].Qty);
                  }
               },
            error: function(data) {
                alert('error');
            }
        });*/
    //});
    
    
    //Qty dependiente de select de Lotes
    $("#Lot").on("change", function(){
        opcion = 6;
        id = $("#Lot").val();
        $.ajax({
            url: "bd/crudTrk.php",
            type: "POST",
            datatype:"json",
            data:  {id:id ,opcion:opcion},    
            success: function(data){
                var opts = JSON.parse(data);
                $('#QtyLot').val(opts[0].Qty);
            },
            error: function(data) {
                alert('error');
            }
        });
    });
    
    //Cargar lotes comentado 27 jul
    function LoadLots(){
            $("#LotsAssc").empty();
            $('#AsscLots').val("");
            $('#QtyDO').val("");
            $('#PWgh').val("");
            var opts = "";
            opcion = 6;
            DO = $.trim($('#DOrd').val());
            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",
                data:  {DO:DO ,opcion:opcion},    
                success: function(data){
                    opts = JSON.parse(data);
                    for (var i = 0; i< opts.length; i++){
                        $('#LotsAssc').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '-' + opts[i].Qty + ' bc </option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });
    }
    
    //$("#DOrd").on("change", function(){
    //        LoadLots();
            //val = document.getElementsByClassName("modal-title"); //.value; //$.trim($(".modal-title"));
            /*$("#LotsAssc").empty();
            $('#AsscLots').val("");
            $('#QtyDO').val("");
            $('#PWgh').val("");
            var opts = "";
            opcion = 6;
            DO = $.trim($('#DOrd').val());
            $.ajax({
                url: "bd/crudTrk.php",
                type: "POST",
                datatype:"json",
                data:  {DO:DO ,opcion:opcion},    
                success: function(data){
                    opts = JSON.parse(data);
                    for (var i = 0; i< opts.length; i++){
                        $('#LotsAssc').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + '-' + opts[i].Qty + '</option>');
                    }        
                },
                error: function(data) {
                    alert('error');
                }
            });*/
    //});

    /*function addLot(){
        opcion = 7;
        id = parseInt($.trim($('#Lot').val()));
        CrgQty = parseInt($.trim($('#QtyLot').val()));
        TrkID = parseInt(TrkID);
        $.ajax({
            url: "bd/crudTrk.php",
            type: "POST",
            datatype:"json",
            data:  {id:id, TrkID:TrkID , CrgQty:CrgQty, opcion:opcion},    
            success: function(data){
                tablaTrk.ajax.reload(null, false);
                alert ("Actualizado con exito.");
                //tablaTrk.ajax.reload(null, false);
            },
            error: function(data) {
                alert('error');
            }
        });
    }*/

    //Agregar IDTruck a tabla de Lotes
    /*$(document).on("click", ".btnAdd", function(){
        addLot();
        update();
        /*opcion = 7;
        id = parseInt($.trim($('#Lot').val()));
        TrkID = parseInt(TrkID);
        //alert (id);
        //alert (TrkID);
        //DO = $.trim($('#DO').val());
        $.ajax({
              url: "bd/crudTrk.php",
              type: "POST",
              datatype:"json",
              data:  {id:id, TrkID:TrkID , opcion:opcion},    
              success: function(data){
                tablaTrk.ajax.reload(null, false);
                alert ("Actualizado con exito.");
                //tablaUsuarios.ajax.reload(null, false);
              },
            error: function(data) {
                alert('error');
            }
        });
    });*/
    
    $('#btnLimpiar').on('click', function(){
        tablaTrk
        .search( '' )
        .columns().search( '' )
        .draw();
        //tablaTrk
        //    .search("").draw();
        //$("#tablaTrk").DataTable().search('').draw();
        //$('#tablaTrk tfoot th').find('input[type=search]').val('');
        //$("#tablaTrk").DataTable().search('').draw();
    });


// botón filtros

$("#filtraboton").click(function(){
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    DepArrDate = $('#DepArrDate').val();
    if(fromdate || todate || DepArrDate){
        $('#fromdate').attr('disabled', false);
        $('#todate').attr('disabled', false);
        $('#timeSelect').attr('disabled', false);
    }else{
        $('#fromdate').attr('disabled', true);
        $('#todate').attr('disabled', true);
        $('#timeSelect').attr('disabled', true);
    }

    $('#filtrarmodal').modal('show');
});
$('#DepArrDate').on('change keyup',function(){
    DepArrDate = $('#DepArrDate').val();


    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    muestras = $('select[name=muestras] option').filter(':selected').val();
    muestratext = $( "#muestras option:selected" ).text();
    DepRegFil = $('select[name=DepRegFil] option').filter(':selected').val();
    ArrRegFil = $('select[name=ArrRegFil] option').filter(':selected').val();
    timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
    idGlobal = $('#idDO').val();
    asLots = $('#asLots').val();
    if(DepArrDate){
        $('#fromdate').attr('disabled', false);
        $('#todate').attr('disabled', false);
        $('#timeSelect').attr('disabled', false);
        $('#muestras').attr('disabled',true);
        if(fromdate || todate)
            $('#buscafiltro').attr('disabled',false);
        else
            $('#buscafiltro').attr('disabled',true);
    }else{
        $('#fromdate').attr('disabled', true);
        $('#todate').attr('disabled', true);
        $('#timeSelect').attr('disabled', true);
        if(idGlobal || asLots || DepRegFil || ArrRegFil)
            $('#muestras').attr('disabled',true);
        else    
            $('#muestras').attr('disabled',false);
        $('#fromdate').val("");
        $('#todate').val("");
        if(muestratext != "200 (default)" || DepRegFil || ArrRegFil ||idGlobal || asLots ){
            $('#buscafiltro').attr('disabled',false);
        }else
            $('#buscafiltro').attr('disabled',true);
        $('#timeSelect').prop('selectedIndex',0);
    }
});

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

    $('#fromdate, #todate').on('change keyup', function () {
        DepArrDate = $('#DepArrDate').val();


        fromdate = $('#fromdate').val();
        todate = $('#todate').val();
        muestras = $('select[name=muestras] option').filter(':selected').val();
        muestratext = $( "#muestras option:selected" ).text();
        DepRegFil = $('select[name=DepRegFil] option').filter(':selected').val();
        ArrRegFil = $('select[name=ArrRegFil] option').filter(':selected').val();
        timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
        idGlobal = $('#idDO').val();
        asLots = $('#asLots').val();

        if(fromdate || todate){
            $('#buscafiltro').attr('disabled', false);
        }else if(DepArrDate){
            $('#timeSelect').prop('selectedIndex',0);
            $('#buscafiltro').attr('disabled', true);
        }else if(muestratext != "200 (default)" || DepRegFil || ArrRegFil ||idGlobal || asLots){
            $('#buscafiltro').attr('disabled', false);
        } else $('#buscafiltro').attr('disabled', true);
    });

$('#timeSelect').on('change', function () {
    timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
    if(timeSelect){
    timeSelect = parseInt(timeSelect);
    let today = new Date();
    //today = today.toLocaleTimeString('es-MX');
    let dateEnMilisegundos = 1000 * 60* 60* 24* timeSelect;
    let suma = today.getTime() - dateEnMilisegundos;
    let fechainicial = new Date(suma);
    //console.log(today);
    datetoday =  formatDate(today);
    datetosearch = formatDate(fechainicial);
    $('#fromdate').val(datetosearch);
    $('#todate').val(datetoday);
    $('#buscafiltro').attr('disabled',false);
    $
    }else{
        $('#fromdate').val("");
        $('#todate').val("");
        $('#buscafiltro').attr('disabled',true);
    }

    //console.log(datetoday); //today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()
});

// Si cambia el contenido de los input se habilita o bloquea el botón de apply 
$('#muestras,#DepRegFil,#ArrRegFil,#idDO,#asLots').on('change keyup',function(){
    DepArrDate = $('select[name=DepArrDate] option').filter(':selected').val();
    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    muestras = $('select[name=muestras] option').filter(':selected').val();
    muestratext = $( "#muestras option:selected" ).text();
    DepRegFil = $('select[name=DepRegFil] option').filter(':selected').val();
    ArrRegFil = $('select[name=ArrRegFil] option').filter(':selected').val();
    timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
    idGlobal = $('#idDO').val();
    asLots = $('#asLots').val();

    
    if( muestratext != "200 (default)" || DepRegFil || ArrRegFil || idGlobal || asLots){
        if(DepArrDate && fromdate || todate)
             $('#buscafiltro').attr('disabled', false);
        else 
            if(DepArrDate == "")
                $('#buscafiltro').attr('disabled', false);
            else
                $('#buscafiltro').attr('disabled', true);
    }else {
        if(DepArrDate && fromdate || todate)
             $('#buscafiltro').attr('disabled', false);
        else
            $('#buscafiltro').attr('disabled', true);
    }
});


$('#idDO, #asLots').on('change keyup', function(){
    idGlobal = $('#idDO').val();
    asLots = $('#asLots').val();
    if(idGlobal || asLots){
        $('#DepRegFil').attr('disabled', true);
        $('#ArrRegFil').attr('disabled', true);
        $('#muestras').attr('disabled', true);
        $('#muestras').prop('selectedIndex',0);
        $('#DepRegFil').prop('selectedIndex',0);
        $('#ArrRegFil').prop('selectedIndex',0);
    }else{
        $('#DepRegFil').attr('disabled', false);
        $('#ArrRegFil').attr('disabled', false);
        $('#muestras').attr('disabled', false);
    }
});

$('#DepRegFil,#ArrRegFil').on('change keyup', function(){
    DepRegFil = $('#DepRegFil').val();
    ArrRegFil= $('#ArrRegFil').val();
    DepArrDate = $('select[name=DepArrDate] option').filter(':selected').val();
    if(DepRegFil || ArrRegFil){
        $('#idDO').attr('disabled', true);
        $('#asLots').attr('disabled', true);
        $('#muestras').attr('disabled', true);
        $('#muestras').prop('selectedIndex',0);
        $('#idDO').val("");
        $('#asLots').val("");

        if(DepArrDate == ""){
            $('#DepArrDate').prop('selectedIndex',1);
            $('#timeSelect').attr('disabled', false);
            $('#timeSelect').prop('selectedIndex',3);
            timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
            timeSelect = parseInt(timeSelect);
            let today = new Date();
            //today = today.toLocaleTimeString('es-MX');
            let dateEnMilisegundos = 1000 * 60* 60* 24* timeSelect;
            let suma = today.getTime() - dateEnMilisegundos;
            let fechainicial = new Date(suma);
            //console.log(today);
            datetoday =  formatDate(today);
            datetosearch = formatDate(fechainicial);
            $('#fromdate').val(datetosearch);
            $('#fromdate').attr('disabled', false);
            $('#todate').attr('disabled',false);
            $('#todate').val(datetoday);
            $('#buscafiltro').attr('disabled',false);
        }

    }else{
        if(DepArrDate){
            $('#muestras').attr('disabled', true);
        }else
            $('#muestras').attr('disabled', false);
        $('#idDO').attr('disabled', false);
        $('#asLots').attr('disabled', false);
        
    }
});

//Last trucks deshabilita todo
$('#muestras').on('change', function () {
    muestras = $('select[name=muestras] option').filter(':selected').val();
    muestratext = $( "#muestras option:selected" ).text();
    if(muestratext != "200 (default)"){
        $('#idDO').attr('disabled', true);
        $('#asLots').attr('disabled', true);
        $('#DepRegFil').attr('disabled', true);
        $('#ArrRegFil').attr('disabled', true);
        $('#DepArrDate').attr('disabled', true);
        $('#fromdate').attr('disabled', true);
        $('#todate').attr('disabled', true);
        $('#timeSelect').attr('disabled', true);
        $('#fromdate').val("");
        $('#todate').val("");
        $('#timeSelect').val("");
        $('#timeSelect').prop('selectedIndex',0);
        $('#DepArrDate').prop('selectedIndex',0);
        $('#DepRegFil').prop('selectedIndex',0);
        $('#ArrRegFil').prop('selectedIndex',0);
        $('#idDO').val("");
        $('#asLots').val("");
    }else{
        $('#idDO').attr('disabled', false);
        $('#asLots').attr('disabled', false);
        $('#DepRegFil').attr('disabled', false);
        $('#ArrRegFil').attr('disabled', false);
        $('#DepArrDate').attr('disabled', false);
      

    }
});


var botonglobal = 0;
$("#buscafiltro, #borrarFiltro, #cer").click(function(){
    boton = $(this).val();
    if(boton == 1 || boton == 0)
        botonglobal = boton;
    
    if(boton != 3){
  //  muestra = Number.isNaN($('#muestras').val()) ? " " : $('#muestras').val();
    muestras =  $('select[name=muestras] option').filter(':selected').val();
    DepRegFil = $('select[name=DepRegFil] option').filter(':selected').val();
    ArrRegFil = $('select[name=ArrRegFil] option').filter(':selected').val();
    DepArrDate = $('select[name=DepArrDate] option').filter(':selected').val();
    idGlobal = $('#idDO').val();
    
    asLots = $('#asLots').val();
    fromdate = $('#fromdate').val();
    todate = $('#todate').val();
    //filters = {"muestras":muestras,"regionfil":regionfil};
    opcion = 4;
    var applyFilter =  $.ajax({
        type: 'POST',
        url: 'bd/assignFilters.php',
        data: {
            boton: boton,
            idGlobal:idGlobal,
            asLots: asLots,
            muestras: muestras,
            DepRegFil: DepRegFil,
            ArrRegFil: ArrRegFil,
            DepArrDate: DepArrDate,
            fromdate: fromdate,
            todate: todate 
        }
        
    }) 
    applyFilter.done(function(data){

        console.log(data);
        tablaTrk.ajax.reload(function(){

            if(boton == 1){
                $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Filtros asignados con éxito</div>')
                setTimeout(function() {
                    $('#alert1').fadeOut(1000);
                    $('#filtrarmodal').modal('hide');
                },1000);
            }
            else{
                // Habilitar botones que pueden estar deshabilitados
                $('#DepRegFil').attr('disabled', false);
                $('#ArrRegFil').attr('disabled', false);
                $('#muestras').attr('disabled', false);
                $('#asLots').attr('disabled', false);
                $('#idDO').attr('disabled', false);
                $('#DepArrDate').attr('disabled', false);
                $('#timeSelect').attr('disabled', true);
                $('#fromdate').attr('disabled', true);
                $('#todate').attr('disabled', true);
    
                $('#muestras').prop('selectedIndex',0);
                $('#DepRegFil').prop('selectedIndex',0);
                $('#ArrRegFil').prop('selectedIndex',0);
                $('#DepArrDate').prop('selectedIndex',0);
                $('#timeSelect').prop('selectedIndex',0);
                $('#idDO').val("");
                $('#fromdate').val("");
                $('#todate').val("");
                $('#asLots').val("");
                $('#buscafiltro').prop('disabled', true);
                $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"> <i class="bi bi-trash-fill"></i> Filtros borrados con éxito</div>')
                setTimeout(function() {
                    $('#alert1').fadeOut(1500);
                },1500);
            }

        });
        $('#aviso').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');      
    });  
}else{
    if(botonglobal == 0){
        $('#DepRegFil').attr('disabled', false);
        $('#ArrRegFil').attr('disabled', false);
        $('#muestras').attr('disabled', false);
        $('#asLots').attr('disabled', false);
        $('#idDO').attr('disabled', false);
        $('#DepArrDate').attr('disabled', false);
        $('#timeSelect').attr('disabled', true);
        $('#fromdate').attr('disabled', true);
        $('#todate').attr('disabled', true);

        $('#muestras').prop('selectedIndex',0);
        $('#DepRegFil').prop('selectedIndex',0);
        $('#ArrRegFil').prop('selectedIndex',0);
        $('#DepArrDate').prop('selectedIndex',0);
        $('#timeSelect').prop('selectedIndex',0);
        $('#idDO').val("");
        $('#fromdate').val("");
        $('#todate').val("");
        $('#asLots').val("");
        $('#buscafiltro').prop('disabled', true);
    }
}
});

    });
    
//MODAL PARA REPORTE INVOICE
$(document).ready(function(){
    var filterDOM=0;
    var filterEXP=0;
    $("#reporteinvoice").click(function(){
        $("#forminvoices")[0].reset();
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Report Invoices");        
        $('#modalinvoices').modal("show");
        document.getElementById('filtros_invoice').style.display = 'block';
        document.getElementById('subir_reporteInv').style.display = 'none';
        //document.getElementById('limpiar_transits').style.display = 'block';
        document.getElementById('generar_invoices').style.display = 'block';
        document.getElementById('btnloadfile_invoice').style.display = 'none';
        $("#filterDOM").prop("checked", true);
        $("#filterEXP").prop("checked", true);

        
    });




    //vista para descargar reporte
    $('#btnreport_invoice').click(function(e){
        e.preventDefault();
        document.getElementById('filtros_invoice').style.display = 'block';
        document.getElementById('subir_reporteInv').style.display = 'none';
        //document.getElementById('limpiar_transits').style.display = 'block';
        document.getElementById('generar_invoices').style.display = 'block';
        document.getElementById('btnloadfile_invoice').style.display = 'none';
     
        
        });


    //vista para subir el csv de los trucks

    $('#btnsube_invoice').click(function(e){
        e.preventDefault();
        document.getElementById('filtros_invoice').style.display = 'none';
        document.getElementById('subir_reporteInv').style.display = 'block';
        //document.getElementById('limpiar_transits').style.display = 'none';
        document.getElementById('generar_invoices').style.display = 'none';
        document.getElementById('btnloadfile_invoice').style.display = 'block';  
        var $el = $('#filecsv_invoice');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();       
        $('#btnloadfile_invoice').prop('disabled', true);   
        //$("#modalreporte").html("Load CSV");
        //$('#modalreporte').text($(".modal-title").attr('title'));
        

        //Hacer la validacion del archivo seleccionado 
        $("#filecsv_invoice").change(function(){           
            valida_csv_invoice();
        });
    });

    $('#generar_invoices').click(function(e){
        e.preventDefault();
        if($('#filterDOM').prop('checked')){
            filterDOM = 1;

        }
        else{
            filterDOM = 0;
        }

        if($('#filterEXP').prop('checked')){
            filterEXP = 1;
        }

        else{
            filterEXP = 0;
        }

       // console.log("valor filterDOM:"+ filterDOM);
        //console.log("valor filterEXP:"+ filterEXP);
        if(filterDOM!=0 || filterEXP !=0){
         window.open("./bd/invoices.php?filterDOM="+filterDOM+"&filterEXP="+filterEXP);   
        }
        else{
            alert("you must check one of the following options.")
        }
         
    });



    $('#btnloadfile_invoice').on('click', function(e) {
        e.preventDefault();    
        var file_data = $('#filecsv_invoice').prop('files')[0];  
        var form_data = new FormData();             
        form_data.append('file', file_data);   

        $.ajax({
            url: 'bd/loadinvoice.php', // <-- point to server-side PHP script 
            dataType: 'text',  // <-- what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(data){
               // datos = JSON.parse(data);
               // cont = datos['cont'];
                //cont = data['cont'];

                alert("Successful file upload" + '\n' +"Modified Trucks: "+ data);
                var $el = $('#filecsv_invoice');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();  
                $('#btnloadfile_invoice').prop('disabled', true);      
                tablaTrk.ajax.reload(null,false);     
            },
            error(data){
                alert("Error to load file");
                var $el = $('#filecsv_invoice');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();  
                $('#btnloadfile_invoice').prop('disabled', true);    
            }

        });
    });
    

});

      //REPORTE DE PESOS
$(document).ready(function(){
     $("#pesosTrk").click(function(e){
        e.preventDefault();
        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $('#reportepesos').modal('show');
        $("#form_pesos")[0].reset();  
        $('#cliente_peso').prop('disabled', true);

        rango_fechas_peso(30) 


        $('#timeSelect_pesos').change(function () {
          //  timeSelect = $("#timeSelect_report").val();
            rango_fechas_peso($(this).val())               
            
        });


        $('#regllegada_pesos').change(function () {
            if($(this).val() == "CLIENTE"){
                $('#cliente_peso').prop('disabled', false);
            }
            else{
                $('#cliente_peso').prop('disabled', true);
            }
        });


        $("#generar_report_peso").click(function(e){ 
                e.preventDefault();
                salidareg= $('#regsalida_pesos').val();
                llegadareg=$('#regllegada_pesos').val();
                cliente= $('#cliente_peso').val();
                fromdate_pesos= $('#fromdate_pesos').val();
                todate_pesos= $('#todate_pesos').val();    
                origen= $('#gin_peso').val();               
                window.open("./bd/weightreport.php?salidareg="+salidareg+"&llegadareg="+llegadareg+"&fromdate_pesos="+fromdate_pesos+"&todate_pesos="+todate_pesos+"&cliente="+cliente+"&origen="+origen);
        });

        $("#limpiar_report_peso").click(function(e){ 
            $("#form_pesos")[0].reset(); 
            $('#cliente_peso').prop('disabled', true); 
            rango_fechas_peso(30) 
           
         });

        
     });
});
     


//----------------DESPLEGAR MODAL REPORTE DE MANIOBRAS------------
$(document).ready(function(){
    $("#btnreporte_maniobras").click(function(e){ 
        User = $.trim($('#dropdownMenuLink2').val());
        e.preventDefault();
        $("#repman").trigger("reset");
        var date=new Date();  
        date.setDate(date.getDate());
        var day=String(date.getDate()).padStart(2, '0');          
        var month=("0" + (date.getMonth() + 1)).slice(-2); 
        var year=date.getFullYear();  
        fechadef=year+"-"+month+"-"+day 

        var date2=new Date();  
        date2.setDate(date2.getDate() - 7);
        var day2=String(date2.getDate()).padStart(2, '0');          
        var month2=("0" + (date2.getMonth() + 1)).slice(-2); 
        var year2=date2.getFullYear();  
        fechadef2=year2+"-"+month2+"-"+day2 ;

        $('#fromdate_man').val(fechadef2)
        $('#todate_man').val(fechadef)

        $('#timeselect_man').on('change',function(){
            rango_fechas_maniobras($(this).val(),fechadef2,fechadef)
        });

        
        $('#fromdate_man').on('change',function(){
            if ($(this).val()== $('#todate_man').val()){
                $('#reporte_man_PDF').prop('disabled', false);
            }
            else{
                $('#reporte_man_PDF').prop('disabled', true);
            }
        });

        $('#todate_man').on('change',function(){
            if ($(this).val()== $('#fromdate_man').val()){
                $('#reporte_man_PDF').prop('disabled', false);
            }
            else{
                $('#reporte_man_PDF').prop('disabled', true);
            }
        });


       

        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $(".modal-title").text("Reporte Maniobras");		
        $('#reporte_maniobras').modal('show'); 	
        
        //Mostrar PDF a personas que no seas del SS        
        if(User=="MANOLO PEÑA"){
            document.getElementById('reporte_man_PDF').style.display = 'none'; 
            document.getElementById('fechas_reporte_maniobras').style.display = 'none'; 
        }
        else{
            document.getElementById('reporte_man_PDF').style.display = 'block'; 
            document.getElementById('fechas_reporte_maniobras').style.display = 'block';   
             
        }


        //funcion botón limpiar
        $("#limpiar_man").click(function(e){ 
            e.preventDefault();
            $("#repman").trigger("reset");
            $('#fromdate_man').val(fechadef2)
            $('#todate_man').val(fechadef)
    
        })

        //FUNCION BOTON GENERAR EXCEL
        $("#reporte_man_excel").click(function(e){ 
            e.preventDefault();
            fromdate_man=$('#fromdate_man').val();
            todate_man=$('#todate_man').val();
            region_man =$('#region_man').val();
            console.log(region_man);

        
            //GENERAR REPORTE DE MANOLO 
            if(User=="MANOLO PEÑA"){
                window.open("./bd/ManiobrasExcel_SS.php");   
            }

            else{
                if(region_man=="PUEBLA"){
                window.open("./bd/ManiobrasExcel.php?fechainicio="+fromdate_man+"&fechafin="+todate_man+"&region="+region_man);   
                }
                else {
                    window.open("./bd/ManiobrasExcelGomez.php?fechainicio="+fromdate_man+"&fechafin="+todate_man+"&region="+region_man);   
                }
            }
        })


         //FUNCION BOTON GENERAR PDF
         $("#reporte_man_PDF").click(function(e){ 
            e.preventDefault();
            fromdate_man=$('#fromdate_man').val();
            todate_man=$('#todate_man').val();
            region_man =$('#region_man').val();
            if(region_man=="PUEBLA"){
                window.open("./bd/ManiobrasPDF.php?fechainicio="+fromdate_man+"&fechafin="+todate_man+"&region="+region_man);   
            }
            else{
                window.open("./bd/ManiobrasPDFGomez.php?fechainicio="+fromdate_man+"&fechafin="+todate_man+"&region="+region_man);   
            }
        })

         //boton para mostrar cargar de csv

         //botón para mostrar filtros de reporte 

         $("#btnreport_maniobras").click(function(e){ 
            e.preventDefault();        
            
            
            document.getElementById('limpiar_man').style.display = 'block';
            document.getElementById('reporte_man_excel').style.display = 'block';
            document.getElementById('reporte_man_PDF').style.display = 'block';
            document.getElementById('filtros_maniobras').style.display = 'block';
           // document.getElementById('btn_reportman').style.display = 'block';
            document.getElementById('carga_csv_man').style.display = 'none';   
            document.getElementById('subir_reporteManiobras').style.display = 'none';   
            
            
            if(User=="MANOLO PEÑA"){
                document.getElementById('reporte_man_PDF').style.display = 'none'; 
              
            }
     
        })

        $("#btnsube_maniobras").click(function(e){ 
            e.preventDefault();
            document.getElementById('filtros_maniobras').style.display = 'none';
           // document.getElementById('btn_reportman').style.display = 'none';
           document.getElementById('limpiar_man').style.display = 'none';
           document.getElementById('reporte_man_excel').style.display = 'none';
           document.getElementById('reporte_man_PDF').style.display = 'none';
            document.getElementById('carga_csv_man').style.display = 'block';   
            document.getElementById('subir_reporteManiobras').style.display = 'block';

            if(User=="MANOLO PEÑA"){
                document.getElementById('reporte_man_PDF').style.display = 'none'; 
              
            }

        });

        $("#reporte_SS").click(function(e){ 
            e.preventDefault();      
            window.open("./bd/ManiobrasExcel_SS.php"); 
        
        });

        $("#filecsv_maniobras").change(function(){
            valida_csv_maniobras();
        });


        //boton load csv maniobras

        
        

        
    });

    $('#carga_csv_man').on('click', function(e) {
    
        e.preventDefault();    
        var file_data = $('#filecsv_maniobras').prop('files')[0];  
        var form_data = new FormData();             
        form_data.append('file', file_data);   
        $('#carga_csv_man').prop('disabled', true); 

        //definir el archivo de carga dependiendo el usuario

        if(User=="MANOLO PEÑA"){
            Ruta='bd/load_csv_maniobras_SS.php';
        }
        else{
            Ruta='bd/load_csv_maniobras.php';
        }

        $.ajax({
            url: Ruta, // <-- point to server-side PHP script 
            dataType: 'text',  // <-- what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(data){
               // datos = JSON.parse(data);
               // cont = datos['cont'];
                //cont = data['cont'];

                alert("Successful file upload" + '\n' +"Modified Trucks: "+ data);
                var $el = $('#filecsv_maniobras');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();  
                $('#carga_csv_man').prop('disabled', true);      
                //tablaTrk.ajax.reload(null,false);     
            },
            error(data){
                alert("Error to load file");
                var $el = $('#filecsv_maniobras');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();  
                $('#carga_csv_man').prop('disabled', true);    
            }

        });
    });

    
})

function valida_csv_invoice(){
    
    var filename = $("#filecsv_invoice").val();
  //  console.log(filename);
    //si es null muestra un mensaje de error
    if(filename == null)
        alert('You have not selected a file');
        
    else{// si se eligio un archivo correcto obtiene la extension para validarla
        var extension = filename.replace(/^.*\./, '');               
        
        //$('#update').prop('disabled', false);
        if (extension == filename)
            extension = '';
        else{
            extension = extension.toLowerCase();
          
            //aqui puedes incluir todas las extensiones que quieres permitir
            if(extension == 'CSV' || extension == 'csv' || extension == 'Csv' ){
                $('#btnloadfile_invoice').prop('disabled', false);
            }
            else{
                alert("The selected file is not a CSV file");
                var $el = $('#filecsv_invoice');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();  
                $('#btnloadfile_invoice').prop('disabled', true);    
               // $('#load').prop('disabled', true);
                
            }
        }
    }


}

function valida_csv_maniobras(){
    
    var filename = $("#filecsv_maniobras").val();
  //  console.log(filename);
    //si es null muestra un mensaje de error
    if(filename == null)
        alert('You have not selected a file');
        
    else{// si se eligio un archivo correcto obtiene la extension para validarla
        var extension = filename.replace(/^.*\./, '');               
        
        //$('#update').prop('disabled', false);
        if (extension == filename)
            extension = '';
        else{
            extension = extension.toLowerCase();
          
            //aqui puedes incluir todas las extensiones que quieres permitir
            if(extension == 'CSV' || extension == 'csv' || extension == 'Csv' ){
                $('#carga_csv_man').prop('disabled', false);
            }
            else{
                alert("The selected file is not a CSV file");
                var $el = $('#filecsv_maniobras');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();  
                $('#carga_csv_man').prop('disabled', true);    
               // $('#load').prop('disabled', true);
                
            }
        }
    }


}


function rango_fechas_maniobras(timeSelect,fechadef2,fechadef){
    //timeSelect = $("#timeSelect_report").val();
    if(timeSelect){
    timeSelect = parseInt(timeSelect);
    var today = new Date();
    let time = today.getTime();
    $('#fromdate_man').prop('disabled', false);
    $('#todate_man').prop('disabled', false);
    
    //today = today.toLocaleTimeString('es-MX');
    let dateEnMilisegundos = 1000 * 60* 60* 24* timeSelect;
    let suma = time - dateEnMilisegundos;
    let fechainicial = new Date(suma);
    //console.log(today);
    datetoday =  formatDate(today);
    datetosearch = formatDate(fechainicial);
    $('#fromdate_man').val(datetosearch);
    $('#todate_man').val(datetoday);
    //$('#buscafiltro').attr('disabled',false);

   // $('#generar').attr('disabled', false);
   
    }else{
        $('#fromdate_man').val(fechadef2);
        $('#todate_man').val(fechadef);
       // $('#fromdate_report').prop('disabled', true);
       // $('#todate_report').prop('disabled', true);
       // $('#buscafiltro').attr('disabled',true);
       // $('#generar').attr('disabled', true);
    }
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}


function rango_fechas_peso(timeS){

    time_Select = parseInt(timeS);
    var today = new Date();
    let time = today.getTime();

    let dateEnMilisegundos = 1000 * 60* 60* 24* time_Select;
    let suma = time - dateEnMilisegundos;
    let fechainicial = new Date(suma);
    //console.log(today);
    datetoday =  formatDate(today);
    datetosearch = formatDate(fechainicial);
    $('#fromdate_pesos').val(datetosearch);
    $('#todate_pesos').val(datetoday);

    
}
