<?php
session_start();
if (!isset($_SESSION['username'])):
    include_once('index.php');
else:
$Typ_Rut = $_SESSION['Typ_Rut'];
$permiso = $_SESSION['Admin'];
?>

<!doctype html>
<html lang="es en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="img/ecom.png" />
    <title>Routes</title>

    <!-- CSS bootstrap -->
    <link rel="stylesheet" href="../portalLogistica/assets/bootstrap/css/bootstrap.min.css">
    <!--datables CSS básico-->
    <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
    <!--datables estilo bootstrap 4 CSS-->
    <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

    <!-- CSS personalizado -->
    <link rel="stylesheet" href="../portalLogistica/main.css">
    <!--Google fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


    <!--font awesome con CDN  -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
</head>

<body>
    <!-- Scripts -->

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>


    <!-- jQuery, Popper.js, Bootstrap JS -->
    <script src="assets/jquery/jquery-3.3.1.min.js"></script>
    <script src="assets/popper/popper.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>

    <!-- librerias necesarias para finalizar sesion por inactividad -->
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <!-- Scrip para finalizar sesion por inactividad -->
    <script type="text/javascript" src="timer.js"></script>    


    

    <!-- datatables JS -->
    <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>

    <!-- para usar botones en datatables JS -->
    <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
    <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
    <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
    <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="mainRoutes.js"></script>

    <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
    <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
        <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
            <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                    </svg>
                </a>

                <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                    <li><a class="dropdown-item" href="trucks.php">Trucks</a></li>
                    <li><a class="dropdown-item" href="lots.php">Lots</a></li>
                    <?php if ($_SESSION['username'] == 'ISELA LARA' || $_SESSION['username'] == 'DARIO ROMERO' || $_SESSION['username'] == 'CESAR ARMAS' || $_SESSION['username'] == 'LUIS ANGEL RENDON TARIN'){?>
                        <li><a class="dropdown-item" href="bales.php">Bales</a></li>
                        <?php } ?>
                    <li><a class="dropdown-item" href="do.php">Delivery Order</a></li>
                    <li><a class="dropdown-item" href="adjustments.php">Adjustments</a></li>
                    <li><a class="dropdown-item" href="inv.php">Invoice</a></li>
                    <li><a class="dropdown-item" href="export.php">Containers</a></li>
                    <li><a class="dropdown-item" href="clients.php">Clients</a></li>
                    <li><a class="dropdown-item" style="background-color: #5a926d;" href="#">Routes</a></li>
                    <li><a class="dropdown-item" href="requisition.php">Requisition</a></li>
                    <li><a class="dropdown-item" href="CMS.php">CMS</a></li>
                    <?php if ($permiso == 1){?>
                        <li><a class="dropdown-item" href="adminusers.php">User Management</a></li>
                    <?php } ?>
                </ul>
            </div>
            <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-2" href="main.php">
                <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
            </a>
            <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                <p>/ Routes</p>
            </div>
            <div class="container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                <!-- Botones -->
                <?php if ($Typ_Rut == 1) : ?>
                    <button id="btnCar" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="New Transport"><i class="bi bi-truck"></i></button>
                    <button id="btnRuta" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="New Route"><i class="bi bi-map-fill"></i></button>
                <?php endif ?>
            </div>
            <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"><?php echo $_SESSION['username']; ?></button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                    <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Esto es el Nav bar, todo contenido en un container-fluid -->


    <!-- Aquí inicia todo código de tablas etc -->

    <!-- Inicio de la tabla -->
    <div class="card card-body" style="opacity:100%;">
        <div class="table-responsive" style="opacity:100%;">
            <table id="tablaRoutes" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;">
                <thead style="background-color: #65ac7c;" style="opacity:100%;">
                    <tr>
                        <th class="th-sm">Route ID</th>
                        <th class="th-sm">Departure Region</th>
                        <th class="th-sm">Transport Company</th>
                        <th class="th-sm">Bussiness Name</th>
                        <!--<th class="no-exportar">Edit</th>-->
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <th class="th-sm">Route ID</th>
                        <th class="th-sm">Departure Region</th>
                        <th class="th-sm">Transport Company</th>
                        <th class="th-sm">Bussiness Name</th>
                        <!--<th class="th-sm">Edit</th>-->
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!--Modal para CRUD-->
    <div class="modal fade" id="modalCRUD" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <form id="formExp">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="" class="col-form-label">Date:</label>
                                    <input type="date" class="form-control" id="Dat" required>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="" class="col-form-label">Delivery Order:</label>
                                    <input type="text" class="form-control" style="text-transform:uppercase;" id="DOrd" required>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="" class="col-form-label">Lot:</label>
                                    <input type="text" class="form-control" style="text-transform:uppercase;" id="Lot">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="" class="col-form-label">Requester:</label>
                                    <input type="text" class="form-control" style="text-transform:uppercase;" id="Requester">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="" class="col-form-label">Quantity:</label>
                                    <input type="text" class="form-control" id="Qty" pattern="[0-9]+">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="" class="col-form-label">Type:</label>
                                    <select class="form-control" id="Typ" name="Typ">
                                        <option value="ADJUSTMENT">ADJUSTMENT</option>
                                        <option value="REJECTED">REJECTED</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="" class="col-form-label">Comments:</label>
                                    <input type="text" class="form-control" style="text-transform:uppercase;" id="Cmt">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="" class="col-form-label">Full Lot:</label>
                                    <select class="form-control" id="FullTyp" name="FullTyp">
                                        <option value="YES">YES</option>
                                        <option value="NO">NO</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="" class="col-form-label">DO Dev:</label>
                                    <input type="text" class="form-control" style="text-transform:uppercase;" id="DODev">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" id="btnGuardar" class="btn btn-dark">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!--Modal Transports-->
    <div class="modal fade" id="modalTransports" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <form id="formTransports">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="" class="col-form-label">Name<font size=2>*</font></label>
                                    <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="TptCo" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label fro="" class="col-form-label">Bussiness Name<font size=2>*</font></label>
                                    <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="BnName" required>
                                </div>
                            </div>
                        </div>
                        <font size=2>*Required Fields</font>
                    </div>
                    <div class="modal-footer">
                    <div id="aviso"></div>
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" id="btnGuardarFile" class="btn btn-dark">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--Modal Routes-->
    <div class="modal fade" id="modalRoutes" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <form id="formRoutes">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="" class="col-form-label">Transport<font size=2>*</font></label>
                                    <select class="form-control form-control-sm" style="text-transform:uppercase;" id="TptCoR" required>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label fro="" class="col-form-label">Departure Region<font size=2>*</font></label>
                                    <select class="form-control form-control-sm" style="text-transform:uppercase;" id="OutReg" required>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="strike">
                            <div class='col-lg-12'>
                                <!--<div class='form-group'>-->
                                <center>
                                    <dd><button type="button" id="btnAddRte" class="btn btn-sm btn-dark">Add Route</button></dd>
                                </center>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label fro="" class="col-form-label">Recorded Departures</label>
                                    <select class="form-control form-control-sm" name="Salidas[]" multiple="multiple" id="Salidas" size="8"></select>
                                </div>
                            </div>
                        </div>
                        <font size=2>*Required Fields</font>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" id="btnGuardarFile" class="btn btn-dark">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>

</html>

<?php 
    endif;
?>
