$(document).ready(function() {

//funcion para sumar
jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
    return this.flatten().reduce( function ( a, b ) {
      if ( typeof a === 'string' ) {
        a = a.replace(/[^\d.-]/g, '') * 1;
      }
      if ( typeof b === 'string' ) {
        b = b.replace(/[^\d.-]/g, '') * 1;
      }
      return a + b;
    }, 0);
});
    
$('#tablaUsuarios tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
});
    
var LotID, opcion;
opcion = 4;
    
tablaUsuarios = $('#tablaUsuarios').DataTable({
    drawCallback: function () {
        var api = this.api();

        var total = api.column( 3, {"filter":"applied"}).data().sum();
        var numlotes = api.rows({"filter":"applied"}).count();
        var pesototal = api.column( 11, {"filter":"applied"}).data().sum();
        $('#monto').html(total);
        $('#totallotes').html(numlotes);
       // $("#totallotes").val(numlotes);
        $('#totalpeso').html(pesototal);
    },
        
    //para usar los botones
    responsive: "true",
    dom: 'Brtilp',       
    buttons:[ 
        {
            extend:    'excelHtml5',
            text:      '<i class="fas fa-file-excel"></i> ',
            titleAttr: 'Export to Excel',
            className: 'btn btn-success',
            exportOptions: {
                columns: ":not(.no-exportar)"
            }
        },
        {
            extend:    'pdfHtml5',
            text:      '<i class="fas fa-file-pdf"></i> ',
            titleAttr: 'Export to PDF',
            className: 'btn btn-danger',
            /*customize: function (doc) {
                doc.content[1].table.widths = 
                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            },*/
            exportOptions: {
                columns: ":not(.no-exportar)"
            }
        },
        {
            text: '<i class="bi bi-funnel-fill"></i>',
            titleAttr: 'Filtrar por',
            orientation: 'landscape',
            className: 'btn btn-info btn-filtrar',
            attr:{
                    id:'filtraboton',
                    "data-toggle": 'modal tooltip',
                    "disabled": false
            }
        },
        /*{
            extend:    'print',
            text:      '<i class="fa fa-print"></i> ',
            titleAttr: 'Imprimir',
            className: 'btn btn-info'
        },*/
    ],
    
    initComplete: function () {
        // Apply the search
        this.api().columns().every( function () {
            var that = this;

            $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value, true, false ).draw();
                        //.draw();
                }
            });
        });
    }, 
    "order": [ 0, 'desc' ],
    "scrollX": true,
    "scrollY": "50vh",
    "scrollCollapse": true,
    "lengthMenu": [100,200,300,500],
    fixedColumns:   {
        left: 0,
        right: 1
    },
    "ajax":{            
        "url": "bd/crud.php", 
        "method": 'POST', //usamos el metodo POST
        "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
        "dataSrc":""
    },
    "columns":[
        {"data": "LotID"},
        {"data": "Crop"},
        {"data": "Lot"},
        {"data": "Qty"},
        {"data": "RegName"},
        {"data": "GinName"},
        {"data": "LocName"},
        {"data": "DOrd"},
        {"data": "SchDate"},
        {"data": "InReg"},
        {"data": "CliDO"},
        {"data": "LiqWgh"},
        {"data": "Qlty"},
        {"data": "Cmt"},
        {"data": "Recap"},
        {"data": "CliID"},
        {"data": "AsgmDate"},
	    {"data": "TrkID"},
        {"data": "Status"},
        {"data": "Cert"},
        {"data": "TotalQty"},        
        {"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button><button class='btn btn-info btn-sm btnTrk'><i class='material-icons'>local_shipping</i></button><button class='btn btn-danger btn-sm btnBorrar'><i class='material-icons'>delete</i></button></div></div>"}
    ],
    columnDefs : [
        { targets : [19],
            render : function (data, type, row) {
              return data == '1' ? 'YES' : 'NO'
            }
        }
    ]
});     

    //<button class='btn btn-danger btn-sm btnReturn'><i class='material-icons'>assignment_return</i></button> crear buscadores 
/*$('#tablaUsuarios thead tr').clone(true).appendTo('#tablaUsuarios thead');
$('#tablaUsuarios thead tr:eq(1) th').each(function(i) {
    var title = $(this).text();
    $(this).html('<input type="text" placeholder="Search" />');
    
    $('input', this).on('keyup change', function(){
        if (tablaUsuarios.column(i).search() !== this.value){
            tablaUsuarios
                .column(i)
                .search(this.value)
                .draw();
        }
    });
});*/
    
    
$('.dataTables_length').addClass('bs-select');

var fila; //captura la fila, para editar o eliminar
//submit para el Alta y Actualización
$('#formUsuarios').submit(function(e){
    /*var comboReg = document.getElementById("Reg");
    var selectedReg = comboReg.options[comboReg.selectedIndex].text;
    var comboGin = document.getElementById("GinID");
    var selectedGin = comboGin.options[comboGin.selectedIndex].text;
    var comboLoc = document.getElementById("Loc");
    var selectedLoc = comboLoc.options[comboLoc.selectedIndex].text;*/
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
    Lot = $.trim($('#Lot').val().toUpperCase());    
    Qty = $.trim($('#Qty').val());
    Reg = $.trim($('#Reg').val());
    GinID = $.trim($('#GinID').val());
    Loc = $.trim($('#Loc').val());
    LiqWgh = $.trim($('#LiqWgh').val());    
    Qlty = $.trim($('#Qlty').val().toUpperCase());
    Cmt = $.trim($('#Cmt').val().toUpperCase());
    Recap = $.trim($('#Recap').val().toUpperCase());
    Cli = $.trim($('#Cli').val());
    AsgmDate = $.trim($('#AsgmDate').val());
    SchDate = $.trim($('#SchDate').val());
    DOrd = $.trim($('#DOrd').val());
    TrkID = $.trim($('#TrkID').val());    
    Status = $.trim($('#Status').val());
    Crop = $.trim($('#Crop').val());
    TotalQty = $.trim($('#cantidadtotal').val());

    if( $('#pacascertificadas').prop('checked') ) {
        pacascert = 1;
    }  else {
        pacascert = 0;
    }





        $.ajax({
          url: "bd/crud.php",
          type: "POST",
          datatype:"json",    
          data: {Crop:Crop, LotID:LotID, Lot:Lot, Qty:Qty, Reg:Reg, GinID:GinID, Loc:Loc, LiqWgh:LiqWgh, Qlty:Qlty, Cmt:Cmt, Recap:Recap, Cli:Cli, AsgmDate:AsgmDate, DOrd:DOrd, SchDate:SchDate, TrkID:TrkID, Status:Status,pacascert:pacascert,TotalQty:TotalQty,opcion:opcion},    
          success: function(data) {
            tablaUsuarios.ajax.reload(null, false);
           }
        });			        
    $('#modalCRUD').modal('hide');											     			
});


//ASIGNAR LA REGION DEL NOMBRE DEL ARCHVIO 
$(document).ready(function(){
    $('#csv').on('change',function(){
        
        Loc = $.trim($('#csv').val());
        if (Loc != ""){
            var res = Loc.split('-')[1];
            var res2 = res.replace(".csv","");
            $('#LocLot').val(res2);
            $('#aviso').html('');
        }
        
    });
    
});


//submit para Carga masiva
$('#formFile').submit(function(e){
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
    var comprobar = $('#csv').val().length;
    var extension = $('#csv').val().split(".").pop().toLowerCase();
    LocLot =  $('#LocLot').val();
    //validar extension
    /*if($.inArray(extension, ['csv']) == -1)
	{
		alert("Invalid File");
		retornarError = true;
		$('#csv').val("");
	}else{*/
    if (comprobar>0){
        var formulario = $("#formFile");
        var archivos = new FormData();
        var url = "bd/procesar2.php";
            for (var i=0; i<(formulario.find('input[type=file]').length); i++){
                archivos.append((formulario.find('input[type="file"]:eq('+i+')').attr("name")),((formulario.find('input[type="file"]:eq('+i+')'))[0].files[0]));
            }
    archivos.append('LocLot',LocLot);
    $.ajax({
        url: "bd/procesar2.php",
        type: "POST",
        contentType: false,
        data:  archivos,
        processData: false,
        success: function(dat) {
                $("#formFile").trigger("reset");
                datos = JSON.parse(dat);
                data = datos['Status'];
                cont = datos['nuevos'];
                upd = datos['update'];
            
            if (data == 'OK'){
                alert("Successful Import");
                //$('#modalFile').modal('hide');
                tablaUsuarios.ajax.reload(null,false);
                $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 24rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i>New records: '+cont+'; Updated records: '+upd+'</div>');
                //location.reload();
                return false;
            } else if (data == 'Columns'){
                $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 24rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i>Check Columns</div>');
              
                return false;
            }else{
                alert("Successful Import"); //alert("Import Error");
                //location.reload();
                tablaUsuarios.ajax.reload(null,false);
                return false;
           }
        }
    });
    return false;    
    }else{
        alert ("Select csv file to import");
        return false;
    }
    //}   	        
});

//para limpiar los campos antes de dar de Alta una Persona
$("#btnNuevo").click(function(){
    $("#InfDO").css("display", "none");
    $("#Reg").empty();
    $("#GinID").empty();
    $("#Loc").empty();
    $("#Cli").empty();
    $('#Reg').append('<option value="" disabled selected >- Select -</option>');
    $('#GinID').append('<option value="" disabled selected >- Select -</option>');
    $('#Loc').append('<option value="" disabled selected >- Select -</option>');
    $('#Cli').append('<option value="" disabled selected >- Select -</option>');
    opcion = 1; //alta
    LotID=null;
    $("#pacascertificadas").prop("checked", false);
    $("#formUsuarios").trigger("reset");
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    $(".modal-title").text("New Lot");
    $('#modalCRUD').modal('show');	    
});
    
//carga masiva
$("#btnFile").click(function(){
    $("#formFile").trigger("reset");
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    $(".modal-title").text("Import Data");
    $('#modalFile').modal('show');  
    $('#aviso').html('');	    
});

//Editar        
$(document).on("click", ".btnEditar", function(){
    $("#InfDO").css("display", "none");
    $("#Reg").empty();
    $("#GinID").empty();
    $("#Loc").empty();
    $("#Cli").empty();
    opcion = 2; //editar
    fila = $(this).closest("tr");	        
    LotID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID		            
    Crop = fila.find('td:eq(1)').text();     //se agrega columna Crop y se recorre la numeracion de las columnas
    Lot = fila.find('td:eq(2)').text();
    Qty = parseFloat(fila.find('td:eq(3)').text());
    DOrd = parseFloat(fila.find('td:eq(7)').text());
    SchDate = fila.find('td:eq(8)').text();
    LiqWgh = parseFloat(fila.find('td:eq(11)').text());
    Qlty = fila.find('td:eq(12)').text();
    Cmt = fila.find('td:eq(13)').text();
    Recap = fila.find('td:eq(14)').text();
    AsgmDate = fila.find('td:eq(15)').text();
    TrkID = parseInt(fila.find('td:eq(17)').text());
    Status = fila.find('td:eq(18)').text();
    certificadas = fila.find('td:eq(19)').text();
    cantidadoriginal = fila.find('td:eq(20)').text();
    $("#LotID").val(LotID);
    $("#Crop").val(Crop);  
    $("#Lot").val(Lot);
    $("#Qty").val(Qty);
    $('#Reg').append('<option class="form-control selected">' + fila.find('td:eq(4)').text() + '</option>');
    $('#GinID').append('<option class="form-control selected">' + fila.find('td:eq(5)').text() + '</option>');
    $('#Loc').append('<option class="form-control selected">' + fila.find('td:eq(6)').text() + '</option>');
    $('#Cli').append('<option class="form-control selected">' + fila.find('td:eq(15)').text() + '</option>');
    $("#LiqWgh").val(LiqWgh);
    $("#Qlty").val(Qlty);
    $("#Cmt").val(Cmt);
    $("#Recap").val(Recap);
    $("#AsgmDate").val(AsgmDate);
    $("#DOrd").val(DOrd);
    $("#SchDate").val(SchDate);
    $("#TrkID").val(TrkID);
    $("#Status").val(Status);
    $("#cantidadtotal").val(cantidadoriginal);
    if (certificadas==='YES'){
        $("#pacascertificadas").prop("checked", true);
    }
    else{
        $("#pacascertificadas").prop("checked", false);
    }

    $(".modal-header").css("background-color", "#17562c");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Edit Lot");		
    $('#modalCRUD').modal('show');

    $("#DOrd").on("change", function(){
        if (TrkID != 0){
            alert ("This lot has an associated truck");
        }else{
            
        }
    });

});


// ----------------------------------add info forma masiva modal----------------------
$("#btnAddInfo").click(function(){
    $("#Lotes").empty();
    $("#LotesSelected").empty();
    $("#InfAdd").trigger("reset");
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    $('#modalAdd').modal('show');
});

$("#btnBuscar").click(function(){
    $("#Lotes").empty();
    $("#LotesSelected").empty();
    Reg = $.trim($('#RegInf').val());
    GinID = $.trim($('#GinInf').val());
    Loc = $.trim($('#LocInf').val());
    if (Reg == "" || Loc == ""){
        $('#avisoInf').html('<div id="alert1" class="alert alert-success" style="width: 20rem; height: 3rem;" role="alert"><i class="bi bi-exclamation-triangle"></i> Select Region and Location</div>')
        setTimeout(function() {
            $('#alert1').fadeOut(1500);
        },1500);
    }else{
        opcion = 13;
        $.ajax({
            url: "bd/crud.php",
            type: "POST",
            datatype:"json",    
            data:  {Reg:Reg, GinID:GinID, Loc:Loc, opcion:opcion},    
            success: function(data) {
                opts = JSON.parse(data);
                for (var i = 0; i< opts.length; i++){
                    if (opts[i].Cli != null){
                        $('#Lotes').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + ' - ' + opts[i].Qty + ' - ' + opts[i].Cli + '</option>  ');
                    }else{
                        $('#Lotes').append('<option value="' + opts[i].LotID + '">' + opts[i].Lot + ' - ' + opts[i].Qty + '</option>  ');
                    }
                }
            }
        });
    }
});


$("#btnAddLote").click(function(){
    Reg = $.trim($('#RegInf').val());
    GinID = $.trim($('#GinInf').val());
    Loc = $.trim($('#LocInf').val());

    Cli = $.trim($('#CliInf').val());
    SchDate = $.trim($('#SchDateInf').val());
    Cmt = $.trim($('#CmtInf').val());
    Lotes = $.trim($('#Lotes').val());

    if (Lotes == ""){
        $('#avisoInf').html('<div id="alert1" class="alert alert-success" style="width: 20rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> No data selected</div>')
        setTimeout(function() {
            $('#alert1').fadeOut(2000);
            $('#InfAdd').modal('hide');
        },1000);
    }else{

        opcion = 14;

        var ApplyCmt = $.ajax({
            url: "bd/crud.php",
            type: "POST",
            datatype:"json",    
            data:  {Reg:Reg, GinID:GinID, Loc:Loc, Cli:Cli, SchDate:SchDate, Cmt:Cmt, Lotes:Lotes, opcion:opcion},    
            success: function(data) {
            }
        })
        ApplyCmt.done(function(data){

            console.log(data);
            tablaUsuarios.ajax.reload(function(){
                    $('#avisoInf').html('<div id="alert1" class="alert alert-success" style="width: 20rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Data assigned successfully</div>')
                    setTimeout(function() {
                        $('#alert1').fadeOut(2000);
                        $('#InfAdd').modal('hide');
                    },1000);
            });
            $('#avisoInf').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');
        });
    }
});

//----------------------------------- fin add info forma masiva modal ------------------------------------------------

//Borrar
$(document).on("click", ".btnBorrar", function(){
    fila = $(this);           
    LotID = $(this).closest('tr').find('td:eq(0)').text();
    Lot = $(this).closest('tr').find('td:eq(2)').text();
    DO = $(this).closest('tr').find('td:eq(7)').text();
    if (DO != 0){
        alert ("This lot "+Lot+" has an associated DO: "+DO);
    }else{
        opcion = 3; //eliminar        
        var respuesta = confirm("Are you sure you want to delete this Lot: "+Lot+"?");                
        if (respuesta) {            
            $.ajax({
              url: "bd/crud.php",
              type: "POST",
              datatype:"json",    
              data:  {opcion:opcion, LotID:LotID},    
              success: function() {
                  tablaUsuarios.ajax.reload(null, false); //tablaInv.row(fila.parents('tr')).remove().draw();                  
               }
            });	
        }
    }
 });
    
//devolucion
/*$(document).on("click", ".btnReturn", function(){
    opcion = 1; //editar
    fila = $(this).closest("tr");	        
    LotID = parseInt(fila.find('td:eq(0)').text()); //capturo el ID		            
    Lot = fila.find('td:eq(1)').text();
    Qty = parseFloat(fila.find('td:eq(2)').text());
    Reg = fila.find('td:eq(3)').text();
    GinID = fila.find('td:eq(4)').text();
    Loc = fila.find('td:eq(5)').text();
    LiqWgh = parseFloat(fila.find('td:eq(6)').text());
    DOrd = 0; //parseFloat(fila.find('td:eq(7)').text());
    TrkID = 0; //parseInt(fila.find('td:eq(8)').text());
    Status = "Rejected"; //fila.find('td:eq(9)').text();
    $("#LotIDDv").val(LotID);
    $("#LotDv").val(Lot);
    $("#QtyDv").val(Qty);
    $('#RegDv').append('<option class="form-control selected">' + fila.find('td:eq(3)').text() + '</option>');
    $('#GinIDDv').append('<option class="form-control selected">' + fila.find('td:eq(4)').text() + '</option>');
    $('#LocDv').append('<option class="form-control selected">' + fila.find('td:eq(5)').text() + '</option>');
    //$("#Reg").append(Reg);
    //$("#GinID").val(GinID);
    //$("#Loc").val(Loc);
    $("#LiqWghDv").val(LiqWgh);
    $("#DOrdDv").val(DOrd);
    $("#TrkIDDv").val(TrkID);
    $("#StatusDv").val(Status);
    $(".modal-header").css("background-color", "#007bff");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Return Lot");		
    $('#modalDev').modal('show');
 });*/

$(document).on("click", ".btnTrk", function(){
    $("#Inf").html("");
    opcion = 5;
    var opts = "";
    fila = $(this).closest("tr");	        
    TrkID = parseInt(fila.find('td:eq(17)').text()); //capturo el ID del camion asociado
    $.ajax({
        url: "bd/crud.php",
        type: "POST",
        datatype:"json",    
        data:  {TrkID:TrkID, opcion:opcion},    
        success: function(data) {
            opts = JSON.parse(data);
            for (var i = 0; i< opts.length; i++){
                htmlData = '<ul><li>Transport Name: '+opts[i].TNam+'</li><li>Driver Name: '+opts[i].DrvNam+'</li><li>Driver Telephone: '+opts[i].DrvTel+'</li><li>Departure Date: '+opts[i].OutDat+'</li><li>Arrival Date: '+opts[i].InDat+'</li><li>Status: '+opts[i].Status+'</li></ul>';
                $('#Inf').html(htmlData);
            } 
        }
    });
    $(".modal-header").css("background-color", "#17562c");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Trucks");		
    $('#modalInf').modal('show');
});
 
 
 //---------------- Inicia funciones botón filtros .-------------------------------------------------------------------------------------------------------

    $("#filtraboton").click(function(){
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        DOL = $('#DOL').val();
        RegL = $('#RegL').val();
        GinL = $('#GinL').val();
        LotL = $('#LotL').val();
        LocL = $('#LocL').val();
        CrpL = $('#CrpL').val();
        CliL = $('#CliL').val();
        //$('#GinL').attr('disabled', true);

        $('#filtrarmodal').modal('show');
        $('#buscafiltro').prop('disabled', true);
    });

    var botonglobal = 0;
    $("#buscafiltro, #borrarFiltro, #cer").click(function(){
        boton = $(this).val();
        if(boton == 1 || boton == 0)
            botonglobal = boton;
        if(boton != 3){
            idGlobal = 0;
            DOL = $('#DOL').val();
            RegL = $('#RegL').val();
            GinL = $('#GinL').val();
            LotL = $('#LotL').val();
            LocL = $('#LocL').val();
            CrpL = $('#CrpL').val();
            CliL = $('#CliL').val();
            //filters = {"muestras":muestras,"regionfil":regionfil};
            opcion = 4;
            var applyFilter =  $.ajax({
                type: 'POST',
                url: 'bd/assignFilters.php',
                data: {
                    boton: boton,
                    idGlobal:idGlobal,
                    DOL: DOL,
                    RegL: RegL,
                    GinL: GinL,
                    LotL: LotL,
                    LocL: LocL,
                    CrpL: CrpL,
                    CliL: CliL
                }
            }) 
            applyFilter.done(function(data){
                console.log(data);
                tablaUsuarios.ajax.reload(function(){
                    if(boton == 1){
                        $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Filtros asignados con éxito</div>')
                        setTimeout(function() {
                            $('#alert1').fadeOut(1000);
                            $('#filtrarmodal').modal('hide');
                        },1000);
                    }
                    else{
                        choicesRegL.enable()
                        $('#GinL').attr('disabled', true);
                        choicesLocL.enable();
                        $('#DOL').attr('disabled', false);
                        $('#LotL').attr('disabled', false);
                        $('#CliL').attr('disabled', false);
                        $('#CrpL').attr('disabled', false);
                        
                        choicesRegL.removeActiveItems();
                        $('#GinL').prop('selectedIndex',0);
                        choicesLocL.removeActiveItems();
                        $('#CliL').prop('selectedIndex',0);
                        $('#CrpL').prop('selectedIndex',0);
                        $('#DOL').val("");
                        $('#LotL').val("");
                        $('#buscafiltro').prop('disabled', true);

                        /*$('#DOL').val("");
                        $('#RegL').val("");
                        $('#GinL').val("");
                        $('#LotL').val("");
                        $('#LocL').val("");
                        $('#CrpL').val("");
                        $('#CliL').val("");*/
                        $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"> <i class="bi bi-trash-fill"></i> Filtros borrados con éxito</div>')
                        setTimeout(function() {
                            $('#alert1').fadeOut(1500);
                        },1500);
                    }

                });

                $('#aviso').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');      
            });  
        }else{
            if(botonglobal == 0){
                choicesRegL.enable()
                $('#GinL').attr('disabled', true);
                choicesLocL.enable();
                $('#DOL').attr('disabled', false);
                $('#LotL').attr('disabled', false);
                $('#CliL').attr('disabled', false);
                $('#CrpL').attr('disabled', false);
                
                choicesRegL.removeActiveItems();
                $('#GinL').prop('selectedIndex',0);
                choicesLocL.removeActiveItems();
                $('#CliL').prop('selectedIndex',0);
                $('#CrpL').prop('selectedIndex',0);
                $('#DOL').val("");
                $('#LotL').val("");
                $('#buscafiltro').prop('disabled', true);
            }
        }
    });

// ---------------------- Fin funcion de filtros --------------------------------------------------------------------------------------------
});

//Function informacion camiones asociados a lotes
/*$(document).ready(function(){
$(document).on("click", ".btnTrk", function(){
    opcion = 5;
    fila = $(this).closest("tr");	        
    TrkID = parseInt(fila.find('td:eq(12)').text()); //capturo el ID del camion asociado
    $.ajax({
          url: "bd/crud.php",
          type: "POST",
          datatype:"json",    
          data:  {TrkID:TrkID, opcion:opcion},    
          success: function(data) {
            opts = JSON.parse(data);
            for (var i = 0; i< opts.length; i++){
                htmlData = '<ul><li>Transport Name: '+opts[i].TNam+'</li><li>Driver Name: '+opts[i].DrvNam+'</li><li>Driver Telephone: '+opts[i].DrvTel+'</li><li>Departure Date: '+opts[i].OutDat+'</li><li>Arrival Date: '+opts[i].InDat+'</li></ul>';
                $('.modal-body').html(htmlData);
            } 
          }
    });
    $(".modal-header").css("background-color", "#17a2b8");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Trucks");		
    $('#modalInf').modal('show');
 });
});*/

$(document).ready(function(){
    //Cargar Regiones
    function LoadReg(){
        //$("#Reg").empty();
        var opts = "";
        opcion = 6;
        $.ajax({
            url: "bd/crud.php",
            type: "POST",
            datatype:"json",
            data:  {opcion:opcion},    
            success: function(data){
                opts = JSON.parse(data);
                //$('#Reg').append('<option value="0">- Select Region -</option disabled>');
                for (var i = 0; i< opts.length; i++){
                    $('#Reg').append('<option value="' + opts[i].RegNam + '">' + opts[i].RegNam + '</option>');
                }        
            },
            error: function(data) {
                alert('error');
            }
        });
    }
    
    function LoadCli(){
        //$("#Cli").empty();
        var opts = "";
        opcion = 9;
        $.ajax({
            url: "bd/crud.php",
            type: "POST",
            datatype:"json",
            data:  {opcion:opcion},    
            success: function(data){
                opts = JSON.parse(data);
                $('#Cli').append('<option value="">- Select Client -</option>');
                for (var i = 0; i< opts.length; i++){
                    $('#Cli').append('<option value="' + opts[i].Cli + '">' + opts[i].Cli + '</option>');
                }        
            },
            error: function(data) {
                alert('error');
            }
        });
    }
    
    function LoadLoc(){
        var opts = "";
        opcion = 8;
        $.ajax({
            url: "bd/crud.php",
            type: "POST",
            datatype:"json",
            data:  {opcion:opcion},    
            success: function(data){
                opts = JSON.parse(data);
                //$('#Loc').append('<option value="0">- Select Location -</option disabled>');
                for (var i = 0; i< opts.length; i++){
                    $('#Loc').append('<option value="' + opts[i].RegNam + '">' + opts[i].RegNam + '</option>');
                }        
            },
            error: function(data) {
                alert('error');
            }
        });
    }
    
    $("#btnNuevo").click(function(){
        LoadReg();
        LoadLoc();
        LoadCli();
        $("#Reg").on("change", function(){
            $("#Loc").empty();
            Reg = $.trim($('#Reg').val());
            $('#Loc').append('<option value="' + Reg + '" selected>' + Reg + '</option>');
            LoadLoc();
        });
    });
    
    $(document).on("click", ".btnEditar", function(){
        LoadReg();
        LoadLoc();
        LoadCli();  
    });
});
// funcion para leer el gin 
$(document).ready(function(){
    function LoadGin(){
        //$("#Reg").empty();
        Reg = $.trim($('#Reg').val());    
        var opts = "";
        opcion = 7;
        $.ajax({
            url: "bd/crud.php",
            type: "POST",
            datatype:"json",
            data:  {Reg:Reg, opcion:opcion},    
            success: function(data){
                opts = JSON.parse(data);
                //$('#GinID').append('<option value="" disabled selected >- Select -</option>');  //$('#GinID').append('<option value="0">- Select Gin -</option disabled>');
                for (var i = 0; i< opts.length; i++){
                    $('#GinID').append('<option value="' + opts[i].GinName + '">' + opts[i].GinName + '</option>');
                }        
            },
            error: function(data) {
                alert('error');
            }
        });
    }
    
    $("#Reg").on("change", function(){
        $("#GinID").empty();
        $('#GinID').append('<option value="" disabled selected >- Select -</option>');
        LoadGin();        
    });
    
    $(document).on("click", ".btnEditar", function(){
        LoadGin();
    });
    
});

$(document).ready(function(){
    $("#DOrd").keyup(function(){
    })
        
    function update(){
        $("#DOQty").val("");
        $('#DOType').val("");
        $('#DOGin').val("");
        $('#DOOutReg').val("");
        $('#DOInReg').val("");
        $('#DOCli').val("");
        var opts = "";
        opcion = 11;
        DOrd = $.trim($('#DOrd').val());
        $.ajax({
            url: "bd/crud.php",
            type: "POST",
            datatype:"json",
            data:  {DOrd:DOrd ,opcion:opcion},    
            success: function(data){
                opts = JSON.parse(data);
                $('#DOQty').val(opts[0].TTQty);
                $('#DOType').val(opts[0].Typ);
                $('#DOGin').val(opts[0].Gin);
                $('#DOOutReg').val(opts[0].OutPlc);
                $('#DOInReg').val(opts[0].InReg);
                $('#DOCli').val(opts[0].CliID);
            },
            error: function(data) {
                alert('error');
            }
        });
    }
        
    $("#DOrd").on("change", function(){
        $("#InfDO").css("display", "block");
        $("#DOQty").val("");
        $('#DOType').val("");
        $('#DOGin').val("");
        $('#DOOutReg').val("");
        $('#DOInReg').val("");
        $('#DOCli').val("");
        update();
    })
});

//Asignar fecha dependiendo del cliente
$(document).ready(function() {
    //Siempre que salgamos de un campo de texto, se chequeará esta función
    $("#Cli").change(function() {
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today = now.getFullYear()+"-"+(month)+"-"+(day);
        $("#AsgmDate").val(today);
    });
});

$(document).ready(function(){

	$('.enviar_archivo').on( "click", function(evt) {
		evt.preventDefault();
		cargarArchivoCSV();
	});

});

function cargarArchivoCSV(){

	var archivo 		  = $('input[name=archivo_csv]').val();
	var extension		  = $('#archivo_csv').val().split(".").pop().toLowerCase();
	var Formulario		  = document.getElementById('formUsuarios');
	var dataForm		  = new FormData(Formulario);

	var retornarError     = false;

	if(archivo=="")
	{
		$('#archivo_csv').addClass('error');
		retornarError = true;
		$('#archivo_csv').focus();
	} 
	else if($.inArray(extension, ['csv']) == -1)
	{
		alert("¡El archivo que esta tratando de subir es invalido!");
		retornarError = true;
		$('#archivo_csv').val("");
	}
	else
	{
		$('#archivo_csv').removeClass('error');
	}

    // A continuacion se resalta todos los campos que contengan errores.
    if(retornarError == true)
    {
        return false;
    }

    $.ajax({

		url: 'bd/procesar.php',
		type: 'POST',
		data: dataForm,
		cache: false,
		contentType: false,
		processData: false,
        beforeSend: function(){
            //$('#estado').prepend('<p><img src="images/facebook.gif" /></p>');
        },
        success: function(data){
            $('#estado').fadeOut("fast",function()
            {
                $('#estado').html(data);
            });
            
            $('#estado').fadeIn("slow");
            $("#formUsuarios").find('input[type=file]').val("");

        },
		error: function (jqXHR, textStatus, errorThrown) {
		    $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
		}
    });
}


///---------------------------------- Comienza funciones para bloquear y desbloquear inputs ---------------------------------------------------------------------------------------
$(document).ready(function(){
    $('#RegL,#GinL,#LocL,#DOL,#LotL,#CliL,#CrpL').on('change',function(){
        $('#buscafiltro').prop('disabled', false);
    });

    $('#RegL').on('change',function(){
        Region = $.trim($('#RegL').val());
        if (Region != ""){
            $('#GinL').attr('disabled', false);
            choicesLocL.disable();
            $('#DOL').attr('disabled', true);
            $('#LotL').attr('disabled', true);
            $('#CliL').attr('disabled', true);
            
            choicesLocL.removeActiveItems();
            $('#CliL').prop('selectedIndex',0);
            $('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            $('#LotL').val("");
            $('#buscafiltro').prop('disabled', false);
            $("#GinL").empty();
            Region = $.trim($('#RegL').val());    
            var opts = "";
            opcion = 12;

            //alert (Reg);
            if (Region.split(',').length === 1) {
                $('#GinL').attr('disabled', false);
                $.ajax({
                    url: "bd/crud.php",
                    type: "POST",
                    datatype: "json",
                    data: { Region: Region, opcion: opcion },
                    success: function (data) {
                        opts = JSON.parse(data);
                        $('#GinL').append('<option value="" selected >All...</option>');
                        for (var i = 0; i < opts.length; i++) {
                            $('#GinL').append('<option value="' + opts[i].IDGin + '">' + opts[i].GinName + '</option>');
                        }
                    },
                    error: function (data) {
                        alert('error');
                    }
                });
            } else {
                $('#GinL').attr('disabled', true);
            }
        }else{
            choicesRegL.enable()
            $('#GinL').attr('disabled', true);
            choicesLocL.enable();
            $('#DOL').attr('disabled', false);
            $('#LotL').attr('disabled', false);
            $('#CliL').attr('disabled', false);
            $('#CrpL').attr('disabled', false);
            
            //$('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            choicesLocL.removeActiveItems();
            $('#CliL').prop('selectedIndex',0);
            //$('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            $('#LotL').val("");
            $('#buscafiltro').prop('disabled', true);
        }
    });


    $('#LocL').on('change',function(){
        LocL = $.trim($('#LocL').val());
        if (LocL != ""){
            choicesRegL.disable()
            $('#GinL').attr('disabled', true);
            $('#DOL').attr('disabled', true);
            //$('#LotL').attr('disabled', true);
            $('#CliL').attr('disabled', true);
            
            //$('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            $('#CliL').prop('selectedIndex',0);
            //$('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            //$('#LotL').val("");
            
            $('#buscafiltro').prop('disabled', false);
        }else{
            choicesRegL.enable()
            $('#GinL').attr('disabled', true);
            choicesLocL.enable();
            $('#DOL').attr('disabled', false);
            $('#LotL').attr('disabled', false);
            $('#CliL').attr('disabled', false);
            $('#CrpL').attr('disabled', false);
            
            //$('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            choicesLocL.removeActiveItems();
            $('#CliL').prop('selectedIndex',0);
            //$('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            $('#LotL').val("");
            $('#buscafiltro').prop('disabled', true);
        }
        
    });

    $('#DOL').on('change',function(){
        DOL = $.trim($('#DOL').val());
        if (DOL != ""){
            choicesRegL.disable()
            $('#GinL').attr('disabled', true);
            choicesLocL.disable();
            $('#LotL').attr('disabled', true);
            $('#CliL').attr('disabled', true);
            $('#CrpL').attr('disabled', true);
            //$("input.group1").attr("disabled", true);
            
            //$('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            choicesLocL.removeActiveItems();
            $('#CliL').prop('selectedIndex',0);
            $('#CrpL').prop('selectedIndex',0);
            //$('#DOL').val("");
            $('#LotL').val("");
            
            $('#buscafiltro').prop('disabled', false);
        }else{
            choicesRegL.enable()
            $('#GinL').attr('disabled', true);
            choicesLocL.enable();
            $('#DOL').attr('disabled', false);
            $('#LotL').attr('disabled', false);
            $('#CliL').attr('disabled', false);
            $('#CrpL').attr('disabled', false);
            //$("input.group1").attr("disabled", false);
            
            //$('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            choicesLocL.removeActiveItems();
            $('#CliL').prop('selectedIndex',0);
            $('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            $('#LotL').val("");
            $('#buscafiltro').prop('disabled', true);
        }
        
    });

    $('#LotL').on('change',function(){
        LotL = $.trim($('#LotL').val());
        if (LotL != ""){
            choicesRegL.disable()
            $('#GinL').attr('disabled', true);
            //choicesLocL.disable();
            $('#DOL').attr('disabled', true);
            $('#CliL').attr('disabled', true);
            $('#CrpL').attr('disabled', true);
            
            //$('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            $('#CliL').prop('selectedIndex',0);
            $('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            //$('#LotL').val("");
            
            $('#buscafiltro').prop('disabled', false);
        }else{
            choicesRegL.enable()
            $('#GinL').attr('disabled', true);
            choicesLocL.enable();
            $('#DOL').attr('disabled', false);
            $('#LotL').attr('disabled', false);
            $('#CliL').attr('disabled', false);
            $('#CrpL').attr('disabled', false);
            
            //$('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            choicesLocL.removeActiveItems();
            $('#CliL').prop('selectedIndex',0);
            $('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            $('#LotL').val("");
            $('#buscafiltro').prop('disabled', true);
        }
        
    });

    $('#CliL').on('change',function(){
        CliL = $.trim($('#CliL').val());
        if (CliL != ""){
            choicesRegL.disable()
            $('#GinL').attr('disabled', true);
            choicesLocL.disable();
            $('#DOL').attr('disabled', true);
            $('#LotL').attr('disabled', true);
            $('#CrpL').attr('disabled', true);
            
            //$('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            choicesLocL.removeActiveItems();
            $('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            $('#LotL').val("");
            
            $('#buscafiltro').prop('disabled', false);
        }else{
            choicesRegL.enable()
            $('#GinL').attr('disabled', true);
            choicesLocL.enable();
            $('#DOL').attr('disabled', false);
            $('#LotL').attr('disabled', false);
            $('#CliL').attr('disabled', false);
            $('#CrpL').attr('disabled', false);
            
            //('#RegL').prop('selectedIndex',0);
            choicesRegL.removeActiveItems();
            $('#GinL').prop('selectedIndex',0);
            //$('#LocL').prop('selectedIndex',0);
            choicesLocL.removeActiveItems();
            $('#CliL').prop('selectedIndex',0);
            $('#CrpL').prop('selectedIndex',0);
            $('#DOL').val("");
            $('#LotL').val("");
            $('#buscafiltro').prop('disabled', true);
        }
        
    });

    $('#CrpL').on('change',function(){
        CrpL = $.trim($('#CrpL').val());
        if (CrpL != ""){
            $('#buscafiltro').prop('disabled', false);
        }
    });
    
    $('#RegInf').on('change',function(){
        
        Region = $.trim($('#RegInf').val());
        $("#GinInf").empty();  
        var opts = "";
        opcion = 12;
        $.ajax({
            url: "bd/crud.php",
            type: "POST",
            datatype:"json",
            data:  {Region:Region, opcion:opcion},    
            success: function(data){
                opts = JSON.parse(data);
                $('#GinInf').append('<option value="" selected >All...</option>');
                for (var i = 0; i< opts.length; i++){
                    $('#GinInf').append('<option value="' + opts[i].IDGin + '">' + opts[i].GinName + '</option>');
                }        
            },
            error: function(data) {
                alert('error');
            }
        });
    });
    
});

//---------------------------------- Termina funciones para bloquear y desbloquear inputs ---------------------------------------------------------------------------------------


$(document).ready( function () {

    //Condiciones check para DO
    $('#SinDO').on('change',function(){
        if (this.checked) {
            $('.groupCDO').prop('checked', false);
            $('.groupAllDO').prop('checked', false);
        }  
    });

    $('#ConDO').on('change',function(){
        if (this.checked) {
            $('.groupSDO').prop('checked', false);
            $('.groupAllDO').prop('checked', false);
        }  
    });

    $('#AllDO').on('change',function(){
        if (this.checked) {
            $('.groupSDO').prop('checked', false);
            $('.groupCDO').prop('checked', false);
        }  
    });

    $('#SinDO,#ConDO,#AllDO').on('change',function(){
        if (this.checked) {
        }else{
            $('.groupAllDO').prop('checked', true);
        }  
    });

    //Condiciones check para assigned
    $('#SinCli').on('change',function(){
        if (this.checked) {
            $('.groupCC').prop('checked', false);
            $('.groupAllC').prop('checked', false);
        }  
    });

    $('#ConCli').on('change',function(){
        if (this.checked) {
            $('.groupSC').prop('checked', false);
            $('.groupAllC').prop('checked', false);
        }  
    });

    $('#AllCli').on('change',function(){
        if (this.checked) {
            $('.groupSC').prop('checked', false);
            $('.groupCC').prop('checked', false);
        }  
    });

    $('#SinCli,#ConCli,#AllCli').on('change',function(){
        if (this.checked) {
        }else{
            $('.groupAllC').prop('checked', true);
        }  
    });

    // filtrar tabla Sin DO
    $.fn.dataTable.ext.search.push(
      function( settings, searchData, index, rowData, counter ) {
        var SDOs = $('input:checkbox[name="SinDO"]:checked').map(function() {
          return this.value;
        }).get();
     
        if (SDOs.length === 0) {
          return true;
        }
        
        if (SDOs.indexOf(searchData[7]) !== -1) {
          return true;
        }
        
        return false;

      }
    );
    
    // Filtrar tabla Con DO
    $.fn.dataTable.ext.search.push(
        function( settings, searchData, index, rowData, counter ) {

          var CDOs = $('input:checkbox[name="ConDO"]:checked').map(function() {
            return this.value;
          }).get();
       
          if (CDOs.length === 0) {
            return true;
          }
          
          if (CDOs.indexOf(searchData[7]) === -1){
            return true;
          }
          
          return false;
          
        }
      );
  
    // Filtrar Sin Client
    $.fn.dataTable.ext.search.push(
      function( settings, searchData, index, rowData, counter ) {

        var SClis = $('input:checkbox[name="SinCli"]:checked').map(function() {
          return this.value;
        }).get();
  
        if (SClis.length === 0) {
          return true;
        }
        
        if (SClis.indexOf(searchData[15]) !== -1) {
          return true;
        }
        
        return false;
      }
    );

    // Filtrar con Client
    $.fn.dataTable.ext.search.push(
        function( settings, searchData, index, rowData, counter ) {
  
          var CClis = $('input:checkbox[name="ConCli"]:checked').map(function() {
            return this.value;
          }).get();
    
          if (CClis.length === 0) {
            return true;
          }
          
          if (CClis.indexOf(searchData[15]) === -1) {
            return true;
          }
          
          return false;
        }
      );
    
  
    //var tablaUsuarios = $('#example').DataTable();
    
   $('input:checkbox').on('change', function () {
    tablaUsuarios.draw();
   });
  
});

var choicesRegL;
$(document).ready(function(){
    choicesRegL = new Choices('#RegL', {
        removeItemButton: true,
        maxItemCount:7
    });     
});

var choicesLocL;
$(document).ready(function(){
    choicesLocL = new Choices('#LocL', {
        removeItemButton: true,
        maxItemCount:7
    });     
});
