$(document).ready(function() {

    $('#tablaFixations tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    } );

    var opcion ;
    opcion = 4;

    tablaFix = $('#tablaFixations').DataTable({
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Export to Excel',
				className: 'btn btn-success',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			{
				extend:    'pdfHtml5',
				text:      '<i class="fas fa-file-pdf"></i> ',
				titleAttr: 'Export to PDF',
                className: 'btn btn-danger',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
		],
        
        // funcion para las busquedas
        initComplete: function () {
            this.api().columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                        .search( this.value )
                        .draw();
                    }
                });
            });
        },
        "order": [ 1, 'asc' ],
        "scrollX": true,
        "scrollY": "50vh",
        "autoWidth": true,
        "scrollCollapse": true,
        "lengthMenu": [100,200,300,500],
        fixedColumns:   {
            left: 0,
            right: 1
        },
        "ajax":{            
            "url": "bd/crudFixations.php", 
            "method": 'POST', //usamos el metodo POST
            "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
            "dataSrc":""
        },
        "columns":[
            {"data": "idFix"},
            {"data": "date"},
            {"data": "s_p"},
            {"data": "name"},
            {"data": "bc"},
            {"data": "fut"},
            {"data": "covMonth"},
            {"data": "fixed"},
            {"data": "confirmed"},
            {"data": "contract"},
            {"data": "pos"},
            {"data": "nav"},
            {"defaultContent": ""}/* <div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button>" */
        ],
        columnDefs : [
            { targets : [2],
                render : function (data, type, row) {
                  return data == '0' ? 'SALE' : 'PURCHASE'
                }
            },
            { targets : [10],
                render : function (data, type, row) {
                  return data == '0' ? 'NO' : 'SI'
                }
            },
            { targets : [11],
                render : function (data, type, row) {
                  return data == '0' ? 'NO' : 'SI'
                }
            }
        ]
    });
    $('.dataTables_length').addClass('bs-select');

    var fila; //captura la fila, para editar o eliminar
    //submit para el Alta y Actualización
    $('#formFix').submit(function(e){
        e.preventDefault(); //evita el recargar toda la pagina
        date1 = $.trim($('#fecha').val());
        s_p = $("input[name='s_p']:checked").val();
        name1 = $.trim($('#name').val());
        bc = $.trim($('#bc').val());
        fut = $.trim($('#futures').val());
        covMonth = $.trim($('#mes').val());
        fixed = $.trim($('#fixed').val());
        confirmed = $.trim($('#confirmed').val());
        contract = ($('#contract').val().toUpperCase())
        pos = $('#pos').prop('checked') ? 1 : 0;
        nav = $('#nav').prop('checked') ? 1 : 0;
        
        $.ajax({
            url: "bd/crudFixations.php",
            type: "POST",
            datatype:"json",    
            data:  {date:date1, s_p:s_p, name:name1, bc:bc, fut:fut, covMonth:covMonth,
                    fixed:fixed, confirmed:confirmed, contract:contract, pos:pos, nav:nav, opcion:opcion},    
            success: function(data) {
                tablaFix.ajax.reload(null, false);
            }
        });
        $('#modalCRUD').modal('hide');									     			
    });

    //Abrir el modal para agregar una fijacion
    $("#btnNueva").click(function(){
        opcion = 0; //alta  
        $('#Aviso').html("");
        $('#AvisoE').html("");
        $("#formFix").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("New fixations");
        $('#modalCRUD').modal('show');

        date();
        $("input[name='s_p']").change(function() {
            upNameSelect();
        });
        mes();
    });
    $("#mes").on("change", function() {
        var selectedCovMonth = $(this).val();
        obtenerValorAnt(selectedCovMonth);
    });
    
    $('#modalCRUD').on('hidden.bs.modal', function() {
        $("#name").prop('disabled', true);
        $("#name").empty().append($('<option>', {
            value: "",
            text: "-SELECT-"
        }));
    });
});

//Funcion para crear fecha
function date(){
    var fecha = new Date();
    document.getElementById("fecha").value = fecha.toJSON().slice(0,10);
}

//Funcion para obtener y habilitar el campo name dependiendo si es S o P
function upNameSelect() {
    var selectedValue = $("input[name='s_p']:checked").val();
    var select = $("#name");
    select.prop('disabled', false);
    select.empty().append($('<option>', {
        value: "",
        text: "-SELECT-"
    }));

    if (selectedValue === "0") {
        nameCli();
    } else if (selectedValue === "1") {
        namePro();
    }
}

//Funcion para traer el nombre de los proveedores
function namePro(){
    $.ajax({
        url: "bd/crudFixations.php",
        cache: false,
        type: "POST",
        datatype:"json",
        data:  {opcion:2},    
        success: function(data){
            $("#name").empty();
            $('#name').append($('<option>').val("").text("-SELECT-"));
            opts = JSON.parse(data);
            for (var i = 0; i< opts.length; i++){    
                $('#name').append($('<option>').val(opts[i].SupName).text(opts[i].SupName));         
            }
        },
        error: function(data) {
            alert('error');
        }
    });
}

//funcion para traer el nombre de los clientes
function nameCli(){
    $.ajax({
        url: "bd/crudFixations.php",
        cache: false,
        type: "POST",
        datatype:"json",
        data:  {opcion:1},    
        success: function(data){
            $("#name").empty();
            $('#name').append($('<option>').val("").text("-SELECT-"));
            opts = JSON.parse(data);
            for (var i = 0; i< opts.length; i++){    
                $('#name').append($('<option>').val(opts[i].ClientGrp).text(opts[i].ClientGrp));         
            }
        },
        error: function(data) {
            alert('error');
        }
    });
}

//funcion para calcular los futuros 
document.addEventListener("DOMContentLoaded", function() {
    var bcElement = document.getElementById("bc");
    var futElement = document.getElementById("futures");
    var initialFuturesValue = 0; // Valor inicial de futures

    // Función para calcular "futures" a partir de "bc"
    function calcularFutures() {
        var bcValue = parseFloat(bcElement.value);
        var futValue = 0;

        if (!isNaN(bcValue)) {
            if (bcValue < 50) {
                futValue = 0;
            } else {
                futValue = Math.max(Math.round(bcValue / 100), 1);
            }
        }

        futElement.value = futValue;
        initialFuturesValue = futValue; // Actualiza el valor inicial de futures
        verificarCambio();
    }

    // Escuchar cambios en el campo "bc"
    bcElement.addEventListener("input", function() {
        calcularFutures();
    });

    // Función para aumentar el valor de "futures"
    document.getElementById("aumentar").addEventListener("click", function() {
        futElement.value = parseInt(futElement.value) + 1;
        verificarCambio();
    });

    // Función para disminuir el valor de "futures"
    document.getElementById("disminuir").addEventListener("click", function() {
        var futValue = parseInt(futElement.value);
        if (futValue > 0) {
            futElement.value = futValue - 1;
            verificarCambio();
        }
    });

    // Función para verificar si "futures" a aumentado en dos o mas unidades
    function verificarCambio() {
        var futValue = parseInt(futElement.value);
        if (Math.abs(futValue - initialFuturesValue) >= 2) {
            if (futValue > initialFuturesValue) {
                $('#Aviso').html('<small id="aviso" class="badge rounded-pill bg-danger">You increased futures</small>');
            } else {
                $('#Aviso').html('<small id="aviso" class="badge rounded-pill bg-danger">You decreased futures</small>');
            }
        } else {
            $('#Aviso').html('');
        }
    }
});

//Funcion para traer los meses de cover month
function mes(){
    $.ajax({
        url: "bd/crudFixations.php",
        type: "POST",
        datatype: "json",
        data:  {opcion:3},    
        success: function(data){  
            $("#mes").empty();
            opts = JSON.parse(data);
            for (var i = 0; i < opts.length; i++) {
                $('#mes').append($('<option>').val(opts[i].CovMon).text(opts[i].CovMon)); 
            }
            // Establecer el valor de "mes" inicialmente con el valor de "Z23"
            var antInput = $("#mes");
            antInput.val("Z23");
            // Obtener y mostrar el valor de "ant" correspondiente a "Z23"
            obtenerValorAnt("Z23");
        },
        error: function(data) {
            alert('error');
        }
    });
}

// Función para obtener y mostrar el valor de "ant"
function obtenerValorAnt(covMonth) {
    $.ajax({
        url: "bd/crudFixations.php",
        type: "POST",
        dataType: "json",
        data: { opcion: 5, covMonth: covMonth },
        success: function(data) {
            var antInput = $("#ant");
            if (data.length > 0) {
                var valorObtenido = data[0].confirmed;
                // Formatear el valor a dos decimales
                var valorFormateado = parseFloat(valorObtenido).toFixed(2);
                antInput.val(valorFormateado);
            } else {
                antInput.val("0.00");
            }
        },
        error: function(data) {
            alert('Error al obtener el valor de covMonth');
        }
    });
}

//funcion para calcular la diferencia de fijado menos confirmado
function calcularDiferencia() {
    var confirmedInput = document.getElementById("confirmed");
    var fixedInput = document.getElementById("fixed");
    var differenceInput = document.getElementById("difference");

    var confirmedValue = parseFloat(confirmedInput.value);
    var fixedValue = parseFloat(fixedInput.value);

    if (!isNaN(confirmedValue) && !isNaN(fixedValue)) {
        var diferencia = confirmedValue - fixedValue;
        diferencia = parseFloat(diferencia.toFixed(3));
        differenceInput.value = diferencia;
    } else {
        differenceInput.value = "0.00";
    }
}

//funcion vala calcular el earned aprox
document.addEventListener("DOMContentLoaded", function() {
    function calcularEarned() {
        var s_p = document.querySelector('input[name="s_p"]:checked');
        var difference = document.getElementById("difference");
        var bc = document.getElementById("bc");
        var fixed = document.getElementById("fixed");
        var confirmed = document.getElementById("confirmed");
        var earned = document.getElementById("earned");

        var s_pValue = s_p ? parseInt(s_p.value) : null;
        var differenceValue = parseFloat(difference.value);
        var bcValue = parseFloat(bc.value);
        var fixedValue = parseFloat(fixed.value);
        var confirmedValue = parseFloat(confirmed.value);

        if (s_pValue !== null && !isNaN(differenceValue) && !isNaN(bcValue) && !isNaN(fixedValue) && !isNaN(confirmedValue)) {
            var resultado = (s_pValue === 0 ? 1 : -1) * differenceValue * 485 / 100 * bcValue;
            earned.value = resultado.toFixed(2);
            if (!isNaN(earned.value ) && earned.value  < 0) {
                $('#AvisoE').html('<small id="aviso" class="badge rounded-pill bg-danger">negative results!!</small>');
            } else {
                $('#AvisoE').html('');
            }
        } else {
            earned.value = "";
        }
    }

    var s_pRadios = document.querySelectorAll('input[name="s_p"]');
    var bcInput = document.getElementById("bc");
    var fixedInput = document.getElementById("fixed");
    var confirmedInput = document.getElementById("confirmed");

    s_pRadios.forEach(function(radio) {
        radio.addEventListener("change", calcularEarned);
    });

    bcInput.addEventListener("input", calcularEarned);
    fixedInput.addEventListener("input", calcularEarned);
    confirmedInput.addEventListener("input", calcularEarned);
});
