<?php
include_once 'bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

// Actualiza el estado del contrato en la base de datos
$contractId = (isset($_POST['contractId'])) ? $_POST['contractId'] : '';
$aprueba = (isset($_POST['aprueba'])) ? $_POST['aprueba'] : '';

$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';

switch($opcion){
    case 0:
        $consulta = "UPDATE amsadb1.contract SET contStatus = 'APROBADO', aprueba = :aprueba WHERE IdContract = :contractId"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->bindParam(':contractId', $contractId, PDO::PARAM_INT);
        $resultado->bindParam(':aprueba', $aprueba, PDO::PARAM_STR);
        $resultado->execute();
    break;
    case 1:
        $consulta = "UPDATE amsadb1.contract SET contStatus = 'RECHAZADO', aprueba = :aprueba WHERE IdContract = :contractId"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->bindParam(':contractId', $contractId, PDO::PARAM_INT);
        $resultado->bindParam(':aprueba', $aprueba, PDO::PARAM_STR);
        $resultado->execute();
    break;
}
$conexion = null;
?>
