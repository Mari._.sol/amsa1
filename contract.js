$(document).ready(function() {
    $('#tablaContract tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    } );

    var opcion ;
    opcion = 4;
    tablaContract = $('#tablaContract').DataTable({
        //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Export to Excel',
				className: 'btn btn-success',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			{
				extend:    'pdfHtml5',
				text:      '<i class="fas fa-file-pdf"></i> ',
				titleAttr: 'Export to PDF',
                className: 'btn btn-danger',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
		],
        
        initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                });
            });
        },
        "order": [ 1, 'asc' ],
        "scrollX": true,
        "scrollY": "50vh",
        "autoWidth": true,
        "scrollCollapse": true,
        "lengthMenu": [100,200,300,500],
        fixedColumns:   {
            left: 0,
            right: 1
        },
        "ajax":{            
            "url": "bd/crudContract.php", 
            "method": 'POST', //usamos el metodo POST
            "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
            "dataSrc":""
        },
        "columns":[
            {"data": "IdContract"},
            {"data": "solicita"},
            {"data": "fecha"},
            {"data": "cliente"},
            {"data": "subCliente"},
            {"data": "lugarEntrega"},
            {"data": "de"},
            {"data": "al"},
            {"data": "incoterm"},
            {"data": "cantidad"},
            {"data": "cosecha"},
            {"data": "mesCobertura"},
            {"data": "region"},
            {"data": "destino"},
            {"data": "grado"},
            {"data": "fibra"},
            {"data": "micronara"},
            {"data": "resistencia"},
            {"data": "uniformidad"},
            {"data": "fijoCall"},
            {"data": "precioNY"}, 
            {"data": "puntos"},
            {"data": "precioFinal"},
            {"data": "uMedida"},
            {"data": "condPago"},
            {"data": "intNormal"}, 
            {"data": "intMoral"},
            {"data": "contStatus"},
            {"data": "comentario"},
            {"data": "nota"},
            {"data": "aprueba"},
            {"defaultContent": ""}//<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button>
        ]
    });
    
    $('#formContract').submit(function(e){
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
        solicita = $.trim($('#solicita').val());
        fecha = $.trim($('#fecha').val());
        //noSolicitud = $.trim($('#noSolicitud').val());
        cliente = $.trim($('#cliente').val());
        subCliente = $.trim($('#subCliente').val());
        lugarEntrega = $.trim($('#lugarEntrega').val());
        de = $.trim($('#de').val());
        al = $.trim($('#al').val());
        incoterm = $.trim($('#incoterm').val());
        cantidad = $.trim($('#cantidad').val());
        cosecha = $.trim($('#cosecha').val());
        mesCobertura = $.trim($('#mesCobertura').val());
        region = $.trim($('#region').val());
        destino = $.trim($('#destino').val());
        grado = $.trim($('#grado').val());
        fibra = $.trim($('#fibra').val());
        micronara = $.trim($('#micronara').val());
        resistencia = $.trim($('#resistencia').val());
        uniformidad = $.trim($('#uniformidad').val());
        fijoCall = $.trim($('#fijoCall').val());
        PrecioNY = $.trim($('#PrecioNY').val());
        puntos = $.trim($('#puntos').val());
        precioFinal = $.trim($('#precioFinal').val());
        uMedida = $.trim($('#uMedida').val());
        condPago = $.trim($('#condPago').val());
        intNormal = $.trim($('#intNormal').val());
        intMoral = $.trim($('#intMoral').val());
        contStatus = $.trim($('#contStatus').val());
        comentario = $.trim($('#comentario').val());
        nota = $.trim($('#nota').val());
        aprueba = $.trim($('#aprueba').val());

        $.ajax({
            url: "bd/crudContract.php",
            type: "POST",
            datatype:"json",    
            data:  {
                solicita:solicita, fecha:fecha, cliente:cliente, subCliente:subCliente, lugarEntrega:lugarEntrega, de:de, al:al, 
                incoterm:incoterm, cantidad:cantidad, cosecha:cosecha, mesCobertura:mesCobertura, region:region, destino:destino,
                grado:grado, fibra:fibra, micronara:micronara, resistencia:resistencia, uniformidad:uniformidad, fijoCall:fijoCall, 
                precioNY:precioNY, puntos:puntos, precioFinal:precioFinal, uMedida:uMedida, condPago:condPago, intNormal:intNormal, 
                intMoral:intMoral, contStatus:contStatus, comentario:comentario, nota:nota, aprueba:aprueba, opcion:opcion},    
            success: function(data) {
                tablaContract.ajax.reload(null, false);
            }
        });
        $('#modalCRUD').modal('hide');	
    });
        
    $("#btnNueva").click(function(){
        opcion = 0;
        $("#formContract").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("SOLICITUD: aprovacion de contrato (Venta)");
        $('#modalCRUD').modal('show');

        contract();
        date();
        clientes ();
        valFechaDeA();
        cosechaAños();
        mesCobertura1();
        
    });
    
    $("#btnSend").click(function(){
        asunto = $("#subject").val();
        solicita = $("#solicita").val();
        fecha = $("#fecha").val();
        noSolicitud = $("#noSolicitud").val();
        cliente = $("#cliente").val();
        subCliente = $("#subCliente").val();
        lugarEntrega = $("#lugarEntrega").val();
        de = $("#de").val();
        al = $("#al").val();
        incoterm = $("#incoterm").val();
        cantidad = $("#cantidad").val();
        cosecha = $("#cosecha").val();
        mesCobertura = $("#mesCobertura").val();
        region = $("#region").val();
        destino = $("#destino").val();
        grado = $("#grado").val();
        fibra = $("#fibra").val();
        micronara = $("#micronara").val();
        resistencia = $("#resistencia").val();
        uniformidad = $("#uniformidad").val();
        fijoCall = $("#fijoCall").val();
        precioNY = $("#precioNY").val();
        puntos = $("#puntos").val();
        precioFinal = $("#precioFinal").val();
        uMedida = $("#uMedida").val();
        condPago = $("#condPago").val();
        intNormal = $("#intNormal").val();
        intMoral = $("#intMoral").val();
        contStatus = $("#contStatus").val();
        comentario = $("#comentario").val();
        nota = $("#nota").val();
        aprueba = $("#aprueba").val();

        $('#modalCRUD').modal('hide');
        $.ajax({
            url: "bd/MailContrato.php",
            type: "POST",
            datatype:"json",
            data:{
                asunto:asunto, solicita:solicita, fecha:fecha, noSolicitud:noSolicitud, cliente:cliente, subCliente:subCliente, lugarEntrega:lugarEntrega, de:de, al:al, 
                incoterm:incoterm, cantidad:cantidad, cosecha:cosecha, mesCobertura:mesCobertura, region:region, destino:destino,
                grado:grado, fibra:fibra, micronara:micronara, resistencia:resistencia, uniformidad:uniformidad, fijoCall:fijoCall, 
                precioNY:precioNY, puntos:puntos, precioFinal:precioFinal, uMedida:uMedida, condPago:condPago, intNormal:intNormal, 
                intMoral:intMoral, contStatus:contStatus, comentario:comentario, nota:nota, aprueba:aprueba
            },    
            success: function(data){
                alert(data)
            }
        });
    });
});

//Funcion para traer el numero de solicitud
function contract(){
    $.ajax({
        url: "bd/crudContract.php",
        cache: false,
        type: "POST",
        dataType: "json",
        data: { opcion: 3 },    
        success: function(data){
            var sigID = data[0].sigID;
            if (sigID === null || isNaN(sigID)) {
                // Establecer un valor predeterminado (en este caso, 1)
                sigID = 1;
            }
            $('#noSolicitud').val(sigID);
        },
        error: function(data) {
            alert('error');
        }
    });
}


//funcion para traer la fecha de hoy 
function date(){
    var fecha = new Date();
    document.getElementById("fecha").value = fecha.toJSON().slice(0,10);
}

// funcion para traer la lista de clientes de la base de datos 
function clientes(){
    $.ajax({
        url: "bd/crudContract.php",
        cache: false,
        type: "POST",
        datatype:"json",
        data:  {opcion:1},    
        success: function(data){
            $("#cliente").empty(); //,#subCliente
            $('#cliente').append('<option class="form-control" selected disabled>-SELECT-</option>');
            opts = JSON.parse(data);
            for (var i = 0; i< opts.length; i++){    
                $('#cliente').append($('<option>').val(opts[i].Cli).text(opts[i].Cli));         
            }
        },
        error: function(data) {
            alert('error');
        }
    });
}

//funcion para validad que la primera fecha no sea mayor que la segunda 
function valFechaDeA(){
    var fechainicial = document.getElementById("de").value;
    var fechafinal = document.getElementById("al").value;

    if(Date.parse(fechafinal) < Date.parse(fechainicial)) {
        alert("Verifica que las fechas seas correctas");
        fechainicial = document.getElementById("de").value="";
        fechafinal = document.getElementById("al").value="";
    }
}

// funcion para evitar que se coloquen numeros negativos en el campo cantidad 
function evitarNegativo(e) {
    if (e.key === "-")
      e.preventDefault();
}

//funcion para cosecha
function cosechaAños() {
    var selectCosecha = document.getElementById("cosecha");
    var añoActual = new Date().getFullYear();

    for (var i = 0; i < 4; i++) {
        var año = añoActual + i;
        var option = document.createElement("option");
        option.value = año;
        option.text = año;
        selectCosecha.appendChild(option);
    }
}

//funcion para traer los cover month de la base de datos 
function mesCobertura1(){
    $.ajax({
        url: "bd/crudContract.php",
        type: "POST",
        datatype:"json",
        data:  {opcion:2},    
        success: function(data){  
            $("#mesCobertura").empty();       
            $('#mesCobertura').append($('<option>').val("").text("-SELECT-"));
            opts = JSON.parse(data);
            for (var i = 0; i< opts.length; i++){
                $('#mesCobertura').append($('<option>').val(opts[i].CovMon).text(opts[i].CovMon)); 
            }
        },
        error: function(data) {
            alert('error');
        }
    });
}

//funcion para sumar el precio base + los puntos 
function sumar(){
    total = document.getElementById('precioFinal');
    let subtotal = 0;
    [ ...document.getElementsByClassName( "prePun" ) ].forEach( function ( element ) {
    if(element.value !== '') {
        subtotal += parseFloat(element.value);
    }
    });
    total.value = subtotal;
}
