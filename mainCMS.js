$(document).ready(function() {

    //funcion requerida para sumar 
    jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
        return this.flatten().reduce( function ( a, b ) {
          if ( typeof a === 'string' ) {
            a = a.replace(/[^\d.-]/g, '') * 1;
          }
          if ( typeof b === 'string' ) {
            b = b.replace(/[^\d.-]/g, '') * 1;
          }
          return a + b;
        }, 0);
    });

    //promedio de columnas
    jQuery.fn.dataTable.Api.register( 'average()', function () {
        var data = this.flatten();
        var sum = data.reduce( function ( a, b ) {
            return (a*1) + (b*1); // cast values in-case they are strings
        }, 0 );
      
        return sum / data.length;
    });
    
    /*$('#tablaBales tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    } );

    $('#tablaBalesMod tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    });*/

    var opcion;
    opcion = 4;
    
    var tablaCMS = $('#tablaCMS').DataTable({

        //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[
			/*{
			    extend:    'excelHtml5',
			    text:      '<i class="fas fa-file-excel"></i> ',
			    titleAttr: 'Export to Excel',
			    className: 'btn btn-success',
                download: 'open',
			    exportOptions: {
			        columns: ":not(.no-exportar)"
			    }
			},
			/*{
			    extend:    'pdfHtml5',
			    text:      '<i class="fas fa-file-pdf"></i> ',
			    titleAttr: 'Export to PDF',
			    orientation: 'landscape',
			    className: 'btn btn-danger',
                download: 'open',
			    exportOptions: {
			        columns: ":not(.no-exportar)"
			    }
			},
            {
                text: '<i class="bi bi-funnel-fill"></i>',
                titleAttr: 'Filtrar por',
                orientation: 'landscape',
                className: 'btn btn-info btn-filtrar',
                attr:{
                        id:'filtraboton',
                        "data-toggle": 'modal tooltip',
                        "disabled": false
                }
            },
			/*{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},*/
        ],
        "order": [ 2, 'asc' ],
        "scrollX": false,
        "scrollY": "50vh",
        "scrollCollapse": true,
        "lengthMenu": [100,200,300,500],
        "ajax":{          
            "url": "bd/crudCMS.php", 
            "method": 'POST', //usamos el metodo POST
            "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
            "dataSrc":""
        },
        "columns":[
            {"data": "Crp"},
            {"data": "GinName"},
            {"data": "Lot"},
            {"data": "Bal"},
            {"data": "Wgh", render: $.fn.dataTable.render.number( ',', '.', 0)},
            {"data": "LiqBal"},
            {"data": "Note"}
            //{"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnMail'><i class='bi bi-envelope-fill'></i></button></div></div>"}
        ],
        "columnDefs": [
            { width: 150, targets: 0 },
            { width: 200, targets: 1 },
            { width: 150, targets: 2 },
            { width: 150, targets: 3 },
            { width: 150, targets: 4 },
            { width: 150, targets: 5 },
            { width: 400, targets: 6 }
        ]
    });

    $('.dataTables_length').addClass('bs-select');
    
    var fila; //captura la fila, para editar o eliminar -- submit para el Alta y Actualización

    // botón filtros

    $("#filtraboton").click(function(){
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        LotCMS = $('#LotCMS').val();

        $('#filtrarmodal').modal('show');
    });

    var botonglobal = 0;
    $("#buscafiltro, #borrarFiltro, #borrarFiltroU, #cer").click(function(){
        boton = $(this).val();
        if(boton == 1 || boton == 0)
            botonglobal = boton;
        if(boton != 3){
            idGlobal = 0;
            DOCMS = $('#DOCMS').val();
            LotCMS = $('#LotCMS').val();
            //filters = {"muestras":muestras,"regionfil":regionfil};
            opcion = 4;
            var applyFilter =  $.ajax({
                type: 'POST',
                url: 'bd/assignFilters.php',
                data: {
                    boton: boton,
                    idGlobal:idGlobal,
                    DOCMS:DOCMS,
                    LotCMS: LotCMS
                }
                
            }) 
            applyFilter.done(function(data){

                console.log(data);
                tablaCMS.ajax.reload(function(){

                    if(boton == 1){
                        $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i> Filtros asignados con éxito</div>')
                        setTimeout(function() {
                            $('#alert1').fadeOut(1500);
                            $('#filtrarmodal').modal('hide');
                        },1500);
                        $('#DOsCMS').val(DOCMS);
                        $('#LotsCMS').val(LotCMS);
                    }
                    else{
                        $('#DOCMS').prop('disabled', false);
                        $('#LotCMS').prop('disabled', false);
                        $('#DOCMS').val("");
                        $('#LotCMS').val("");
                        $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"> <i class="bi bi-trash-fill"></i> Filtros borrados con éxito</div>')
                        setTimeout(function() {
                            $('#alert1').fadeOut(1500);
                        },1500);
                    }
                });
                $('#aviso').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');      
            });  
        }else{
            if(botonglobal == 0){
                $('#DOCMS').val("");
                $('#LotCMS').val("");
                $('#DOCMS').prop('disabled', false);
                $('#LotCMS').prop('disabled', false);
            }
        }
    });
    
        // boton SeachBales -------------------------------------------------------------------------------------------------------------------------------------
    $("#SearchBales").click(function(){
        $('#AreaOriginal').html("");
        $("#Bales").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $('#modal-SearchBales').modal('show');
    });
    

    // boton Report Stock -------------------------------------------------------------------------------------------------------------------------------------
    $("#ReportStock").click(function(){
        $("#Stock").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $('#modal-ReportStock').modal('show');
    });

});


//Accion para bloquear inputs
$(document).ready(function(){
    $('#DOCMS,#LotCMS').on('change', function () {
        DO = $('#DOCMS').val();
        Lot = $('#LotCMS').val();
        if (DO != ""){
            $('#LotCMS').val('');
            $('#LotCMS').prop('disabled', true);
        }else{
            $('#LotCMS').prop('disabled', false);
        }

        if (Lot != ""){
            $('#DOCMS').val('');
            $('#DOCMS').prop('disabled', true);
        }else{
            $('#DOCMS').prop('disabled', false);
        }

    });
});


$(document).ready(function(){
    $("#SubeLots").click(function(){
        
        DOsCMS = $.trim($('#DOsCMS').val());
        LotsCMS = $.trim($('#LotsCMS').val());
        DateCMS = $.trim($('#DateCMS').val());
        
        if (DOsCMS != "" || LotsCMS != ""){
        window.open("./bd/SubeLotsCsv.php?DOCMS="+DOCMS+"&LotsCMS="+LotsCMS+"&DateCMS="+DateCMS+"");
        }else{
            alert ("No lots filtered...");
        }

    });
});

$(document).ready(function(){
    $("#SubePacasLots").click(function(){
        
        DOsCMS = $.trim($('#DOsCMS').val());
        LotsCMS = $.trim($('#LotsCMS').val());
        DateCMS = $.trim($('#DateCMS').val());
        
        if (DOsCMS != "" || LotsCMS != ""){
        window.open("./bd/SubePacasCsv.php?DOCMS="+DOCMS+"&LotsCMS="+LotsCMS+"&DateCMS="+DateCMS+"");
        }else{
            alert ("No lots filtered...");
        }

    });
});

$(document).ready(function(){
    $("#ExportList").click(function(){
        
        DOsCMS = $.trim($('#DOsCMS').val());
        LotsCMS = $.trim($('#LotsCMS').val());
        
        if (DOsCMS != "" || LotsCMS != ""){
        window.open("./bd/ExportList.php?DOCMS="+DOCMS+"&LotsCMS="+LotsCMS+"");
        }else{
            alert ("No lots filtered...");
        }

    });
});

$(document).ready(function(){
    $("#ExportListPDF").click(function(){
        
        DOsCMS = $.trim($('#DOsCMS').val());
        LotsCMS = $.trim($('#LotsCMS').val());
        
        if (DOsCMS != "" || LotsCMS != ""){
        window.open("./bd/ExportListPDF.php?DOCMS="+DOCMS+"&LotsCMS="+LotsCMS+"");
        }else{
            alert ("No lots filtered...");
        }

    });
});

// ----- Reporte stock -------
$(document).ready(function(){
    $("#SearchStock").click(function(){

        Reg = $.trim($('#RegStock').val());
        if (Reg != ""){
            window.open("./bd/ReportStock.php?Reg="+Reg+"");
            $('#modal-ReportStock').modal('hide');
        }else{
            alert ("No data selected...");
        }

    });
});

$(document).ready(function(){
    $('#SBReg').on('change',function(){
            
        Region = $.trim($('#SBReg').val());
        $("#SBGin").empty();  
        var opts = "";
        opcion = 5;
        $.ajax({
            url: "bd/crudCMS.php",
            type: "POST",
            datatype:"json",
            data:  {Region:Region, opcion:opcion},    
            success: function(data){
                opts = JSON.parse(data);
                $('#SBGin').append('<option value="" selected >All...</option>');
                for (var i = 0; i< opts.length; i++){
                    $('#SBGin').append('<option value="' + opts[i].IDGin + '">' + opts[i].GinName + '</option>');
                }        
            },
            error: function(data) {
                alert('error');
            }
        });
    });
});

$(document).ready(function(){
    var opts = "";
    opcion = 7;
    $.ajax({
        url: "bd/crudCMS.php",
        type: "POST",
        datatype:"json",
        data:  {opcion:opcion},    
        success: function(data){
            opts = JSON.parse(data);
            $('#SBReg').append('<option value="" selected >All...</option>');
            for (var i = 0; i< opts.length; i++){
                $('#SBReg').append('<option value="' + opts[i].IDReg + '">' + opts[i].RegNam + '</option>');
            }        
        },
        error: function(data) {
            alert('error');
        }
    });
});


// -----   boton buscar dentro de modal search by bales ---------- 
$(document).ready(function(){
    $("#Search").click(function(){
        $('#AreaOriginal').html("");
        opcion = 6;
        
        Region = $('#SBReg').val();
        console.log(Region)
        SBGin = $('#SBGin').val();
        console.log(SBGin)
        SBBale = $('#SBBale').val();
        console.log(SBBale)
        SBCrp = $('#SBCrp').val();
        console.log(SBCrp)
        
        $.ajax({
            url: "bd/crudCMS.php",
            type: "POST",
            datatype:"json",
            data:  { Region:Region, SBGin:SBGin, SBBale:SBBale, SBCrp:SBCrp, opcion:opcion },    
            success: function(data){
                
                console.log(data);
                opts = JSON.parse(data);

                // Dibujar tabla con resultado de la busqueda de pacas
                var tablaGO = '<table id="TablaRecap" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;">';
                tablaGO += '<thead>';
                tablaGO += '<tr class="">';
                tablaGO += '<th>Crop</th><th>Bale</th><th>Supplier</th><th>Region</th><th>Gin</th><th>Wgh</th><th>Lot</th><th>DO</th><th>LiqID</th><th>Grupo</th>';
                tablaGO += '</tr>';
                tablaGO += '</thead>';
                tablaGO += '<tbody>';
                tr = '';
                i=0;
                for (var i = 0; i< opts.length; i++){
                    if (opts[i].DOrd == null){
                        DOrd = "N / A";
                    }else{
                        DOrd = opts[i].Lot;
                    }

                    if (opts[i].Lot == null){
                        LotStatus = "N / A";
                    }else{
                        LotStatus = opts[i].Lot;
                    }
                    
                    if (opts[i].LiqID == null){
                        LiqStatus = "N / A";
                    }else if (opts[i].LiqID.substring(3, 6) == "SLQ" || opts[i].LiqID.substring(0, 3) == "SLQ"){
                        LiqStatus = "N / A";
                    }else{
                        LiqStatus = opts[i].LiqID;
                    }


                    if (opts[i].Grp == 20){
                        Grp = "Certificada";
                    }else if (opts[i].Grp == 30){
                        Grp = "Pepena";
                    }else if (opts[i].Grp == 40){
                        Grp = "Bark";
                    }else if (opts[i].Grp == 81){
                        Grp = "Ojo de gato-Bajo";
                    }else if (opts[i].Grp == 82){
                        Grp = "Ojo de gato-Medio";
                    }else if (opts[i].Grp == 83){
                        Grp = "Ojo de gato-Alto";
                    }else {//if(opts[i].Grp == 0)
                        Grp = "Linea";
                    }

                    tr += '<tr>';
                    tr += '<td>'+opts[i].Crp+'</td><td>'+opts[i].Bal+'</td><td>'+opts[i].SupName+'</td><td>'+opts[i].RegNam+'</td><td>'+opts[i].GinName+'</td><td>'+opts[i].Wgh+'</td><td>'+LotStatus+'</td><td>'+DOrd+'</td><td>'+LiqStatus+'</td><td>'+Grp+'</td>';
                    tr += '</tr>';
                }
                tablaGO += tr;
                tablaGO += '</tbody></table>';

                $('#AreaOriginal').html(tablaGO);
      
            },
            error: function(data) {
                alert('error');
            }
        });    
    });
});


//funcion formato de fechas
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}