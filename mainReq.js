$(document).ready(function() {

    $('#tablaReq tfoot th').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        } );
    
    
    var opcion;
    opcion = 4;
        
    tablaReq = $('#tablaReq').DataTable({
        
        //para usar los botones   
            responsive: "true",
            dom: 'Brtilp',       
            buttons:[ 
                {
                    extend:    'excelHtml5',
                    text:      '<i class="fas fa-file-excel"></i> ',
                    titleAttr: 'Export to Excel',
                    className: 'btn btn-success',
                    exportOptions: {
                        columns: ":not(.no-exportar)"
                    }
                },
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fas fa-file-pdf"></i> ',
                    titleAttr: 'Export to PDF',
                    className: 'btn btn-danger',
                    exportOptions: {
                        columns: ":not(.no-exportar)"
                    }
                },
                /*{
                    extend:    'print',
                    text:      '<i class="fa fa-print"></i> ',
                    titleAttr: 'Imprimir',
                    className: 'btn btn-info'
                },*/
            ],
        
        initComplete: function () {
                // Apply the search
                this.api().columns().every( function () {
                    var that = this;
     
                    $( 'input', this.footer() ).on( 'keyup change clear', function () {
                        if ( that.search() !== this.value ) {
                            that
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );
        },
        "order": [ 0, 'desc' ],
        "scrollX": true,
        "scrollY": "50vh",
        "autoWidth": true,
        "scrollCollapse": true,
        "lengthMenu": [100,200,300,500],
        //"aoColumnDefs": [{ "bVisible": false, "aTargets": [ 19 ]}],
        "ajax":{            
            "url": "bd/crudReq.php", 
            "method": 'POST', //usamos el metodo POST
            "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
            "dataSrc":""
        },
        "columns":[
            {"data": "ReqID"},
            {"data": "Services"},
            {"data": "Typ"},
            {"data": "OutReg"},
            {"data": "InReg"},
            {"data": "Cont"},
            {"data": "Total"},
            {"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button><button class='btn btn-danger btn-sm btnBorrar'><i class='material-icons'>delete</i></button></div></div>"}
        ]
    });     
    
    $('.dataTables_length').addClass('bs-select');
    
    var fila; //captura la fila, para editar o eliminar
    //submit para el Alta y Actualización
    $('#formReq').submit(function(e){
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
        ReqID = $.trim($('#ReqID').val());
        Serv = $.trim($('#Serv').val());
        Typ = $.trim($('#Typ').val());
        OutReg = $.trim($('#OutReg').val());
        InReg = $.trim($('#InReg').val());
        
        if (opcion == 1){
            Cont = Serv;
        }
        
        let Max = Math.max(1, Cont);
        
        if (Serv >= Max){
            $.ajax({
              url: "bd/crudReq.php",
              type: "POST",
              datatype:"json",    
              data:  { ReqID:ReqID, Serv:Serv, Typ:Typ, OutReg:OutReg, InReg:InReg, opcion:opcion},    
              success: function(data) {
                tablaReq.ajax.reload(null, false);
               }
            });			        
            $('#modalCRUD').modal('hide');
        }else{
            alert ("Services must be greater than 0 or greater than equal to used services");
        }
    });
            
     
    
    //para limpiar los campos antes de dar de Alta una Persona
    $("#btnNuevo").click(function(){
        $("#ReqID").prop("disabled", false);
        LoadRegion();
        $('#OutReg').append('<option value="" disabled selected >- Select -</option>');
        $('#InReg').append('<option value="" disabled selected >- Select -</option>');
        opcion = 1; //alta
        $("#formReq").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("New Requisition");
        $('#modalCRUD').modal('show');	    
    });
    
    //Editar       
    $(document).on("click", ".btnEditar", function(){
        LoadRegion();
        opcion = 2;//editar
        fila = $(this).closest("tr");	        
        ReqID = parseFloat(fila.find('td:eq(0)').text()); //capturo el ID
        Serv = parseInt(fila.find('td:eq(1)').text());
        Cont = parseInt(fila.find('td:eq(5)').text());
        Typ = fila.find('td:eq(2)').text();
        $("#ReqID").prop("disabled", true);
        $("#ReqID").val(ReqID);
        $("#Serv").val(Serv);
        $("#Typ").val(Typ);
        $('#OutReg').append('<option class="form-control selected">' + fila.find('td:eq(3)').text() + '</option>');
        $('#InReg').append('<option class="form-control selected">' + fila.find('td:eq(4)').text() + '</option>');
        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $(".modal-title").text("Edit Requisition");		
        $('#modalCRUD').modal('show');		   
    });
    
    //Borrar
    $(document).on("click", ".btnBorrar", function(){
        fila = $(this);           
        ReqID = $(this).closest('tr').find('td:eq(0)').text();
        ReqUsed = $(this).closest('tr').find('td:eq(5)').text();
        ReqAll = $(this).closest('tr').find('td:eq(1)').text();
        if (ReqUsed > 0){
            opcion = 9; //mandar req a cero        
            var respuesta = confirm("¿Está seguro que deseas deshabilitar el registro "+ReqID+"?");                
            if (respuesta) {            
                $.ajax({
                url: "bd/crudReq.php",
                type: "POST",
                datatype:"json",    
                data:  {opcion:opcion, ReqID:ReqID, ReqAll:ReqAll},    
                success: function() {
                    tablaReq.ajax.reload(null, false); //tablaInv.row(fila.parents('tr')).remove().draw();                  
                }
                });	
            }
        }else{
            opcion = 8; //eliminar        
            var respuesta = confirm("¿Está seguro que deseas eliminar el registro "+ReqID+"?");                
            if (respuesta) {            
                $.ajax({
                url: "bd/crudReq.php",
                type: "POST",
                datatype:"json",    
                data:  {opcion:opcion, ReqID:ReqID, ReqAll:ReqAll},    
                success: function() {
                    tablaReq.ajax.reload(null, false); //tablaInv.row(fila.parents('tr')).remove().draw();                  
                }
                });	
            }
        }
     });
         
    });
    
    //Cargar Region
    function LoadRegion(){
        $("#OutReg").empty();
        $("#InReg").empty();
        var opts = "";
        opcion = 5;
        $.ajax({
              url: "bd/crudDO.php",
              type: "POST",
              datatype:"json",
              data:  {opcion:opcion},    
              success: function(data){
                  opts = JSON.parse(data);
                  //$('#OutPlc').append('<option class="form-control" >- Select -</option>');
                  //$('#InReg').append('<option class="form-control" >- Select -</option>');
                  for (var i = 0; i< opts.length; i++){
                      $('#OutReg').append('<option value="' + opts[i].RegNam + '">' + opts[i].RegNam + '</option>');
                      $('#InReg').append('<option value="' + opts[i].RegNam + '">' + opts[i].RegNam + '</option>');
                  }
               },
            error: function(data) {
                alert('error');
            }
        });
    }
