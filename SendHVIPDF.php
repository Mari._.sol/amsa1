<?php

setlocale(LC_TIME, "spanish");

function lowercase($element)
{
    return "'".$element."'";
}
require __DIR__.'/bd/vendor/autoload.php';
include_once 'bd/conexion.php';
include_once 'funcionAWS.php';
include_once('bd/correo-model.php');
$objeto = new Conexion();
$conexion = $objeto->Conectar();
include_once 'fpdf/fpdf.php';

$cliid = (isset($_POST['planta'])) ? $_POST['planta'] : '';
$Lots = (isset($_POST['lotes'])) ? $_POST['lotes'] : '';
$usuario = (isset($_POST['usuario'])) ? $_POST['usuario'] : '';
$comentarios = (isset($_POST['comentarios'])) ? $_POST['comentarios'] : '';
$fechaload= date('Y-m-d h:i:s a', time());  

$correoamsa = (isset($_POST['correoamsa'])) ? $_POST['correoamsa'] : '';
$correocliente = (isset($_POST['correocliente'])) ? $_POST['correocliente'] : '';
$adjuntarexcel= (isset($_POST['adjuntarexcel'])) ? $_POST['adjuntarexcel'] : '';
$adjuntarpdf= (isset($_POST['adjuntarpdf'])) ? $_POST['adjuntarpdf'] : '';

$consulta = "SELECT Cli FROM amsadb1.Clients WHERE CliID = '$cliid';";
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$usercliente=$resultado->fetch();
$cliente=$usercliente['Cli'];

  //Traer email y contraseña de la bd
  $AuthRow = getAuth($usuario);
  $email = $AuthRow['email'];
  $pass = $AuthRow['passApp'];

  $array = explode(",",$Lots);


if ($adjuntarpdf == 1){
    class PDF extends FPDF
    {
      //Cabecera de página
      function Header()
      {
        $this->SetFont('Arial','B',10);
        $this->Image('img/logo1.png', 11, 10, 10, 10, 'PNG');
        $this->Cell(0,6, utf8_decode('AGROINDUSTRIAS UNIDAS DE MEXICO S.A. DE C.V.'),0,0,'C');
        $this->Ln(4);
        $this->SetFont('Arial','B',9);
        $this->Cell(0,7,'COTTON DIVISION', 0, 0,'C');
        $this->Ln();
        $this->SetFont('Arial','B', 9);
        $this->Cell(26,5, utf8_decode('Bales'),'TB',0,'C');
        $this->Cell(15,5, utf8_decode('Grd'),'TB',0,'C');
        $this->Cell(15,5, utf8_decode('Trs'),'TB',0,'C');
        $this->Cell(19,5, utf8_decode('Mic'),'TB',0,'C');
        $this->Cell(15,5, utf8_decode('Len'),'TB',0,'C');
        $this->Cell(17,5, utf8_decode('Str'),'TB',0,'C');
        $this->Cell(19,5, utf8_decode('Unf'),'TB',0,'C');
        $this->Cell(13,5, utf8_decode('Rd'),'TB',0,'C');
        $this->Cell(13,5, utf8_decode('b'),'TB',0,'C');
        $this->Cell(15,5, utf8_decode('Col'),'TB',0,'C');
        $this->Cell(15,5, utf8_decode('Tar'),'TB',0,'C');
        $this->Cell(15,5, utf8_decode('Tcnt'),'TB',0,'C');
        $this->Cell(13,5, utf8_decode('Sfi'),'TB',0,'C');
        $this->Cell(19,5, utf8_decode('Elg'),'TB',0,'C');
        $this->Cell(17,5, utf8_decode('Mat'),'TB',0,'C');
        $this->Cell(15,5, utf8_decode('Mst'),'TB',0,'C');
        $this->Ln();
      }

      //Pie de página
      function Footer()
      {
          // Position at 1.5 cm from bottom
          $this->SetY(-15);
          // Arial italic 8
          $this->SetFont('Arial','I',8);
          // Page number
          $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
      }
    }


    /*$pdf = new PDF();
    $pdf->AliasNbPages();
    $pdf->AddPage('Landscape','Letter');
    $pdf->SetTitle("HVI ".$Cli);*/



    $pdf = new PDF();


        
    //Consultas preparadas

    $cols = ['Bal', 'Col1_Mod as Col1', 'Tgrd_Mod as Tgrd', 'Mic_Mod as Mic', 'Len_Mod as Len', 'Str_Mod as Str', 'Unf_Mod as Unf', 'Rd_Mod as Rd', 'b_Mod as b', 'Tar_Mod as Tar', 'Col2_Mod as Col2', 'Tcnt_Mod as Tcnt', 'Sfi_Mod as Sfi', 'Elg_Mod as Elg', 'Mat_Mod as Mat', 'Mst_Mod as Mst'];


    $placeholders = implode(",", array_fill(0, count($cols), "?"));

    $consulta = "SELECT " . implode(",", $cols) . " FROM Bales WHERE Lot = ? ORDER BY Bal ASC";
    $consultaMedia = "SELECT count(Bal) as SumBal, avg(Mic_Mod) as avgMic, avg(Tgrd_Mod) as avgTgrd, avg(Tar_Mod) as avgTar, avg(Tcnt_Mod) as avgTcnt, avg(Unf_Mod) as avgUnf, avg(Len_Mod) as avgLen, avg(Sfi_Mod) as avgSfi, avg(Str_Mod) as avgStr, avg(Elg_Mod) as avgElg, avg(Mat_Mod) as avgMat, avg(Rd_Mod) as avgRd, avg(b_Mod) as avgb FROM Bales WHERE Lot = ?";


    $stmt = $conexion->prepare($consulta);
    $stmtMedia = $conexion->prepare($consultaMedia);

    foreach ($array as $dato){ //for ($i= 0; $i < array_count_values($array); $i++){ //

    $pdf->AliasNbPages();
    $pdf->AddPage('Landscape','Letter');
    $pdf->SetTitle("HVI ".$cliente);

    $stmt->execute([$dato]);
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
    $stmtMedia->execute([$dato]);
    $dataAvg = $stmtMedia->fetchAll(PDO::FETCH_ASSOC);


    $pdf->Cell(26,4, utf8_decode($dato),'TB',0,'C');
    $pdf->Cell(0,4, utf8_decode(''),'TB',0,'C');

    foreach ($data as $row) { //foreach($data as $row){
        $pdf->Ln();
        $pdf->SetFont('Arial','', 9);
        $pdf->Cell(26,3,($row['Bal']),0,0,'C');
        $pdf->Cell(15,3, ($row['Col1']),0,0,'C');
        $pdf->Cell(15,3, ($row['Tgrd']),0,0,'C');
        $pdf->Cell(19,3, (number_format(Round($row['Mic'],1),1)),0,0,'C');
        $pdf->Cell(15,3, (number_format(Round($row['Len'],2),2)),0,0,'C');
        $pdf->Cell(17,3, (number_format(Round($row['Str'],1),1)),0,0,'C');
        $pdf->Cell(19,3, (number_format(Round($row['Unf'],1),1)),0,0,'C');
        $pdf->Cell(13,3, (number_format(Round($row['Rd'],1),1)),0,0,'C');
        $pdf->Cell(13,3, (number_format(Round($row['b'],1),1)),0,0,'C');
        $pdf->Cell(15,3, ($row['Col1']."-".$row['Col2']),0,0,'C');
        $pdf->Cell(15,3, (number_format(Round($row['Tar'],2),2)),0,0,'C');
        $pdf->Cell(15,3, ($row['Tcnt']),0,0,'C');
        $pdf->Cell(13,3, (number_format(Round($row['Sfi'],1),1)),0,0,'C');
        $pdf->Cell(19,3, (number_format(Round($row['Elg'],1),1)),0,0,'C');
        $pdf->Cell(17,3, (number_format(Round($row['Mat'],2),2)),0,0,'C');
        $pdf->Cell(15,3, (number_format(Round($row['Mst'],1),1)),0,0,'C');
    }
    foreach ($dataAvg as $rowAvg) {
        $pdf->Ln();
        $pdf->SetFont('Arial','B', 9);
        $pdf->Cell(26,4, utf8_decode($rowAvg['SumBal']." Bales"),'T',0,'C');
        $pdf->Cell(15,4, utf8_decode(''),'T',0,'C');
        $pdf->Cell(15,4, utf8_decode(Round($rowAvg['avgTgrd'],0)),'T',0,'C');
        $pdf->Cell(19,4, utf8_decode(number_format(Round($rowAvg['avgMic'],2),2)),'T',0,'C');
        $pdf->Cell(15,4, utf8_decode(number_format(Round($rowAvg['avgLen'],2),2)),'T',0,'C');
        $pdf->Cell(17,4, utf8_decode(number_format(Round($rowAvg['avgStr'],2),2)),'T',0,'C');
        $pdf->Cell(19,4, utf8_decode(number_format(Round($rowAvg['avgUnf'],2),2)),'T',0,'C');
        $pdf->Cell(13,4, utf8_decode(number_format(Round($rowAvg['avgRd'],2),2)),'T',0,'C');
        $pdf->Cell(13,4, utf8_decode(number_format(Round($rowAvg['avgb'],2),2)),'T',0,'C');
        $pdf->Cell(15,4, utf8_decode(''),'T',0,'C');
        $pdf->Cell(15,4, utf8_decode(number_format(Round($rowAvg['avgTar'],2),2)),'T',0,'C');
        $pdf->Cell(15,4, utf8_decode(number_format(Round($rowAvg['avgTcnt'],2),2)),'T',0,'C');
        $pdf->Cell(13,4, utf8_decode(number_format(Round($rowAvg['avgSfi'],2),2)),'T',0,'C');
        $pdf->Cell(19,4, utf8_decode(number_format(Round($rowAvg['avgElg'],2),2)),'T',0,'C');
        $pdf->Cell(17,4, utf8_decode(number_format(Round($rowAvg['avgMat'],2),2)),'T',0,'C');
        $pdf->Cell(15,4, utf8_decode(''),'T',0,'C');
    }

    }

    $token = generatoken();

    $fileHVIPDF="HVI-".$cliente."-".date('d-M-Y').'-'.$token.".pdf";
    $pdf->Output('F', $fileHVIPDF);
  }

  use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
  use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill};
  require 'bd/vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
  require 'bd/vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
  require 'bd/vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
  require 'bd/vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
  require 'bd/vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
  //Para scribir xlsx
  use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if ($adjuntarexcel==1){
    // Para usar la phpSpreadsheet llamamos a autoload
    //require './vendor/autoload.php';
   

    //Define the filename with current date
    $fileName = "HVI ".$cliente."-".date('d-M-Y')."-".$token.".xlsx";

    $excel = new Spreadsheet();
    $hojaActiva = $excel->getActiveSheet();
    $hojaActiva->setTitle("Hoja 1");


    $hojaActiva->getColumnDimension('A')->setWidth(8);
    $hojaActiva->setCellValue('A1','Lot');
    $hojaActiva->getColumnDimension('B')->setWidth(9);
    $hojaActiva->setCellValue('B1','Bales');
    $hojaActiva->getColumnDimension('C')->setWidth(7);
    $hojaActiva->setCellValue('C1','Grd');
    $hojaActiva->getColumnDimension('D')->setWidth(7);
    $hojaActiva->setCellValue('D1','Trs');
    $hojaActiva->getColumnDimension('E')->setWidth(7);
    $hojaActiva->setCellValue('E1','Mic');
    $hojaActiva->getColumnDimension('F')->setWidth(7);
    $hojaActiva->setCellValue('F1','Len');
    $hojaActiva->getColumnDimension('G')->setWidth(7);
    $hojaActiva->setCellValue('G1','Str');
    $hojaActiva->getColumnDimension('H')->setWidth(7);
    $hojaActiva->setCellValue('H1','Unf');
    $hojaActiva->getColumnDimension('I')->setWidth(7);
    $hojaActiva->setCellValue('I1','Rd');
    $hojaActiva->getColumnDimension('J')->setWidth(7);
    $hojaActiva->setCellValue('J1','b');
    $hojaActiva->getColumnDimension('K')->setWidth(7);
    $hojaActiva->setCellValue('K1','Col');
    $hojaActiva->getColumnDimension('L')->setWidth(7);
    $hojaActiva->setCellValue('L1','Tar');
    $hojaActiva->getColumnDimension('M')->setWidth(7);
    $hojaActiva->setCellValue('M1','Tcnt');
    $hojaActiva->getColumnDimension('N')->setWidth(7);
    $hojaActiva->setCellValue('N1','Sfi');
    $hojaActiva->getColumnDimension('O')->setWidth(7);
    $hojaActiva->setCellValue('O1','Elg');
    $hojaActiva->getColumnDimension('P')->setWidth(7);
    $hojaActiva->setCellValue('P1','Mat');
    $hojaActiva->getColumnDimension('Q')->setWidth(7);
    $hojaActiva->setCellValue('Q1','Mst');

    $fila = 2;

    $today = new DateTime();//DateTime("2022-04-06"); //echo $today->format('Y-m-d');


    foreach ($array as $dato){


            $query= "SELECT Bal, Col1_Mod as Col1, Tgrd_Mod as Tgrd, Mic_Mod as Mic, Len_Mod as Len, Str_Mod as Str, Unf_Mod as Unf, Rd_Mod as Rd, b_Mod as b, Tar_Mod as Tar, Col2_Mod as Col2, Tcnt_Mod as Tcnt, Sfi_Mod as Sfi, Elg_Mod as Elg, Mat_Mod as Mat, Mst_Mod as Mst, Lot FROM Bales WHERE Lot = '$dato'  ORDER BY Bal ASC;";

            $result = $conexion->prepare($query);
            $result->execute();
            $siexiste=0; //Para verificar que hayan datos

            //promedios Lotes
            $consulta = "SELECT count(Bal) as SumBal, avg(Mic_Mod) as avgMic, avg(Tgrd_Mod) as avgTgrd, avg(Tar_Mod) as avgTar, avg(Tcnt_Mod) as avgTcnt, avg(Unf_Mod) as avgUnf, avg(Len_Mod) as avgLen, avg(Sfi_Mod) as avgSfi, avg(Str_Mod) as avgStr, avg(Elg_Mod) as avgElg, avg(Mat_Mod) as avgMat, avg(Rd_Mod) as avgRd, avg(b_Mod) as avgb FROM Bales WHERE Lot = '$dato';"; 
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
        


        while($row = $result->fetch(PDO::FETCH_ASSOC)){

            $style = $hojaActiva->getStyle('A'. $fila);
            $style->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
            $hojaActiva->getCell('A'. $fila)->setValueExplicit($row['Lot'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);


          // $hojaActiva->setCellValue('A' . $fila,$row['Lot']);
            $hojaActiva->setCellValue('B' . $fila,$row['Bal']);
            $hojaActiva->setCellValue('C' . $fila,$row['Col1']);
            $hojaActiva->setCellValue('D' . $fila,$row['Tgrd']);
            $hojaActiva->setCellValue('E' . $fila,number_format(Round($row['Mic'],1),1));
            $hojaActiva->setCellValue('F' . $fila,number_format(Round($row['Len'],2),2));
            $hojaActiva->setCellValue('G'. $fila,number_format(Round($row['Str'],1),1));
            $hojaActiva->setCellValue('H'. $fila,number_format(Round($row['Unf'],1),1));
            $hojaActiva->setCellValue('I'. $fila,number_format(Round($row['Rd'],1),1));
            $hojaActiva->setCellValue('J'. $fila,number_format(Round($row['b'],1),1));
            $hojaActiva->setCellValue('K'. $fila,$row['Col1']."-".$row['Col2']);
            $hojaActiva->setCellValue('L'. $fila,number_format(Round($row['Tar'],2),2));
            $hojaActiva->setCellValue('M'. $fila,$row['Tcnt']);
            $hojaActiva->setCellValue('N'. $fila,number_format(Round($row['Sfi'],1),1));
            $hojaActiva->setCellValue('O'. $fila,number_format(Round($row['Elg'],1),1));
            $hojaActiva->setCellValue('P'. $fila,number_format(Round($row['Mat'],2),2));
            $hojaActiva->setCellValue('Q' . $fila,number_format(Round($row['Mst'],1),1));

            $fila++;
        }


        while($row = $resultado->fetch(PDO::FETCH_ASSOC)){
            $hojaActiva->setCellValue('B' . $fila,$row['SumBal']);
            $hojaActiva->setCellValue('D' . $fila,Round($row['avgTgrd'],0));
            $hojaActiva->setCellValue('E' . $fila,number_format(Round($row['avgMic'],2),2));
            $hojaActiva->setCellValue('F' . $fila,number_format(Round($row['avgLen'],2),2));
            $hojaActiva->setCellValue('G'. $fila,number_format(Round($row['avgStr'],2),2));
            $hojaActiva->setCellValue('H'. $fila,number_format(Round($row['avgUnf'],2),2));
            $hojaActiva->setCellValue('I'. $fila,number_format(Round($row['avgRd'],2),2));
            $hojaActiva->setCellValue('J'. $fila,number_format(Round($row['avgb'],2),2));
            $hojaActiva->setCellValue('L'. $fila,number_format(Round($row['avgTar'],2),2));
            $hojaActiva->setCellValue('M'. $fila,number_format(Round($row['avgTcnt'],2),2));
            $hojaActiva->setCellValue('N'. $fila,number_format(Round($row['avgSfi'],2),2));
            $hojaActiva->setCellValue('O'. $fila,number_format(Round($row['avgElg'],2),2));
            $hojaActiva->setCellValue('P'. $fila,number_format(Round($row['avgMat'],2),2));
            
            $fila++;
        }
    }


    $hojaActiva->getStyle('G2:J'.$fila)->getNumberFormat()->setFormatCode('#,##0.0');
    $hojaActiva->getStyle('N2:O'.$fila)->getNumberFormat()->setFormatCode('#,##0.0');
    $hojaActiva->getStyle('Q2:Q'.$fila)->getNumberFormat()->setFormatCode('#,##0.0');

    $hojaActiva->getStyle('F2:F'.$fila)->getNumberFormat()->setFormatCode('#,##0.00');
    $hojaActiva->getStyle('P2:P'.$fila)->getNumberFormat()->setFormatCode('#,##0.00');

    // redirect output to client browser
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$fileName.'"');
    header('Cache-Control: max-age=0');

    //$path = '../files/Listado_de_Pacas.xlsx';
    $writer = IOFactory::createWriter($excel, 'Xlsx');
    $writer->save($fileName);

}


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

require 'bd/vendor/phpmailer/phpmailer/src/PHPMailer.php';
require 'bd/vendor/phpmailer/phpmailer/src/SMTP.php';
require 'bd/vendor/autoload.php';


//$email = "josue.reyes@ecomtrading.com";
//$pass = "ltawuhsytbiroant";

$lotes_string=strtoupper($Lots);
$lotes_string=str_replace(",","','", $lotes_string);

$query = "Select Count(Bal) as Qty,Lot from amsadb1.Bales where Lot IN ('".$lotes_string."') Group by Lot order by Lot;";
$result = $conexion->prepare($query);
$result->execute();
if(!empty($result)){
    $lotesmail = "";
    foreach ($result as $lt) {
      $lotesmail.= $lt['Lot'].' - '.$lt['Qty'].' '.' bc'."<br>";
    }
}
else{
    $lotesmail="";
}


// Intancia de PHPMailer
$mail                = new PHPMailer();
// Es necesario para poder usar un servidor SMTP como gmail
$mail->isSMTP();
// Si estamos en desarrollo podemos utilizar esta propiedad para ver mensajes de error
//SMTP::DEBUG_OFF    = off (for production use) 0
//SMTP::DEBUG_CLIENT = client messages 1 
//SMTP::DEBUG_SERVER = client and server messages 2
$mail->SMTPDebug     = 0; //SMTP::DEBUG_SERVER;
//Set the hostname of the mail server
$mail->Host          = 'smtp.gmail.com';
$mail->Port          = 465; // 465 o 587
// Propiedad para establecer la seguridad de encripción de la comunicación
$mail->SMTPSecure    = PHPMailer::ENCRYPTION_SMTPS; // tls o ssl para gmail obligado
// Para activar la autenticación smtp del servidor
$mail->SMTPAuth      = true;
//$mail->SMTPAuthTLS      = false;
// Credenciales de la cuenta
$mail->Username     = $email;
$mail->Password     = $pass;
// Quien envía este mensaje
$mail->setFrom($email, $usuario);
// Si queremos una dirección de respuesta
//$mail->addReplyTo('replyto@panchos.com', 'Pancho Doe');
// Destinatario
$mail->addAddress("josue.reyes@ecomtrading.com"); //(acencion.pani@ecomtrading.com);
//$mail->addAddress("martin.romero@ecomtrading.com"); 
//$mail->addCC("christian.becerra@ecomtrading.com ");


if($correoamsa !=""){
    $correos= array_unique($correos = explode(",", $correoamsa) , SORT_STRING);
      $n = 0;
      while ($n < count($correos)) {
        $mail->addAddress($correos[$n]);
        $n++;
      }
    
    }
    
    
    
    
if ($correocliente !=""){
    $correocli= array_unique($correocli = explode(",", $correocliente) , SORT_STRING);
      $j = 0;
      while ($j < count( $correocli)) {
        $mail->addAddress($correocli[$j]);
        $j++;
      }
}

$asunto = 'HVI '.$cliente.' '.date('d-M-Y');
  // Asunto del correo
  $mail->Subject = $asunto;
  // Contenido
  $mail->IsHTML(true);
  $mail->CharSet = 'UTF-8';

$complemento="<b>Comentarios:</b> ".$comentarios;
$linkexcel = '';
$linkpdf = '';
if ($adjuntarpdf == 1){
  $filepdfbucket = str_replace(' ', '+', $fileHVIPDF);
  $linkpdf='<a href="https://hvi-clientes.s3.amazonaws.com/'.$filepdfbucket.'" title="Descargar PDF HVI"> <img src="https://img.icons8.com/fluency/48/pdf.png" alt="Descargar PDF de HVI" width="50" height="50" /></a>';      

}
if ($adjuntarexcel == 1){
  $fileexclebucket = str_replace(' ', '+', $fileName);
  $linkexcel='<a href="https://hvi-clientes.s3.amazonaws.com/'.$fileexclebucket.'" title="Descargar Excel HVI"><img src="https://img.icons8.com/color/48/ms-excel.png" alt="Descargar Excel de HVI" width="50" height="50" /></a>'; 
}
  $bodyy ='
  <style>
  table, th, td, th{
    border: 1px solid black;
  }
  </style>
    
    <p>Buen día<br><br>   
       
       Se comparten los archivos HVI de los siguientes lotes:<br>
       <dd><dd><dd>'.$lotesmail. '</dd></dd></dd><br>'  
       .$complemento.'<br><br>'.
       $linkexcel.' '.$linkpdf.'
       
       <br><br>
       
    ';  
    
    
    $mail->Body    = $bodyy;

    //$mail->addAttachment($fileName);
    //$mail->addAttachment($fileHVIPDF);
    if ($adjuntarexcel==1){
    subeHVIcompleto($fileName,$fileName);
    }
    if ($adjuntarpdf == 1){
    subeHVIcompleto($fileHVIPDF,$fileHVIPDF);
    }

    $mail->send();

    /*unlink($fileName);
    unlink($fileHVIPDF);*/

    

$conexion=null;



//FUNCIONES 

function generatoken($length = 10) {
    // Genera bytes aleatorios y conviértelos a hexadecimal
    $token = bin2hex(random_bytes($length / 2));
    
    // Si la longitud es impar, agregar un carácter adicional
    if ($length % 2 != 0) {
        $token .= bin2hex(random_bytes(1))[0];
    }
    
    return substr($token, 0, $length);
}

?>