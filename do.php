<?php

session_start();
header("Cache-Control: no-cache, must-revalidate"); //para borrar cache
if (!isset($_SESSION['username'])) :
    include_once('index.php');
else :
    $Typ_DO = $_SESSION['Typ_DO'];
    $permiso = $_SESSION['Admin'];
    $filtersSes = $_SESSION['Filters'];
    $rates=$_SESSION['ViewRates'];
    include_once('bd/correo-model.php');
    $regiones = getRegiones();
    $clients = getClients();
    $_SESSION['Filters'] = array(
                                //filtros para trucks
                                "idGlobal" => "",
                                "asLots" => "",
                                "DepArrDate" => "",
                                //filtros para DO
                                "typeDO" => "",
                                "clientsDO" => "",
                                "FDO" => "",
                                //filtros para trucks y DO
                                "muestras" => 200,
                                "regionfil" => "",
                                "DepRegFil" => "",
                                "ArrRegFil" => "",
                                "fromdate" => "",
                                "todate" => "" 
                                );
    foreach($regiones as $reg){
        $regs[] = $reg['RegNam'];
    }
    foreach($clients as $client){
        $clientes[] = $client['Cli'];
    }
?>

    <!doctype html>
    <html lang="es en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="img/ecom.png" />
        <title>Delivery Order</title>

        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="../portalLogistica/assets/bootstrap/css/bootstrap.min.css">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="../portalLogistica/main.css">
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>

    <body>
        <!-- Inicio scripts -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>

        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <!-- librerias necesarias para finalizar sesion por inactividad -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
        <!-- Scrip para finalizar sesion por inactividad -->
        <script type="text/javascript" src="timer.js"></script>


        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>

        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
        
        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>

        <!-- Multiselect en filtro-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
        <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>

        <?php if ($Typ_DO == 1 ) { ?>
            <script type="text/javascript" src="mainDO.js"></script>
        <?php } else { ?>
            <script type="text/javascript" src="mainDOR.js"></script>
        <?php } ?>
      

        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>

                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <li><a class="dropdown-item" href="trucks.php">Trucks</a></li>
                        <li><a class="dropdown-item" href="lots.php">Lots</a></li>
                        <?php if ($_SESSION['username'] == 'ISELA LARA' || $_SESSION['username'] == 'DARIO ROMERO' || $_SESSION['username'] == 'CESAR ARMAS' || $_SESSION['username'] == 'LUIS ANGEL RENDON TARIN' || $_SESSION['username'] == 'JOSE RENDON'){?>
                        <li><a class="dropdown-item" href="bales.php">Bales</a></li>
                        <?php } ?>
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="#">Delivery Order</a></li>
                        <li><a class="dropdown-item" href="adjustments.php">Adjustments</a></li>
                        <li><a class="dropdown-item" href="inv.php">Invoice</a></li>
                        <li><a class="dropdown-item" href="export.php">Containers</a></li>
                        <li><a class="dropdown-item" href="clients.php">Clients</a></li>
                        <li><a class="dropdown-item" href="routes.php">Routes</a></li>
                        <li><a class="dropdown-item" href="requisition.php">Requisition</a></li>
                        <li><a class="dropdown-item" href="CMS.php">CMS</a></li>
                        <?php if ($permiso == 1){?>
                        <li><a class="dropdown-item" href="adminusers.php">User Management</a></li>
                        <li><a class="dropdown-item" href="adminproveed.php">Proveed Management</a></li>
                        <?php } ?>                        
                        <?php if ($rates == 1){?>
                        <li><a class="dropdown-item" href="Rates.php">Rates</a></li>
                        <?php } ?>
                        
                    </ul>
                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-2" href="main.php">
                    <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <p>/ Delivery Order</p>
                </div>
                <div class="container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                    <!-- Botones -->
                    <?php if ($Typ_DO == 1) : ?>
                        <button id="btnNuevo" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Add"><i class="bi bi-plus-square"></i></button>
                        <button id="btnSplit" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Split a Lot"><i class="bi bi-scissors"></i></button>
                    <?php endif ?>
                </div>
                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                    <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"><?php echo $_SESSION['username']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->

        <!-- Empieza contenido de las tablas -->
        <!-- Inicio de la tabla -->
        <div class="card card-body" style="opacity:100%;">
            <div class="table-responsive" style="opacity:100%;">
                <table id="tablaOrdenes" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;">
                    <thead style="background-color: #65ac7c;" style="opacity:100%;">
                        <tr>
                            <th class="th-sm">Date</th>
                            <th class="th-sm">Year</th>
                            <th class="th-sm">Type</th>
                            <th class="th-sm">Delivery Order</th>
                            <th class="th-sm">Requester</th>
                            <th class="th-sm">Gin</th>
                            <th class="th-sm">Departure Region</th>
                            <th class="th-sm">Arrival Region</th>
                            <th class="th-sm">Contract</th>
                            <th class="th-sm">Client</th>
                            <th class="th-sm">Crossing Date</th>
                            <th class="th-sm">Purchase No.</th>
                            <th class="th-sm">Total Quantity</th>
<!--                            <th class="th-sm">Adjustments</th>
                            <th class="th-sm">Total Quantity</th> -->
                            <th class="th-sm">To Deliver</th>
                            <!-- <th class="th-sm">Delivered</th> -->
                            <th class="th-sm">In Transit</th>
                            <th class="th-sm">Received</th>
                            <th class="th-sm">Rejected</th>
<!--                             <th class="th-sm">To Invoice</th>
                            <th class="th-sm">Invoiced</th> -->
                            <th class="th-sm">Associated</th>
                            <th class="th-sm">To Associate</th>
                            <th class="th-sm">Comments</th>
                            <th class="no-exportar" class="th-sm">ID</th>
                            <th class="no-exportar" class="th-sm">PDF</th>
                            <th class="no-exportar" class="th-sm">Certificados de Origen</th>
                            <th class="no-exportar" class="th-sm">Pacas Certificadas</th>
                            <th class="no-exportar" class="th-sm">Muestras</th>
                            <th class="no-exportar"></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="th-sm">Date</th>
                            <th class="th-sm">Year</th>
                            <th class="th-sm">Type</th>
                            <th class="th-sm">Delivery Order</th>
                            <th class="th-sm">Requester</th>
                            <th class="th-sm">Gin</th>
                            <th class="th-sm">Departure Region</th>
                            <th class="th-sm">Arrival Region</th>
                            <th class="th-sm">Contract</th>
                            <th class="th-sm">Client</th>
                            <th class="th-sm">Crossing Date</th>
                            <th class="th-sm">Purchase No.</th>
                            <th class="th-sm">Total Quantity</th>
                            <!--  <th class="th-sm">Adjustments</th>
                            <th class="th-sm">Total Quantity</th>-->
                            <th class="th-sm">To Deliver</th>
                            <!-- <th class="th-sm">Delivered</th> -->
                            <th class="th-sm">In Transit</th>
                            <th class="th-sm">Received</th>
                            <th class="th-sm">Rejected</th>
                            <!--  <th class="th-sm">To Invoice</th>
                            <th class="th-sm">Invoiced</th> -->
                            <th class="th-sm">Associated</th>
                            <th class="th-sm">To Associate</th>
                            <th class="th-sm">Comments</th>
                            <th class="th-sm">ID</th>
                            <th class="th-sm">PDF</th>
                            <th class="no-exportar" class="th-sm">Certificados de Origen</th>
                            <th class="no-exportar" class="th-sm">Pacas Certificadas</th>
                            <th class="no-exportar" class="th-sm">Muestras</th>
                            <th class="th-sm"></th>
                        </tr>
                    </tfoot>
                </table>
            </div>

        </div>

        <!--Modal para CRUD-->
        <div class="modal fade" id="modalCRUD" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formDO">
                        <div class="modal-body">
                            <div class="row">
                                <div class='col-lg-3' id="NewDO">
                                    <div class='form-group'>
                                        <button type="button" id="btnDONumber" class="btn btn-sm btn-dark">Generate DO Number</button>
                                        <!--<button type="button" id="btnDelLot" class="btn btn-sm btn-dark">Remove Lot</button>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Delivery Order<font size=2>*</font></label>
                                        <input type="text" class="form-control form-control-sm" id="DOrd" required readonly>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Quantity<font size=2>*</font></label>
                                        <input type="text" class="form-control form-control-sm" id="Qty" pattern="[0-9]+" required>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Type<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="Typ" name="Typ" required>
                                            <option value="" disabled selected>- Select -</option>
                                            <option value="CON">CON</option>
                                            <option value="DOM">DOM</option>
                                            <option value="EXP">EXP</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Year<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="Year" name="Year" required>
                                            <option value="2022">2022</option>
                                            <option value="2023" selected>2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Date<font size=2>*</font></label>
                                        <input type="date" class="form-control form-control-sm" id="Dat" value="<?php echo date("Y-m-d"); ?>" required readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label fro="" class="col-form-label">Departure Region<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="OutPlc" required>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label fro="" class="col-form-label">Gin</label>
                                        <select class="form-control form-control-sm" id="Gin"></select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Arrival Region<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="InReg" required></select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Client<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="InPlc" required></select>
                                    </div>
                                </div>
                            </div>
                            <div id="USA" style="display: none">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Crossing Date</label>
                                            <input type="date" class="form-control form-control-sm" id="Crss" value="<?php echo date("Y-m-d"); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Purchase No.</label>
                                            <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="PurNo">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Requester<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="Rqs" name="Rqs" required></select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Contract<font size=2>*</font></label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="Ctc" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Comments</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="DoCmt">
                                    </div>
                                </div>
                            </div>
                            <div class="row" id='centrar'>

                                <div class="col-lg-3">
                                    <div class="form-group" >
                                        <label for="" class="col-form-label">Pacas Certificadas</label>
                                        <br>
                                        <input class="form-check-input" type="checkbox" id="pacascertificadas" >
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group" >
                                        <label for="" class="col-form-label">Certificado de Origen</label>
                                        <br>
                                        <input class="form-check-input" type="checkbox" id="certificado" >
                                    </div>
                                </div>

                                <div class="col-lg-2">
                                    <div class="form-group" >
                                        <label for="" class="col-form-label">Muestras</label>
                                        <br>
                                        <input class="form-check-input" type="checkbox" id="muestrasdo" >
                                    </div>
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">PDF Notes</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="PDF">
                                    </div>
                                </div>                               
                            </div>
                            

                       
                            <font size=2>*Required Fields</font>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" id="btnGuardar" class="btn btn-dark">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <!--Modal para Lotes disponibles-->
        <div class="modal fade" id="modalLots" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formDO-Lots">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label fro="" class="col-form-label">Delivery Order</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="DO" disabled>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label fro="" class="col-form-label">Departure Region</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="OutReg" disabled>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label fro="" class="col-form-label">Gin</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="OutGin" disabled>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label fro="" class="col-form-label">Crop</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="CrpDO" disabled>
                                    </div>
                                </div>
                                <div class="col-lg-2" style="display: none" id="colpacascert">
                                    <div class="form-group">
                                        <label fro="" class="col-form-label">Pacas Certificadas</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="pacascertaddlots" disabled>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label fro="" class="col-form-label">Scheduled Arrival Date</label>
                                        <input type="date" class="form-control form-control-sm" style="text-transform:uppercase;" id="ArrDte">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label fro="" class="col-form-label">Lots</label>
                                        <select class="form-control form-control-sm" name="Lotes[]" multiple="multiple" id="Lotes" size="4"></select>
                                    </div>
                                </div>
                                <div class='col-lg-2'>
                                    <!--<div class='form-group'>-->
                                    <center>
                                        <dl>
                                            <br><br>
                                            <dd><button type="button" id="btnAddLot" class="btn btn-sm btn-dark">Add Lot</button></dd>
                                            <dd><button type="button" id="btnDelLot" class="btn btn-sm btn-dark">Remove Lot</button></dd>
                                        </dl>
                                    </center>
                                    <!--</div>-->
                                </div>
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label fro="" class="col-form-label">Lots in DO</label>
                                        <select class="form-control form-control-sm" name="LotesDO[]" multiple="multiple" id="LotesDO" size="5">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Comments</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="DoCmtLot">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <!--<button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>-->
                            <button type="submit" id="btnGuardar" class="btn btn-dark">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        
                <!--Modal para Split de Lotes-->
        <div class="modal fade" id="SplitLots" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="Split-Lots">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label fro="" class="col-form-label">Location<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="LocSplit" required>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">
                                            <!--<input type="checkbox" name="InDO" id="InDO" checked>-->
                                            Delivery Order
                                        </label>
                                        <input type="text" class="form-control form-control-sm" name="DOSplit" id="DOSplit">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <br>
                                        <button type="button" id="btnFndLot" class="btn btn-sm btn-dark"><i class="material-icons">search</i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Lot - Quantity (BC)</label>
                                        <select class="form-control form-control-sm" id="LotsSplit" required>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label fro="" class="col-form-label">Inserts the partitions separated by a comma (E.g. 40,80)</label>
                                        <input type="text" class="form-control form-control-sm" id="Split" required>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="row">
                    <div class="col-lg-9">
                    <div class="form-group">
                    <label fro="" class="col-form-label">Inserts the partitions separated by a comma (for example 40,80)</label>
                    <input type="text" class="form-control form-control-sm" id="Split" required>
                    </div>
                    </div>
                </div>-->
                        </div>
                        <div class="modal-footer">
                            <!--<button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>-->
                            <button type="submit" id="btnGuardar" class="btn btn-dark">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


	<div class="modal hide fade" id="Correo" data-bs-backdrop="static" tabindex="-1" role="dialog" data-bs-keyboard="false" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-tittle">Preview email</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="correo" class="correo">
                        <div class="input-group mt-3">
                                <label class="input-group-text" for="extraEmails" >To:  </label>
                                <input type="text" placeholder="Separados por coma" class="form-control extraEmails" id="extraEmails">
                            </div>
                            <div class="input-group mt-3">
                                <label class="input-group-text" for="subject">Subject</label>
                                <input type="text" class="form-control subject" id="subject">
                                <div class="form-check form-switch ms-2 mt-2">
                                <!--<input class="form-check-input" type="checkbox" id="esMuestra">
                                <label class="form-check-label" for="esMuestra">¿Samples?</label>-->
                                </div>
                            </div>
                            <div class="input-group mt-3">
                            <label class="input-group-text" for="observ">Comments</label>
                                <input  type="textarea" class="form-control observ" id="observ" >
                            </div>
                            <div  class="input-group mt-3">
                            <label class="input-group-text" for="tablamue">Lots</label>
                                <div style="overflow-y: scroll; max-height: 13rem;" class="form-control tablamue" id="tablamue"></div>
                            </div>
                            <input type="text" class="form-control tablahtml" id="tablahtml" hidden >
                            <input type="text" class="form-control usuario" id="usuario" hidden>
                            <input type="text" class="form-control typeDO" id="typeDO" hidden>
                            <input type="text" class="form-control salSucu" id="salSucu" hidden>
                            <input type="text" class="form-control regOri" id="regOri" hidden>
                            <input type="text" class="form-control arrReg" id="arrReg" hidden>
                            <input type="text" class="form-control client" id="client" hidden>
                            <input type="text" class="form-control contra" id="contra" hidden>
                            <input type="text" class="form-control totalQty" id="totalQty" hidden>
                            <input type="text" class="form-control numDO" id="numDO" hidden>
                            
                            
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="lotess" id="lotess"></div>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button id="sendEmail" type="button" class="btn btn-primary sendEmail">Send Email</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- Modal para enviar listado de lotes -->
        <div class="modal hide fade" id="CorreoList" data-bs-backdrop="static" tabindex="-1" role="dialog" data-bs-keyboard="false" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-tittle">Preview email</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="correoList" class="correo">
                            <div class="input-group mt-3">
                                <label class="input-group-text" for="extraEmails" >Mails Origen:  </label>
                                <input type="text" placeholder="Separados por coma" class="form-control extraEmails" id="extraEmailsList">
                            </div>
                            <div class="input-group mt-3">
                                <label class="input-group-text" for="extraEmails" >Mails AMSA:  </label>
                                <input type="text" placeholder="Separados por coma" class="form-control extraEmails" id="CCList">
                            </div>
                            <div class="input-group mt-3">
                                <label class="input-group-text" for="subject">Subject</label>
                                <input type="text" class="form-control subject" id="subjectList">
                            </div>
                            <div class="input-group mt-3">
                            <label class="input-group-text" for="observ">Comments</label>
                                <input  type="textarea" class="form-control observ" id="observList" >
                            </div>
                            <div  class="input-group mt-3">
                            <label class="input-group-text" for="tablamue">Lots</label>
                                <div style="overflow-y: scroll; max-height: 13rem;" class="form-control tablamue" id="tablamueList"></div>
                            </div>
                            <input type="text" class="form-control tablahtml" id="tablahtmlList" hidden >
                            <input type="text" class="form-control usuario" id="usuarioList" hidden>
                            <input type="text" class="form-control typeDO" id="typeDOList" hidden>
                            <input type="text" class="form-control salSucu" id="salSucuList" hidden>
                            <input type="text" class="form-control regOri" id="regOriList" hidden>
                            <input type="text" class="form-control arrReg" id="arrRegList" hidden>
                            <input type="text" class="form-control client" id="clientList" hidden>
                            <input type="text" class="form-control contra" id="contraList" hidden>
                            <input type="text" class="form-control totalQty" id="totalQtyList" hidden>
                            <input type="text" class="form-control numDO" id="numDOList" hidden>
                        
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="lotess" id="lotess"></div>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button id="sendEmailList" type="button" class="btn btn-primary sendEmailList">Send Email</button>
                    </div>
                </div>
            </div>
        </div>

        
        <!-- Modal de filtros -->                

       <div class="modal hide fade" id="filtrarmodal" data-bs-backdrop="static" tabindex="-1" role="dialog" data-bs-keyboard="false" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-tittle">Filter table by</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <div class="input-group mb-3">
                            <label class="input-group-text" for="FDO">DO:</label>
                            <input type="text" class="form-control me-2" placeholder="Write DO" name="FilterDO" id="FilterDO">
                            <label class="input-group-text " for="TDO">Type</label>
                            <select class="form-select me-2" name="TDO" id="TDO">
                                <option value="" selected>Choose...</option>
                                <option value="CON">CON</option>
                                <option value="DOM">DOM</option>
                                <option value="EXP">EXP</option>
                            </select>   
                            <label class="input-group-text " for="clientsDO">Client</label>
                            <select class="form-select me-2" name="clientsDO" id="clientsDO">
                                <option value="" selected>Choose..</option>
                                <option value="ALL">All</option>
                                <?php $i = 0; while($i < count($clientes)): ?>
                                <option value="<?php echo $clientes[$i]; ?>"><?php echo $clientes[$i]; ?></option>
                                <?php $i++; endwhile; ?>
                            </select>
                            <label class="input-group-text " for="muestras">Last DO</label>
                            <select class="form-select me-2" name="muestras" id="muestras">
                                <option value="200" selected>200 (default)</option>
                                <option value="ALL">All</option>
                                <?php $x = 25; for($i=1; $i<7; $i++): ?>
                                    <option value="<?php echo $x*2; ?>"><?php echo $x*2; $x*=2;?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <!--Creation Date y last do sin div de columnas -->
                        <div class="input-group mb-3">
                            <label class="input-group-text " for="DepRegFil">Select Departure Region</label>
                            <div class='col-lg-7'>
                                <select class="form-select me-2" name="DepRegFil" id="DepRegFil" multiple placeholder="Choose...">
                                    <?php $i = 0; while($i < count($regs)): ?>
                                    <option value="<?php echo $regs[$i]; ?>"><?php echo $regs[$i]; ?></option>
                                    <?php $i++; endwhile; ?>
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <label class="input-group-text" for="ArrRegFil">Select Arrival Region</label>
                            <div class='col-lg-7'>
                                <select class="form-select me-2" id="ArrRegFil" name="ArrRegFil" multiple placeholder="Choose...">
                                <?php $i=0; while($i < count($regs)): ?> 
                                    <option value="<?php echo $regs[$i]; ?>"><?php echo $regs[$i]; ?></option>
                                <?php $i++; endwhile; ?>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class='col-lg-6'>
                                <div class="input-group mb-3">
                                    <label class="input-group-text" for="timeSelect">Creation Date: </label>
                                    <select class="form-select me-2" id="timeSelect" name="timeSelect">
                                        <option value="" selected >Choose...</option>
                                        <option value="7">Last week</option>
                                        <option value="14">Last 2 weeks</option>
                                        <option value="30">Last month</option>
                                        <option value="60">Last 2 months</option>
                                        <option value="120">Last 4 months</option>
                                        <option value="180">Last 6 months</option>
                                        <option value="365">Last 1 year </option>  
                                    </select>
                                </div>
                            </div>
                        </div>        
                        
                                    
                        <div class="input-group mb-3">
                            <label class="input-group-text" for="fromdate">From date:</label>
                            <input type="date" class="form-control me-2" name="fromdate" id="fromdate">
                            <label class="input-group-text" for="todate">To date:</label>
                            <input type="date" class="form-control me-2" name="todate" id="todate">
                        </div>

                    <div class="modal-footer">
                        <div style="height: 2.5em;" id="aviso"></div>
                    <button id="cer" type="button" class="btn btn-light" value="3" data-bs-dismiss="modal" >Close</button>
                        <button id="borrarFiltro" type="button" value="0" class="btn btn-danger Filtro" >Clear filters</button>
                        <button id="buscafiltro" type="button" value="1" class="btn btn-primary Filtro" disabled>Apply filters</button>
                    </div>
                </div>
            </div>
        </div>



        <!-- Termina contenido de las tablas -->



    </body>

    </html>
<?php
endif;
?>
