$(document).ready(function() {
    
    $('#tablaClients tfoot th').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        } );
    
    
    var opcion;
    opcion = 4;
    
    tablaCli = $('#tablaClients').DataTable({
        
            //resumen de total de lotes, total de pacas y suma de pesos 
   
        //para usar los botones   
            responsive: "true",
            dom: 'Brtilp',       
            buttons:[ 
                {
                    extend:    'excelHtml5',
                    text:      '<i class="fas fa-file-excel"></i> ',
                    titleAttr: 'Export to Excel',
                    className: 'btn btn-success',
                    exportOptions: {
                        columns: ":not(.no-exportar)"
                    }
                },
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fas fa-file-pdf"></i> ',
                    titleAttr: 'Export to PDF',
                    className: 'btn btn-danger',
                    exportOptions: {
                        columns: ":not(.no-exportar)"
                    }
                },
                /*{
                    extend:    'print',
                    text:      '<i class="fa fa-print"></i> ',
                    titleAttr: 'Imprimir',
                    className: 'btn btn-info'
                },*/
            ],
        
        initComplete: function () {
                // Apply the search
                this.api().columns().every( function () {
                    var that = this;
     
                    $( 'input', this.footer() ).on( 'keyup change clear', function () {
                        if ( that.search() !== this.value ) {
                            that
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );
        },
        "order": [ 0, 'asc' ],
        "scrollX": true,
        "scrollY": "50vh",
        "autoWidth": true,
        "scrollCollapse": true,
        "lengthMenu": [100,200,300,500],
        //"aoColumnDefs": [{ "bVisible": false, "aTargets": [ 19 ]}],
        fixedColumns:   {
                left: 0,
                right: 1
            },
        "ajax":{            
            "url": "bd/crudadminproveed.php", 
            "method": 'POST', //usamos el metodo POST
            "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
            "dataSrc":""
        },
        "columns":[
            {"data": "ProveedID"},
            {"data": "NameProveed"},
            {"data": "Region"},
            {"data": "email"},
            {"data": "Locked", className: "text-center"},            
            {"data": "GinID", className: "text-center"},

          


            {"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button><button class='btn btn-success btn-sm btnUnblock'><i class='material-icons'>lock_open</i></button></div></div>"}
        
        ],
        columnDefs : [
            { targets : [4],
              render : function (data, type, row) {
                return data == '1' ? 'Locked' : 'Active'
              }
            }
       ]

       

       
    });     

    muestra();  

    
    $('.dataTables_length').addClass('bs-select');
    
    var fila; //captura la fila, para editar o eliminar
    //submit para el Alta y Actualización
    $('#formproveed').submit(function(e){
       
        e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
        nameuser = $.trim($('#nameuser').val());    
        UserID = $.trim($('#UserID').val());
        region = $.trim($('#region').val());
        email = $.trim($('#email').val());
        gin = $.trim($('#gin').val());
                  
            $.ajax({
              url: "bd/crudadminproveed.php",
              type: "POST",
              datatype:"json",    
              data:  { opcion:opcion, nameuser:nameuser, UserID:UserID, region:region, email:email,gin:gin},    
              success: function(data) {
                tablaCli.ajax.reload(null, false);
                alert('Successful');
               },error: function(data) {
                alert('error');
            }
            });			        
        $('#modalCRUD').modal('hide');											     			
    });
            
     
    
    //para limpiar los campos antes de dar de Alta una Persona
    $("#btnNuevo").click(function(){
        opcion = 1; //alta           
        CliID=null;
        $("#formproveed").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("New User");
        $('#modalCRUD').modal('show');	    
    });

    //Editar        
    $(document).on("click", ".btnEditar", function(){
        opcion = 2;//editar
        fila = $(this).closest("tr");	        
        ProveedID = fila.find('td:eq(0)').text(); //capturo el ID
        proveedName = fila.find('td:eq(1)').text(); 		            
        region = fila.find('td:eq(2)').text();
        email = fila.find('td:eq(3)').text();
        //Cty = fila.find('td:eq(4)').text();
        gin = parseInt(fila.find('td:eq(5)').text());
        var data = $('#tablaClients').DataTable().row(fila).data(); //fila.find('td:eq(24)').text();
        Cde = data['Cde'];
        $("#UserID").val(ProveedID);
        $("#nameuser").val(proveedName);
        $("#region").val(region);
        $("#email").val(email);      
        $("#gin").val(gin);

        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $(".modal-title").text("Edit Client");		
        $('#modalCRUD').modal('show');		   
    });
    
    //DESBLOQUEAR USER 
    $(document).on("click", ".btnUnblock", function(){
        fila = $(this);           
        UserID = $(this).closest('tr').find('td:eq(0)').text();
        Estatuts = $(this).closest('tr').find('td:eq(4)').text();		
        opcion = 3; //Desbloquear usuario        
             
        if (Estatuts === "Locked") {            
            $.ajax({
              url: "bd/crudadminproveed.php",
              type: "POST",
              datatype:"json",    
              data:  {opcion:opcion, UserID:UserID},    
              success: function() {
                tablaCli.ajax.reload(null, false); //tablaInv.row(fila.parents('tr')).remove().draw();
                  alert('User unlocked');                  
               }
            });	
        }

        else  {            
            alert('this user is not locked ');	
        }
     });
});
    

function muestra (){
    tablaCli.column(4).nodes().each(function (node, index, dt) {
        if(table.cell(node).data() == '0'){
            console.log("encontrado");
            table.cell(node).data('100');
        }
        });
}

function UserCheck(){
    var userid = document.getElementById("UserID").value;
    
    var valuser= tablaCli.column(0).search(userid);
    var column = tablaCli .row(0).data();
    
    console.log(column[0]);
    
    // var lastElement = valuser.reverse()[0].countryid

    //$.each(valuser, function(index, val) {
        //  console.log(valuser);
    //});
    /*if (valuser===lastElement) {
        document.getElementById("userexist").innerHTML = "Este usuario y existe";
        $('#btnGuardar').prop('disabled', true);
    }else{
        document.getElementById("userexist").innerHTML = "";
        $('#btnGuardar').prop('disabled', false);
    }*/

}

$(document).on("click", "#ExportExcel", function(e){
    e.preventDefault(); 
    window.open("./bd/exportUltimaCon.php");
});

var choices;

function nameProv() {
    $.ajax({
        url: "bd/crudadminproveed.php",
        type: "POST",
        datatype: "json",
        data: { opcion: 6 },
        success: function (data) {
            try {
                $("#prov").empty();
                var opts = JSON.parse(data);
                for (var i = 0; i < opts.length; i++) {
                    $('#prov').append($('<option>').val(opts[i].ProveedID).text(opts[i].ProveedID));
                }
                ChoisesSelect(opts);
            } catch (error) {
                console.error("Error al procesar la respuesta del servidor:", error);
            }
        },
        error: function (data) {
            alert('error');
        }
    });
}

function ChoisesSelect(opts) {
    choices = new Choices('#prov', {
        removeItemButton: true,
        maxItemCount: 20
    });
}

//enviar correos 
$(document).ready(function() {
    $('#prov').on('change', function () {
        var selectedOptions = $(this).val();
        $('#btnSend').prop('disabled', !selectedOptions || selectedOptions.length === 0);
    });

    $("#mailReminder").click(function(){
        $("#mail").trigger("reset");
        $("#formMail").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Mail Reminder");
        $('#mail').modal('show');	   

        nameProv();
        
        $("#cancel, #close, #mailReminder").click(function () {
            if (choices) {
                choices.destroy();
                $('#btnSend').prop('disabled', true);
            }
        });
    });

    $("#btnSend").click(function (e) {
        e.preventDefault();
        asunto = $("#subject").val();
        prov = $("#prov").val();
        console.log(prov)
    
        $('#mail').modal('hide');
        $.ajax({
            url: "bd/MailReminder.php",
            type: "POST",
            datatype:"json",
            data:{
                asunto:asunto, prov:prov
            },    
            success: function(data){
                alert(data)
            }
        });
    });
});
