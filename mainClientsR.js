$(document).ready(function() {

$('#tablaClients tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );


var opcion;
opcion = 4;
    
tablaCli = $('#tablaClients').DataTable({
    
    //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Export to Excel',
				className: 'btn btn-success',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			{
				extend:    'pdfHtml5',
				text:      '<i class="fas fa-file-pdf"></i> ',
				titleAttr: 'Export to PDF',
                className: 'btn btn-danger',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
			},
			/*{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},*/
		],
    
    initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
    },
    "order": [ 1, 'asc' ],
    "scrollX": true,
    "scrollY": "50vh",
    "autoWidth": true,
    "scrollCollapse": true,
    "lengthMenu": [100,200,300,500],
    "aoColumnDefs": [{ "bVisible": false, "aTargets": [ 19 ]}],
    "ajax":{            
        "url": "bd/crudClients.php", 
        "method": 'POST', //usamos el metodo POST
        "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
        "dataSrc":""
    },
    "columns":[
        {"data": "CliID"},
        {"data": "Cli"},
        {"data": "BnName"},
	{"data": "RFC"},
        {"data": "Cty"},
        {"data": "State"},
        {"data": "Town"},
        {"data": "CP"},
        {"data": "Drctn"},
        {"data": "Ref"},
        {"data": "Ct1"},
        {"data": "Tel1"},
        {"data": "Ext1"},
        {"data": "Ct2"},
        {"data": "Tel2"},
        {"data": "Ext2"},
        {"data": "Ct3"},
        {"data": "Tel3"},
        {"data": "Ext3"},
        {"data" : "Cmt"},
        {"data" : "Cde"},
        {"data" : "ClientGrp"},
        {"data" : "Maps"}
        /*{"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button>"}*/
    ]
});
    
$('.dataTables_length').addClass('bs-select');

var fila; //captura la fila, para editar o eliminar
//submit para el Alta y ActualizaciÃ³n
$('#formExp').submit(function(e){
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la pÃ¡gina
    InvID = $.trim($('#InvID').val());    
    DO = $.trim($('#DO').val());
    CtcInv = $.trim($('#CtcInv').val());
    Lot = $.trim($('#Lot').val());
    Qty = $.trim($('#Qty').val());
    InvDat = $.trim($('#InvDat').val());    
    WghGss = $.trim($('#WghGss').val());
    WghNet = $.trim($('#WghNet').val());
    Amt = $.trim($('#Amt').val());
    Price = $.trim($('#Price').val());
    PosDat = $.trim($('#PosDat').val());
    InvSts = $.trim($('#Status').val());
    InvCmt = $.trim($('#InvCmt').val());                            
        $.ajax({
          url: "bd/crudExport.php",
          type: "POST",
          datatype:"json",    
          data:  { InvID:InvID, DO:DO, CtcInv:CtcInv, Lot:Lot, Qty:Qty, InvDat:InvDat, WghGss:WghGss, WghNet:WghNet, Amt:Amt, Price:Price, PosDat:PosDat, InvSts:InvSts, InvCmt:InvCmt, opcion:opcion},    
          success: function(data) {
            tablaExp.ajax.reload(null, false);
           }
        });			        
    $('#modalCRUD').modal('hide');											     			
});

//para limpiar los campos antes de dar de Alta una Persona
$("#btnNuevo").click(function(){
    opcion = 1; //alta           
    InvID=null;
    $("#formExp").trigger("reset");
    $(".modal-header").css( "background-color", "#17a2b8");
    $(".modal-header").css( "color", "white" );
    $(".modal-title").text("New Export");
    $('#modalCRUD').modal('show');	    
});

//Editar        
$(document).on("click", ".btnEditar", function(){
    opcion = 2;//editar
    fila = $(this).closest("tr");	        
    InvID = fila.find('td:eq(1)').text(); //capturo el ID		            
    DO = parseFloat(fila.find('td:eq(3)').text());
    CtcInv = fila.find('td:eq(2)').text();
    Lot = fila.find('td:eq(4)').text();
    Qty = parseFloat(fila.find('td:eq(5)').text());
    InvDat = fila.find('td:eq(0)').text();
    WghGss = parseFloat(fila.find('td:eq(7)').text());
    WghNet = parseFloat(fila.find('td:eq(9)').text());
    Price = parseFloat(fila.find('td:eq(10)').text());
    Amt = parseFloat(fila.find('td:eq(11)').text());
    PosDat = fila.find('td:eq(6)').text();
    InvSts = fila.find('td:eq(13)').text();
    InvCmt = fila.find('td:eq(12)').text();
    $("#InvID").val(InvID);
    $("#DO").val(DO);
    $("#CtcInv").val(CtcInv);
    $("#Lot").val(Lot);
    $("#Qty").val(Qty);
    $("#InvDat").val(InvDat);
    $("#WghGss").val(WghGss);
    $("#WghNet").val(WghNet);
    $("#Amt").val(Amt);
    $("#Price").val(Price);
    $("#PosDat").val(PosDat);
    $("#Status").val(InvSts);
    $("#InvCmt").val(InvCmt);
    $(".modal-header").css("background-color", "#007bff");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Edit Export");		
    $('#modalCRUD').modal('show');		   
});

//Borrar
$(document).on("click", ".btnBorrar", function(){
    fila = $(this);           
    InvID = $(this).closest('tr').find('td:eq(1)').text();		
    opcion = 3; //eliminar        
    var respuesta = confirm("Â¿EstÃ¡ seguro que deseas deshabilitar el registro "+InvID+"?");                
    if (respuesta) {            
        $.ajax({
          url: "bd/crudExport.php",
          type: "POST",
          datatype:"json",    
          data:  {opcion:opcion, InvID:InvID},    
          success: function() {
              tablaExp.ajax.reload(null, false); //tablaInv.row(fila.parents('tr')).remove().draw();                  
           }
        });	
    }
 });
     
});
