<?php

session_start();
header("Cache-Control: no-cache, must-revalidate"); //para borrar cache
if (!isset($_SESSION['username'])) :
    include_once('index.php');
else :
    $Typ_Trk = $_SESSION['Typ_Trk'];
    $permiso = $_SESSION['Admin'];
    $Priv = $_SESSION['Priv'];
    $filtersSes = $_SESSION['Filters'];
    $rates = $_SESSION['ViewRates'];
    include_once('bd/correo-model.php');
    $regiones = getRegiones();
    $PrivR = getPrivReg($Priv);
    
    foreach($PrivR as $R){
        $Re[] = $R['RegNam'];
    }

    /*$j = 0;
    while($j < count($Re)):
        echo $Re[$j];
        $j++;
    endwhile;*/

    $_SESSION['Filters'] = array("muestras" => "200",
                                "idGlobal" => "",
								"regionfil" => "",
                                "asLots" => "",
								"DepRegFil" => "",
								"ArrRegFil" => "",
								"DepArrDate" => "",
								"fromdate" => "",
								"todate" => "",
                "idTrk" => ""
                                 );
    foreach($regiones as $reg){
        $regs[] = $reg['RegNam'];
    }
?>

    <!doctype html>
    <html lang="es en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="img/ecom.png" />
        <title>Trucks</title>
        
        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="./assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="./assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="./main.css">
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    
    </head>

    <body>

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
        <!-- Codigo para realizar funciones relacionadas con la DO dentro del formulario-->
        

        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <!-- librerias necesarias para finalizar sesion por inactividad -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
        <!-- Scrip para finalizar sesion por inactividad -->
        <script type="text/javascript" src="timer.js"></script>


        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>



        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>

        <!-- importo la libreria moments -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.0/moment.min.js"></script>
        <!-- importo todos los idiomas -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.0/moment-with-locales.min.js"></script>

        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>

        <?php if ($Typ_Trk == 1) { ?>
            <script type="text/javascript" src="mainTrkv2.js"></script>
        <?php } else {  ?>
            <script type="text/javascript" src="mainTR.js"></script>
        <?php } ?>


        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item nav-item col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>

                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="#">Trucks</a></li>
                        <li><a class="dropdown-item" href="lots.php">Lots</a></li>
                        <?php if ($_SESSION['username'] == 'ACENCION PANI' || $_SESSION['username'] == 'DARIO ROMERO' || $_SESSION['username'] == 'CESAR ARMAS' || $_SESSION['username'] == 'LUIS ANGEL RENDON TARIN'){?>
                        <li><a class="dropdown-item" href="bales.php">Bales</a></li>
                        <?php } ?>
                        <li><a class="dropdown-item" href="do.php">Delivery Order</a></li>
                        <li><a class="dropdown-item" href="adjustments.php">Adjustments</a></li>
                        <li><a class="dropdown-item" href="inv.php">Invoice</a></li>
                        <li><a class="dropdown-item" href="export.php">Exports</a></li>
                        <li><a class="dropdown-item" href="clients.php">Clients</a></li>
                        <li><a class="dropdown-item" href="routes.php">Routes</a></li>
                        <li><a class="dropdown-item" href="requisition.php">Requisition</a></li>
                        <li><a class="dropdown-item" href="CMS.php">CMS</a></li>
                        <?php if ($permiso == 1){?>
                        <li><a class="dropdown-item" href="adminusers.php">User Management</a></li>
                        <?php } ?>
                        <?php if ($rates == 1){?>
                        <li><a class="dropdown-item" href="Rates.php">Rates</a></li>
                        <?php } ?>
                    </ul>
                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-2 col-md-2 col-sm-1 col-xs-2" href="main.php">
                    <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <p>/ Trucks</p>
                </div>
                <div class="container-fluid col-xl-7 col-lg-4 col-md-4 col-sm-2 col-xs-1">
                    <!-- Botones -->
                    <?php if ($Typ_Trk == 1 && $_SESSION['username'] != 'JAIR DEL MORAL') : ?>
                        <button id="btnNuevo" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Add"><i class="bi bi-plus-square"></i></button>
                        <button id="btnSplit" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Split a Lot"><i class="bi bi-scissors"></i></button>
                        <?php if ($_SESSION['username'] == 'ACENCION PANI' || $_SESSION['username'] == 'DARIO ROMERO') : ?>
                        <button onclick="location.href='./bd/exportExcel.php'" id="export" type="submit" class="btn btn-info" data-toggle="modal" title="Export to Excel"><i class="bi bi-file-earmark-excel-fill"></i></button>
                        <button id="btnFile" type="button" class="btn btn-info" data-toggle="modal" title="Import CSV"><i class="bi bi-filetype-csv"></i></button>
                        <?php endif ?>
                        <button id="transit" class="btn btn-success" title="Trucks in transit"><i class="bi bi-truck"></i></button>
                    <?php endif ?>
                    
                    <?php if ($Typ_Trk == 2 || $_SESSION['username'] == 'RUBY FRAGA' || $_SESSION['username'] == 'ARACELI SANTOS') : ?>
                        <button onclick="location.href='./bd/exportExcel.php'" id="export" type="submit" class="btn btn-info" data-toggle="modal" title="Export to Excel"><i class="bi bi-file-earmark-excel-fill"></i></button>
                        <button id="btnFile" type="button" class="btn btn-info" data-toggle="modal" title="Import CSV"><i class="bi bi-filetype-csv"></i></button>
                    <?php endif ?>
                    
                    <?php if ($_SESSION['username'] == 'JAIR DEL MORAL' || $_SESSION['username'] == 'ACENCION PANI') : ?>
                    <button id="btnPOPR" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="Remove PO and PR"><i class="bi bi-trash3"></i></button>
                    <?php endif ?>    
                </div>
                </div>
                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-4 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                    <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"  value="<?php echo $_SESSION['username']; ?>"><?php echo $_SESSION['username']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        
        <!-- Div oculto para almacenar regiones de los usuarios -->
        <div id="Regiones" style="display:none;">
            <input id="Priv" value="<?php echo $_SESSION['Priv']; ?>"/>
        </div> 
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->




        <!-- Aquí inicia todo código de tablas etc -->

        <!-- Inicio de la tabla -->
        <div class="card card-body" style="opacity:100%;">
            <div class="table-responsive" style="opacity:100%;">
                <table id="tablaTrk" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;">
                    <thead style="background-color: #65ac7c;" style="opacity:100%;">
                        <tr>
                            <th class="th-sm">TrkID</th>
                            <th class="th-sm">Delivery Order</th>
                            <th class="th-sm">Type</th>
                            <th class="th-sm">Associated Lots</th>
                            <th class="th-sm">Qty</th>
                            <th class="th-sm">Scheduled Departure Date</th>
                            <th class="th-sm">Departure Date</th>
                            <th class="th-sm">Scheduled Departure Time</th>
                            <th class="th-sm">Departure Time</th>
                            <th class="th-sm">Scheduled Arrival Date</th>
                            <th class="th-sm">Arrival Date</th>
                            <th class="th-sm">Scheduled Arrival Time</th>
                            <th class="th-sm">Arrival Time</th>
                            <th class="th-sm">Gin</th>
                            <th class="th-sm">Departure Region</th>
                            <th class="th-sm">Arrival Region</th>
                            <th class="th-sm">Contract</th>
                            <th class="th-sm">Client</th>
                            <th class="th-sm">Transport</th>
                            <th class="th-sm">Waybill</th>
                            <th class="th-sm">Driver License</th>
                            <th class="th-sm">Trk Plate</th>
                            <th class="th-sm">Trailer Plate</th>
                            <th class="th-sm">Driver Name</th>
                            <th class="th-sm">Driver Telephone</th>
                            <th class="th-sm">Purchase Weight</th>
                            <th class="th-sm">Departure Wgh</th>
                            <th class="th-sm">Arrival Wgh</th>
                            <th class="th-sm">PR</th>
                            <th class="th-sm">PO</th>
                            <th class="th-sm">RO</th>
                            <th class="th-sm">Freight Cost</th>
                            <th class="th-sm">Comments</th>
                            <th class="th-sm">Status</th>
                            <th class="th-sm">Irrigation Date</th>
                            <th class="th-sm">Creation Date</th>
                            <th class="th-sm">Carta Porte PDF</th>
                            <th class="th-sm">XML</th>
                            <th class="th-sm">Samples</th>
                            <!--<th class="th-sm">Booking</th>
                            <th class="th-sm">Container</th>
                            <th class="th-sm">Seals</th>
                            <th class="th-sm">CAAT</th>-->
                            <?php if ($Typ_Trk == 1) { ?>
                                <th class="no-exportar"></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="th-sm">TrkID</th>
                            <th class="th-sm">Delivery Order</th>
                            <th class="th-sm">Type</th>
                            <th class="th-sm">Associated Lots</th>
                            <th class="th-sm">Qty</th>
                            <th class="th-sm">Scheduled Departure Date</th>
                            <th class="th-sm">Departure Date</th>
                            <th class="th-sm">Scheduled Departure Time</th>
                            <th class="th-sm">Departure Time</th>
                            <th class="th-sm">Scheduled Arrival Date</th>
                            <th class="th-sm">Arrival Date</th>
                            <th class="th-sm">Scheduled Arrival Time</th>
                            <th class="th-sm">Arrival Time</th>
                            <th class="th-sm">Gin</th>
                            <th class="th-sm">Departure Region</th>
                            <th class="th-sm">Arrival Region</th>
                            <th class="th-sm">Contract</th>
                            <th class="th-sm">Client</th>
                            <th class="th-sm">Transport</th>
                            <th class="th-sm">Waybill</th>
                            <th class="th-sm">Driver License</th>
                            <th class="th-sm">Trk Plate</th>
                            <th class="th-sm">Trailer Plate</th>
                            <th class="th-sm">Driver Name</th>
                            <th class="th-sm">Driver Telephone</th>
                            <th class="th-sm">Purchase Weight</th>
                            <th class="th-sm">Departure Wgh</th>
                            <th class="th-sm">Arrival Wgh</th>
                            <th class="th-sm">PR</th>
                            <th class="th-sm">PO</th>
                            <th class="th-sm">RO</th>
                            <th class="th-sm">Freight Cost</th>
                            <th class="th-sm">Comments</th>
                            <th class="th-sm">Status</th>
                            <th class="th-sm">Irrigation Date</th>
                            <th class="th-sm">Creation Date</th>
                            <th class="th-sm">Carta Porte PDF</th>
                            <th class="th-sm">XML</th>
                            <th class="th-sm">Samples</th>
                            <!--<th class="th-sm">Booking</th>
                            <th class="th-sm">Container</th>
                            <th class="th-sm">Seals</th>
                            <th class="th-sm">CAAT</th>-->
                            <?php if ($Typ_Trk == 1) { ?>
                                <th class="th-sm"></th>
                            <?php } ?>
                        </tr>
                    </tfoot>

                </table>
            </div>

        </div>

        <!--Modal para CRUD data-keyboard="false" -->
        <div class="modal hide fade in" data-bs-backdrop="static" class="modal" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <!--g" role="document">-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formTrk">
                        <div class="modal-body" id="InData">
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Delivery O. <font size=2>*</font></label>
                                        <input type="text" class="form-control form-control-sm" id="DOrd" required>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Departure Reg.</label>
                                        <input type="text" class="form-control form-control-sm" id="OutRegDO" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Arrival Reg.</label>
                                        <input type="text" class="form-control form-control-sm" id="InRegDO" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Type</label>
                                        <input type="text" class="form-control form-control-sm" id="TypDO" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Client</label>
                                        <input type="text" class="form-control form-control-sm" id="Client" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Trk Plate</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="TrkLPlt" pattern="[A-Za-z0-9]+">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Trailer Plate</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="TraLPlt" pattern="[A-Za-z0-9]+">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Transport</label>
                                        <select class="form-control form-control-sm" id="TNam">
                                        </select>
                                        <!--                <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="TNam" required>-->
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Driver Name</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="DrvNam">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Waybill</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="WBill">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label fro="" class="col-form-label">Driver Telephone</label>
                                        <input type="text" class="form-control form-control-sm" id="DrvTel" placeholder="xxx-xxx-xxxx" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" maxlength="12">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Driver License</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="DrvLcs">
                                    </div>
                                </div>
                                <div class="col-lg-2">                                   
                                        <div class="form-group">
                                            <label for="" class="col-form-label">PR</label>
                                            <select class="form-control form-control-sm" id="RqstID">
                                            </select>
                                        </div>                                    
                                </div>

                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label ">PO</label>
                                        <input type="text" class="form-control form-control-sm" id="PO" readonly>
                                    </div>
                                </div>

                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">RO</label>
                                        <input type="text" class="form-control form-control-sm" id="RO">
                                    </div>
                                </div>
                                
                            </div>
                            <div class="strike">
                                <span>Departure</span>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Date (Scheduled)<font size=2>*</font></label>
                                        <input type="date" class="form-control form-control-sm" id="SchOutDat" onkeydown="return false" required>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Time (Scheduled)</label>
                                        <input type="time" class="form-control form-control-sm" id="SchOutTime" value="12:00:00" max="22:30:00" min="8:30:00" step="1">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Date<font size=2>*</font></label>
                                        <input type="date" class="form-control form-control-sm" id="OutDat" onkeydown="return false">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Time</label>
                                        <input type="time" class="form-control form-control-sm" id="OutTime" value="12:00:00" max="22:30:00" min="8:30:00" step="1">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Weight</label>
                                        <input type="text" class="form-control form-control-sm" id="OutWgh" onkeyup="peso_salida()">
                                        <div id="NoPWgh" style="display: none">
                                            <small id="emailHelp" class="form-text text-muted">Purchase Weight hasn't been registered.</small>
                                        </div>
                                        <div id="OutWghMin" style="display: none">
                                            <small id="emailHelp" class="form-text text-muted">Departure Weight less than Purchase Weight.</small>
                                        </div>
                                        <div id="OutWghMax" style="display: none">
                                            <small id="emailHelp" class="form-text text-muted">Departure Weight too high compared to Purchase Weight.</small>
                                        </div>
                                    </div>
                                </div>

                                 <!-- PESO PROMEDIO POR PACA SALIDA -->
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Avarange Weight </label>
                                        <input type="text" class="form-control form-control-sm" id="promsalida" readonly>
                                    </div>
                                </div>
                                <!-- TERMINA PESO PROMEDIO POR PACA -->



                            </div>
                            <div class="strike">
                                <span>Arrival</span>
                            </div>
                            <div class="row">
                            <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Date (Scheduled)<font size=2>*</font></label>
                                        <input type="date" class="form-control form-control-sm" id="SchInDat" onkeydown="return false" required>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Time (Scheduled)</label>
                                        <input type="time" class="form-control form-control-sm" id="SchInTime" value="12:00:00" max="22:30:00" min="8:30:00" step="1">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Date<font size=2>*</font></label>
                                        <input type="date" class="form-control form-control-sm" id="InDat" onkeydown="return false">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Time</label>
                                        <input type="time" class="form-control form-control-sm" id="InTime" value="12:00:00" max="22:30:00" min="8:30:00" step="1">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Weight</label>
                                        <input type="text" class="form-control form-control-sm" id="InWgh"  onkeyup="peso_llegada()">
                                        <div id="NoWgh" style="display: none">
                                            <small id="emailHelp" class="form-text text-muted">Purchase Weight hasn't been registered.</small>
                                        </div>
                                        <div id="InWghMin" style="display: none">
                                            <small id="emailHelp" class="form-text text-muted">Arrival Weight less than Departure Weight.</small>
                                        </div>
                                        <div id="InWghMax" style="display: none">
                                            <small id="emailHelp" class="form-text text-muted">Arrival Weight too high compared to Departure Weight.</small>
                                        </div>
                                    </div>
                                </div>

                                 <!-- PESO PROMEDIO POR PACA LLEGADA -->
                                 <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Avarange Weight </label>
                                        <input type="text" class="form-control form-control-sm" id="promllegada" readonly>
                                    </div>
                                </div>
                                <!-- TERMINA PESO PROMEDIO POR PACA -->
                            </div>
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Comments</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="TrkCmt">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Status</label>
                                        <select class="form-control form-control-sm" id="Status" name="Status">
                                            <option value="Programmed">Programmed</option>
                                            <option value="Transit">Transit</option>
                                            <option value="Received">Received</option>
                                            <option value="Cancelled">Cancelled</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Irrigation Date</label>
                                        <input type="date" class="form-control form-control-sm" id="IrrDat">
                                    </div>
                                </div>
                            </div>
                            <div id="LotsTrk">
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Lots</label>
                                        <select class="form-control form-control-sm" id="LotsAssc">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Associated Lots</label>
                                        <input type="text" class="form-control form-control-sm" id="AsscLots" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Qty</label>
                                        <input type="text" class="form-control form-control-sm" id="QtyDO" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Purchase Wgh</label>
                                        <input type="text" class="form-control form-control-sm" id="PWgh" readonly>
                                    </div>
                                </div>

                                <div class="col-lg-2">
                                    <div id ="centrar">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Samples</label>
                                            <br>
                                            <input class="form-check-input" type="checkbox" id="samples">
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class='col-lg-3'>
                                    <div class='form-group'>
                                        <button type="button" id="btnAddLot" class="btn btn-sm btn-dark" style="margin-top: 0.6em;">Add Lot</button>
                                        <button type="button" id="btnDelLot" class="btn btn-sm btn-dark" style="margin-top: 0.6em;">Remove Lot</button>
                                        <!--                    <button class='btn btn-sm btnAddLot'><i class="material-icons">add_box</i></button>-->
                                        <!--                    <button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>Cancel</i></button>-->
                                    </div>
                                </div>
                                <!--
                                <div id="EditCost" class='col-lg-9'>
                                    <div class='form-group'>
                                        <button type="button" id="btnFreightC" class="btn btn-sm btn-dark" style="float: right; margin-top: 0.6em;">Edit Freight Cost</button>
                                    </div>
                                </div>
                                 -->
                            </div>
                            </div>
                            <font size=2>*Required Fields</font>
                        </div>

                        <div class="row">
                                <div class="col-lg-3" id="documento" style="display: none">
                                    <div class="input-group mb-3">
                                    <label class="input-group-text" for="actualfile"> Waybill PDF</label>
                                    <input  class="form-control me-2" type="text" name="actualfile" id="actualfile" placeholder="" readonly>           
                                    <button class="btn btn-danger btn-sm viewfile"><i class="material-icons">picture_as_pdf</i></button>                               
                                    </div>
                                </div>           
                                
                                <div class="col-lg-3" id="documentoxml" style="display: none">
                                    <div class="input-group mb-3">
                                    <label class="input-group-text" for="actualfilexml"> Waybill XML</label>
                                    <input  class="form-control me-2" type="text" name="actualxml" id="actualxml" placeholder="" readonly>           
                                    <button class="btn btn-secondary  btn-sm btndownloadxml"><i class="material-icons">file_download</i></button>                               
                                    </div>
                                </div>    
                                

                        </div>
                        <div id="divCreate">
                            <div class="modal-footer">
                                <!--<button class='btn btn-success btn-sm btnBuscar'><i class='material-icons'>manage_search</i></button>-->
                                <div id="avisoCrud"></div>
                                 
                                <button type="button" class="btn btn-light" id="btnCancel" data-bs-dismiss="modal">Cancel</button>
                                <button type="submit" id="btnGuardar" class="btn btn-dark">Create Truck</button>
                            </div>
                        </div>
                        <div id="divAsociar">
                            <div class="modal-footer">
                                <!--<button class='btn btn-success btn-sm btnBuscar'><i class='material-icons'>manage_search</i></button>-->
                                <div id="avisoCrud"></div>
                             <!------------BOTONES PARA CARGAR PDF Y XML DE LA CARTA PORTE------- -->                               
                                <button id="btnloadfile" data-dismiss="modal" data-toggle="modal"  style="display:none" class="btn btn-primary" >Load File PDF</button>
                                <button id="btnupdate" style="display:none"  class="btn btn-success" >Update File PDF</button>                               
                               
                                <div class="me-auto">
                                    <button id="btnloadxml" data-dismiss="modal" data-toggle="modal"  style="display:none" class="btn btn-primary" >Load XML</button> 
                                    <button id="btnupdatexml" style="display:none "  class=" btn btn-success " >Update XML</button>
                                </div>               
                                <!-----------------AQUI TEMINAN------------- -->
                                <button type="button" class="btn btn-light" id="btnCancel" data-bs-dismiss="modal">Cancel</button>
                                <button  id="btntarifario"  class="btn btn-dark" >Tarifario</button>
                                <button type="submit" id="btnGuardar" class="btn btn-dark">Save</button>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Modal asociar OE y lotes-->
        <div class="modal fade" id="modalOEL" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formOEL">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Delivery Order:</label>
                                        <input type="text" class="form-control" id="DO" required><br><button class='btn btn-success btn-sm btnBuscarLot'><i class='material-icons'>search</i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Lots:</label>
                                        <select class="form-control" id="Lot" name="Lot">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Qty:</label>
                                        <input type="text" class="form-control" id="QtyLot">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-dark btn-sm btnAdd"><i class="material-icons">add_box</i></button>
                            <button class="btn btn-light" data-bs-dismiss="modal"><i class="material-icons">highlight_off</i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <!--Modal para Split de Lotes-->
        <div class="modal fade" id="SplitLots" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="Split-Lots">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label fro="" class="col-form-label">Location<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="LocSplit" required>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">
                                            <!--<input type="checkbox" name="InDO" id="InDO" checked>-->
                                            Delivery Order
                                        </label>
                                        <input type="text" class="form-control form-control-sm" name="DOSplit" id="DOSplit">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <br>
                                        <button type="button" id="btnFndLot" class="btn btn-sm btn-dark"><i class="material-icons">search</i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Lot - Quantity (BC)</label>
                                        <select class="form-control form-control-sm" id="LotsSplit" required>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label fro="" class="col-form-label">Inserts the partitions separated by a comma (E.g. 40,80)</label>
                                        <input type="text" class="form-control form-control-sm" id="Split" required>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="row">
                    <div class="col-lg-9">
                    <div class="form-group">
                    <label fro="" class="col-form-label">Inserts the partitions separated by a comma (for example 40,80)</label>
                    <input type="text" class="form-control form-control-sm" id="Split" required>
                    </div>
                    </div>
                </div>-->
                        </div>
                        <div class="modal-footer">
                            <!--<button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>-->
                            <div id="avisoLot"></div>
                            <button type="submit" id="btnGuardar" class="btn btn-dark">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <!--Modal PO's-->
        <div class="modal fade" id="modalFile" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formFile">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Select File</label>
                                        <input type="file" class="form-control" id="csv" name="csv">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" id="btnGuardarFile" class="btn btn-dark">Import</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="modal hide fade" id="filtrarmodal" data-bs-backdrop="static" tabindex="-1" role="dialog" data-bs-keyboard="false" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-tittle">Filter table by</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="filtros" class="filtros">
                            <div class="input-group mb-3">
                                <label class="input-group-text" for="idDO">Truck ID</label>
                                <input  class="form-control me-2" type="text" name="idDO" id="idTrk" placeholder="Write Truck ID" >
                                <label class="input-group-text" for="idDO">ID Delivery Order</label>
                                <input  class="form-control me-2" type="text" name="idDO" id="idDO" placeholder="Write Delivery Order" >
                                <label class="input-group-text" for="asLots">Associated Lots</label>
                                <input  class="form-control me-2" type="text" name="asLots" id="asLots" placeholder="Write Associated Lots" >
                                <label class="input-group-text " for="muestras">Last trucks</label>
                                <select class="form-select me-2" name="muestras" id="muestras">
                                    <option value="200" selected>200 (default)</option>
                                    <option value="ALL">All</option>
                                    <?php $x = 25; for($i=1; $i<7; $i++): ?>
                                        <option value="<?php echo $x*2; ?>"><?php echo $x*2; $x*=2;?></option>
                                    <?php endfor; ?>
                                    
                                </select>   
                                
                            </div>
                            <div class="input-group mb-3">
                                <label class="input-group-text" for="DepRegFil">Select Departure Region</label>
                                <select class="form-select me-2" name="DepRegFil" id="DepRegFil">
                                    <option value="">Choose...</option>
                                    <?php $i = 0; while($i < count($regs)): ?>
                                    <option value="<?php echo $regs[$i]; ?>"><?php echo $regs[$i]; ?></option>
                                    <?php $i++; endwhile; ?>
                            
                                </select>
                                <label class="input-group-text" for="ArrRegFil">Select Arrival Region</label>
                                <select class="form-select me-2" id="ArrRegFil" name="ArrRegFil">
                                    <option value="">Choose...</option>
                                <?php $i=0; while($i < count($regs)): ?> 
                                    <option value="<?php echo $regs[$i]; ?>"><?php echo $regs[$i]; ?></option>
                                <?php $i++; endwhile; ?>
                                    
                                </select>
                            </div>
                            <div class="input-group mb-3">
                            
                                <label class="input-group-text" for="DepArrDate">Select Departure or Arrival Date</label>
                                <select class="form-select me-2" id="DepArrDate" name="DepArrDate">
                                    <option value="" selected >Choose...</option>
                                    <option value="DepartureDate">Departure Date</option>
                                    <option value="ArrivalDate">Arrival Date</option>
                                    
                                </select>
                            
                                <label class="input-group-text" for="timeSelect">Select Range</label>
                                <select class="form-select me-2" id="timeSelect" name="timeSelect">
                                    <option value="" selected >Choose...</option>
                                    <option value="7">Last week</option>
                                    <option value="14">Last 2 weeks</option>
                                    <option value="30">Last month</option>
                                    <option value="60">Last 2 months</option>
                                    <option value="120">Last 4 months</option>
                                    <option value="180">Last 6 months</option>
                                    <option value="365">Last 1 year </option>


                                    
                                </select>
                               
                            </div>
                            <div class="input-group mb-3">
                            
                                <label class="input-group-text" for="fromdate">From date:</label>
                                <input type="date" class="form-control me-4" name="fromdate" id="fromdate">
                                <label class="input-group-text" for="todate">To date:</label>
                                <input type="date" class="form-control me-2" name="todate" id="todate">
                            </div>

                                
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div style="height: 2.5em;" id="aviso"></div>
                        <button id="cer" type="button" class="btn btn-light" value="3" data-bs-dismiss="modal" >Close</button>
                        <button id="borrarFiltro" type="button" value="0" class="btn btn-danger Filtro" >Clear filters</button>
                        <button id="buscafiltro" type="button" value="1" class="btn btn-primary Filtro" disabled>Apply filters</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal para eliminar PO's y PR's  -->                             
        <div class="modal hide fade" id="modalPOPR" data-bs-backdrop="static" tabindex="-1" role="dialog" data-bs-keyboard="false" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-tittle">Remove PO's and PR's</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="POPR" class="filtros">
                            <div class="input-group mb-3">
                                <label class="input-group-text" for="idDO">TrkID</label>
                                <input  class="form-control me-2" type="text" name="RTrkID" id="RTrkID" >
                                <label class="input-group-text" for="asLots">DO</label>
                                <input  class="form-control me-2" type="text" name="RDO" id="RDO" readonly>
                                <label class="input-group-text" for="asLots">Type</label>
                                <input  class="form-control me-2" type="text" name="RTyp" id="RTyp" readonly>
                                <label class="input-group-text" for="asLots">PR</label>
                                <input  class="form-control me-2" type="text" name="RPO" id="RPR" readonly>
                                <label class="input-group-text" for="asLots">PO</label>
                                <input  class="form-control me-2" type="text" name="RPR" id="RPO" readonly>
                                
                            </div>
                            <div class="input-group mb-3">
                                <label class="input-group-text" for="asLots">Lots</label>
                                <input  class="form-control me-2" type="text" name="RLots" id="RLots" readonly>
                                <label class="input-group-text" for="idDO">Transport</label>
                                <input  class="form-control me-2" type="text" name="RTNam" id="RTNam" readonly>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div style="height: 2.5em;" id="avisoPOPR"></div>
                        <button id="cerrPOPR" type="button" class="btn btn-light" data-bs-dismiss="modal" >Close</button>
                        <button id="RemPOPR" type="button" class="btn btn-primary Filtro">Remove</button>
                        <!--<button id="borrarFiltro" type="button" value="0" class="btn btn-danger Filtro" >Clear filters</button>-->
                    </div>
                </div>
            </div>
        </div>
<!-- FORMULARIO PARA SUBIR REMISIONES-->
<div class="modal fade" id="filesload" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formFile">
                        <div class="modal-body">
                             
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="" class="col-form-label"></label>
                                        <input type="file"  accept=".pdf,.Pdf" class="form-control" name="archivo" id="file" required onchange="upfile()" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                        <div style="height: 1em;" id="avisofile"></div>
                        <button id="loadfile"  style="display:none" class="btn btn-primary" disabled>Load File</button>
                        <button id="update" style="display:none"  class=" btn btn-success" disabled>Update File</button>
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                        

                        </div>
                      
                    </form>
                </div>
            </div>
        </div>



                <!----------------------------------------------------- FORMULARIO PARA ENVIAR CORREO ---------------------------------------------------->
            <div class="modal hide fade" id="Correo" data-bs-backdrop="static" tabindex="-1" role="dialog" data-bs-keyboard="false" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-tittle">Preview email</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="correo" class="correo">
                        <div class="input-group mt-3">
                                <label class="input-group-text" for="extraEmails" >Mails Transporte:  </label>
                                <input type="text" placeholder="Separados por coma" class="form-control extraEmails" id="mailtransporte">
                        </div>

                        <div class="row" style="display: none" id ="origenes">
                        <div class="input-group mt-3" >
                                <label class="input-group-text" for="extraEmails" >Mail Origen:  </label>
                                <input type="text" placeholder="Separados por coma" class="form-control extraEmails" id="mailorigen">
                        </div>
                        </div>
                        <div class="input-group mt-3">
                                <label class="input-group-text" for="extraEmails" >Mail AMSA:  </label>
                                <input type="text" placeholder="Separados por coma" class="form-control extraEmails" id="mailamsa">
                        </div>
                        <div class="input-group mt-3">
                                <label class="input-group-text" for="subject">Subject</label>
                                <input type="text" class="form-control subject" id="subject">
                                <div class="form-check form-switch ms-2 mt-2">
                                
                                </div>
                         </div>
                            
                            <br>
                            <div class="strike">
                                <span>Body Mail</span>
                            </div>

                            <div class="form-group">                              
                                        
                                        <textarea class="form-control observ" id="datoscarga" rows="7" readonly></textarea>
                            </div>
                             <!--ALERTA QUE SE MUESTRA SI EL LOTE NO TIENE UN TRKID ASIGNADO-->
                             <span class="badge rounded-pill bg-danger" id ="estatus"></span>
                            
                            <!--        
                            <div class="input-group mt-3">
                            <label class="input-group-text" for="observ">Datos de descarga </label>
                                <textarea  class="form-control observ" id="datosdescarga" rows="4" readonly></textarea>
                            </div>
                            -->

     

                           <!--          
                            <div  class="input-group mt-3">
                            <label class="input-group-text" for="tablamue">Lots</label>
                                <div style="overflow-y: scroll; max-height: 13rem;" class="form-control tablamue" id="tablamue"></div>
                            </div>
                            -->

                             <!-- 
                            <div class="input-group mt-3">
                            <label class="input-group-text" for="observ">Datos de Trasporte</label>
                                <textarea  class="form-control observ" id="datostransport" rows="4" readonly></textarea>
                            </div>
                            -->

                                    
                            <div class="input-group mt-3" hidden>
                                <label class="input-group-text" for="link">Link Maps</label>
                                <input type="text" class="form-control subject" id="link">
                                <div class="form-check form-switch ms-2 mt-2">                                
                                </div>
                            </div>
                           

                            <div class="input-group mt-3">
                            <label class="input-group-text" for="comentarios">Comentarios Adicionales</label>
                                <textarea  class="form-control observ" id="comentarios" rows="3" ></textarea>
                            </div>


                            <input type="text" class="form-control tablahtml" id="tablahtml" hidden >
                            <input type="text" class="form-control usuario" id="usuario" hidden>
                            <input type="text" class="form-control typeDO" id="typeDO" hidden>
                            <input type="text" class="form-control lugar_carga" id="lugar_carga" hidden>
                            <input type="text" class="form-control direccion_carga" id="direccion_carga" hidden>
                            <input type="text" class="form-control referencia_carga" id="referencia_carga" hidden>
                            <input type="text" class="form-control fecha_salida" id="fecha_salida" hidden>                          
                            <input type="text" class="form-control datos_trasporte" id="datos_trasporte" hidden>
                            <input type="text" class="form-control placastruck" id="placastruck" hidden>
                            <input type="text" class="form-control placascaja" id="placascaja" hidden>
                            <input type="text" class="form-control operadortruck" id="operadortruck" hidden>
                            <input type="text" class="form-control talontruck" id="talontruck" hidden>
                            <input type="text" class="form-control telchofer" id="telchofer" hidden>
                            <input type="text" class="form-control celular" id="celular" hidden>
                            <input type="text" class="form-control referencia_descarga" id="referencia_descarga" hidden>
                            <input type="text" class="form-control fecha_llegada" id="fecha_llegada" hidden>
                            <input type="text" class="form-control lugar_descarga" id="lugar_descarga" hidden>
                            <input type="text" class="form-control direccion_descarga" id="direccion_descarga" hidden>
                            <input type="text" class="form-control delivery" id="delivery" hidden>
                            <input type="text" class="form-control client" id="client" hidden>
                            <input type="text" class="form-control regarrival" id="regarrival" hidden>
                            <input type="text" class="form-control trkid" id="trkid" hidden>
                            <input type="text" class="form-control horasalida" id="horasalida" hidden>
                            <input type="text" class="form-control horallegada" id="horallegada" hidden>

                            
                            
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="lotess" id="lotess"></div>
                        <div style="height: 1em;" id="avisomail"></div>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button id="sendEmail" type="button" class="btn btn-primary sendEmail">Send Email</button>
                    </div>
                </div>
            </div>
        </div>


        <!--------------------------------------------FORMULARIO PARA SUBIR XML DE CARTA PORTE----------------------------------------->
<div class="modal fade" id="xml_load" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="formxmls">
                        <div class="modal-body">
                             
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="" class="col-form-label"></label>
                                        <input type="file"  accept=".XML,.xml,.Xml" class="form-control" name="archivo" id="filexml" required onchange="upfilexml()">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                        <div style="height: 1em;" id="avisoxml"></div>

                      
                        <button id="loadxml"  style="display:none" class="btn btn-primary" disabled>Load XML</button>
                        <button id="updatexml" style="display:none"  class=" btn btn-success" disabled>Update XML</button>

                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                        

                        </div>
                      
                    </form>
                </div>
            </div>
        </div>




          <!----------------------------------------------------- FORMULARIO PARA TARIFARIO ---------------------------------------------------->
          <div class="modal hide fade" id="tarifario" data-bs-backdrop="static" tabindex="-1" role="dialog" data-bs-keyboard="false" aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-tittle">TARIFARIO</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                    <div class="modal-body">
                        <form id="formulario-tarifario" class="formulario-tarifario">
                            

                            <div class="row">
                              <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Transport</label>
                                        <input type="text" class="form-control form-control-sm" id="tarifario-trasnport" readonly>    
                                                                         
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Departure Region</label>
                                        <input type="text" class="form-control form-control-sm" id="tarifario-regsalida" readonly>    
                                                                             
                                    </div>
                                </div>

                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Gin</label>                                        
                                        <input type="text" class="form-control form-control-sm" id="tarifario-gin" readonly>                                   
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Departure Zone</label>                                        
                                        <input type="text" class="form-control form-control-sm" id="tarifario-zonasalida" readonly>                                   
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Arrival Region</label>                                        
                                        <input type="text" class="form-control form-control-sm" id="tarifario-regllegada" readonly>                                   
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Arrival City</label>                                        
                                        <input type="text" class="form-control form-control-sm" id="tarifario-cityllegada" readonly>                                   
                                    </div>
                                </div>


                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Qty</label>                                        
                                        <input type="text" class="form-control form-control-sm" id="tarifario-cantidad" readonly>                                   
                                    </div>
                                </div>


                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label"> Cotton Samples</label>                                        
                                        <input type="text" class="form-control form-control-sm" id="tarifario-muestras" readonly>                                   
                                    </div>
                                </div>                                                     
                            
                            
                            </div>

                            <div class="row">
                                <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="col-form-label"> Authorized Average Cost</label>                                        
                                            <input type="text" class="form-control form-control-sm" id="tarifario-costopaca" readonly>                                   
                                        </div>
                                </div>          
                                <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="col-form-label"> Authorized Total Cost</label>                                        
                                            <input type="text" class="form-control form-control-sm" id="tarifario-costoauto" readonly>                                   
                                        </div>
                                </div>   
                                <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="col-form-label"> Total Cost</label>                                        
                                            <input type="numbre" step="0.01" class="form-control form-control-sm" id="tarifario-costototal"  onkeyup="cost_total()"  oninput="alerta()" value="0" >                                   
                                        </div>
                                </div>   
                                
                              

                                <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Average Cost </label>                                        
                                            <input type="text" class="form-control form-control-sm" id="tarifario-costoprom" readonly>                                   
                                        </div>
                                </div>   


                            </div>
                            <div class="row">

                                <div class="col-lg-9">
                                        <div class="form-group">
                                            <label for="" class="col-form-label">Comentarios </label>                                        
                                            <input type="text" class="form-control form-control-sm" id="tarifario-comentarios" onkeyup="boxcomentario()" >                                   
                                        </div>
                                </div>   


                            </div>

                            <!-- LINEAS PARA SIMBOLO DE ALERTA EN TARIFARIO -->

                            <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                            <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                                <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                            </symbol>
                            </svg>

                             <!-- TERMINA SIMBOLO DE ALERTA EN TARIFARIO -->
                            
                            <br>
                            <div class="row" id="alertacosto"style="display:none">
                                <div class="col-lg-12">
                                    
                                    <div class="alert alert-warning d-flex align-items-center" role="alert">
                                        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Warning:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                                        <div id="alertacostotext" >
                                            El costo total supera el costo autorizado. Agregar comentario para guardar
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="tarifa_inactiva"style="display:none">
                                <div class="col-lg-12">
                                    
                                    <div class="alert alert-info d-flex align-items-center" role="alert">
                                        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Warning:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                                        <div id="mensaje">
                                            La tarifa Autorizada ya no es vigente 
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="lotess" id="lotess"></div>
                        <div style="height: 1em;" id="avisomail"></div>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button id="tarifario-save" type="button" class="btn btn-primary ">Save </button>
                    </div>
                </div>
            </div>
        </div>     

        <!-- Aquí termina-->

    </body>

    </html>
<?php
endif;
?>